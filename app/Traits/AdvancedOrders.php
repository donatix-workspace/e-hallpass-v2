<?php

namespace App\Traits;

use App\Models\Pass;
use Illuminate\Database\Eloquent\Builder;

trait AdvancedOrders
{
    /**
     * @param Builder $query
     * @return Builder
     */
    public static function orderByJoin(Builder $query): Builder
    {
        $sort = explode(':', request()->get('sort'));

        if (count($sort) <= 1) {
            return $query->orderBy('id', 'asc');
        }

        $sortingKeywords = ['asc', 'desc'];
        if (in_array($sort[1], $sortingKeywords)) {

            $relation = explode('.', $sort[0]);

            if (count($relation)) {
                //TODO make order by relationship column
                return $query->orderBy('id', 'asc');
            }

            return $query->orderBy($sort[0], $sort[1]);
        }
        return $query->orderBy('id', 'asc');
    }

}
