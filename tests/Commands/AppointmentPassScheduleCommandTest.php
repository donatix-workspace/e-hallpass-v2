<?php

namespace Tests\Commands;

use App\Events\AppointmentPassRemind;
use App\Events\AppointmentPassRun;
use App\Events\AppointmentPassSendEmail;
use App\Models\AppointmentPass;
use App\Models\Pass;
use App\Models\Period;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\ModuleSeeder;
use Database\Seeders\RolesDatabaseSeeder;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AppointmentPassScheduleCommandTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;
    protected $firstUser;
    protected $teacherUser;
    protected $pass;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();
        (new ModuleSeeder())->run();

        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);

        $this->teacherUser = User::factory()->createOne([
            'role_id' => config('roles.teacher'),
            'school_id' => $this->school->id
        ]);

        $this->pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->adminUser->id,
            'user_id' => $this->firstUser->id
        ]);

        SchoolModule::create([
            'school_id' => $this->school->id,
            'module_id' => 1, // Appointment pass
            'option_json' => '{"email": 1}'
        ]);
    }


    /**
     * @test
     * The command will send a notification for first reminder
     * @return void
     * @throws Exception
     */
    public function the_command_will_send_a_notification_for_first_reminder()
    {
        Carbon::setTestNow('01/01/2020 13:00');

        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);


        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => config('roles.system'),
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::now(optional($this->school)->getTimezone())->setTime(13,00)->addMinutes(AppointmentPass::FIRST_REMIND_MINUTE),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'confirmed_by_teacher_at' => Carbon::now(optional($this->school)->getTimezone()),
            'reason' => 'reason'
        ]);

        $this->expectsEvents([
            AppointmentPassRemind::class
        ]);

        // Events that should be ran after the tests
        $this->artisan('passes:appointment_schedule')->assertExitCode(0)->run();
    }

    /**
     * @test
     * The command will run the pass last 5 minutes or less
     * @return void
     * @throws Exception
     */
    public function the_command_will_run_the_pass_last_5_minutes_or_less()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => config('roles.system'),
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::now(optional($this->school)->getTimezone())->addMinutes(AppointmentPass::SECOND_REMIND_MINUTE),
            'school_id' => $this->firstUser->school_id,
            'confirmed_by_teacher_at' => Carbon::now(optional($this->school)->getTimezone()),
            'period_id' => $period->id,
            'acknowledge_at' => Carbon::now()->subMinutes(5),
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        // Events that shoud be runned after the tests
        $this->expectsEvents([
            AppointmentPassRun::class,
        ]);
        $this->artisan('passes:appointment_schedule')->assertExitCode(0)->run();
    }

    /**
     * @test
     * The command will send email to the user for the appointment pass agenda
     * @return void
     * @throws Exception
     */
    public function the_command_will_send_email_to_the_user_for_the_appointment_pass_agenda()
    {
        Carbon::setTestNow(Carbon::parse('today at 7 am'));
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => config('roles.system'),
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::today()->addHours(7),
            'school_id' => $this->firstUser->school_id,
            'confirmed_by_teacher_at' => Carbon::now(optional($this->school)->getTimezone()),
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        // Events that should be ran after the tests
        $this->expectsEvents([
            AppointmentPassSendEmail::class,
        ]);

        $this->artisan('passes:appointment_email')->assertExitCode(0)->run();
    }

    /**
     * @test
     * The command will create a pass on second reminder
     * @return void
     */
    public function the_command_will_create_a_pass_on_second_reminder()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => config('roles.system'),
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'confirmed_by_teacher_at' => Carbon::now(optional($this->school)->getTimezone()),
            'for_date' => Carbon::now(optional($this->school)->getTimezone())->format('Y-m-d H:i:s'),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'acknowledge_at' => Carbon::now(optional($this->school)->getTimezone())->subMinutes(5),
            'reason' => 'reason_from_apt'
        ]);
        $this->artisan('passes:appointment_schedule')->assertExitCode(0)->run();

        $this->assertNotNull($appointmentPass->refresh()->pass_id);
        $this->assertDatabaseHas('passes', [
            'from_id' => config('roles.system'),
            'requested_at' => Carbon::parse($appointmentPass->for_date, optional($this->school)->getTimezone())
                ->setTimezone('UTC')
                ->format('Y-m-d H:i:s')
        ]);
    }
}
