<?php

namespace App\Traits;

use App\Events\DashboardBuildingRefreshEvent;
use App\Events\DashboardRefreshEvent;
use App\Models\Pass;
use App\Models\Room;
use App\Models\StaffSchedule;
use App\Models\User;

trait DashboardRefreshable
{
    /**
     * Refresh the dashboard to the correct users
     * @return void
     */
    public function refreshDashboard(): void
    {
        $notifiableUserIds = [];

        if ($this->from_type === User::class && $this->from_id !== 0) {
            array_push($notifiableUserIds, $this->from_id);
            //            event(
            //                new DashboardRefreshEvent(
            //                    $this->school_id,
            //                    User::find($this->from_id)
            //                )
            //            );
        }

        if ($this->to_type === User::class && $this->to_id !== 0) {
            array_push($notifiableUserIds, $this->to_id);
            //            event(
            //                new DashboardRefreshEvent(
            //                    $this->school_id,
            //                    User::find($this->to_id)
            //                )
            //            );
        }

        if ($this->canceled_by) {
            array_push($notifiableUserIds, $this->canceled_by);
            //            event(
            //                new DashboardRefreshEvent(
            //                    $this->school_id,
            //                    User::find($this->canceled_by)
            //                )
            //            );
        }

        if ($this->approved_by) {
            array_push($notifiableUserIds, $this->approved_by);
            //            event(
            //                new DashboardRefreshEvent(
            //                    $this->school_id,
            //                    User::find($this->approved_by)
            //                )
            //            );
        }

        if ($this->completed_by) {
            array_push($notifiableUserIds, $this->completed_by);
            //            event(
            //                new DashboardRefreshEvent(
            //                    $this->school_id,
            //                    User::find($this->completed_by)
            //                )
            //            );
        }

        if ($this->to_type === Room::class) {
            $staffScheduleToUserId = StaffSchedule::where(
                'room_id',
                $this->to_id
            )
                ->select('user_id')
                ->get()
                ->pluck('user_id')
                ->toArray();
            $notifiableUserIds = array_merge(
                $staffScheduleToUserId,
                $notifiableUserIds
            );

            //            foreach ($staffScheduleToUserId as $userId) {
            //                event(
            //                    new DashboardRefreshEvent(
            //                        $this->school_id,
            //                        User::find($userId)
            //                    )
            //                );
            //            }
        }

        if ($this->from_type === Room::class) {
            $staffScheduleToUserId = StaffSchedule::where(
                'room_id',
                $this->from_id
            )
                ->select('user_id')
                ->get()
                ->pluck('user_id')
                ->toArray();

            $notifiableUserIds = array_merge(
                $staffScheduleToUserId,
                $notifiableUserIds
            );

            //            foreach ($staffScheduleToUserId as $userId) {
            //                event(
            //                    new DashboardRefreshEvent(
            //                        $this->school_id,
            //                        User::find($userId)
            //                    )
            //                );
            //           $userId }
        }

        $notifiableUserIds = array_values(array_unique($notifiableUserIds));
        logger('Socket dashboard notifiable ids: ', [$notifiableUserIds]);

        foreach ($notifiableUserIds as $notifyUserId) {
            // We use this in the foreach because, it may be maximum of 4 users associated here. (4+1)
            $notifiableUser = User::find($notifyUserId);

            if ($notifiableUser) {
                event(
                    new DashboardRefreshEvent($this->school_id, $notifiableUser)
                );
            }
        }

        event(new DashboardBuildingRefreshEvent($this->school_id));
    }
}
