<?php

namespace App\Mail\Custom;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use InvalidArgumentException;
use App\Mail\Custom\Mailable;

class MailManager extends \Illuminate\Mail\MailManager
{
    /**
     * Resolve the given mailer.
     *
     * @param  string  $name
     * @return Mailable
     *
     * @throws InvalidArgumentException
     */
    protected function resolve($name): Mailable
    {
        $config = $this->getConfig($name);

        if (is_null($config)) {
            throw new InvalidArgumentException(
                "Mailer [{$name}] is not defined."
            );
        }

        // Once we have created the mailer instance we will set a container instance
        // on the mailer. This allows us to resolve mailer classes via containers
        // for maximum testability on said classes instead of passing Closures.
        $mailer = new Mailable(
            $name,
            $this->app['view'],
            $this->createSwiftMailer($config),
            $this->app['events']
        );

        if ($this->app->bound('queue')) {
            $mailer->setQueue($this->app['queue']);
        }

        // Next we will set all of the global addresses on this mailer, which allows
        // for easy unification of all "from" addresses as well as easy debugging
        // of sent messages since these will be sent to a single email address.
        foreach (['from', 'reply_to', 'to', 'return_path'] as $type) {
            $this->setGlobalAddress($mailer, $config, $type);
        }

        return $mailer;
    }

    /**
     * Set a global address on the mailer by type.
     *
     * @param  Mailable $mailer
     * @param  array  $config
     * @param  string  $type
     * @return void
     */
    protected function setGlobalAddress($mailer, array $config, string $type)
    {
        $address = Arr::get(
            $config,
            $type,
            $this->app['config']['mail.' . $type]
        );

        if (is_array($address) && isset($address['address'])) {
            $mailer->{'always' . Str::studly($type)}(
                $address['address'],
                $address['name']
            );
        }
    }
}
