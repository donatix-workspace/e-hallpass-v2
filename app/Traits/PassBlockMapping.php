<?php

namespace App\Traits;

trait PassBlockMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'school_id' => [
                'type' => 'long'
            ],
            'reason' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'from_date' => [
                'type' => 'date',
                "format" => "MM/dd/yyyy h:m a",
                "fields" => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'to_date' => [
                'type' => 'date',
                "format" => "MM/dd/yyyy h:m a",
                "fields" => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ]
        ]
    ];
}
