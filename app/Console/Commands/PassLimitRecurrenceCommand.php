<?php

namespace App\Console\Commands;

use App\Models\PassLimit;
use Illuminate\Console\Command;

class PassLimitRecurrenceCommand extends Command
{
    protected $signature = 'passes:limit_recurrence';

    protected $description = 'Recurrence pass limits';

    public function handle()
    {
        $passLimits = PassLimit::where('archived', 0)
            ->where('recurrence_type', '!=', null)
            ->get();

        foreach ($passLimits as $passLimit) {
            if ($passLimit->isDaily() || $passLimit->isWeekly() || $passLimit->isMonthly()) {
                $passLimit->updateByRecurrence();
            }
        }
    }
}
