<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Filters\DashboardFilter;
use App\Http\Filters\PassHistoryFilter;
use App\Imports\PassesImport;
use Excel;
use Log;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ImportController extends Controller
{
    /**
     * Import pass history data as csv
     *
     * @authenticated
     * @param DashboardFilter $filters
     * @return \Illuminate\Http\JsonResponse|BinaryFileResponse
     */
    public function export(DashboardFilter $filters)
    {
        try {
            return Excel::download(
                new PassesImport($filters, true),
                'pass-dashboard.csv'
            );
        } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
            Log::error($e);
            return response()->json([
                'data' => [],
                'status' => __('fail'),
                'message' => __('something.went.wrong')
            ]);
        }
    }
}
