<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->unsignedBigInteger('parent_id')->nullable()->default(null);
            $table->text('from_teacher_note')->nullable();
            $table->bigInteger('school_id')->unsigned()->nullable();
            $table->morphs('from');
            $table->morphs('to');
            $table->unsignedBigInteger('requested_by')->nullable();
            $table->unsignedBigInteger('approved_by')->nullable();
            $table->unsignedBigInteger('completed_by')->nullable();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->text('reason')->nullable();
            $table->smallInteger('ordering')->nullable()->default(0);
            $table->text('to_teacher_note')->nullable();
            $table->boolean('pass_status')->default(1);
            $table->enum('approved_with', [\App\Models\Pass::BY_AUTOPASS, \App\Models\Pass::BY_CHECKIN])->nullable();
            $table->enum('ended_with', [\App\Models\Pass::BY_AUTOPASS, \App\Models\Pass::BY_CHECKIN])->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->timestamp('completed_at')->nullable();
            $table->timestamp('requested_at')->nullable();
            $table->timestamp('arrived_at')->nullable();
            $table->enum('type', [\App\Models\Pass::PROXY_PASS, \App\Models\Pass::APPOINTMENT_PASS, \App\Models\Pass::STUDENT_CREATED_PASS, \App\Models\Pass::KIOSK_PASS]); // Proxy, Student, Kiosk etc
            $table->timestamp('expired_at')->nullable();
            $table->timestamp('canceled_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passes');
    }
}
