<?php

namespace App\Policies;

use App\Models\RoomRestriction;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RoomRestrictionPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view any room restrictions.
     *
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the room restriction.
     *
     * @param User $user
     * @param RoomRestriction $roomRestriction
     * @return bool
     */
    public function view(User $user, RoomRestriction $roomRestriction)
    {
        return $user->isAdmin() && $user->school_id === $roomRestriction->school_id;
    }

    /**
     * Determine whether the user can create room restrictions.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the room restriction.
     *
     * @param User $user
     * @param RoomRestriction $roomRestriction
     * @return bool
     */
    public function update(User $user, RoomRestriction $roomRestriction)
    {
        return $user->isAdmin() && $user->school_id === $roomRestriction->school_id;
    }

    /**
     * Determine whether the user can delete the room restriction.
     *
     * @param User $user
     * @param RoomRestriction $roomRestriction
     * @return bool
     */
    public function delete(User $user, RoomRestriction $roomRestriction)
    {
        return $user->isAdmin() && $user->school_id === $roomRestriction->school_id;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function bulkDelete(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can restore the room restriction.
     *
     * @param User $user
     * @param RoomRestriction $roomRestriction
     * @return bool
     */
    public function restore(User $user, RoomRestriction $roomRestriction)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the room restriction.
     *
     * @param User $user
     * @param RoomRestriction $roomRestriction
     * @return bool
     */
    public function forceDelete(User $user, RoomRestriction $roomRestriction)
    {
        //
    }
}
