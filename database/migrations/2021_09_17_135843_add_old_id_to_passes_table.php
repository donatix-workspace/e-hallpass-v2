<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOldIdToPassesTable extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('passe', 'old_id')) {
            Schema::table('passes', function (Blueprint $table) {
                $table->bigInteger('old_id');
            });
        }
    }

    public function down()
    {
        Schema::table('passes', function (Blueprint $table) {
            //
        });
    }
}
