SET FOREIGN_KEY_CHECKS = 0;
START TRANSACTION ;
truncate ehp_v2_prod.rooms_restrictions;
INSERT INTO ehp_v2_prod.rooms_restrictions (room_id, school_id, user_id, type, from_date, to_date, recurrence_type, grade_year, status, created_at, updated_at) SELECT
                                                                                                                                                                    (SELECT id FROM ehp_v2_prod.rooms WHERE old_id = oldLa.room_id limit 1),
                                                                                                                                                                    (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldLa.school_id limit 1),
                                                                                                                                                                    (SELECT id FROM ehp_v2_prod.users WHERE old_id=oldLa.student_id limit 1),
                                                                                                                                                                    (SELECT IF(oldLa.limitation_type = 1, 'access_denied', 'members_only')),
                                                                                                                                                                    (SELECT DATE(oldLa.from_datetime)),
                                                                                                                                                                    (SELECT DATE(oldLa.to_datetime)),
                                                                                                                                                                    (SELECT NULL),
                                                                                                                                                                    (SELECT IF(oldLa.gradeyear > 0, CONCAT('["',oldLa.gradeyear,'"]'), (SELECT NULL))),
                                                                                                                                                                    (SELECT oldLa.status),
                                                                                                                                                                    (SELECT CONVERT_TZ(oldLa.updated_at,
                                                                                                                                                                                       (SELECT timezone from ehp_v2_prod.schools where old_id = oldLa.school_id limit 1),
                                                                                                                                                                                       'UTC'
                                                                                                                                                                                )),
                                                                                                                                                                    (SELECT CONVERT_TZ(oldLa.created_at,
                                                                                                                                                                                       (SELECT timezone from ehp_v2_prod.schools where old_id = oldLa.school_id limit 1),
                                                                                                                                                                                       'UTC'
                                                                                                                                                                                ))

FROM ehallpassDB.location_availability as oldLa;
COMMIT ;
SET FOREIGN_KEY_CHECKS = 1;
