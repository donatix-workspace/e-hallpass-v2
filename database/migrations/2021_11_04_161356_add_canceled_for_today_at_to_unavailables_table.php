<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCanceledForTodayAtToUnavailablesTable extends Migration
{
    public function up()
    {
        Schema::table('unavailables', function (Blueprint $table) {
            $table->timestamp('canceled_for_today_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('unavailables', function (Blueprint $table) {
            $table->dropColumn('canceled_for_today_at');
        });
    }
}
