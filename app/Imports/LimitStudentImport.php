<?php

namespace App\Imports;


use App\Models\PassLimit;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class LimitStudentImport implements FromQuery, WithHeadings, WithMapping
{
    public $user;

    /**
     *
     */
    public function __construct()
    {
        $this->user = auth()->user();
    }


    /**
     * @return PassLimit|\Illuminate\Database\Eloquent\Builder|Builder
     */
    public function query()
    {
        return PassLimit::where('school_id', optional($this->user)->school_id);
    }

    public function headings(): array
    {
        return [
            'Student',
            'From Date',
            'To Date',
            'Limit',
            'Repeat',
            'Reason',
            'Created on'
        ];
    }

    public function map($row): array
    {
        $type = null;

        if ($row->limitable_type === School::class) {
            $type = optional($row->limitable)->name;
        }

        if ($row->limitable_type === User::class) {
            $type = optional($row->limitable)->first_name . ' ' . optional($row->limitable)->last_name;
        }

        if ($row->limitable_type === School::class && $row->grade_year !== null) {
            $type = json_decode($row->grade_year);
            $type = implode(',', $type);
        }

        return [
            $type,
            School::convertTimeToCorrectTimezone($row->from_date, $this->user->school->getTimezone(), true),
            School::convertTimeToCorrectTimezone($row->to_date, $this->user->school->getTimezone(), true),
            $row->limit,
            $row->recurrence_type,
            $row->reason,
            School::convertTimeToCorrectTimezone($row->created_at, $this->user->school->getTimezone()),
        ];
    }
}
