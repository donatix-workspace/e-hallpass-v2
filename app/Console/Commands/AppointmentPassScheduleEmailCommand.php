<?php

namespace App\Console\Commands;

use App\Mail\AppointmentsAgendaMail;
use App\Models\AppointmentPass;
use App\Models\AuthType;
use App\Models\Module;
use App\Models\School;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;

class AppointmentPassScheduleEmailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'passes:appointment_email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email with agenda for today\'s passes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $usersEmailForAgenda = User::select(['u.id', 'u.email', 'u.school_id'])
            ->withoutGlobalScopes()
            ->from('users as u')
            ->leftJoin('appointment_passes as ap', function ($join) {
                $join
                    ->on('ap.from_id', '=', 'u.id')
                    ->where('ap.from_type', User::class)
                    ->whereBetween('ap.for_date', [
                        now()
                            ->subDay()
                            ->startOfDay()
                            ->toDateTimeString(),
                        now()
                            ->endOfDay()
                            ->toDateTimeString()
                    ])
                    ->where('ap.canceled_at', null)
                    ->where('ap.confirmed_by_teacher_at', '!=', null)
                    ->where('ap.expired_at', null)
                    ->where('ap.ran_at', null);
            })
            ->rightJoin('appointment_passes as ap2', function ($join) {
                $join
                    ->on('ap2.from_id', '=', 'u.id')
                    ->where('ap2.from_type', User::class)
                    ->whereBetween('ap2.for_date', [
                        now()
                            ->subDay()
                            ->startOfDay()
                            ->toDateTimeString(),
                        now()
                            ->endOfDay()
                            ->toDateTimeString()
                    ])
                    ->where('ap2.canceled_at', null)
                    ->where('ap2.confirmed_by_teacher_at', '!=', null)
                    ->where('ap2.expired_at', null)
                    ->where('ap2.ran_at', null);
            })
            ->join('schools_users as su', function ($join) {
                $join
                    ->on('su.user_id', '=', 'u.id')
                    ->where('su.role_id', '!=', config('roles.student'));
            })
            ->where(function ($subQuery) {
                $subQuery
                    ->where('u.auth_type', '!=', AuthType::HIDDEN)
                    ->orWhereNull('u.auth_type');
            })
            ->where(function ($subQuery) {
                $subQuery
                    ->where('u.auth_type', '!=', AuthType::HIDDEN)
                    ->orWhereNull('u.auth_type');
            })
            ->whereRaw("not (u.username <=> 'system')")
            ->where('u.deleted_at', null)
            ->where('u.status', User::ACTIVE)
            ->where('u.is_substitute', User::INACTIVE)
            ->cursor();

        $preventDuplicateSendEmailList = [];

        foreach ($usersEmailForAgenda as $user) {
            $school = School::findCacheFirst($user->school_id);

            $nowTimeInTheSchool = now()
                ->setTimezone(optional($school)->getTimezone())
                ->format('h');

            $remindHour = now()
                ->parse(AppointmentPass::EMAIL_REMIND_HOUR)
                ->format('g');

            $nowHourToInteger = intval($nowTimeInTheSchool);
            $remindHourToInt = intval($remindHour);
            $appointments = $this->getAppointmentsOfTheUser(
                $user->id,
                $user->school_id,
                $school->getTimezone()
            );

            if (
                $remindHourToInt == $nowHourToInteger &&
                $appointments->count() > 0
            ) {
                if (!in_array($user->email, $preventDuplicateSendEmailList)) {
                    Mail::to($user->email, $user->school_id)->send(
                        new AppointmentsAgendaMail($appointments)
                    );
                }
                $preventDuplicateSendEmailList[] = $user->email;
            }
        }

        return 0;
    }

    /**
     * @param $userId
     * @param $schoolId
     * @param $schoolTimezone
     * @return AppointmentPass[]|Builder[]|Collection|\Illuminate\Support\Collection
     */
    public function getAppointmentsOfTheUser(
        $userId,
        $schoolId,
        $schoolTimezone
    ) {
        return AppointmentPass::with(['user', 'from', 'to', 'school'])
            ->where('school_id', $schoolId)
            ->orderBy('confirmed_by_teacher_at', 'asc')
            ->where('confirmed_by_teacher_at', '!=', null)
            ->whereBetween('for_date', [
                now()
                    ->setTimezone($schoolTimezone)
                    ->startOfDay()
                    ->toDateTimeString(),
                now()
                    ->setTimezone($schoolTimezone)
                    ->endOfDay()
                    ->toDateTimeString()
            ])
            ->where('expired_at', null)
            ->where('ran_at', null)
            ->whereCanceledAt(null)
            ->where(function ($query) use ($userId) {
                $query
                    ->where('from_type', User::class)
                    ->where('from_id', $userId)
                    ->orWhere(function ($query) use ($userId) {
                        $query
                            ->where('to_type', User::class)
                            ->where('to_id', $userId);
                    });
            })
            ->get();
    }
}
