<?php

namespace App\Console\Commands\Migrations;

use App\Models\Room;
use App\Models\User;
use Illuminate\Console\Command;

class MigrateRoomPinCommand extends Command
{
    protected $signature = 'migrate_sys:room_pin';

    protected $description = 'Migrate room pins from EHPv1 to EHPv2';

    public function handle()
    {
        $this->info('--- Migrating Room Pins ---');

        $newDb = \DB::connection('mysql');
        $oldDb = \DB::connection('oldDB');

        $oldDb->disableQueryLog();
        $newDb->disableQueryLog();

        $roomPins = $oldDb->table('room_pins')
            ->orderBy('created_at', 'asc')
            ->get()
            ->toArray();

        $this->output->progressStart(count($roomPins));

        $roomPinsChunks = array_chunk($roomPins, 1500);

        foreach ($roomPinsChunks as $roomPinChunk) {
            $insertableRoomPinChunk = [];
            foreach ($roomPinChunk as $roomPin) {
                $oldSchool = $oldDb->table('school_schedule')->where('school_id', $roomPin->school_id)
                    ->first();

                if ($oldSchool) {
                    $newSchool = $newDb->table('schools')
                        ->select('id')
                        ->where('old_id', $oldSchool->school_id)
                        ->first();

                    if ($newSchool) {
                        $oldRoom = $oldDb->table('rooms')->select('room_id')
                            ->where('room_id', optional($roomPin)->room_id)
                            ->where('school_id', $oldSchool->school_id)
                            ->first();

                        $newRoom = $newDb->table('rooms')
                            ->where('old_id', optional($oldRoom)->room_id)
                            ->select('id')
                            ->where('school_id', $newSchool->id)
                            ->first();

                        $insertableRoomPinChunk[] = [
                            'school_id' => $newSchool->id,
                            'pinnable_type' => Room::class,
                            'pinnable_id' => $newRoom->id,
                            'pin' => $roomPin->pin,
                            'created_at' => now(),
                            'updated_at' => now()
                        ];
                    }
                }
            }

            try {
                $newDb->table('pins')
                    ->insert($insertableRoomPinChunk);
                $this->output->progressAdvance(count($insertableRoomPinChunk));
            } catch (\Exception $e) {
                $this->info($e->getMessage());
            }

            $this->output->progressFinish();
        }
    }
}
