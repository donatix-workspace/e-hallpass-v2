<?php

namespace App\Http\Controllers\Admin\Polarities;

use App\Http\Controllers\Controller;
use App\Imports\PolaritiesImport;
use App\Models\Polarity;
use App\Models\PolarityReport;
use Excel;

class ExportController extends Controller
{
    /**
     * Get all polarities by csv.
     *
     *
     * @authenticated
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function index()
    {
        $user = auth()->user();

        if (!request()->has('detailled_report')) {
            $polaritiesQuery = $this->polarityQuery($user);
        } else {
            $polaritiesQuery = $this->polarityReportQuery($user);
        }

        return Excel::download(
            new PolaritiesImport(
                $polaritiesQuery,
                request()->has('detailled_report')
            ),
            'polarities.csv'
        );
    }

    /**
     * @param $user
     * @return PolarityReport|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function polarityReportQuery($user)
    {
        $searchQuery =
            request()->get('search_query') !== null
                ? convertElasticQuery(request()->get('search_query'))
                : config('scout_elastic.all_records');

        if (request()->has('sort') || request()->has('search_query')) {
            $polarityReports = PolarityReport::search($searchQuery)
                ->when(request()->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('id', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                    return $query->orderBy('id', 'asc');
                })
                ->where('school_id', $user->school_id)
                ->take(
                    request()->get(
                        'per_page',
                        config('pagination.default_pagination')
                    )
                )
                ->get()
                ->pluck('id')
                ->toArray();

            $implodedIds = implode(',', $polarityReports);

            $polarityReportsQuery = PolarityReport::whereIntegerInRaw(
                'id',
                $polarityReports
            )
                ->orderByRaw("field(id, $implodedIds)")
                ->where('school_id', $user->school_id);
        } else {
            $polarityReportsQuery = PolarityReport::whereSchoolId(
                $user->school_id
            );
        }

        return $polarityReportsQuery;
    }

    /**
     * @param $user
     * @return Polarity|\Illuminate\Database\Eloquent\Builder
     */
    public function polarityQuery($user)
    {
        if (request()->has('search_query') || request()->has('sort')) {
            $polarities = collect(
                Polarity::search(
                    request()->get('search_query') === null
                        ? config('scout_elastic.all_records')
                        : convertElasticQuery(request()->get('search_query'))
                )
                    ->where('school_id', $user->school_id)
                    ->when(request()->has('sort'), function ($query) {
                        $sort = explode(':', request()->get('sort'));
                        if (count($sort) <= 1) {
                            return $query->orderBy('id', 'asc');
                        }

                        $sortingKeywords = ['asc', 'desc'];
                        if (in_array($sort[1], $sortingKeywords)) {
                            return $query->orderBy($sort[0], $sort[1]);
                        }
                        return $query->orderBy('id', 'asc');
                    })
                    ->take(Polarity::recordsPerPage())
                    ->get()
            )
                ->pluck('id')
                ->toArray();

            $polaritiesIdsImploded = implode(',', $polarities);

            $polaritiesQuery = Polarity::whereIntegerInRaw('id', $polarities)
                ->orderByRaw("field(id,$polaritiesIdsImploded)")
                ->fromSchoolId($user->school_id);
        } else {
            $polaritiesQuery = Polarity::fromSchoolId($user->school->id);
        }

        return $polaritiesQuery;
    }
}
