<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AutoPass extends Model
{
    use HasFactory;

    protected $table = 'auto_passes';

    public const ACTIVE = 1;
    public const INACTIVE = 0;

    protected $fillable = [
        'room_id', 'school_id',
    ];

//    /**
//     * @param $existingRoomIds
//     * @param $newRoomIds
//     * @return array
//     */
//    public static function differences($existingRoomIds, $newRoomIds)
//    {
//        $changesExistOne = array_diff($existingRoomIds, $newRoomIds);
//        $changesCurrentOne = array_diff($newRoomIds, $existingRoomIds);
//
//
//        return [
//            'changesExistOne' => $changesExistOne,
//            'changesCurrentOne' => $changesCurrentOne
//        ];
//    }


    /**
     * @return LocationCapacity|mixed|null
     */
    public function getLimitAttribute()
    {
        if ($this->room) {
            if ($this->room->capacity !== null) {
                return $this->room->capacity->limit;
            }
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getRoomAdvanceAttribute()
    {
        $roomAdvanceProperties = $this->room->capacity()
            ->where('room_id', $this->room_id)
            ->where('school_id', $this->school_id)
            ->with('room:id,name')
            ->first();

        return $roomAdvanceProperties !== null ? $roomAdvanceProperties : [
            'room' => [
                'id' => $this->room->id, 'name' => $this->room->name
            ]
        ];
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeOfRoomId($query, $room)
    {
        return $query->where('room_id', $room);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function preferences()
    {
        return $this->hasMany(AutoPassPreference::class);
    }
}
