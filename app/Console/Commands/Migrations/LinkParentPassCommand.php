<?php

namespace App\Console\Commands\Migrations;

use Illuminate\Console\Command;

class LinkParentPassCommand extends Command
{
    protected $signature = 'migrate_sys:link_pass';

    protected $description = 'Link parent and child passes together from EHPV1 to EHPV2';

    public function handle()
    {
        $this->info('--- Link parent passes to child passes from EHPV1 to EHPV2 ---');

        $newDb = \DB::connection('mysql');

        $passes = $newDb->table('passes')
            ->where('old_parent_id', '>', 0)
            ->get()
            ->toArray();

        $this->output->progressStart(count($passes));

        $passesChunks = array_chunk($passes, 1500);

        $newDb->disableQueryLog();

        foreach ($passesChunks as $passesChunk) {
            foreach ($passesChunk as $pass) {
                $passParent = $newDb->table('passes')->where('old_id', $pass->old_parent_id)
                    ->select('id')
                    ->first();

                $newDb->table('passes')
                    ->where('id', $pass->id)
                    ->update(['parent_id' => optional($passParent)->id]);
                $this->output->progressAdvance();
            }
        }
        $this->output->progressFinish();
    }
}
