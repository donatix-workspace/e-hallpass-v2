<?php

namespace App\Http\Controllers\Admin\Transparency;

use App\Http\Controllers\Controller;
use App\Http\Requests\TransparencyUserRequest;
use App\Http\Resources\TransparencyCollection;
use App\Http\Resources\TransparencyResource;
use App\Models\Transparency;
use App\Models\TransparencyUser;
use App\Models\User;
use DB;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

/**
 * @group Pass Transparency.
 */
class TransparencyController extends Controller
{
    /**
     * Transparency: Get all users that can view all passes
     *
     * @queryParam search_query string Search with elastic
     *
     * @authenticated
     * @return TransparencyCollection
     * @apiResourceCollection App\Http\Resources\TransparencyCollection
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Transparency::class);

        $user = auth()->user();

        if (request()->has('search_query') || request()->has('sort')) {
            $transparencyUsers = TransparencyUser::search(
                request()->input('search_query') === null
                    ? config('scout_elastic.all_records')
                    : convertElasticQuery(request()->get('search_query'))
            )
                ->where('school_id', $user->school_id)
                ->when(request()->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('id', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                    return $query->orderBy('id', 'asc');
                })
                ->paginate(TransparencyUser::recordsPerPage());
        } else {
            $transparencyUsers = TransparencyUser::fromSchoolId(
                $user->school_id
            )->paginate(TransparencyUser::recordsPerPage());
        }

        $transparencyExists = Transparency::fromSchoolId(
            $user->school_id
        )->exists();

        return new TransparencyCollection([
            'transparency_users' => $transparencyUsers,
            'transparency_flags' => [
                'exists' => $transparencyExists
            ]
        ]);
    }

    /**
     * Update Transparency state.
     *
     * @bodyParam status boolean required The transparency status state.
     *
     * @authenticated
     * @param Request $request
     * @return TransparencyResource
     * @apiResourceModel App\Models\Transparency
     * @apiResource App\Http\Resources\TransparencyResource
     * @throws AuthorizationException
     */
    public function update(Request $request)
    {
        $this->authorize('update', Transparency::class);

        // There's no need to create Request class for that.
        $request->validate(['status' => 'boolean|required']);

        if ($request->input('status')) {
            $transparency = Transparency::firstOrCreate([
                'school_id' => auth()->user()->school_id
            ]);

            return new TransparencyResource($transparency);
        }
        $transparency = Transparency::fromSchoolId(auth()->user()->school_id);
        $transparencyRecord = $transparency->first();
        $transparency->delete();

        return new TransparencyResource($transparencyRecord);
    }

    /**
     * Create a transparency for user
     *
     * @bodyParam user_ids array required The id's of the user for transparency.
     *
     * @param TransparencyUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws AuthorizationException
     * @throws \Throwable
     * @apiResourceModel App\Models\Transparency
     * @apiResource App\Http\Resources\TransparencyResource
     * @authenticated
     */
    public function store(
        TransparencyUserRequest $request
    ): \Illuminate\Http\JsonResponse {
        $user = auth()->user();

        $now = now();

        $this->authorize('createTransparencyUser', Transparency::class);

        $userIds = $request->input('user_ids');

        $transparencyUserInsertArray = [];

        foreach ($userIds as $userId) {
            if (
                TransparencyUser::whereSchoolId(auth()->user()->school_id)
                    ->where('user_id', $userId)
                    ->exists()
            ) {
                return response()->json(
                    [
                        'data' => ['user_id' => $userId],
                        'message' => __('record.already.exists'),
                        'status' => __('fail')
                    ],
                    422
                );
            }

            $transparencyUserInsertArray[] = [
                'user_id' => $userId,
                'school_id' => $user->school_id,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        DB::beginTransaction();
        TransparencyUser::insert($transparencyUserInsertArray);
        TransparencyUser::where('school_id', $user->school_id)
            ->where('created_at', $now->toDateTimeString())
            ->searchable();
        DB::commit();

        return response()->json([
            'data' => [],
            'message' => __('success'),
            'status' => __('success')
        ]);
    }

    /**
     * Delete a transparency by id
     *
     * @bodyParam transparency_id integer required The id of the transparency for deleting
     *
     * @param Transparency $transparency
     * @return TransparencyResource
     * @apiResourceModel App\Models\Transparency
     * @apiResource App\Http\Resources\TransparencyResource
     * @throws AuthorizationException
     */
    public function delete(TransparencyUser $transparencyUser)
    {
        $this->authorize('deleteTransparencyUser', Transparency::class);
        $transparencyUser->delete();

        return new TransparencyResource($transparencyUser);
    }
}
