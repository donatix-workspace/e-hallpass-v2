<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMigratedToOldUsersTable extends Migration
{
    public function up()
    {
        if (config('app.migrate_mode')) {
            if (!Schema::connection('oldDB')->hasColumn('users', 'migrated')) {
                Schema::connection('oldDB')->table('users', function (Blueprint $table) {
                    $table->boolean('migrated')->default(0);
                });
            }
        }
    }

    public function down()
    {
        if (config('app.migrate_mode')) {
            Schema::table('users', function (Blueprint $table) {
                //
            });
        }
    }
}
