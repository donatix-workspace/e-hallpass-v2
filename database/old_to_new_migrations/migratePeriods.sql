SET FOREIGN_KEY_CHECKS  = 0;
TRUNCATE ehp_v2_prod.periods;
INSERT INTO ehp_v2_prod.periods (school_id, name, `order`, status, created_at, updated_at, deleted, old_id)
SELECT
    (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldPeriod.school_id limit 1),
    (SELECT oldPeriod.period_name),
    (SELECT oldPeriod.ordering),
    (SELECT IF(oldPeriod.status <= 0, 0, 1)),
    (SELECT CONVERT_TZ(oldPeriod.created_at, 'America/New_York', 'UTC')),
    (SELECT CONVERT_TZ(oldPeriod.updated_at, 'America/New_York', 'UTC')),
    (SELECT 0),
    (SELECT oldPeriod.id)
FROM ehallpassDB.school_period as oldPeriod;
SET FOREIGN_KEY_CHECKS  = 1;
