<?php

namespace App\Http\Controllers\Kiosk;

use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Models\School;
use App\Models\StudentFavorite;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    /**
     * Return the favorites that are global for the Kiosk screen
     *
     * @response status=200 {
     *    "data": [{}],
     *    "status": "success"
     * }
     *
     * @authenticated
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $school = School::select('id')
            ->where('cus_uuid', $request->get('cus_uuid'))
            ->firstOrFail();

        $defaultFavourites = StudentFavorite::query()
            ->with([
                'favorable' => function ($morphTo) {
                    $morphTo->morphWith([
                        Room::class => ['activeUnavailability', 'capacity'],
                        User::class => ['activeUnavailability']
                    ]);
                }
            ])
            ->where('global', 1)
            ->where('user_id', null)
            ->where('school_id', $school->id)
            ->where('section', StudentFavorite::DESTINATION)
            ->orderBy('position', 'asc')
            ->orderBy('section', 'asc')
            ->orderBy('global', 'asc')
            ->get();

        return response()->json([
            'data' => $defaultFavourites,
            'status' => __('success')
        ]);
    }
}
