<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PassTimeSettingsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'teacher_pass_setting' => $this->collection->get('teacher_pass_setting'),
                'pass_description' => $this->collection->get('pass_description'),
            ],
            'status' => 'success'
        ];
    }
}
