<?php

namespace App\Http\Controllers\Admin\Passes;

use App\Events\AppointmentPassCancelled;
use App\Events\AppointmentPassConfirmed;
use App\Events\AppointmentPassCreated;
use App\Events\AppointmentPassTimeChanged;
use App\Events\DashboardRefreshEvent;
use App\Http\Controllers\Controller;
use App\Http\Filters\AppointmentFilter;
use App\Http\Requests\AppointmentPassRequest;
use App\Http\Requests\AppointmentPassUpdateRequest;
use App\Http\Requests\RecurrenceAppointmentPassRequest as RecAptRequest;
use App\Http\Resources\AppointmentAndRecurrencesCollection;
use App\Http\Resources\AppointmentPassCollection;
use App\Http\Resources\AppointmentPassEntityCollection;
use App\Http\Resources\AppointmentPassResource;
use App\Http\Resources\RecurrenceAppointmentPassResource;
use App\Http\Services\ElasticSearch\Models\AppointmentPassAndRecurrences;
use App\Imports\AppointmentsImport;
use App\Jobs\GenerateAppointmentsJob;
use App\Jobs\UpdateAppointmentsJob;
use App\Models\AppointmentPass;
use App\Models\Period;
use App\Models\RecurrenceAppointmentPass;
use App\Models\Room;
use App\Models\School;
use App\Models\StaffSchedule;
use App\Models\Transparency;
use App\Models\TransparencyUser;
use App\Models\User;
use App\Notifications\Push\StudentNotification;
use App\Rules\DateIsPassBlockedRule;
use Cache;
use Carbon\Carbon;
use Excel;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Validator;

/**
 * @group AppointmentPass management.
 * API's for managing AppointmentPasses.
 */
class AppointmentController extends Controller
{
    /**
     * Get appointments entities for filters.
     *
     *
     * @apiResourceCollection App\Http\Resources\AppointmentPassEntityCollection
     * @return AppointmentPassEntityCollection
     * @throws AuthorizationException
     * @authenticated
     */
    public function index(): AppointmentPassEntityCollection
    {
        $user = auth()->user();
        $staffSchedulesRoomIds = StaffSchedule::ofUserId($user->id)->pluck(
            'room_id'
        );

        $staffMembers = Cache::remember(
            'staffMembers_appointments_school_' . $user->school_id,
            now()->addMinutes(3),
            function () use ($user) {
                return User::fromSchoolId($user->school_id)
                    ->fromStaff()
                    ->active()
                    ->where('is_searchable', User::USER_SEARCHABLE)
                    ->get([
                        'id',
                        'first_name',
                        'last_name',
                        'avatar',
                        'user_initials'
                    ]);
            }
        );

        $rooms = Cache::remember(
            'rooms_appointments_school_' . $user->school_id,
            now()->addMinutes(3),
            function () use ($user) {
                return Room::fromSchoolId($user->school_id)->get();
            }
        );

        // We get the students with they're pass limits and room restriction
        $students = User::fromSchoolId($user->school_id)
            ->withCount('passes')
            ->with(['passLimits', 'roomRestrictions'])
            ->fromStudents()
            ->get();

        // Get all of the school periods
        $periods = Period::where('status', 1)
            ->fromSchoolId($user->school_id)
            ->get();

        $this->authorize('viewAny', AppointmentPass::class);

        return new AppointmentPassEntityCollection([
            'rooms' => $rooms,
            'students' => $students,
            'users' => $staffMembers,
            'periods' => $periods,
            'assigned_rooms_ids' => $staffSchedulesRoomIds
        ]);
    }

    /**
     * Get appointments
     *
     * @queryParam per_page integer How many records will be showed on page.
     * @queryParam search_query string Elastic search query
     * @queryParam between_dates array Get the appointments between two dates Example: ["04/20/2021", "05/20/2021"]
     * @queryParam just_my_created boolean Get only my created appointment passes
     * @queryParam from_me boolean Get only passes that are from me
     * @queryParam to_me boolean Get only passes that are to me
     * @queryParam to_and_from_others boolean Get only passes that are to_and_from_others
     * @queryParam needs_confirmation boolean Get only passes that needing confirmation
     * @queryParam type_activated boolean Get all active appointment passes
     * @queryParam type_canceled boolean Get all appointments that are canceled.
     * @queryParam type_missed boolean Get all appointments that are missed.
     * @queryParam type_future boolean Get all appointments that are in the future.
     * @queryParam type_ended boolean Get all appointments that are completed.
     * @queryParam type_on_other_pass boolean Get all appointments that are on other passes
     *
     * @apiResourceCollection App\Http\Resources\AppointmentPassCollection
     * @apiResourceModel App\Models\AppointmentPass
     * @authenticated
     * @param AppointmentFilter $filters
     * @return AppointmentPassCollection
     * @throws AuthorizationException
     */
    public function filter(
        AppointmentFilter $filters
    ): AppointmentPassCollection {
        $user = auth()->user();
        $this->authorize('viewAny', AppointmentPass::class);

        // If there's transparency record show all of the appointments
        if (Transparency::isEnabled()) {
            $appointments = AppointmentPass::transparencyQueries(
                $filters,
                AppointmentPass::recordsPerPage()
            );
        } else {
            $appointments = AppointmentPass::nonSearchableQueries(
                $filters,
                AppointmentPass::recordsPerPage()
            );
        }

        if (
            request()->has('search_query') &&
            request()->get('search_query') !== null
        ) {
            $appointments = AppointmentPass::searchableQueries(
                $filters,
                AppointmentPass::recordsPerPage(),
                false,
                Transparency::isEnabled()
            );
        }

        return new AppointmentPassCollection($appointments);
    }

    /**
     * Export records as csv (with filters)
     *
     * @queryParam between_dates array Get the appointments between two dates Example: ["04/20/2021", "05/20/2021"]
     * @queryParam just_my_created boolean Get only my created appointment passes
     * @queryParam from_me boolean pGet only passes that are from me
     * @queryParam to_me boolean Get only passes that are to me
     * @queryParam to_and_from_others boolean Get only passes that are to_and_from_others
     * @queryParam needs_confirmation boolean Get only passes that needing confirmation
     * @queryParam type_activated boolean Get all active appointment passes
     * @queryParam type_canceled boolean Get all appointments that are canceled.
     * @queryParam type_missed boolean Get all appointments that are missed.
     * @queryParam type_future boolean Get all appointments that are in the future.
     * @queryParam type_ended boolean Get all appointments that are completed.
     * @queryParam type_on_other_pass boolean Get all appointments that are on other passes
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function csv(AppointmentFilter $filters): BinaryFileResponse
    {
        return Excel::download(
            new AppointmentsImport($filters),
            'csv_appointments_import_school_id_' .
                auth()->user()->school_id .
                '.csv'
        );
    }

    /**
     * Create an appointment pass record
     *
     * @bodyParam appointment array required Array of below inputs for creating many appointments record.
     * @bodyParam from_id integer required Current destination of the user
     * @bodyParam to_id integer required To what destination user wants to go
     * @bodyParam from_type string required Current destination type (Room or User)
     * @bodyParam to_type string required Target destination type (Room or User)
     * @bodyParam for_date date required Date for the appointment pass
     * @bodyParam period_id integer required The period that appointment is being created
     * @bodyParam reason string required The reason that appointment is being created
     * @bodyParam user_id integer required The id of the student user.
     *
     *
     * @param AppointmentPassRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     * @authenticated
     */
    public function store(AppointmentPassRequest $request): JsonResponse
    {
        $user = auth()->user();

        $this->authorize('create', AppointmentPass::class);

        $appointmentsRequestArray = $request->get('appointments'); // Array of appointment inputs.

        //Removed in https://eduspiresolutions.atlassian.net/browse/EHP2-1623 Comment #2
        //        $appointmentForThatDayExists = AppointmentPass::validateAppointmentDates(
        //            $appointmentsRequestArray
        //        );

        $doesPassBlockExistsValidator = Validator::make($request->all(), [
            'appointments.*.for_date' => [new DateIsPassBlockedRule()]
        ]);

        if ($doesPassBlockExistsValidator->fails()) {
            return response()->json(
                [
                    'data' => [],
                    'message' => $doesPassBlockExistsValidator
                        ->errors()
                        ->first(),
                    'status' => __('fail')
                ],
                422
            );
        }

        //Removed in https://eduspiresolutions.atlassian.net/browse/EHP2-1623 Comment #2
        //       // Check for existing appointment
        //        if ($appointmentForThatDayExists['status']) {
        //            return response()->json(
        //                [
        //                    'data' => $appointmentForThatDayExists['data'],
        //                    'message' => $appointmentForThatDayExists['message'],
        //                    'status' => __('fail')
        //                ],
        //                422
        //            );
        //        }

        $job = GenerateAppointmentsJob::dispatch(
            $appointmentsRequestArray,
            $user
        )
            ->onQueue('appointments')
            ->delay(now()->addSeconds(3));

        return response()->json(
            [
                'data' => [],
                'message' => __('passes.appointments.created'),
                'status' => __('success')
            ],
            201
        );
    }

    /**
     * Edit an appointment pass.
     *
     * @urlParam appointmentPass integer required The id of the pass
     * @bodyParam from_id integer required Current destination of the user
     * @bodyParam to_id integer required To what destination user wants to go
     * @bodyParam from_type string required Current destination type (Room or User)
     * @bodyParam to_type string required Target destination type (Room or User)
     * @bodyParam for_date date required Date for the appointment pass
     * @bodyParam period_id integer required The period that appointment is being created
     * @bodyParam reason string required The reason that appointment is being created
     * @bodyParam user_id integer required The id of the student user.
     *
     *
     * @authenticated
     * @param AppointmentPass $appointmentPass
     * @param AppointmentPassUpdateRequest $request
     * @return AppointmentPassResource|JsonResponse
     * @throws AuthorizationException
     * @throws Exception
     * @apiResource App\Http\Resources\AppointmentPassResource
     * @apiResourceModel App\Models\AppointmentPass
     */
    public function update(
        AppointmentPass $appointmentPass,
        AppointmentPassUpdateRequest $request
    ) {
        $this->authorize('update', $appointmentPass);

        $currentAppointmentDate = $appointmentPass->for_date->toDateTimeString();

        $givenDate = Carbon::parse($request->input('for_date'))
            ->setTimezone(config('app.timezone'))
            ->toDateTimeString();

        if (
            $givenDate !== $currentAppointmentDate &&
            $appointmentPass->user_id === $request->input('user_id')
        ) {
            event(
                new AppointmentPassTimeChanged(
                    $appointmentPass,
                    auth()->user()->id,
                    $currentAppointmentDate,
                    $givenDate
                )
            );
            //            Notification::send(
            //                $appointmentPass,
            //                new StudentNotification('AppointmentPassTimeChanged', [
            //                    'givenDate' => $request->input('for_date')
            //                ])
            //            );
        }

        if ($appointmentPass->user_id !== $request->input('user_id')) {
            event(
                new AppointmentPassCancelled(
                    $appointmentPass,
                    auth()->user()->id
                )
            );

            event(
                new AppointmentPassCreated($appointmentPass, auth()->user()->id)
            );

            //            Notification::send(
            //                $appointmentPass,
            //                new StudentNotification('AppointmentPassCancelled')
            //            );
        }

        if ($givenDate !== $currentAppointmentDate) {
            $appointmentForThatTimeExists = AppointmentPass::where(
                'user_id',
                $request->input('user_id')
            )
                ->where(
                    'for_date',
                    '=',
                    Carbon::parse($request->input('for_date'))
                        ->setTimezone(config('app.timezone'))
                        ->toDateTimeString()
                )
                ->where('canceled_at', null)
                ->where('expired_at', null)
                ->where('ran_at', null)
                ->where('school_id', auth()->user()->school_id)
                ->exists();

            $appointmentFor15MinExists = AppointmentPass::where(
                'user_id',
                $request->input('user_id')
            )
                ->where(
                    'for_date',
                    '=',
                    Carbon::parse($request->input('for_date'))
                        ->setTimezone(config('app.timezone'))
                        ->addMinutes(15)
                        ->setTimezone(config('app.timezone'))
                        ->toDateTimeString()
                )
                ->where('canceled_at', null)
                ->where('expired_at', null)
                ->where('ran_at', null)
                ->where('school_id', auth()->user()->school_id)
                ->exists();

            if ($appointmentForThatTimeExists || $appointmentFor15MinExists) {
                return response()->json(
                    [
                        'data' => $appointmentPass,
                        'message' => $appointmentForThatTimeExists
                            ? __('appointment.for.that.time.exists')
                            : __('appointment.15.minutes.exists'),
                        'status' => __('fail')
                    ],
                    422
                );
            }
        }

        if ($request->input('reason') !== $appointmentPass->reason) {
            $appointmentPass->comments()->create([
                'comment' => $request->input('reason'),
                'user_id' => auth()->user()->id,
                'school_id' => auth()->user()->school_id
            ]);
        }

        $appointmentPass->update([
            'from_id' => $request->input('from_id'),
            'from_type' => $request->input('from_type'),
            'to_id' => $request->input('to_id'),
            'to_type' => $request->input('to_type'),
            'user_id' => $request->input('user_id'),
            'for_date' => $request->input('for_date'),
            'school_id' => auth()->user()->school_id,
            'period_id' => $request->input('period_id'),
            'reason' => $request->input('reason'),
            'edited_at' => now()
        ]);

        $appointmentPass->changeRecurrenceIfNeeded(
            $request->input('recurrence_end_at'),
            $request->input('recurrence_type'),
            $request->input('recurrence_days'),
            $request->input('recurrence_week')
        );

        // Refresh the dashboard counter and instantly when the pass is to user
        if ($appointmentPass->to_type === User::class) {
            event(
                new DashboardRefreshEvent(
                    $appointmentPass->school_id,
                    User::findOrFail($appointmentPass->to_id)
                )
            );
            event(
                new DashboardRefreshEvent(
                    $appointmentPass->school_id,
                    auth()->user()
                )
            );

            event(
                new AppointmentPassCreated($appointmentPass, auth()->user()->id)
            );
        }

        return new AppointmentPassResource($appointmentPass->refresh());
    }

    /**
     * Cancel an appointment pass.
     *
     * @urlParam appointmentPass integer required The id of the appointment
     * @bodyParam cancel_reason string required The cancel reason.
     *
     * @authenticated
     * @param AppointmentPass $appointmentPass
     * @param Request $request
     * @return AppointmentPassResource|JsonResponse
     * @throws AuthorizationException
     *
     * @apiResource App\Http\Resources\AppointmentPassResource
     * @apiResourceModel App\Models\AppointmentPass
     */
    public function cancel(AppointmentPass $appointmentPass, Request $request)
    {
        $this->authorize('cancel', $appointmentPass);

        if (!$request->has('from_recurrence')) {
            $request->validate(['cancel_reason' => 'required|string|max:255']);
        }

        if ($appointmentPass->isCanceled()) {
            return response()->json([
                'data' => [
                    'id' => $appointmentPass->id
                ],
                'message' => __('passes.appointments.already.canceled'),
                'status' => __('fail')
            ]);
        }

        $appointmentPass->update([
            'canceled_at' => Carbon::now(),
            'canceled_by' => auth()->user()->id,
            'cancel_reason' => $request->input('cancel_reason', null)
        ]);

        if (!$request->has('from_recurrence')) {
            $appointmentPass->comments()->create([
                'comment' => $request->input('cancel_reason'),
                'user_id' => auth()->user()->id,
                'school_id' => auth()->user()->school_id
            ]);
        }

        event(
            new AppointmentPassCancelled($appointmentPass, auth()->user()->id)
        );

        //        Notification::send(
        //            $appointmentPass,
        //            new StudentNotification('AppointmentPassCancelled')
        //        );

        $appointmentPass->refreshDashboard();

        return new AppointmentPassResource($appointmentPass->refresh());
    }

    /**
     * Confirm an appointment pass.
     *
     * @urlParam appointmentPass integer required The id of the appointment
     *
     * @authenticated
     * @param AppointmentPass $appointmentPass
     * @return AppointmentPassResource
     * @throws AuthorizationException
     * @apiResource App\Http\Resources\AppointmentPassResource
     * @apiResourceModel App\Models\AppointmentPass
     */
    public function confirm(
        AppointmentPass $appointmentPass
    ): AppointmentPassResource {
        $this->authorize('confirmTeacher', $appointmentPass);

        $appointmentPass->update([
            'confirmed_by_teacher_at' => Carbon::now(),
            'confirmed_by' => auth()->user()->id
        ]);

        event(
            new AppointmentPassConfirmed(
                $appointmentPass->id,
                $appointmentPass->school_id
            )
        );

        //        Notification::send(
        //            $appointmentPass,
        //            new StudentNotification('AppointmentPassConfirmed', [
        //                'confirmedAt' => School::convertTimeToCorrectTimezone(
        //                    $appointmentPass->confirmed_by_teacher_at,
        //                    auth()
        //                        ->user()
        //                        ->school->getTimezone()
        //                )
        //            ])
        //        );

        $appointmentPass->refreshDashboard();

        return new AppointmentPassResource($appointmentPass->refresh());
    }

    /**
     * Get appointment pass by ID.
     * @param AppointmentPass $appointmentPass
     * @return AppointmentPassResource
     * @throws AuthorizationException
     * @urlParam appointmentPass required integer The id of the appointment
     *
     * @apiResource App\Http\Resources\AppointmentPassResource
     * @apiResourceModel App\Models\AppointmentPass
     */
    public function show(AppointmentPass $appointmentPass)
    {
        $this->authorize('view', $appointmentPass);

        return new AppointmentPassResource($appointmentPass);
    }

    /**
     * Show all appointment recurrences for the current pass.
     *
     * @apiResourceCollection App\Http\Resources\AppointmentAndRecurrencesCollection
     * @return AppointmentAndRecurrencesCollection
     * @throws AuthorizationException
     * @throws Exception
     * @authenticated
     */
    public function recurrences(): AppointmentAndRecurrencesCollection
    {
        $user = auth()->user();

        $this->authorize('viewAny', RecurrenceAppointmentPass::class);

        $schoolTimezone = School::findCacheFirst(
            $user->school_id
        )->getTimezone();

        $recordsPerPage = AppointmentPass::recordsPerPage();

        $dates = request()->input('between_dates');

        $dates = explode(',', $dates);

        if ($dates === null || count($dates) <= 1) {
            $startDate = Carbon::parse(
                now()->setTimezone($schoolTimezone),
                $schoolTimezone
            )
                ->setTimezone(config('app.timezone'))
                ->startOfDay()
                ->toDateTimeString();
            $endDate = Carbon::parse(
                now()->setTimezone($schoolTimezone),
                $schoolTimezone
            )
                ->setTimezone(config('app.timezone'))
                ->endOfDay()
                ->toDateTimeString();
        } else {
            $startDate = Carbon::parse($dates[0] . ' 00:00:00', $schoolTimezone)
                ->setTimezone(config('app.timezone'))
                ->toDateTimeString();
            $endDate = Carbon::parse($dates[1] . ' 23:59:59', $schoolTimezone)
                ->setTimezone(config('app.timezone'))
                ->addYears(5)
                ->toDateTimeString();
        }

        $elasticAppointmentPass = new AppointmentPassAndRecurrences();

        if (request()->has('sort') || request()->has('search_query')) {
            $searchQuery = convertElasticQuery(
                request()->get(
                    'search_query',
                    config('scout_elastic.all_records')
                )
            );

            $sort = request()->has('sort')
                ? explode(':', request()->get('sort'))
                : explode(':', 'for_date:asc');

            $appointmentsAndRecurrences = $elasticAppointmentPass
                ->queryParams(
                    ['school_id' => $user->school_id, 'show_in_beyond' => true],
                    $searchQuery
                )
                ->whereBetween('recurrence_end_at', [$startDate, $endDate])
                ->orderBy($sort[0], $sort[1])
                ->paginate($recordsPerPage);
        } else {
            $appointmentsAndRecurrences = $elasticAppointmentPass
                ->queryParams([
                    'school_id' => $user->school_id,
                    'show_in_beyond' => true
                ])
                ->whereBetween('recurrence_end_at', [$startDate, $endDate])
                ->orderBy('is_recurrence', 'desc')
                ->paginate($recordsPerPage);
        }

        return new AppointmentAndRecurrencesCollection(
            $appointmentsAndRecurrences
        );
    }

    /**
     * Show a recurrence appointment pass
     *
     * @urlParam recurrenceAppointmentPass required integer The id of the recurrence pass.
     *
     * @apiResourceCollection App\Http\Resources\RecurrenceAppointmentPassResource
     * @apiResourceModel App\Models\RecurrenceAppointmentPass
     * @param RecurrenceAppointmentPass $recurrenceAppointmentPass
     * @return RecurrenceAppointmentPassResource
     * @authenticated
     * @throws AuthorizationException
     */
    public function recurrence(
        RecurrenceAppointmentPass $recurrenceAppointmentPass
    ) {
        $this->authorize('view', $recurrenceAppointmentPass);

        return new RecurrenceAppointmentPassResource(
            $recurrenceAppointmentPass
        );
    }

    /**
     *
     * @urlParam recurrenceAppointmentPass integer required The id of the recurrence pass
     * @bodyParam action integer required The action for the delete type. Example: 1 -> Future recurrences, 0 -> Current week and future
     *
     * @apiResourceModel App\Models\RecurrenceAppointmentPass
     * @apiResource App\Http\Resources\RecurrenceAppointmentPassResource
     *
     * @param RecurrenceAppointmentPass $recurrenceAppointmentPass
     * @param Request $request
     * @return RecurrenceAppointmentPassResource
     * @authenticated
     * @throws Exception
     */
    public function cancelRecurrence(
        RecurrenceAppointmentPass $recurrenceAppointmentPass,
        Request $request
    ): RecurrenceAppointmentPassResource {
        $user = auth()->user();
        $request->validate(['action' => 'required|min:1|max:2']);

        $this->authorize('delete', $recurrenceAppointmentPass);

        $deleteAction = $request->input('action');
        $nowAfterSevenDays = Carbon::now()
            ->addDays(7)
            ->endOfDay()
            ->format('Y-m-d H:i:s');
        $now = today()
            ->startOfDay()
            ->format('Y-m-d H:i:s');

        if ($deleteAction === 1) {
            $recurrenceAppointmentPass
                ->appointmentPasses()
                ->where(
                    'recurrence_appointment_pass_id',
                    $recurrenceAppointmentPass->id
                )
                ->whereBetween('for_date', [$now, $nowAfterSevenDays])
                ->update([
                    'canceled_at' => Carbon::now(),
                    'canceled_by' => auth()->user()->id,
                    'cancel_reason' => $request->input('cancel_reason')
                ]);
        } else {
            $recurrenceAppointmentPass
                ->appointmentPasses()
                ->where(
                    'recurrence_appointment_pass_id',
                    $recurrenceAppointmentPass->id
                )
                ->update([
                    'canceled_at' => Carbon::now(),
                    'canceled_by' => auth()->user()->id,
                    'cancel_reason' => $request->input('cancel_reason')
                ]);
        }

        $recurrenceAppointmentPass->update(['canceled_at' => now()]);

        event(
            new AppointmentPassCancelled(
                $recurrenceAppointmentPass,
                auth()->user()->id
            )
        );
        //TODO:: ADD FCM ?
        return new RecurrenceAppointmentPassResource(
            $recurrenceAppointmentPass
        );
    }

    /**
     * Delete a normal appointment pass.
     *
     * @urlParam appointmentPass integer required The id of the appointment
     *
     * @authenticated
     * @param AppointmentPass $appointmentPass
     * @return AppointmentPassResource
     * @throws AuthorizationException
     */
    public function delete(AppointmentPass $appointmentPass)
    {
        $this->authorize('delete', $appointmentPass);

        $appointmentPass->delete();

        event(new AppointmentPassCancelled($appointmentPass));
        //TODO:: ADD FCM ?
        return new AppointmentPassResource($appointmentPass);
    }

    /**
     * Update an existing recurrence appointment pass
     *
     * @apiResource App\Http\Resources\RecurrenceAppointmentPassResource
     * @apiResourceModel App\Models\RecurrenceAppointmentPass
     *
     *
     * @urlParam recurrenceAppointmentPass integer required The id of the recurrence appointment pass.
     * @bodyParam from_id integer required Current destination of the user
     * @bodyParam to_id integer required To what destination user wants to go
     * @bodyParam from_type string required Current destination type (Room or User)
     * @bodyParam to_type string required Target destination type (Room or User)
     * @bodyParam for_date date required Date for the appointment pass
     * @bodyParam period_id integer required The period that appointment is being created
     * @bodyParam reason string required The reason that appointment is being created
     * @bodyParam user_id integer required The id of the student user.
     * @bodyParam recurrence_type string The type of the recurrence: (daily, weekly, monthly)
     * @bodyParam recurrence_days json What days from the week the recurrence should be run (as json with array key "days")
     * @bodyParam recurrence_week json Monthly week number for recurrence Example: {"2": "Monday"}
     * @bodyParam recurrence_end_at date The date on which recurrence won't be executed .
     *
     * @param RecurrenceAppointmentPass $recurrenceAppointmentPass
     * @param RecAptRequest $request
     * @return RecurrenceAppointmentPassResource
     * @authenticated
     * @throws AuthorizationException
     */
    public function updateRecurrence(
        RecurrenceAppointmentPass $recurrenceAppointmentPass,
        RecAptRequest $request
    ) {
        $user = auth()->user();

        $this->authorize('update', $recurrenceAppointmentPass);

        $recurrenceAppointmentPass->update([
            'user_id' => $request->input('user_id'),
            'from_id' => $request->input('from_id'),
            'from_type' => $request->input('from_type'),
            'to_id' => $request->input('to_id'),
            'to_type' => $request->input('to_type'),
            'for_date' => $request->input('for_date'),
            'period_id' => $request->input('period_id'),
            'reason' => $request->input('reason'),
            'recurrence_type' => $request->input('recurrence_type'),
            'recurrence_end_at' => $request->input('recurrence_end_at'),
            'recurrence_week' => $request->input('recurrence_week'),
            'recurrence_days' => $request->input('recurrence_days')
        ]);

        $job = UpdateAppointmentsJob::dispatch(
            $recurrenceAppointmentPass->refresh()
        )
            ->onQueue('appointments')
            ->delay(now()->addSeconds(1));

        return new RecurrenceAppointmentPassResource(
            $recurrenceAppointmentPass
        );
    }

    /**
     * Acknowledge an appointment pass
     *
     * @urlParam appointmentPass integer required The id of the appointment pass.
     * @apiResourceModel App\Models\AppointmentPass
     * @apiResource App\Http\Resources\AppointmentPassResource
     * @authenticated
     * @param AppointmentPass $appointmentPass
     * @return AppointmentPassResource
     * @throws AuthorizationException
     */
    public function acknowledge(AppointmentPass $appointmentPass)
    {
        $user = auth()->user();

        $this->authorize('acknowledgeByTeacher', $appointmentPass);

        $appointmentPass->update([
            'acknowledged_by_teacher_at' => Carbon::now(),
            'acknowledged_by' => $user->id,
            'from_id' => $appointmentPass->from_id
                ? $appointmentPass->from_id
                : $user->id,
            'from_type' => $appointmentPass->from_id
                ? $appointmentPass->from_type
                : User::class
        ]);

        //        Notification::send(
        //            $appointmentPass,
        //            new StudentNotification('AppointmentPassAcknowledged', [
        //                'acknowledgedAt' => School::convertTimeToCorrectTimezone(
        //                    $appointmentPass->for_date,
        //                    auth()
        //                        ->user()
        //                        ->school->getTimezone()
        //                )
        //            ])
        //        );

        return new AppointmentPassResource($appointmentPass->refresh());
    }
}
