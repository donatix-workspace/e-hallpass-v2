<?php

namespace App\Http\Requests;

use App\Rules\CheckIfCommentTypeExistRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CommentRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'comment' => 'string|required|max:255',
            'commentable_type' => ['required', 'string', new CheckIfCommentTypeExistRule()],
            'permission' => ['boolean', Rule::requiredIf(!optional(auth()->user())->isStudent())]
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }
}
