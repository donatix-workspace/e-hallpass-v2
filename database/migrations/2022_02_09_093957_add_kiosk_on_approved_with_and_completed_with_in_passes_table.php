<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKioskOnApprovedWithAndCompletedWithInPassesTable extends Migration
{
    public function up()
    {
        Schema::table('passes', function (Blueprint $table) {
            $byAutoPass = \App\Models\Pass::BY_AUTOPASS;
            $byCheckIn = \App\Models\Pass::BY_CHECKIN;
            $byKiosk = \App\Models\Pass::BY_KIOSK;

            DB::statement(
                "ALTER TABLE `passes` MODIFY COLUMN `approved_with` ENUM('$byAutoPass','$byCheckIn','$byKiosk');"
            );

            DB::statement(
                "ALTER TABLE `passes` MODIFY COLUMN `ended_with` ENUM('$byAutoPass','$byCheckIn','$byKiosk');"
            );
        });
    }

    public function down()
    {
        Schema::table('passes', function (Blueprint $table) {
            //
        });
    }
}
