<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserFavoriteBulkRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'section' => ['required'],
            'global' => ['sometimes', 'boolean'],
            'favorites' => ['array', 'present'],
            'favorites.*.id' => ['required', 'integer'],
            'favorites.*.type' => ['required'],
            'favorites.*.position' => ['required'],
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }
}
