<?php

namespace Database\Factories;

use App\Models\RoomRestriction;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoomRestrictionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RoomRestriction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'from_date' => $this->faker->dateTimeBetween('now', 'now'),
            'to_date' => $this->faker->dateTimeBetween('+1 day', '+1 day')
        ];
    }
}
