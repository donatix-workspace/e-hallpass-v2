<?php

namespace App\Http\Services\CommonUserService;

use App\Models\User;
use Exception;
use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use http\Env\Response;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Session;
use Str;

class EntityService
{

    protected $client;
    protected $headers;

    public function __construct()
    {
        $baseUri = config('commonuserservice.base_url');

        if ($baseUri !== '') {
            $this->client = new Client([
                'debug' => false,
                'base_uri' => $baseUri,
                'allow_redirects' => true,
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'auth' => [
                    config('commonuserservice.basic_auth_user'),
                    config('commonuserservice.basic_auth_password')
                ],
            ]);
        } else {
            throw new Exception('Please, define CUS_BASE_URL in the .env file');
        }

        $this->headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];

    }

    public function call($action = 'GET', $url = '')
    {
        $response = $this->client->request($action, $url);

        return [
            'data' => json_decode($response->getBody(), true),
            'pagination' => [
                'total' => $response->getHeaderLine('X-Pagination-Total-Count'),
                'per_page' => 50,
                'pages' => $response->getHeaderLine('X-Pagination-Total-Count') / 50,
            ],
        ];
    }

    public function listSchoolsAfterDate($date, $page = 1, $perPage = 100)
    {
        $response = [];

        $createdAfter = $this->client->request('GET', "/common/school?pageSize={$perPage}&page={$page}&createdAfter={$date}");
        $response = json_decode($createdAfter->getBody(), true);
        $modifiedAfter = $this->client->request('GET', "/common/school?pageSize={$perPage}&page={$page}&modifiedAfter={$date}");
        $response = array_merge($response, json_decode($modifiedAfter->getBody(), true));
        return $response;
    }

    public function getUsersCountAfterDate($date, $type = 'createdAfter')
    {
        $initialCall = $this->client->request('GET', "/common/authorization/membership?pageSize=1&{$type}={$date}");
        return (int)$initialCall->getHeaderLine('X-Pagination-Total-Count');
    }

    public function getSchoolsCountAfterDate($date, $type = 'createdAfter')
    {
        $initialCall = $this->client->request('GET', "/common/school?pageSize=1&{$type}={$date}");
        return (int)$initialCall->getHeaderLine('X-Pagination-Total-Count');
    }


    public function listUsersAfterDate($date, $page = 1, $perPage = 100)
    {
        $response = [];

        $createdAfter = $this->client->request('GET', "/common/authorization/membership?pageSize={$perPage}&includes=user,school.uuid&page={$page}&createdAfter={$date}");
        $response = json_decode($createdAfter->getBody(), true) ?? [];
        $modifiedAfter = $this->client->request('GET', "/common/authorization/membership?pageSize={$perPage}&includes=user,school.uuid&page={$page}&modifiedAfter={$date}");
        $response = array_merge($response, json_decode($modifiedAfter->getBody(), true) ?? []);

        return $response;
    }

    public function listDistricts()
    {
        //https://ftm2dev-common-k8s.flextimemanager.com/common/district?page=1&pageSize=100000&filter=NY&status=1
        return $this->call('GET', '/common/district?pageSize=');
    }

    public function listTimeZones($filter)
    {
//        return $this->call('GET', "/common/utc-offset/time-zone&filter={$filter}");
        try {
            $response = $this->client->request('GET', "/common/utc-offset/time-zone?page=1&pageSize=100000&filter={$filter}" );
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dd($responseBodyAsString);
//            logger('POST CALL ERROR', [$responseBodyAsString]);
            return false;
        }
        return json_decode($response->getBody(), 1);
    }


    public function listSchools($page = 1)
    {
        return $this->call('GET', '/common/school?orderBy=-created&pageSize=50&page=' . $page);

    }

    public function getSchool($schoolUuid)
    {
        try {
            $response = $this->client->request('GET', '/common/district/school/' . $schoolUuid);
        } catch (ClientException $e) {
            return false;
        }
        return json_decode($response->getBody(), 1);
    }

    public function createSchool($districtUuid, $body)
    {
        $data['body'] = json_encode($body);
        try {
            $response = $this->client->request('POST', '/common/district/' . $districtUuid, $data);
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
//            logger('POST CALL ERROR', [$responseBodyAsString]);
            return false;
        }
        return json_decode($response->getBody(), 1);
    }


    public function listUsers($schoolUuid, $page = 1)
    {
        return $this->call('GET', "/common/authorization/membership?schoolUuid={$schoolUuid}&includes=user&pageSize=50&page={$page}");
    }

    public function listMemberships($page = 1, $perPage = 1000)
    {
        return $this->call('GET', "/common/authorization/membership?includes=school.uuid&page={$page}&pageSize={$perPage}");
    }

    public function getTotalMemberships()
    {
        $initialCall = $this->client->request('GET', "/common/authorization/membership?includes=school.uuid&pageSize=1");
        return (int)$initialCall->getHeaderLine('X-Pagination-Total-Count');
    }

    public function getUser($userUuid)
    {
        $response = $this->client->request('GET', '/common/authentication/user/' . $userUuid);
        return json_decode($response->getBody(), 1);

    }

}
