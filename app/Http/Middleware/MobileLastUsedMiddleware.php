<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Agent\Agent;

class MobileLastUsedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $agent = new Agent();
        $user = auth()->user();

        if ($agent->isMobile() && $user) {
            $user->update([
                'mobile_last_used_at' => null
            ]);
        }

        return $next($request);
    }
}
