<?php

namespace App\Http\Controllers\Users;

use App\Elastic\Rules\BelongsToSchoolSearchRule;
use App\Http\Controllers\Controller;
use App\Http\Filters\UserFilter;
use App\Http\Requests\ImportKioskPasswordCSV;
use App\Http\Requests\KioskFinalImportRequest;
use App\Http\Requests\KioskImportEditRequest;
use App\Http\Requests\UpdateImportedUserRequest;
use App\Http\Requests\UserImportRequest;
use App\Http\Resources\FileUploadsResource;
use App\Http\Resources\UsersImportCollection;
use App\Http\Services\CommonUserService\CommonUserService;
use App\Imports\KioskPasswordsImport;
use App\Imports\UsersImport;
use App\Jobs\InsertUsersJob;
use App\Imports\UserExports;
use App\Models\AuthType;
use App\Models\Kiosk;
use App\Models\KioskUser;
use App\Models\School;
use App\Models\User;
use App\Models\UserImport;
use Excel;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Log;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ImportController extends Controller
{
    /**
     * Import user by csv file
     *
     * @bodyParam csv_file file required The csv file for importing the users.
     *
     * @authenticated
     * @param UserImportRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function upload(UserImportRequest $request): JsonResponse
    {
        $user = auth()->user();

        $this->authorize('import', User::class);

        $school = School::where('id', $user->school_id)->first();
        $path = $school->sftp_directory;

        $directory = config('filesystems.disks.mounted.root') . '/' . $path;
        if (!file_exists($directory)) {
            mkdir($directory);
        }
        $fileName = $request->file('csv_file')->getClientOriginalName();
        if ($school->sftp_file_prefix) {
            $fileName = trim($school->sftp_file_prefix, '_') . '_' . $fileName;
        }
        $path = $path . '/' . $fileName;

        Storage::disk('mounted')->put(
            $path,
            file_get_contents($request->file('csv_file'))
        );

        //        $request->file('csv_file')->storeAs('csv', $usersImportCsvFileName);
        //
        //        $import = new UsersImport($user->school_id, $usersImportCsvFileName);
        //        $import->import(storage_path('app/csv/' . $usersImportCsvFileName));
        //        UserImport::importFailed(
        //            $import->failures(),
        //            $user->school_id,
        //            $usersImportCsvFileName
        //        );

        return response()->json([
            'data' => [
                'file_name' => $path
            ],
            'status' => __('success')
        ]);
    }

    /**
     * Get inserted file information.
     *
     * @urlParam fileName string The name of the file.
     * @queryParam per_page integer How many records will be shown per page.
     *
     * @authenticated
     * @param Request $request
     * @return UsersImportCollection
     * @throws AuthorizationException
     * @apiResourceCollection App\Http\Resources\UsersImportCollection
     * @apiResourceModel App\Models\UserImport
     */
    public function index(Request $request)
    {
        $user = auth()->user();

        $this->authorize('import', User::class);

        //Get All records from specific file that are not already inserted
        $toBeImportedUsers = DB::table('users_imports')
            ->where('file_name', $request->fileName)
            ->where('inserted', 0);

        $existingUsersMails = Db::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->selectRaw('u.email as email')
            ->where('m.school_id', $user->school_id)
            ->whereIn('u.email', $toBeImportedUsers->get()->pluck('email'))
            ->get()
            ->pluck('email');

        //Mark existing records
        if ($existingUsersMails->count()) {
            UserImport::where('school_id', $user->school_id)
                ->whereIn('email', $existingUsersMails)
                ->update([
                    'exist' => true
                ]);
        }

        //Get all already created users for the specific school and arrange the data in users_imports format
        $allUsers = DB::table('users')
            ->select([
                'id',
                'first_name',
                'email',
                'last_name',
                'status',
                'role_id',
                'school_id',
                'grade_year',
                'created_at',
                'updated_at',
                DB::raw('1 as inserted'),
                DB::raw('null as file_name'),
                'is_substitute',
                DB::raw('1 as exist'),
                DB::raw('0 as is_failed'),
                DB::raw('null as errors')
            ])
            ->where('school_id', $user->school_id)
            ->where(function ($query) {
                $query
                    ->where('auth_type', '<>', AuthType::HIDDEN)
                    ->orWhereNull('auth_type');
            })
            ->whereNotIn('email', $toBeImportedUsers->get()->pluck('email'));

        return new UsersImportCollection(
            $allUsers->get()->merge($toBeImportedUsers->get())
        );
    }

    /**
     * Importing users from csv final step.
     *
     * @urlParam fileName string required The name of the file.
     *
     * @authenticated
     * @param UpdateImportedUserRequest $updateUserRequest
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function importFinal(
        UpdateImportedUserRequest $updateUserRequest
    ): JsonResponse {
        $this->authorize('import', User::class);

        $updateUserRequest['authenticatedUser'] = auth()->user();
        $updateUserRequest['school'] = School::find(auth()->user()->school_id);

        InsertUsersJob::dispatch($updateUserRequest);

        return response()->json([
            'data' => [],
            'message' => __('users.imported.successfully'),
            'status' => __('success')
        ]);
    }

    /**
     * Update user import record
     *
     * @authenticated
     * @param UserImport $userImport
     * @param UpdateImportedUserRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function update(
        UserImport $userImport,
        UpdateImportedUserRequest $request
    ): JsonResponse {
        $this->authorize('import', User::class);

        $userImport->update([
            'role_id' => $request->input('role_id'),
            'status' => $request->input('status'),
            'email' => $request->input('email'),
            'grade_year' => $request->input('grade_year'),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name')
        ]);

        return response()->json([
            'data' => $userImport,
            'status' => __('success')
        ]);
    }

    /**
     * @param $filters
     * @return User
     */
    public function getMainUsersCsvQuery($filters)
    {
        if (request()->has('sort') || request()->has('search_query')) {
            $usersEsQueryIds = User::search(
                request()->input('search_query') === null
                    ? '__all'
                    : convertElasticQuery(request()->input('search_query'))
            )
                ->rule(BelongsToSchoolSearchRule::class)
                ->query(function ($query) {
                    $query
                        ->withoutGlobalScopes()
                        ->skipHidden()
                        ->skipSystem();
                })
                ->with(['kioskUsers', 'role', 'unArchive'])
                ->when(request()->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('id', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                    return $query->orderBy('id', 'asc');
                })
                //                ->when(request()->has('archived'), function ($query) {
                //                    $query->where(
                //                        'status',
                //                        filter_var(
                //                            request()->get('archived'),
                //                            FILTER_VALIDATE_BOOLEAN
                //                        )
                //                            ? User::INACTIVE
                //                            : User::ACTIVE
                //                    );
                //                })
                ->where('is_substitute', User::INACTIVE)
                ->take(User::recordsPerPage())
                ->get()
                ->pluck('id')
                ->toArray();

            $idsImploded = implode(',', $usersEsQueryIds);

            $usersQuery = User::withoutGlobalScopes()
                ->whereIntegerInRaw('id', $usersEsQueryIds)
                ->orderByRaw("field(id,$idsImploded)")
                ->where('school_id', auth()->user()->school_id);
        } else {
            $usersQuery = User::onWriteConnection()
                ->with(['kioskUsers', 'role', 'unArchive'])
                ->withoutGlobalScopes()
                ->belongsToThisSchool()
                ->skipHidden()
                ->where('deleted_at', null)
                ->orderBy('created_at', 'desc')
                ->where('is_substitute', User::INACTIVE)
                ->filter($filters);
        }

        return $usersQuery;
    }

    /**
     * @param $filters
     * @return User|\Illuminate\Database\Eloquent\Builder
     */
    public function getSubstituteCsvQuery()
    {
        if (request()->has('sort') || request()->has('search_query')) {
            $usersEsQueryIds = User::search(
                request()->input('search_query') === null
                    ? config('scout_elastic.all_records')
                    : convertElasticQuery(request()->input('search_query'))
            )
                ->query(function ($query) {
                    $query->withoutGlobalScopes();
                })
                ->with('pin')
                ->when(request()->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('id', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                    return $query->orderBy('id', 'asc');
                })
                ->where('role_id', config('roles.teacher'))
                ->where('school_id', auth()->user()->school_id)
                ->where('is_substitute', User::ACTIVE)
                ->take(User::recordsPerPage())
                ->get()
                ->pluck('id')
                ->toArray();

            $idsImploded = implode(',', $usersEsQueryIds);

            $usersQuery = User::withoutGlobalScopes()
                ->whereIntegerInRaw('id', $usersEsQueryIds)
                ->orderByRaw("field(id,$idsImploded)")
                ->where('school_id', auth()->user()->school_id);
        } else {
            $usersQuery = User::where(
                'school_id',
                '=',
                auth()->user()->school_id
            )
                ->with('pin')
                ->where('role_id', config('roles.teacher'))
                ->where('is_substitute', User::ACTIVE)
                ->orderBy('created_at', 'DESC');
        }

        return $usersQuery;
    }

    /**
     * Import users data as csv
     *
     * @authenticated
     * @return \Illuminate\Http\JsonResponse|BinaryFileResponse
     */
    public function export(UserFilter $filters)
    {
        $substitute = filter_var(
            request()->input('substitute'),
            FILTER_VALIDATE_BOOLEAN
        );
        $archived = filter_var(
            request()->input('archived'),
            FILTER_VALIDATE_BOOLEAN
        );

        if (!$substitute) {
            $usersQuery = $this->getMainUsersCsvQuery($filters);
        } else {
            $usersQuery = $this->getSubstituteCsvQuery();
        }

        try {
            return Excel::download(
                new UserExports($substitute, $archived, $usersQuery),
                'users.csv'
            );
        } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
            Log::error($e);
            return response()->json([
                'data' => [],
                'status' => __('fail'),
                'message' => __('something.went.wrong')
            ]);
        }
    }

    /**
     * Import password by CSV
     *
     * @bodyParam csv_file file required The file with CSV passwords.
     *
     * @param ImportKioskPasswordCSV $request
     * @return JsonResponse
     */
    public function kioskPassword(ImportKioskPasswordCSV $request): JsonResponse
    {
        $user = auth()->user();

        $kioskPasswordsCsvFile =
            time() .
            '-' .
            $user->id .
            '-' .
            $user->school_id .
            '-kiosk-pw-import' .
            '.csv';

        $request
            ->file('csv_file')
            ->storeAs(storage_path('/app/csv'), $kioskPasswordsCsvFile);

        Excel::import(
            new KioskPasswordsImport($kioskPasswordsCsvFile),
            storage_path('app/csv/' . $kioskPasswordsCsvFile)
        );

        $kioskUsersAfterCsvImport = DB::table('kiosk_imports')
            ->where('file_name', $kioskPasswordsCsvFile)
            ->where('school_id', $user->school_id)
            ->paginate(
                $request->get(
                    'per_page',
                    config('pagination.default_pagination')
                )
            );

        return response()->json([
            'file_name' => $kioskPasswordsCsvFile,
            'data' => $kioskUsersAfterCsvImport,
            'status' => __('success')
        ]);
    }

    /**
     * Import password by CSV
     *
     * @urlParam kioskImportId The id of the imported record
     *
     * @param KioskImportEditRequest $request
     * @param $kioskImportId
     * @return JsonResponse
     */
    public function kioskImportUpdate(
        KioskImportEditRequest $request,
        $kioskImportId
    ): JsonResponse {
        $user = auth()->user();

        $kioskImportRecord = DB::table('kiosk_imports')
            ->where('id', $kioskImportId)
            ->where('school_id', $user->school_id);

        if ($kioskImportRecord->exists()) {
            $kioskImportRecord->update([
                'kiosk_password' => $request->input('kiosk_password')
            ]);

            return response()->json([
                'data' => $kioskImportRecord->first(),
                'status' => __('success')
            ]);
        }

        return response()->json(
            [
                'data' => ['id' => $kioskImportId],
                'status' => __('Record not found')
            ],
            404
        );
    }

    /**
     * Import password by CSV
     *
     * @urlParam fileName The id of the imported record
     * @bodyParam user_passwords_and_emails array required An array with emails and kiosk passwords
     *
     * @param KioskFinalImportRequest $request
     * @param $fileName
     * @return JsonResponse
     */
    public function kioskFinal(
        KioskFinalImportRequest $request,
        $fileName
    ): JsonResponse {
        $user = auth()->user();

        $kioskImportData = $request->input('user_passwords_and_emails');

        foreach ($kioskImportData as $kioskImportItem) {
            $userId = User::select('id')
                ->where('school_id', $user->school_id)
                ->where('email', $kioskImportItem['email'])
                ->firstOrFail();

            $existsInTempKioskImportTable = DB::table('kiosk_imports')
                ->where('school_id', $user->school_id)
                ->where('email', $kioskImportItem['email'])
                ->exists();

            if ($existsInTempKioskImportTable) {
                KioskUser::updateOrCreate(
                    [
                        'user_id' => $userId->id,
                        'school_id' => $user->school_id
                    ],
                    [
                        'kpassword' => bcrypt(
                            $kioskImportItem['kiosk_password']
                        ),
                        'password_processed' => Kiosk::ACTIVE
                    ]
                );
            }
        }

        DB::table('kiosk_imports')
            ->where('file_name', $fileName)
            ->where('school_id', $user->school_id)
            ->delete();

        return response()->json([
            'data' => ['file_name' => $fileName],
            'status' => __(
                'Total :recordsCount Record(s) are uploaded. Your file has been received and is currently in the queue being processed.',
                [
                    'recordsCount' => count(
                        $request->input('user_passwords_and_emails.*.email')
                    )
                ]
            )
        ]);
    }

    public function files(School $school, Request $request)
    {
        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 25);

        $client = new CommonUserService(auth()->user());

        try {
            $response = $client->files($school->cus_uuid, 0, $page, $perPage);
        } catch (\Exception $e) {
            $response = [];
        }

        return response()->json(
            [
                'data' => FileUploadsResource::collection($response),
                'meta' => [
                    'page' => $page,
                    'perPage' => $perPage
                ]
            ],
            200
        );
    }
}
