<?php

namespace App\Http\Filters;

use App\Models\User;
use Illuminate\Http\Request;

class UserFilter extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    //    /**
    //     * @param string $sort
    //     * @return mixed
    //     */
    //    public function sort(string $sort = "id:asc")
    //    {
    //        $sortExploding = explode(":", $sort);
    //
    //        if (count($sortExploding) <= 1) {
    //            return $this->builder;
    //        }
    //
    //        $sortingKeywords = ['asc', 'desc'];
    //
    //        if (in_array($sortExploding[1], $sortingKeywords)) {
    //            return $this->builder->orderBy($sortExploding[0], $sortExploding[1]);
    //        }
    //        return $this->builder;
    //    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder|mixed
     */
    public function archived($status)
    {
        /**
         * $status = 1 ; // Get only the archived users
         * $status = 0 ; // Get all users with valid membership
         */
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $this->builder->where(function ($query) use ($status) {
            $query
                ->when(!$status, function ($query) {
                    $query->whereHasIn('schoolUser', function ($query) {
                        $query
                            ->whereNull('schools_users.deleted_at')
                            ->where(
                                'schools_users.school_id',
                                auth()->user()->school_id
                            )
                            ->where('schools_users.status', User::ACTIVE)
                            ->whereNull('schools_users.archived_at');
                    });
                })
                ->when($status, function ($query) {
                    $query->whereHasIn('schoolUser', function ($query) {
                        $query
                            ->whereNull('schools_users.deleted_at')
                            ->where(
                                'schools_users.school_id',
                                auth()->user()->school_id
                            )
                            ->whereNotNull('schools_users.archived_at');
                    });
                });
        });
    }

    //    /**
    //     * @param $status
    //     * @return \Illuminate\Database\Query\Builder|mixed
    //     */
    //    public function is_substitute($status = false)
    //    {
    //        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);
    //
    //        return $this->builder->where(function ($query) use ($status) {
    //            $query->where('is_substitute', $status ? 1 : 0);
    //        });
    //    }
}
