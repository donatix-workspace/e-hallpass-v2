<?php

namespace Database\Seeders;

use App\Models\RoomType;
use App\Models\School;
use Illuminate\Database\Seeder;

class RoomTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $randomSchool = School::inRandomOrder()->first();
        $roomTypes = RoomType::factory()
            ->count(20)
            ->create(['school_id' => $randomSchool->id]);
    }
}
