<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AutoPassPreferenceRequest extends FormRequest
{
    public function rules()
    {
        return [
            'rooms_and_modes' => 'array',
            'rooms_and_modes.*.room_id' => ['integer', Rule::exists('rooms', 'id')->where(function ($query) {
                $query->where('school_id', optional(auth()->user())->school_id);
            }), Rule::exists('auto_passes', 'room_id')->where('school_id', optional(auth()->user())->school_id)],
            'rooms_and_modes.*.mode' => 'string|required|in:start_only,start_and_stop',
            'limit' => 'sometimes|integer|min:-1|nullable'
        ];
    }

    public function authorize()
    {
        return auth()->check() && !auth()->user()->isStudent();
    }
}
