<?php

namespace App\Http\Controllers\Admin\Passblock;

use App\Http\Controllers\Controller;
use App\Http\Filters\RestrictionFilter;
use App\Http\Requests\DeleteRestrictionBulkRequest;
use App\Http\Requests\RoomRestrictionRequest;
use App\Http\Resources\RoomRestrictionCollection;
use App\Http\Resources\RoomRestrictionResource;
use App\Jobs\RestrictionCsvCreateJob;
use App\Jobs\RestrictionStudentsCreateJob;
use App\Models\PassLimit;
use App\Models\RoomRestriction;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Maatwebsite\Excel\Facades\Excel;

/**
 * @group Restrictions room manage
 */
class RestrictionController extends Controller
{
    /**
     * Show all restrictions.
     *
     *
     * @apiResourceCollection App\Http\Resources\RoomRestrictionCollection
     * @apiResourceModel App\Models\RoomRestriction
     * @authenticated
     * @return RoomRestrictionCollection
     * @throws AuthorizationException
     */
    public function index(RestrictionFilter $filters)
    {
        $user = auth()->user();

        $this->authorize('viewAny', RoomRestriction::class);

        if (request()->has('search_query') || request()->has('sort')) {
            $roomRestriction = collect(RoomRestriction::search(request()->get('search_query') === null ? config('scout_elastic.all_records') : convertElasticQuery(request()->get('search_query')))
                ->where('school_id', $user->school_id)
                ->when(request()->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('id', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                    return $query->orderBy('id', 'asc');
                })
                ->take(RoomRestriction::recordsPerPage())
                ->get())
                ->pluck('id')
                ->toArray();

            $roomRestrictionImploded = implode(',', $roomRestriction);

            $roomRestriction = RoomRestriction::whereIntegerInRaw('id', $roomRestriction)
                ->orderByRaw("field(id,$roomRestrictionImploded)")
                ->fromSchoolId($user->school_id)
                ->filter($filters)
                ->paginate(RoomRestriction::recordsPerPage());
        } else {
            $roomRestriction = RoomRestriction::with(['user:id,first_name,last_name', 'room:id,name'])
                ->orderBy('from_date', 'desc')
                ->fromSchoolId($user->school_id)
                ->filter($filters)
                ->paginate(RoomRestriction::recordsPerPage());
        }

        return new RoomRestrictionCollection($roomRestriction);
    }

    /**
     * Create a RoomRestriction record.
     *
     * @response status=201 scenario="Successful creation." {
     *     "data": [],
     *     "status": "success"
     * }
     *
     * @bodyParam limit_for string required Type of the limitation Example: student, csv, gradeyear
     * @bodyParam csv_file file The csv file with emails (required only when limit_for input is csv).
     * @bodyParam grade_years json The json values of all grade years that will be limited (required if limit_for input is gradeyear)
     * @bodyParam selected_student_ids json The json values of all students ids that will be limited (required if limit_for input is students)
     * @bodyParam from_date date required From what date the limitation will be valid.
     * @bodyParam to_date date required To what date the limitation will longer be valid.
     * @bodyParam type string required The limitation type Example: ('access_denied','members_only')
     * @authenticated
     *
     * @param RoomRestrictionRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(RoomRestrictionRequest $request): JsonResponse
    {
        $user = auth()->user();
        $this->authorize('create', RoomRestriction::class);
        $limitFor = $request->input('limit_for');
        $roomId = $request->input('room_id');

        $roomRestrictionRoomExistType = RoomRestriction::whereRoomId($roomId)
            ->where('from_date', '=', Carbon::parse($request->input('from_date'))->toDateString())
            ->where('to_date', '=', Carbon::parse($request->input('to_date'))->toDateString())
            ->where('status', RoomRestriction::ACTIVE)
            ->fromSchoolId($user->school_id)
            ->first();

        $type = $roomRestrictionRoomExistType ? $roomRestrictionRoomExistType->type : $request->input('type');

        if ($limitFor === RoomRestriction::GRADEYEAR) {
            $gradeYears = collect(json_decode($request->input('grade_years'), true));
            $roomRestriction = RoomRestriction::where('room_id', $roomId)
                ->fromSchoolId($user->school_id)
                ->where('type', $type)
                ->where('grade_year', '!=', null)
                ->where(function ($query) use ($request) {
                    $query->whereBetween('from_date', [
                        Carbon::parse($request->input('from_date')),
                        Carbon::parse($request->input('to_date'))
                    ])
                        ->orWhereBetween('to_date', [
                            Carbon::parse($request->input('from_date')),
                            Carbon::parse($request->input('to_date'))
                        ]);
                })
                ->first();

            if ($roomRestriction !== null) {
                RoomRestriction::where('id', $roomRestriction->id)->update([
                    'grade_year' => $gradeYears->toJson(),
                    'from_date' => $request->input('from_date'),
                    'to_date' => $request->input('to_date')
                ]);
            }

            if ($roomRestriction === null) {
                RoomRestriction::create([
                    'room_id' => $roomId,
                    'school_id' => $user->school_id,
                    'from_date' => $request->input('from_date'),
                    'to_date' => $request->input('to_date'),
                    'grade_year' => $gradeYears->toJson(),
                    'type' => $type
                ]);
            }
        }

        if ($limitFor === RoomRestriction::STUDENT) {
            $selectedStudentIds = json_decode($request->input('selected_student_ids'));
            $fromDate = $request->input('from_date');
            $toDate = $request->input('to_date');

            RestrictionStudentsCreateJob::dispatchSync($fromDate, $toDate, $roomId, $type, $user, $selectedStudentIds);
        }

        if ($limitFor === RoomRestriction::CSV) {
            $csvFile = $request->file('csv_file');
            $fromDate = $request->input('from_date');
            $toDate = $request->input('to_date');
            $storageTempPath = storage_path('/tmp/');
            $csvFileName = time() . $csvFile->getClientOriginalName();
            $storageCsvFile = $csvFile->move($storageTempPath, $csvFileName)->getRealPath();

            RestrictionCsvCreateJob::dispatchSync($storageCsvFile, $roomId, $fromDate, $toDate, $user, $type);
        }


        return response()->json([
            'data' => [],
            'status' => __('success'),
        ], 201);
    }

    /**
     * Disable the room restriction
     *
     * @urlParam roomRestriction integer required The id of the room restriction record.
     * @apiResourceModel App\Models\RoomRestriction
     * @apiResource App\Http\Resources\RoomRestrictionResource
     *
     * @authenticated
     * @param RoomRestriction $roomRestriction
     * @return RoomRestrictionResource
     * @throws AuthorizationException
     */
    public function disable(RoomRestriction $roomRestriction)
    {
        $this->authorize('update', $roomRestriction);

        $roomRestriction->update([
            'status' => 0
        ]);

        return new RoomRestrictionResource($roomRestriction);
    }

    /**
     * Edit the status of the room restriction
     *
     * @urlParam roomRestriction integer required The id of the room restriction record.
     * @apiResourceModel App\Models\RoomRestriction
     * @apiResource App\Http\Resources\RoomRestrictionResource
     *
     * @authenticated
     * @param RoomRestriction $roomRestriction
     * @return RoomRestrictionResource
     * @throws AuthorizationException
     */
    public function status(RoomRestriction $roomRestriction): RoomRestrictionResource
    {
        $this->authorize('update', $roomRestriction);

        $roomRestriction->update([
            'status' => !$roomRestriction->status
        ]);

        return new RoomRestrictionResource($roomRestriction);
    }

    /**
     * Delete a room restriction.
     *
     * @urlParam roomRestriction integer required The id of the room restriction record.
     * @apiResourceModel App\Models\RoomRestriction
     * @apiResource App\Http\Resources\RoomRestrictionResource
     *
     * @authenticated
     * @param RoomRestriction $roomRestriction
     * @return RoomRestrictionResource
     * @throws AuthorizationException
     */
    public function delete(RoomRestriction $roomRestriction)
    {
        $this->authorize('delete', $roomRestriction);

        $roomRestriction->delete();

        return new RoomRestrictionResource($roomRestriction);
    }

    /**
     * Delete bulk room restrictions.
     *
     * @bodyParam room_restriction_ids array required The ids of room restriction
     *
     *
     * @authenticated
     * @param DeleteRestrictionBulkRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function bulkDelete(DeleteRestrictionBulkRequest $request): JsonResponse
    {
        $user = auth()->user();

        $this->authorize('bulkDelete', RoomRestriction::class);

        $roomRestrictionDelete = RoomRestriction::whereIntegerInRaw('id', $request->input('room_restriction_ids'))
            ->where('school_id', $user->school_id)
            ->delete();

        return response()->json([
            'data' => [],
            'status' => __('success'),
            'message' => __('Room restriction deleted successfully.')
        ]);
    }
}
