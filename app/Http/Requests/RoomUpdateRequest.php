<?php

namespace App\Http\Requests;

use App\Rules\ExtendedPassTimeRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoomUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'name' => ['required', 'string', Rule::unique('rooms', 'name')
//                ->where('school_id', optional(auth()->user())->school_id)
//                ->ignore($this->id)
//            ],
            'name' => ['required', 'string'],
            'trip_type' => 'required|string|in:Layover,One Way,Roundtrip',
            'status' => 'required|boolean',
            'is_extended' => 'sometimes|boolean',
            'extended_pass_time' => ['required_if:is_extended,true', 'date_format:"H:i:s"', 'nullable', new ExtendedPassTimeRule()],
            'comment_type' => 'sometimes|string|in:Mandatory,Optional,Hidden',
            'icon' => 'sometimes',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'extended_pass_time.required_if' => 'The extended pass time field is required',
            'name.unique' => 'This room name already exists',
        ];
    }
}
