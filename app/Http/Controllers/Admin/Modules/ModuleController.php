<?php

namespace App\Http\Controllers\Admin\Modules;

use App\Events\ModuleActivated;
use App\Events\ModuleChanged;
use App\Http\Controllers\Controller;
use App\Http\Requests\ModuleRequest;
use App\Http\Resources\ModuleResource;
use App\Http\Resources\ModulesCollection;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\DB;

/**
 * @group Modules management.
 * API's for managing the Modules
 */
class ModuleController extends Controller
{
    /**
     * Get all school modules.
     *
     * @authenticated
     * @return ModulesCollection
     * @apiResourceModel App\Models\Module
     * @throws AuthorizationException
     */
    public function index()
    {
        $user = auth()->user();

        $this->authorize('viewAny', Module::class);

        $modules = Module::all();

        return new ModulesCollection($modules);
    }

    /**
     * Get active school modules
     *
     * @authenticated
     * @return ModulesCollection
     * @apiResourceModel App\Models\Module
     * @throws AuthorizationException
     */
    public function active()
    {
        $user = auth()->user();
        $this->authorize('viewActive', Module::class);

        $schoolActiveModules = SchoolModule::where('school_id', $user->school_id)
            ->get()
            ->append(['display_name', 'name']);

        return new ModulesCollection($schoolActiveModules);
    }

    /**
     * Update active school modules options.
     *
     * @urlParam school integer required The id of the school
     * @urlParam module integer required The id of the module.
     * @bodyParam option_json json required The options as json format
     *
     *
     * @authenticated
     * @param ModuleRequest $request
     * @param Module $module
     * @return ModuleResource
     * @throws AuthorizationException
     * @apiResource App\Http\Resources\ModuleResource
     * @apiResourceModel App\Models\Module
     */
    public function update(ModuleRequest $request, Module $module): ModuleResource
    {
        $user = auth()->user();

        $this->authorize('update', [$module, $user->school]);

        $schoolModuleUpdate = SchoolModule::whereModuleId($module->id)
            ->fromSchoolId($user->school_id)
            ->update([
                'option_json' => $request->input('option_json'),
                'status' => Module::ACTIVE,
            ]);

        $schoolActiveModules = $user->school->modules()
            ->withPivot('permission', 'option_json', 'status')
            ->get();

        event(new ModuleChanged($schoolActiveModules, $user->school_id));
        return new ModuleResource($module->refresh());
    }

    /**
     * Update module status
     *
     * @urlParam module integer required The id of the module.
     *
     * @authenticated
     * @param Module $module
     * @return \Illuminate\Http\JsonResponse
     * @throws AuthorizationException
     * @apiResource App\Http\Resources\ModuleResource
     * @apiResourceModel App\Models\Module
     */
    public function status(Module $module): \Illuminate\Http\JsonResponse
    {
        $user = auth()->user();

        $this->authorize('update', [$module, $user->school]);

        $schoolModuleActiveUpdate = SchoolModule::whereModuleId($module->id)
            ->fromSchoolId($user->school_id)
            ->first();

        $schoolModuleActiveUpdate
            ->where('module_id', $module->id)
            ->where('school_id', $user->school_id)
            ->update([
                'status' => !$schoolModuleActiveUpdate->status
            ]);

        $schoolActiveModules = $user->school->modules()
            ->withPivot('permission', 'option_json', 'status')
            ->get();
        event(new ModuleChanged($schoolActiveModules, $user->school_id));

        return response()->json([
            'data' => $schoolModuleActiveUpdate,
            'status' => __('success'),
            'message' => __('success')
        ]);
    }

    public function addSchool()
    {
        $user = auth()->user();
        //TODO: add school modules
    }
}
