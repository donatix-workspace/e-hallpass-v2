<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_favorites', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('position')->nullable();
            $table->morphs('favorable');
            $table->bigInteger('user_id')->unsigned()->nullable()->index('user_id');
            $table->bigInteger('school_id')->unsigned()->nullable()->index('school_id');
            $table->integer('order')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_favorites');
    }
}
