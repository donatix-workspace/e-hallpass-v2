<?php

namespace App\Http\Requests;

use App\Models\Pass;
use App\Models\Room;
use App\Models\User;
use App\Rules\CheckIfDestinationIsUnavailableRule;
use App\Rules\CheckIfUserAllowsPassToHimRule;
use App\Rules\PassRoomOrUserExistRule;
use App\Rules\ProxyPassOnlyForStudentRule;
use App\Rules\RoomOrUserExistsRule;
use App\Rules\RoomOrUserStringRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;

class ProxyPassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !auth()
            ->user()
            ->isStudent();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $toType = Request::input('to_type');

        return [
            'to_id' => [
                'integer',
                new PassRoomOrUserExistRule($toType),
                'required',
                new CheckIfDestinationIsUnavailableRule($toType),
                new CheckIfUserAllowsPassToHimRule($toType)
            ],
            'to_type' => ['required', 'string', new RoomOrUserStringRule()],
            'comment' => ['max:255'],
            'user_id' => [
                'required',
                'integer',
                Rule::exists('users', 'id')->where(function ($query) {
                    $query
                        ->where('school_id', auth()->user()->school_id)
                        ->where('role_id', config('roles.student'));
                }),
                new ProxyPassOnlyForStudentRule()
            ],
            'auto_approve' => 'boolean'
        ];
    }
}
