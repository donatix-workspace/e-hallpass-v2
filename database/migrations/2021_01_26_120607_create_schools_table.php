<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->id();
            $table->integer('parent');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('logo');
            $table->boolean('stop_status');
            $table->string('student_domain')->nullable()->index('student_domain');
            $table->string('school_domain')->index('school_domain');
            $table->tinyInteger('time_format');
            $table->boolean('status')->index('status');
            $table->string('webservice');
            $table->string('avatar_api');
            $table->string('api_key', 50);
            $table->text('total_user_role');
            $table->integer('total_users');
            $table->string('upload_folder', 25);
            $table->string('archive_prefix', 10);
            $table->text('csv_headers');
            $table->text('csv_roles');
            $table->tinyInteger('if_username')->default(0);
            $table->tinyInteger('is_user_csv')->default(0);
            $table->integer('is_sftp_image')->default(0);
            $table->string('timezone');
            $table->smallInteger('is_manual')->default(0);
            $table->text('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
