<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\StudentFavoriteRequest;
use App\Http\Requests\UserFavoriteBulkReorderRequest;
use App\Http\Requests\UserFavoriteBulkRequest;
use App\Http\Resources\RoomCollection;
use App\Http\Resources\StudentFavoriteResource;
use App\Models\Room;
use App\Models\RoomRestriction;
use App\Models\StudentFavorite;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FavoriteController extends Controller
{
    /**
     * Get the user favorite locations
     *
     * @authenticated
     * @return StudentFavoriteResource
     * @throws AuthorizationException
     */
    public function index()
    {
        $user = auth()->user();

        $restrictedLocationsIds = RoomRestriction::getRestrictedRoomIds();

        $defaultFavourites = StudentFavorite::query()
            ->with(['favorable' => function ($morphTo) {
                $morphTo->morphWith([
                    Room::class => ['activeUnavailability', 'capacity'],
                    User::class => ['activeUnavailability'],
                ]);
            }])
            ->when($user->isStudent() && count($restrictedLocationsIds), function ($query) use ($restrictedLocationsIds) {
                $query->where(function ($query) use ($restrictedLocationsIds) {
                    $query->where('favorable_type', Room::class)
                        ->whereNotIn('favorable_id', $restrictedLocationsIds)
                        ->orWhere('favorable_type', User::class);
                });
            })
            ->where('global', 1)
            ->where('user_id', null)
            ->where('school_id', $user->school_id)
            ->where('section', StudentFavorite::DESTINATION)
            ->orderBy('position', 'asc')
            ->orderBy('section', 'asc')
            ->orderBy('global', 'asc')
            ->get();

        $studentFavorites = StudentFavorite::query()
            ->with(['favorable' => function ($morphTo) {
                $morphTo->morphWith([
                    Room::class => ['activeUnavailability', 'capacity'],
                    User::class => ['activeUnavailability'],
                ]);
            }])
            ->where('user_id', $user->id)
            ->when($user->isStudent() && count($restrictedLocationsIds), function ($query) use ($restrictedLocationsIds) {
                $query->where(function ($query) use ($restrictedLocationsIds) {
                    $query->where('favorable_type', Room::class)
                        ->whereNotIn('favorable_id', $restrictedLocationsIds)
                    ->orWhere('favorable_type', User::class);
                });
            })
            ->where('school_id', $user->school_id)
            ->orderBy('position', 'asc')
            ->orderBy('section', 'asc')
            ->orderBy('global', 'asc')
            ->get();

        foreach ($defaultFavourites as $key => $defaultFavourite) {
            if ($studentFavorites
                ->where('favorable_type', $defaultFavourite->favorable_type)
                ->where('favorable_id', $defaultFavourite->favorable_id)
                ->where('section', StudentFavorite::DESTINATION)
                ->first()) {
                $defaultFavourites->forget($key);
            }
        }
        $favourites = $studentFavorites->merge($defaultFavourites);
        return new StudentFavoriteResource($favourites->filter(function ($favourite) {
            return $favourite->favorable != null;
        }));
    }

    /**
     * Create a student favorite location/adult
     *
     * @bodyParam favorable_id The location/adult id
     * @bodyParam favorable_type The  favorite type (App\\Models\\Room, App\\Models\\User)
     *
     * @param StudentFavoriteRequest $request
     * @return StudentFavoriteResource
     * @throws AuthorizationException
     */
    public function store(StudentFavoriteRequest $request)
    {
        $user = auth()->user();

        $this->authorize('create', StudentFavorite::class);

        $studentFavorite = StudentFavorite::firstOrCreate([
            'user_id' => $user->id,
            'school_id' => $user->school_id,
            'position' => 0,
            'favorable_id' => $request->input('favorable_id'),
            'favorable_type' => $request->input('favorable_type')
        ]);

        return new StudentFavoriteResource($studentFavorite);
    }

    /**
     * Update a position on favorite location
     *
     * @urlParam studentFavorite integer required The id of the favorite record
     * @bodyParam position integer required The position that are gonna be dispalyed in the front end.
     *
     * @param Request $request
     * @param StudentFavorite $studentFavorite
     * @return StudentFavoriteResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, StudentFavorite $studentFavorite)
    {
        $request->validate([
            'position' => 'required|integer|min:0',
        ]);

        $this->authorize('update', $studentFavorite);

        if ($studentFavorite->global) {
            return response()->json([
                'data' => [],
                'message' => __('favorites.the.position.cant.be.changed'),
                'status' => __('fail')
            ], 422);
        }

        $studentFavorite->update([
            'position' => $request->input('position')
        ]);

        return new StudentFavoriteResource($studentFavorite->refresh());
    }

    /**
     * Update user favorites bulk
     *
     * @bodyParam favorites array The object of id and position.
     *
     * @authenticated
     * @param UserFavoriteBulkRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function bulkReorder(UserFavoriteBulkReorderRequest $request)
    {
        $this->authorize('updateBulk', StudentFavorite::class);

        $favorites = $request->input('favorites');
        foreach ($favorites as $favorite) {
            StudentFavorite::where('id', $favorite['id'])
                ->where(function ($query) {
                    $query->where('user_id', auth()->user()->id)
                        ->orWhere('user_id', null);
                })
                ->update([
                    'position' => $favorite['position']
                ]);
        }

        return response()->json([
            'data' => [],
            'message' => __('favorites.bulk.updated.successfully'),
            'status' => __('success')
        ]);
    }

    public function bulkUpdate(UserFavoriteBulkRequest $request)
    {
        $this->authorize('updateBulk', StudentFavorite::class);

        $favorites = $request->input('favorites');
        $user = auth()->user();

        $section = $request->input('section');

        if ($user->isAdmin()) {
            StudentFavorite::where('school_id', $user->school_id)->where('global', true)->delete();
            $rooms = Room::where('school_id', $user->school_id);
            $rooms->update(['admin_favorite' => 0]);
            $rooms->searchable();

            $userId = null;
        } else {
            StudentFavorite::where('user_id', $user->id)
                ->where('school_id', $user->school_id)
                ->where('section', $section)
                ->delete();

            $userId = $user->id;
        }

        $schoolGlobals = StudentFavorite::where('school_id', $user->school_id)
            ->where('user_id', null)
            ->where('global', true)
            ->where('section', StudentFavorite::DESTINATION)
            ->get();


        foreach ($favorites as $favorite) {
            $globalCheck = $section == StudentFavorite::DESTINATION && StudentFavorite::isGlobal($schoolGlobals, $favorite['id'], $favorite['type']);
            $data = [
                'user_id' => $userId,
                'school_id' => $user->school_id,
                'position' => $favorite['position'],
                'favorable_id' => $favorite['id'],
                'favorable_type' => $favorite['type'],
                'global' => $globalCheck,
                'section' => $section,
            ];
            if ($isGlobal = $request->input('global', false) && $section == StudentFavorite::DESTINATION) {
                $this->authorize('updateBulkGlobal', StudentFavorite::class);
                if ($favorite['type'] === Room::class) {
                    $roomUpdate = Room::where('school_id', $user->school_id)
                        ->where('id', $favorite['id'])->first();
                    $roomUpdate->update(['admin_favorite' => Room::ACTIVE]);
                }

                $data['global'] = $isGlobal;
            }
            StudentFavorite::create($data);
        }

        return response()->json([
            'data' => [],
            'message' => __('favorites.bulk.updated.successfully'),
            'status' => __('success')
        ]);
    }

    /**
     * Delete a favorite location/adult.
     *
     * @urlParam studentFavorite integer required The id of the favorite record
     * @param StudentFavorite $studentFavorite
     * @return StudentFavoriteResource|JsonResponse
     * @throws AuthorizationException
     */
    public function delete(StudentFavorite $studentFavorite)
    {
        $this->authorize('delete', $studentFavorite);

        if ($studentFavorite->global === 1) {
            return response()->json([
                'data' => [],
                'message' => __('favorites.cant.be.deleted'),
                'status' => __('fail')
            ], 422);
        }

        $studentFavorite->delete();

        return new StudentFavoriteResource($studentFavorite);
    }
}
