<?php

namespace App\Listeners;

use App\Events\PassMinTimeReached;

class PassMinTimeReachedListener
{
    public function __construct()
    {
        //
    }

    public function handle(PassMinTimeReached $event)
    {
        $event->pass->refreshDashboard();
    }
}
