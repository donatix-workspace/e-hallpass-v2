<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSchoolIdToAutoPassesPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_passes_preferences', function (Blueprint $table) {
            $table->unsignedBigInteger('school_id')->after('user_id');
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_passes_preferences', function (Blueprint $table) {
            $table->dropForeign('auto_passes_preferences_school_id_foreign');
        });
    }
}
