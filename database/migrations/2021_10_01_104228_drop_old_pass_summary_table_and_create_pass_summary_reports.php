<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropOldPassSummaryTableAndCreatePassSummaryReports extends Migration
{
    public function up()
    {
        Schema::dropIfExists('pass_summary_reports');
        Schema::create('pass_summary_reports', function (Blueprint $table) {
            $table->morphs('destination');
            $table->integer('total_passes');
            $table->string('total_time_all');
            $table->string('total_time_ksk');
            $table->string('total_time_proxy');
            $table->string('total_time_student');
            $table->unsignedBigInteger('school_id');
        });
    }

    public function down()
    {

    }
}
