<?php

namespace App\Rules;

use App\Models\PassBlock;
use App\Models\School;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class DateIsPassBlockedRule implements Rule
{
    public $user;

    public function __construct()
    {
        $this->user = auth()->user();
    }

    public function passes($attribute, $value): bool
    {
        $passFullBlock = PassBlock::fromSchoolId($this->user->school_id)
            ->whereAppointments(1)
            ->where('from_date', '<=', School::convertSchoolDatesToUTC($value, $this->user->school->getTimezone()))
            ->where('to_date', '>=', School::convertSchoolDatesToUTC($value, $this->user->school->getTimezone()));

        return !$passFullBlock->exists();
    }

    /**
     * @return string
     */
    public function message(): string
    {
        $passFullBlock = PassBlock::fromSchoolId($this->user->school_id)
            ->whereAppointments(1)
            ->where('from_date', '<=', now()->toDateTimeString())
            ->where('to_date', '>=', now()->toDateTimeString())
            ->first();

        if ($passFullBlock !== null) {
            $passFullBlock = $passFullBlock->toArray();

            $fromDate = Carbon::parse($passFullBlock['from_date'])->toDayDateTimeString();
            $toDate = Carbon::parse($passFullBlock['to_date'])->toDayDateTimeString();

            return __('passes.full.block', ['from_date' => $fromDate, 'to_date' => $toDate, 'reason' => $passFullBlock['reason']]);
        }

        return __('Something went wrong with the dates. Please check line 49 DateIsPassBlockedRule');
    }
}
