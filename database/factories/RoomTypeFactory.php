<?php

namespace Database\Factories;

use App\Models\RoomType;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoomTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RoomType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'comment_type' => $this->faker->boolean,
            'description' => $this->faker->text(250),
            'status' => $this->faker->boolean,
            'ordering' => $this->faker->numberBetween(1,10),
            'receiver' => $this->faker->boolean,
        ];
    }
}
