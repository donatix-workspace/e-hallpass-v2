<?php

namespace App\Http\Controllers\Admin\Passes;

use App\Events\AdminPassCreated;
use App\Events\DashboardRefreshEvent;
use App\Events\PassCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\PassRequest;
use App\Http\Requests\ProxyPassRequest;
use App\Http\Resources\PassEntityCollection;
use App\Http\Resources\PassResource;
use App\Models\ActivePassLimit;
use App\Models\Pass;
use App\Models\PassLimit;
use App\Models\Room;
use App\Models\School;
use App\Models\Unavailable;
use App\Models\User;
use App\Notifications\Push\StudentNotification;
use App\Policies\PassPolicy;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Notification;

/**
 * @group Proxy Passes
 */
class ProxyController extends Controller
{
    /**
     * Show proxy pass creation form.
     *
     * @authenticated
     * @return PassEntityCollection
     */
    public function index(): PassEntityCollection
    {
        $user = auth()->user();

        $passLimitOfSchool = PassLimit::whereType(School::class)
            ->where('limitable_id', $user->school_id)
            ->whereDate('from_date', '<=', Carbon::now())
            ->whereDate('to_date', '>=', Carbon::now())
            ->first();

        return new PassEntityCollection([
            'school_pass_limit' => $passLimitOfSchool
        ]);
    }

    /**
     * Create a pass
     *
     * @bodyParam user_id integer required The id of the student, that is gonna be related for the pass.
     * @bodyParam to_id integer required To what destination user wants to go
     * @bodyParam to_type string required Target destination type (Room or User)
     * @bodyParam comment string required The reason why the user start the pass.
     * @bodyParam auto_approve boolean The pass will be automatically activated.
     *
     * @response status=422 scenario="The user is in another pass" {
     *      'data': [],
     *      'message': 'passes.proxy.user.has.active.pass',
     *      'status': 'fail'
     * }
     *
     * @response status=422 scenario="The active pass limit is reached" {
     *     'data': [],
     *     'message': 'passes.active.pass.limit.reached',
     *     'status': 'fail'
     * }
     *
     * @param ProxyPassRequest $request
     * @return PassResource|JsonResponse
     * @throws AuthorizationException
     * @apiResourceModel App\Models\Pass
     * @apiResource App\Http\Resources\PassResource
     * @authenticated
     */
    public function store(ProxyPassRequest $request)
    {
        $this->authorize('createProxy', Pass::class);

        $activePassOfTheUserExists = Pass::active()
            ->fromSchoolId(auth()->user()->id)
            ->ofUserId($request->input('user_id'))
            ->exists();

        $passesWithActiveStatus = Pass::wherePassStatus(Pass::ACTIVE)
            ->fromSchoolId(auth()->user()->school_id)
            ->ofUserId($request->input('user_id'))
            ->exists();

        $activePassLimitStatus = ActivePassLimit::canCreatePasses(
            ActivePassLimit::PROXY_PASS
        );

        if ($activePassLimitStatus) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('passes.active.pass.limit.reached'),
                    'status' => __('fail')
                ],
                422
            );
        }

        // Removed in Task EHP2-1837
        //        if (
        //            $request->input('to_type') === User::class &&
        //            $request->input('to_id') === auth()->user()->id
        //        ) {
        //            return response()->json(
        //                [
        //                    'data' => [],
        //                    'message' => __('You can not create pass to yourself!'),
        //                    'status' => __('fail')
        //                ],
        //                422
        //            );
        //        }

        if ($activePassOfTheUserExists || $passesWithActiveStatus) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('passes.proxy.user.has.active.pass'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $pass = Pass::create([
            'user_id' => $request->input('user_id'),
            'from_id' => auth()->user()->id,
            'from_type' => User::class,
            'to_id' => $request->input('to_id'),
            'to_type' => $request->input('to_type'),
            'school_id' => auth()->user()->school_id,
            'requested_by' => auth()->user()->id,
            'requested_at' => Carbon::now(),
            'pass_status' => Pass::ACTIVE,
            'approved_at' => $request->input('auto_approve')
                ? Carbon::now()
                : null,
            'approved_by' => $request->input('auto_approve')
                ? auth()->user()->id
                : null,
            'type' =>
                request()->has('type') && request()->input('type') !== 'KSK'
                    ? Pass::KIOSK_PASS
                    : Pass::PROXY_PASS
        ]);

        if ($request->input('comment') !== null) {
            $pass->comments()->create([
                'school_id' => auth()->user()->school_id,
                'user_id' => auth()->user()->id,
                'comment' => $request->input('comment')
            ]);
        }

        event(new PassCreated($pass->load('from', 'to')));
        event(new AdminPassCreated($pass->load('from', 'to')));
        $pass->refreshDashboard();

        //        Notification::send(
        //            $pass,
        //            new StudentNotification('PassCreated', [
        //                'notifyStudent' => true
        //            ])
        //        );

        return new PassResource($pass);
    }
}
