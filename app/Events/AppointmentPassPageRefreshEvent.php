<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;

class AppointmentPassPageRefreshEvent implements ShouldBroadcast
{
    use Dispatchable;

    /**
     * @var int $schoolId Provide on which school the Appointment Pass page should be refreshed
     */
    public $schoolId;

    /**
     * @param int $schoolId
     */
    public function __construct(int $schoolId)
    {
        $this->schoolId = $schoolId;
    }

    /**
     * @return PrivateChannel[]
     */
    public function broadcastOn(): array
    {
        return [
            new PrivateChannel('appointments.passes.'.$this->schoolId)
        ];
    }
}
