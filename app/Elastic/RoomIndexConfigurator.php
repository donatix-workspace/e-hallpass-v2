<?php

namespace App\Elastic;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class RoomIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'rooms_index';
    /**
     * @var array
     */
    protected $settings = [
        'analysis' => [
            'analyzer' => [
                'lowercase' => [
                    'type' => 'custom',
                    'tokenizer' => 'keyword',
                    'filter' => ['lowercase']
                ]
            ]
        ],
        'index.mapping.total_fields.limit' => 2000,
        'index.max_result_window' => 10000,
        'index.number_of_shards' => 4,
        'index.number_of_replicas' => 2
    ];
}
