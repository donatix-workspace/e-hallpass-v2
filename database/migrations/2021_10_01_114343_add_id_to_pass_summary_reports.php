<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdToPassSummaryReports extends Migration
{
    public function up()
    {
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->id()->first();
        });
    }

    public function down()
    {
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->dropColumn('id');
        });
    }
}
