<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSearchIdToContactTracesTable extends Migration
{
    public function up()
    {
        Schema::table('contact_traces', function (Blueprint $table) {
            $table->uuid('search_id');
        });
    }

    public function down()
    {
        Schema::table('contact_traces', function (Blueprint $table) {
            $table->dropColumn('search_id');
        });
    }
}
