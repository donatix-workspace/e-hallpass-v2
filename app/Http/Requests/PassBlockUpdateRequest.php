<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PassBlockUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() &&
            auth()
                ->user()
                ->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $oldDate = 'today';

        return [
            'from_date' => 'required|date',
            'from_time' => 'required|date',
            'to_date' => 'required|date',
            'to_time' => 'required|date|after:from_time',
            'reason' => 'required|string|max:255',
            'message' => 'required|string|max:255',
            'kiosk' => 'boolean',
            'students' => 'boolean',
            'proxy' => 'boolean',
            'appointments' => 'boolean'
        ];
    }
}
