SET FOREIGN_KEY_CHECKS=0;
INSERT INTO ehp_v2_prod.polarities (`first_user_id`,`second_user_id`,`status`,`school_id`,`message`,`created_at`,
                                         `updated_at`)
SELECT
    (SELECT id FROM ehp_v2_prod.users where old_id=oldPolarity.first_user_id limit 1),
    (SELECT id FROM ehp_v2_prod.users where old_id=oldPolarity.second_user_id limit 1),
    (SELECT oldPolarity.status),
    (SELECT id FROM schools where old_id=oldPolarity.school_id limit 1),
    (SELECT oldPolarity.message_for_admin),
    (SELECT CONVERT_TZ(oldPolarity.created_at,
                       (SELECT timezone from schools where old_id = oldPolarity.school_id limit 1),
                       'UTC'
                )) as created_at,
    (SELECT CONVERT_TZ(oldPolarity.updated_at,
                       (SELECT timezone from schools where old_id = oldPolarity.school_id limit 1),
                       'UTC'
                )) as updated_at

FROM ehallpassDB.ab_polarity as oldPolarity;
SET FOREIGN_KEY_CHECKS=1;
