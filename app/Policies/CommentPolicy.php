<?php

namespace App\Policies;

use App\Models\AppointmentPass;
use App\Models\Comment;
use App\Models\Pass;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any comments.
     *
     * @param User $user
     * @param AppointmentPass $appointmentPass
     * @return bool
     */
    public function viewAppointmentComments(User $user, AppointmentPass $appointmentPass)
    {
        return $user->school_id === $appointmentPass->school_id;
    }

    /**
     * Determine whether the user can view any comments.
     *
     * @param User $user
     * @param Pass $pass
     * @return bool
     */
    public function viewPassComments(User $user, Pass $pass)
    {
        return $user->school_id === $pass->school_id;
    }

    /**
     * Determine whether the user can create comments.
     *
     * @param User $user
     * @param $commentableId
     * @param $commentableType
     * @return bool
     */
    public function create(User $user, $commentableId, $commentableType)
    {
        $userAndPassIsWithValidProperties = false;

        if ($commentableType === Pass::class) {
            $pass = Pass::where('id', $commentableId)->first();
            $userAndPassIsWithValidProperties = optional($pass)->school_id === $user->school_id;
        }

        if ($commentableType === AppointmentPass::class) {
            $appointmentPass = AppointmentPass::where('id', $commentableId)->first();
            $userAndPassIsWithValidProperties = optional($appointmentPass)->school_id === $user->school_id;

            if (optional($appointmentPass)->user_id !== $user->id && $user->isStudent()) {
                $userAndPassIsWithValidProperties = false;
            }
        }

        return $userAndPassIsWithValidProperties;
    }

    /**
     * Determine whether the user can update the comment.
     *
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function update(User $user, Comment $comment)
    {
        //
    }

    /**
     * Determine whether the user can delete the comment.
     *
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function delete(User $user, Comment $comment)
    {
        //
    }

    /**
     * Determine whether the user can restore the comment.
     *
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function restore(User $user, Comment $comment)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the comment.
     *
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function forceDelete(User $user, Comment $comment)
    {
        //
    }
}
