<?php

namespace App\Console\Commands;

use App\Models\PassLimit;
use App\Models\School;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class PassLimitArchiveScheduleCommand extends Command
{
    protected $signature = 'passes:limits_archive';

    protected $description = 'Archive expired pass limits';

    public function handle()
    {
        $passLimits = PassLimit::where('archived', 0)
            ->whereDate('to_date', '<', Carbon::today())
            ->where('recurrence_type', null)
            ->get();

        foreach ($passLimits as $passLimit) {
            $passLimit->update([
                'archived' => 1,
            ]);
        }

        return 0;
    }
}
