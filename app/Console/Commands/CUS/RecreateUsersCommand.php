<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonSchoolDomain;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Translators\SchoolTranslator;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CreateCommonUserJob;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class RecreateUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:recreate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recreate users that for some reason dont have cus_uuid';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $admin = User::where('email', 'viktor.lalev@donatix.net')->first();

        $dbQuery = Db::table('users')
//            ->whereIn('id', [1886470, 1886471, 1886472, 1886474, 1886473, 1886475, 1886476])
            ->whereNull('cus_uuid')
            ->whereNotNull('email')
            ->where('email', 'not like', 'simulate%')
            ->where('email', 'not like', 'system%')
            ->where('status', 1)
            ->whereNull('archived_at')
            ->whereRaw("not (username <=> 'system')")
            ->where(function ($q) {
                $q->where('auth_type','!=' ,1001)->orWhereNull('auth_type');
            })
            ->orderBy('id', 'asc');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($users) use ($admin) {
            foreach ($users as $user) {
                $userModel = User::where('id', $user->id)->first();

                if ($userModel) {
                    $school = School::where('id', $userModel->school_id)->whereNotNull('cus_uuid')->first();

                    if ($school) {

                        $commonUser = CommonUser::where('email', $userModel->email)->first();

//                    dd($school);
                        if (!$commonUser) {
                            CreateCommonUserJob::dispatchSync([
                                'user' => $userModel,
                                'authenticatedUser' => $admin,
                                'school' => $school,
                            ]);
                        } else {
                            $userModel->update([
                                'cus_uuid' => $commonUser->uuid,
                                'common_user_id' => $commonUser->id,
                            ]);

                            DB::table('schools_users')->updateOrInsert(
                                [
                                    'school_id' => $school->id,
                                    'user_id' => $userModel->id,
                                ],
                                [
                                    'role_id' => $userModel->role_id
                                ]);
                        }


                    }
                }


                $this->output->progressAdvance();

            }
        });

    }


}
