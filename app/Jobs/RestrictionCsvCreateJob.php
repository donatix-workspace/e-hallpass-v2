<?php

namespace App\Jobs;

use App\Models\RoomRestriction;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;

class RestrictionCsvCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $csvFile;
    public $roomId;
    public $type;
    public $fromDate;
    public $toDate;
    public $user;

    public function __construct($file, $roomId, $fromDate, $toDate, $user, $type)
    {
        $this->csvFile = $file;
        $this->roomId = $roomId;
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
        $this->type = $type;
        $this->user = $user;
    }

    public function handle()
    {
        $roomRestrictionData = [];

        $type = $this->type;
        $roomId = $this->roomId;
        $user = $this->user;
        $fromDate = $this->fromDate;
        $toDate = $this->toDate;
        $csvFile = $this->csvFile;

        $csvEmails = Excel::toCollection((object)[], $csvFile)
            ->flatten()
            ->skip(1);
        $usersFromEmail = User::whereIn('email', $csvEmails)
            ->where('role_id', config('roles.student'))
            ->fromSchoolId($user->school_id)
            ->get('id');

        foreach ($usersFromEmail as $userId) {
            $roomRestrictionData[] = [
                'room_id' => $roomId,
                'school_id' => $user->school_id,
                'user_id' => $userId->id,
                'from_date' => Carbon::parse($fromDate)->setTimezone(config('app.timezone')),
                'to_date' => Carbon::parse($toDate)->setTimezone(config('app.timezone')),
                'type' => $type
            ];
        }

        DB::beginTransaction();
        $roomRestriction = RoomRestriction::multipleInsertOrUpdate($roomRestrictionData);
        DB::commit();
    }
}
