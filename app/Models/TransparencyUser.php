<?php

namespace App\Models;

use App\Elastic\TransparenciesIndexConfigurator;
use App\Traits\ExtendedSearchable;
use App\Traits\PerPage;
use App\Traits\TransparencyMapping;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use ScoutElastic\Searchable;

class TransparencyUser extends Model
{
    use HasFactory, TransparencyMapping, ExtendedSearchable, PerPage;

    protected $with = ['user:id,first_name,last_name'];
    protected $guarded = [];
    protected $indexConfigurator = TransparenciesIndexConfigurator::class;

    protected $table = 'transparency_users';

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @param $query
     * @param $user
     * @return mixed
     */
    public function scopeOfUserId($query, $user)
    {
        return $query->where('user_id', $user);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }
}
