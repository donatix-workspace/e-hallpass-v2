<?php

namespace App\Events;

use App\Entities\CountersEntities;
use App\Models\AppointmentPass;
use App\Models\AutoPass;
use App\Models\AutoPassLimit;
use App\Models\AutoPassPreference;
use App\Models\Kiosk;
use App\Models\Pass;
use App\Models\PassLimit;
use App\Models\Room;
use App\Models\School;
use App\Models\StaffSchedule;
use App\Models\Transparency;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

class DashboardRefreshEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $schoolId;
    public $stats;
    public $user;

    /**
     * @param int $schoolId
     * @param $user
     */
    public function __construct(int $schoolId, $user)
    {
        $this->schoolId = $schoolId;
        $this->user = $user;

        $this->stats = CountersEntities::get($user);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return [new PrivateChannel('dashboard-stats.' . $this->user->id)];
    }
}
