<?php

namespace App\Http\Requests;

use App\Rules\AutoPassValidRoomRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DeleteBulkAutoPassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'autopass_rooms' => 'required|array',
            'autopass_rooms.*' => ['required', 'integer', Rule::exists('rooms', 'id')->where(function ($query) {
                $query->where('school_id', auth()->user()->school_id);
            }), new AutoPassValidRoomRule()]
        ];
    }
}
