<?php

namespace App\Entities;

use App\Models\AppointmentPass;
use App\Models\AutoPass;
use App\Models\AutoPassLimit;
use App\Models\AutoPassPreference;
use App\Models\Kiosk;
use App\Models\Pass;
use App\Models\PassLimit;
use App\Models\Room;
use App\Models\School;
use App\Models\StaffSchedule;
use App\Models\User;
use Carbon\Carbon;
use DB;

class CountersEntities
{
    /**
     * @param null $user
     * @return array
     */
    public static function get($user = null): array
    {
        if ($user === null) {
            $user = auth()->user();
        }

        $schoolTimezone = School::findCacheFirst(
            $user->school_id
        )->getTimezone();
        //
        //        // We pass Carbon object directly to the whereBetween() below
        //        // To correct counters being reset by UTC time.
        $startDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->startOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));
        $endDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->endOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));
        //
        $staffSchedules = StaffSchedule::fromSchoolId($user->school_id)
            ->where('status', AutoPass::ACTIVE)
            ->ofUserId($user->id)
            ->get()
            ->append(['limit', 'room_advance']);
        //
        $staffSchedulesRoomIds = $staffSchedules->pluck('room_id')->toArray();

        $autoPassesRoomsCount = AutoPass::fromSchoolId($user->school_id)
            ->where('status', AutoPass::ACTIVE)
            ->count();
        //
        $autoPassPreferences = AutoPassPreference::onWriteConnection()
            ->with('autoPass')
            ->where('user_id', $user->id)
            ->fromSchoolId($user->school_id)
            ->whereHasIn('autoPass', function ($query) {
                $query->where('status', AutoPass::ACTIVE);
            })
            ->get();

        $autoPassesPreferencesCount = $autoPassPreferences->count();
        //
        $flaggedPassesCount = Pass::on('mysql::read')
            ->where('passes.school_id', $user->school_id)
            ->whereBetween('passes.created_at', [$startDate, $endDate])
            ->useIndex(Pass::PASS_HISTORY_MYSQL_INDEX)
            ->leftJoin('passes as child', 'passes.id', '=', 'child.parent_id')
            ->flaggedPassesAssignedToMe($user, $staffSchedulesRoomIds)
            ->count();
        //
        $passesToMeCount = Pass::on('mysql::read')
            ->where('passes.school_id', $user->school_id)
            ->without(['parent', 'child'])
            ->whereBetween('passes.created_at', [$startDate, $endDate])
            ->leftJoin('passes as child', 'passes.id', '=', 'child.parent_id')
            ->where('passes.parent_id', null)
            ->where('passes.canceled_at', null)
            ->where('passes.approved_at', '!=', null)
            ->where(function ($query) {
                $query
                    ->where('passes.completed_at', '!=', null)
                    ->orWhere('passes.pass_status', Pass::ACTIVE);
            })
            ->passesToMeAndAssignedToMe($user, $staffSchedulesRoomIds)
            ->count();
        //
        //        $staffSchedulesRoomIds = StaffSchedule::ofUserId($user->id)->pluck(
        //            'room_id'
        //        );
        //
        //        $todayAppointmentsCount = AppointmentPass::where(
        //            'school_id',
        //            $user->school_id
        //        )
        //            ->orderBy('confirmed_by_teacher_at', 'asc')
        //            ->where(function ($query) use ($schoolTimezone) {
        //                $query
        //                    ->where('confirmed_by_teacher_at', '!=', null)
        //                    ->orWhere('confirmed_by_teacher_at', null)
        //                    ->where('canceled_at', '!=', null)
        //                    ->orWhere('confirmed_by_teacher_at', null)
        //                    ->where(
        //                        'for_date',
        //                        '<=',
        //                        Carbon::parse(
        //                            now()->setTimezone($schoolTimezone),
        //                            $schoolTimezone
        //                        )
        //                            ->setTimezone(config('app.timezone'))
        //                            ->toDateTimeString()
        //                    );
        //            })
        //            ->where('expired_at', null)
        //            ->where('ran_at', null)
        //            ->whereBetween('for_date', [$startDate, $endDate])
        //            ->where(function ($query) use ($staffSchedulesRoomIds, $user) {
        //                $query
        //                    ->where('from_type', User::class)
        //                    ->where('from_id', $user->id)
        //                    ->orWhere(function ($query) use ($user) {
        //                        $query
        //                            ->where('to_type', User::class)
        //                            ->where('to_id', $user->id);
        //                    })
        //                    ->orWhere(function ($query) use ($staffSchedulesRoomIds) {
        //                        $query
        //                            ->where('to_type', Room::class)
        //                            ->whereIntegerInRaw(
        //                                'to_id',
        //                                $staffSchedulesRoomIds
        //                            );
        //                    })
        //                    ->orWhere(function ($query) use ($staffSchedulesRoomIds) {
        //                        $query
        //                            ->where('from_type', Room::class)
        //                            ->whereIntegerInRaw(
        //                                'from_id',
        //                                $staffSchedulesRoomIds
        //                            );
        //                    })
        //                    ->orWhere(function ($query) use ($user) {
        //                        $query->where('created_by', $user->id);
        //                    });
        //            })
        //            ->count();
        //
        //        $futureAppointmentsCount = AppointmentPass::where(
        //            'school_id',
        //            $user->school_id
        //        )
        //            ->where('canceled_at', null)
        //            ->where('expired_at', null)
        //            ->where('confirmed_by_teacher_at', null)
        //            ->where('ran_at', null)
        //            ->whereHasIn('createdByUser', function ($query) {
        //                $query->where('role_id', config('roles.student'));
        //            })
        //            ->awaiting($user, $staffSchedulesRoomIds)
        //            ->whereBetween('for_date', [
        //                Carbon::parse($startDate)->addDay(),
        //                Carbon::parse($endDate)->addYears(3)
        //            ])
        //            ->count();
        //
        //        $awaitingTodayAppointmentCount = AppointmentPass::where(
        //            'school_id',
        //            $user->school_id
        //        )
        //            ->orderBy('confirmed_by_teacher_at', 'asc')
        //            ->where('canceled_at', null)
        //            ->where('confirmed_by_teacher_at', null)
        //            ->where('expired_at', null)
        //            ->where('ran_at', null)
        //            ->whereHasIn('createdByUser', function ($query) {
        //                $query->where('role_id', config('roles.student'));
        //            })
        //            ->awaiting($user, $staffSchedulesRoomIds)
        //            ->where(
        //                'for_date',
        //                '>=',
        //                Carbon::parse(
        //                    now()->setTimezone($schoolTimezone),
        //                    $schoolTimezone
        //                )
        //                    ->setTimezone(config('app.timezone'))
        //                    ->toDateTimeString()
        //            )
        //            ->whereBetween('for_date', [$startDate, $endDate])
        //            ->count();
        //
        $passLimitOfSchool = PassLimit::whereType(School::class)
            ->where('limitable_id', $user->school_id)
            ->where('grade_year', null)
            ->whereTime('from_date', '<=', Carbon::now())
            ->whereTime('to_date', '>=', Carbon::now())
            ->first();

        $autoPassLimit = AutoPassLimit::where('user_id', $user->id)
            ->where('school_id', $user->school_id)
            ->first();
        //
        $myActivePassesCount = Pass::on('mysql::read')
            ->where('passes.school_id', $user->school_id)
            ->whereBetween('passes.created_at', [$startDate, $endDate])
            ->leftJoin('passes as child', 'passes.id', '=', 'child.parent_id')
            ->where(function ($query) use ($user) {
                $query
                    ->where('passes.parent_id', null)
                    ->where(function ($query) use ($user) {
                        $query
                            ->where('passes.pass_status', Pass::ACTIVE)
                            ->where('passes.approved_at', '!=', null)
                            ->where(function ($query) use ($user) {
                                $query
                                    ->where('passes.approved_by', $user->id)
                                    ->orWhere('passes.completed_by', $user->id)
                                    ->orWhere(function ($query) use ($user) {
                                        $query
                                            ->where(
                                                'passes.from_type',
                                                User::class
                                            )
                                            ->where(
                                                'passes.from_id',
                                                $user->id
                                            );
                                    })
                                    ->orWhere(function ($query) use ($user) {
                                        $query
                                            ->where(
                                                'passes.to_type',
                                                User::class
                                            )
                                            ->where('passes.to_id', $user->id);
                                    });
                            });
                    })
                    ->orWhere(function ($query) use ($user) {
                        $query
                            ->where('child.pass_status', Pass::ACTIVE)
                            ->where(function ($query) use ($user) {
                                $query
                                    ->where('child.approved_by', $user->id)
                                    ->orWhere('child.completed_by', $user->id)
                                    ->orWhere(function ($query) use ($user) {
                                        $query
                                            ->where(
                                                'child.from_type',
                                                User::class
                                            )
                                            ->where('child.from_id', $user->id);
                                    })
                                    ->orWhere(function ($query) use ($user) {
                                        $query
                                            ->where(
                                                'child.to_type',
                                                User::class
                                            )
                                            ->where('child.to_id', $user->id);
                                    });
                            });
                    })
                    ->orWhere(function ($query) use ($user) {
                        $query
                            ->where(function ($query) use ($user) {
                                $query
                                    ->where('passes.approved_by', $user->id)
                                    ->orWhere('passes.completed_by', $user->id)
                                    ->orWhere(function ($query) use ($user) {
                                        $query
                                            ->where(
                                                'passes.from_type',
                                                User::class
                                            )
                                            ->where(
                                                'passes.from_id',
                                                $user->id
                                            );
                                    })
                                    ->orWhere(function ($query) use ($user) {
                                        $query
                                            ->where(
                                                'passes.to_type',
                                                User::class
                                            )
                                            ->where('passes.to_id', $user->id);
                                    });
                            })
                            ->where(function ($query) use ($user) {
                                $query->where(
                                    'child.pass_status',
                                    Pass::ACTIVE
                                );
                            });
                    });
            })
            ->count();
        //
        $myOpenKiosksCount = Kiosk::where('user_id', $user->id)
            ->where('school_id', $user->school_id)
            ->whereNull('expired_date')
            ->count();
        //
        $buildingPasses = CountersEntities::buildingCounters(
            $user->school_id,
            $schoolTimezone
        );
        //
        return [
            'autoPassesPreferencesCount' => $autoPassesPreferencesCount,
            'autoPassesRoomsCount' => $autoPassesRoomsCount,
            'flaggedPassesCount' => $flaggedPassesCount,
            'buildingPassesCount' =>
                $buildingPasses['buildingPassesCount'] ?: 0,
            'buildingActivePassesCount' =>
                $buildingPasses['buildingActivePassesCount'] ?: 0,
            'passesToMeCount' => $passesToMeCount,
            'todayAppointmentsCount' => 0,
            'futureAppointmentsCount' => 0,
            'awaitingTodayAppointmentsCount' => 0,
            'myActivePassesCount' => $myActivePassesCount,
            'passLimitOfSchoolCount' => $passLimitOfSchool,
            'myOpenKiosksCount' => $myOpenKiosksCount,
            'autoPassLimit' => $autoPassLimit,
            'tabCounters' => self::tabCounters($user)
        ];
    }

    /**
     * @param int $schoolId
     * @param string $schoolTimezone
     * @return array
     */
    public static function buildingCounters(
        int $schoolId,
        string $schoolTimezone
    ): array {
        // We pass Carbon object directly to the whereBetween() below
        // To correct counters being reset by UTC time.
        $startDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->startOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));
        $endDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->endOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));

        // Building passes
        $buildingCounters = DB::table('passes')
            ->selectRaw(
                'sum(
		CASE WHEN parent_id IS NULL
			AND approved_at IS NOT NULL THEN
			1
		    WHEN parent_id IS NOT NULL
				AND pass_status = 1 THEN 0
			ELSE
				0
			END) AS buildingTotal,
	sum(
		CASE WHEN pass_status = 1
			AND approved_at THEN
			1
			WHEN pass_status = 1 and parent_id is not null
			THEN 1
		ELSE
			0
		END) buildingActive
        '
            )
            ->where('school_id', $schoolId)
            ->whereBetween('created_at', [$startDate, $endDate])
            ->limit(1)
            ->first();

        return [
            'buildingPassesCount' => $buildingCounters->buildingTotal,
            'buildingActivePassesCount' => $buildingCounters->buildingActive
        ];
    }

    /**
     * @param $user
     * @return array
     */
    public static function tabCounters($user): array
    {
        //        $staffSchedules = StaffSchedule::fromSchoolId($user->school_id)
        //            ->where('status', AutoPass::ACTIVE)
        //            ->ofUserId($user->id)
        //            ->get()
        //            ->append(['limit', 'room_advance']);
        //
        //        $staffSchedulesIds = $staffSchedules->pluck('id')->toArray();
        //
        //        $schoolTimezone = School::findCacheFirst(
        //            $user->school_id
        //        )->getTimezone();
        //
        //        $startDate = Carbon::parse(
        //            now()
        //                ->setTimezone($schoolTimezone)
        //                ->startOfDay(),
        //            $schoolTimezone
        //        )->setTimezone(config('app.timezone'));
        //        $endDate = Carbon::parse(
        //            now()
        //                ->setTimezone($schoolTimezone)
        //                ->endOfDay(),
        //            $schoolTimezone
        //        )->setTimezone(config('app.timezone'));
        //
        //        $waitingApprovalPasses = Pass::where('parent_id', null)
        //            ->activeWithoutApprove()
        //            ->where('approved_at', null)
        //            ->transparency($user, $staffSchedulesIds, $staffSchedulesIds)
        //            ->whereBetween('created_at', [$startDate, $endDate])
        //            ->where('school_id', $user->school_id)
        //            ->count();
        //
        //        $approvedPasses = Pass::where('parent_id', null)
        //            ->where(function ($query) use ($user) {
        //                $query
        //                    ->active()
        //                    ->orWhereHasIn('child', function ($query) use ($user) {
        //                        $query
        //                            ->where('school_id', $user->school_id)
        //                            ->activeWithoutApprove();
        //                    });
        //            })
        //            ->transparency($user, $staffSchedulesIds, $staffSchedulesIds)
        //            ->whereBetween('created_at', [$startDate, $endDate])
        //            ->where('school_id', $user->school_id)
        //            ->count();
        //
        return [
            'waitingApprovalPasses' => 0,
            'approvedPasses' => 0
        ];
    }
}
