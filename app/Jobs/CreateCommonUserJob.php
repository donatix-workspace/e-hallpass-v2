<?php

namespace App\Jobs;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class CreateCommonUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $user;
    protected $password;
    protected $authenticatedUser;
    protected $sendInvite;
    protected $skipDelete;
    protected $school;

    public function __construct($data)
    {
        $this->user = $data['user'];
        $this->password = $data['password'] ?? null;
        $this->authenticatedUser = $data['authenticatedUser'];
        $this->sendInvite = $data['sendInvite'] ?? false;
        $this->skipDelete = $data['skipDelete'] ?? false;
        $this->school = $data['school'] ?? $this->user->school;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new CommonUserService($this->authenticatedUser);

        $membership = DB::table('schools_users')
            ->where('school_id', $this->school->id)
            ->where('user_id', $this->user->id)
            ->orderBy('id', 'desc')
            ->first();

        if (!$membership) {
            return false;
        }

        $roleCollection = CommonUser::getRoleName($membership->role_id);

        $userData = [
            'email' => $this->user->email,
            'firstName' => $this->user->first_name,
            'lastName' => $this->user->last_name,
            'status' => $this->user->status,
        ];
        if ($this->user->user_initials) {
            $userData['title'] = $this->user->user_initials;
        }
//        if ($this->password) {
        $userData['password'] = $this->password;
//        }

        $gradeYear = $this->user->grade_year;
        if (!$gradeYear && $this->user->isStudent()) {
            $gradeYear = 0;
        }


        $membershipData = [
            "status" => $this->user->status,
            "school" => $this->school->cus_uuid,
            'authClientId' => 2,
            "user" => $userData,
            "role" => $roleCollection[0],
            "graduationYear" => $gradeYear,
            'sisId' => $membership->student_sis_id,
            "sendConfirmationEmail" => $this->sendInvite,
            "preventFromArchiving" => true
        ];

        $resMembership = $client->assignUserToSchool($membershipData);
        //logger('MEMBERSHIP', [$membership]);
        if (isset($resMembership['id'])) {

            $userCommonId = CommonMembership::where('id', $resMembership['id'])->first()->auth_user_id;

            $this->user->update([
                'cus_uuid' => CommonUser::find($userCommonId)->uuid,
                'initial_password' => null
            ]);
            DB::table('schools_users')->where('id', $membership->id)
                ->update(
                    [
                        'membership_uuid' => $resMembership['uuid'],
                        'role_id' => $this->user->role_id
                    ]);

        } else {
            //TODO What to do if there are no cus uuid
//            if (!$this->skipDelete) {
//                $this->user->delete();
//            }
        }
    }
}
