<?php

namespace App\Http\Middleware;

use App\Models\Pass;
use App\Models\Polarity;
use App\Models\TeacherPassSetting;
use Closure;
use Illuminate\Http\Request;

class PassMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();
        $teacherPassSettings = TeacherPassSetting::where(
            'school_id',
            $user->school_id
        )
            ->select('polarity_message')
            ->first();

        $polarityCanceledIds = Polarity::active()
            ->ofUserId($user->id)
            ->fromSchoolId($user->school_id)
            ->selectRaw(
                'if(first_user_id = ?, second_user_id, first_user_id) as polarity_ids',
                [$user->id]
            )
            ->pluck('polarity_ids');

        /** @var void $polarityOutStudents */
        $polarityOutStudents = Pass::wherePassStatus(Pass::ACTIVE)
            ->fromSchoolId($user->school_id)
            ->whereIntegerInRaw('user_id', $polarityCanceledIds)
            ->get();

        // If has outgoing student we cancel the request.
        if (!$polarityOutStudents->isEmpty()) {
            $destination = [
                'type' => $request->input('to_type'),
                'id' => $request->input('to_id')
            ];

            $fromApplication = $request->header('From-Application');
            Polarity::registerAttempt(
                $polarityCanceledIds,
                $destination,
                $fromApplication
            );

            return response()->json(
                [
                    'data' => ['ids' => $polarityCanceledIds],
                    'message' => !empty($teacherPassSettings->polarity_message)
                        ? $teacherPassSettings->polarity_message
                        : __('passes.polarities.student'),
                    'status' => __('fail')
                ],
                422
            );
        }

        return $next($request);
    }
}
