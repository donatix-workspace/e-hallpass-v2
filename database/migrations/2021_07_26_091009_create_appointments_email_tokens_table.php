<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsEmailTokensTable extends Migration
{
    public function up()
    {
        Schema::create('appointments_email_tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('appointment_pass_id');
            $table->string('token', 256);
            $table->timestamp('used_at');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('appointments_email_tokens');
    }
}
