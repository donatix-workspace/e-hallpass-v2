<?php

namespace App\Traits;

trait WhereInRawChunked
{
    /**
     * @param $query
     * @param $column
     * @param $array
     * @param int $size
     * @return mixed
     */
    public function scopeWhereIntegerInRawChunk($query, $column, $array, int $size = 5000)
    {
        $sizeOfArray = count($array);
        if ($sizeOfArray >= $size) {
            $chunk = array_chunk($array, $size);
            $query->where(function ($query) use ($column, $chunk) {
                $query->whereIntegerInRaw($column, $chunk[0]);

                foreach ($chunk as $key => $value) {
                    if ($key === 0) continue;
                    $query->orWhereIntegerInRaw($column, $value);
                }
            });
        } else {
            $query->whereIntegerInRaw($column, $array);
        }

        return $query;
    }
}
