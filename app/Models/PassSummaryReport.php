<?php

namespace App\Models;

use App\Elastic\SummaryReportIndexConfigurator;
use App\Http\Filters\Filterable;
use App\Traits\ExtendedSearchable;
use App\Traits\PassSummaryReportMapping;
use App\Traits\PerPage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use ScoutElastic\Searchable;

class PassSummaryReport extends Model
{
    use HasFactory,
        PassSummaryReportMapping,
        ExtendedSearchable,
        PerPage,
        Filterable;

    protected $table = 'pass_summary_reports';
    protected $indexConfigurator = SummaryReportIndexConfigurator::class;

    protected $with = ['destination'];

    protected $appends = ['from_date', 'to_date'];

    public const HIGHEST_PASS_TAKERS = 'highest_pass_takers';
    public const HIGHEST_PASS_GRANTERS = 'highest_pass_granters';
    public const HIGHEST_PASS_DESTINATION = 'highest_pass_destination';
    public const RUNNED_CACHE_NAME = 'pass_summary_runned_by_school_id_';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function destination(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo('destination')->withoutGlobalScopes();
    }

    /**
     * @param $type
     * @return array
     */
    public static function summaryModelBindByType($type)
    {
        if (
            $type === self::HIGHEST_PASS_TAKERS ||
            $type === self::HIGHEST_PASS_GRANTERS
        ) {
            return [
                'model' => User::class,
                'table' => 'users',
                'selects' => ['first_name', 'last_name', 'id as user_id']
            ];
        }

        return [
            'model' => Room::class,
            'table' => 'rooms',
            'selects' => ['name', 'id as room_id']
        ];
    }

    /**
     * @return mixed
     */
    public function getFromDateAttribute()
    {
        $fromDate = json_decode($this->filters_dates, true);
        if ($fromDate === null) {
            return null;
        }

        return array_key_exists('dates', $fromDate)
            ? $fromDate['dates']['from']
            : null;
    }

    /**
     * @return mixed
     */
    public function getToDateAttribute()
    {
        $toDate = json_decode($this->filters_dates, true);

        if ($toDate === null) {
            return null;
        }

        return array_key_exists('dates', $toDate)
            ? $toDate['dates']['to']
            : null;
    }

    /**
     * @param $aggregators
     * @param string $aggregatorsBy
     * @param string $model
     * @param $user
     * @param $table
     * @param $fromDate
     * @param $toDate
     * @return mixed
     */
    public static function allTypeAggregatorQueryAdd(
        $aggregators,
        string $aggregatorsBy,
        string $model,
        $user,
        $table,
        $fromDate,
        $toDate
    ) {
        return $aggregators
            ->addSelect([
                'total_passes' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::selectRaw('count(id)')
                            ->where('passes.created_at', '>=', $fromDate)
                            ->where('passes.created_at', '<=', $toDate)
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->limit(150)
                            ->queryByModel($table, $model)
                        : Pass::selectRaw('count(id)')
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.created_at', '>=', $fromDate)
                            ->where('passes.parent_id', null)
                            ->where('passes.created_at', '<=', $toDate)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->whereColumn('passes.user_id', "$table.id")
            ])
            ->addSelect([
                'total_time_proxy' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::calculateTotalTimeBySql()
                            ->where('passes.type', Pass::PROXY_PASS)
                            ->where('passes.created_at', '>=', $fromDate)
                            ->where('passes.created_at', '<=', $toDate)
                            ->where('passes.school_id', $user->school_id)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->where('passes.parent_id', null)
                            ->queryByModel($table, $model)
                        : Pass::calculateTotalTimeBySql()
                            ->where('passes.type', Pass::PROXY_PASS)
                            ->where('passes.created_at', '>=', $fromDate)
                            ->where('passes.parent_id', null)
                            ->where('passes.created_at', '<=', $toDate)
                            ->where('passes.school_id', $user->school_id)
                            ->limit(150)
                            ->whereColumn('passes.user_id', "$table.id")
            ])
            ->addSelect([
                'total_time_ksk' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::calculateTotalTimeBySql()
                            ->where('passes.created_at', '>=', $fromDate)
                            ->where('passes.created_at', '<=', $toDate)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->where('passes.type', Pass::KIOSK_PASS)
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->queryByModel($table, $model)
                        : Pass::calculateTotalTimeBySql()
                            ->where('passes.type', Pass::KIOSK_PASS)
                            ->where('passes.created_at', '>=', $fromDate)
                            ->where('passes.parent_id', null)
                            ->where('passes.created_at', '<=', $toDate)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->where('passes.school_id', $user->school_id)
                            ->whereColumn('passes.user_id', "$table.id")
            ])
            ->addSelect([
                'total_time_apt' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::calculateTotalTimeBySql()
                            ->where('passes.type', Pass::APPOINTMENT_PASS)
                            ->where('passes.created_at', '>=', $fromDate)
                            ->where('passes.created_at', '<=', $toDate)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->queryByModel($table, $model)
                        : Pass::calculateTotalTimeBySql()
                            ->where('passes.type', Pass::APPOINTMENT_PASS)
                            ->where('passes.created_at', '>=', $fromDate)
                            ->where('passes.parent_id', null)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->where('passes.created_at', '<=', $toDate)
                            ->where('passes.school_id', $user->school_id)
                            ->whereColumn('passes.user_id', "$table.id")
            ])
            ->addSelect([
                'total_time_student' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::calculateTotalTimeBySql()
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.created_at', '>=', $fromDate)
                            ->where('passes.created_at', '<=', $toDate)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->where('passes.parent_id', null)
                            ->where('passes.type', Pass::STUDENT_CREATED_PASS)
                            ->queryByModel($table, $model)
                        : Pass::calculateTotalTimeBySql()
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.created_at', '>=', $fromDate)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->where('passes.created_at', '<=', $toDate)
                            ->where('passes.parent_id', null)
                            ->whereColumn('passes.user_id', "$table.id")
                            ->where('passes.type', Pass::STUDENT_CREATED_PASS)
            ]);
    }

    /**
     * @param $aggregators
     * @param string $aggregatorsBy
     * @param string $model
     * @param $user
     * @param $table
     * @param $fromDate
     * @param $toDate
     * @return mixed
     */
    public static function studentTypeAggregatorQueryAdd(
        $aggregators,
        string $aggregatorsBy,
        string $model,
        $user,
        $table,
        $fromDate,
        $toDate
    ) {
        return $aggregators
            ->addSelect([
                'total_passes' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::selectRaw('count(id)')
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->limit(150)
                            ->queryByModel($table, $model)
                        : Pass::selectRaw('count(id)')
                            ->where('passes.school_id', $user->school_id)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where('passes.parent_id', null)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->whereColumn('passes.user_id', "$table.id")
            ])
            ->addSelect([
                'total_time_student' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::calculateTotalTimeBySql()
                            ->where('passes.school_id', $user->school_id)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where('passes.type', Pass::STUDENT_CREATED_PASS)
                            ->where('passes.parent_id', null)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->queryByModel($table, $model)
                        : Pass::calculateTotalTimeBySql()
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->limit(150)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where('passes.type', Pass::STUDENT_CREATED_PASS)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->whereColumn('passes.user_id', "$table.id")
            ]);
    }

    /**
     * @param $aggregators
     * @param string $aggregatorsBy
     * @param string $model
     * @param $user
     * @param $table
     * @param $fromDate
     * @param $toDate
     * @return mixed
     */
    public static function appointmentTypeAggregatorQueryAdd(
        $aggregators,
        string $aggregatorsBy,
        string $model,
        $user,
        $table,
        $fromDate,
        $toDate
    ) {
        return $aggregators
            ->addSelect([
                'total_passes' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::selectRaw('count(id)')
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->where('passes.to_type', $model)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->queryByModel($table, $model)
                        : Pass::selectRaw('count(id)')
                            ->where('passes.school_id', $user->school_id)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->whereColumn('passes.user_id', "$table.id")
                            ->where('passes.parent_id', null)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
            ])
            ->addSelect([
                'total_time_apt' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::calculateTotalTimeBySql()
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->where('passes.type', Pass::APPOINTMENT_PASS)
                            ->where('passes.to_type', $model)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->queryByModel($table, $model)
                        : Pass::calculateTotalTimeBySql()
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->where('passes.type', Pass::APPOINTMENT_PASS)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->whereColumn('passes.user_id', "$table.id")
            ]);
    }

    /**
     * @param $aggregators
     * @param string $aggregatorsBy
     * @param string $model
     * @param $user
     * @param $table
     * @param $fromDate
     * @param $toDate
     * @return mixed
     */
    public static function proxyTypeAggregatorQueryAdd(
        $aggregators,
        string $aggregatorsBy,
        string $model,
        $user,
        $table,
        $fromDate,
        $toDate
    ) {
        return $aggregators
            ->addSelect([
                'total_passes' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::selectRaw('count(id)')
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->limit(150)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where('passes.to_type', $model)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->queryByModel($table, $model)
                        : Pass::selectRaw('count(id)')
                            ->where('passes.school_id', $user->school_id)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->limit(150)
                            ->where('passes.parent_id', null)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->whereColumn('passes.user_id', "$table.id")
            ])
            ->addSelect([
                'total_time_proxy' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::calculateTotalTimeBySql()
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->limit(150)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where('passes.type', Pass::PROXY_PASS)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->queryByModel($table, $model)
                        : Pass::calculateTotalTimeBySql()
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->where('passes.type', Pass::PROXY_PASS)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->whereColumn('passes.user_id', "$table.id")
            ]);
    }

    /**
     * @param $aggregators
     * @param string $aggregatorsBy
     * @param string $model
     * @param $user
     * @param $table
     * @param $fromDate
     * @param $toDate
     * @return mixed
     */
    public static function kioskTypeAggregatorQueryAdd(
        $aggregators,
        string $aggregatorsBy,
        string $model,
        $user,
        $table,
        $fromDate,
        $toDate
    ) {
        return $aggregators
            ->addSelect([
                'total_passes' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::selectRaw('count(id)')
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where('passes.to_type', $model)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->queryByModel($table, $model)
                        : Pass::selectRaw('count(id)')
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->limit(150)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->whereColumn('passes.user_id', "$table.id")
            ])
            ->addSelect([])
            ->addSelect([
                'total_time_ksk' =>
                    $aggregatorsBy !== PassSummaryReport::HIGHEST_PASS_TAKERS
                        ? Pass::calculateTotalTimeBySql()
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where('passes.to_type', $model)
                            ->where('passes.type', Pass::KIOSK_PASS)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->queryByModel($table, $model)
                        : Pass::calculateTotalTimeBySql()
                            ->where('passes.school_id', $user->school_id)
                            ->where('passes.parent_id', null)
                            ->whereBetween('passes.created_at', [
                                $fromDate,
                                $toDate
                            ])
                            ->where('passes.type', Pass::KIOSK_PASS)
                            ->limit(150)
                            ->where(function ($query) {
                                $query
                                    ->where('passes.approved_at', '!=', null)
                                    ->orWhere('passes.approved_at', null)
                                    ->where('passes.expired_at', '!=', null)
                                    ->where(
                                        'passes.type',
                                        Pass::APPOINTMENT_PASS
                                    )
                                    ->orWhere('passes.canceled_at', '!=', null)
                                    ->where('passes.approved_at', null);
                            })
                            ->whereColumn('passes.user_id', "$table.id")
            ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param array $totalFields
     * @return false|string
     */
    public static function calculateTotalTime(array $totalFields)
    {
        return gmdate('H:i:s', array_sum($totalFields));
    }
}
