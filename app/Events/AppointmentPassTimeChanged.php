<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AppointmentPassTimeChanged implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $appointment;
    public $newUserId;
    public $newTime;
    public $oldTime;

    /**
     * Create a new event instance.
     *
     * @param $appointmentPass
     * @param null $newUserId
     * @param null $oldTime
     * @param null $newTime
     */
    public function __construct($appointmentPass, $newUserId = null, $oldTime = null, $newTime = null)
    {
        $this->appointment = $appointmentPass;
        $this->newUserId = $newUserId;
        $this->newTime = $newTime;
        $this->oldTime = $oldTime;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('user.appointments.passes.' . $this->appointment->user_id),
            new PrivateChannel('appointments.passes.' . $this->appointment->school_id)
        ];
    }
}
