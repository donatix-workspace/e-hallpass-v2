<?php

namespace App\Console\Commands\Rollback;

use Illuminate\Console\Command;
use DB;

class RollbackAutoPassCommand extends Command
{
    protected $signature = 'ehpv1_rollback:autopass';

    protected $description = 'Migrate from EhpV2 to EhpV1 AutoPasses';

    public function handle(): int
    {
        $this->info('--- Migrating EhpV2 Auto Passes to EhpV1 ---');

        $newDb = DB::connection();
        $oldDb = DB::connection('oldDB');

        $newDb->disableQueryLog();
        $oldDb->disableQueryLog();

        $autoPasses = $newDb->table('auto_passes')
            ->orderBy('id', 'desc')
            ->get()
            ->toArray();

        $this->output->progressStart(count($autoPasses));

        $autoPassesChunk = array_chunk($autoPasses, 1500);

        foreach ($autoPassesChunk as $autoPassChunk) {
            $insertableAutoPassArray = [];

            foreach ($autoPassChunk as $autoPass) {
                $newSchool = $newDb->table('schools')->where('id', $autoPass->school_id)->select('id', 'old_id')->first();

                if ($newSchool) {
                    $room = $newDb->table('rooms')->select('id')->where('id', $autoPass->room_id)->first();

                    $insertableAutoPassArray[] = [
                        'room_id' => $room->old_id,
                        'school_id' => $newSchool->old_id,
                        'created_at' => now(),
                        'updated_at' => now()
                    ];
                }
            }

            try {
                DB::beginTransaction();
                $oldDb->table('auto_pass')
                    ->insert($insertableAutoPassArray);

                $this->output->progressAdvance(count($insertableAutoPassArray));

                DB::commit();
            } catch (\Throwable $e) {
                $this->info($e->getMessage());
            }
        }

        $this->output->progressFinish();

    return 0;
    }
}
