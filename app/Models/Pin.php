<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pin extends Model
{
    use HasFactory;

    protected $fillable = ['school_id', 'pinnable_id', 'pinnable_type', 'pin'];

    public const USER_TYPE = User::class;
    public const ROOM_TYPE = Room::class;
    public const ACTIVE = 1;
    public const INACTIVE = 0;

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @param $query
     * @param string $type (Default: Teacher pin)
     * @return mixed
     */
    public function scopeFromType($query, $type = self::USER_TYPE)
    {
        return $query->where('pinnable_type', $type);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function pinnable()
    {
        return $this->morphTo()->withoutGlobalScopes();
    }
}
