<?php

namespace App\Http\Filters;

use App\Models\Pass;
use App\Models\PassSummaryReport;
use App\Models\RoomRestriction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class SummaryReportFilter extends QueryFilters
{
    protected $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    /**
     * @param array $values
     * @return Builder
     */
    public function teacher_ids(array $values): Builder
    {
        return count($values) && $this->request->input('aggregate_by') === PassSummaryReport::HIGHEST_PASS_GRANTERS ? $this->builder->where(function ($query) use ($values) {
            return $query->whereIntegerInRaw('id', $values);
        }) : $this->builder;
    }

    /**
     * @param array $values
     * @return Builder
     */
    public function student_ids(array $values): Builder
    {
        return count($values) && $this->request->input('aggregate_by') === PassSummaryReport::HIGHEST_PASS_TAKERS ? $this->builder->where(function ($query) use ($values) {
            return $query->whereIntegerInRaw('id', $values);
        }) : $this->builder;
    }

    /**
     * @param array $values
     * @return Builder
     */
    public function room_ids(array $values): Builder
    {
        return count($values) && $this->request->input('aggregate_by') === PassSummaryReport::HIGHEST_PASS_DESTINATION ? $this->builder->where(function ($query) use ($values) {
            return $query->whereIntegerInRaw('id', $values);
        }) : $this->builder;
    }

    /**
     * @param array $values
     * @return Builder
     */
    public function by_grad_year(array $values): Builder
    {
        return count($values) && $this->request->input('aggregate_by') === PassSummaryReport::HIGHEST_PASS_TAKERS ? $this->builder->where(function ($query) use ($values) {
            return $query->whereIntegerInRaw('grade_year', $values);
        }) : $this->builder;
    }
}
