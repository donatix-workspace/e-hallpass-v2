<?php

namespace App\Http\Controllers\Admin\Passes;

use App\Http\Controllers\Controller;
use App\Http\Filters\PassHistoryFilter;
use App\Http\Requests\PassHistoryTimeUpdateRequest;
use App\Http\Resources\PassCollection;
use App\Http\Resources\PassResource;
use App\Models\AutoPass;
use App\Models\AutoPassPreference;
use App\Models\Pass;
use App\Models\Room;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Cache;
use Carbon\Carbon;
use Gate;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;

/**
 * @group Pass Histories
 */
class PassHistoryController extends Controller
{
    /**
     * Get passes history with filters
     *
     * @queryParam per_page integer How many items will be showed per page
     * @queryParam search_query string Elastic search query.
     * @queryParam sort string The sort types: "asc", "desc"
     * @queryParam only_my_passes boolean  Get only passes to are approved by authenticated teacher
     * @queryParam only_canceled_passes boolean Get only passes that are canceled.
     * @queryParam only_missed_passes boolean  Get only passes that are expired.
     * @queryParam by_students string Get the passes created by selected students Example: ?by_students=1,2,3.
     * @queryParam by_teachers string Get the passes approved,ended,canceled by teachers Example: ?by_teachers=1,2,3
     * @queryParam by_rooms string Get all the passes pointed to current selected rooms Example: ?by_rooms =1,2,3
     * @queryParam dates string Get all the passes pointed from date to date Example: ?dates=11/02/2020,12/02/2020
     * @queryParam yellow_time string Get all passes that are > from yellow time.
     * @queryParam system_ended string Get all passes that are system ended.
     * @queryParam extended_time string Get all passes that are with total_time > extended
     * @queryParam edited string Get all passes that are edited.
     *
     * @authenticated
     * @apiResourceModel App\Models\Pass
     * @apiResourceCollection App\Http\Resources\PassCollection
     * @param PassHistoryFilter $filters
     * @return PassCollection|JsonResponse
     */
    public function filter(PassHistoryFilter $filters)
    {
        if (!Gate::allows('pass-history-run-filters')) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('invalid.permission'),
                    'status' => __('fail')
                ],
                403
            );
        }

        if (
            request()->has('search_query') &&
            request()->get('search_query') !== null
        ) {
            $pass = Pass::searchableQueries(
                Pass::recordsPerPage(),
                false,
                $filters
            );

            return new PassCollection($pass);
        }

        $pass = Pass::nonSearchableQueries($filters, Pass::recordsPerPage());

        return new PassCollection($pass);
    }

    /**
     * Change PassHistory Time
     *
     * @bodyParam out_time required date The new out time
     * @bodyParam in_time required date The new in time
     * @bodyParam child_out_time required date The new child out time
     * @bodyParam child_in_time required date The new child in time.
     *
     * @authenticated
     * @param Pass $pass
     * @param PassHistoryTimeUpdateRequest $request
     * @return PassResource|JsonResponse
     */
    public function time(Pass $pass, PassHistoryTimeUpdateRequest $request)
    {
        if (!Gate::allows('pass-history-update-time', $pass)) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('can\'t.update.time.user.not.own.the.pass'),
                    'status' => __('fail')
                ],
                403
            );
        }

        if (
            !$pass->isApproved() ||
            !$pass->isCompleted() ||
            $pass->pass_status === 1
        ) {
            return response()->json([
                'data' => ['pass_id' => $pass->id],
                'message' => __('passes.is.not.approved.or.completed'),
                'status' => __('fail')
            ]);
        }

        $outTime = $request->input('out_time');
        $inTime = $request->input('in_time');
        $childOutTime = $request->input('child_out_time');
        $childInTime = $request->input('child_in_time');
        $teacherPassSettings = TeacherPassSetting::where(
            'school_id',
            $pass->school_id
        )->first();
        $systemEndMinute = TeacherPassSetting::convertInMinutes(
            $teacherPassSettings->auto_expire_time
        );
        $minTimeYellow = TeacherPassSetting::convertInMinutes(
            $teacherPassSettings->min_time
        );

        if ($pass->hasChild()) {
            if ($pass->child->isApproved()) {
                $pass->child()->update([
                    'approved_at' => Carbon::parse(
                        $pass->child->approved_at->format('Y-m-d') .
                            " $childOutTime",
                        $pass->school->getTimezone()
                    )->setTimezone(config('app.timezone')),
                    'completed_at' =>
                        $pass->child->completed_at !== null
                            ? Carbon::parse(
                                $pass->child->completed_at->format('Y-m-d') .
                                    " $childInTime",
                                $pass->school->getTimezone()
                            )->setTimezone(config('app.timezone'))
                            : null,
                    'edited_at' => Carbon::now()
                ]);
            }
        }

        $pass->update([
            'approved_at' => Carbon::parse(
                $pass->approved_at->format('Y-m-d') . " $outTime",
                $pass->school->getTimezone()
            )->setTimezone(config('app.timezone')),
            'completed_at' => Carbon::parse(
                $pass->completed_at->format('Y-m-d') . " $inTime",
                $pass->school->getTimezone()
            )->setTimezone(config('app.timezone')),
            'edited_at' => Carbon::now()
        ]);

        $passNewTotalTime = TeacherPassSetting::convertInMinutes(
            $pass->refresh()->total_time
        );

        // When the total_time is less than system end time we remove the red flag
        if ($passNewTotalTime < $systemEndMinute) {
            if ($pass->hasChild()) {
                $pass->child()->update([
                    'system_completed' => Pass::SYSTEM_COMPLETED_INACTIVE
                ]);
            }
            $pass->update([
                'system_completed' => Pass::SYSTEM_COMPLETED_INACTIVE
            ]);
        } else {
            if ($pass->hasChild()) {
                $pass->child()->update([
                    'system_completed' => Pass::SYSTEM_COMPLETED_ACTIVE
                ]);
            }
            $pass->update([
                'system_completed' => Pass::SYSTEM_COMPLETED_ACTIVE
            ]);
        }

        if ($passNewTotalTime < $minTimeYellow) {
            if ($pass->hasChild()) {
                $pass->child()->update([
                    'flagged_at' => null
                ]);
            }
            $pass->update([
                'flagged_at' => null
            ]);
        } else {
            if ($pass->hasChild()) {
                $pass->child()->update([
                    'flagged_at' => now()
                ]);
            }

            $pass->update([
                'flagged_at' => now()
            ]);
        }

        return new PassResource($pass->refresh());
    }
}
