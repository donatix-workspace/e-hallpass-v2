<?php

namespace App\Console\Commands\Migrations;

use App\Models\User;
use DB;
use Illuminate\Console\Command;

class MigratePinCommand extends Command
{
    protected $signature = 'migrate_sys:pin';

    protected $description = 'Migrate Pins from EhpV1 to EhpV2';

    public function handle()
    {
        $newDb = DB::connection('mysql');
        $oldDb = DB::connection('oldDB');

        $adultsPins = $oldDb->table('teacher_pin')
            ->get()
            ->toArray();
        $this->output->info('-- Migrating teacher pins --');
        $this->output->progressStart(count($adultsPins));


        $adultsPinsChunks = array_chunk($adultsPins, 1000);
        foreach ($adultsPinsChunks as $adultsPinChunk) {
            $userPinInsertable = [];
            foreach ($adultsPinChunk as $adultPin) {
                $oldSchool = $oldDb->table('school_schedule')
                    ->where('school_id', $adultPin->school_id)
                    ->first();
                if ($oldSchool) {
                    $newSchool = $newDb->table('schools')->where('old_id', $oldSchool->school_id)->first();
                    $oldUser = $oldDb->table('users')->where('user_id', $adultPin->teacher_id)
                        ->first();
                    if ($newSchool) {
                        $newUser = $newDb->table('users')->where('old_id', optional($oldUser)->user_id)
                            ->where('school_id', $newSchool->id)
                            ->first();

                        if ($newUser) {
                            $userPinInsertable[] = [
                                'school_id' => $newSchool->id,
                                'pinnable_type' => User::class,
                                'pinnable_id' => $newUser->id,
                                'pin' => $adultPin->pin,
                                'created_at' => now(),
                                'updated_at' => now()
                            ];
                        }
                    }
                }
            }

            $newDb->table('pins')->insert($userPinInsertable);
            $this->output->progressAdvance(count($userPinInsertable));
        }
        $this->output->progressFinish();
    }
}
