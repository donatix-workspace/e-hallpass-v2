<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class MigrateChangesFromEHPV1Command extends Command
{
    protected $signature = 'ehpv1:transfer';

    protected $description = 'Migrate latest data from EHPV1';

    /**
     * @return int
     */
    public function handle(): int
    {
        $this->info('Starting EHPV1 syncing.....');

        $this->call('migrate_sys:rooms', [
            '--offset' => DB::table('rooms')
                ->orderBy('old_id', 'desc')
                ->select('old_id')
                ->first()->old_id
        ]);

        DB::statement('call transferData();');

        $this->call('es:reindex', [
            '--indexes' =>
                'appointment_passes_index,location_capacities,pass_limits,polarities,room_restrictions,rooms_index,transparencies_index,unavailables,pass_block,polarity_reports,recurrence_appointment_passes,summary_report,contact_tracing',
            '--force' => ''
        ]);

        $this->info(
            'The procedure was runned and the reindex was finished. Synced from EHPv1'
        );

        return 0;
    }
}
