<?php

namespace App\Listeners;

use App\Events\PassInOut;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PassInOutNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PassInOut $event
     * @return void
     */
    public function handle(PassInOut $event)
    {
        //
    }
}
