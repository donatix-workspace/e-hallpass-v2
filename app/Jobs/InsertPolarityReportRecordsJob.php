<?php

namespace App\Jobs;

use App\Models\Polarity;
use App\Models\PolarityReport;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Throwable;

class InsertPolarityReportRecordsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $destination;
    private $polarities;
    private $user;
    private $fromApplication;

    public function __construct(
        $polarities,
        array $destination,
        $user,
        $fromApplication = null
    ) {
        $this->destination = $destination;
        $this->polarities = $polarities;
        $this->user = $user;
        $this->fromApplication = $fromApplication;
    }

    public function handle()
    {
        $polarities = $this->polarities;
        $destination = $this->destination;
        $user = $this->user;
        $agent = PolarityReport::WEB_AGENT;

        if ($this->fromApplication === 'Mobile') {
            $agent = PolarityReport::MOBILE_AGENT;
        }

        if ($this->fromApplication === 'Kiosk') {
            $agent = 'kiosk';
        }

        $polarityReports = [];

        foreach ($polarities as $polarity) {
            $polarityReports[] = [
                'destination_id' => $destination['id'],
                'destination_type' => $destination['type'],
                'user_id' => $user->id,
                'out_user_id' =>
                    $polarity->first_user_id === $user->id
                        ? $polarity->second_user_id
                        : $polarity->first_user_id,
                'prevented_at' => Carbon::now(),
                'prevented_from' => $agent,
                'polarity_id' => $polarity->id,
                'school_id' => $user->school_id,
                'created_at' => now(),
                'updated_at' => now()
            ];
        }
        try {
            DB::beginTransaction();

            // We use O(n) because of elastic indexing.
            foreach ($polarityReports as $polarityReport) {
                $polarityReportCreate = PolarityReport::create($polarityReport);
                Polarity::where(
                    'id',
                    $polarityReportCreate->polarity_id
                )->searchable();
            }

            DB::commit();
        } catch (Exception $e) {
            Log::error($e->getMessage());
        } catch (Throwable $e) {
            Log::error($e->getMessage());
        }
    }
}
