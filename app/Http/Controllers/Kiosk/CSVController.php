<?php

namespace App\Http\Controllers\Kiosk;

use App\Http\Controllers\Controller;
use App\Http\Requests\UploadKioskCSVFileRequest;
use App\Imports\KioskImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Response;

class CSVController extends Controller
{
    public function exportTemplate()
    {
        $headers = array(
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=kioskTemplate.csv',
            'Pragma' => 'no-cache',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Expires' => '0',
        );

        $columns = array('email', 'password');
        $callback = function () use ($columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            fclose($file);
        };

        return Response::stream($callback, 200, $headers);
    }

    public function uploadCSV(UploadKioskCSVFileRequest $request)
    {
        $storageTempPath = storage_path('/tmp/');
        $csvFile = $request->file('csv_file');
        $csvFileName = time() . $csvFile->getClientOriginalName();
        $storageCsvFile = $csvFile->move($storageTempPath, $csvFileName);
        $csvData = Excel::import(new KioskImport(), $storageCsvFile);

        return $csvData;
    }

    public function processCSV(Request $request)
    {
        $users = $request->input('users');
        $recordsProcessed = 0;
        foreach ($users as $user => $kioskPassword) {
            app('App\Modules\User\Controllers\UsersController')->addUpdateKuser($user, $kioskPassword);
            $recordsProcessed++;
        }

        return response()->json([
            'message' => __('kiosk.csv.import.records.processed') . $recordsProcessed,
            'status' => __('success')
        ]);
    }
}
