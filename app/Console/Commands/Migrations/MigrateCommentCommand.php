<?php

namespace App\Console\Commands\Migrations;

use App\Models\AppointmentPass;
use App\Models\Comment;
use DB;
use Illuminate\Console\Command;

class MigrateCommentCommand extends Command
{
    protected $signature = 'migrate_sys:comment';

    protected $description = 'Migrate comments from AppointmentPasses and Passes';

    public function handle()
    {
        $newDb = DB::connection('mysql');
        $oldDb = DB::connection('oldDB');

        $appointments = $newDb
            ->table('recurrence_appointment_passes')
            ->get(['id', 'reason'])
            ->toArray();
        $this->output->progressStart();

        $appointmentsCommentChunks = array_chunk($appointments, 1000);
        foreach ($appointmentsCommentChunks as $appointmentsCommentChunk) {
            $aptCommentsArray = [];
            foreach ($appointmentsCommentChunk as $commentChunk) {
                $comments = json_decode($commentChunk->reason, true);
                if ($comments) {
                    foreach ($comments as $comment) {
                        $newCommenterId = $newDb
                            ->table('users')
                            ->where('old_id', $comment['commenter_id'])
                            ->first();

                        if ($newCommenterId) {
                            $aptCommentsArray[] = [
                                'commentable_type' => AppointmentPass::class,
                                'commentable_id' => $commentChunk->id,
                                'comment' => $comment['comment'],
                                'permission' => $comment['adult'],
                                'user_id' => $newCommenterId->id,
                                'school_id' => $newCommenterId->school_id
                            ];
                        }
                    }
                }
            }
            $newDb->table('comments')->insert($aptCommentsArray);
            $this->output->progressAdvance(count($aptCommentsArray));
        }
        $this->output->progressFinish();
    }
}
