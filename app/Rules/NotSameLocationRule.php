<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NotSameLocationRule implements Rule
{
    public $fromId;
    public $fromType;
    public $toType;

    /**
     * @param string|null $fromType
     * @param int|null $fromId
     * @param string|null $toType
     */
    public function __construct(
        ?string $fromType,
        ?int $fromId,
        ?string $toType
    ) {
        $this->fromId = $fromId;
        $this->fromType = $fromType;
        $this->toType = $toType;
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if ($value === $this->fromId && $this->fromType === $this->toType) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return __('Start point and destination point should not be the same.');
    }
}
