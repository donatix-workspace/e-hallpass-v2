<?php

namespace App\Console\Commands;

use App\Models\Pass;
use App\Models\User;
use DB;
use Illuminate\Console\Command;

class FixUndefinedUsersOnPassesCommand extends Command
{
    protected $signature = 'passes:fixUndefined {--schoolId=}';

    protected $description = 'Fix undefined users on the passes records';

    /**
     * @var Pass $pass
     */
    public function handle()
    {
        $passesWithUndefined = Pass::where(function ($query) {
            $query
                ->where('from_type', User::class)
                ->where('from_id', 0)
                ->orWhere('to_type', User::class)
                ->where('to_id', 0);
        })
            ->when($this->option('schoolId') !== 'all', function ($query) {
                $query->where('school_id', $this->option('schoolId'));
            })
            ->where('type', Pass::STUDENT_CREATED_PASS)
            ->cursor();

        $count = $passesWithUndefined->count();

        $this->info('Fixing undefined passes. Count: ' . $count);

        $this->newLine();

        $this->output->progressStart($count);
        DB::disableQueryLog();
        DB::connection('oldDbPassHistory')->disableQueryLog();

        foreach ($passesWithUndefined as $pass) {
            if ($pass->from_id === 0) {
                $oldPass = DB::table('passhistory.epass_temp')
                    ->select('fromteacher_id')
                    ->where('pass_id', $pass->old_id)
                    ->first();

                if ($oldPass) {
                    $newUser = DB::table('schools_users')
                        ->select('user_id')
                        ->where('old_id', $oldPass->fromteacher_id)
                        ->first();

                    if ($newUser) {
                        $pass->update(['from_id' => $newUser->user_id]);
                    }
                }
            }

            if ($pass->to_id === 0) {
                $oldPass = DB::table('passhistory.epass_temp')
                    ->select('toteacher_id')
                    ->where('pass_id', $pass->old_id)
                    ->first();

                if ($oldPass) {
                    $newUser = DB::table('schools_users')
                        ->select('user_id')
                        ->where('old_id', $oldPass->toteacher_id)
                        ->first();

                    if ($newUser) {
                        $pass->update(['to_id' => $newUser->user_id]);
                    }
                }
            }

            $this->output->progressAdvance();
        }

        $this->output->progressFinish();

        $this->info('Finished...');
    }
}
