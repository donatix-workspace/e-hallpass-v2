<?php

namespace App\Console\Commands;

use App\Models\AppointmentPass;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AppointmentPassExpireCommand extends Command
{
    protected $signature = 'passes:appointment_expire';

    protected $description = 'Expire appointment passes.';

    public function handle()
    {
        $appointmentPasses = AppointmentPass::where('canceled_at', null)
            ->where('expired_at', null)
            ->get();

        foreach ($appointmentPasses as $appointmentPass) {
            if ($appointmentPass->pass_id !== null && $appointmentPass->for_date->isPast() && !$appointmentPass->pass->isCompleted()) {
                $appointmentPass->update(['expired_at' => Carbon::now()]);
                $appointmentPass->pass->update(['expired_at' => Carbon::now()]);
            }

            if (!$appointmentPass->isConfirmedByTeacher() && $appointmentPass->for_date->isPast()) {
                $appointmentPass->update(['expired_at' => Carbon::now()]);
            }
        }

    }
}
