SET FOREIGN_KEY_CHECKS =0;
INSERT INTO laraveldb.pass_limits (limitable_type, limitable_id, `limit`, from_date, to_date, grade_year, reason, recurrence_type, archived, created_at, updated_at, school_id) SELECT
                                                                                                                                                                                    (SELECT CASE
                                                                                                                                                                                                WHEN oldPl.type = 'school' THEN ('App\\Models\\School')
                                                                                                                                                                                                WHEN oldPl.type = 'student' THEN ('App\\Models\\User')
                                                                                                                                                                                                WHEN oldPl.type = 'gradyear' THEN ('App\\Models\\School')
                                                                                                                                                                                                END),
                                                                                                                                                                                    (SELECT CASE
                                                                                                                                                                                                WHEN oldPl.type = 'school' THEN (SELECT id FROM schools WHERE old_id=oldPl.school_id limit 1)
                                                                                                                                                                                                WHEN oldPl.type = 'student' THEN (SELECT id FROM users WHERE old_id=oldPl.student_id limit 1)
                                                                                                                                                                                                WHEN oldPl.type = 'gradyear' THEN (SELECT id FROM schools WHERE old_id=oldPl.school_id limit 1)
                                                                                                                                                                                                END),
                                                                                                                                                                                    (SELECT oldPl.limit),
                                                                                                                                                                                    (SELECT oldPl.`from`),
                                                                                                                                                                                    (SELECT oldPl.`to`),
                                                                                                                                                                                    (SELECT IF(oldPl.type='gradyear',JSON_ARRAY(oldPl.gradyear),null)),
                                                                                                                                                                                    (SELECT oldPl.reason),
                                                                                                                                                                                    (SELECT oldPl.`recursive`),
                                                                                                                                                                                    (SELECT 0),
                                                                                                                                                                                    (SELECT CONVERT_TZ(oldPl.created_at,
                                                                                                                                                                                                       (SELECT timezone from schools where old_id = oldPl.school_id limit 1),
                                                                                                                                                                                                       'UTC'
                                                                                                                                                                                                )) as created_at,
                                                                                                                                                                                    (SELECT NULL),
                                                                                                                                                                                    (SELECT id FROM schools WHERE old_id=oldPl.school_id limit 1)

FROM ehallpassDB.pass_limits as oldPl;
SET FOREIGN_KEY_CHECKS=1;
