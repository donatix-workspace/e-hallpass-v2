<?php

namespace Database\Factories;

use App\Models\AuthType;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName,
            'cus_uuid' => $this->faker->uuid,
            'last_name' => $this->faker->lastName,
            'role_id' => config('roles.admin'),
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'is_searchable' => 1,
            'auth_type' => AuthType::INHOUSESSO,
            'password' => bcrypt('secret2020'), // password
        ];
    }
}
