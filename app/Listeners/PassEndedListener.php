<?php

namespace App\Listeners;

use App\Events\DashboardRefreshEvent;
use App\Events\PassEnded;
use App\Models\Pass;
use App\Models\User;

class PassEndedListener
{
    /**
     * @param PassEnded $event
     */
    public function handle(PassEnded $event)
    {
        event(
            new DashboardRefreshEvent(
                $event->pass->school_id,
                User::find($event->pass->approved_by)
            )
        );
    }
}
