<?php

namespace App\Console\Commands\Migrations;

use App\Models\Period;
use App\Models\School;
use App\Models\TeacherPassSetting;
use DB;
use Illuminate\Console\Command;

class MigratePeriodsCommand extends Command
{
    protected $signature = 'migrate_sys:period';

    protected $description = 'Migrate Periods From EhpV1 to EhpV2';

    public function handle()
    {
        $this->info('--- Migrating Periods --- ');
        $oldDb = DB::connection('oldDB');
        $oldDb->disableQueryLog();
        DB::connection('mysql')->disableQueryLog();
        DB::connection()->disableQueryLog();

        $this->output->progressStart($oldDb->table('school_period')->count());

        $oldDb
            ->table('school_period')
            ->chunkById(150, function ($schoolPeriods) use ($oldDb) {
                $schoolPeriods->each(function ($schoolPeriod) use ($oldDb) {
                    $oldSchool = $oldDb
                        ->table('school_schedule')
                        ->where('school_id', $schoolPeriod->school_id)
                        ->first();
                    if ($oldSchool) {
                        $newSchool = School::where(
                            'old_id',
                            $oldSchool->school_id
                        )->first();
                        if ($newSchool) {
                            Period::create([
                                'school_id' => $newSchool->id,
                                'name' => $schoolPeriod->period_name,
                                'order' => $schoolPeriod->ordering,
                                'status' => $schoolPeriod->status
                            ]);
                        }

                        $this->output->progressAdvance();
                    }
                });
            });
        sleep(2);
        $this->output->progressFinish();
    }
}
