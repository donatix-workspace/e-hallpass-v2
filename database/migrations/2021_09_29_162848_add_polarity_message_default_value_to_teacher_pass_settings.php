<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPolarityMessageDefaultValueToTeacherPassSettings extends Migration
{
    public function up()
    {
        Schema::table('teacher_pass_settings', function (Blueprint $table) {
            $table->string('polarity_message')->default(__('passes.polarities.student'))
                ->change();
        });
    }

    public function down()
    {
        Schema::table('teacher_pass_settings', function (Blueprint $table) {
            $table->string('polarity_message')->change();
        });
    }
}
