<?php

namespace App\Elastic\Rules;

use ScoutElastic\SearchRule;

class DefaultSearchRule extends SearchRule
{
    /**
     * @inheritdoc
     */
    public function buildHighlightPayload()
    {
        //
    }

    /**
     * @inheritdoc
     */
    public function buildQueryPayload(): array
    {
        return [
            'must' => [
                'query_string' => [
                    'fields' => ['name'],
                    'query' => $this->builder->query
                ]
            ]
        ];
    }
}
