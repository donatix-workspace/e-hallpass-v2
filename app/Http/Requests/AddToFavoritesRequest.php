<?php

namespace App\Http\Requests;

use App\Rules\FavoriteRoomOrUserExistsRule;
use App\Rules\RoomOrUserStringRule;
use Illuminate\Foundation\Http\FormRequest;

class AddToFavoritesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'favorites' => ['array', 'present'],
            'favorites.*.id' => ['required', 'integer'],
            'favorites.*.type' => ['required'],
            'favorites.*.position' => ['required'],
        ];
    }
}
