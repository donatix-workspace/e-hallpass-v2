SET FOREIGN_KEY_CHECKS = 0;
START TRANSACTION ;
    INSERT INTO ehp_v2_prod.transparency_users (school_id, user_id, created_at, updated_at) SELECT
                                                                                                (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldPs.school_id limit 1),
                                                                                                IF((SELECT id FROM ehp_v2_prod.users WHERE old_id=oldPs.teacher_id limit 1) IS NOT NULL,(SELECT id FROM ehp_v2_prod.users WHERE old_id=oldPs.teacher_id limit 1),0),
                                                                                                (SELECT CONVERT_TZ(oldPs.updated_at,
                                                                                                                   (SELECT timezone from ehp_v2_prod.schools where old_id = oldPs.school_id limit 1),
                                                                                                                   'UTC'
                                                                                                            )),
                                                                                                (SELECT CONVERT_TZ(oldPs.created_at,
                                                                                                                   (SELECT timezone from ehp_v2_prod.schools where old_id = oldPs.school_id limit 1),
                                                                                                                   'UTC'
                                                                                                            ))
        FROM ehallpassDB.privacy_setting_teacher as oldPs;
COMMIT ;
SET FOREIGN_KEY_CHECKS = 1;
