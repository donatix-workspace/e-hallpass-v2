<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KioskAutoApproveRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'action' => 'min:1|max:2|numeric|required',
        ];
    }

    public function authorize(): bool
    {
        return auth()->check();
    }
}
