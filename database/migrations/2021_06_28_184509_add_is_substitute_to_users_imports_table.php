<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsSubstituteToUsersImportsTable extends Migration
{
    public function up()
    {
        Schema::table('users_imports', function (Blueprint $table) {
            $table->boolean('is_substitute')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users_imports', function (Blueprint $table) {
            $table->dropColumn('is_substitute');
        });
    }
}
