<?php


namespace App\Http\Services\CommonUserService\Translators;


use Alaouy\Youtube\Youtube;
use App\Http\Services\CommonUserService\Models\CommonDistrict;
use App\Http\Services\CommonUserService\Models\CommonTimezone;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class DocumentTranslator
{

    public static function collection($collection)
    {
        $data = [];
        foreach ($collection as $item) {

            $link = $item['link'];
            try {
                $key = Youtube::parseVidFromURL($item['link']);
                $link = "https://www.youtube.com/embed/{$key}";

            }catch (\Exception $e){

            }


            if ($item['status']) {
                $data[] = [
                    'topic' => $item['topic'],
                    'description' => $item['description'],
                    'link' => $link,
                ];
            }
        }
        return $data;

    }
}

