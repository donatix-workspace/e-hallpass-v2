<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class OnlyIntegersJsonValuesRule implements Rule
{
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $gradeYears = collect(json_decode($value, true))->toArray();
        foreach ($gradeYears as $gradeYear) {
            if (!is_numeric($gradeYear)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('only.integers.in.json.string');
    }
}
