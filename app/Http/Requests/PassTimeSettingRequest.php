<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PassTimeSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'white_passes_time' => 'required|date_format:H:i:s', // Default pass for approval
            'min_time' => 'required|date_format:H:i:s', // Long running passes
            'auto_expire_time' => 'required|date_format:H:i:s', // Auto system expire time
            'awaiting_apt_time' => 'required|date_format:H:i:s', // Apt passes time
            'nurse_expire_time' => 'required|date_format:H:i:s'
        ];
    }
}
