<?php

namespace App\Http\Controllers\Rooms;

use App\Http\Controllers\Controller;
use App\Imports\RoomSampleExport;
use App\Models\Room;
use Excel;
use App\Imports\RoomsExport;

class ExportController extends Controller
{
    /**
     * Download room sample
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function exportTemplate()
    {
        return Excel::download(new RoomSampleExport(), 'sample.csv');
    }

    /**
     * Download rooms csv
     *
     * @authenticated
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function export()
    {
        return Excel::download(new RoomsExport(), 'rooms.csv');
    }
}

