<?php

namespace App\Http\Controllers\Rooms;

use App\Http\Controllers\Controller;
use App\Http\Requests\AssignDeleteRequest;
use App\Http\Requests\AssignRequest;
use App\Http\Resources\AssignEntityCollection;
use App\Models\AutoPass;
use App\Models\AutoPassLimit;
use App\Models\Pin;
use App\Models\Room;
use App\Models\StaffSchedule;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * @group Room assignment
 */
class AssignController extends Controller
{
    /**
     * Get the assigned rooms
     *
     * @authenticated
     * @return AssignEntityCollection
     */
    public function index()
    {
        $user = auth()->user();

        $assignedRooms = StaffSchedule::fromSchoolId($user->school_id)
            ->where('user_id', $user->id)
            ->get();

        return new AssignEntityCollection([
            'assigned_rooms' => $assignedRooms
        ]);
    }

    /**
     * Assign a location to staff or admin
     *
     * @bodyParam room_ids_and_pins required json Example: "{3: "1234", "4": "2345"}"
     *
     * @authenticated
     * @param AssignRequest $request
     * @return JsonResponse
     */
    public function assign(AssignRequest $request): JsonResponse
    {
        $user = auth()->user();
        $roomsAndPins = $request->input('room_ids_and_pins');

        foreach ($roomsAndPins as $assignedRoom) {
            if ($assignedRoom['pin'] !== null) {
                $samePinRecord = Pin::fromSchoolId($user->school_id)
                    ->whereIn('pinnable_type', [Room::class, User::class])
                    ->where('pinnable_id', '!=', $assignedRoom['room_id'])
                    ->where('pin', $assignedRoom['pin'])
                    ->first();

                // Pin exists for another room from school
                if ($samePinRecord !== null) {
                    return response()->json(
                        [
                            'data' => ['id' => $assignedRoom['room_id']],
                            'message' => __(
                                'same.pin.already.exists.for.another.room'
                            ),
                            'status' => __('fail')
                        ],
                        422
                    );
                }
            }
        }

        StaffSchedule::where('user_id', $user->id)
            ->where('school_id', $user->school_id)
            ->delete();

        foreach ($roomsAndPins as $assignedRoom) {
            $assignUserToRoom = StaffSchedule::whereRoomId(
                $assignedRoom['room_id']
            )
                ->where('school_id', $user->school_id)
                ->firstOrCreate([
                    'user_id' => $user->id,
                    'room_id' => $assignedRoom['room_id'],
                    'school_id' => $user->school_id,
                    'status' => 1
                ]);

            $roomPinUpdate = Pin::fromType(Room::class)
                ->fromSchoolId($user->school_id)
                ->where('pinnable_id', $assignedRoom['room_id'])
                ->updateOrCreate(
                    [
                        'pinnable_type' => Room::class,
                        'pinnable_id' => $assignedRoom['room_id'],
                        'school_id' => $user->school_id
                    ],
                    ['pin' => $assignedRoom['pin']]
                );
        }

        return response()->json([
            'data' => [],
            'message' => __('assign.room.success'),
            'status' => __('success')
        ]);
    }

    /**
     * Delete assigned rooms
     *
     * @queryParam room_ids required int[] The id of the rooms
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(): JsonResponse
    {
        $roomsIds = request()->get('room_ids');
        $user = auth()->user();

        $roomExists = Room::whereIntegerInRaw('id', $roomsIds)
            ->where('school_id')
            ->exists();

        $staffSchedulers = StaffSchedule::whereIntegerInRaw(
            'room_id',
            $roomsIds
        )->where('school_id', $user->school_id);

        // If there's only one staff schedule, we remove the pin completely.
        if ($staffSchedulers->count() < 2) {
            Pin::fromSchoolId(auth()->user()->school_id)
                ->where('pinnable_type', Room::class)
                ->whereIntegerInRaw('pinnable_id', $roomsIds)
                ->delete();
        }

        $staffSchedulers->where('user_id', $user->id)->delete();

        return response()->json([
            'data' => [],
            'message' => __('delete.assign.room.success'),
            'status' => __('success')
        ]);
    }
}
