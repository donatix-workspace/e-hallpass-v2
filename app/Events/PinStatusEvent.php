<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;

class PinStatusEvent implements ShouldBroadcastNow
{
    use Dispatchable;
    private $userId;
    public $pinAttempts;
    public $showPinMessage;
    public $locked;

    /**
     * @param int $userId
     * @param int $pinAttempts
     * @param bool $showPinMessage
     * @param bool $locked
     */
    public function __construct(
        int $userId,
        int $pinAttempts,
        bool $showPinMessage,
        bool $locked
    ) {
        $this->userId = $userId;
        $this->pinAttempts = $pinAttempts;
        $this->showPinMessage = $showPinMessage;
        $this->locked = $locked;
    }

    public function broadcastOn(): PrivateChannel
    {
        return new PrivateChannel('user.passes.' . $this->userId);
    }
}
