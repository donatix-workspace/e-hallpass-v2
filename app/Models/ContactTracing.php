<?php

namespace App\Models;

use App\Elastic\ContactTracingIndexConfigurator;
use App\Traits\ContactTracingMapping;
use App\Traits\ExtendedSearchable;
use Illuminate\Database\Eloquent\Model;

class ContactTracing extends Model
{
    use ExtendedSearchable, ContactTracingMapping;

    protected $guarded = [];
    protected $table = 'contact_traces';

    protected $indexConfigurator = ContactTracingIndexConfigurator::class;

    /**
     * @return array
     */
    public function toSearchableArray(): array
    {
        $contactTracing = $this->toArray();

        $contactTracing['share_same_location'] = $contactTracing[
            'share_same_location'
        ]
            ? 'Y'
            : 'N';

        return $contactTracing;
    }
}
