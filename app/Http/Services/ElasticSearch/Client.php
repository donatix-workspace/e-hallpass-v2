<?php

namespace App\Http\Services\ElasticSearch;

use GuzzleHttp\Exception\GuzzleException;

class Client
{
    private $uri;
    private $body;

    /**
     * @param $body
     */
    public function __construct($body)
    {
        // Get the first available elastic search
        $this->uri = config('scout_elastic.client.hosts')[0];
        $this->body = $body;
    }

    /**
     * @param string $indexes
     * @param array|null $params
     * @param string $type
     * @return array|string|null
     * @throws \Exception
     */
    public function makeSearchRequest(string $indexes, ?array $params, string $type = 'search')
    {
        $http = new \GuzzleHttp\Client();
        $results = null;
        $uriParams = $params ? '?' . http_build_query($params) : null;

        try {
            $request = $http->get($this->uri . '/' . $indexes . '/_' . $type . $uriParams, [
                'body' => $this->body,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ]
            ]);
            $results = $request->getBody()
                ->getContents();
            $results = json_decode($results);

            if ($type === 'search') {
                $results = $results->hits->hits;
            }
        } catch (GuzzleException $e) {
            throw new \Exception($e->getMessage());
        }

        if ($results && $type === 'search') {
            $results = array_map(function ($item) {
                return $item->_source;
            }, $results);
        }

        if($type === 'count') {
            $results = $results->count;
        }

        return $results;
    }
}
