<?php

namespace App\Traits;

trait LocationCapacityMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'status' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
            ],
            'limit' => [
                'type' => 'long',
            ],
            'school_id' => [
                'type' => 'long'
            ],
            'room' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                    ]
                ]
            ],
            'user' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                    ],
                ]
            ]
        ]
    ];
}
