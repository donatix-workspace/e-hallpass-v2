<?php

namespace App\Jobs;

use App\Models\RoomRestriction;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RestrictionStudentsCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $roomId;
    public $type;
    public $user;
    public $selectedStudentIds;
    public $fromDate;
    public $toDate;

    public function __construct(
        $fromDate,
        $toDate,
        $roomId,
        $type,
        $user,
        $selectedStudentIds
    ) {
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
        $this->type = $type;
        $this->roomId = $roomId;
        $this->user = $user;
        $this->selectedStudentIds = $selectedStudentIds;
    }

    /**
     * @throws \Throwable
     */
    public function handle()
    {
        $fromDate = $this->fromDate;
        $toDate = $this->toDate;
        $type = $this->type;
        $roomId = $this->roomId;
        $user = $this->user;
        $now = now();
        $selectedStudentIds = $this->selectedStudentIds;

        $roomRestrictionData = [];

        $usersIds = User::whereIntegerInRaw('id', $selectedStudentIds)
            ->where('role_id', config('roles.student'))
            ->fromSchoolId($user->school_id)
            ->get('id');

        foreach ($usersIds as $userId) {
            $roomRestrictionData[] = [
                'room_id' => $roomId,
                'school_id' => $user->school_id,
                'user_id' => $userId->id,
                'from_date' => Carbon::parse($fromDate)->setTimezone(
                    config('app.timezone')
                ),
                'to_date' => Carbon::parse($toDate)->setTimezone(
                    config('app.timezone')
                ),
                'type' => $type,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        DB::beginTransaction();
        $roomRestriction = RoomRestriction::multipleInsertOrUpdate(
            $roomRestrictionData
        );

        RoomRestriction::where('school_id', $user->school_id)
            ->where('created_at', $now->toDateTimeString())
            ->searchable();
        DB::commit();
    }
}
