<?php

namespace Tests\Feature;

use App\Models\School;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PassTimeTest extends TestCase
{
    use RefreshDatabase;

    protected $adminUser;
    protected $school;

    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        // Create example school
        $this->school = School::factory()->createOne();

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);
    }

    /**
     * @test
     * An admin user can view pass time settings form entities
     * @return void
     */
    public function an_admin_user_can_view_pass_time_settings_form_entities()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.admin.passes.time'))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();
    }

    /**
     * @test
     * An admin user can update pass time settings
     * @return void
     */
    public function an_admin_user_can_update_pass_time_settings()
    {
        $passTimeSettings = TeacherPassSetting::create(['school_id' => $this->school->id]);

        $this->actingAs($this->adminUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->put(route('api.v5.admin.passes.time.update'), [
                'white_passes_time' => '00:10:00',
                'min_time' => '00:05:00',
                'auto_expire_time' => '00:30:00',
                'max_time' => '00:20:00',
                'awaiting_apt_time' => '00:15:00'
            ])
            ->assertOk()
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('teacher_pass_settings', ['school_id' => $this->school->id]);
    }
}
