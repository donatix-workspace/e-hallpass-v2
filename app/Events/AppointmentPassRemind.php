<?php

namespace App\Events;

use App\Models\AppointmentPass;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AppointmentPassRemind implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $appointmentPass;
    public $timeRemain;

    /**
     * Create a new event instance.
     *
     * @param $appointmentPass
     * @param $timeRemain
     */
    public function __construct($appointmentPass, $timeRemain)
    {
        $this->appointmentPass = $appointmentPass;
        $this->timeRemain = $timeRemain;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('appointments.passes.' . $this->appointmentPass->school_id),
            new PrivateChannel('user.appointments.passes.' . $this->appointmentPass->user_id),
        ];
    }

}
