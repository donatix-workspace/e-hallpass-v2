<?php

namespace App\Console\Commands\Rollback;

use DB;
use Illuminate\Console\Command;

class RollbackPeriodsCommand extends Command
{
    protected $signature = 'ehpv1_rollback:periods';

    protected $description = 'Rollback Periods from EhpV2 to EhpV1';

    public function handle()
    {
        $this->info('--- Starting RollBack Of Periods ---');

        $newDb = DB::connection();
        $oldDb = DB::connection('oldDB');

        $periods = $newDb->table('periods')
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();

        $this->output->progressStart(count($periods));

        $periodsChunks = array_chunk($periods, 1500);
        $now = now();

        foreach ($periodsChunks as $periodChunk) {
            $insertablePeriods = [];
            foreach ($periodChunk as $period) {
                $oldSchoolId = $newDb->table('schools')->where('school_id', $period->school_id)->select('old_id')->first();


                if ($oldSchoolId) {
                    $insertablePeriods[] = [
                        'school_id' => $oldSchoolId->old_id,
                        'period_name' => $period->name,
                        'ordering' => $period->order,
                        'status' => $period->status,
                        'created_at' => $now,
                        'updated_at' => $now
                    ];
                }
            }

            try {
                DB::beginTransaction();
                $oldDb->table('school_period')
                    ->insert($insertablePeriods);
                $this->output->progressAdvance(count($insertablePeriods));
                DB::commit();
            } catch (\Throwable $e) {
                $this->info($e->getMessage());
            }
        }

        $this->output->progressFinish();

        return 0;
    }
}
