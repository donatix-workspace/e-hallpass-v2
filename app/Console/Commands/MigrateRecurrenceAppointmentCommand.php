<?php

namespace App\Console\Commands;

use App\Models\Room;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class MigrateRecurrenceAppointmentCommand extends Command
{
    protected $signature = 'migrate_sys:recurrence_apt';

    protected $description = 'Migrate recurrence appointment passes';

    public function handle()
    {
        $oldDb = DB::connection('oldDB');
        $newDb = DB::connection('mysql');

        $oldRecApt = $oldDb
            ->table('rec_appointment_pass')
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();
        $this->output->info('-- Migrating Rec Apt --');
        $this->output->progressStart(count($oldRecApt));
        $oldRecAptChunks = array_chunk($oldRecApt, 1000);
        $weekNames = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ];

        $now = now();

        $oldDb->disableQueryLog();
        $newDb->disableQueryLog();
        foreach ($oldRecAptChunks as $oldRecAptChunk) {
            $insertableRecApt = [];
            foreach ($oldRecAptChunk as $oldRecApt) {
                $oldSchool = $oldDb
                    ->table('school_schedule')
                    ->where('school_id', $oldRecApt->school_id)
                    ->first();
                $monthlyWeeks = null;
                $toDate = null;
                $fromDate = null;
                $recurrenceWeeks = null;
                $newToRoomId = null;
                $newToTeacherId = null;
                $newFromTeacherId = null;
                $newFromRoomId = null;

                if ($oldSchool) {
                    $newSchool = $newDb
                        ->table('schools')
                        ->where('old_id', $oldSchool->school_id)
                        ->first();

                    $newCreatedBy = $newDb
                        ->table('users')
                        ->where('old_id', $oldRecApt->created_by)
                        ->first();

                    $newPeriod = $newDb
                        ->table('periods')
                        ->where('old_id', $oldRecApt->period_id)
                        ->where('school_id', $newSchool->id)
                        ->first();

                    if ($oldRecApt->from_teacherid > 0) {
                        $newFromTeacherId = $newDb
                            ->table('users')
                            ->where('old_id', $oldRecApt->from_teacherid)
                            ->first();
                    }

                    if ($oldRecApt->to_teacherid > 0) {
                        $newToTeacherId = $newDb
                            ->table('users')
                            ->where('old_id', $oldRecApt->to_teacherid)
                            ->first();
                    }

                    if ($oldRecApt->to_roomid > 0) {
                        $newToRoomId = $newDb
                            ->table('rooms')
                            ->where('old_id', $oldRecApt->to_roomid)
                            ->where('school_id', $newSchool->id)
                            ->first();
                    }

                    if ($oldRecApt->from_roomid > 0) {
                        $newFromRoomId = $newDb
                            ->table('rooms')
                            ->where('old_id', $oldRecApt->from_roomid)
                            ->where('school_id', $newSchool->id)
                            ->first();
                    }

                    $newStudentId = $newDb
                        ->table('users')
                        ->where('old_id', $oldRecApt->student_id)
                        ->first();

                    if ($newSchool && $newStudentId) {
                        $fromDate = Carbon::parse(
                            $oldRecApt->pass_date . ' ' . $oldRecApt->pass_time,
                            $newSchool->timezone
                        )->setTimezone(config('app.timezone'));
                        $toDate = Carbon::parse(
                            $oldRecApt->to_date . ' ' . $oldRecApt->pass_time,
                            $newSchool->timezone
                        )->setTimezone(config('app.timezone'));

                        if ($oldRecApt->recurrence === 'monthly') {
                            $monthlyWeeks = [
                                $fromDate->weekNumberInMonth => $fromDate->format(
                                    'l'
                                )
                            ];
                        }

                        if ($oldRecApt->recurrence === 'weekly') {
                            $oldRecWeekNumbers = explode(
                                ',',
                                $oldRecApt->week_days
                            );
                            $newDays = [];
                            foreach ($oldRecWeekNumbers as $weekNumber) {
                                if (array_key_exists($weekNumber, $weekNames)) {
                                    array_push(
                                        $newDays,
                                        $weekNames[$weekNumber]
                                    );
                                }
                            }

                            $recurrenceWeeks = [
                                'days' => $newDays
                            ];
                        }
                    }

                    if (
                        ($newFromTeacherId !== null ||
                            $newFromRoomId !== null) &&
                        ($newToTeacherId !== null || $newToRoomId !== null) &&
                        $newStudentId &&
                        $newCreatedBy
                    ) {
                        $insertableRecApt[] = [
                            'created_by' => $newCreatedBy->id,
                            'from_type' =>
                                $oldRecApt->from_teacherid > 0
                                    ? User::class
                                    : Room::class,
                            'from_id' =>
                                $oldRecApt->from_teacherid > 0
                                    ? $newFromTeacherId->id
                                    : $newFromRoomId->id,
                            'to_type' =>
                                $oldRecApt->to_teacherid > 0
                                    ? User::class
                                    : Room::class,
                            'to_id' =>
                                $oldRecApt->to_teacherid > 0
                                    ? $newToTeacherId->id
                                    : $newToRoomId->id,
                            'user_id' => $newStudentId->id,
                            'school_id' => $newSchool->id,
                            'for_date' => $fromDate,
                            'period_id' => optional($newPeriod)->id,
                            'recurrence_type' => $oldRecApt->recurrence,
                            'recurrence_end_at' => $toDate,
                            'recurrence_week' => collect(
                                $monthlyWeeks
                            )->toJson(),
                            'recurrence_days' => collect(
                                $recurrenceWeeks
                            )->toJson(),
                            'reason' => $oldRecApt->pass_note,
                            'expired_at' => null,
                            'created_at' => $now,
                            'updated_at' => $now
                        ];
                    }
                }
            }

            $this->output->progressAdvance(count($insertableRecApt));
            DB::table('recurrence_appointment_passes')->insert(
                $insertableRecApt
            );
        }

        $this->output->progressFinish();
    }
}
