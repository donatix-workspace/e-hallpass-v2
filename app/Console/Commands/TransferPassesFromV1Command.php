<?php

namespace App\Console\Commands;

use Artisan;
use DB;
use Illuminate\Console\Command;

class TransferPassesFromV1Command extends Command
{
    protected $signature = 'ehpv1:transferPasses';

    protected $description = 'Transfer existing passes from EhpV1 to EhpV2';

    /**
     * @return int
     */
    public function handle(): int
    {
        $lastOldId = DB::table('passes')
            ->select('old_id')
            ->orderBy('old_id', 'desc')
            ->first();

        $lastNewId = DB::table('passes')
            ->select('id')
            ->orderBy('id', 'desc')
            ->first();

        $this->info(
            'Starting import new passes from ID: ' . $lastOldId->old_id
        );

        DB::statement('call transferPassData()');

        $this->newLine();

        $this->info('Finished. Starting index import scripts. ');

        Artisan::call('es:reindex', [
            '--offset' => $lastNewId->id,
            '--indexes' => 'passes_index'
        ]);

        return 0;
    }
}
