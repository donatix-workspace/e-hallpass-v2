<?php

namespace App\Console\Commands\Migrations;

use App\Models\AppointmentPass;
use DB;
use Illuminate\Console\Command;

class MigratePolarityCommand extends Command
{
    protected $signature = 'migrate_sys:polarity';

    protected $description = 'Migrate Polarity from EHPv1 to EHPv2';

    public function handle()
    {
        $newDb = DB::connection('mysql');
        $oldDb = DB::connection('oldDB');

        $oldPolarity = $oldDb->table('ab_polarity')
            ->get()
            ->toArray();
        $this->output->progressStart(count($oldPolarity));


        $polaritiesChunks = array_chunk($oldPolarity, 1000);
        foreach ($polaritiesChunks as $polarityChunk) {
            $polaritiesInsert = [];
            foreach ($polarityChunk as $polarity) {
                $oldSchool = $oldDb->table('school_schedule')
                    ->where('school_id', $polarity->school_id)
                    ->first();
                if ($oldSchool) {
                    $newSchool = $newDb->table('schools')->where('old_id', $oldSchool->school_id)->first();
                    $oldFirstUser = $oldDb->table('users')->where('user_id', $polarity->first_user_id)
                        ->first();
                    $oldSecondUser = $oldDb->table('users')->where('user_id', $polarity->second_user_id)
                        ->first();

                    if ($newSchool) {
                        $newFirstUser = $newDb->table('users')->where('email', optional($oldFirstUser)->email)
                            ->where('school_id', $newSchool->id)
                            ->first();

                        $newSecondUser = $newDb->table('users')->where('email', optional($oldSecondUser)->email)
                            ->where('school_id', $newSchool->id)
                            ->first();

                        if ($newFirstUser && $newSecondUser) {
                            $polaritiesInsert[] = [
                                'first_user_id' => $newFirstUser->id,
                                'second_user_id' => $newSecondUser->id,
                                'school_id' => $newSchool->id,
                                'message' => $polarity->message_for_admin,
                                'status' => $polarity->status,
                                'created_at' => now(),
                                'updated_at' => now()
                            ];
                        }
                    }
                }
            }

            $newDb->table('polarities')->insert($polaritiesInsert);
            $this->output->progressAdvance(count($polaritiesInsert));
        }
        $this->output->progressFinish();
    }
}
