<?php

namespace App\Http\Resources;

use App\Models\School;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class PassResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (optional($this)->id === null) {
            return [];
        }

        //        $schoolTimezone = School::findCacheFirst(
        //            optional($this)->school_id
        //        )->getTimezone();

        $schoolTimezone = Cache::remember(
            'passes_school_timezone_' . $this->school_id,
            now()->addMinutes(5),
            function () {
                return DB::table('schools')
                    ->where('id', optional($this)->school_id)
                    ->select('timezone')
                    ->first()->timezone;
            }
        );

        return [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'school_id' => $this->school_id,
            'from_id' => $this->from_id,
            'from_type' => $this->from_type,
            'from_kiosk_id' => $this->from_kiosk_id,
            'to_kiosk_id' => $this->to_kiosk_id,
            'created_by_kiosk' =>
                $this->createdByKiosk !== null
                    ? [
                        'id' => optional($this->createdByKiosk)->id,
                        'user' =>
                            optional($this->createdByKiosk)->user !== null
                                ? [
                                    'id' => optional($this->createdByKiosk)
                                        ->user->id,
                                    'first_name' => optional(
                                        $this->createdByKiosk
                                    )->user->first_name,
                                    'last_name' => optional(
                                        $this->createdByKiosk
                                    )->user->last_name
                                ]
                                : null,
                        'room' =>
                            optional($this->createdByKiosk)->room !== null
                                ? [
                                    'id' => optional($this->createdByKiosk)
                                        ->room->id,
                                    'name' => optional($this->createdByKiosk)
                                        ->room->name
                                ]
                                : null
                    ]
                    : null,
            'to_kiosk' =>
                $this->toKiosk !== null
                    ? [
                        'id' => optional($this->toKiosk)->id,
                        'user' =>
                            optional($this->toKiosk)->user !== null
                                ? [
                                    'id' => optional($this->toKiosk)->user->id,
                                    'first_name' => optional($this->toKiosk)
                                        ->user->first_name,
                                    'last_name' => optional($this->toKiosk)
                                        ->user->last_name
                                ]
                                : null,
                        'room' =>
                            optional($this->toKiosk)->room !== null
                                ? [
                                    'id' => optional($this->toKiosk)->room->id,
                                    'name' => optional($this->toKiosk)->room
                                        ->name
                                ]
                                : null
                    ]
                    : null,
            'from_kiosk' =>
                $this->fromKiosk !== null
                    ? [
                        'id' => optional($this->fromKiosk)->id,
                        'user' =>
                            optional($this->fromKiosk)->user !== null
                                ? [
                                    'id' => optional($this->fromKiosk)->user
                                        ->id,
                                    'first_name' => optional($this->fromKiosk)
                                        ->user->first_name,
                                    'last_name' => optional($this->fromKiosk)
                                        ->user->last_name
                                ]
                                : null,
                        'room' =>
                            optional($this->fromKiosk)->room !== null
                                ? [
                                    'id' => optional($this->fromKiosk)->room
                                        ->id,
                                    'name' => optional($this->fromKiosk)->room
                                        ->name
                                ]
                                : null
                    ]
                    : null,
            'to_type' => $this->to_type,
            'to_id' => $this->to_id,
            'requested_by' => $this->requested_by,
            'approved_by' => $this->approved_by,
            'completed_by' => $this->completed_by,
            'created_by' => $this->created_by,
            'pass_status' => $this->pass_status,
            'approved_with' => $this->approved_with,
            'ended_with' => $this->ended_with,
            'approved_at' => School::convertTimeToCorrectTimezone(
                $this->approved_at,
                $schoolTimezone
            ),
            'completed_at' => School::convertTimeToCorrectTimezone(
                $this->completed_at,
                $schoolTimezone
            ),
            'requested_at' => School::convertTimeToCorrectTimezone(
                $this->requested_at,
                $schoolTimezone
            ),
            'arrived_at' => School::convertTimeToCorrectTimezone(
                $this->arrived_at,
                $schoolTimezone
            ),
            'type' => $this->type,
            'expired_at' => School::convertTimeToCorrectTimezone(
                $this->expired_at,
                $schoolTimezone
            ),
            'canceled_at' => School::convertTimeToCorrectTimezone(
                $this->canceled_at,
                $schoolTimezone
            ),
            'created_at' => School::convertTimeToCorrectTimezone(
                $this->created_at,
                $schoolTimezone
            ),
            'created_date' => $this->created_date,
            'updated_at' => School::convertTimeToCorrectTimezone(
                $this->updated_at,
                $schoolTimezone
            ),
            'edited_at' => School::convertTimeToCorrectTimezone(
                $this->edited_at,
                $schoolTimezone
            ),
            'system_completed' => $this->system_completed,
            'flagged_at' => School::convertTimeToCorrectTimezone(
                $this->flagged_at,
                $schoolTimezone
            ),
            'extended_at' => School::convertTimeToCorrectTimezone(
                $this->extended_at,
                $schoolTimezone
            ),
            'badge_flags' => $this->badge_flags,
            'flags' => $this->flags,
            'ended_at' => School::convertTimeToCorrectTimezone(
                $this->ended_at,
                $schoolTimezone
            ),
            'signatures' => $this->signatures,
            'total_time' => $this->total_time,
            'is_flagged' => $this->is_flagged,
            'expire_after' => $this->expire_after,
            'elastic_advanced_fields' => $this->elastic_advanced_fields,
            'user' => $this->user,
            'comments' => $this->comments,
            'latest_comment' => $this->latestComment,
            'created_by_user' =>
                $this->createdByUser !== null
                    ? [
                        'id' => optional($this->createdByUser)->id,
                        'first_name' => optional($this->createdByUser)
                            ->first_name,
                        'last_name' => optional($this->createdByUser)
                            ->last_name,
                        'name' => optional($this->createdByUser)->name,
                        'email' => optional($this->createdByUser)->email,
                        'comment_type' => optional($this->createdByUser)
                            ->comment_type,
                        'role_id' => optional($this->createdByUser)->role_id,
                        'role' => optional($this->createdByUser)->role
                    ]
                    : null,
            'approved_by_user' =>
                $this->approvedByUser !== null
                    ? [
                        'id' => optional($this->approvedByUser)->id,
                        'first_name' => optional($this->approvedByUser)
                            ->first_name,
                        'last_name' => optional($this->approvedByUser)
                            ->last_name,
                        'name' => optional($this->approvedByUser)->name,
                        'email' => optional($this->approvedByUser)->email,
                        'comment_type' => optional($this->approvedByUser)
                            ->comment_type,
                        'role_id' => optional($this->approvedByUser)->role_id,
                        'role' => optional($this->approvedByUser)->role
                    ]
                    : null,
            'completed_by_user' =>
                $this->completedByUser !== null
                    ? [
                        'id' => optional($this->completedByUser)->id,
                        'first_name' => optional($this->completedByUser)
                            ->first_name,
                        'last_name' => optional($this->completedByUser)
                            ->last_name,
                        'name' => optional($this->completedByUser)->name,
                        'email' => optional($this->completedByUser)->email,
                        'comment_type' => optional($this->completedByUser)
                            ->comment_type,
                        'role_id' => optional($this->completedByUser)->role_id,
                        'role' => optional($this->completedByUser)->role
                    ]
                    : null,
            'requested_by_user' =>
                $this->requestedByUser !== null
                    ? [
                        'id' => optional($this->requestedByUser)->id,
                        'first_name' => optional($this->requestedByUser)
                            ->first_name,
                        'last_name' => optional($this->requestedByUser)
                            ->last_name,
                        'name' => optional($this->requestedByUser)->name,
                        'email' => optional($this->requestedByUser)->email,
                        'comment_type' => optional($this->requestedByUser)
                            ->comment_type,
                        'role_id' => optional($this->requestedByUser)->role_id,
                        'role' => optional($this->requestedByUser)->role
                    ]
                    : null,
            'to' =>
                $this->to || $this->to_id === 0
                    ? [
                        'id' =>
                            optional($this->to)->id === null
                                ? 0
                                : $this->to->id,
                        'name' =>
                            optional($this->to)->name === null
                                ? config('roles.prefixes.system')
                                : $this->to->name,
                        'first_name' => optional($this->to)->first_name,
                        'last_name' => optional($this->to)->last_name,
                        'comment_type' => optional($this->to)->comment_type,
                        'trip_type' => optional($this->to)->trip_type,
                        'enable_appointment_passes' => optional($this->to)
                            ->enable_appointment_passes,
                        'user_ids_assigned' => optional($this->to)
                            ->user_ids_assigned
                    ]
                    : null,
            'from' =>
                $this->from || $this->from_id === 0
                    ? [
                        'id' =>
                            optional($this->from)->id === null
                                ? 0
                                : $this->from->id,
                        'name' =>
                            optional($this->from)->name === null
                                ? config('roles.prefixes.system')
                                : $this->from->name,
                        'first_name' => optional($this->from)->first_name,
                        'last_name' => optional($this->from)->last_name,
                        'comment_type' => optional($this->from)->comment_type,
                        'trip_type' => optional($this->from)->trip_type,
                        'enable_appointment_passes' => optional($this->from)
                            ->enable_appointment_passes,
                        'user_ids_assigned' => optional($this->from)
                            ->user_ids_assigned
                    ]
                    : null,
            'child' =>
                $this->child !== null
                    ? [
                        'id' => optional($this->child)->id,
                        'parent_id' => optional($this->child)->parent_id,
                        'school_id' => optional($this->child)->school_id,
                        'from_type' => optional($this->child)->from_type,
                        'to_type' => optional($this->child)->to_type,
                        'to_id' => optional($this->child)->to_id,
                        'to_kiosk' =>
                            optional($this->child)->toKiosk !== null
                                ? [
                                    'id' => optional($this->child->toKiosk)->id,
                                    'user' =>
                                        optional($this->child->toKiosk)
                                            ->user !== null
                                            ? [
                                                'id' => optional(
                                                    $this->child->toKiosk
                                                )->user->id,
                                                'first_name' => optional(
                                                    $this->child->toKiosk
                                                )->user->first_name,
                                                'last_name' => optional(
                                                    $this->child->toKiosk
                                                )->user->last_name
                                            ]
                                            : null,
                                    'room' =>
                                        optional($this->child->toKiosk)
                                            ->room !== null
                                            ? [
                                                'id' => optional(
                                                    $this->child->toKiosk
                                                )->room->id,
                                                'name' => optional(
                                                    $this->child->toKiosk
                                                )->room->name
                                            ]
                                            : null
                                ]
                                : null,
                        'from_kiosk' =>
                            optional($this->child)->fromKiosk !== null
                                ? [
                                    'id' => optional($this->child->fromKiosk)
                                        ->id,
                                    'user' =>
                                        optional($this->child->fromKiosk)
                                            ->user !== null
                                            ? [
                                                'id' => optional(
                                                    $this->child->fromKiosk
                                                )->user->id,
                                                'first_name' => optional(
                                                    $this->child->fromKiosk
                                                )->user->first_name,
                                                'last_name' => optional(
                                                    $this->child->fromKiosk
                                                )->user->last_name
                                            ]
                                            : null,
                                    'room' =>
                                        optional($this->child->fromKiosk)
                                            ->room !== null
                                            ? [
                                                'id' => optional(
                                                    $this->child->fromKiosk
                                                )->room->id,
                                                'name' => optional(
                                                    $this->child->fromKiosk
                                                )->room->name
                                            ]
                                            : null
                                ]
                                : null,
                        'requested_by' => optional($this->child)->requested_by,
                        'approved_by' => optional($this->child)->approved_by,
                        'completed_by' => optional($this->child)->completed_by,
                        'created_by' => optional($this->child)->created_by,
                        'pass_status' => optional($this->child)->pass_status,
                        'approved_with' => optional($this->child)
                            ->approved_with,
                        'ended_with' => optional($this->child)->ended_with,
                        'approved_at' => School::convertTimeToCorrectTimezone(
                            optional($this->child)->approved_at,
                            $schoolTimezone
                        ),
                        'completed_at' => School::convertTimeToCorrectTimezone(
                            optional($this->child)->completed_at,
                            $schoolTimezone
                        ),
                        'requested_at' => School::convertTimeToCorrectTimezone(
                            optional($this->child)->requested_at,
                            $schoolTimezone
                        ),
                        'arrived_at' => School::convertTimeToCorrectTimezone(
                            optional($this->child)->arrived_at,
                            $schoolTimezone
                        ),
                        'type' => optional($this->child)->type,
                        'expired_at' => School::convertTimeToCorrectTimezone(
                            optional($this->child)->expired_at,
                            $schoolTimezone
                        ),
                        'canceled_at' => School::convertTimeToCorrectTimezone(
                            optional($this->child)->canceled_at,
                            $schoolTimezone
                        ),
                        'created_at' => School::convertTimeToCorrectTimezone(
                            optional($this->child)->created_at,
                            $schoolTimezone
                        ),
                        'updated_at' => School::convertTimeToCorrectTimezone(
                            optional($this->child)->updated_at,
                            $schoolTimezone
                        ),
                        'edited_at' => School::convertTimeToCorrectTimezone(
                            optional($this->child)->edited_at,
                            $schoolTimezone
                        ),
                        'system_completed' => optional($this->child)
                            ->system_completed,
                        'flagged_at' => School::convertTimeToCorrectTimezone(
                            optional($this->child)->flagged_at,
                            $schoolTimezone
                        ),
                        'extended_at' => School::convertTimeToCorrectTimezone(
                            optional($this->child)->extended_at,
                            $schoolTimezone
                        ),
                        'total_time' => optional($this->child)->total_time,
                        'is_flagged' => optional($this->child)->is_flagged,
                        'comments' => optional($this->child)->comments,
                        'latest_comment' => optional($this->child)
                            ->latestComment,
                        'created_by_user' =>
                            optional(optional($this->child)->createdByUser) !==
                            null
                                ? [
                                    'id' => optional(
                                        optional($this->child)->createdByUser
                                    )->id,
                                    'first_name' => optional(
                                        optional($this->child)->createdByUser
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->child)->createdByUser
                                    )->last_name,
                                    'name' => optional(
                                        optional($this->child)->createdByUser
                                    )->name,
                                    'email' => optional(
                                        optional($this->child)->createdByUser
                                    )->email,
                                    'comment_type' => optional(
                                        optional($this->child)->createdByUser
                                    )->comment_type,
                                    'role_id' => optional(
                                        optional($this->child)->createdByUser
                                    )->role_id,
                                    'role' => optional(
                                        optional($this->child)->createdByUser
                                    )->role
                                ]
                                : null,
                        'approved_by_user' =>
                            optional(optional($this->child)->approvedByUser) !==
                            null
                                ? [
                                    'id' => optional(
                                        optional($this->child)->approvedByUser
                                    )->id,
                                    'first_name' => optional(
                                        optional($this->child)->approvedByUser
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->child)->approvedByUser
                                    )->last_name,
                                    'name' => optional(
                                        optional($this->child)->approvedByUser
                                    )->name,
                                    'email' => optional(
                                        optional($this->child)->approvedByUser
                                    )->email,
                                    'comment_type' => optional(
                                        optional($this->child)->approvedByUser
                                    )->comment_type,
                                    'role_id' => optional(
                                        optional($this->child)->approvedByUser
                                    )->role_id,
                                    'role' => optional(
                                        optional($this->child)->approvedByUser
                                    )->role
                                ]
                                : null,
                        'user' => $this->child->user,
                        'completed_by_user' =>
                            optional(
                                optional($this->child)->completedByUser
                            ) !== null
                                ? [
                                    'id' => optional(
                                        optional($this->child)->completedByUser
                                    )->id,
                                    'first_name' => optional(
                                        optional($this->child)->completedByUser
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->child)->completedByUser
                                    )->last_name,
                                    'name' => optional(
                                        optional($this->child)->completedByUser
                                    )->name,
                                    'email' => optional(
                                        optional($this->child)->completedByUser
                                    )->email,
                                    'comment_type' => optional(
                                        optional($this->child)->completedByUser
                                    )->comment_type,
                                    'role_id' => optional(
                                        optional($this->child)->completedByUser
                                    )->role_id,
                                    'role' => optional(
                                        optional($this->child)->completedByUser
                                    )->role
                                ]
                                : null,
                        'requested_by_user' =>
                            optional(
                                optional($this->child)->requestedByUser
                            ) !== null
                                ? [
                                    'id' => optional(
                                        optional($this->child)->requestedByUser
                                    )->id,
                                    'first_name' => optional(
                                        optional($this->child)->requestedByUser
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->child)->requestedByUser
                                    )->last_name,
                                    'name' => optional(
                                        optional($this->child)->requestedByUser
                                    )->name,
                                    'email' => optional(
                                        optional($this->child)->requestedByUser
                                    )->email,
                                    'comment_type' => optional(
                                        optional($this->child)->requestedByUser
                                    )->comment_type,
                                    'role_id' => optional(
                                        optional($this->child)->requestedByUser
                                    )->role_id,
                                    'role' => optional(
                                        optional($this->child)->requestedByUser
                                    )->role
                                ]
                                : null,
                        'to' =>
                            optional(optional($this->child)->to) ||
                            optional(optional($this->child)->to_id) === 0
                                ? [
                                    'id' =>
                                        optional(optional($this->child)->to)
                                            ->id === null
                                            ? 0
                                            : optional(
                                                optional($this->child)->to
                                            )->id,
                                    'name' =>
                                        optional(optional($this->child)->to)
                                            ->name === null
                                            ? config('roles.prefixes.system')
                                            : optional(
                                                optional($this->child)->to
                                            )->name,
                                    'first_name' => optional(
                                        optional($this->child)->to
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->child)->to
                                    )->last_name,
                                    'comment_type' => optional(
                                        optional($this->child)->to
                                    )->comment_type,
                                    'trip_type' => optional(
                                        optional($this->child)->to
                                    )->trip_type,
                                    'enable_appointment_passes' => optional(
                                        optional($this->child)->to
                                    )->enable_appointment_passes,
                                    'user_ids_assigned' => optional(
                                        optional($this->child)->to
                                    )->user_ids_assigned
                                ]
                                : null,
                        'from' =>
                            optional(optional($this->child)->from) ||
                            optional(optional($this->child)->from_id) === 0
                                ? [
                                    'id' =>
                                        optional(optional($this->child)->from)
                                            ->id === null
                                            ? 0
                                            : optional(
                                                optional($this->child)->from
                                            )->id,
                                    'name' =>
                                        optional(optional($this->child)->from)
                                            ->name === null
                                            ? config('roles.prefixes.system')
                                            : optional(
                                                optional($this->child)->from
                                            )->name,
                                    'first_name' => optional(
                                        optional($this->child)->from
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->child)->from
                                    )->last_name,
                                    'comment_type' => optional(
                                        optional($this->child)->from
                                    )->comment_type,
                                    'trip_type' => optional(
                                        optional($this->child)->from
                                    )->trip_type,
                                    'enable_appointment_passes' => optional(
                                        optional($this->child)->from
                                    )->enable_appointment_passes,
                                    'user_ids_assigned' => optional(
                                        optional($this->child)->from
                                    )->user_ids_assigned
                                ]
                                : null
                    ]
                    : null,
            'parent' =>
                $this->parent !== null
                    ? [
                        'id' => optional($this->parent)->id,
                        'parent_id' => optional($this->parent)->parent_id,
                        'school_id' => optional($this->parent)->school_id,
                        'from_type' => optional($this->parent)->from_type,
                        'to_type' => optional($this->parent)->to_type,
                        'to_id' => optional($this->parent)->to_id,
                        'requested_by' => optional($this->parent)->requested_by,
                        'approved_by' => optional($this->parent)->approved_by,
                        'completed_by' => optional($this->parent)->completed_by,
                        'created_by' => optional($this->parent)->created_by,
                        'pass_status' => optional($this->parent)->pass_status,
                        'approved_with' => optional($this->parent)
                            ->approved_with,
                        'ended_with' => optional($this->parent)->ended_with,
                        'approved_at' => School::convertTimeToCorrectTimezone(
                            optional($this->parent)->approved_at,
                            $schoolTimezone
                        ),
                        'completed_at' => School::convertTimeToCorrectTimezone(
                            optional($this->parent)->completed_at,
                            $schoolTimezone
                        ),
                        'requested_at' => School::convertTimeToCorrectTimezone(
                            optional($this->parent)->requested_at,
                            $schoolTimezone
                        ),
                        'arrived_at' => School::convertTimeToCorrectTimezone(
                            optional($this->parent)->arrived_at,
                            $schoolTimezone
                        ),
                        'type' => optional($this->parent)->type,
                        'expired_at' => School::convertTimeToCorrectTimezone(
                            optional($this->parent)->expired_at,
                            $schoolTimezone
                        ),
                        'canceled_at' => School::convertTimeToCorrectTimezone(
                            optional($this->parent)->canceled_at,
                            $schoolTimezone
                        ),
                        'created_at' => School::convertTimeToCorrectTimezone(
                            optional($this->parent)->created_at,
                            $schoolTimezone
                        ),
                        'updated_at' => School::convertTimeToCorrectTimezone(
                            optional($this->parent)->updated_at,
                            $schoolTimezone
                        ),
                        'edited_at' => School::convertTimeToCorrectTimezone(
                            optional($this->parent)->edited_at,
                            $schoolTimezone
                        ),
                        'system_completed' => optional($this->parent)
                            ->system_completed,
                        'flagged_at' => School::convertTimeToCorrectTimezone(
                            optional($this->parent)->flagged_at,
                            $schoolTimezone
                        ),
                        'extended_at' => School::convertTimeToCorrectTimezone(
                            optional($this->parent)->extended_at,
                            $schoolTimezone
                        ),
                        'total_time' => optional($this->parent)->total_time,
                        'is_flagged' => optional($this->parent)->is_flagged,
                        'comments' => optional($this->parent)->comments,
                        'latest_comment' => optional($this->parent)
                            ->latestComment,
                        'user' => $this->parent->user,
                        'created_by_user' =>
                            optional(optional($this->parent)->createdByUser) !==
                            null
                                ? [
                                    'id' => optional(
                                        optional($this->parent)->createdByUser
                                    )->id,
                                    'first_name' => optional(
                                        optional($this->parent)->createdByUser
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->parent)->createdByUser
                                    )->last_name,
                                    'name' => optional(
                                        optional($this->parent)->createdByUser
                                    )->name,
                                    'email' => optional(
                                        optional($this->parent)->createdByUser
                                    )->email,
                                    'comment_type' => optional(
                                        optional($this->parent)->createdByUser
                                    )->comment_type,
                                    'role_id' => optional(
                                        optional($this->parent)->createdByUser
                                    )->role_id,
                                    'role' => optional(
                                        optional($this->parent)->createdByUser
                                    )->role
                                ]
                                : null,
                        'approved_by_user' =>
                            optional(
                                optional($this->parent)->approvedByUser
                            ) !== null
                                ? [
                                    'id' => optional(
                                        optional($this->parent)->approvedByUser
                                    )->id,
                                    'first_name' => optional(
                                        optional($this->parent)->approvedByUser
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->parent)->approvedByUser
                                    )->last_name,
                                    'name' => optional(
                                        optional($this->parent)->approvedByUser
                                    )->name,
                                    'email' => optional(
                                        optional($this->parent)->approvedByUser
                                    )->email,
                                    'comment_type' => optional(
                                        optional($this->parent)->approvedByUser
                                    )->comment_type,
                                    'role_id' => optional(
                                        optional($this->parent)->approvedByUser
                                    )->role_id,
                                    'role' => optional(
                                        optional($this->parent)->approvedByUser
                                    )->role
                                ]
                                : null,
                        'completed_by_user' =>
                            optional(
                                optional($this->parent)->completedByUser
                            ) !== null
                                ? [
                                    'id' => optional(
                                        optional($this->parent)->completedByUser
                                    )->id,
                                    'first_name' => optional(
                                        optional($this->parent)->completedByUser
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->parent)->completedByUser
                                    )->last_name,
                                    'name' => optional(
                                        optional($this->parent)->completedByUser
                                    )->name,
                                    'email' => optional(
                                        optional($this->parent)->completedByUser
                                    )->email,
                                    'comment_type' => optional(
                                        optional($this->parent)->completedByUser
                                    )->comment_type,
                                    'role_id' => optional(
                                        optional($this->parent)->completedByUser
                                    )->role_id,
                                    'role' => optional(
                                        optional($this->parent)->completedByUser
                                    )->role
                                ]
                                : null,
                        'requested_by_user' =>
                            optional(
                                optional($this->parent)->requestedByUser
                            ) !== null
                                ? [
                                    'id' => optional(
                                        optional($this->parent)->requestedByUser
                                    )->id,
                                    'first_name' => optional(
                                        optional($this->parent)->requestedByUser
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->parent)->requestedByUser
                                    )->last_name,
                                    'name' => optional(
                                        optional($this->parent)->requestedByUser
                                    )->name,
                                    'email' => optional(
                                        optional($this->parent)->requestedByUser
                                    )->email,
                                    'comment_type' => optional(
                                        optional($this->parent)->requestedByUser
                                    )->comment_type,
                                    'role_id' => optional(
                                        optional($this->parent)->requestedByUser
                                    )->role_id,
                                    'role' => optional(
                                        optional($this->parent)->requestedByUser
                                    )->role
                                ]
                                : null,
                        'to' =>
                            optional(optional($this->parent)->to) ||
                            optional(optional($this->parent)->to_id === 0)
                                ? [
                                    'id' =>
                                        optional(optional($this->parent)->to)
                                            ->id === null
                                            ? 0
                                            : optional(
                                                optional($this->parent)->to
                                            )->id,
                                    'name' =>
                                        optional(optional($this->parent)->to)
                                            ->name === null
                                            ? config('roles.prefixes.system')
                                            : optional(
                                                optional($this->parent)->to
                                            )->name,
                                    'first_name' => optional(
                                        optional($this->parent)->to
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->parent)->to
                                    )->last_name,
                                    'comment_type' => optional(
                                        optional($this->parent)->to
                                    )->comment_type,
                                    'trip_type' => optional(
                                        optional($this->parent)->to
                                    )->trip_type,
                                    'enable_appointment_passes' => optional(
                                        optional($this->parent)->to
                                    )->enable_appointment_passes,
                                    'user_ids_assigned' => optional(
                                        optional($this->parent)->to
                                    )->user_ids_assigned
                                ]
                                : null,
                        'from' =>
                            optional(optional($this->parent)->from) ||
                            optional(optional($this->parent)->from_id) === 0
                                ? [
                                    'id' =>
                                        optional(optional($this->parent)->from)
                                            ->id === null
                                            ? 0
                                            : optional(
                                                optional($this->parent)->from
                                            )->id,
                                    'name' =>
                                        optional(optional($this->parent)->from)
                                            ->name === null
                                            ? config('roles.prefixes.system')
                                            : optional(
                                                optional($this->parent)->from
                                            )->name,
                                    'first_name' => optional(
                                        optional($this->parent)->from
                                    )->first_name,
                                    'last_name' => optional(
                                        optional($this->parent)->from
                                    )->last_name,
                                    'comment_type' => optional(
                                        optional($this->parent)->from
                                    )->comment_type,
                                    'trip_type' => optional(
                                        optional($this->parent)->from
                                    )->trip_type,
                                    'enable_appointment_passes' => optional(
                                        optional($this->parent)->from
                                    )->enable_appointment_passes,
                                    'user_ids_assigned' => optional(
                                        optional($this->parent)->from
                                    )->user_ids_assigned
                                ]
                                : null
                    ]
                    : null
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array|string[]
     */
    public function with($request)
    {
        return [
            'status' => __('success')
        ];
    }
}
