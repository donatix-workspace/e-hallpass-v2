<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCanceledAtToRecurrenceAppointmentPasses extends Migration
{
    public function up()
    {
        Schema::table('recurrence_appointment_passes', function (Blueprint $table) {
            $table->timestamp('canceled_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('recurrence_appointment_passes', function (Blueprint $table) {
            $table->dropColumn('canceled_at');
        });
    }
}
