<?php

namespace App\Console\Commands\Migrations;

use Illuminate\Console\Command;

class MigrateStaffScheduleCommand extends Command
{
    protected $signature = 'migrate_sys:staff_schedule';

    protected $description = 'Migrate assigned location from Ehpv1 to Ehpv2';

    public function handle()
    {
        $this->info('--- Migrating Staff Schedules ---');

        $newDb = \DB::connection('mysql');
        $oldDb = \DB::connection('oldDB');

        $oldDb->disableQueryLog();
        $newDb->disableQueryLog();

        $staffSchedules = $oldDb->table('staff_schedule')
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();

        $this->output->progressStart(count($staffSchedules));

        $staffSchedulesChunks = array_chunk($staffSchedules, 1500);

        foreach ($staffSchedulesChunks as $staffSchedulesChunk) {
            $insertStaffSchedulesChunk = [];
            foreach ($staffSchedulesChunk as $staffSchedule) {
                $oldSchool = $oldDb->table('school_schedule')->where('school_id', $staffSchedule->school_id)
                    ->first();

                if ($oldSchool) {
                    $newSchool = $newDb->table('schools')->select('id')->where('old_id', $oldSchool->school_id)->first();

                    $oldUser = $oldDb->table('users')
                        ->select('email')
                        ->where('user_id', $staffSchedule->staff_id)
                        ->first();

                    $newUser = $newDb->table('users')
                        ->select('id')
                        ->where('old_id', optional($oldUser)->user_id)
                        ->where('school_id', $newSchool->id)
                        ->first();

                    $oldRoom = $oldDb->table('rooms')
                        ->select('room_name')
                        ->where('room_id', $staffSchedule->room_id)
                        ->first();

                    $newRoom = $newDb->table('rooms')
                        ->where('old_id', optional($oldRoom)->old_id)
                        ->select('id')
                        ->where('school_id', $newSchool->id)
                        ->first();

                    if ($newRoom && $newUser) {
                        $insertStaffSchedulesChunk[] = [
                            'room_id' => $newRoom->id,
                            'user_id' => $newUser->id,
                            'school_id' => $newSchool->id,
                            'status' => $staffSchedule->status,
                            'created_at' => now(),
                            'updated_at' => now()
                        ];
                    }

                }
            }

            try {
                $newDb->table('staff_schedules')
                    ->insert($insertStaffSchedulesChunk);
                $this->output->progressAdvance(count($insertStaffSchedulesChunk));
            } catch (\Exception $e) {
                $this->info($e->getMessage());
            }
        }

        $this->output->progressFinish();
    }
}
