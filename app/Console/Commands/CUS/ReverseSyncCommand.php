<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Translators\SchoolTranslator;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ReverseSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:rsync {school?} {--uuidsync} {--create} {--force} {--adminUserEmail=}';

    protected $admin;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create school users but school uuid';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {



        if ($this->option('force') && $this->option('create')) {
            $schools = School::whereNotNull('cus_uuid');
            $this->admin = User::where('email', $this->option('adminUserEmail'))->first();
            $this->info('Sync Schools To CUS');

            foreach ($schools->get() as $school) {
                $this->createMissingUsers($school->load('users'), 'dispatch');
            }
            die;
        }
        if ($this->option('force') && $this->option('uuidsync')) {
            $schools = School::whereNotNull('cus_uuid');
            $this->info('Sync Users From CUS');

            foreach ($schools->get() as $school) {
                $this->info($school->name . ' - ' . $school->cus_uuid);
                $this->syncUuids($school);

            }
            die;
        }

        $school = School::with('users')->where('cus_uuid', $this->argument('school'))->first();

        if ($this->option('adminUserEmail')) {
            $this->admin = User::where('email', $this->option('adminUserEmail'))->first();
        } else {
            $this->admin = User::where('school_id', $school->id)->where('role_id', 2)->where('auth_type', 1001)->latest('created_at')->first();
        }


        if ($this->option('create') && !$this->option('force')) {
            $this->createMissingUsers($school, 'dispatchSync');
        }
        if ($this->option('uuidsync') && !$this->option('force')) {
            $this->syncUuids($school);
        }

    }

    public function syncUuids($school)
    {

        $cusSchool = CommonSchool::where('uuid', $school->cus_uuid)->first();
        
        $memberships = CommonMembership::where('common_bm_school_id', $cusSchool->id)
            ->where('auth_client_id', 2)
            ->get();



        if(!$memberships->isEmpty()){
            $this->output->progressStart($memberships->count());

            foreach ($memberships as $membership) {
                $commonUser = CommonUser::where('id', $membership->auth_user_id)->first();

                $user = User::where('email', $commonUser->email)->where('school_id', $school->id)->first();

                if ($user) {
                    $data = UserTranslator::translateDb($commonUser, $membership, $school);
                    $data['cus_uuid'] = $commonUser->uuid;
                    $user->update($data);
                    DB::table('schools_users')->updateOrInsert(
                        [
                            'school_id' => $school->id,
                            'user_id' => $user->id,
                        ],
                        [
                            'membership_uuid' => $membership->uuid,
                            'role_id' => CommonMembership::getRoleId($membership->common_bm_role_membership_id),
                        ]);
                }
                $this->output->progressAdvance();
            }
        }


    }

    public function createMissingUsers($school, $method)
    {

        $users = $school->users->where('auth_type', '!=', 1001);

        $this->info($school->name . ' - ' . $school->cus_uuid);
        $this->output->progressStart($users->count());

        foreach ($users as $user) {
            $hasCommonUser = CommonUser::where('email', $user->email)->exists();
            if (!$hasCommonUser) {
                CreateCommonUserJob::$method([
                    'user' => $user,
                    'authenticatedUser' => $this->admin,
                    'password' => 'secret2020',
                    'sendInvite' => false,
                    'skipDelete' => true
                ]);
            } else {
                UpdateCommonUserJob::$method($user, $this->admin);
            }
            $this->output->progressAdvance();

        }
    }


}
