<?php

namespace App\Http\Controllers\Users;

use App\Elastic\Rules\BelongsToSchoolSearchRule;
use App\Http\Controllers\Controller;
use App\Http\Filters\UserFilter;
use App\Http\Requests\BulkInviteRequest;
use App\Http\Requests\BulkTeacherDeleteRequest;
use App\Http\Requests\StoreTeacherRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Resources\ArchiveUsersCollection;
use App\Http\Resources\EditUserCollection;
use App\Http\Resources\SchoolCollection;
use App\Http\Resources\SchoolResource;
use App\Http\Resources\UnArchiveUsersCollection;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Http\Services\CommonUserService\AuthenticationService;
use App\Jobs\CancelAndUpdateAppointmentsAfterArchiveJob;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\SendBulkInviteJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\InviteUser;
use App\Models\KioskUser;
use App\Models\Module;
use App\Models\Pin;
use App\Models\Room;
use App\Models\School;
use App\Models\SchoolUser;
use App\Models\UnArchive;
use App\Models\User;
use App\Scopes\WithValidMembership;
use Cache;
use DB;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;

/**
 * @group User management
 */
class UserController extends Controller
{
    /**
     * User Login
     *
     * @bodyParam email string required The email of the user
     * @bodyParam password string required The Password of the user
     * @param UserLoginRequest $request
     * @return JsonResponse
     * @group User authentication
     */
    public function login(UserLoginRequest $request)
    {
        $userId = 0;
        if (!config('commonuserservice.enabled')) {
            if (auth('web')->attempt($request->validated())) {
                $userId = auth('web')->user()->id;
            }
        } else {
            $credentials = [
                'username' => strtolower(trim($request->email)),
                'password' => trim($request->password)
            ];
            $commonUserService = new AuthenticationService();
            $userId = $commonUserService->login($request, $credentials);
        }

        if ($userId) {
            $authenticatedUser = User::find($userId);
            $tokenCreation = $authenticatedUser->createToken('authToken');

            if ($authenticatedUser->invited_at != null) {
                $authenticatedUser->update([
                    'invited_at' => null,
                    'send_invite' => 0
                ]);
            }

            /**
             * If the request comes from mobile, then set the token mobile flag
             */
            if (
                $request->filled('from_mobile') &&
                filter_var(
                    $request->input('from_mobile'),
                    FILTER_VALIDATE_BOOLEAN
                )
            ) {
                DB::table('oauth_access_tokens')
                    ->where('id', $tokenCreation->token->id)
                    ->update([
                        'mobile' => true
                    ]);
            }

            return response()->json([
                'access_token' => $tokenCreation->accessToken,
                'user' => $authenticatedUser
            ]);
        }

        return response()->json(
            [
                'message' => __('auth.failed')
            ],
            422
        );
    }

    public function authcallback(Request $request)
    {
        //TODO if fails redirect
        $data = $request->validate([
            'code' => 'required|string',
            'redirectUri' => 'sometimes|string',
            'codeVerifier' => 'sometimes|string',
            'state' => 'required',
            'school' => 'sometimes|string'
        ]);

        $commonUserService = new AuthenticationService();

        $user = $commonUserService->callback($request);

        return $user;
    }

    /**
     * Logout from the current user session.
     *
     * @authenticated
     * @return JsonResponse
     */
    public function logout()
    {
        $user = auth()->user();
        //Forget token
        $user->update(['is_loggin' => 0]);
        $user->token()->delete();

        return response()->json([
            'data' => [],
            'message' => __('logout.success'),
            'status' => __('success')
        ]);
    }

    public function resetPassword(Request $request)
    {
        $userExist = User::whereEmail($request->input('email'))->exists();

        if (!$userExist) {
            return response()->json(
                [
                    'message' => __('The email can not be found in our system.')
                ],
                422
            );
        }

        $commonUserService = new AuthenticationService();

        $response = $commonUserService->passwordResetInitiate(
            $request->get('email')
        );

        return response()->json(
            [
                'message' => __('password.reset.mail.sent')
            ],
            $response
        );
    }

    /**
     * Login Via Oauth Provider
     *
     * @bodyParam code string required Code
     * @bodyParam redirectUri string required Redirect Uri
     * @bodyParam codeVerifier string Code verifier
     * @bodyParam state integer required Provider ID - SS0 - 1 | Google - 10 | Microsoft - 20 | ClassLink - 101 | Clever - 102 | GG4L - 103
     * @return JsonResponse
     * @group 3rd Party Logins
     */
    public function oauth(Request $request)
    {
        //logger('oAuth', [$request->all()]);
        $data = $request->validate([
            'code' => 'required|string',
            'redirectUri' => 'sometimes|string',
            'codeVerifier' => 'sometimes|string|nullable',
            'state' => 'required',
            'school' => 'sometimes|string'
        ]);
        $commonUserService = new AuthenticationService();
        if ($data['state'] == '1001' && isset($data['school'])) {
            $user = $commonUserService->hiddenLogin(
                $data['code'],
                $data['school']
            );
        } else {
            $user = User::find($commonUserService->callback($request));
        }
        if ($user) {
            $tokenCreation = $user->createToken('authToken');
            return response()->json([
                'access_token' => $tokenCreation->accessToken,
                'user' => $user
            ]);
        }
        return response()->json(
            [
                'message' => __('auth.failed')
            ],
            422
        );
    }

    /**
     * Get the authenticated user.
     *
     * @authenticated
     * @return UserResource
     */
    public function me(): UserResource
    {
        $user = auth()
            ->user()
            ->append([
                'is_available',
                'pin',
                'is_locked',
                'pin_attempts_count'
            ]);

        $now = now();
        $agent = new Agent();

        if ($user->is_loggin === 0) {
            if ($agent->isMobile()) {
                $user->update([
                    'is_loggin' => 1,
                    'web_last_login_at' => $now
                    //TODO: Do this when the app is done.
                ]);
            }

            if ($agent->isDesktop()) {
                $user->update([
                    'is_loggin' => 1,
                    'web_last_login_at' => $now
                ]);
            }
        }

        return new UserResource($user);
    }

    /**
     * Get available schools for the users.
     *
     * @apiResourceModel  App\\Models\\School
     * @apiResourceCollection  App\\Http\\Resources\\SchoolCollection
     *
     * @group User management
     * @return SchoolCollection
     */
    public function schools()
    {
        $userValidMembershipSchoolsIds = DB::table('schools_users')
            ->select('school_id')
            ->where('user_id', auth()->id())
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->whereNull('archived_at')
            ->get()
            ->pluck('school_id');

        $userSchools = School::whereIntegerInRaw(
            'id',
            $userValidMembershipSchoolsIds
        )->get();

        return new SchoolCollection($userSchools);
    }

    /**
     * Get Users
     *
     * @queryParam per_page integer How many records will be shown on rooms
     * @queryParam sort string Sort rooms by name 'asc', 'desc'
     * @queryParam search_query string Search by keyword (only for the names of the rooms).
     * @queryParam is_substitute boolean Get only substitutes.
     * @queryParam archived boolean Get only archived user
     * @queryParam without_paginate boolean Get users with certain columns without paginate.
     *
     * @return UserCollection
     * @group User management
     */
    public function index(UserFilter $filters)
    {
        if (request()->has('sort') || request()->has('search_query')) {
            $users = User::search(
                request()->input('search_query') === null
                    ? '__all'
                    : convertElasticQuery(request()->input('search_query'))
            )
                ->rule(BelongsToSchoolSearchRule::class)
                ->query(function ($query) {
                    $query
                        ->withoutGlobalScopes()
                        ->skipHidden()
                        ->skipSystem();
                })
                ->with(['kioskUsers', 'role', 'unArchive'])
                ->when(request()->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('id', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                    return $query->orderBy('id', 'asc');
                })
                //                ->when(request()->has('archived'), function ($query) {
                //                    $query->where(
                //                        'status',
                //                        filter_var(
                //                            request()->get('archived'),
                //                            FILTER_VALIDATE_BOOLEAN
                //                        )
                //                            ? User::INACTIVE
                //                            : User::ACTIVE
                //                    );
                //                })
                ->where('is_substitute', User::INACTIVE)
                ->paginate(User::recordsPerPage());
        } else {
            $users = User::onWriteConnection()
                ->with(['kioskUsers', 'role', 'unArchive'])
                ->withoutGlobalScopes()
                ->belongsToThisSchool()
                ->skipHidden()
                ->skipSystem()
                ->where('deleted_at', null)
                ->orderBy('created_at', 'desc')
                ->where('is_substitute', User::INACTIVE)
                ->filter($filters)
                ->paginate(User::recordsPerPage());
        }

        return new UserCollection($users);
    }

    /**
     * Get users global count statistics
     *
     * @authenticated
     * @return JsonResponse
     */
    public function statistics(): JsonResponse
    {
        // Get user count statistics.

        return response()->json([
            'data' => [
                'stats' => User::countStatistics()
            ],
            'status' => __('success')
        ]);
    }

    /**
     * Create a User
     *
     * @bodyParam role_id integer required Role of the user
     * @bodyParam first_name string required First name
     * @bodyParam last_name string required Last name
     * @bodyParam email string required Email
     * @bodyParam status boolean required Status
     * @bodyParam prevent_archive boolean required Prevent archive only if status is true
     * @bodyParam student_sis_id integer required Student sis ID required if role_id is 4
     * @bodyParam auth_type integer required Authentication type from 1 to 7
     * @bodyParam kiosk_password string sometimes Kiosk Password
     * @bodyParam clever_user_id string Clever User ID, required if auth type is 4
     * @bodyParam send_invite boolean Send Invite, required if auth type is 1
     *
     * @param StoreUserRequest $request
     * @return UserResource
     * @throws AuthorizationException
     * @group User management
     * @authenticated
     */
    public function store(StoreUserRequest $request)
    {
        $this->authorize('create', User::class);

        $validatedData = $request->validated();
        $school = School::where('id', auth()->user()->school_id)->first();
        $kiosk = $school->hasModule(Module::KIOSK);

        $plainPassword = $request->get('password', null);
        $password = Hash::make(trim($plainPassword));

        $user = User::where('email', $request->input('email'))->first();

        if (!$user) {
            $user = User::create([
                'cus_uuid' => null,
                'role_id' => $request->input('role_id'),
                'first_name' => $request->input('first_name'),
                'school_id' => $school->id,
                'last_name' => $request->input('last_name'),
                'email' => $request->input('email'),
                'grade_year' => $request->input('grade_year'),
                'status' => $request->input('status'),
                'student_sis_id' => '0', // Deprecated moved into membership
                'auth_type' => $request->input('auth_type'),
                'password' => $password,
                'user_initials' => $request->input('initials', null),
                'initial_password' => $plainPassword,
                'created_by' => auth()->user()->id
            ]);
        } else {
            $user->update([
                'role_id' => $request->input('role_id'),
                'first_name' => $request->input('first_name'),
                'school_id' => $school->id,
                'last_name' => $request->input('last_name'),
                'grade_year' => $request->input('grade_year'),
                'status' => $request->input('status'),
                'student_sis_id' => '0', // Deprecated (Moved into the membership)
                'auth_type' => $request->input('auth_type'),
                'password' => $password,
                'user_initials' => $request->input('initials', null),
                'initial_password' => $plainPassword,
                'created_by' => auth()->user()->id
            ]);
        }

        if ($user) {
            if (
                $request->has('kiosk_password') &&
                !empty($request->input('kiosk_password'))
            ) {
                $this->createOrUpdateKioskUser(
                    $user->id,
                    $request->kiosk_password,
                    'manual'
                );
            }

            //          Move this functionality to Devision
            //            if ($request->has('send_invite') && $request->input('send_invite')) {
            //                SendUserInviteJob::dispatch($validatedData, $user, $request->has('prevent_archive'))
            //                    ->delay(now()->addSeconds(10));
            //            }

            if (
                $request->input('prevent_archive') &&
                !$user->isArchivePrevented($school)
            ) {
                UnArchive::where('user_id', $user->id)->delete(); //TODO MAke it many to many
                UnArchive::create([
                    'user_id' => $user->id,
                    'school_id' => $school->id
                ]);
            }
            DB::table('schools_users')->updateOrInsert(
                [
                    'school_id' => $school->id,
                    'user_id' => $user->id
                ],
                [
                    'role_id' => $request->input('role_id'),
                    'student_sis_id' => $request->input('student_sis_id'),
                    'role_changed_at' => now()
                ]
            );

            CreateCommonUserJob::dispatchSync([
                'user' => $user,
                'authenticatedUser' => auth()->user(),
                'password' => trim($plainPassword),
                'sendInvite' =>
                    $request->has('send_invite') &&
                    $request->input('send_invite'),
                'school' => $school
            ]);
        }

        return new UserResource($user);
    }

    function createOrUpdateKioskUser(
        $userId,
        $kioskPassword,
        $created_by = 'csv'
    ): void {
        $kUsers = KioskUser::where('user_id', $userId)->count();
        if ($kUsers) {
            $kUsers = KioskUser::where('user_id', $userId)->update([
                'kpassword' => bcrypt($kioskPassword),
                'password_processed' => $created_by == 'csv' ? 0 : 1
            ]);
        } else {
            $kioskUser = KioskUser::create([
                'user_id' => $userId,
                'school_id' => auth()->user()->school_id,
                'kpassword' => bcrypt($kioskPassword),
                'password_processed' => $created_by == 'csv' ? 0 : 1
            ]);
        }
    }

    /**
     * Get edit entities for a user
     *
     * @param User $user
     * @return EditUserCollection
     * @throws AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('view', [User::class, $user]);

        $authType = 0; // Auth type

        $preventArchive = false;
        if (UnArchive::where('user_id', $user->id)->count()) {
            $preventArchive = true;
        }

        $data = [
            'user' => $user,
            'prevent_archive' => $preventArchive,
            'auth_type' => $authType
        ];

        return new EditUserCollection($data);
    }

    /**
     * Update a User
     *
     * @urlParam user integer required The id of the user.
     * @bodyParam role_id integer required Role of the user
     * @bodyParam first_name string required First name
     * @bodyParam last_name string required Last name
     * @bodyParam email string required Email
     * @bodyParam status boolean required Status
     * @bodyParam prevent_archive boolean required Prevent archive only if status is true
     * @bodyParam student_sis_id integer required Student sis ID required if role_id is 4
     * @bodyParam auth_type integer required Authentication type from 1 to 7
     * @bodyParam password string required Password, required if auth_type is 1
     * @bodyParam kiosk_password string sometimes Kiosk Password
     * @bodyParam clever_user_id string Clever User ID, required if auth type is 4
     * @bodyParam send_invite boolean Send Invite, required if auth type is 1
     *
     * @param StoreUserRequest $request
     * @param User $user
     * @return JsonResponse
     * @throws AuthorizationException
     * @group User management
     * @authenticated
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update', [User::class, $user]);
        $validatedData = $request->validated();

        if (
            isset($validatedData['auth_type']) &&
            $validatedData['auth_type'] == 1
        ) {
            $validatedData['password'] = Hash::make(trim($request->password));
        } else {
            $validatedData['password'] = '0';
        }

        //Removing send_invite field
        unset($validatedData['send_invite']);

        // Remove prevent_archiving element for update request.

        $preventArchive = $validatedData['prevent_archive'];

        unset($validatedData['prevent_archive']);
        unset($validatedData['kiosk_password']);

        $validatedData['user_initials'] = $validatedData['initials'] ?? null;
        unset($validatedData['initials']);

        $isArchiving = $request->input('status') === User::INACTIVE;

        $schoolRole = DB::table('schools_users')
            ->where('user_id', $user->id)
            ->where('school_id', auth()->user()->school_id)
            ->first();

        if ($schoolRole && $schoolRole->role_id != $validatedData['role_id']) {
            DB::table('schools_users')
                ->where('user_id', $user->id)
                ->where('school_id', auth()->user()->school_id)
                ->update([
                    'role_id' => $validatedData['role_id'],
                    'role_changed_at' => now()
                ]);
        }

        DB::table('schools_users')
            ->where('user_id', $user->id)
            ->where('school_id', auth()->user()->school_id)
            ->update(['student_sis_id' => $validatedData['student_sis_id']]);

        $stickyRole = SchoolUser::where('user_id', $user->id)
            ->where('school_id', auth()->user()->school_id)
            ->whereNotNull('role_changed_at')
            ->exists();
        // Unset role if the user is not in the same school.
        // to prevent role changing in other school
        if ($user->school_id !== auth()->user()->school_id) {
            unset($validatedData['role_id']);
        }

        if ($user->update($validatedData)) {
            if ($request->has('kiosk_password')) {
                $this->createOrUpdateKioskUser(
                    $user->id,
                    $request->kiosk_password,
                    'manual'
                );
            }
            UnArchive::where('user_id', $user->id)->delete();
            //            if ($request->has('prevent_archive')) {
            //                if ($request->input('prevent_archive')) {
            //                    UnArchive::create([
            //                        'user_id' => $user->id,
            //                        'school_id' => $user->school_id
            //                    ]);
            //                } else {
            //                    UnArchive::where('user_id', $user->id)
            //                        ->where('school_id', $user->school_id)
            //                        ->delete();
            //                }
            //            }

            if ($isArchiving) {
                $user->archive();

                CancelAndUpdateAppointmentsAfterArchiveJob::dispatch(
                    $user
                )->onQueue('appointments');
            }

            $password = trim($request->password);

            if ($password == '') {
                $password = null;
            }

            UpdateCommonUserJob::dispatchSync(
                $user,
                auth()->user(),
                $password,
                $stickyRole,
                $preventArchive
            );

            return response()->json([
                'message' => __('user.updated.successfully'),
                'status' => __('success')
            ]);
        }

        return response()->json([
            'message' => __('an.error.occurred'),
            'status' => __('fail')
        ]);
    }

    /**
     * Sending Invitation by Bulk
     *
     * @bodyParam users array required Array of user ids
     *
     * @authenticated
     * @param BulkInviteRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function bulkInvite(BulkInviteRequest $request): JsonResponse
    {
        $this->authorize('bulkInvite', User::class);

        $userIds = $request->input('users');
        $user = auth()->user();

        $users = User::whereIntegerInRaw('id', $userIds)->where(
            'school_id',
            $user->school_id
        );

        $users->update([
            'send_invite' => true,
            'invited_at' => null
        ]);

        SendBulkInviteJob::dispatch($users->get(), $user)->onQueue('high');

        return response()->json([
            'message' => __('user.invites.successfully'),
            'status' => __('success')
        ]); // The user was successfully created
    }

    /**
     * Update the user profile (Student).
     *
     * @bodyParam kiosk_password string The password of the student.
     * @apiResourceModel App\Models\User
     * @apiResource App\Http\Resources\UserResource
     *
     * @authenticated
     * @param UpdateStudentRequest $request
     * @return UserResource
     * @throws AuthorizationException
     */
    public function updateStudent(UpdateStudentRequest $request)
    {
        $user = auth()->user();

        $this->authorize('updateStudent', User::class);

        if ($user->school->hasModule('Kiosk')) {
            $this->createOrUpdateKioskUser(
                $user->id,
                $request->input('kiosk_password')
            );
        }

        return new UserResource($user);
    }

    public function recreate(User $user)
    {
        $this->authorize('update', [User::class, $user]);
        Artisan::call(
            "cus:sync --userEmail={$user->email} --entity=users --force"
        );
        $user->refresh();

        if (!$user->cus_uuid && $user->initial_password) {
            CreateCommonUserJob::dispatchSync([
                'user' => $user,
                'authenticatedUser' => auth()->user(),
                'password' => $user->initial_password ?? 'secret2020',
                'sendInvite' => false
            ]);
        }

        return new UserResource($user);
    }

    /**
     * User Archives
     *
     * @group User management
     * @return ArchiveUsersCollection
     *
     */
    public function archives(): ArchiveUsersCollection
    {
        $users = User::withoutGlobalScope(NotArchivedUser::class)
            ->with('role')
            ->where([
                ['status', '=', '0'],
                ['school_id', '=', auth()->user()->school_id]
            ])
            ->orderBy('created_at', 'DESC')
            ->get();
        $data = ['archived_users' => $users];

        return new ArchiveUsersCollection($data);
    }

    /**
     * UnArchive User
     *
     * @param User $user
     * @return UserResource
     * @throws \Exception
     *
     * @group User management
     */
    public function unArchive(User $user)
    {
        $this->authorize('archive', [User::class, $user]);

        \Illuminate\Support\Facades\DB::table('schools_users')
            ->where('user_id', $user->id)
            ->where('school_id', auth()->user()->school_id)
            ->update([
                'status' => User::ACTIVE,
                'archived_at' => null,
                'archived_by' => null
            ]);

        $deleteInvites = InviteUser::where('user_id', $user->id)->delete();

        $user->update(['status' => User::ACTIVE, 'archived_at' => null]);

        return new UserResource($user);
    }

    /**
     * UnArchive users in bulk
     *
     * @bodyParam users string Comma separated string of users
     * @param Request $request
     * @return UnArchiveUsersCollection|JsonResponse
     * @throws \Exception
     *
     * @group User management
     */
    public function unArchiveBulk(Request $request)
    {
        $userIds = explode(',', $request->input('users'));
        foreach ($userIds as $userId) {
            $user = User::withoutGlobalScopes()
                ->where('id', $userId)
                ->fromSchoolId(auth()->user()->school_id)
                ->first();
            if ($user === null) {
                return response()->json([
                    'data' => ['id' => $userId],
                    'message' => __('invalid.user.id.specified'),
                    'status' => __('fail')
                ]);
            }

            $user
                ->schoolUser()
                ->where('user_id', $user->id)
                ->where('school_id', auth()->user()->school_id)
                ->update([
                    'archived_at' => null,
                    'archived_by' => null
                ]);

            $user->searchable();

            $deleteInvites = InviteUser::where('user_id', $user->id)->delete();
        }
        $data = [
            'users' => $userIds
        ];
        return new UnArchiveUsersCollection($data);
    }

    /**
     * Archive selected user
     *
     * @urlParam user integer required The id of the user
     *
     * @apiResourceModel App\Models\User
     * @apiResource App\Http\Resources\UserResource
     * @param User $user
     * @authenticated
     * @group User management
     * @return UserResource|JsonResponse
     * @throws AuthorizationException
     */
    public function archive(User $user)
    {
        $this->authorize('archive', [User::class, $user]);

        //        $preventUserFromArchive = UnArchive::where('user_id', $user->id)
        //            ->whereSchoolId($user->school_id)
        //            ->exists();

        //        if (!$preventUserFromArchive) {
        $user->archive();

        return new UserResource($user);
        //        }

        //        return response()->json([
        //            'data' => ['id' => $user->id],
        //            'message' => __('user.cannot.be.archived'),
        //            'status' => __('fail')
        //        ]); // deprecated for issue EHP2-1898
    }

    /**
     * Archive bulk users
     *
     * @response status=201 scenario="Success archiving bulk of users" {
     *  "data" => [],
     *  "message" => "users.bulk.archive.success",
     *  "status" => "success"
     * }
     *
     * @bodyParam users string Comma separated string of users
     * @param Request $request
     * @return JsonResponse
     * @group User management
     */
    public function bulkArchive(Request $request)
    {
        $request->validate(['users' => 'string|required']);

        $userIds = explode(',', $request->input('users'));
        foreach ($userIds as $userId) {
            $user = User::withoutGlobalScopes()
                ->select('id', 'status', 'updated_at', 'school_id')
                ->where('id', $userId)
                ->first();

            $user->archive();

            CancelAndUpdateAppointmentsAfterArchiveJob::dispatch(
                $user
            )->onQueue('appointments');
        }

        return response()->json([
            'data' => [],
            'message' => __('users.bulk.archive.success'),
            'status' => __('success')
        ]);
    }

    /**
     * Create teacher
     *
     * @queryParam is_substitute boolean Creation for substitute teacher.
     * @bodyParam first_name string First name
     * @bodyParam last_name string Last name
     * @bodyParam user_initials string User initials
     * @bodyParam email email Email
     * @bodyParam pin string Pin
     * @param StoreTeacherRequest $request
     * @group User management
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function createTeacher(StoreTeacherRequest $request): JsonResponse
    {
        $user = auth()->user();
        $this->authorize('create', User::class);

        $validatedData = $request->validated();
        $isSubstitute = filter_var(
            $request->get('is_substitute'),
            FILTER_VALIDATE_BOOLEAN
        );

        $pin = $validatedData['pin'];
        unset($validatedData['pin']);
        $additionalData = [
            'password' => 0,
            'role_id' => config('roles.teacher'),
            'is_searchable' => 0,
            'school_id' => $user->school_id,
            'is_substitute' => $isSubstitute ? 1 : 0
        ];

        $teacherData = array_merge($validatedData, $additionalData);

        $samePinRecord = Pin::fromSchoolId($user->school_id)
            ->whereIn('pinnable_type', [Room::class, User::class])
            ->where('pin', $pin)
            ->first();

        // Pin exists for another room from school
        if ($samePinRecord !== null) {
            return response()->json(
                [
                    'data' => ['id' => $samePinRecord->pinnable_id],
                    'message' => __('same.pin.already.exists.for.another.room'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $teacher = User::create($teacherData);
        if ($teacher) {
            Pin::create([
                'pinnable_type' => User::class,
                'pinnable_id' => $teacher->id,
                'pin' => $pin,
                'school_id' => auth()->user()->school_id
            ]);

            DB::table('schools_users')->updateOrInsert(
                [
                    'school_id' => auth()->user()->school_id,
                    'user_id' => $teacher->id
                ],
                [
                    'role_id' => config('roles.teacher')
                ]
            );

            return response()->json([
                'message' => __('teacher.created.successfully'),
                'status' => __('success')
            ]);
        }

        return response()->json([
            'message' => __('an.error.occurred'),
            'status' => __('error')
        ]);
    }

    /**
     * Get Substitute Teachers
     * @return UserCollection
     */
    public function substituteTeacher(): UserCollection
    {
        if (request()->has('sort') || request()->has('search_query')) {
            $users = User::search(
                request()->input('search_query') === null
                    ? config('scout_elastic.all_records')
                    : convertElasticQuery(request()->input('search_query'))
            )
                ->query(function ($query) {
                    $query->withoutGlobalScopes();
                })
                ->with('pin')
                ->when(request()->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('id', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                    return $query->orderBy('id', 'asc');
                })
                ->where('role_id', config('roles.teacher'))
                ->where('school_id', auth()->user()->school_id)
                ->where('is_substitute', User::ACTIVE)
                ->paginate(User::recordsPerPage());
        } else {
            $users = User::where('school_id', '=', auth()->user()->school_id)
                ->with('pin')
                ->where('role_id', config('roles.teacher'))
                ->where('is_substitute', User::ACTIVE)
                ->orderBy('created_at', 'DESC')
                ->paginate(config('pagination.default_pagination'));
        }

        return new UserCollection($users);
    }

    /**
     * Update a substitute user
     *
     * @bodyParam first_name string First name
     * @bodyParam last_name string Last name
     * @bodyParam user_initials string User initials
     * @bodyParam email email Email
     * @bodyParam pin string Pin
     *
     * @apiResourceModel App\Models\User
     * @apiResource App\Http\Resources\UserResource
     *
     * @param User $user
     * @param StoreTeacherRequest $request
     * @return UserResource|JsonResponse
     * @throws AuthorizationException
     */
    public function updateSubstituteTeacher(
        User $user,
        StoreTeacherRequest $request
    ) {
        $this->authorize('update', [User::class, $user]);

        if (!$user->isSubstitute()) {
            return response()->json([
                'data' => ['user_id' => $user->id],
                'message' => __('user.not.substitute'),
                'status' => __('fail')
            ]);
        }

        $samePinRecord = Pin::fromSchoolId($user->school_id)
            ->whereIn('pinnable_type', [Room::class, User::class])
            ->where('pinnable_id', '!=', $user->id)
            ->where('pin', $request->input('pin'))
            ->first();

        // Pin exists for another room from school
        if ($samePinRecord !== null) {
            return response()->json(
                [
                    'data' => ['id' => $samePinRecord->pinnable_id],
                    'message' => __('same.pin.already.exists.for.another.room'),
                    'status' => __('fail')
                ],
                422
            );
        }

        // Update the user data
        $user->update([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'user_initials' => $request->input('user_initials'),
            'email' => $request->input('email'),
            'status' => $request->input('status')
        ]);

        $user->pins()->delete();

        $user->pins()->create([
            'pin' => $request->input('pin'),
            'school_id' => $user->school_id
        ]);

        return new UserResource($user->refresh());
    }

    /**
     * Delete Substitute Teacher
     *
     * @param User $user
     * @return JsonResponse
     * @group User management
     * @throws AuthorizationException
     */
    public function deleteSubstituteTeacher(User $user)
    {
        $this->authorize('update', [User::class, $user]);

        if (!$user->isSubstitute()) {
            return response()->json([
                'data' => ['user_id' => $user->id],
                'message' => __('user.not.substitute'),
                'status' => __('fail')
            ]);
        }

        $user->update(['status' => User::INACTIVE]);

        return response()->json([
            'message' => __('teacher.deleted.successfully'),
            'status' => __('success')
        ]);
    }

    /**
     * Activate Substitute Teacher
     *
     * @param User $user
     * @return JsonResponse
     * @group User management
     * @throws AuthorizationException
     */
    public function activateSubstituteTeacher(User $user): JsonResponse
    {
        $this->authorize('update', [User::class, $user]);

        if (!$user->isSubstitute()) {
            return response()->json([
                'data' => ['user_id' => $user->id],
                'message' => __('user.not.substitute'),
                'status' => __('fail')
            ]);
        }

        $user->update(['status' => User::ACTIVE]);

        return response()->json([
            'message' => __('teacher.activated.successfully'),
            'status' => __('success')
        ]);
    }

    /**
     * Bulk Delete Substitute Teachers
     * @bodyParam teacher_ids string Teachers ids separated by comma
     * @param BulkTeacherDeleteRequest $request
     * @return JsonResponse
     * @group User management
     */
    public function bulkDeleteSubstituteTeacher(
        BulkTeacherDeleteRequest $request
    ) {
        $validatedData = $request->validated();
        $substituteTeachers = explode(',', $validatedData['teacher_ids']);
        foreach ($substituteTeachers as $substituteTeacherId) {
            $teacher = User::where('id', $substituteTeacherId)
                ->where('school_id', auth()->user()->school_id)
                ->where('is_substitute', 1)
                ->first();

            if ($teacher === null) {
                return response()->json([
                    'data' => ['id' => $substituteTeacherId],
                    'message' => __('invalid.user.id.specified'),
                    'status' => __('fail')
                ]);
            }

            $teacher->update(['status' => 0]);
        }

        return response()->json([
            'message' => __('teachers.deleted.successfully'),
            'status' => __('success')
        ]);
    }
}
