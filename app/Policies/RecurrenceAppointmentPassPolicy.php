<?php

namespace App\Policies;

use App\Models\RecurrenceAppointmentPass;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RecurrenceAppointmentPassPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view any recurrence appointment passes.
     *
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return !$user->isStudent();
    }

    /**
     * Determine whether the user can view the recurrence appointment pass.
     *
     * @param User $user
     * @param RecurrenceAppointmentPass $recurrenceAppointmentPass
     * @return bool
     */
    public function view(User $user, RecurrenceAppointmentPass $recurrenceAppointmentPass)
    {
        return !$user->isStudent() && $recurrenceAppointmentPass->school_id === $user->school_id;
    }

    /**
     * Determine whether the user can create recurrence appointment passes.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the recurrence appointment pass.
     *
     * @param User $user
     * @param RecurrenceAppointmentPass $recurrenceAppointmentPass
     * @return bool
     */
    public function update(User $user, RecurrenceAppointmentPass $recurrenceAppointmentPass)
    {
        return !$user->isStudent() && $recurrenceAppointmentPass->school_id === $user->school_id;
    }

    /**
     * Determine whether the user can delete the recurrence appointment pass.
     *
     * @param User $user
     * @param RecurrenceAppointmentPass $recurrenceAppointmentPass
     * @return bool
     */
    public function delete(User $user, RecurrenceAppointmentPass $recurrenceAppointmentPass)
    {
        return !$user->isStudent() && $user->school_id === $recurrenceAppointmentPass->school_id;
    }

    /**
     * Determine whether the user can restore the recurrence appointment pass.
     *
     * @param User $user
     * @param RecurrenceAppointmentPass $recurrenceAppointmentPass
     * @return bool
     */
    public function restore(User $user, RecurrenceAppointmentPass $recurrenceAppointmentPass)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the recurrence appointment pass.
     *
     * @param User $user
     * @param RecurrenceAppointmentPass $recurrenceAppointmentPass
     * @return bool
     */
    public function forceDelete(User $user, RecurrenceAppointmentPass $recurrenceAppointmentPass)
    {
        //
    }
}
