<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @group User Favorites (Students).
 */
class StudentFavorite extends Model
{
    use HasFactory;

    public const DESTINATION = 'destination';
    public const DEPARTING = 'departing';

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function favorable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public static function isGlobal($schoolGlobals,$id,$type)
    {
        return !!$schoolGlobals->where('favorable_type',$type)->where('favorable_id',$id)->count();
    }
}
