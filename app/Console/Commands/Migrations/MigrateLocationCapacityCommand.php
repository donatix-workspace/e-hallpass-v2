<?php

namespace App\Console\Commands\Migrations;

use App\Models\School;
use DB;
use Illuminate\Console\Command;

class MigrateLocationCapacityCommand extends Command
{
    protected $signature = 'migrate_sys:location_cap';

    protected $description = 'Migrate location capacities from ehpv1 to ehpv2';

    public function handle()
    {
        $this->info('--- Importing Location Capacities --- ');
        $this->output->progressStart(
            DB::connection('oldDB')
                ->table('location_max_cap')
                ->count()
        );

        $locationMaxCapacity = DB::connection('oldDB')
            ->table('location_max_cap')
            ->orderBy('id', 'desc')
            ->get()
            ->toArray();

        $locationMaxCapacitiesChunk = array_chunk($locationMaxCapacity, 1000);

        foreach ($locationMaxCapacitiesChunk as $locationMaxCapacity) {
            $locationCapacityInsertable = [];
            foreach ($locationMaxCapacity as $capacity) {
                $oldSchool = DB::connection('oldDB')
                    ->table('school_schedule')
                    ->where('school_id', $capacity->school_id)
                    ->first();
                if ($oldSchool) {
                    $newSchool = School::where(
                        'old_id',
                        $oldSchool->school_id
                    )->first();
                    if ($newSchool) {
                        $oldRoom = DB::connection('oldDB')
                            ->table('rooms')
                            ->where('room_id', optional($capacity)->room_id)
                            ->first();
                        $newRoom = DB::connection('mysql')
                            ->table('rooms')
                            ->where('old_id', optional($oldRoom)->room_id)
                            ->where('school_id', $newSchool->id)
                            ->first();

                        if ($newRoom) {
                            $locationCapacityInsertable[] = [
                                'room_id' => $newRoom->id,
                                'school_id' => $newSchool->id,
                                'limit' => $capacity->limit,
                                'status' => $capacity->status
                            ];
                        }
                    }
                }
            }

            DB::table('location_capacities')->insert(
                $locationCapacityInsertable
            );
            $this->output->progressAdvance(count($locationCapacityInsertable));
        }

        sleep(2);
        $this->output->progressFinish();
    }
}
