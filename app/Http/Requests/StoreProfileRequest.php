<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && !auth()->user()->isStudent();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'sometimes|file|mimes:jpeg,jpg,png,gif|max:2048|nullable',
            'initials' => 'sometimes|string|nullable',
            'first_name' => 'required|string|max:35|min:2',
            'last_name' => 'required|string|max:35|min:2',
            'is_searchable' => 'required|boolean',
            'teacher_pin' => 'sometimes|string|min:4|max:6|regex:/^[a-zA-Z0-9!@#$&()\\-`.+,\/\"?=_<>}{]*$/',
            'password' => 'sometimes|string|min:8',
            'allow_appointment_requests' => 'required',
            'allow_passes_to_me' => 'required',
        ];
    }
}
