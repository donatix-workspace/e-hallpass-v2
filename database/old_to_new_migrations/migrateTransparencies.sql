SET FOREIGN_KEY_CHECKS = 0;
START TRANSACTION ;
TRUNCATE ehp_v2_prod.transparencies;
INSERT INTO ehp_v2_prod.transparencies (school_id, created_at, updated_at) SELECT
    (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldPs.school_id limit 1),
    (SELECT CONVERT_TZ(oldPs.updated_at,
                       (SELECT timezone from ehp_v2_prod.schools where old_id = oldPs.school_id limit 1),
                       'UTC'
                )),
    (SELECT CONVERT_TZ(oldPs.created_at,
                       (SELECT timezone from ehp_v2_prod.schools where old_id = oldPs.school_id limit 1),
                       'UTC'
                ))
FROM ehallpassDB.privacy_setting as oldPs;
COMMIT ;
SET FOREIGN_KEY_CHECKS = 1;
