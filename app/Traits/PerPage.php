<?php

namespace App\Traits;

trait PerPage
{
    /**
     * @return int
     */
    public static function recordsPerPage(): int
    {
        $perPage = request('per_page', config('pagination.default_pagination'));

        if($perPage > config('pagination.max_per_page')) {
            return abs(config('pagination.max_per_page'));
        }

        return abs((int)$perPage);
    }
}
