<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSystemCompletedIndexToPassesTable extends Migration
{
    public function up()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->index('system_completed');
        });
    }

    public function down()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->dropIndex('passes_system_completed_index');
        });
    }
}
