<?php

namespace App\Imports;

use App\Models\PassLimit;
use App\Models\Polarity;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PolaritiesImport implements FromQuery, WithHeadings, WithMapping
{
    private $polarityQuery;
    public $detailedReport = false;

    /**
     *
     */
    public function __construct($polarityQuery, bool $detailedReport = false)
    {
        $this->polarityQuery = $polarityQuery;
        $this->detailedReport = $detailedReport;
    }

    /**
     * @return PassLimit|\Illuminate\Database\Eloquent\Builder|Builder
     */
    public function query()
    {
        return $this->polarityQuery;
    }

    public function headings(): array
    {
        if ($this->detailedReport) {
            return [
                'Student Prevented',
                'Destination',
                'Student on active Pass',
                'Attempt from',
                'Date',
                'Prevented Time'
            ];
        }
        return ['Student A', 'Student B', 'Times Triggered', 'Message'];
    }

    public function map($row): array
    {
        if ($this->detailedReport) {
            return [
                $row->user->first_name . ' ' . $row->user->last_name,
                $row->destination_type === User::class
                    ? $row->destination->first_name .
                        ' ' .
                        $row->destination->last_name
                    : $row->destination->name,
                $row->outUser->name,
                $row->prevented_from,
                Carbon::parse($row->prevented_at)->format('m/d/Y'),
                Carbon::parse($row->prevented_at)->format('g:i A')
            ];
        }

        return [
            $row->firstUser->first_name . ' ' . $row->firstUser->last_name,
            $row->secondUser->first_name . ' ' . $row->secondUser->last_name,
            $row->times_triggered,
            $row->message
        ];
    }
}
