<?php
namespace Tests\TestFactories;
/**
 * Interface TestFactoriesInterface
 * Just added interface for our test factories
 * for more easily adaptation between devs
 */
interface TestFactoriesInterface {
    public static function createOne(array $attributes = []);
}
