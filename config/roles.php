<?php
// EHP User roles
return [
    'admin' => 2,
    'teacher' => 3,
    'student' => 1,
    'staff' => 4,
    'superadmin' => 5,
    'system' => 0,
    'prefixes' => [
        'system' => 'System'
    ]
];
