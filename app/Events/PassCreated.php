<?php

namespace App\Events;

use App\Models\Pass;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PassCreated implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $pass;
    public $notifyStudent;
    public $notifyAdultsIds;

    /**
     * Create a new event instance.
     *
     * @param Pass $pass
     * @param bool|null $notifyStudent
     */
    public function __construct(Pass $pass, ?bool $notifyStudent = true)
    {
        $this->pass = $pass;
        $this->notifyStudent = $notifyStudent;

        if (!$notifyStudent) {
            $this->notifyAdultsIds = $pass->getNotifiableAdults();
        }
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('passes.' . $this->pass->id),
            new PrivateChannel('school.passes.' . $this->pass->school_id),
            new PrivateChannel('user.passes.' . $this->pass->user_id),
        ];
    }
}
