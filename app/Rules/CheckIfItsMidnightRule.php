<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class CheckIfItsMidnightRule implements Rule
{
    public $fromStudent;

    public function __construct(bool $fromStudent = false)
    {
        $this->fromStudent = $fromStudent;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $midnight = "00:00";
        $endMidnight = "00:59";
        if ($this->fromStudent) {
            $value = Carbon::parse($value)->format('H:i');
        }

        if ($value >= $midnight && $value <= $endMidnight) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Start time must be after now and not more than a day.';
    }
}
