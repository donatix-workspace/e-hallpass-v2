<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ActivePassLimit extends Model
{
    protected $guarded = [];

    public const PROXY_PASS = 'proxy_pass';
    public const STUDENT_PASS = 'student_pass';
    public const KIOSK_PASS = 'kiosk_pass';
    public const ACTIVE = 1;
    public const INACTIVE = 0;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return bool
     */
    public function canTeacherOverride()
    {
        return $this->teacher_override !== 0;
    }

    /**
     * @param string $limitType
     * @return bool
     */
    public static function canCreatePasses($limitType = self::STUDENT_PASS): bool
    {
        $activePassLimit = self::fromSchoolId(auth()->user()->school_id)
            ->where($limitType, self::ACTIVE)
            ->where('limit', '!=', -1)
            ->first();

        if ($activePassLimit !== null) {
            $passActiveCount = Pass::fromSchoolId(auth()->user()->school_id)
                ->wherePassStatus(Pass::ACTIVE)
                ->whereDate('created_at', Carbon::today())
                ->count();

            // If teacher override is 1 and proxy pass limitation is on ignore the limit
            if ($activePassLimit->teacher_override === self::ACTIVE && $limitType === self::PROXY_PASS && $activePassLimit->proxy_pass === self::ACTIVE) {
                return false;
            }


            return $passActiveCount >= $activePassLimit->limit;
        }

        return false;
    }

    /**
     * @param string $limitType
     * @return bool
     */
    public static function showActivePassLimitMessageStatus($limitType = self::STUDENT_PASS): bool
    {
        $activePassLimit = self::fromSchoolId(auth()->user()->school_id)
            ->where($limitType, self::ACTIVE)
            ->where('limit', '!=', -1)
            ->first();


        if ($activePassLimit !== null) {
            $passActiveCount = Pass::fromSchoolId(auth()->user()->school_id)
                ->wherePassStatus(Pass::ACTIVE)
                ->whereParentId(null)
                ->whereDate('created_at', Carbon::today())
                ->count();

            // If teacher override is 1 and proxy pass limitation is on ignore the limit
            if ($activePassLimit->teacher_override === self::ACTIVE && $limitType === self::PROXY_PASS && $activePassLimit->proxy_pass === self::ACTIVE && $passActiveCount >= $activePassLimit->limit) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $value
     * @return string|int
     */
    public function getLimitAttribute($value)
    {
        if ($value === -1) {
            return "-";
        }

        return (int)$value;
    }
}
