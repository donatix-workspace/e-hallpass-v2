<?php

namespace App\Jobs;

use App\Models\ContactTracing;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Throwable;

class CreateContactTracesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array $contactTraces
     */
    public $contactTraces;

    /**
     * @var string $searchId
     */
    private $searchId;

    /**
     * @var int $searchId
     */
    public $schoolId;

    /**
     * @param array|object $contactTraces
     * @param string $searchId
     * @param int $schoolId
     */
    public function __construct($contactTraces, string $searchId, int $schoolId)
    {
        $this->contactTraces = $contactTraces;
        $this->searchId = $searchId;
        $this->schoolId = $schoolId;
    }

    /**
     * @return void
     */
    public function handle()
    {
        $contactTraces = $this->contactTraces;

        $contactTracesInsertableArray = [];

        foreach ($contactTraces as $contactTrace) {
            $contactTracesInsertableArray[] = [
                'email' => $contactTrace->email,
                'first_name' => $contactTrace->first_name,
                'last_name' => $contactTrace->last_name,
                'overlap_time_count' => $contactTrace->overlap_time_count,
                'share_same_location' => $contactTrace->share_same_location,
                'school_id' => $this->schoolId,
                'search_id' => $this->searchId,
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        try {
            DB::beginTransaction();

            ContactTracing::insert($contactTracesInsertableArray);

            ContactTracing::where('school_id', $this->schoolId)
                ->where('search_id', $this->searchId)
                ->searchable();

            DB::commit();
        } catch (Throwable $e) {
            logger($e->getMessage());
        }
    }
}
