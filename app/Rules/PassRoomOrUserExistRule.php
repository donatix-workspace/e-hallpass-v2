<?php

namespace App\Rules;

use App\Models\Room;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class PassRoomOrUserExistRule implements Rule
{
    private $type;

    /**
     * Create a new rule instance.
     *
     * @param $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // It's system
        if ($value === config('roles.system')) {
            return true;
        }

        if ($this->type === User::class) {
            $user = User::where('id', $value)
                ->belongsToThisSchool()
                ->exists();

            $userIsAdminOrTeacher = !optional($user)->isStudent();
            return $user && $userIsAdminOrTeacher;
        }

        if ($this->type === Room::class) {
            return Room::where('id', $value)
                ->where('school_id', auth()->user()->school_id)
                ->whereStatus(1)
                ->exists();
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('passes.user.or.room.not.exists');
    }
}
