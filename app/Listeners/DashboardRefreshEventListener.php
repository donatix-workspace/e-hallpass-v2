<?php

namespace App\Listeners;

use App\Entities\CountersEntities;
use App\Events\DashboardRefreshEvent;
use App\Events\TabRefreshEvent;

class DashboardRefreshEventListener
{
    /**
     * @param DashboardRefreshEvent $event
     */
    public function handle(DashboardRefreshEvent $event)
    {
        $counters = CountersEntities::tabCounters($event->user);
        event(new TabRefreshEvent($event->user->id, $counters));
    }
}
