<?php

namespace App\Http;

use App\Http\Middleware\ActionInSchoolMiddleware;
use App\Http\Middleware\AdminPermissionsMiddleware;
use App\Http\Middleware\CheckIfAppointmentPassesIsFullBlocked;
use App\Http\Middleware\CheckIfKioskPassesIsFullBlocked;
use App\Http\Middleware\CheckIfProxyPassesIsFullBlocked;
use App\Http\Middleware\CheckIfStudentPassesIsFullBlocked;
use App\Http\Middleware\CheckIfUserIsLockedMiddleware;
use App\Http\Middleware\HorizonBasicAuthMiddleware;
use App\Http\Middleware\MobileLastUsedMiddleware;
use App\Http\Middleware\ModuleMiddleware;
use App\Http\Middleware\PassMiddleware;
use App\Http\Middleware\PolarityKioskMiddleware;
use App\Http\Middleware\StaffPermissionsMiddleware;
use App\Http\Middleware\TeacherPermissionsMiddleware;
use App\Http\Middleware\TimezoneMiddleware;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        // \App\Http\Middleware\TrustHosts::class,
        \App\Http\Middleware\TrustProxies::class,
        \Fruitcake\Cors\HandleCors::class,
        \App\Http\Middleware\PreventRequestsDuringMaintenance::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class
        ],

        'api' => [
            //            'throttle:api',
            \Illuminate\Routing\Middleware\SubstituteBindings::class
        ]
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' =>
            \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' =>
            \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'adminPermissions' => AdminPermissionsMiddleware::class,
        'teacherPermissions' => TeacherPermissionsMiddleware::class,
        'staffPermissions' => StaffPermissionsMiddleware::class,
        'pass' => PassMiddleware::class,
        'checkIfUserIsLocked' => CheckIfUserIsLockedMiddleware::class,
        'checkIfStudentPassesIsFullBlocked' =>
            CheckIfStudentPassesIsFullBlocked::class,
        'checkIfAppointmentPassesIsFullBlocked' =>
            CheckIfAppointmentPassesIsFullBlocked::class,
        'checkIfKioskPassesIsFullBlocked' =>
            CheckIfKioskPassesIsFullBlocked::class,
        'checkIfProxyPassesIsFullBlocked' =>
            CheckIfProxyPassesIsFullBlocked::class,
        'PolarityKioskCheck' => PolarityKioskMiddleware::class,
        'modules' => ModuleMiddleware::class,
        'mobile.meta.log' => MobileLastUsedMiddleware::class,
        'horizonBasicAuth' => HorizonBasicAuthMiddleware::class,
    ];
}
