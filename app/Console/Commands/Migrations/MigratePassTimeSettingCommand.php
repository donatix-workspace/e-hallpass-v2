<?php

namespace App\Console\Commands\Migrations;

use App\Models\School;
use App\Models\TeacherPassSetting;
use DB;
use Illuminate\Console\Command;

class MigratePassTimeSettingCommand extends Command
{
    protected $signature = 'migrate_sys:time_settings';

    protected $description = 'Migrate Time Settings From EhpV1 to EhpV2';

    public function handle()
    {
        $this->info('--- Migrating Time Settings --- ');
        DB::connection('mysql')->disableQueryLog();
        DB::connection()->disableQueryLog();
        $oldDb = DB::connection('oldDB');
        $oldDb->disableQueryLog();

        $this->output->progressStart(
            $oldDb->table('teacher_pass_settings')->count()
        );

        $oldDb
            ->table('teacher_pass_settings')
            ->chunkById(150, function ($teacherPassSettings) use ($oldDb) {
                $teacherPassSettings->each(function ($timeSetting) use (
                    $oldDb
                ) {
                    $oldSchool = $oldDb
                        ->table('school_schedule')
                        ->where('school_id', $timeSetting->school_id)
                        ->first();
                    if ($oldSchool) {
                        $newSchool = School::where(
                            'old_id',
                            $oldSchool->school_id
                        )->first();
                        if ($newSchool) {
                            TeacherPassSetting::create([
                                'min_time' => $timeSetting->min_time,
                                'awaiting_apt' => $timeSetting->awaiting_apt,
                                'white_passes' => $timeSetting->white_passes,
                                'max_time' => $timeSetting->max_time,
                                'auto_expire_time' =>
                                    $timeSetting->auto_expire_time,
                                'nurse_expire_time' =>
                                    $timeSetting->nurse_auto_expire_time,
                                'mail_time' => $timeSetting->mail_time,
                                'school_id' => $newSchool->id
                            ]);
                        }

                        $this->output->progressAdvance();
                    }
                });
            });
        sleep(2);
        $this->output->progressFinish();
    }
}
