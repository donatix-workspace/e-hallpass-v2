<?php

namespace App\Http\Requests;

use App\Rules\DaysKeyExistRule;
use App\Rules\OnlyIntegersJsonValuesRule;
use App\Rules\OnlyValidJsonYearValuesRule;
use App\Rules\RecurrenceDaysRule;
use App\Rules\RecurrenceMonthlyCorrectStructureRule;
use App\Rules\RecurrenceTypeRule;
use App\Rules\RoomRestrictionValidRule;
use App\Rules\RoomRestrictionValidTypeRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoomRestrictionRequest extends FormRequest
{
    public function rules()
    {
        return [
            'room_id' => ['required', 'integer', Rule::exists('rooms', 'id')
                ->where(function ($query) {
                    $query->where('school_id', auth()->user()->school_id);
                })
            ],
            'limit_for' => ['required', 'string', new RoomRestrictionValidRule()],
            'csv_file' => 'file|mimes:csv,txt,xlsx,doc,docx|required_if:limit_for,csv|nullable',
            'grade_years' => ['json', 'required_if:limit_for,gradeyear', 'nullable', new OnlyValidJsonYearValuesRule()],
            'selected_student_ids' => ['required_if:limit_for,student', 'json', 'nullable', new OnlyIntegersJsonValuesRule()],
            'from_date' => 'date|after_or_equal:today|required|date_format:m/d/Y',
            'to_date' => 'date|required|after_or_equal:from_date|date_format:m/d/Y',
            'type' => ['required', 'string', new RoomRestrictionValidTypeRule()],
        ];
    }

    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}
