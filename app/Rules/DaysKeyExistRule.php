<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DaysKeyExistRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $inputDates = json_decode($value, true);

        return array_key_exists('days', $inputDates);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('days.key.missing');
    }
}
