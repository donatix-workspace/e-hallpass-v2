<?php

namespace Database\Factories;

use App\Models\Release;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class ReleaseFactory extends Factory
{
    protected $model = Release::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'school_id' => 1,
            'heading' => $this->faker->word,
            'content' => $this->faker->word,
            'version' => $this->faker->word,
            'seen_by_user_ids' => collect([])->toJson(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
