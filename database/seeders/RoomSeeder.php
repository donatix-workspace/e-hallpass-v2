<?php

namespace Database\Seeders;

use App\Models\Room;
use App\Models\RoomType;
use App\Models\School;
use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $randomRoomType = RoomType::inRandomOrder()->first();
        $rooms = Room::factory()
            ->count(10)
            ->create(['type_id' => $randomRoomType->id]);
        $rooms->each(function ($room) {
            $randomRoomType = RoomType::inRandomOrder()->first();
            $randomSchool = School::inRandomOrder()->first();
            if ($randomRoomType) {
                $room->update([
                    'school_id' => $randomSchool->id,
                    'type_id' => $randomRoomType->id
                ]);
            }
        });
    }
}
