SET FOREIGN_KEY_CHECKS=0;
START TRANSACTION;
UPDATE ehp_v2_prod.`passes` as child, ehp_v2_prod.`passes` as parent
SET child.parent_id = parent.id
WHERE child.old_parent_id = parent.old_id AND child.old_parent_id > 0;
COMMIT;
SET FOREIGN_KEY_CHECKS=1;
