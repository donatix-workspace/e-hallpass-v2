<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFileNameToUsersImportsTable extends Migration
{
    public function up()
    {
        Schema::table('users_imports', function (Blueprint $table) {
            $table->string('file_name')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users_imports', function (Blueprint $table) {
            //
        });
    }
}
