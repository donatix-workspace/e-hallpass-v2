<?php

namespace App\Traits;

trait AppointmentPassMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'id' => [
                'type' => 'long'
            ],
            'recurrence_appointment_pass_id' => [
                'type' => 'long',
                'null_value' => 0
            ],
            'recurrence_end_at' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||yyyy-MM-dd HH:mm'
            ],
            'show_in_beyond' => [
                'type' => 'boolean'
            ],
            'acknowledged_by_teacher_at' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'fields' => [
                    'keyword' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                        'ignore_above' => 256
                    ]
                ]
            ],
            'search_date_string' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'ignore_above' => 256
            ],
            'user_id' => [
                'type' => 'long'
            ],
            'is_for_confirm' => [
                'type' => 'boolean'
            ],
            'is_for_acknowledge' => [
                'type' => 'boolean'
            ],
            'from_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'to_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'recurrence_appointment_pass' => [
                'properties' => [
                    'recurrence_type' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                        'null_value' => 'NULL'
                    ]
                ]
            ],
            'to_id' => [
                'type' => 'long'
            ],
            'created_by' => [
                'type' => 'long'
            ],
            'string_status' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 'Today\'s APT'
            ],
            'from_id' => [
                'type' => 'long'
            ],
            'is_recurrence' => [
                'type' => 'long'
            ],
            'acknowledged_by' => [
                'type' => 'long'
            ],
            'confirmed_by' => [
                'type' => 'long'
            ],
            'school_id' => [
                'type' => 'long'
            ],
            'pass' => [
                'properties' => [
                    'from.created_at' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'parent_id' => [
                        'type' => 'long',
                        'null_value' => 0
                    ]
                ]
            ],
            'created_by_user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'email' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'period' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'email' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'confirm_cancel' => [
                'type' => 'long'
            ],
            'confirmed_by_teacher_at' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis'
            ],
            'for_date' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||yyyy-MM-dd HH:mm'
            ],
            'from' => [
                'properties' => [
                    'id' => [
                        'type' => 'long'
                    ],
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'to' => [
                'properties' => [
                    'id' => [
                        'type' => 'long'
                    ],
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'reason' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 'NULL'
            ],
            'expired_at' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||yyyy-MM-dd HH:mm'
            ],
            'latest_comment' => [
                'properties' => [
                    'comment' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ]
        ]
    ];
}
