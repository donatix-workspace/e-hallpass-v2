<?php

namespace App\Events;

use App\Models\Pass;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PassCanceled implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $pass;
    public $fromStudent;

    /**
     * Create a new event instance.
     *
     * @param $pass
     * @param bool $fromStudent
     */
    public function __construct($pass, bool $fromStudent = false)
    {
        $this->pass = $pass;
        $this->fromStudent = $fromStudent;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel[]
     */
    public function broadcastOn(): array
    {
        return [
            new PrivateChannel('school.passes.' . $this->pass->school_id),
            new PrivateChannel('passes.' . $this->pass->id),
            new PrivateChannel('user.passes.' . $this->pass->user_id)
        ];
    }
}
