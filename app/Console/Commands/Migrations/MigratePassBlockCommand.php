<?php

namespace App\Console\Commands\Migrations;

use App\Models\School;
use Illuminate\Console\Command;

class MigratePassBlockCommand extends Command
{
    protected $signature = 'migrate_sys:pass_block';

    protected $description = 'Migrate PassBlock from EHPV1 to EhpV2';

    public function handle()
    {
        $this->info('--- Migrating PassBlock ---');

        $newDb = \DB::connection('mysql');
        $oldDb = \DB::connection('oldDB');

        $oldDb->disableQueryLog();
        $newDb->disableQueryLog();

        $passBlocks = $oldDb
            ->table('pass_block')
            ->get()
            ->toArray();

        $passBlocksChunks = array_chunk($passBlocks, 1500);

        $this->output->progressStart(count($passBlocks));

        foreach ($passBlocksChunks as $passBlocksChunk) {
            $insertablePassBlockArray = [];
            foreach ($passBlocksChunk as $passBlock) {
                $oldSchool = $oldDb
                    ->table('school_schedule')
                    ->where('school_id', $passBlock->school_id)
                    ->first();

                if ($oldSchool) {
                    $newSchool = $newDb
                        ->table('schools')
                        ->select('id')
                        ->where('old_id', $oldSchool->school_id)
                        ->first();
                    if ($newSchool) {
                        $insertablePassBlockArray[] = [
                            'school_id' => $newSchool->id,
                            'from_date' => School::convertSchoolDatesToUTC(
                                $passBlock->from_datetime,
                                $oldSchool->school_timezone
                            ),
                            'to_date' => School::convertSchoolDatesToUTC(
                                $passBlock->to_datetime,
                                $oldSchool->school_timezone
                            ),
                            'reason' => $passBlock->reason,
                            'message' => $passBlock->message,
                            'kiosk' => $passBlock->isKioskPass,
                            'appointments' => $passBlock->isApptPass,
                            'students' => $passBlock->isStuPass,
                            'proxy' => $passBlock->isProxyPass,
                            'created_at' => now(),
                            'updated_at' => now()
                        ];
                    }
                }
            }
            try {
                $newDb->table('pass_blocks')->insert($insertablePassBlockArray);

                $this->output->progressAdvance(
                    count($insertablePassBlockArray)
                );
            } catch (\Exception $e) {
                $this->info($e->getMessage());
            }
        }

        $this->output->progressFinish();
    }
}
