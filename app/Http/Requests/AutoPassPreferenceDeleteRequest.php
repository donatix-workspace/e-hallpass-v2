<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AutoPassPreferenceDeleteRequest extends FormRequest
{
    public function rules()
    {
        return [
            'room_ids' => 'array|required',
            'room_ids.*' => ['integer', Rule::exists('rooms', 'id')->where(function ($query) {
                $query->where('school_id', optional(auth()->user())->school_id);
            }), Rule::exists('auto_passes', 'room_id')->where('school_id', optional(auth()->user())->school_id)],
        ];
    }

    public function authorize()
    {
        return auth()->check() && !auth()->user()->isStudent();
    }
}
