<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutoPassesLimitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_passes_limits', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('school_id');
            $table->integer('limit')->default(0);
            $table->timestamps();
        });

        Schema::table('auto_passes_limits', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_passes_limits', function (Blueprint $table) {
            $table->dropForeign('auto_passes_limits_user_id_foreign');
            $table->dropForeign('auto_passes_limits_school_id_foreign');
        });
        Schema::dropIfExists('auto_passes_limits');
    }
}
