<?php

namespace App\Events;

use App\Entities\CountersEntities;
use App\Models\School;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;

class DashboardBuildingRefreshEvent implements ShouldBroadcastNow
{
    use Dispatchable;

    private $schoolId;
    public $buildingCounters;

    /**
     * @param int $schoolId
     */
    public function __construct(int $schoolId)
    {
        $this->schoolId = $schoolId;

        $schoolTimezone = School::findCacheFirst($schoolId)->getTimezone();

        $buildingPasses = CountersEntities::buildingCounters(
            $schoolId,
            $schoolTimezone
        );

        $this->buildingCounters = [
            'buildingPassesCount' => $buildingPasses['buildingPassesCount'],
            'buildingActivePassesCount' =>
                $buildingPasses['buildingActivePassesCount']
        ];

        //buildingCounter.buildingPassesCount
        //buildingCounters.buildingActivePassesCount
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn(): PrivateChannel
    {
        return new PrivateChannel(
            'dashboard-building-refresh.' . $this->schoolId
        );
    }
}
