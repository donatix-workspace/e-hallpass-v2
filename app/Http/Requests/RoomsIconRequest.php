<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoomsIconRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'icon' => 'string|min:1|nullable'
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check() && !auth()->user()->isStudent();
    }
}
