<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isSuperAdmin() || $user->isAdmin();
    }

    /**
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function update(User $user, User $targetUser)
    {
        return $user->isSuperAdmin() || $user->isAdmin() || $user->school_id == $targetUser->school_id;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function bulkInvite(User $user)
    {
        return $user->isAdmin() || $user->isSuperAdmin();
    }

    /**
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function archive(User $user, User $targetUser)
    {
        return $user->school_id === $targetUser->school_id;
    }

    /**
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function view(User $user, User $targetUser)
    {
        return $user->school_id === $targetUser->school_id;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function updateStudent(User $user)
    {
        return $user->isStudent();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function import(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * @param User $user
     * @param User $targetUser
     */
    public function delete(User $user, User $targetUser)
    {
        //
    }

    /**
     * @param User $user
     * @param User $targetUser
     */
    public function restore(User $user, User $targetUser)
    {
        //
    }

    /**
     * @param User $user
     * @param User $targetUser
     */
    public function forceDelete(User $user, User $targetUser)
    {
        //
    }

}
