<?php

namespace App\Console\Commands;

use App\Jobs\DeleteCommonUserJob;
use App\Models\User;
use Illuminate\Console\Command;

class RemoveArchivedUsersCommand extends Command
{
    protected $signature = 'users:archived';

    protected $description = 'Remove the archived users from the database';
    public const ADULTS_DELETE_AFTER_DAYS = 365;
    public const STUDENTS_DELETE_AFTER_DAYS = 30;

    /**
     * Issue: https://eduspiresolutions.atlassian.net/browse/EHP2-1247
     * @return int
     */
    public function handle(): int
    {
        // Delete adults (more than 365 Days).
        $adultsQuery = User::withoutGlobalScopes()
            ->where('status', User::INACTIVE)
            ->where('role_id', '!=', config('roles.student'))
            ->whereDate(
                'archived_at',
                '<',
                today()->subDays(self::ADULTS_DELETE_AFTER_DAYS)
            );

        // Delete a student
        $studentQuery = User::withoutGlobalScopes()
            ->where('status', User::INACTIVE)
            ->where('role_id', '=', config('roles.student'))
            ->whereDate(
                'archived_at',
                '<',
                today()->subDays(self::STUDENTS_DELETE_AFTER_DAYS)
            );
        $studentQuery->unsearchable();
        $adultsQuery->unsearchable();

        DeleteCommonUserJob::dispatch($adultsQuery->get());
        DeleteCommonUserJob::dispatch($studentQuery->get());

        return 0;
    }
}
