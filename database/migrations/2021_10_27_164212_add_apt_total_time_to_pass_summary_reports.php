<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAptTotalTimeToPassSummaryReports extends Migration
{
    public function up()
    {
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->string('total_time_apt');
        });
    }

    public function down()
    {
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->dropColumn('total_time_apt');
        });
    }
}
