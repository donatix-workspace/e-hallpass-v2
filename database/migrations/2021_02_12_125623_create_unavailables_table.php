<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnavailablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unavailables', function (Blueprint $table) {
            $table->id();
            $table->morphs('unavailable');
            $table->unsignedBigInteger('school_id');
            $table->timestamp('from_date');
            $table->timestamp('to_date');
            $table->boolean('status')->default(1);
            $table->string('recurrence_type')->nullable();
            $table->json('recurrence_days')->nullable();
            $table->json('recurrence_week')->nullable();
            $table->timestamp('recurrence_end_at')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });

        Schema::table('unavailables', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unavailables', function (Blueprint $table) {
            $table->dropForeign('unavailables_school_id_foreign');
        });
        Schema::dropIfExists('unavailables');
    }
}
