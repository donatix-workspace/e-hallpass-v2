<?php

namespace App\Console\Commands;

use App\Models\School;
use Illuminate\Console\Command;

class BindTimezoneOffsetsOfSchoolCommand extends Command
{
    protected $signature = 'schools:timezone_offset';

    protected $description = 'Bind timezone offset where the fields are null';

    public function handle()
    {
        $schools = School::select('id', 'timezone')
            ->where(function ($query) {
                $query->where('timezone_offset', '00:00')
                    ->orWhere('timezone_offset', null);
            })
            ->get();
        $bindSchoolsCount = 0;
        foreach ($schools as $school) {
            $school->update([
                'timezone_offset' => config('timezones.offsets.' . $school->timezone)
            ]);
            $bindSchoolsCount++;
        }

        $this->output->info('Successfully bind! Updated ' . $bindSchoolsCount . ' schools.');
    }
}
