<?php

namespace Tests\Feature;

use App\Models\Polarity;
use App\Models\School;
use App\Models\User;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PolarityTest extends TestCase
{
    use RefreshDatabase;

    protected $adminUser;
    protected $firstUser;
    protected $secondUser;
    protected $school;
    protected $polarity;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();
        // Create example school
        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);
        $this->secondUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);
        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);

        $this->polarity = Polarity::factory()
            ->forFirstUser(['school_id' => $this->school->id])
            ->forSecondUser(['school_id' => $this->school->id])
            ->createOne(['school_id' => $this->school->id]);


    }

    /** @test
     * Can admin user view polarity page
     * @return void
     */
    public function an_admin_user_can_view_polarity_page()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.admin.polarities.show', ['polarity' => $this->polarity->id]))
            ->assertJsonStructure(['data', 'status']);
        $this->assertDatabaseHas('polarities', ['id' => $this->polarity->id]);
    }

    /** @test
     * A student user can't view polarities
     * @return void
     */
    public function a_student_user_can_t_view_polarities()
    {
        $this->actingAs($this->firstUser)
            ->get(route('api.v5.admin.polarities.show', ['polarity' => $this->polarity->id]))
            ->assertStatus(403);
    }

    /** @test
     * An admin user can create polarity
     * @return void
     */
    public function an_admin_user_can_create_polarity()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.admin.polarities.create'), [
                'school_id' => $this->school->id,
                'first_user_id' => $this->firstUser->id,
                'second_user_id' => $this->secondUser->id,
                'message' => 'Hello World',
                'status' => 1
            ])->assertJson(['status' => 'success']);
        $this->assertDatabaseHas('polarities', ['first_user_id' => $this->firstUser->id, 'second_user_id' => $this->secondUser->id]);
    }

    /** @test
     * Can admin user change polarity status
     * @return void
     */
    public function an_admin_user_can_change_polarity_status()
    {

        $this->actingAs($this->adminUser)
            ->post(route('api.v5.admin.polarities.status', ['polarity' => $this->polarity->id]))
            ->assertJson(['status' => __('success')]);
        $this->assertDatabaseHas('polarities', ['id' => $this->polarity->id, 'status' => 0]);
    }

    /** @test
     * An admin user can check student polarity status
     * @return void
     */
    public function an_admin_user_can_check_student_polarity_status()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.polarities.students.check', ['user' => $this->firstUser->id]))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();
    }

    /** @test
     * An admin user can update polarity.
     * @return void
     */
    public function an_admin_user_can_update_polarity()
    {
        $this->actingAs($this->adminUser)
            ->put(route('api.v5.admin.polarities.update', ['polarity' => $this->polarity->id]), [
                'message' => 'Hello',
                'first_user_id' => $this->firstUser->id,
                'second_user_id' => $this->secondUser->id,
                'status' => 1,
                'school_id' => $this->school->id,
            ])->assertJson(['status' => __('success'), 'data' => ['id' => $this->polarity->id]]);

        $this->assertDatabaseHas('polarities', ['id' => $this->polarity->id, 'first_user_id' => $this->firstUser->id]);
    }


    /** @test
     * A student user can't update polarity
     * @return void
     */
    public function a_student_user_can_t_update_polarity()
    {
        $this->actingAs($this->firstUser)
            ->put(route('api.v5.admin.polarities.update', ['polarity' => $this->polarity->id]), [
                'message' => 'Hello',
                'first_user_id' => $this->firstUser->id,
                'second_user_id' => $this->secondUser->id,
                'status' => 1,
                'school_id' => $this->school->id,
            ])->assertStatus(403);

        $this->assertDatabaseMissing('polarities', ['id' => $this->polarity->id, 'first_user_id' => $this->firstUser->id]);
    }


    /** @test
     * A student can't delete polarity record
     * @return void
     */
    public function a_student_can_t_delete_polarity_record()
    {
        $this->actingAs($this->firstUser)
            ->delete(route('api.v5.admin.polarities.delete', ['polarity' => $this->polarity->id]))
            ->assertStatus(403);

        $this->assertDatabaseHas('polarities', ['id' => $this->polarity->id]);
    }

    /** @test
     * Can admin user delete polarity record
     * @return void
     */
    public function an_admin_user_can_delete_polarity_record()
    {
        $this->actingAs($this->adminUser)
            ->delete(route('api.v5.admin.polarities.delete', ['polarity' => $this->polarity->id]))
            ->assertJson(['status' => __('success'), 'data' => ['id' => $this->polarity->id]]);

        $this->assertDatabaseMissing('polarities', ['id' => $this->polarity->id]);
    }

}
