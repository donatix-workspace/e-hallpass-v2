<?php

namespace App\Http\Controllers\Admin\Transparency;

use App\Http\Controllers\Controller;
use App\Imports\TransparenciesExport;
use Excel;

class ExportController extends Controller
{
    /**
     * Download Transparency csv
     *
     * @authenticated
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function index()
    {
        return Excel::download(new TransparenciesExport(), 'transparencies.csv');
    }
}
