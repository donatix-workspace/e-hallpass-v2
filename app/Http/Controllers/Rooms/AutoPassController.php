<?php

namespace App\Http\Controllers\Rooms;

use App\Http\Controllers\Controller;
use App\Http\Requests\AutoPassPreferenceDeleteRequest;
use App\Http\Requests\AutoPassPreferenceRequest;
use App\Http\Requests\DeleteBulkAutoPassRequest;
use App\Http\Requests\StoreAutoPassRequest;
use App\Http\Resources\RoomAndAutopassCollection;
use App\Models\AutoPass;
use App\Models\AutoPassLimit;
use App\Models\AutoPassPreference;
use App\Models\Room;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * @group AutoPass Management
 */
class AutoPassController extends Controller
{
    /**
     * Get all rooms and auto pass rooms for user's school.
     *
     * This endpoint returns Rooms and Auto pass rooms
     * @return RoomAndAutopassCollection
     */
    public function index(): RoomAndAutopassCollection
    {
        $schoolId = auth()->user()->school_id;

        $autoRooms = AutoPass::onWriteConnection()
            ->with('room')
            ->where('status', AutoPass::ACTIVE)
            ->where('school_id', $schoolId);

        $rooms = Room::where('status', Room::ACTIVE)
            ->where('school_id', $schoolId)
            ->whereIntegerInRaw('id', $autoRooms->pluck('room_id'))
            ->get();

        $autoPassPreferenceToUser = AutoPassPreference::onWriteConnection()
            ->where('user_id', auth()->user()->id)
            ->where('school_id', $schoolId)
            ->get();

        $data = [
            'rooms' => $rooms,
            'auto_pass_rooms' => $autoRooms->get(),
            'auto_pass_preference_to_user' => $autoPassPreferenceToUser
        ];

        return new RoomAndAutopassCollection($data);
    }

    /**
     * Create room autopass
     *
     * This endpoint lets you create a room autopass.
     * @bodyParam autopass_rooms int[] required Rooms ids
     * @param StoreAutoPassRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function store(StoreAutoPassRequest $request)
    {
        $autoPassRooms = $request->input('autopass_rooms');

        $autoPassIds = AutoPass::where('school_id', auth()->user()->school_id)
            ->get()
            ->pluck('room_id')
            ->toArray();

        $autoPassDifferenceIdsForDelete = array_diff($autoPassIds, $autoPassRooms);

        AutoPass::whereIntegerInRaw('room_id', $autoPassDifferenceIdsForDelete)
            ->where('school_id', auth()->user()->school_id)
            ->delete();

        foreach ($autoPassRooms as $autoPassRoomId) {
            AutoPass::firstOrCreate([
                'room_id' => $autoPassRoomId,
                'school_id' => auth()->user()->school_id
            ]);
        }

        return response()->json([
            'data' => [],
            'message' => __('auto.pass.rooms.updated'),
            'status' => __('success')
        ], 201);
    }

    /**
     * Delete an auto pass rooms.
     *
     * @bodyParam autopass_rooms int[] required Rooms ids
     * @param DeleteBulkAutoPassRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(DeleteBulkAutoPassRequest $request)
    {
        $autoPassRooms = $request->input('autopass_rooms');

        $autoPass = AutoPass::where('school_id', auth()->user()->school_id)
            ->whereIntegerInRaw('room_id', $autoPassRooms);

        $autoPassIds = $autoPass->pluck('id')->toArray();

        $autoPassPreference = AutoPassPreference::whereIntegerInRaw('auto_pass_id', $autoPassIds)
            ->where('school_id', auth()->user()->school_id)
            ->delete();

        $autoPass->delete();

        return response()->json([
            'data' => [],
            'message' => __('auto.pass.rooms.delete'),
            'status' => __('success')
        ], 200);
    }

    /**
     * Add Auto Pass preference to authenticated user.
     *
     * @bodyParam room_ids array required The ids of the rooms
     * @bodyParam limit integer The auto pass limit amount. (0 for no limit)
     * @bodyParam mode string required The auto pass mode.
     *
     * @authenticated
     * @param AutoPassPreferenceRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function addPreference(AutoPassPreferenceRequest $request)
    {
        $roomsAndModes = $request->input('rooms_and_modes');
        $user = auth()->user();

        // If we have limit create it
        if ($request->has('limit')) {
            if ($request->input('limit') >= 0) {
                AutoPassLimit::updateOrCreate([
                    'user_id' => $user->id, 'school_id' => $user->school_id
                ], ['limit' => $request->input('limit') === null ? 0 : $request->input('limit'),]);
            } else {
                AutoPassLimit::ofUserId($user->id)
                    ->fromSchoolId($user->school_id)
                    ->delete();
            }
        }

        $autoPassPreference = AutoPassPreference::where('user_id', $user->id)
            ->where('school_id', $user->school_id)
            ->delete();

        foreach ($roomsAndModes as $roomAndMode) {
            $autoPassRoom = AutoPass::where('room_id', $roomAndMode['room_id'])->first();

            $autoPassPreferenceSameModeCheck = AutoPassPreference::where('auto_pass_id', $autoPassRoom->id)
                ->where('user_id', $user->id)
                ->where('school_id', $user->school_id)
                ->where('status', AutoPassPreference::ACTIVE)
                ->exists();

            if ($autoPassPreferenceSameModeCheck) {
                return response()->json([
                    'message' => __('Room can be assigned only to one mode.'),
                    'status' => __('fail')
                ], 422);
            }

            $autoPassPreference = AutoPassPreference::create([
                'auto_pass_id' => $autoPassRoom->id,
                'user_id' => $user->id,
                'school_id' => $user->school_id,
                'status' => 1,
                'mode' => $roomAndMode['mode']
            ]);
        }

        return response()->json([
            'data' => ['rooms_and_modes' => $roomsAndModes],
            'message' => __('passes.auto.room.preference.added'),
            'status' => __('success')
        ]);
    }

    /**
     * Delete a room user preference
     *
     * @bodyParam room_ids array required The ids of the rooms
     * @authenticated
     * @param AutoPassPreferenceDeleteRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function deletePreference(AutoPassPreferenceDeleteRequest $request)
    {
        $roomIds = $request->input('room_ids');

        $autoPassesRooms = AutoPass::where('school_id', auth()->user()->school_id)
            ->whereIntegerInRaw('room_id', $roomIds)
            ->pluck('id')
            ->toArray();

        $autoPassPreference = AutoPassPreference::where('school_id', auth()->user()->school_id)
            ->whereIntegerInRaw('auto_pass_id', $autoPassesRooms)
            ->where('user_id', auth()->user()->id);

        $autoPassPreference->delete();

        $autoPassPreferenceUserCount = AutoPassPreference::where('user_id', auth()->user()->id)
            ->where('school_id', auth()->user()->school_id)
            ->count();

        if ($autoPassPreferenceUserCount === 0) {
            AutoPassLimit::ofUserId(auth()->user()->id)
                ->whereSchoolId(auth()->user()->school_id)
                ->delete();
        }

        return response()->json([
            'data' => [],
            'message' => __('user.preference.deleted'),
            'status' => __('success')
        ]);
    }
}
