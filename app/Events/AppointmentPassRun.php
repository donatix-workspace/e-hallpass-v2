<?php

namespace App\Events;

use App\Models\AppointmentPass;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AppointmentPassRun implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $appointmentPass;
    public $passId;

    /**
     * Create a new event instance.
     *
     * @param $appointmentPass
     * @param $passId
     */
    public function __construct($appointmentPass, $passId)
    {
        $this->appointmentPass = $appointmentPass;
        $this->passId = $passId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('appointments.passes.' . $this->appointmentPass->school_id),
            new PrivateChannel('user.appointments.passes.' . $this->appointmentPass->user_id)
        ];
    }

}
