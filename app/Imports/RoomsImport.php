<?php

namespace App\Imports;

use App\Models\Module;
use App\Models\Room;
use App\Models\School;
use Illuminate\Validation\Rule;
use Log;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Throwable;

class RoomsImport implements ToModel, WithHeadingRow, WithValidation
{
    /**
     * @var int
     */
    public $schoolId;

    public function __construct(int $schoolId)
    {
        $this->schoolId = $schoolId;
    }

    /**
     * @param array $row
     * @return Room
     */
    public function model(array $row): Room
    {
        $roomInsertArray = [
            'name' => trim($row['room_name']),
            'trip_type' => trim($row['trip_type']),
            'comment_type' => array_key_exists('comment', $row)
                ? $row['comment']
                : Room::COMMENT_OPTIONAL,
            'school_id' => $this->schoolId,
            'enable_appointment_passes' => 0,
            'status' => Room::ACTIVE
        ];

        $school = School::findOrFail($this->schoolId);

        if ($school->hasModule(Module::APPOINTMENTPASS)) {
            $moduleOptions = json_decode(
                $school->getModuleOptions(Module::APPOINTMENTPASS)
            );

            if (optional($moduleOptions)->location) {
                $roomInsertArray = array_merge($roomInsertArray, [
                    'enable_appointment_passes' =>
                        $row['allow_student_apt_request'] === 'yes' ? 1 : 0
                ]);
            }
        }

        return new Room($roomInsertArray);
    }

    /**
     * @return int
     */
    public function batchSize(): int
    {
        return 1500;
    }

    /**
     * @param Throwable $e
     */
    public function failed(Throwable $e): void
    {
        Log::error($e);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'room_name' => [
                'required',
                'min:3',
                'max:30',
                Rule::unique('rooms', 'name')->where(
                    'school_id',
                    auth()->user()->school_id
                )
            ],
            'trip_type' => 'required|string|in:Layover,One Way,Roundtrip',
            'allow_student_apt_request' => ['string', 'nullable', 'in:yes,no'],
            'comment' =>
                'required|string|in:Mandatory,Optional,Hidden,optional,hidden,mandatory'
        ];
    }

    /**
     * @return string[]
     */
    public function customValidationMessages(): array
    {
        return [
            'room_name.required' => 'The room name is required.',
            'room_name.unique' => 'The room name must be unique.',
            'trip_type.required' => 'The Trip Type column is required.',
            'allow_student_apt_request.in' =>
                'Allow students APT Requests field must be yes, no or empty.',
            'room_name.min' =>
                'The room name must be with at least 3 characters.',
            'room_name.max' =>
                'The room name must be with maximum 30 characters.'
        ];
    }
}
