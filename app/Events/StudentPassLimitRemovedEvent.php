<?php

namespace App\Events;

use App\Models\School;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StudentPassLimitRemovedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $typeId;
    private $type;
    public $passLimit;

    /**
     * @param $typeId
     * @param $type
     */
    public function __construct($typeId, $type)
    {
        $this->typeId = $typeId;
        $this->type = $type;
        $this->passLimit = [
            'current' => 0,
            'from_date' => now()->format('Y-m-d'),
            'has' => false,
            'max' => -1,
            'student_reached_limit' => false,
            'to_date' => now()->format('Y-m-d')
        ];
    }

    /**
     * @return Channel
     */
    public function broadcastOn(): Channel
    {
        if ($this->type === School::class) {
            return new PrivateChannel('pass-limits.' . $this->typeId);
        }

        return new PrivateChannel('user.profile.' . $this->typeId);
    }
}
