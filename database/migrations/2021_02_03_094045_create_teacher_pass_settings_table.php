<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherPassSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_pass_settings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('school_id')->unsigned()->index('school_id');
            $table->time('min_time')->default('00:05:00');
            $table->time('white_passes')->default('00:05:00');
            $table->time('awaiting_apt')->default('00:05:00');
            $table->time('max_time')->default('00:10:00');
            $table->time('auto_expire_time')->default('00:15:00');
            $table->time('nurse_expire_time')->default('00:15:00');
            $table->time('mail_time')->default('00:15:00');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_pass_settings');
    }
}
