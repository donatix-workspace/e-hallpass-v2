<?php

namespace App\Http\Requests;

use App\Models\Role;
use App\Models\User;
use App\Rules\DomainIsValid;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateImportedUserRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'authType' => 'required',
            'fileName' => 'required',
            'users' => 'array',
            'users.*.email' => [
                'email',
                new DomainIsValid(auth()->user()->school_id)
            ],
            'users.*.first_name' => 'required|string|max:30',
            'users.*.last_name' => 'required|string|max:30',
            'users.*.role_id' => 'sometimes|min:0|max:5',
            'users.*.role' => [
                'required',
                Rule::in(Role::getRoles()->toArray()),
            ],
            'users.*.status' => [
                'required',
                Rule::in(User::getStatuses()),
            ],
//            'users.*.grade_year' => 'required|min:2000|numeric|max:2060',
            'users.*.grade_year' => 'required_if:users.*.role_id,1|min:2000|numeric|max:2060|nullable',
            'users.*.school_id' => 'required',
            'users.*.is_substitute' => 'sometimes'
        ];
    }
}
