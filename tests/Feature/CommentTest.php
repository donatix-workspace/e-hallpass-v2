<?php

namespace Tests\Feature;

use App\Models\AppointmentPass;
use App\Models\Comment;
use App\Models\Pass;
use App\Models\Period;
use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;
    protected $firstUser;
    protected $teacherUser;
    protected $pass;
    protected $period;
    protected $appointmentPass;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);

        $this->teacherUser = User::factory()->createOne([
            'role_id' => config('roles.teacher'),
            'school_id' => $this->school->id
        ]);

        $this->pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->teacherUser->id,
            'user_id' => $this->firstUser->id
        ]);

        $this->period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);
        $this->appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->teacherUser->id,
            'from_type' => User::class,
            'to_id' => $this->adminUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::tomorrow(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $this->period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);
    }


    /**
     * @test
     * An admin user can post a comment on a pass
     * @return void
     */
    public function an_admin_user_can_post_a_comment_on_a_pass()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.comments.create', ['commentableId' => $this->pass->id]), [
                'commentable_type' => Pass::class,
                'comment' => 'Example comment',
                'permission' => Comment::ONLY_STAFF
            ])->assertStatus(201)
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('comments', [
            'commentable_id' => $this->pass->id,
            'comment' => 'Example comment',
            'user_id' => $this->adminUser->id,
            'commentable_type' => Pass::class
        ]);
    }

    /**
     * @test
     * A teacher user can post a comment on a pass
     * @return void
     */
    public function a_teacher_user_can_post_a_comment_on_a_pass()
    {
        $this->actingAs($this->teacherUser)
            ->post(route('api.v5.comments.create', ['commentableId' => $this->pass->id]), [
                'commentable_type' => Pass::class,
                'comment' => 'Example comment',
                'permission' => Comment::ONLY_STAFF
            ])->assertStatus(201)
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('comments', [
            'commentable_id' => $this->pass->id,
            'comment' => 'Example comment',
            'user_id' => $this->teacherUser->id,
            'commentable_type' => Pass::class
        ]);
    }

    /**
     * @test
     * A student user can't comment on his pass after creation
     * @return void
     */
    public function a_student_user_can_comment_on_his_pass_after_creation()
    {
        $this->actingAs($this->firstUser)
            ->post(route('api.v5.comments.create', ['commentableId' => $this->pass->id]), [
                'commentable_type' => Pass::class,
                'comment' => 'Example comment'
            ])
            ->assertJsonStructure(['data', 'status'])
            ->assertStatus(201);

        $this->assertDatabaseHas('comments', [
            'commentable_id' => $this->pass->id,
            'user_id' => $this->firstUser->id,
            'comment' => 'Example comment',
            'commentable_type' => Pass::class
        ]);
    }

    /**
     * @test
     * An admin user can't post a comment with different type
     * @return void
     */
    public function an_admin_user_can_t_post_a_comment_with_different_type()
    {
        $this->actingAs($this->adminUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->post(route('api.v5.comments.create', ['commentableId' => $this->pass->id]), [
                'commentable_type' => 'Invalid type',
                'comment' => 'Example comment with invalid type'
            ])->assertStatus(422);

        $this->assertDatabaseMissing('comments', [
            'commentable_id' => $this->pass->id,
            'comment' => 'Example comment with invalid type',
            'user_id' => $this->adminUser->id,
            'commentable_type' => Pass::class
        ]);
    }

    /**
     * @test
     * A admin user can post a comment on appointment pass
     * @return void
     */
    public function a_admin_user_can_post_a_comment_on_appointment_pass()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.comments.create', ['commentableId' => $this->appointmentPass->id]), [
                'commentable_type' => AppointmentPass::class,
                'comment' => 'Example comment',
                'permission' => Comment::ONLY_STAFF
            ])->assertStatus(201)
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('comments', [
            'commentable_id' => $this->appointmentPass->id,
            'comment' => 'Example comment',
            'user_id' => $this->adminUser->id,
            'commentable_type' => AppointmentPass::class
        ]);
    }

    /**
     * @test
     * An admin user can view appointment pass comments
     * @return void
     */
    public function an_admin_user_can_view_appointment_pass_comments()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.comments.appointments.show', ['appointmentPass' => $this->appointmentPass->id]))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();
    }

    /**
     * @test
     * A teacher user can view appointment pass comments
     * @return void
     */
    public function a_teacher_user_can_view_appointment_pass_comments()
    {
        $this->actingAs($this->teacherUser)
            ->get(route('api.v5.comments.appointments.show', ['appointmentPass' => $this->appointmentPass->id]))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();
    }

    /**
     * @test
     * An admin user can view pass comments
     * @return void
     */
    public function an_admin_user_can_view_pass_comments()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.comments.passes.show', ['pass' => $this->pass->id]))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();
    }

    /**
     * @test
     * A student can comment on his appointment pass
     * @return void
     */
    public function a_student_can_comment_on_his_appointment_pass()
    {
        $this->actingAs($this->firstUser)
            ->post(route('api.v5.comments.create', ['commentableId' => $this->appointmentPass->id]), [
                'commentable_type' => AppointmentPass::class,
                'comment' => 'Example comment',
            ])->assertStatus(201)
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('comments', [
            'commentable_id' => $this->appointmentPass->id,
            'comment' => 'Example comment',
            'user_id' => $this->firstUser->id,
            'commentable_type' => AppointmentPass::class
        ]);
    }

    /**
     * @test
     * A student can't view comments that are with adults permissions
     * @return void
     */
    public function a_student_can_t_view_comments_that_are_with_adults_permissions()
    {
        $comment = Comment::create([
            'commentable_id' => $this->appointmentPass->id,
            'commentable_type' => AppointmentPass::class,
            'comment' => 'Example Admin comment',
            'permission' => Comment::ONLY_STAFF,
            'user_id' => $this->adminUser->id,
            'school_id' => $this->adminUser->school_id
        ]);

        $this->actingAs($this->firstUser)
            ->get(route('api.v5.comments.appointments.show', ['appointmentPass' => $this->appointmentPass->id]))
            ->assertJsonStructure(['data', 'status'])
            ->assertDontSee($comment->comment);
    }
}
