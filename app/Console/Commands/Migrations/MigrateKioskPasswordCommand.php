<?php

namespace App\Console\Commands\Migrations;

use App\Models\Period;
use App\Models\School;
use App\Models\TeacherPassSetting;
use DB;
use Illuminate\Console\Command;

class MigrateKioskPasswordCommand extends Command
{
    protected $signature = 'migrate_sys:kpassword';

    protected $description = 'Migrate Periods From EhpV1 to EhpV2';

    public function handle()
    {
        $this->info('--- Migrating Kiosk Password --- ');

        $oldDb = DB::connection('oldDB');
        $newDb = DB::connection();

        $oldDb->disableQueryLog();
        $newDb->disableQueryLog();

        $kioskPasswords = $oldDb->table('kiosk_users')
            ->orderBy('id')
            ->get()
            ->toArray();

        $this->output->progressStart(count($kioskPasswords));

        $kioskPasswordsChunk = array_chunk($kioskPasswords, 1000);


        foreach ($kioskPasswordsChunk as $kioskPasswordChunk) {
            $insertableKioskPasswordArray = [];
            foreach ($kioskPasswordChunk as $kioskPassword) {
                $oldSchool = $oldDb->table('school_schedule')->where('school_id', $kioskPassword->school_id)
                    ->first();

                if ($oldSchool) {
                    $newSchool = $newDb->table('schools')
                        ->select('id')
                        ->where('old_id', $oldSchool->school_id)
                        ->first();

                    if ($newSchool) {
                        $oldUser = $oldDb->table('users')->where('user_id', $kioskPassword->user_id)
                            ->first();

                        $newUser = $newDb->table('users')->where('old_id', optional($oldUser)->old_id)
                            ->select('id')
                            ->where('school_id', $newSchool->id)
                            ->first();

                        if ($newUser) {
                            $insertableKioskPasswordArray[] = [
                                'user_id' => $newUser->id,
                                'kpassword' => $kioskPassword->kpassword,
                                'password_processed' => $kioskPassword->password_processed,
                                'school_id' => $newSchool->id,
                                'created_at' => now(),
                                'updated_at' => now()
                            ];
                        }
                    }
                }
            }

            try {
                DB::beginTransaction();
                $newDb->table('kiosk_users')
                    ->insert($insertableKioskPasswordArray);
                $this->output->progressAdvance(count($insertableKioskPasswordArray));
                DB::commit();
            } catch (\Throwable $e) {
                $this->info($e->getMessage());
            }
        }


        sleep(2);
        $this->output->progressFinish();
    }
}
