<?php

namespace App\Console\Commands\Migrations;

use App\Models\Role;
use App\Models\School;
use App\Models\tmp;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateSchoolUserCommand extends Command
{
    protected $signature = 'migrate_sys:school_user';

    protected $description = 'Import School_users from EHpV1 to EHpV2';

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $this->info('--- Migrating School Users --- ');
        DB::connection('mysql')->disableQueryLog();
        DB::connection()->disableQueryLog();
        $oldDb = DB::connection('oldDB');
        $oldDb->disableQueryLog();

        $this->output->progressStart($oldDb->table('users')
//            ->where('migrated', 0)
            ->whereIn('school_id', [1842, 1844, 1845, 1846, 1847, 1848, 1849, 1850, 1851, 1852, 1853, 1854, 1855, 1856, 1857, 1858, 1859, 1860, 1861, 1863, 1864, 1865, 1866, 1867, 1868, 1869, 1870])
            ->count());
        $oldUsersArray = $oldDb->table('users')
            ->orderBy('user_id', 'desc')
//            ->where('migrated', 0)
            ->whereIn('school_id', [1842, 1844, 1845, 1846, 1847, 1848, 1849, 1850, 1851, 1852, 1853, 1854, 1855, 1856, 1857, 1858, 1859, 1860, 1861, 1863, 1864, 1865, 1866, 1867, 1868, 1869, 1870])
            ->get()
            ->toArray();

        $now = now();
        $roles = [
            -2 => 1,
            1 => 5,
            2 => 2,
            3 => 3,
            4 => 1,
            5 => 4,
            6 => 2,
            7 => 1,
            8 => 1,
        ];

        $usersArrayChunk = array_chunk($oldUsersArray, 1000);

        foreach ($usersArrayChunk as $oldUsersChunk) {
            $insertableUsers = [];
            $migratedIds = [];
            foreach ($oldUsersChunk as $oldUser) {
                $newSchool = School::where('old_id', $oldUser->school_id)->select('id')->first();
                if ($oldUser !== null) {
                    if ($newSchool) {
                        array_push($migratedIds, $oldUser->user_id);
                        $insertableUsers[] = [
                            'student_sis_id' => $oldUser->student_sis_id,
                            'email' => $oldUser->email,
                            'username' => $oldUser->username,
                            'password' => $oldUser->password,
                            'first_name' => $oldUser->firstname,
                            'last_name' => $oldUser->lastname,
                            'metatag' => $oldUser->metatag,
                            'role_id' => isset($roles[$oldUser->role_id]) ? $roles[$oldUser->role_id] : config('roles.student'),
                            'avatar' => $oldUser->avatar,
                            'grade_year' => $oldUser->gradyear,
                            'status' => $oldUser->status,
                            'school_id' => $newSchool->id,
                            'is_searchable' => $oldUser->is_searchable,
                            'user_initials' => $oldUser->user_initials,
                            'is_loggin' => $oldUser->is_loggin,
                            'clever_id' => $oldUser->clever_id,
                            'updated_role' => $oldUser->updated_role,
                            'last_logged_in_at' => $oldUser->last_logged_in_at,
                            'auth_type' => $oldUser->auth_type,
                            'created_at' => $now,
                            'updated_at' => $now,
                            'old_id' => $oldUser->user_id
                        ];
                    }
                }
            }

            $this->output->progressAdvance(count($insertableUsers));
            try {

                DB::table('users')->insert($insertableUsers);

//                $oldDb->table('users')->whereIntegerInRaw('user_id', $migratedIds)
//                    ->update(['migrated' => 1]);

            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }

//        $oldDb->table('users')
//            ->orderBy('user_id', 'asc')
//            ->chunk(300, function ($users) use ($oldDb) {
//                $users->each(function ($user) use ($oldDb) {
//                    $schoolId = $user !== null ? $user->school_id : 0;
//                    $oldSchool = $oldDb->table('school_schedule')->where('school_id', $schoolId)->first();
//                    if ($oldSchool) {
//                        $newSchool = School::where('school_domain', $oldSchool->school_domain)->first();
//
//                        $oldRole = DB::connection('oldDB')->table('user_roles')->where('role_id', $user->role_id)->first();
//                        if ($oldRole) {
//                            $newRole = Role::where('name', $oldRole->role_name)->first();
//                            $duplicatedUser = User::where('email', $user->email)->exists();
//                            if ($duplicatedUser) {
//                                $tmp = tmp::create([
//                                    'old_db_user_id' => $user->user_id,
//                                    'email' => $user->email,
//                                ]);
//                            }
//
//                            if (!$duplicatedUser && $user !== null) {
//
//                                DB::table('users')->insert([
//                                    'student_sis_id' => $user->student_sis_id,
//                                    'email' => $user->email,
//                                    'username' => $user->username,
//                                    'password' => $user->password,
//                                    'first_name' => $user->firstname,
//                                    'last_name' => $user->lastname,
//                                    'metatag' => $user->metatag,
//                                    'role_id' => optional($newRole)->id !== null ? optional($newRole)->id : config('roles.student'),
//                                    'avatar' => $user->avatar,
//                                    'grade_year' => $user->gradyear,
//                                    'status' => $user->status,
//                                    'school_id' => $newSchool->id,
//                                    'is_searchable' => $user->is_searchable,
//                                    'user_initials' => $user->user_initials,
//                                    'is_loggin' => $user->is_loggin,
//                                    'clever_id' => $user->clever_id,
//                                    'clever_building_id' => $user->clever_building_id,
//                                    'updated_role' => $user->updated_role,
//                                    'last_logged_in_at' => $user->last_logged_in_at,
//                                    'auth_type' => $user->auth_type,
//                                ]);
//
//                                $this->output->progressAdvance();
//                            }
//                        }
//                    }
//                });
//            });
        sleep(2);
        $this->output->progressFinish();

    }
}
