<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEditedAtToAppointmentPassesTable extends Migration
{
    public function up()
    {
        Schema::table('appointment_passes', function (Blueprint $table) {
            $table->timestamp('edited_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('appointment_passes', function (Blueprint $table) {
            $table->dropColumn('edited_at');
        });
    }
}
