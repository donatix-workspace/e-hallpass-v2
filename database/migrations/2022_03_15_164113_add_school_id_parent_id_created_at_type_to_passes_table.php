<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSchoolIdParentIdCreatedAtTypeToPassesTable extends Migration
{
    public function up()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->index(
                ['school_id', 'parent_id', 'created_at', 'type'],
                'passes_summary_report_school_id_parent_id_created_at_type_index'
            );
        });
    }

    public function down()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->dropIndex(
                'passes_summary_report_school_id_parent_id_created_at_type_index'
            );
        });
    }
}
