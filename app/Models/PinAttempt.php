<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PinAttempt extends Model
{
    use HasFactory;

    public const MAXIMUM_PIN_ATTEMPTS = 5;
    public const LOGOUT_PIN_ATTEMPTS = 3;

    protected $guarded = [];

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pass()
    {
        return $this->belongsTo(Pass::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
