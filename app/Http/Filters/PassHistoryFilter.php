<?php

namespace App\Http\Filters;

use App\Models\Pass;
use App\Models\Room;
use App\Models\School;
use App\Models\StaffSchedule;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class PassHistoryFilter extends QueryFilters
{
    protected $request;
    protected $passTimeSettings;
    public $schoolTimezone;
    public $user;

    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->user = auth()->user();
        $this->schoolTimezone = School::findCacheFirst(
            $this->user->school_id
        )->getTimezone();

        $this->passTimeSettings = TeacherPassSetting::fromSchoolId(
            $request->user()->school_id
        )->first();
        parent::__construct($request);
    }

    /**
     * @param $status
     * @return mixed
     */
    public function only_my_passes($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        $staffSchedules = StaffSchedule::where('user_id', $this->user->id)
            ->get()
            ->pluck('room_id');

        return $status
            ? $this->builder->where(function ($query) use ($staffSchedules) {
                return $query
                    ->where(function ($query) {
                        $query
                            ->where('passes.approved_by', $this->user->id)
                            ->orWhere('passes.completed_by', $this->user->id);
                    })
                    ->orWhere(function ($query) {
                        $query
                            ->where('passes.from_id', $this->user->id)
                            ->where('passes.from_type', User::class);
                    })
                    ->orWhere(function ($query) {
                        $query
                            ->where('passes.to_id', $this->user->id)
                            ->where('passes.to_type', User::class);
                    })
                    ->orWhere(function ($query) use ($staffSchedules) {
                        $query
                            ->whereIntegerInRaw('passes.to_id', $staffSchedules)
                            ->where('passes.to_type', Room::class);
                    })
                    ->orWhere(function ($query) use ($staffSchedules) {
                        $query
                            ->whereIntegerInRaw(
                                'passes.from_id',
                                $staffSchedules
                            )
                            ->where('passes.from_type', Room::class);
                    });
            })
            : $this->builder;
    }

    /**
     * @param $students
     * @return mixed
     */
    public function by_students($students)
    {
        $studentsArray = explode(',', $students);

        if (count($studentsArray) > 1) {
            return $this->builder->where(function ($query) use (
                $studentsArray
            ) {
                return $query->whereIntegerInRaw(
                    'passes.user_id',
                    $studentsArray
                );
            });
        }

        return $this->builder->where(function ($query) use ($studentsArray) {
            return $query->where('passes.user_id', $studentsArray[0]);
        });
    }

    /**
     * @param $teachers
     * @return mixed
     */
    public function by_teachers($teachers)
    {
        $teachersArray = explode(',', $teachers);

        if (count($teachersArray) > 1) {
            return $this->builder->where(function ($query) use (
                $teachersArray
            ) {
                return $query
                    ->whereIntegerInRaw('passes.approved_by', $teachersArray)
                    ->orWhereIntegerInRaw('passes.completed_by', $teachersArray)
                    ->orWhere(function ($query) use ($teachersArray) {
                        $query
                            ->where('passes.to_type', User::class)
                            ->whereIntegerInRaw('passes.to_id', $teachersArray)
                            ->orWhere(function ($query) use ($teachersArray) {
                                $query
                                    ->where('passes.from_type', User::class)
                                    ->whereIntegerInRaw(
                                        'passes.from_id',
                                        $teachersArray
                                    );
                            });
                    });
            });
        }

        return $this->builder->where(function ($query) use ($teachersArray) {
            return $query
                ->where('passes.approved_by', $teachersArray)
                ->orWhere('passes.completed_by', $teachersArray[0])
                ->orWhere(function ($query) use ($teachersArray) {
                    $query
                        ->where('passes.to_type', User::class)
                        ->where('passes.to_id', $teachersArray[0])
                        ->orWhere(function ($query) use ($teachersArray) {
                            $query
                                ->where('passes.from_type', User::class)
                                ->where('passes.from_id', $teachersArray[0]);
                        });
                });
        });
    }

    /**
     * @param $rooms
     * @return mixed
     */
    public function by_rooms($rooms)
    {
        $roomsArray = explode(',', $rooms);

        if (count($roomsArray) > 1) {
            return $this->builder->where(function ($query) use ($roomsArray) {
                return $query
                    ->where('passes.to_type', Room::class)
                    ->whereIntegerInRaw('passes.to_id', $roomsArray);
            });
        }

        return $this->builder->where(function ($query) use ($roomsArray) {
            return $query
                ->where('passes.to_type', Room::class)
                ->where('passes.to_id', $roomsArray[0]);
        });
    }

    /**
     * @param $dates
     * @return mixed
     */
    public function dates($dates)
    {
        // We pass Carbon object directly to the whereBetween() below
        // To correct counters being reset by UTC time.
        $datesArray = explode(',', $dates);

        $schoolTimezone = $this->schoolTimezone;

        $startDate = Carbon::parse(
            $datesArray[0] . ' 00:00:00',
            $schoolTimezone
        )->setTimezone(config('app.timezone'));
        $endDate = Carbon::parse(
            $datesArray[1] . ' 23:59:59',
            $schoolTimezone
        )->setTimezone(config('app.timezone'));

        return $this->builder->where(function ($query) use (
            $startDate,
            $endDate
        ) {
            return $query->whereBetween('passes.created_at', [
                $startDate,
                $endDate
            ]);
        });
    }

    /**
     * @param $filters
     * @return \Illuminate\Database\Query\Builder
     */
    public function filters_list($filters)
    {
        $filters = json_decode($filters);

        return $this->builder->where(function ($query) use ($filters) {
            $query
                ->when(in_array('system_ended', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->orWhere('passes.system_completed', Pass::ACTIVE)
                            ->where('passes.extended_at', null)
                            ->orWhere(function ($query) {
                                $query
                                    ->where(function ($query) {
                                        $query
                                            ->where(
                                                'child.system_completed',
                                                Pass::ACTIVE
                                            )
                                            ->where('child.extended_at', null);
                                    })
                                    ->where(
                                        'passes.system_completed',
                                        Pass::INACTIVE
                                    )
                                    ->where('passes.extended_at', null);
                            });
                    });
                })
                ->when(in_array('yellow_time', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('passes.flagged_at', '!=', null)
                            ->where('passes.system_completed', Pass::INACTIVE)
                            ->whereDoesntHave('child')
                            ->orWhere(function ($query) {
                                $query
                                    ->where('child.flagged_at', '!=', null)
                                    ->where(
                                        'child.system_completed',
                                        Pass::INACTIVE
                                    );
                            })
                            ->orWhere(function ($query) {
                                $query
                                    ->where(function ($query) {
                                        $query
                                            ->where('child.flagged_at', null)
                                            ->where('child.extended_at', null)
                                            ->where(
                                                'child.system_completed',
                                                Pass::INACTIVE
                                            );
                                    })
                                    ->where('passes.flagged_at', '!=', null)
                                    ->where(
                                        'passes.system_completed',
                                        Pass::INACTIVE
                                    );
                            });
                    });
                })
                ->when(in_array('extended_time', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('passes.extended_at', '!=', null)
                            ->where(
                                'passes.system_completed',
                                '=',
                                Pass::ACTIVE
                            )
                            ->orWhere(function ($query) {
                                $query
                                    ->where('child.extended_at', '!=', null)
                                    ->where(
                                        'child.system_completed',
                                        '=',
                                        Pass::ACTIVE
                                    );
                            });
                    });
                })
                ->when(in_array('edited', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query->where('passes.edited_at', '!=', null);
                    });
                })
                ->when(in_array('only_missed_passes', $filters), function (
                    $query
                ) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('passes.expired_at', '!=', null)
                            ->where('passes.approved_at', null)
                            ->where('passes.type', '=', Pass::APPOINTMENT_PASS);
                    });
                })
                ->when(in_array('only_canceled_passes', $filters), function (
                    $query
                ) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('passes.canceled_at', '!=', null)
                            ->where('passes.approved_at', null)
                            ->where('passes.expired_at', null);
                    });
                })
                ->when(in_array('missed_request', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('passes.pass_status', Pass::INACTIVE)
                            ->where('passes.expired_at', '!=', null)
                            ->where('passes.approved_at', null)
                            ->where('passes.type', '=', Pass::APPOINTMENT_PASS);
                    });
                });
        });
    }
}
