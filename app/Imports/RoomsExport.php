<?php

namespace App\Imports;

use App\Models\Room;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RoomsExport implements FromQuery, WithHeadings, WithMapping
{
    public function query()
    {
        $user = auth()->user();
        return Room::where('school_id', $user->school_id);
    }

    public function headings(): array
    {
        return [
            'Room Name',
            'Trip Type',
            'Comment',
            'Allow Student APT Request',
        ];
    }

    public function map($row): array
    {
        return [
            $row->name,
            $row->trip_type,
            $row->comment_type,
            $row->enable_appointment_passes ? 'yes' : 'no',
        ];
    }
}
