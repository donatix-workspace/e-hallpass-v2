<?php

namespace App\Traits;

trait PolarityReportMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'school_id' => [
                'type' => 'long'
            ],
            'user_id' => [
                'type' => 'long'
            ],
            'destination_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'destination_id' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'out_user_id' => [
                'type' => 'long'
            ],
            'prevented_from' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'prevented_at' => [
                'type' => 'date',
                'format' => 'mm/dd/YYYY',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'prevented_time' => [
                'type' => 'date',
                "format" => "h:m a",
                'null_value' => '12:00 AM',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'destination' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                ]
            ],
            'user' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                ]
            ],
            'out_user' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                ]
            ],
        ]
    ];
}
