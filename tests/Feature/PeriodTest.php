<?php

namespace Tests\Feature;

use App\Models\Period;
use App\Models\School;
use App\Models\User;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PeriodTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $secondSchool;
    protected $adminUser;
    protected $firstUser;
    protected $teacherUser;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        $this->school = School::factory()->createOne();
        $this->secondSchool = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->teacherUser = User::factory()->createOne([
            'role_id' => config('roles.teacher'),
            'school_id' => $this->school->id
        ]);

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);
    }

    /**
     * @test
     * An admin user can create a period
     * @return void
     */
    public function an_admin_user_can_create_a_period()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.admin.periods.create'), [
                'name' => 'Period',
                'order' => 1,
                'status' => true
            ])
            ->assertJsonStructure(['data', 'status'])
            ->assertStatus(201);

        $this->assertDatabaseHas('periods', ['name' => 'Period']);
    }

    /**
     * @test
     * A teacher user can't create a period
     * @return void
     */
    public function a_teacher_user_can_t_create_a_period()
    {
        $this->actingAs($this->teacherUser)
            ->post(route('api.v5.admin.periods.create'), [
                'name' => 'Period',
                'order' => 1,
                'status' => true
            ])
            ->assertStatus(403);

        $this->assertDatabaseMissing('periods', ['name' => 'Period']);
    }

    /**
     * @test
     * An admin user can update existing period
     * @return void
     */
    public function an_admin_user_can_update_existing_period()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'name' => 'Period',
            'status' => true,
            'order' => 1
        ]);

        $this->actingAs($this->adminUser)
            ->put(route('api.v5.admin.periods.update', ['period' => $period->id]), [
                'name' => 'PeriodEdited',
                'order' => 1,
                'status' => true
            ])->assertOk()
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('periods', ['name' => 'PeriodEdited', 'id' => $period->id]);
    }

    /**
     * @test
     * An admin user update the status of existing period
     * @return void
     */
    public function an_admin_user_update_the_status_of_existing_period()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'name' => 'Period',
            'status' => false,
            'order' => 1
        ]);

        $this->actingAs($this->adminUser)
            ->put(route('api.v5.admin.periods.status', ['period' => $period->id]), [
                'status' => true
            ])->assertOk()
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('periods', ['name' => 'Period', 'id' => $period->id, 'status' => 1]);
    }

    /**
     * @test
     * An admin user can delete a period
     * @return void
     */
    public function an_admin_user_can_delete_a_period()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'name' => 'Period',
            'status' => false,
            'order' => 1
        ]);

        $this->actingAs($this->adminUser)
            ->delete(route('api.v5.admin.periods.delete', ['period' => $period->id]))
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseMissing('periods', ['id' => $period->id]);
    }
}
