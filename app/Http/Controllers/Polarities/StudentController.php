<?php

namespace App\Http\Controllers\Polarities;

use App\Http\Controllers\Controller;
use App\Models\Pass;
use App\Models\Polarity;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * @group Polarities Status check
 */
class StudentController extends Controller
{
    /**
     * Check if user polarity partner is outside
     *
     * @urlParam user integer required The id of the user
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *      "data": {"ids": [{}]},
     *      "message": 'passes.polarities.student',
     *      "status": 'success'
     * }
     * @authenticated
     */
    public function check(User $user)
    {
        $polarityCanceledIds = Polarity::active()
            ->ofUserId($user->id)
            ->fromSchoolId($user->school->id)
            ->selectRaw('if(first_user_id = ?, second_user_id, first_user_id) as polarity_ids', [$user->id])
            ->pluck('polarity_ids');

        $polarityOutStudents = Pass::active()
            ->fromSchoolId($user->school->id)
            ->whereIntegerInRaw('user_id', $polarityCanceledIds)
            ->get();

        if (!$polarityOutStudents->isEmpty()) {
            return response()->json([
                'data' => ['ids' => $polarityCanceledIds],
                'message' => __('passes.polarities.student'),
                'status' => __('fail')
            ]);
        }

        return response()->json([
            'data' => [],
            'message' => __('passes.polarities.studentSuccess'),
            'status' => __('success')
        ]);
    }
}
