<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class KioskUser extends Model
{
    protected $fillable = ['user_id', 'school_id', 'kpassword', 'password_processed'];

    use HasFactory, HasApiTokens, Authenticatable;

    /**
     * @param $value
     * @return string
     */
    public function getkpasswordAttribute($value): string
    {
        return $value !== null ? 'Y' : 'N';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Invalidate user token
     */
    public static function kioskLogout()
    {
        auth()->guard('kiosk')->user()->token()->delete();
    }

    /**
     * @return bool
     */
    public function isStudent()
    {
        return $this->user->isStudent();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }
}
