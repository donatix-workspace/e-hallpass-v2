<?php

namespace App\Models;

use App\Elastic\RoomRestrictionsIndexConfigurator;
use App\Http\Filters\Filterable;
use App\Http\Resources\RoomRestrictionResource;
use App\Traits\ExtendedSearchable;
use App\Traits\PerPage;
use App\Traits\RoomRestrictionMapping;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use ScoutElastic\Searchable;

class RoomRestriction extends Model
{
    use HasFactory,
        Filterable,
        ExtendedSearchable,
        RoomRestrictionMapping,
        PerPage;

    protected $table = 'rooms_restrictions';
    protected $appends = ['location'];
    protected $indexConfigurator = RoomRestrictionsIndexConfigurator::class;

    protected $dates = ['from_date', 'to_date', 'recurrence_end_at'];

    protected $with = ['room:id,name', 'user'];
    protected $guarded = ['id'];

    public const ACCESS_DENIED_GROUP = 'access_denied';
    public const MEMBERS_ONLY_GROUP = 'members_only';

    public const CSV = 'csv';
    public const GRADEYEAR = 'gradeyear';
    public const STUDENT = 'student';

    public const ACTIVE = 1;
    public const INACTIVE = 0;

    /**
     * @param DateTimeInterface $date
     * @return string
     */
    public function serializeDate(DateTimeInterface $date)
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($date, config('app.timezone'))
            ->setTimezone(optional($school)->getTimezone())
            ->format('Y-m-d H:i:s');
    }

    /**
     * @return array
     */
    public function toSearchableArray(): array
    {
        $school = School::findCacheFirst($this->school_id);

        $roomRestriction = $this->toArray();

        $roomRestriction['from_date'] = School::convertTimeToCorrectTimezone(
            $roomRestriction['from_date'],
            $roomRestriction['from_date'],
            true,
            false,
            'm/d/Y'
        );
        $roomRestriction['to_date'] = School::convertTimeToCorrectTimezone(
            $roomRestriction['to_date'],
            optional($school)->getTimezone(),
            true,
            false,
            'm/d/Y'
        );

        return $roomRestriction;
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @param $query
     * @param $user
     * @return mixed
     */
    public function scopeOfUserId($query, $user)
    {
        return $query->where('user_id', $user);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }

    /**
     * @return mixed|string|null
     */
    public function getLocationAttribute()
    {
        if ($this->user_id !== null) {
            return optional($this->user)->name;
        }
        $gradeYearDecode = json_decode($this->grade_year, true);

        if ($gradeYearDecode !== null) {
            return implode(', ', $gradeYearDecode);
        }

        return null;
    }

    /**
     * @param null $user
     * @return \Illuminate\Support\Collection
     */
    public static function getRestrictedRoomIds(
        $user = null
    ): \Illuminate\Support\Collection {
        if ($user === null) {
            $user = auth()->user();
        }

        $restrictions = self::fromSchoolId($user->school_id)
            ->active()
            ->where('from_date', '<=', now()->toDateString())
            ->where('to_date', '>=', now()->toDateString())
            ->get();

        /**
         * Check if the user has separated members only above the grade year.
         */
        $existsMembersOnlyForThemself = RoomRestriction::active()
            ->where('from_date', '<=', now()->toDateString())
            ->where('to_date', '>=', now()->toDateString())
            ->where('grade_year', null)
            ->where('user_id', $user->id)
            ->exists();

        // Array based on Laravel Collection.
        $restrictedRoomIds = collect([]);

        // Walk through all of the restriction records
        foreach ($restrictions as $restriction) {
            $gradeYears = collect(json_decode($restriction->grade_year, true));
            // Check if restriction group is `Access Danied` a.k.a users who are blacklisted
            if ($restriction->type === self::ACCESS_DENIED_GROUP) {
                // If the restriction is by grade_year add the id of the room to the array
                if (
                    $restriction->grade_year !== null &&
                    $gradeYears->contains($user->grade_year)
                ) {
                    $restrictedRoomIds->add($restriction->room_id);
                }

                // If the restriction user id is equals as the restriction `user_id` add the room id to the array
                if ($restriction->user_id === $user->id) {
                    $restrictedRoomIds->add($restriction->room_id);
                }
            }

            // Check if restriction group is `Members Only`
            if ($restriction->type === self::MEMBERS_ONLY_GROUP) {
                // if the restriction user id is not equals as the `user_id` we add the room_id to the array
                // because the user is not part of that member group
                if (
                    $restriction->user_id !== $user->id &&
                    !$restriction->grade_year
                ) {
                    $restrictedRoomIds->add($restriction->room_id);
                }

                // If the restriction is by grade_year and grade_year is not equals
                // as the user grade_year, add the room to the array
                if (
                    $restriction->grade_year !== null &&
                    !$gradeYears->contains($user->grade_year)
                ) {
                    if (!$existsMembersOnlyForThemself) {
                        $restrictedRoomIds->add($restriction->room_id);
                    }
                }
            }
        }

        return $restrictedRoomIds->flatten();
    }

    /**
     * @param array $roomRestrictionData
     * @return bool
     */
    public static function multipleInsertOrUpdate(array $roomRestrictionData)
    {
        collect($roomRestrictionData)->each(function ($item) {
            $roomRestriction = self::where('room_id', '=', $item['room_id'])
                ->fromSchoolId($item['school_id'])
                ->ofUserId($item['user_id'])
                ->where('status', RoomRestriction::ACTIVE)
                ->where(function ($query) use ($item) {
                    $query
                        ->whereBetween('from_date', [
                            $item['from_date'],
                            $item['to_date']
                        ])
                        ->orWhereBetween('to_date', [
                            $item['from_date'],
                            $item['to_date']
                        ]);
                })
                ->first();

            if ($roomRestriction !== null) {
                self::where('id', $roomRestriction->id)->update([
                    'from_date' => $item['from_date'],
                    'to_date' => $item['to_date']
                ]);
            }

            if ($roomRestriction === null) {
                self::create($item);
            }
        });

        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(Room::class)->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withoutGlobalScopes();
    }

    public function setFromDateAttribute($value)
    {
        $this->attributes['from_date'] = Carbon::parse($value)->setTimezone(
            config('app.timezone')
        );
    }

    public function setToDateAttribute($value)
    {
        $this->attributes['to_date'] = Carbon::parse($value)->setTimezone(
            config('app.timezone')
        );
    }

    //    /**
    //     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    //     */
    //    public function group()
    //    {
    ////        return $this->belongsTo(Room::class);
    //    }
}
