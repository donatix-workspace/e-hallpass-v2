<?php

namespace App\Console\Commands;

use App\Events\PassExpired;
use App\Events\PassMinTimeReached;
use App\Models\NurseRoom;
use App\Models\Pass;
use App\Models\Room;
use App\Models\TeacherPassSetting;
use App\Notifications\Push\StudentNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class PassExtendedCommand extends Command
{
    protected $signature = 'passes:extended';

    protected $description = 'Add timestamp of time when the pass is being extended with nurse';

    public function handle(): int
    {
        $passes = Pass::whereBetween('created_at', [
            now()
                ->startOfDay()
                ->toDateTimeString(),
            now()
                ->endOfDay()
                ->toDateTimeString()
        ])
            ->where('expired_at', null)
            ->where('extended_at', null)
            ->where('canceled_at', null)
            ->where('pass_status', Pass::ACTIVE)
            ->where(function ($query) {
                $query
                    ->where('approved_at', '!=', null)
                    ->orWhere(function ($query) {
                        $query
                            ->where('approved_at', null)
                            ->where('parent_id', '!=', null);
                    });
            })
            ->orderBy('created_at', 'desc')
            ->cursor();

        foreach ($passes as $pass) {
            $pass->setAppends([]);
            $teacherPassSettings = TeacherPassSetting::where(
                'school_id',
                $pass->school_id
            )->first();
            $nurseRoom = NurseRoom::where('room_id', $pass->to_id)
                ->where('school_id', $pass->school_id)
                ->first();

            if (
                $nurseRoom &&
                $pass->to_type === Room::class &&
                $pass->isApproved()
            ) {
                $nurseExpireTime = TeacherPassSetting::convertInMinutes(
                    $nurseRoom->end_time
                );
                $autoExpireTime = TeacherPassSetting::convertInMinutes(
                    $teacherPassSettings->auto_expire_time
                );

                if ($pass->parent_id !== null) {
                    $differenceInMinutes = $pass->parent->created_at->diffInMinutes(
                        now()
                    );
                } else {
                    $differenceInMinutes = $pass->created_at->diffInMinutes(
                        now()
                    );
                }

                if (
                    $differenceInMinutes >= $autoExpireTime &&
                    $differenceInMinutes < $nurseExpireTime
                ) {
                    $pass->update([
                        'extended_at' => now()
                    ]);

                    if ($pass->parent) {
                        $pass->parent->update([
                            'extended_at' => now()
                        ]);
                    }
                }
            }
            $pass->searchable();
            if ($pass->parent !== null) {
                $pass->parent->searchable();
            }
        }

        return 0;
    }
}
