<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePassBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pass_blocks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id');
            $table->timestamp('from_date');
            $table->timestamp('to_date');
            $table->string('reason');
            $table->string('message');
            $table->boolean('kiosk')->default(0);
            $table->boolean('appointments')->default(0);
            $table->boolean('students')->default(0);
            $table->boolean('proxy')->default(0);
            $table->timestamps();
        });
        Schema::table('pass_blocks', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pass_blocks', function (Blueprint $table) {
            $table->dropForeign('pass_blocks_school_id_foreign');
        });

        Schema::dropIfExists('pass_blocks');
    }
}
