<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolModule extends Model
{
    use HasFactory;

    protected $table = 'school_modules';
    public $timestamps = false;
    protected $guarded = [];

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    /**
     * @return mixed|string
     */
    public function getDisplayNameAttribute()
    {
        return $this->module->display_name;
    }

    /**
     * @return mixed|string
     */
    public function getNameAttribute()
    {
        return $this->module->name;
    }

    /**
     * @param $school
     * @param $moduleId
     * @param $status
     * @return SchoolModule|\Illuminate\Database\Eloquent\Builder|Model|object|null
     */
    public static function updateOrCreateModule($school, $moduleId, $status)
    {
        $schoolModule = self::where('school_id', $school->id)
            ->where('module_id', $moduleId)
            ->first();

        if ($schoolModule) {
//            $schoolModule->update([
//                'status' => $status
//            ]);
        } else {
            $schoolModule = self::create([
                'school_id' => $school->id,
                'module_id' => $moduleId,
                'status' => $status,
                'permission' => Module::PERMISSION_ENABLED,
                'option_json' => Module::getDefaultOptionsById($moduleId)
            ]);
        }
        return $schoolModule;
    }

    /**
     * @param $school
     * @param $moduleId
     * @param $status
     * @return SchoolModule|\Illuminate\Database\Eloquent\Builder|Model|object|void|null
     */
    public static function dropOrCreateSchoolModule($school, $moduleId, $status)
    {
        $schoolModule = self::where('school_id', $school->id)->where(
            'module_id',
            $moduleId
        );

        // If the status comes false from division we delete the module, else we just update it.
        return $status
            ? self::updateOrCreateModule($school, $moduleId, $status)
            : $schoolModule->delete();
    }
}
