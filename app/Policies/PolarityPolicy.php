<?php

namespace App\Policies;

use App\Models\Polarity;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PolarityPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return !$user->isStudent();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Polarity $polarity
     * @return mixed
     */
    public function view(User $user, Polarity $polarity)
    {
        return !$user->isStudent() && $user->school_id === $polarity->school_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return !$user->isStudent();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Polarity $polarity
     * @return mixed
     */
    public function update(User $user, Polarity $polarity)
    {
        return (!$user->isStudent()) && $user->school_id === $polarity->school_id;
    }

    /**
     * Determine whether the user can update status.
     *
     * @param User $user
     * @param Polarity $polarity
     * @return bool
     */
    public function updateStatus(User $user, Polarity $polarity)
    {
        return (!$user->isStudent()) && $user->school_id === $polarity->school_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Polarity $polarity
     * @return mixed
     */
    public function delete(User $user, Polarity $polarity)
    {
        return (!$user->isStudent()) && $user->school_id === $polarity->school_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Polarity $polarity
     * @return mixed
     */
    public function restore(User $user, Polarity $polarity)
    {
        return (!$user->isStudent()) && $user->school_id === $polarity->school_id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Polarity $polarity
     * @return mixed
     */
    public function forceDelete(User $user, Polarity $polarity)
    {
        return (!$user->isStudent()) && $user->school_id === $polarity->school_id;
    }


}
