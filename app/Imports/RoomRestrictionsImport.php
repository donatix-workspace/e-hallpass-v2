<?php

namespace App\Imports;

use App\Models\PassLimit;
use App\Models\RoomRestriction;
use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RoomRestrictionsImport implements FromQuery, WithHeadings, WithMapping
{
    public $restrictionQuery;

    /**
     *
     */
    public function __construct($restrictionQuery)
    {
        $this->restrictionQuery = $restrictionQuery;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|Builder
     */
    public function query()
    {
        return $this->restrictionQuery;
    }

    public function headings(): array
    {
        return [
            'Student',
            'From Date',
            'To Date',
            'Location',
            'Limitation',
            'Status'
        ];
    }

    public function map($row): array
    {
        $type = null;
        if ($row->user_id === null) {
            $type = json_decode($row->grade_year);
            $type = implode(',', $type);
        } else {
            $type = $row->user->first_name . ' ' . $row->user->last_name;
        }

        return [
            $type,
            Carbon::parse($row->from_date)->toDateString(),
            Carbon::parse($row->to_date)->toDateString(),
            $row->room->name,
            $row->type,
            $row->status ? 'ACTIVE' : 'INACTIVE'
        ];
    }
}
