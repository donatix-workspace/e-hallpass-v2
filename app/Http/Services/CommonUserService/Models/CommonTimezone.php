<?php

namespace App\Http\Services\CommonUserService\Models;


use Illuminate\Database\Eloquent\Model;

class CommonTimezone extends Model
{
   protected $connection = 'commonDb';
   protected $table = 'l10n_datetime_time_zone';


}
