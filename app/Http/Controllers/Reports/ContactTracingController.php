<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactTracingCsvExportRequest;
use App\Http\Requests\ContactTracingSearchRequest;
use App\Http\Resources\ContactTracingCollection;
use App\Imports\ContactTracingImport;
use App\Jobs\CreateContactTracesJob;
use App\Models\ContactTracing;
use App\Models\Pass;
use App\Models\User;
use Carbon\Carbon;
use Excel;
use Gate;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Str;

/**
 * @group Contact Tracing
 */
class ContactTracingController extends Controller
{
    /**
     * Get Contact tracing form entities
     *
     * @response status=200 {
     *  "data": {
     *      "students": [{}]
     *  },
     * "status": "success"
     * }
     *
     * @authenticated
     * @return JsonResponse
     */
    public function index()
    {
        if (!Gate::allows('admin-reports-view-contact-tracing')) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('invalid.permission'),
                    'status' => __('fail')
                ],
                403
            );
        }

        $user = auth()->user();

        // We get the students with they're pass limits and room restriction
        $students = User::fromSchoolId($user->school_id)
            ->fromStudents()
            ->withCount('passes')
            ->get();

        return response()->json([
            'data' => [
                'students' => $students
            ],
            'status' => __('success')
        ]);
    }

    /**
     * Contact trace the user
     *
     * @queryParam per_page integer How many items per page will be shown
     * @bodyParam user_id integer required The id of the user
     * @bodyParam from_date date required From what date to trace
     * @bodyParam to_date date required To date to trace.
     *
     * @response status=200 {
     *     "data": [
     * {
     * "user_id": 12394,
     * "overlap_time_count": 3,
     * "share_same_location": 1,
     * "total_time": "00:00:00",
     * "is_flagged": true,
     * "from": null,
     * "to": null,
     * "approved_by": null,
     * "requested_by": null,
     * "comments": [],
     *  "user": {
     *     "id": 12394,
     *     "cus_uuid": null,
     *     "student_sis_id": "0",
     *     "email": "student2@admin.bg",
     *     "email_verified_at": null,
     *     "username": null,
     *     "first_name": "Lubomir",
     *     "last_name": "Stankov",
     *     "metatag": null,
     *     "role_id": 1,
     *  },
     * "child": null
     * }
     * ],
     * "status": "success"
     * }
     * @authenticated
     * @param ContactTracingSearchRequest $request
     * @return JsonResponse
     */
    public function search(ContactTracingSearchRequest $request): JsonResponse
    {
        if (!Gate::allows('admin-reports-view-contact-tracing')) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('invalid.permission'),
                    'status' => __('fail')
                ],
                403
            );
        }

        $user = auth()->user();

        $fromDate =
            Carbon::parse($request->input('from_date'))->toDateString() .
            ' 00:00:01';
        $toDate =
            Carbon::parse($request->input('to_date'))->toDateString() .
            ' 23:59:59';
        $userId = $request->input('user_id');

        $contactTracingPasses = Pass::runContactTracing(
            $userId,
            $fromDate,
            $toDate
        );

        $contactTracingSearchUuid = Str::uuid();

        CreateContactTracesJob::dispatchSync(
            $contactTracingPasses,
            $contactTracingSearchUuid,
            $user->school_id
        );

        return response()->json([
            'data' => [
                'search_id' => $contactTracingSearchUuid
            ],
            'status' => __('success')
        ]);
    }

    /**
     * Show the results from the contact trace
     *
     * @response status=200 {
     *     "data": [
     * {
     * "user_id": 12394,
     * "overlap_time_count": 3,
     * "share_same_location": 1,
     * "total_time": "00:00:00",
     * "is_flagged": true,
     * "from": null,
     * "to": null,
     * "approved_by": null,
     * "requested_by": null,
     * "comments": [],
     *  "user": {
     *     "id": 12394,
     *     "cus_uuid": null,
     *     "student_sis_id": "0",
     *     "email": "student2@admin.bg",
     *     "email_verified_at": null,
     *     "username": null,
     *     "first_name": "Lubomir",
     *     "last_name": "Stankov",
     *     "metatag": null,
     *     "role_id": 1,
     *  },
     * "child": null
     * }
     * ],
     * "status": "success"
     * }
     * @authenticated
     * @param Request $request
     * @return ContactTracingCollection|JsonResponse
     */
    public function results(Request $request)
    {
        if (!$request->filled('search_id')) {
            return response()->json(
                [
                    'data' => [],
                    'status' => __('fail'),
                    'message' => __('Please provide a valid search Id')
                ],
                422
            );
        }
        $user = auth()->user();

        if ($request->filled('sort') || $request->filled('search_query')) {
            $contactTraces = ContactTracing::search(
                convertElasticQuery(
                    $request->has('search_query')
                        ? $request->get('search_query')
                        : config('scout_elastic.all_records')
                )
            )
                ->when($request->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('search_id', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];

                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }

                    return $query->orderBy('id', 'asc');
                })
                ->where('search_id', $request->input('search_id'))
                ->where('school_id', $user->school_id)
                ->paginate(
                    $request->get(
                        'per_page',
                        config('pagination.default_pagination')
                    )
                );
        } else {
            $contactTraces = ContactTracing::where(
                'school_id',
                $user->school_id
            )
                ->where('search_id', $request->input('search_id'))
                ->paginate(
                    $request->get(
                        'per_page',
                        config('pagination.default_pagination')
                    )
                );
        }

        return new ContactTracingCollection($contactTraces);
    }

    /**
     * @param $items
     * @param int $perPage
     * @param null $page
     * @param array $options
     * @return LengthAwarePaginator
     */
    public function contactTracingPagination(
        $items,
        int $perPage = 5,
        $page = null,
        array $options = []
    ) {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items =
            $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator(
            $items->forPage($page, $perPage),
            $items->count(),
            $perPage,
            $page,
            $options
        );
    }

    /**
     * Contact Tracing CSV Export
     *
     * @bodyParam user_id integer required The id of the user
     * @bodyParam from_date date required From what date to trace
     * @bodyParam to_date date required To date to trace.
     *
     * @queryParam export_as string What type of file to be exported  Default: csv Example: csv, excel
     *
     * @authenticated
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function csv(ContactTracingCsvExportRequest $request)
    {
        if (!Gate::allows('admin-reports-view-contact-tracing')) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('invalid.permission'),
                    'status' => __('fail')
                ],
                403
            );
        }

        $user = auth()->user();
        $contactTraces = null;

        if ($request->filled('sort') || $request->filled('search_query')) {
            $contactTraces = ContactTracing::search(
                convertElasticQuery(
                    $request->has('search_query')
                        ? $request->get('search_query')
                        : config('scout_elastic.all_records')
                )
            )
                ->when($request->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('search_id', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];

                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }

                    return $query->orderBy('id', 'asc');
                })
                ->where('search_id', $request->input('search_id'))
                ->where('school_id', $user->school_id);

            $contactTracesIds = $contactTraces
                ->take(
                    $request->get(
                        'per_page',
                        config('pagination.default_pagination')
                    )
                )
                ->get()
                ->pluck('id');

            $contactTracesCsv = ContactTracing::whereIntegerInRaw(
                'id',
                $contactTracesIds
            )->orderByRaw(
                'field(id,' . implode(',', $contactTracesIds->toArray()) . ')'
            );
        } else {
            $contactTracesCsv = ContactTracing::where(
                'school_id',
                $user->school_id
            )->where('search_id', $request->input('search_id'));
        }

        $userId = $request->input('user_id');

        if ($request->get('export_as') === 'print') {
            return response()->json([
                'data' => $contactTracesCsv
                    ->take(
                        $request->get(
                            'per_page',
                            config('pagination.default_pagination')
                        )
                    )
                    ->get()
                    ->toArray(),
                'status' => __('success')
            ]);
        }
        if (
            $request->has('export_as') &&
            $request->get('export_as') !== 'csv'
        ) {
            return Excel::download(
                new ContactTracingImport($userId, $contactTracesCsv),
                'contact_tracing_' .
                    time() .
                    '_school_id_' .
                    auth()->user()->school_id .
                    '.xlsx'
            );
        }

        return Excel::download(
            new ContactTracingImport($userId, $contactTracesCsv),
            'contact_tracing_' .
                time() .
                '_school_id_' .
                auth()->user()->school_id .
                '.csv'
        );
    }
}
