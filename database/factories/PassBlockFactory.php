<?php

namespace Database\Factories;

use App\Models\PassBlock;
use App\Models\School;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PassBlockFactory extends Factory
{
    protected $model = PassBlock::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'from_date' => $this->faker->word,
            'to_date' => $this->faker->word,
            'reason' => $this->faker->word,
            'message' => $this->faker->word,
            'kiosk' => $this->faker->boolean,
            'appointments' => $this->faker->boolean,
            'students' => $this->faker->boolean,
            'proxy' => $this->faker->boolean,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'school_id' => function () {
                return School::factory()->create()->id;
            },
        ];
    }
}
