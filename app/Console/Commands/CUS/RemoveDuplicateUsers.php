<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Translators\SchoolTranslator;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\AppointmentPass;
use App\Models\Module;
use App\Models\Pass;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\StudentFavorite;
use App\Models\TeacherPassSetting;
use App\Models\Unavailable;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class RemoveDuplicateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:remove {--email=} {--force} {--afterId=} {--schoolId=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Duplicate Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->option('force')) {

            $userId = $this->option('afterId', null);

            $dbQuery = DB::table('users')
                ->select(['id', 'email', DB::raw('count(email) as duplicates')])
                ->groupBy('email')
                ->when($userId, function ($query) use ($userId) {
                    $query->where('id', '>', $userId);
                })
                ->havingRaw('count(email) > 1')
                ->orderBy('id');

            $dbQuery->chunk(100, function ($users) {
                foreach ($users as $user) {
                    $this->info('Clear ' . $user->id);
                    $this->sync($user);
                    $this->clear($user);
                }

            });

        } elseif ($this->option('email')) {
            $user = User::where('email', $this->option('email'))->first();

            $this->sync($user);

            $this->clear($user);
        } elseif ($schoolId = $this->option('schoolId')) {
            $userIds = DB::table('schools_users')
                ->select(['user_id'])
                ->where('school_id', $schoolId)
                ->get()
                ->pluck('user_id');
            foreach ($userIds as $userId) {
                $user = User::withoutGlobalScopes()
                    ->whereNotNull('email')
                    ->where('id', $userId)->first();
                if($user){
                    $this->sync($user);
                    $this->clear($user);
                }

            }
        }


    }

    protected function sync($existingUser)
    {
        $dbQuery = DB::connection('commonDb')
            ->table('common_bm_membership as m')
            ->join('auth_user as u', 'm.auth_user_id', '=', 'u.id')
            ->join('common_bm_school as s', 'm.common_bm_school_id', '=', 's.id')
            ->leftJoin('auth_title as t', 'u.auth_title_id', '=', 't.id')
            ->selectRaw('u.uuid as user_uuid,
            u.email as email,
            m.sis_id as student_sis_id,
            u.username as username,
            u.first_name as first_name,
            u.last_name as last_name,
            m.common_bm_role_membership_id as role_id,
            u.image as avatar,
            m.graduation_year as grade_year,
            u.r_status as user_status,
            m.r_status as membership_status,
            u.last_login_time as last_login_time,
            u.created as created_at,
            u.modified as updated_at,
            s.uuid as school_uuid,
            m.uuid as membership_uuid,
            t.r_name as title,
            u.id as common_user_id,
            u.archived_at as deleted_at
            ',
            )
            ->where('m.auth_client_id', 2)
            ->where('u.email', $existingUser->email)
            ->orderBy('m.id', 'asc');


        $dbQuery->chunk(5, function ($fullRecords) {
            foreach ($fullRecords as $fullRecord) {

                $school = School::where('cus_uuid', $fullRecord->school_uuid)->first();
                if ($school) {
                    $data = UserTranslator::translateJoinedRecord($fullRecord, $school);

                    User::enableMassIndex();
                    $email = $data['email'];
                    unset($data['email']);

                    $users = User::withoutGlobalScopes()->where('email', $email)->get();

                    foreach ($users as $user) {
                        $user->update($data);
                        DB::table('schools_users')
                            ->where('school_id', $school->id)
                            ->where('user_id', $user->id)
                            ->delete();
                        DB::table('schools_users')->updateOrInsert(
                            [
                                'school_id' => $school->id,
                                'user_id' => $user->id,
                            ],
                            [
                                'membership_uuid' => $fullRecord->membership_uuid,
                                'role_id' => CommonMembership::getRoleId($fullRecord->role_id),
                            ]);
                    }

                    User::disableMassIndex();


                }
            }

        });
        return true;
    }

    protected function clear($user)
    {

        $duplicates = User::withoutGlobalScopes()->where('email', $user->email)->get();

        $this->output->progressStart($duplicates->count());

        if ($duplicates->count() > 1) {
            $notUsed = [];
            $used = [];
            foreach ($duplicates as $duplicate) {

                if ($this->isUsed($duplicate)) {
                    $used[] = $duplicate->id;
                } else {
                    $notUsed[] = $duplicate->id;
                }
                $this->output->progressAdvance();

            }
            if (empty($used)) {
                $used[] = array_shift($notUsed);
            }
            if (count($used) == 1) {
                DB::table('kiosk_users')
                    ->whereIntegerInRaw('user_id', $notUsed)
                    ->delete();

                DB::table('schools_users')
                    ->whereIntegerInRaw('user_id', $notUsed)
                    ->delete();
                DB::table('users')
                    ->whereIntegerInRaw('id', $notUsed)
                    ->update(['deleted_at' => now()]);
            }
        }


    }

    protected function hasPasses($user)
    {
        return !!Pass::where('user_id', $user->id)
            ->orWhere(function ($query) use ($user) {
                $query->where('from_type', User::class)->where('from_id', $user->id);
            })
            ->orWhere(function ($query) use ($user) {
                $query->where('to_type', User::class)->where('to_id', $user->id);
            })
            ->orWhere('requested_by', $user->id)
            ->orWhere('approved_by', $user->id)
            ->orWhere('completed_by', $user->id)
            ->orWhere('created_by', $user->id)
            ->first();
    }

    protected function hasFav($user)
    {
        return !!StudentFavorite::where('user_id', $user->id)
            ->orWhere(function ($query) use ($user) {
                $query->where('favorable_type', User::class)->where('favorable_id', $user->id);
            })
            ->first();
    }

    protected function hasUnavailables($user)
    {
        return !!Unavailable::where(function ($query) use ($user) {
            $query->where('unavailable_type', User::class)->where('unavailable_id', $user->id);
        })->first();
    }

    protected function hasAptPasses($user)
    {
        return !!AppointmentPass::where('created_by', $user->id)
            ->orWhere(function ($query) use ($user) {
                $query->where('from_type', User::class)->where('from_id', $user->id);
            })
            ->orWhere(function ($query) use ($user) {
                $query->where('to_type', User::class)->where('to_id', $user->id);
            })
            ->orWhere('confirmed_by', $user->id)
            ->orWhere('canceled_by', $user->id)
            ->orWhere('acknowledged_by', $user->id)
            ->first();
    }

    protected function isUsed($user)
    {
        return $this->hasPasses($user) || $this->hasAptPasses($user) || $this->hasUnavailables($user);

    }


}
