<?php

namespace App\Console;

use App\Console\Commands\AppointmentPassExpireCommand;
use App\Console\Commands\AppointmentPassRecurrenceCommand;
use App\Console\Commands\AppointmentPassScheduleCommand;
use App\Console\Commands\AppointmentPassScheduleEmailCommand;
use App\Console\Commands\BindTimezoneOffsetsOfSchoolCommand;
use App\Console\Commands\DeleteTempFilesFromStorageCommand;
use App\Console\Commands\DeleteTempSummaryReportsCommand;
use App\Console\Commands\EndPassesAtMidnightCommand;
use App\Console\Commands\FixDeprecatedSchoolTimezonesCommand;
use App\Console\Commands\FixFavoritesRoomsCommand;
use App\Console\Commands\LogoutUsersCommand;
use App\Console\Commands\MigrateAutoPassLimitCommand;
use App\Console\Commands\MigrateAutoPassPreferenceCommand;
use App\Console\Commands\Migrations\LinkParentPassCommand;
use App\Console\Commands\Migrations\MigrateActivePassLimitCommand;
use App\Console\Commands\Migrations\MigrateAppointmentCommand;
use App\Console\Commands\MigrateRecurrenceAppointmentCommand;
use App\Console\Commands\Migrations\MigrateAutoPassCommand;
use App\Console\Commands\Migrations\MigrateCommentCommand;
use App\Console\Commands\Migrations\MigrateFavoritesCommand;
use App\Console\Commands\Migrations\MigrateLocationCapacityCommand;
use App\Console\Commands\Migrations\MigratePassBlockCommand;
use App\Console\Commands\Migrations\MigratePeriodsCommand;
use App\Console\Commands\Migrations\MigrateModuleCommand;
use App\Console\Commands\Migrations\MigratePassCommand;
use App\Console\Commands\Migrations\MigratePassTimeSettingCommand;
use App\Console\Commands\Migrations\MigratePinCommand;
use App\Console\Commands\Migrations\MigratePolarityCommand;
use App\Console\Commands\Migrations\MigrateRoleCommand;
use App\Console\Commands\Migrations\MigrateRoomCommand;
use App\Console\Commands\Migrations\MigrateRoomPinCommand;
use App\Console\Commands\Migrations\MigrateSchoolCommand;
use App\Console\Commands\Migrations\MigrateSchoolUserCommand;
use App\Console\Commands\Migrations\LinkUsersToSchoolsCommand;
use App\Console\Commands\Migrations\MigrateStaffScheduleCommand;
use App\Console\Commands\PassBlockRunCommand;
use App\Console\Commands\PassExpiredCommand;
use App\Console\Commands\PassExtendedCommand;
use App\Console\Commands\PassLimitArchiveScheduleCommand;
use App\Console\Commands\PassLimitRecurrenceCommand;
use App\Console\Commands\PassMinTimeReachedCommand;
use App\Console\Commands\RemoveArchivedUsersCommand;
use App\Console\Commands\Rollback\PassesRollbackCommand;
use App\Console\Commands\Rollback\RollbackAdultsPinCommand;
use App\Console\Commands\Rollback\RollbackAutoPassCommand;
use App\Console\Commands\Rollback\RollbackPeriodsCommand;
use App\Console\Commands\Rollback\RollbackStaffScheduleCommand;
use App\Console\Commands\Rollback\RollbackTransparenciesCommand;
use App\Console\Commands\Rollback\RollbackTransparencyUserCommand;
use App\Console\Commands\Rollback\RollBackUserPreferencesCommand;
use App\Console\Commands\RoomsFixEmptyCommand;
use App\Console\Commands\RunEhpMigrationCommand;
use App\Console\Commands\UnavailableExpiredCommand;
use App\Console\Commands\UnavailableInTimeCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        PassExpiredCommand::class,
        AppointmentPassScheduleCommand::class,
        AppointmentPassScheduleEmailCommand::class,
        UnavailableExpiredCommand::class,
        AppointmentPassExpireCommand::class,
        AppointmentPassExpireCommand::class,
        PassLimitArchiveScheduleCommand::class,
        PassLimitRecurrenceCommand::class,
        DeleteTempFilesFromStorageCommand::class,
        AppointmentPassRecurrenceCommand::class,
        PassMinTimeReachedCommand::class,
        UnavailableInTimeCommand::class,
        PassExtendedCommand::class,
        MigrateSchoolCommand::class,
        MigrateModuleCommand::class,
        MigratePassTimeSettingCommand::class,
        MigratePeriodsCommand::class,
        MigrateRoleCommand::class,
        LinkUsersToSchoolsCommand::class,
        MigrateSchoolUserCommand::class,
        MigratePassCommand::class,
        RunEhpMigrationCommand::class,
        MigrateRoomCommand::class,
        MigrateLocationCapacityCommand::class,
        MigrateFavoritesCommand::class,
        MigrateRecurrenceAppointmentCommand::class,
        MigrateAppointmentCommand::class,
        MigrateCommentCommand::class,
        MigratePolarityCommand::class,
        MigratePinCommand::class,
        LinkParentPassCommand::class,
        MigrateAutoPassCommand::class,
        MigrateAutoPassPreferenceCommand::class,
        MigrateStaffScheduleCommand::class,
        MigrateRoomPinCommand::class,
        MigrateAutoPassLimitCommand::class,
        MigrateActivePassLimitCommand::class,
        MigratePassBlockCommand::class,
        FixDeprecatedSchoolTimezonesCommand::class,
        DeleteTempSummaryReportsCommand::class,
        RemoveArchivedUsersCommand::class,
        FixFavoritesRoomsCommand::class,
        RoomsFixEmptyCommand::class,
        LogoutUsersCommand::class,
        BindTimezoneOffsetsOfSchoolCommand::class,
        EndPassesAtMidnightCommand::class,
        PassesRollbackCommand::class,
        RollbackAdultsPinCommand::class,
        RollbackStaffScheduleCommand::class,
        RollbackAutoPassCommand::class,
        RollBackUserPreferencesCommand::class,
        RollbackTransparenciesCommand::class,
        RollbackTransparencyUserCommand::class,
        RollbackPeriodsCommand::class,
        PassBlockRunCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //        $schedule
        //            ->command('passes:expired')
        //            ->everyMinute();

        $schedule
            ->command('passes:mintime')
            ->everyMinute()
            ->onOneServer();

        $schedule
            ->command('passes:extended')
            ->everyMinute()
            ->onOneServer();

        $schedule
            ->command('passes:appointment_schedule')
            ->everyMinute()
            ->onOneServer();

        $schedule
            ->command('passes:appointment_email')
            ->everyThirtyMinutes()
            ->withoutOverlapping()
            ->timezone('America/New_York')
            ->between('05:00', '12:00')
            ->onOneServer();

        $schedule
            ->command('passes:appointment_expire')
            ->daily()
            ->onOneServer();
        $schedule
            ->command('passes:limits_archive')
            ->everyFiveMinutes()
            ->onOneServer();
        $schedule
            ->command('passes:limit_recurrence')
            ->daily()
            ->onOneServer();
        $schedule
            ->command('passes:appointment_recurrence_schedule')
            ->everyMinute()
            ->between('01:10', '20:30')
            ->onOneServer()
            ->withoutOverlapping();

        $schedule
            ->command('unavailables:expired')
            ->everyTwoMinutes()
            ->onOneServer();
        $schedule
            ->command('unavailables:in_time')
            ->everyMinute()
            ->onOneServer();
        $schedule
            ->command('storage:delete_temp')
            ->dailyAt('23:59')
            ->onOneServer();
        $schedule->command('summary:clean')->dailyAt('23:59');
        $schedule->command('users:archived')->dailyAt('23:59');
        $schedule->command('users:logout')->everyThirtyMinutes();
        $schedule->command('passes:midnight')->everyThirtyMinutes();
        $schedule->command('horizon:snapshot')->everyFiveMinutes();
        $schedule->command('room_restrictions:expire')->everySixHours();

        $schedule
            ->command('pass-blocks:run')
            ->everyMinute()
            ->onOneServer()
            ->withoutOverlapping();

        $schedule
            ->command('cus:sync --entity=schools')
            ->everyMinute()
            ->onOneServer()
            ->withoutOverlapping();

        $schedule
            ->command('cus:sync --entity=users --mins=5')
            ->everyTwoMinutes()
            ->onOneServer()
            ->withoutOverlapping();

        $schedule
            ->command('cus:resync --onchange=16 --action=create')
            ->everyFifteenMinutes()
            ->onOneServer()
            ->withoutOverlapping();

        $schedule
            ->command('cus:resync --onchange=11 --action=update')
            ->everyTenMinutes()
            ->onOneServer()
            ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
