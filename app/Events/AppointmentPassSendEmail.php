<?php

namespace App\Events;

use App\Models\AppointmentPass;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AppointmentPassSendEmail implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $appointmentPass;
    public $token;
    public $tokenAdmin;

    /**
     * Create a new event instance.
     *
     * @param $appointmentPass
     * @param string $token
     * @param string $tokenAdmin
     */
    public function __construct(
        $appointmentPass,
        string $token = '',
        string $tokenAdmin = ''
    ) {
        $this->appointmentPass = $appointmentPass;
        $this->token = $token;
        $this->tokenAdmin = $tokenAdmin;

        logger(
            'Token in the AppointmentPassSendMail Event' .
                $token .
                ' Current obj token: ' .
                $this->token .
                ' Admin Token: ' .
                $this->tokenAdmin
        );
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel
     */
    public function broadcastOn()
    {
        return new PrivateChannel(
            'appointments.passes.' . $this->appointmentPass->school_id
        );
    }
}
