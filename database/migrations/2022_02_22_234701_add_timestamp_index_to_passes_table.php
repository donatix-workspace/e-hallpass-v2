<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimestampIndexToPassesTable extends Migration
{
    public function up()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->index(
                [
                    'created_at',
                    'expired_at',
                    'pass_status',
                    'flagged_at',
                    'canceled_at',
                    'extended_at',
                    'approved_at'
                ],
                'timestamps_index'
            );
        });
    }

    public function down()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->dropIndex('timestamps_index');
        });
    }
}
