<?php

namespace App\Jobs;

use App\Events\NewUsersCreatedEvent;
use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Models\AuthType;
use App\Models\Role;
use App\Models\User;
use App\Models\UserImport;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class InsertUsersJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $csvUsers;
    public $authType;
    public $fileName;
    public $authenticatedUser;
    public $school;

    public function __construct($data)
    {
        $this->csvUsers = $data['users'];
        $this->authType = AuthType::nameToCode($data['authType']);
        $this->fileName = $data['fileName'];
        $this->authenticatedUser = $data['authenticatedUser'];
        $this->school = $data['school'];
    }

    public function handle()
    {

        $usersDataIds = [];
        $linkUsersToSchool = [];
        foreach ($this->csvUsers as $csvUser) {

            $roleId = $csvUser['role_id'] ?? Role::getRoleIdByName($csvUser['role']);
            $status = $csvUser['status'] == User::ACTIVE_STATUS_NAME ? User::ACTIVE : User::INACTIVE;

            $user = User::updateOrCreate(
                [
                    'email' => $csvUser['email'],
                ],
                [
                    'first_name' => $csvUser['first_name'],
                    'last_name' => $csvUser['last_name'],
                    'role_id' => $roleId,
                    'email_verified_at' => now(),
                    'status' => $status,
                    'school_id' => $this->school->id,
                    'grade_year' => $csvUser['grade_year'],
                    'auth_type' => $this->authType,
                    'is_substitute' => $csvUser['is_substitute'],
                    'created_by' => $this->authenticatedUser->id,
                ]
            );

            DB::table('schools_users')->updateOrInsert(
                [
                    'school_id' => $this->school->id,
                    'user_id' => $user->id,
                ],
                [
                    'role_id' => $roleId,
                ]);


            $usersDataIds[] = $user->id;
            if (!$user->cus_uuid) {
                CreateCommonUserJob::dispatch([
                    'user' => $user,
                    'authenticatedUser' => $this->authenticatedUser,
                    'password' => null,
                    'school' => $this->school,
                    'sendInvite' => true
                ]);
            } else {
                UpdateCommonUserJob::dispatch($user, $this->authenticatedUser);
            }


        }
        UserImport::markInserted(array_column($this->csvUsers, 'email'), $this->fileName);

        event(new NewUsersCreatedEvent($usersDataIds));
    }

}
