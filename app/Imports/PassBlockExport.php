<?php

namespace App\Imports;


use App\Models\PassBlock;
use App\Models\School;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PassBlockExport implements FromQuery, WithHeadings, WithMapping
{
    public $user;

    /**
     *
     */
    public function __construct()
    {
        $this->user = auth()->user();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Builder|Builder
     */
    public function query()
    {
        return PassBlock::where('school_id', optional($this->user)->school_id);
    }

    public function headings(): array
    {
        return [
            'Start Time',
            'End Time',
            'Reason',
        ];
    }

    public function map($row): array
    {
        $school = School::findCacheFirst($this->user->school_id);
        $fromDate = School::convertTimeToCorrectTimezone($row->from_date, $school->getTimezone());
        $toDate = School::convertTimeToCorrectTimezone($row->to_date, $school->getTimezone());

        return [
            Carbon::parse($fromDate)->format('M d, Y H:i:s'),
            Carbon::parse($toDate)->format('M d, Y H:i:s'),
            $row->reason
        ];
    }
}
