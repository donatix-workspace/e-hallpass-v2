<?php

namespace App\Rules;

use App\Models\Room;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class RoomOrUserStringRule implements Rule
{
    public const USER_TYPE = User::class;
    public const ROOM_TYPE = Room::class;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return self::USER_TYPE === $value || self::ROOM_TYPE === $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('passes.room.or.user.string.not.same');
    }
}
