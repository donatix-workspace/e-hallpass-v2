<?php

namespace App\Jobs;

use App\Models\PassSummaryReport;
use App\Models\Room;
use App\Models\TeacherPassSetting;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Throwable;

class AddPassSummaryInformationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $aggregator;
    public $schoolId;
    public $model;
    public $filter;
    public $fromDate;
    public $toDate;
    public $searchSessionId;

    public function __construct(
        $aggregator,
        int $schoolId,
        string $model,
        string $filter,
        $fromDate,
        $toDate,
        string $searchSessionId
    ) {
        $this->aggregator = unserialize($aggregator);
        $this->schoolId = $schoolId;
        $this->model = $model;
        $this->filter = $filter;
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
        $this->searchSessionId = $searchSessionId;
    }

    /**
     * @throws Throwable
     */
    public function handle()
    {
        $summaryReports = $this->aggregator;
        $model = $this->model;
        $dates = [
            'dates' => [
                'from' => $this->fromDate,
                'to' => $this->toDate
            ]
        ];

        $passSummaryReportInsertArray = [];
        $now = now();

        foreach ($summaryReports as $summaryReport) {
            $summaryKioskTime = empty($summaryReport['total_time_ksk'])
                ? '00:00:00'
                : str_replace('-', '', $summaryReport['total_time_ksk']);
            $summaryProxyTime = empty($summaryReport['total_time_proxy'])
                ? '00:00:00'
                : str_replace('-', '', $summaryReport['total_time_proxy']);
            $summaryAptTime = empty($summaryReport['total_time_apt'])
                ? '00:00:00'
                : str_replace('-', '', $summaryReport['total_time_apt']);
            $summaryStudentTime = empty($summaryReport['total_time_student'])
                ? '00:00:00'
                : str_replace('-', '', $summaryReport['total_time_student']);

            $totalTimeAllArray = [
                TeacherPassSetting::convertInSeconds($summaryAptTime),
                TeacherPassSetting::convertInSeconds($summaryProxyTime),
                TeacherPassSetting::convertInSeconds($summaryStudentTime),
                TeacherPassSetting::convertInSeconds($summaryKioskTime)
            ];

            if (
                $summaryReport['total_passes'] &&
                array_sum($totalTimeAllArray)
            ) {
                $passSummaryReportInsertArray[] = [
                    'destination_type' => $model,
                    'destination_id' =>
                        $model === Room::class
                            ? $summaryReport['room_id']
                            : $summaryReport['user_id'],
                    'total_passes' => $summaryReport['total_passes'],
                    'total_time_all' => PassSummaryReport::calculateTotalTime(
                        $totalTimeAllArray
                    ),
                    'selected_filter' => $this->filter,
                    'filters_dates' => json_encode($dates),
                    'total_time_ksk' => $summaryKioskTime,
                    'total_time_proxy' => $summaryProxyTime,
                    'total_time_student' => $summaryStudentTime,
                    'total_time_apt' => $summaryAptTime,
                    'school_id' => $this->schoolId,
                    'search_session_id' => $this->searchSessionId,
                    'last_synced_at' => $now,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }

//        logger('Summary Reports Insert Array: ', $passSummaryReportInsertArray);
//
        try {
            DB::beginTransaction();

            PassSummaryReport::insert($passSummaryReportInsertArray);

            PassSummaryReport::where('school_id', $this->schoolId)
                ->where('selected_filter', $this->filter)
                ->where('search_session_id', $this->searchSessionId)
                ->searchable();

            DB::commit();
        } catch (\Exception $e) {
            logger($e->getMessage());
        } catch (Throwable $e) {
            logger($e->getMessage());
        }
    }
}
