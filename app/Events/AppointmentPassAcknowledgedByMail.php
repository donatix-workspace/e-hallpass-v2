<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AppointmentPassAcknowledgedByMail implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $appointmentPassId;
    public $schoolId;

    /**
     * Create a new event instance.
     *
     * @param $appointmentPassId
     * @param $schoolId
     */
    public function __construct($appointmentPassId, $schoolId)
    {
        $this->appointmentPassId = $appointmentPassId;
        $this->schoolId = $schoolId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('appointments.passes.' . $this->schoolId);
    }
}
