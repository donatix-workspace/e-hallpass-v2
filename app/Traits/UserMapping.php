<?php

namespace App\Traits;

trait UserMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'id' => [
                'type' => 'long'
            ],
            'first_name' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'auth_type' => [
                'type' => 'keyword'
            ],
            'archived_date' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'auth_type_name' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'last_name' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'name' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'school_id' => [
                'type' => 'long'
            ],
            'email' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'grade_year' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'role_id' => [
                'type' => 'long'
            ],
            'is_substitute' => [
                'type' => 'long'
            ],
            'status' => [
                'type' => 'long'
            ],
            'deleted_at' => [
                'type' => 'keyword',
                'null_value' => '0'
            ],
            'allow_appointment_requests' => [
                'type' => 'long'
            ],
            'school_user' => [
                'type' => 'nested',
                'properties' => [
                    'id' => [
                        'type' => 'long'
                    ]
                ]
            ],
            'role_user' => [
                'type' => 'nested',
                'include_in_parent' => true,
                'properties' => [
                    'id' => [
                        'type' => 'long'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'pivot' => [
                        'properties' => [
                            'school_id' => [
                                'type' => 'long'
                            ],
                            'user_id' => [
                                'type' => 'long'
                            ],
                            'status' => [
                                'type' => 'long'
                            ],
                            'deleted_at' => [
                                'type' => 'keyword',
                                'null_value' => 0
                            ],
                            'archived_at' => [
                                'type' => 'keyword',
                                'null_value' => 0
                            ],
                            'archived_by' => [
                                'type' => 'keyword',
                                'null_value' => 0
                            ]
                        ]
                    ]
                ]
            ],
            'role' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'created_at' => [
                'type' => 'date',
                'format' =>
                    'hour_minute||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                        'null_value' => '-'
                    ]
                ]
            ],
            'archived_at' => [
                'type' => 'date',
                'format' => 'MM/dd/yyyy',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                        'null_value' => '-'
                    ]
                ]
            ],
            'updated_at' => [
                'type' => 'date',
                'format' =>
                    'hour_minute||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis'
            ],
            'web_last_login_at' => [
                'type' => 'date',
                'format' => 'MM-dd-yyyy / h:m:ss a',
                'null_value' => '01-01-1999 / 11:59:59 PM',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                        'null_value' => 'None'
                    ]
                ]
            ],
            'mobile_last_login_at' => [
                'type' => 'date',
                'format' => 'MM-dd-yyyy / h:m:ss a',
                'null_value' => '01-01-1999 / 11:59:59 PM',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                        'null_value' => 'None'
                    ]
                ]
            ],
            'mobile_last_used_at' => [
                'type' => 'date',
                'format' => 'MM-dd-yyyy / h:m:ss a',
                'null_value' => '01-01-1999 / 11:59:59 PM',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                        'null_value' => 'None'
                    ]
                ]
            ]
        ]
    ];
}
