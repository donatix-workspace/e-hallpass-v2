<?php

namespace App\Imports;


use App\Models\Room;
use App\Models\School;
use App\Models\Unavailable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UnavailableImport implements FromQuery, WithHeadings, WithMapping
{
    public function query()
    {
        $user = auth()->user();
        if (request()->has('search_query') && request()->get('search_query') !== null || request()->has('sort')) {
            return Unavailable::searchableQueries(null, Unavailable::recordsPerPage(), false);
        } else {
            return Unavailable::with('unavailable')
                ->orderBy('from_date', 'desc')
                ->fromSchoolId($user->school_id);
        }

    }

    public function headings(): array
    {
        return [
            'Teacher/Location',
            'Start',
            'End',
            'Recurrence',
            'Comment'
        ];
    }

    public function map($row): array
    {
        $school = School::findCacheFirst($row->school_id);

        return [
            $row->unavailable_type === Room::class ? $row->unavailable->name : optional($row->unavailable)->first_name . ' ' . optional($row->unavailable)->last_name,
            School::convertTimeToCorrectTimezone($row->from_date, $school->getTimezone(), false, false, 'm/d/Y g:i A'),
            School::convertTimeToCorrectTimezone($row->to_date, $school->getTimezone(), false, false, 'm/d/Y g:i A'),
            $row->recurrence_type,
            $row->comment
        ];
    }
}
