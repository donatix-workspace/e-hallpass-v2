<?php

namespace App\Providers;

use App\Events\AppointmentPassCancelled;
use App\Events\AppointmentPassCreated;
use App\Events\AppointmentPassSendEmail;
use App\Events\AppointmentPassTimeChanged;
use App\Events\DashboardRefreshEvent;
use App\Events\NewUsersCreatedEvent;
use App\Events\PassApproved;
use App\Events\PassArrived;
use App\Events\PassBlockRunnedEvent;
use App\Events\PassCanceled;
use App\Events\PassCreated;
use App\Events\PassEnded;
use App\Events\PassExpired;
use App\Events\PassInOut;
use App\Events\PassMinTimeReached;
use App\Events\PassReturning;
use App\Listeners\AppointmentPassCanceledListener;
use App\Listeners\AppointmentPassCreatedListener;
use App\Listeners\AppointmentPassTimeChangeListener;
use App\Listeners\DashboardRefreshEventListener;
use App\Listeners\LogNotification;
use App\Listeners\NewUsersCreatedListener;
use App\Listeners\PassApprovedNotification;
use App\Listeners\PassArrivedListener;
use App\Listeners\PassArrivedNotification;
use App\Listeners\PassBlockRunnedEventListener;
use App\Listeners\PassCanceledListener;
use App\Listeners\PassCreatedNotification;
use App\Listeners\PassEndedListener;
use App\Listeners\PassEndedNotification;
use App\Listeners\PassExpiredEventListener;
use App\Listeners\PassInOutNotification;
use App\Listeners\PassMinTimeReachedListener;
use App\Listeners\PassReturningNotification;
use App\Listeners\SendEmailOnAppointmentPass;
use App\Models\Comment;
use App\Observers\CommentObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Notifications\Events\NotificationSent;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [SendEmailVerificationNotification::class],
        PassApproved::class => [PassApprovedNotification::class],
        PassCreated::class => [PassCreatedNotification::class],
        PassEnded::class => [
            PassEndedNotification::class,
            PassEndedListener::class
        ],
        PassInOut::class => [PassInOutNotification::class],
        PassReturning::class => [PassReturningNotification::class],
        PassArrived::class => [
            PassArrivedNotification::class,
            PassArrivedListener::class
        ],
        AppointmentPassSendEmail::class => [SendEmailOnAppointmentPass::class],
        AppointmentPassCancelled::class => [
            AppointmentPassCanceledListener::class
        ],
        AppointmentPassTimeChanged::class => [
            AppointmentPassTimeChangeListener::class
        ],
        NewUsersCreatedEvent::class => [NewUsersCreatedListener::class],
        AppointmentPassCreated::class => [
            AppointmentPassCreatedListener::class
        ],
        PassExpired::class => [PassExpiredEventListener::class],
        NotificationSent::class => [LogNotification::class],
        PassCanceled::class => [PassCanceledListener::class],
        PassMinTimeReached::class => [PassMinTimeReachedListener::class],
        PassBlockRunnedEvent::class => [PassBlockRunnedEventListener::class],
        DashboardRefreshEvent::class => [DashboardRefreshEventListener::class]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Add comments to the Pass elasticsearch object dynamically.
         */
        Comment::observe(CommentObserver::class);
    }
}
