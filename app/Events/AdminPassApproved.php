<?php

namespace App\Events;

use App\Http\Resources\PassResource;
use App\Models\Pass;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AdminPassApproved implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $pass;
    public $notifyAdultsIds;

    /**
     * Create a new event instance.
     *
     * @param Pass $pass
     */
    public function __construct(Pass $pass)
    {
        $this->notifyAdultsIds = $pass->getNotifiableAdults();
        $this->pass = new PassResource($pass);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('school.passes.' . $this->pass->school_id),
        ];
    }
}
