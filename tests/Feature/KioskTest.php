<?php

namespace Tests\Feature;

use App\Models\Polarity;
use App\Models\School;
use App\Models\User;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class KioskTest extends TestCase
{
    use RefreshDatabase;

    protected $adminUser;
    protected $firstUser;
    protected $secondUser;
    protected $school;
    protected $polarity;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();
        // Create example school
        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);
        $this->secondUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);
        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);
    }

    /** @test */
    /* An admin can view kiosk index page */
    public function an_admin_can_view_kiosk_index_page()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.kiosk'))
            ->assertJsonStructure(['data', 'status']);
    }
}
