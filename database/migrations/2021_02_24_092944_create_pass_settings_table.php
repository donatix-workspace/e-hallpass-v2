<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePassSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pass_settings', function (Blueprint $table) {
            $table->id();
            $table->string('type', 45);
            $table->mediumText('description');
            $table->unsignedBigInteger('school_id');
            $table->boolean('status');
            $table->timestamps();
        });

        Schema::table('pass_settings', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pass_settings', function (Blueprint $table) {
            $table->dropForeign('pass_settings_school_id_foreign');
        });
        Schema::dropIfExists('pass_settings');
    }
}
