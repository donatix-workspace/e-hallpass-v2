<?php

namespace App\Console\Commands;

use App\Models\AutoPass;
use App\Models\AutoPassPreference;
use Illuminate\Console\Command;

class MigrateAutoPassPreferenceCommand extends Command
{
    protected $signature = 'migrate_sys:auto_pass_preference';

    protected $description = 'Migrate preferences from ehpV1 to EhpV2';

    public function handle()
    {
        $this->info('--- Migrating Auto Passes Preferences ---');

        $newDb = \DB::connection('mysql');
        $oldDb = \DB::connection('oldDB');

        $oldDb->disableQueryLog();
        $newDb->disableQueryLog();

        $autoPassesPreference = $oldDb->table('auto_pass_preference')
            ->orderBy('created_at', 'asc')
            ->get()
            ->toArray();

        $this->output->progressStart(count($autoPassesPreference));

        $autoPassesPreferenceChunks = array_chunk($autoPassesPreference, 1500);

        foreach ($autoPassesPreferenceChunks as $autoPassPreferenceChunk) {
            $insertAutoPassesChunk = [];
            foreach ($autoPassPreferenceChunk as $autoPassPreference) {
                $oldSchool = $oldDb->table('school_schedule')->where('school_id', $autoPassPreference->school_id)
                    ->first();

                if ($oldSchool) {
                    $newSchool = $newDb->table('schools')->select('id')->where('school_domain', $oldSchool->school_domain)->first();

                    if ($newSchool) {
                        $oldRoom = $oldDb->table('rooms')->select('room_name')
                            ->where('room_id', optional($autoPassPreference)->room_id)
                            ->where('school_id', $oldSchool->school_id)
                            ->first();

                        $newRoom = $newDb->table('rooms')->where('name', optional($oldRoom)->room_name)
                            ->select('id')
                            ->where('school_id', $newSchool->id)
                            ->first();

                        $oldUser = $oldDb->table('users')->where('user_id', optional($autoPassPreference)->teacher_id)
                            ->select('email')
                            ->where('school_id', optional($autoPassPreference)->school_id)
                            ->first();

                        $newUser = $newDb->table('users')->where('email', optional($oldUser)->email)
                            ->select('id')
                            ->where('school_id', $newSchool->id)
                            ->first();

                        $autoPass = $newDb->table('auto_passes')
                            ->select('id')
                            ->where('room_id', optional($newRoom)->id)
                            ->where('school_id', optional($newSchool)->id)
                            ->first();

                        if ($newRoom && $newUser && $autoPass) {
                            $insertAutoPassesChunk[] = [
                                'auto_pass_id' => $autoPass->id,
                                'school_id' => $newSchool->id,
                                'user_id' => $newUser->id,
                                'status' => $autoPassPreference->status,
                                'mode' => $autoPassPreference->mode ? AutoPassPreference::MODE_START_AND_STOP : AutoPassPreference::MODE_START_ONLY,
                                'created_at' => now(),
                                'updated_at' => now()
                            ];
                        }
                    }
                }
            }

            try {
                $newDb->table('auto_passes_preferences')
                    ->insert($insertAutoPassesChunk);
                $this->output->progressAdvance(count($insertAutoPassesChunk));
            } catch (\Exception $e) {
                $this->info($e->getMessage());
            }
        }

        $this->output->progressFinish();
    }
}
