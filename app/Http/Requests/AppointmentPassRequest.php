<?php

namespace App\Http\Requests;

use App\Models\Pass;
use App\Rules\CheckIfItsMidnightRule;
use App\Rules\DateIsPassBlockedRule;
use App\Rules\DaysKeyExistRule;
use App\Rules\PassRoomOrUserExistRule;
use App\Rules\RecurrenceDaysRule;
use App\Rules\RecurrenceMonthlyCorrectStructureRule;
use App\Rules\RecurrenceTypeRule;
use App\Rules\RoomOrUserExistsAppointmentRule;
use App\Rules\RoomOrUserExistsRule;
use App\Rules\RoomOrUserStringRule;
use App\Rules\TimeAfterNowRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class AppointmentPassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() &&
            (auth()
                ->user()
                ->isAdmin() ||
                auth()
                    ->user()
                    ->isTeacher() ||
                auth()
                    ->user()
                    ->isStaff());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'appointments' => 'required|array|min:1',
            'appointments.*.from_id' => [
                'integer',
                new RoomOrUserExistsAppointmentRule(),
                'required'
            ],
            'appointments.*.to_id' => [
                'integer',
                new RoomOrUserExistsAppointmentRule(),
                'required'
            ],
            'appointments.*.from_type' => [
                'required',
                'string',
                new RoomOrUserStringRule()
            ],
            'appointments.*.to_type' => [
                'required',
                'string',
                new RoomOrUserStringRule()
            ],
            'appointments.*.for_date' => ['required', 'date'],
            'appointments.*.for_time' => [
                'required',
                'date_format:H:i',
                new CheckIfItsMidnightRule()
            ],
            'appointments.*.period_id' => 'required|exists:periods,id|integer',
            'appointments.*.reason' => 'required|max:255|string',
            'appointments.*.user_id' => [
                'required',
                'integer',
                Rule::exists('users', 'id')->where(function ($query) {
                    $query
                        ->where('school_id', auth()->user()->school_id)
                        ->where('status', 1)
                        ->where('role_id', config('roles.student'));
                })
            ],
            'appointments.*.recurrence_type' => [
                'nullable',
                new RecurrenceTypeRule()
            ],
            'appointments.*.recurrence_days' => [
                'json',
                'nullable',
                new DaysKeyExistRule(),
                new RecurrenceDaysRule()
            ],
            'appointments.*.recurrence_week' => [
                'json',
                'nullable',
                'required_if:recurrence_type,monthly',
                new RecurrenceMonthlyCorrectStructureRule()
            ],
            'appointments.*.recurrence_end_at' =>
                'date|after:yesterday|nullable|after:appointments.*.for_date'
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'appointments.*.for_date.after' =>
                'Please provide valid time and date.',
            'appointments.*.from_time.after' =>
                'Please provide valid time and date.',
            'appointments.*.for_date.after_or_equal' =>
                'The Date and Time must be after or equal now.'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validatorOnTime = \Illuminate\Support\Facades\Validator::make(
            $this->all(),
            [
                'appointments.*.for_date' => 'after_or_equal:now -1 minute'
            ]
        );

        $validator->after(function ($validator) use ($validatorOnTime) {
            if ($validatorOnTime->fails()) {
                $validator
                    ->errors()
                    ->add(
                        'for_time',
                        'The time must be after or equal to now!'
                    );
            }
        });
    }
}
