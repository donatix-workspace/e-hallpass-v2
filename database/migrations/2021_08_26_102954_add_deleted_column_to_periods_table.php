<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeletedColumnToPeriodsTable extends Migration
{
    public function up()
    {
        Schema::table('periods', function (Blueprint $table) {
            $table->boolean('deleted')
                ->default(0)
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('periods', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });
    }
}
