<?php

namespace App\Elastic\Rules;

use App\Models\School;
use ScoutElastic\Builders\SearchBuilder;
use ScoutElastic\SearchRule;

class UserActiveByConditionRule extends SearchRule
{
    private $serviceSearchType;
    private $authenticatedUser;

    public function __construct(SearchBuilder $builder)
    {
        $this->serviceSearchType = request()->get('type');
        $this->authenticatedUser =
            $this->serviceSearchType !== 'kiosk'
                ? auth()->user()
                : School::where('cus_uuid', request()->get('cus_uuid'))
                    ->select('id')
                    ->firstOrFail();
        parent::__construct($builder);
    }
    /**
     * @inheritdoc
     */
    public function buildHighlightPayload()
    {
        //
    }

    /**
     * @inheritdoc
     */
    public function buildQueryPayload(): array
    {
        if (
            auth()
                ->user()
                ->isStudent() ||
            request()->has('active_only')
        ) {
            $this->builder->wheres['filter'][] = [
                'nested' => [
                    'path' => 'role_user',
                    'query' => [
                        'bool' => [
                            'must' => [
                                [
                                    'match' => [
                                        'role_user.pivot.school_id' =>
                                            $this->serviceSearchType === 'kiosk'
                                                ? $this->authenticatedUser->id
                                                : $this->authenticatedUser
                                                    ->school_id
                                    ]
                                ],
                                [
                                    'match' => [
                                        'role_user.pivot.status' => 1
                                    ]
                                ],
                                [
                                    'match' => [
                                        'role_user.pivot.deleted_at' => 0
                                    ]
                                ],
                                [
                                    'match' => [
                                        'role_user.pivot.archived_at' => 0
                                    ]
                                ]
                            ],
                            'must_not' => []
                        ]
                    ]
                ]
            ];
        }

        if ($this->builder->query === '__all') {
            return [
                'must' => ['match_all' => (object) []]
            ];
        }

        return [
            'must' => [
                'query_string' => [
                    'query' => $this->builder->query
                ]
            ]
        ];
    }
}
