<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TransparencyUserRequest extends FormRequest
{
    public function rules()
    {
        return [
            'user_ids' => 'array|required',
            'user_ids.*' => ['required', 'integer', Rule::exists('users', 'id')->where('school_id', auth()->user()->school_id)]
        ];
    }

    public function authorize(): bool
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}
