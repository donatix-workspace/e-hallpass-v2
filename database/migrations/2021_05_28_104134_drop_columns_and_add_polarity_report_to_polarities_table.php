<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsAndAddPolarityReportToPolaritiesTable extends Migration
{
    public function up()
    {
        Schema::table('polarities', function (Blueprint $table) {
            $table->dropColumn('times_triggered');
            $table->dropColumn('attempted_at');
        });
        Schema::create('polarities_reports', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->morphs('destination');
            $table->unsignedBigInteger('out_user_id');
            $table->enum('prevented_from', ['web', 'mobile', 'kiosk']);
            $table->timestamp('prevented_at');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('out_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('polarities_reports');
    }
}
