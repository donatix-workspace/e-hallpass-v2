<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Translators\SchoolTranslator;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class ClearDuplicateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:clear {--sisid=} {--from=} {--to=}';
    protected $client;
    protected $adminUser;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear Duplicate Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->adminUser = User::where('email', 'viktor.lalev@donatix.net')
            ->latest()
            ->first();
        $this->client = new CommonUserService($this->adminUser);

        if ($this->option('sisid')) {
            $this->info('Clear duplicates in v2 school per sis_id');

            $param = $this->option('sisid');
            $from = $this->option('from') ?? 0;
            $to = $this->option('to') ?? 3000;

            if ($param == 'all') {
                $this->info("Clear from {$from} to {$to}");

                $schools = \App\Models\V1\School::where('id', '>', $from)
                    ->where('id', '<=', $to)
                    ->whereNotNull('old_id')->get();

            } elseif (strpos($param, ",") !== false) {

                $ids = explode(',', $param);

                $schools = School::whereIn('id', $ids)->whereNotNull('old_id')->get();
            } else {
                $schools = School::where('id', $param)->whereNotNull('old_id')->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Claer for {$school->name} with id {$school->id}");
                $this->clearSisid($school);
            }

        }

    }

    protected function clearSisid($school)
    {
        $duplicatedSysIds = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select('u.student_sis_id')
            ->where('m.school_id', $school->id)
            ->where('m.role_id', 1)
            ->whereNotNull('u.student_sis_id')
            ->where('u.student_sis_id', '!=', 0)
            ->where('u.student_sis_id', '!=', '')
            ->groupBy('u.student_sis_id')
            ->havingRaw('count(u.student_sis_id) = 2')
            ->get()
            ->pluck('student_sis_id');

        $this->output->progressStart(count($duplicatedSysIds));

        foreach ($duplicatedSysIds as $duplicatedSysId) {

            $oldUser = \App\Models\V1\User::select('email', 'username', 'user_id', 'clever_id', 'firstname', 'lastname')
                ->where('school_id', $school->old_id)
                ->where('student_sis_id', $duplicatedSysId)
                ->first();
            if (!$oldUser) {
                return true;
            }

            $user = User::withoutGlobalScopes()
                ->where('school_id', $school->id)
                ->select('id', 'cus_uuid', 'email', 'username', 'old_id', 'clever_id', 'common_user_id', 'first_name', 'last_name')
                ->where('student_sis_id', $duplicatedSysId)
                ->where('email', $oldUser->email)
                ->first();

            if ($user) {
                $membership = DB::table('schools_users')
                    ->where('user_id', $user->id)
                    ->where('school_id', $school->id)
                    ->first();

                if (!$membership) {
                    $oldUser->migrateToV2andCus();
                }

                $duplicateUser = User::withoutGlobalScopes()
                    ->select('id', 'cus_uuid', 'email', 'username', 'old_id', 'clever_id', 'common_user_id', 'first_name', 'last_name')
                    ->where('school_id', $school->id)
                    ->where('id', '!=', $user->id)
                    ->where('student_sis_id', $duplicatedSysId)
                    ->first();

                if ($duplicateUser) {
                    $duplicatedMembership = DB::table('schools_users')
                        ->where('user_id', $duplicateUser->id)
                        ->where('school_id', $school->id)
                        ->first();

                    if ($duplicatedMembership) {
                        $this->deleteMembership($duplicatedMembership->membership_uuid);
                    }
                    $duplicateUser->update([
                        'deleted_at' => now(),
                        'status' => 0
                    ]);
                }
            }


            $this->output->progressAdvance();
        }
    }

    protected function deleteMembership($membershipUuid)
    {
        $response = $this->client->deleteUser($membershipUuid);

        \Illuminate\Support\Facades\DB::table('schools_users')
            ->where('membership_uuid', $membershipUuid)
            ->update([
                'deleted_at' => now(),
                'status' => 0
            ]);
        return true;
    }


}
