<?php


namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class QueryFilters
{
    protected $request;

    /**@var \Illuminate\Database\Query\Builder $builder **/
    protected $builder;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        foreach ($this->filters() as $name => $value) {
            if (!method_exists($this, $name)) {
                continue;
            }

            if (!is_array($value) && strlen($value)) {
                $this->$name($value);
            } else if(!is_array($value) && !strlen($value)){
                $this->$name();
            }

            if(is_array($value)) {
                $this->$name($value);
            }

        }
        return $this->builder;
    }

    public function filters()
    {
        return $this->request->all();
    }
}
