<?php


namespace App\Http\Services\CommonUserService\Translators;


use App\Http\Services\CommonUserService\Models\CommonMembership;
use Carbon\Carbon;

class UserTranslator
{
    public static function translateDb($user, $membership, $school)
    {
        return [
            "email" => $user->email ?? null,
            "student_sis_id" => $membership->sis_id ?? '',
            "username" => $user->username,
            "password" => $user->password ?? '',
            "first_name" => $user->first_name,
            "last_name" => $user->last_name,
            "role_id" => CommonMembership::getRoleId($membership->common_bm_role_membership_id),
            "avatar" => $user->image ?? null,
            "grade_year" => $membership->graduation_year,
            "status" => $user->r_status ? $membership->r_status : false,
//            "auth_type"=>$membership["user"]["auth_type"],
            "audio_enabled" => false,
            "is_substitute" => CommonMembership::isSubstitute($membership->common_bm_role_membership_id),
            "web_last_login_at" => $user->last_login_time,
            "created_at" => $user->created,
            "updated_at" => $user->modified,
            'school_id' => $school->id
        ];

    }

    protected static function convertTime($time = null)
    {
        return $time ? Carbon::createFromFormat("Y-m-d\TH:i:s\Z", $time) : Carbon::now();
    }

    public static function translateJoinedRecord($record, $school)
    {
        $data = [
            "cus_uuid" => $record->user_uuid,
            "email" => $record->email,
            "student_sis_id" => $record->student_sis_id ?? '',
            "user_initials" => $record->title,
//            "username" => $record->username,
            "first_name" => $record->first_name,
            "last_name" => $record->last_name,
            "role_id" => CommonMembership::getRoleId($record->role_id),
            "avatar" => $record->avatar ?? null,
            "grade_year" => $record->grade_year,
            "status" => $record->user_status,
            "audio_enabled" => false,
            "is_substitute" => CommonMembership::isSubstitute($record->role_id),
            "web_last_login_at" => $record->last_login_time,
            "created_at" => $record->created_at,
            "updated_at" => $record->updated_at,
            "school_id" => $school ? $school->id : null,
            "common_user_id" => $record->common_user_id,
//            "deleted_at"=>$record->deleted_at
        ];
        if ($record->user_status) {
            $data['deleted_at'] = null;
            $data['archived_at'] = null;
        }
        if ($record->title) {
            $data['user_initials'] = $record->title;
        }
        return $data;

    }
}
