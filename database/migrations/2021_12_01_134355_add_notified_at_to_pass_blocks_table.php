<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNotifiedAtToPassBlocksTable extends Migration
{
    public function up()
    {
        Schema::table('pass_blocks', function (Blueprint $table) {
            $table->timestamp('notified_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('pass_blocks', function (Blueprint $table) {
            $table->dropColumn('notified_at');
        });
    }
}
