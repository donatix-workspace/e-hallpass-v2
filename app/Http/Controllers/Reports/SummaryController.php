<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Http\Filters\SummaryReportFilter;
use App\Http\Requests\SummaryFilterRequest;
use App\Http\Requests\SummaryResultsRequest;
use App\Http\Resources\PassSummaryReportCollection;
use App\Imports\SummaryImports;
use App\Jobs\AddPassSummaryInformationJob;
use App\Models\Pass;
use App\Models\PassSummaryReport;
use App\Models\Room;
use App\Models\User;
use Carbon\Carbon;
use Excel;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class SummaryController extends Controller
{
    /**
     * Run Summary Report Filter
     *
     * @bodyParam aggregate_by string required Select from what to aggregate. Example: highest_pass_takers, highest_pass_granters, highest_pass_destination
     * @bodyParam teacher_ids array The ids of the teachers.
     * @bodyParam student_ids array The ids of the students.
     * @bodyParam room_ids array The ids of the rooms.
     * @bodyParam by_pass_types string required Show only by selected pass type Example: ALL, PRX, STU, APT, KSK
     * @bodyParam by_grade_years array Show only by selected grade year.
     * @bodyParam from_date date required From date.
     * @bodyParam to_date date required To Date.
     *
     *
     * @authenticated
     * @param SummaryReportFilter $filter
     * @param SummaryFilterRequest $request
     * @return JsonResponse|BinaryFileResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function filter(
        SummaryReportFilter $filter,
        SummaryFilterRequest $request
    ) {
        $user = auth()->user();
        $aggregators = null;
        $aggregatorsInfo = PassSummaryReport::summaryModelBindByType(
            $request->input('aggregate_by')
        );

        $aggregatorsBy = $request->input('aggregate_by');

        $model = $aggregatorsInfo['model'];

        $selectors = $aggregatorsInfo['selects'];

        $table = $aggregatorsInfo['table'];

        $fromDate = Carbon::parse($request->input('from_date'))
            ->startOfDay()
            ->toDateTimeString();
        $toDate = Carbon::parse($request->input('to_date'))
            ->endOfDay()
            ->toDateTimeString();

        $aggregators = $model
            ::where('school_id', $user->school_id)
            ->setEagerLoads([])
            ->select($selectors)
            ->from($table);

        $searchSessionId = Str::uuid();

        if (
            $request->input('aggregate_by') ===
            PassSummaryReport::HIGHEST_PASS_TAKERS
        ) {
            $aggregators->where('role_id', '=', config('roles.student'));
        }

        if (
            $request->input('aggregate_by') ===
            PassSummaryReport::HIGHEST_PASS_GRANTERS
        ) {
            $aggregators->where('role_id', '!=', config('roles.student'));
        }

        $aggregators->filter($filter);

        if ($request->input('by_pass_types') == 'ALL') {
            $aggregators = PassSummaryReport::allTypeAggregatorQueryAdd(
                $aggregators,
                $aggregatorsBy,
                $model,
                $user,
                $table,
                $fromDate,
                $toDate
            );

            $summaryArray = $aggregators
                ->limit(config('pagination.max_per_page'))
                ->get()
                ->toArray();

            AddPassSummaryInformationJob::dispatchSync(
                serialize($summaryArray),
                $user->school_id,
                $model,
                $request->input('by_pass_types'),
                $fromDate,
                $toDate,
                $searchSessionId
            );
        }

        if ($request->input('by_pass_types') == Pass::STUDENT_CREATED_PASS) {
            $aggregators = PassSummaryReport::studentTypeAggregatorQueryAdd(
                $aggregators,
                $aggregatorsBy,
                $model,
                $user,
                $table,
                $fromDate,
                $toDate
            );

            $summaryArray = $aggregators
                ->limit(config('pagination.max_per_page'))
                ->get()
                ->toArray();

            AddPassSummaryInformationJob::dispatchSync(
                serialize($summaryArray),
                $user->school_id,
                $model,
                $request->input('by_pass_types'),
                $fromDate,
                $toDate,
                $searchSessionId
            );
        }

        if ($request->input('by_pass_types') == Pass::APPOINTMENT_PASS) {
            $aggregators = PassSummaryReport::appointmentTypeAggregatorQueryAdd(
                $aggregators,
                $aggregatorsBy,
                $model,
                $user,
                $table,
                $fromDate,
                $toDate
            );

            $summaryArray = $aggregators
                ->limit(config('pagination.max_per_page'))
                ->get()
                ->toArray();

            AddPassSummaryInformationJob::dispatchSync(
                serialize($summaryArray),
                $user->school_id,
                $model,
                $request->input('by_pass_types'),
                $fromDate,
                $toDate,
                $searchSessionId
            );
        }

        if ($request->input('by_pass_types') == Pass::PROXY_PASS) {
            $aggregators = PassSummaryReport::proxyTypeAggregatorQueryAdd(
                $aggregators,
                $aggregatorsBy,
                $model,
                $user,
                $table,
                $fromDate,
                $toDate
            );

            $summaryArray = $aggregators->get()->toArray();

            AddPassSummaryInformationJob::dispatchSync(
                serialize($summaryArray),
                $user->school_id,
                $model,
                $request->input('by_pass_types'),
                $fromDate,
                $toDate,
                $searchSessionId
            );
        }

        if ($request->input('by_pass_types') == Pass::KIOSK_PASS) {
            $aggregators = PassSummaryReport::kioskTypeAggregatorQueryAdd(
                $aggregators,
                $aggregatorsBy,
                $model,
                $user,
                $table,
                $fromDate,
                $toDate
            );

            $summaryArray = $aggregators
                ->limit(config('pagination.max_per_page'))
                ->get()
                ->toArray();

            AddPassSummaryInformationJob::dispatchSync(
                serialize($summaryArray),
                $user->school_id,
                $model,
                $request->input('by_pass_types'),
                $fromDate,
                $toDate,
                $searchSessionId
            );
        }

        return response()->json([
            'data' => [
                'search_id' => $searchSessionId
            ],
            'status' => __('success'),
            'message' => __('The pass summary is generated successfully.')
        ]);
    }

    /**
     * Run Summary Report Filter
     *
     * @param SummaryResultsRequest $request
     * @return PassSummaryReportCollection|BinaryFileResponse
     */
    public function results(SummaryResultsRequest $request)
    {
        $user = auth()->user();
        $currentSearchSessionId = $request->input('search_id');

        if ($request->has('sort') || $request->has('search_query')) {
            $searchQuery = convertElasticQuery(
                $request->input('search_query') === null
                    ? config('scout_elastic.all_records')
                    : $request->input('search_query')
            );

            $passSummary = PassSummaryReport::search($searchQuery)
                ->where('school_id', $user->school_id)
                ->select('id')
                ->where('search_session_id', $currentSearchSessionId)
                ->when($request->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('id', 'asc');
                    }
                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                    return $query->orderBy('id', 'asc');
                })
                ->take(
                    PassSummaryReport::whereSearchSessionId(
                        $currentSearchSessionId
                    )
                        ->where('school_id', $user->school_id)
                        ->count()
                )
                ->get();

            $passSummaryIds = collect($passSummary)
                ->pluck('id')
                ->toArray();

            $passSummaryIdsImploded = implode(',', $passSummaryIds);

            $passSummary = PassSummaryReport::where(
                'school_id',
                $user->school_id
            )
                ->whereIntegerInRaw('id', $passSummaryIds)
                ->orderByRaw("field(id, $passSummaryIdsImploded)")
                ->where('search_session_id', $currentSearchSessionId);
        } else {
            $passSummary = PassSummaryReport::where(
                'school_id',
                $user->school_id
            )
                ->orderBy('total_passes', 'desc')
                ->where('search_session_id', $currentSearchSessionId);
        }

        if ($request->has('export_csv')) {
            try {
                return Excel::download(
                    new SummaryImports($passSummary),
                    'summary.csv'
                );
            } catch (\PhpOffice\PhpSpreadsheet\Writer\Exception | \PhpOffice\PhpSpreadsheet\Exception $e) {
                logger($e->getMessage());
            }
        }

        return new PassSummaryReportCollection(
            $passSummary->paginate(PassSummaryReport::recordsPerPage())
        );
    }
}
