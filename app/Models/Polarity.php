<?php

namespace App\Models;

use App\Elastic\PolaritiesIndexConfigurator;
use App\Jobs\InsertPolarityReportRecordsJob;
use App\Traits\ExtendedSearchable;
use App\Traits\PerPage;
use App\Traits\PolarityMapping;
use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Agent\Agent;
use ScoutElastic\Searchable;

class Polarity extends Model
{
    use HasFactory, ExtendedSearchable, PerPage, PolarityMapping, SoftDeletes;

    protected $appends = ['times_triggered'];
    protected $indexConfigurator = PolaritiesIndexConfigurator::class;

    protected $fillable = [
        'first_user_id',
        'second_user_id',
        'status',
        'school_id',
        'message'
    ];

    protected $with = ['firstUser', 'secondUser'];

    /**
     * Active polarity
     * @var integer
     */
    public const POLARITY_STATUS_ACTIVE = 1;

    /**
     * Deactive polarity
     * @var integer
     */
    public const POLARITY_STATUS_INACTIVE = 0;

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::POLARITY_STATUS_ACTIVE);
    }

    /**
     * @return array
     */
    public function toSearchableArray(): array
    {
        $polarity = $this->toArray();

        $polarity['created_at'] = Carbon::parse(
            $polarity['created_at']
        )->format('m/d/Y');
        $polarity['status_string'] = $polarity['status']
            ? 'Active'
            : 'InActive';

        return $polarity;
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @param $query
     * @param $user
     * @return mixed
     */
    public function scopeOfUserId($query, $user)
    {
        return $query
            ->where('first_user_id', $user)
            ->orWhere('second_user_id', $user);
    }

    /**
     * Relationship with school
     * Example: $polarity->school->name
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firstUser()
    {
        return $this->belongsTo(
            User::class,
            'first_user_id'
        )->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function secondUser()
    {
        return $this->belongsTo(
            User::class,
            'second_user_id'
        )->withoutGlobalScopes();
    }

    /**
     * Adding function for registering
     * student attempts for creating a pass when someone else
     * is out in a hall.
     *
     * @param $polarityCanceledIds
     * @param array $destination
     * @param null $fromApplication
     * @param bool $kiosk
     * @return bool
     */
    public static function registerAttempt(
        $polarityCanceledIds,
        array $destination,
        $fromApplication = null,
        bool $kiosk = false
    ): bool {
        $user = $kiosk ? User::find(auth()->user()->user_id) : auth()->user();

        $polarities = self::active()
            ->fromSchoolId($user->school_id)
            ->where(function ($query) use ($user, $polarityCanceledIds) {
                $query
                    ->where('first_user_id', $user->id)
                    ->whereIntegerInRaw('second_user_id', $polarityCanceledIds);
            })
            ->orWhere(function ($query) use ($user, $polarityCanceledIds) {
                $query
                    ->whereIntegerInRaw('first_user_id', $polarityCanceledIds)
                    ->where('second_user_id', $user->id);
            })
            ->get();

        InsertPolarityReportRecordsJob::dispatch(
            $polarities,
            $destination,
            $user,
            $fromApplication
        );

        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function polarityReports()
    {
        return $this->hasMany(PolarityReport::class);
    }

    /**
     * @return int
     */
    public function getTimesTriggeredAttribute()
    {
        return $this->polarityReports()
            ->useWritePdo()
            ->count();
    }
}
