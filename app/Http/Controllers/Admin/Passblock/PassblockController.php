<?php

namespace App\Http\Controllers\Admin\Passblock;

use App\Events\PassBlockCreated;
use App\Events\PassBlockRemoved;
use App\Http\Controllers\Controller;
use App\Http\Requests\PassBlockRequest;
use App\Http\Requests\PassBlockUpdateRequest;
use App\Http\Resources\PassblockCollection;
use App\Http\Resources\PassblockResource;
use App\Models\PassBlock;
use App\Notifications\Push\SchoolNotification;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Notification;

/**
 * @group PassBlock management.
 * API's for managing passblocking
 */
class PassblockController extends Controller
{
    /**
     * Get all pass blocks from the current school.
     *
     * @return PassblockCollection
     * @authenticated
     * @apiResourceCollection App\Http\Resources\PassblockCollection
     * @apiResourceModel App\Models\PassBlock
     * @throws AuthorizationException
     */
    public function index(): PassblockCollection
    {
        $user = auth()->user();

        $this->authorize('viewAny', PassBlock::class);

        if (request()->has('sort') || request()->has('search_query')) {
            $searchQuery =
                request()->get('search_query') === null
                    ? config('scout_elastic.all_records')
                    : convertElasticQuery(request()->get('search_query'));
            $passBlocks = PassBlock::searchableQuery(
                $searchQuery,
                PassBlock::recordsPerPage()
            );
        } else {
            $passBlocks = PassBlock::fromSchoolId($user->school_id)
                ->orderBy('from_date', 'desc')
                ->paginate(PassBlock::recordsPerPage());
        }

        return new PassblockCollection($passBlocks);
    }

    /**
     * Create a full pass block
     *
     * @bodyParam from_date date required Pass block starts from date.
     * @bodyParam to_date date required Pass block end date.
     * @bodyParam reason string required Reason for the block pass
     * @bodyParam kiosk boolean Block for kiosk passes
     * @bodyParam students boolean Block for student passes
     * @bodyParam proxy boolean Block for proxy passes
     * @bodyParam appointments boolean Block for appointment passes.
     *
     * @param PassBlockRequest $request
     * @return PassblockResource|JsonResponse
     * @response status=422 scenario="Pass block for that time already exist" {
     *    "data": [],
     *    "message": passes.block.already.exists,
     *    "status": "fail"
     * }
     * @apiResource App\Http\Resources\PassblockResource
     * @apiResourceModel App\Models\PassBlock
     * @authenticated
     */
    public function store(PassBlockRequest $request)
    {
        $passBlockExists = PassBlock::where(
            'from_date',
            Carbon::parse($request->input('from_date'))->setTimezone(
                config('app.timezone')
            )
        )
            ->where(
                'to_date',
                Carbon::parse($request->input('to_date'))->setTimezone(
                    config('app.timezone')
                )
            )
            ->fromSchoolId(auth()->user()->school_id)
            ->exists();

        if ($passBlockExists) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('passes.block.already.exists'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $passBlock = PassBlock::create([
            'from_date' => $request->input('from_date'),
            'to_date' => $request->input('to_date'),
            'school_id' => auth()->user()->school_id,
            'reason' => $request->input('reason'),
            'message' => $request->input('message'),
            'kiosk' => $request->input('kiosk'),
            'students' => $request->input('students'),
            'proxy' => $request->input('proxy'),
            'appointments' => $request->input('appointments')
        ]);

        event(new PassBlockCreated($passBlock));
        //        Notification::send(
        //            $passBlock,
        //            new SchoolNotification('PassBlockCreated')
        //        );
        return new PassblockResource($passBlock->refresh());
    }

    /**
     * Update the existing pass block period.
     *
     * @urlParam passBlock integer required The id of the pass block
     *
     * @bodyParam from_date date required Pass block starts from date.
     * @bodyParam to_date date required Pass block end date.
     * @bodyParam reason string required Reason for the block pass
     * @bodyParam kiosk boolean Block for kiosk passes
     * @bodyParam students boolean Block for student passes
     * @bodyParam proxy boolean Block for proxy passes
     * @bodyParam appointments boolean Block for appointment passes.
     * @param PassBlockUpdateRequest $request
     * @param PassBlock $passBlock
     * @return PassblockResource
     * @throws AuthorizationException
     * @apiResource App\Http\Resources\PassblockResource
     * @apiResourceModel App\Models\PassBlock
     * @authenticated
     */
    public function update(
        PassBlockUpdateRequest $request,
        PassBlock $passBlock
    ): PassblockResource {
        $this->authorize('update', $passBlock);

        $passBlockUpdate = $passBlock->update([
            'from_date' => $request->input('from_date'),
            'to_date' => $request->input('to_date'),
            'school_id' => auth()->user()->school_id,
            'reason' => $request->input('reason'),
            'message' => $request->input('message'),
            'kiosk' => $request->input('kiosk'),
            'students' => $request->input('students'),
            'proxy' => $request->input('proxy'),
            'appointments' => $request->input('appointments')
        ]);

        return new PassblockResource($passBlock);
    }

    /**
     * Remove global pass block restriction.
     *
     * @urlParam passBlock integer required The id of the pass block record.
     *
     * @param PassBlock $passBlock
     * @return PassblockResource
     * @throws AuthorizationException
     * @apiResource App\Http\Resources\PassblockResource
     * @apiResourceModel App\Models\PassBlock
     *
     * @authenticated
     */
    public function delete(PassBlock $passBlock)
    {
        $this->authorize('delete', $passBlock);
        $user = auth()->user();
        $passBlockId = $passBlock->id;

        event(new PassBlockRemoved($passBlockId, $user->school_id));
        //        Notification::send(
        //            $passBlock,
        //            new SchoolNotification('PassBlockRemoved')
        //        );

        $passBlock->delete();

        return new PassblockResource($passBlock);
    }
}
