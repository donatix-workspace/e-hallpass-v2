<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class InCaseInsesitive implements Rule
{
    protected $array;
    protected $attribute;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($array)
    {
        $this->array = array_map(function ($element) {
            return strtolower($element);
        }, $array);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        return in_array(strtolower($value), $this->array);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->attribute . ' is not in ' . implode(',',$this->array);
    }
}
