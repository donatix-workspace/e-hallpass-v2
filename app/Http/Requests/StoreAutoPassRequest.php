<?php

namespace App\Http\Requests;

use App\Models\Room;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreAutoPassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'autopass_rooms' => 'array',
            'autopass_rooms.*' => ['required', 'integer', 'distinct', Rule::exists('rooms', 'id')->where(function ($query) {
                $query->where('school_id', auth()->user()->school_id)
                    ->where('status', Room::ACTIVE);
            })]
        ];
    }
}
