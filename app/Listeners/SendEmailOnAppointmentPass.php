<?php

namespace App\Listeners;

use App\Events\AppointmentPassSendEmail;
use App\Mail\AppointmentPassRemindMail;
use App\Models\Module;
use Illuminate\Support\Facades\Mail;
use Log;

class SendEmailOnAppointmentPass
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param AppointmentPassSendEmail $event
     * @return void
     */
    public function handle(AppointmentPassSendEmail $event)
    {
        $moduleOption = json_decode(
            $event->appointmentPass->school->getModuleOptions(
                Module::APPOINTMENTPASS
            )
        );

        Log::channel('appointments')->info(
            'Apt Email Token adult: ' . $event->tokenAdmin
        );

        Log::channel('appointments')->info(
            'Module email for appointment pass status: Id of apt: ' .
                $event->appointmentPass->id .
                ', Module email status:' .
                $moduleOption->email
        );

        if (optional($moduleOption)->email) {
            if ($event->appointmentPass->user->email !== null) {
                Mail::to(
                    $event->appointmentPass->user,
                    $event->appointmentPass->school_id
                )->send(
                    new AppointmentPassRemindMail(
                        $event->appointmentPass,
                        false,
                        $event->token
                    )
                );
            }
        }
    }
}
