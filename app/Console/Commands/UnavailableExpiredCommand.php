<?php

namespace App\Console\Commands;

use App\Events\FavoriteUnavailableEvent;
use App\Events\UnavailableExpired;
use App\Events\UnavailableUpdate;
use App\Models\Unavailable;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UnavailableExpiredCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unavailables:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change the status of expired unavailables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $unavailables = Unavailable::active()->cursor();

        foreach ($unavailables as $unavailable) {
            if (
                $unavailable->isDaily() ||
                $unavailable->isWeekly() ||
                $unavailable->isMonthly()
            ) {
                $unavailable->updateByRecurrence();
            }

            if ($unavailable->to_date->isPast()) {
                event(
                    new FavoriteUnavailableEvent($unavailable->school_id, [
                        'type' => $unavailable->unavailable_type,
                        'id' => $unavailable->unavailable_id,
                        'unavailability' => null
                    ])
                );

                if ($unavailable->recurrence_type !== null) {
                    $unavailable->update([
                        'canceled_for_today_at' => now()
                    ]);

                    event(new UnavailableExpired($unavailable));
                    event(
                        new UnavailableUpdate(
                            $unavailable,
                            Unavailable::INACTIVE
                        )
                    );
                } else {
                    if ($unavailable->update(['status' => 0])) {
                        event(new UnavailableExpired($unavailable));
                        event(
                            new UnavailableUpdate(
                                $unavailable,
                                Unavailable::INACTIVE
                            )
                        );
                    }
                }
            }
        }
        return 0;
    }
}
