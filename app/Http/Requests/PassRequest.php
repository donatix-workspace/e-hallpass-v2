<?php

namespace App\Http\Requests;

use App\Models\Room;
use App\Models\User;
use App\Rules\NotSameLocationRule;
use App\Rules\PassRoomOrUserExistRule;
use App\Rules\RoomOrUserExistsRule;
use App\Rules\RoomOrUserStringRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;

class PassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() &&
            auth()
                ->user()
                ->isStudent();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $fromType = Request::input('from_type');
        $toType = Request::input('to_type');

        return [
            'from_id' => ['integer', new PassRoomOrUserExistRule($fromType)],
            'to_id' => [
                'integer',
                new PassRoomOrUserExistRule($toType),
                new NotSameLocationRule(
                    $this->input('from_type'),
                    $this->input('from_id'),
                    $this->input('to_type')
                )
            ],
            'from_type' => ['required', 'string', new RoomOrUserStringRule()],
            'to_type' => ['required', 'string', new RoomOrUserStringRule()],
            'comment' => [
                'max:255',
                Rule::requiredIf(function () use ($toType) {
                    return $toType === User::class ||
                        !Room::where('id', request()->input('to_id'))
                            ->where(function ($query) {
                                return $query
                                    ->where(
                                        'comment_type',
                                        Room::COMMENT_HIDDEN
                                    )
                                    ->orWhere(
                                        'comment_type',
                                        Room::COMMENT_OPTIONAL
                                    );
                            })
                            ->exists();
                })
            ]
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'comment.required' => 'Reason for the pass is required.'
        ];
    }
}
