<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MigrateAutoPassLimitCommand extends Command
{
    protected $signature = 'migrate_sys:auto_pass_limit';

    protected $description = 'Migrate auto pass limits from Ehpv1 To Ehpv2';

    public function handle()
    {
//        $this->info('--- Migrating AutoPassLimits---');
//
//        $newDb = \DB::connection('mysql');
//        $oldDb = \DB::connection('oldDB');
//
//        $oldDb->disableQueryLog();
//        $newDb->disableQueryLog();
//
//        $autoPassLimits = $oldDb
//            ->table('auto_pass_limits')
//            ->get()
//            ->toArray();
//
//        $this->output->progressStart(count($autoPassLimits));
//
//        $autoPassLimitsChunks = array_chunk($autoPassLimits, 1500);
//
//        foreach ($autoPassLimitsChunks as $autoPassLimitsChunk) {
//            $autoPassLimitInsertArray = [];
//            foreach ($autoPassLimitsChunk as $autoPassLimit) {
//                $oldSchool = $oldDb
//                    ->table('school_schedule')
//                    ->where('school_id', $autoPassLimit->school_id)
//                    ->first();
//
//                if ($oldSchool) {
//                    $newSchool = $newDb
//                        ->table('schools')
//                        ->select('id')
//                        ->where('old_id', $oldSchool->school_id)
//                        ->first();
//
//                    $oldUser = $oldDb
//                        ->table('users')
//                        ->where('user_id', optional($autoPassLimit)->teacher_id)
//                        ->select('email')
//                        ->where(
//                            'school_id',
//                            optional($autoPassLimit)->school_id
//                        )
//                        ->first();
//
//                    $newUser = $newDb
//                        ->table('users')
//                        ->where('email', optional($oldUser)->email)
//                        ->select('id')
//                        ->where('school_id', $newSchool->id)
//                        ->first();
//
//                    if ($newUser) {
//                        $autoPassLimitInsertArray[] = [
//                            'user_id' => $newUser->id,
//                            'school_id' => $newSchool->id,
//                            'limit' => $autoPassLimit->limit,
//                            'created_at' => now(),
//                            'updated_at' => now()
//                        ];
//                    }
//                }
//            }
//            try {
//                $newDb
//                    ->table('auto_passes_limits')
//                    ->insert($autoPassLimitInsertArray);
//                $this->output->progressAdvance(
//                    count($autoPassLimitInsertArray)
//                );
//            } catch (\Exception $e) {
//                $this->info($e->getMessage());
//            }
//        }
//        $this->output->progressFinish();
    }
}
