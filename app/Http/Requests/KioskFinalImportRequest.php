<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KioskFinalImportRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'user_passwords_and_emails' => 'required|array',
            'user_passwords_and_emails.*.email' => 'required|email|exists:users,email',
            'user_passwords_and_emails.*.kiosk_password' => 'required|min:0|max:6'
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}
