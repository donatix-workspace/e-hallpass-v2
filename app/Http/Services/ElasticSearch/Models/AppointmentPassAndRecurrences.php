<?php

namespace App\Http\Services\ElasticSearch\Models;

use App\Http\Services\ElasticSearch\EModel;
use App\Models\Room;
use App\Models\StaffSchedule;
use App\Models\Transparency;
use App\Models\User;

class AppointmentPassAndRecurrences extends EModel
{
    public $orderBy = 'for_date:asc';
    protected $indexes = 'appointment_passes_index,recurrence_appointment_passes';

    /**
     * @param array $queryParams
     * @param string $search
     * @param bool $applyTransparency
     */
    protected function setQueryParams(
        array $queryParams,
        string $search = '*',
        bool $applyTransparency = false
    ) {
        if ($search !== config('scout_elastic.all_records')) {
            $type = [
                'query_string' => [
                    'query' => $search
                ]
            ];
        } else {
            $type = [
                'match_all' => (object) null
            ];
        }

        $this->query = [
            'query' => [
                'bool' => [
                    'must' => $type,
                    'filter' => [
                        'bool' => [
                            'must' => []
                        ]
                    ]
                ]
            ]
        ];

        foreach ($queryParams as $key => $value) {
            array_push(
                $this->query['query']['bool']['filter']['bool']['must'],
                [
                    'term' => [
                        $key => $value
                    ]
                ]
            );
        }

        if (Transparency::isEnabled()) {
            $staffSchedules = StaffSchedule::where(
                'user_id',
                auth()->user()->id
            )
                ->get()
                ->pluck('room_id');

            array_push(
                $this->query['query']['bool']['filter']['bool']['must'],
                [
                    'bool' => [
                        'should' => [
                            [
                                'bool' => [
                                    'must' => [
                                        [
                                            'match' => [
                                                'from_type' => User::class
                                            ]
                                        ],
                                        [
                                            'match' => [
                                                'from_id' => auth()->user()->id
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'bool' => [
                                    'must' => [
                                        [
                                            'match' => [
                                                'to_type' => User::class
                                            ]
                                        ],
                                        [
                                            'match' => [
                                                'to_id' => auth()->user()->id
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'bool' => [
                                    'must' => [
                                        [
                                            'match' => [
                                                'acknowledged_by' => auth()->user()
                                                    ->id
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'bool' => [
                                    'must' => [
                                        [
                                            'match' => [
                                                'confirmed_by' => auth()->user()
                                                    ->id
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'bool' => [
                                    'must' => [
                                        [
                                            'match' => [
                                                'created_by' => auth()->user()
                                                    ->id
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'bool' => [
                                    'must' => [
                                        [
                                            'terms' => [
                                                'to_id' => $staffSchedules
                                            ]
                                        ],
                                        [
                                            'match' => [
                                                'to_type' => Room::class
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'bool' => [
                                    'must' => [
                                        [
                                            'terms' => [
                                                'from_id' => $staffSchedules
                                            ]
                                        ],
                                        [
                                            'match' => [
                                                'from_type' => Room::class
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            );
        }
    }
}
