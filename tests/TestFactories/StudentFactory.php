<?php


namespace Tests\TestFactories;


use App\Models\User;
use App\Modules\Basepass\Models\EpassDetail;

class StudentFactory implements TestFactoriesInterface
{
    public $student;

    public function __construct($student)
    {
        $this->student = $student;
    }

    public static function createOne(array $attributes = [])
    {
        $defaultAttributes = [
            'role_id' => config('role.STUDENT'),
            'password' => 'password'
        ];

        $attr = array_merge($defaultAttributes, $attributes);

        $student = factory(User::class)->create($attr);

        return new StudentFactory($student);
    }

    public function withEpassDetail(array $attributes = [])
    {
        $defaultAttributes = [
            'student_id' => $this->student->user_id
        ];
        $attr = array_merge($defaultAttributes, $attributes);

        factory(EpassDetail::class)->create($attr);

        return $this;
    }
}
