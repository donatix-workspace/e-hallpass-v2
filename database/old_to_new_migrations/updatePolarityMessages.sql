SET FOREIGN_KEY_CHECKS=0;
START TRANSACTION;
UPDATE ehp_v2_prod.teacher_pass_settings as tchP, ehallpassDB.ab_polarity_settings as oldPS
SET tchP.polarity_message = oldPS.message_for_student
WHERE tchP.school_id = (SELECT id FROM schools WHERE old_id = oldPS.school_id limit 1);
COMMIT;
SET FOREIGN_KEY_CHECKS=1;
