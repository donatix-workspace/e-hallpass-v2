<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use HasFactory;

    public const ACTIVE = 1;

    // Module names
    public const AUTO_CHECKIN = 'Auto Check-In';
    public const APPOINTMENTPASS = 'Appointmentpass';
    public const KIOSK = 'Kiosk';

    // Module Ids
    public const APPOINTMENTPASS_ID = 1;
    public const AUTO_CHECKIN_ID = 2;
    public const KIOSK_ID = 3;

    public const PERMISSION_ENABLED = true;
    public const PERMISSION_DISABLED = true;

    protected $guarded = ['id'];

    protected static $defaultOptions = [
        1 => [
            'admin_edit' => 0,
            'teacher' => 0,
            'location' => 0
        ],
        2 => [],
        3 => [
            'barcode' => 'barcode',
            'spassword' => 'spassword'
        ],
    ];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function schools()
    {
        return $this->belongsToMany(School::class, 'school_modules');
    }

    public static function getDefaultOptionsById($moduleId)
    {
        return json_encode(self::$defaultOptions[$moduleId]) ?? null;
    }
}
