<?php

namespace Database\Seeders;

use App\Models\School;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schools = School::all();
        $schools->each(function ($item) {
            $user = User::factory()->create(['school_id' => $item->id]);
            \DB::table('schools_users')->insert([
                'user_id' => $user->id,
                'school_id' => $item->id,
                'role_id' => $user->role_id
            ]);
        });
    }
}
