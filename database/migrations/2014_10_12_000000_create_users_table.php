<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('student_sis_id', 100)->default('0');
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('username', 55)->nullable();
            $table->string('password')->default('0');
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->text('metatag')->nullable();
            $table->bigInteger('role_id')->unsigned()->index('role_id');
            $table->string('avatar', 100)->nullable();
            $table->integer('grade_year')->nullable();
            $table->boolean('status')->default(1)->index('status');
            $table->bigInteger('school_id')->unsigned()->default(1)->index('school_id');
            $table->boolean('is_searchable')->default(1)->index('is_searchable');
            $table->string('user_initials', 55)->nullable();
            $table->boolean('is_loggin')->default(0);
            $table->string('clever_id', 100)->nullable()->index('clever_id');
            $table->string('clever_building_id', 100)->nullable()->comment('Clever School ID');
            $table->boolean('updated_role')->nullable()->default(0);
            $table->timestamp('last_logged_in_at')->nullable();
            $table->boolean('auth_type')->nullable();
            $table->index(['first_name', 'last_name'], 'search');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
