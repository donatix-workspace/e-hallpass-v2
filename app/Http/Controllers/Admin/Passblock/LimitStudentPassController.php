<?php

namespace App\Http\Controllers\Admin\Passblock;

use App\Events\StudentPassLimitCreatedEvent;
use App\Events\StudentPassLimitRemovedEvent;
use App\Http\Controllers\Controller;
use App\Http\Filters\PassLimitFilter;
use App\Http\Requests\PassLimitDeleteBulkRequest;
use App\Http\Requests\PassLimitRequest;
use App\Http\Requests\PassLimitUpdateRequest;
use App\Http\Resources\PassCollection;
use App\Http\Resources\PassLimitCollection;
use App\Http\Resources\PassLimitResource;
use App\Models\PassLimit;
use App\Models\School;
use App\Models\User;
use App\Notifications\Push\SchoolNotification;
use App\Notifications\Push\StudentNotification;
use App\Policies\PassPolicy;
use Artisan;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Maatwebsite\Excel\Facades\Excel;
use Notification;

/**
 * @group PassLimit manage.
 */
class LimitStudentPassController extends Controller
{
    /**
     * View pass limits
     *
     * @apiResourceCollection App\Http\Resources\PassLimitCollection
     * @apiResourceModel App\Models\PassLimit
     *
     * @authenticated
     * @return PassLimitCollection
     * @throws AuthorizationException
     */
    public function index(PassLimitFilter $filters)
    {
        $user = auth()->user();
        $this->authorize('viewAny', PassLimit::class);

        if (
            (request()->has('search_query') &&
                request()->get('search_query') !== null) ||
            request()->has('sort')
        ) {
            $passLimits = collect(
                PassLimit::search(
                    request()->get('search_query') === null
                        ? config('scout_elastic.all_records')
                        : convertElasticQuery(request()->get('search_query'))
                )
                    ->select('id')
                    ->when(request()->has('sort'), function ($query) {
                        $sort = explode(':', request()->get('sort'));
                        if (count($sort) <= 1) {
                            return $query->orderBy('id', 'asc');
                        }

                        $sortingKeywords = ['asc', 'desc'];
                        if (in_array($sort[1], $sortingKeywords)) {
                            return $query->orderBy($sort[0], $sort[1]);
                        }
                        return $query->orderBy('id', 'asc');
                    })
                    ->where('school_id', $user->school_id)
                    ->where('archived', request()->has('active') ? 0 : 1)
                    ->take(
                        PassLimit::where('school_id', $user->school_id)->count()
                    )
                    ->get()
            )
                ->pluck('id')
                ->toArray();
            $passLimitIdsImploded = implode(',', $passLimits);

            $passLimits = PassLimit::fromSchoolId($user->school_id)
                ->whereIntegerInRaw('id', $passLimits)
                ->orderByRaw("field(id,$passLimitIdsImploded)")
                ->filter($filters)
                ->paginate(PassLimit::recordsPerPage());
        } else {
            $passLimits = PassLimit::fromSchoolId($user->school_id)
                ->orderBy('created_at', 'desc')
                ->filter($filters)
                ->paginate(PassLimit::recordsPerPage());
        }

        return new PassLimitCollection($passLimits);
    }

    /**
     * Create a student pass limit.
     *
     * @response status=201 scenario="Successful creation." {
     *     "data": [],
     *     "status": "success"
     * }
     *
     * @bodyParam limit_for string required Type of the limitation Example: students, csv, gradeyear, school
     * @bodyParam csv_file file The csv file with emails (required only when limit_for input is csv).
     * @bodyParam grade_years json The json values of all grade years that will be limited (required if limit_for input is gradeyear)
     * @bodyParam selected_student_ids json The json values of all students ids that will be limited (required if limit_for input is students)
     * @bodyParam from_date date required From what date the limitation will be valid.
     * @bodyParam to_date date required To what date the limitation will longer be valid.
     * @bodyParam limit integer required The pass amount limit.
     * @bodyParam reason string required The reason for limitation.
     * @bodyParam recurrence_type string The type of the recurrence: (daily, weekly, monthly)
     *
     * @authenticated
     * @param PassLimitRequest $request
     * @return JsonResponse
     * @throws AuthorizationException|\Throwable
     */
    public function store(PassLimitRequest $request): JsonResponse
    {
        $user = auth()->user();

        $now = now();

        $this->authorize('create', PassLimit::class);

        $limitFor = $request->input('limit_for');
        $passLimitData = [];

        if ($limitFor === PassLimit::SCHOOL) {
            $checkIfLimitExistForThePeriod = PassLimit::checkIfExistsForThatPeriod(
                $request
            );

            if ($checkIfLimitExistForThePeriod) {
                return $checkIfLimitExistForThePeriod;
            }

            $passLimit = PassLimit::create([
                'school_id' => $user->school_id,
                'limitable_type' => PassLimit::SCHOOL_TYPE,
                'from_date' => $request->input('from_date'),
                'to_date' => $request->input('to_date'),
                'limitable_id' => $user->school_id,
                'reason' => $request->input('reason'),
                'recurrence_type' => $request->input('recurrence_type'),
                'limit' => $request->input('limit', null)
            ]);
        }

        if ($limitFor === PassLimit::GRADEYEAR) {
            $gradeYears = collect(
                json_decode($request->input('grade_years'), true)
            );

            $checkIfLimitExistForThePeriod = PassLimit::checkIfExistsForThatPeriod(
                $request,
                $userId = 0,
                $gradeYears->toJson()
            );

            if ($checkIfLimitExistForThePeriod) {
                return $checkIfLimitExistForThePeriod;
            }

            $passLimit = PassLimit::create([
                'school_id' => $user->school_id,
                'limitable_type' => PassLimit::SCHOOL_TYPE,
                'limitable_id' => $user->school_id,
                'from_date' => $request->input('from_date'),
                'to_date' => $request->input('to_date'),
                'grade_year' => $gradeYears->toJson(),
                'reason' => $request->input('reason'),
                'recurrence_type' => $request->input('recurrence_type'),
                'limit' => $request->input('limit')
            ]);
        }

        if ($limitFor === PassLimit::CSV) {
            // When frontend convert the content data to formData , null value is being set as string "null"
            // So we do this to make recurrence_type null as type.
            if ($request->input('recurrence_type') === 'null') {
                $request->merge(['recurrence_type' => null]);
            }

            $storageTempPath = storage_path('/tmp/');
            $csvFile = $request->file('csv_file');
            $csvFileName = time() . $csvFile->getClientOriginalName();
            $storageCsvFile = $csvFile->move($storageTempPath, $csvFileName);
            $csvEmails = Excel::toCollection((object) [], $storageCsvFile)
                ->flatten()
                ->skip(1);
            $usersFromEmail = User::whereIn('email', $csvEmails)
                ->where('role_id', config('roles.student'))
                ->fromSchoolId($user->school_id)
                ->get('id');

            //There's empty users.
            if (!count($usersFromEmail)) {
                return response()->json(
                    [
                        'data' => [],
                        'message' => __(
                            'There\'s invalid user in your CSV file.'
                        ),
                        'status' => __('fail')
                    ],
                    422
                );
            }

            foreach ($usersFromEmail as $userCsv) {
                $checkIfLimitExistForThePeriod = PassLimit::checkIfExistsForThatPeriod(
                    $request,
                    $userCsv->id
                );

                if ($checkIfLimitExistForThePeriod) {
                    return $checkIfLimitExistForThePeriod;
                }

                $passLimitData[] = [
                    'school_id' => $user->school_id,
                    'limitable_type' => PassLimit::STUDENT_TYPE,
                    'limitable_id' => $userCsv->id,
                    'reason' => $request->input('reason'),
                    'recurrence_type' => $request->input('recurrence_type'),
                    'from_date' => Carbon::parse(
                        $request->input('from_date')
                    )->setTimezone(config('app.timezone')),
                    'to_date' => Carbon::parse(
                        $request->input('to_date')
                    )->setTimezone(config('app.timezone')),
                    'limit' => $request->input('limit'),
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }

        if ($limitFor === PassLimit::STUDENT) {
            $selectedStudentIds = json_decode(
                $request->input('selected_student_ids')
            );

            $usersIds = User::whereIntegerInRaw('id', $selectedStudentIds)
                ->where('role_id', config('roles.student'))
                ->fromSchoolId($user->school_id)
                ->get('id');

            foreach ($usersIds as $userStudent) {
                $checkIfLimitExistForThePeriod = PassLimit::checkIfExistsForThatPeriod(
                    $request,
                    $userStudent->id
                );

                if ($checkIfLimitExistForThePeriod) {
                    return $checkIfLimitExistForThePeriod;
                }

                $passLimitData[] = [
                    'school_id' => $user->school_id,
                    'limitable_type' => PassLimit::STUDENT_TYPE,
                    'limitable_id' => $userStudent->id,
                    'reason' => $request->input('reason'),
                    'recurrence_type' => $request->input('recurrence_type'),
                    'from_date' => Carbon::parse(
                        $request->input('from_date')
                    )->setTimezone(config('app.timezone')),
                    'to_date' => Carbon::parse(
                        $request->input('to_date')
                    )->setTimezone(config('app.timezone')),
                    'limit' => $request->input('limit'),
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }

        DB::beginTransaction();
        $passLimit = PassLimit::insert($passLimitData);
        PassLimit::where('school_id', $user->school_id)
            ->where('created_at', $now->toDateTimeString())
            ->searchable();
        DB::commit();

        event(
            new StudentPassLimitCreatedEvent($user->school_id, School::class)
        );

        //        Notification::send(
        //            $user->school,
        //            new SchoolNotification('StudentPassLimitCreatedEvent')
        //        );

        return response()->json(
            [
                'data' => [],
                'status' => __('success')
            ],
            201
        );
    }

    /**
     * Delete a pass limit
     *
     * @urlParam passLimit integer required The id of the pass limit
     *
     * @apiResource App\Http\Resources\PassLimitResource
     * @apiResourceModel App\Models\PassLimit
     *
     * @param PassLimit $passLimit
     * @return PassLimitResource
     * @throws AuthorizationException
     * @throws Exception
     * @authenticated
     */
    public function delete(PassLimit $passLimit)
    {
        $user = auth()->user();

        $this->authorize('delete', $passLimit);

        if (
            $passLimit->from_date->isPast() &&
            $passLimit->to_date->isFuture()
        ) {
            $passLimit->update([
                'archived' => PassLimit::ARCHIVED,
                'to_date' => now()->toDateTimeString()
            ]);
        } else {
            $passLimit->delete();
        }

        event(
            new StudentPassLimitRemovedEvent(
                $passLimit->limitable_id,
                $passLimit->limitable_type
            )
        );

        //        Notification::send(
        //            $passLimit,
        //            new StudentNotification('StudentPassLimitRemovedEvent', [
        //                'pass_limit' => [
        //                    'current' => 0,
        //                    'from_date' => now()->format('Y-m-d'),
        //                    'has' => false,
        //                    'max' => -1,
        //                    'student_reached_limit' => false,
        //                    'to_date' => now()->format('Y-m-d')
        //                ]
        //            ])
        //        );

        return new PassLimitResource($passLimit);
    }

    /**
     * Delete a pass limit (Bulk)
     *
     * @bodyParam pass_limit_ids array required The ids with pass limits ids;
     *
     * @apiResource App\Http\Resources\PassLimitResource
     * @apiResourceModel App\Models\PassLimit
     *
     * @param PassLimitDeleteBulkRequest $request
     * @return JsonResponse
     * @authenticated
     */
    public function deleteBulk(
        PassLimitDeleteBulkRequest $request
    ): JsonResponse {
        $user = auth()->user();
        $passLimits = PassLimit::whereIntegerInRaw(
            'id',
            $request->input('pass_limit_ids')
        )
            ->where('school_id', $user->school_id)
            ->get();

        foreach ($passLimits as $passLimit) {
            if (
                $passLimit->from_date->isPast() &&
                $passLimit->to_date->isFuture()
            ) {
                $passLimit->update([
                    'archived' => PassLimit::ARCHIVED,
                    'to_date' => now()->toDateTimeString()
                ]);
            } else {
                $passLimit->delete();
            }
        }

        return response()->json([
            'data' => [
                'deleted_ids' => $request->input('pass_limit_ids')
            ],
            'message' => __('success'),
            'status' => __('success')
        ]);
    }

    /**
     * Show a pass limit record
     *
     * @urlParam passLimit integer required The id of the pass limit
     *
     * @apiResourceModel App\Models\PassLimit
     * @apiResource App\Http\Resources\PassLimitResource
     *
     * @authenticated
     * @param PassLimit $passLimit
     * @return PassLimitResource
     * @throws AuthorizationException
     */
    public function show(PassLimit $passLimit)
    {
        $this->authorize('view', $passLimit);

        return new PassLimitResource($passLimit);
    }

    /**
     * Update a pass limit
     *
     * @apiResource App\Http\Resources\PassLimitResource
     * @apiResourceModel App\Models\PassLimit
     *
     * @urlParam passLimit integer required The id of the pass limit
     * @bodyParam to_date date required To what date the limitation will longer be valid.
     * @bodyParam limit integer required The pass amount limit.
     * @bodyParam reason string required The reason for limitation.
     * @bodyParam recurrence_type string The type of the recurrence: (daily, weekly, monthly)
     *
     * @authenticated
     * @param PassLimit $passLimit
     * @param PassLimitUpdateRequest $request
     * @return PassLimitResource|JsonResponse|void
     * @throws AuthorizationException
     */
    public function update(
        PassLimit $passLimit,
        PassLimitUpdateRequest $request
    ) {
        $this->authorize('update', $passLimit);

        //        $currentPassLimitFromDate = $passLimit->from_date->toDateTimeString();
        //        $currentPassLimitDate = $passLimit->to_date->toDateTimeString();
        //
        //        $givenDateFrom = Carbon::parse($request->input('from_date'))
        //            ->setTimezone(config('app.timezone'))
        //            ->toDateTimeString();
        //
        //
        //        $givenDate = Carbon::parse($request->input('to_date'))
        //            ->setTimezone(config('app.timezone'))
        //            ->toDateTimeString();

        //        if ($givenDate !== $currentPassLimitDate || $givenDateFrom !== $currentPassLimitFromDate) {
        //            $checkIfLimitExistForThePeriod = PassLimit::checkIfExistsForThatPeriod($request, $passLimit->limitable_type === User::class ? $passLimit->limitable_id : 0, $passLimit->grade_year !== null ? $passLimit->grade_year : 0);
        //
        //            if ($checkIfLimitExistForThePeriod) {
        //                return $checkIfLimitExistForThePeriod;
        //            }
        //
        //        }

        $passLimit->update([
            'from_date' => $passLimit->from_date->isFuture()
                ? $request->input('from_date')
                : $passLimit->from_date,
            'to_date' => $request->input('to_date'),
            'reason' => $request->input('reason'),
            'recurrence_type' => $request->input('recurrence_type'),
            'limit' => $request->input('limit')
        ]);

        return new PassLimitResource($passLimit);
    }

    /**
     * Get pass limit of spec user.
     *
     * @urlParam user integer required The id of the user.
     *
     * @authenticated
     * @param User $user
     * @return JsonResponse
     */
    public function studentLimitInformation(User $user): JsonResponse
    {
        $passLimitOfUser = PassLimit::whereType(User::class)
            ->without('limitable')
            ->select('limit')
            ->where('limitable_id', $user->id)
            ->whereDate('from_date', '<=', now())
            ->whereDate('to_date', '>=', now())
            ->select('limit')
            ->first();

        return response()->json([
            'data' => $passLimitOfUser,
            'message' => __('success'),
            'status' => __('success')
        ]);
    }
}
