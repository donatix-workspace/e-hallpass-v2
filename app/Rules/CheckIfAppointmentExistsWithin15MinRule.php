<?php

namespace App\Rules;

use App\Models\AppointmentPass;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class CheckIfAppointmentExistsWithin15MinRule implements Rule
{
    public function __construct()
    {
        //
    }

    public function passes($attribute, $value): bool
    {
        $dateSubMinutes15 = Carbon::parse($value)
            ->setTimezone(config('app.timezone'))
            ->subMinutes(15);
        $dateWithIn15Minutes = Carbon::parse($value)
            ->setTimezone(config('app.timezone'))
            ->addMinutes(15);

        return !AppointmentPass::whereBetween('for_date', [
            $dateSubMinutes15,
            $dateWithIn15Minutes
        ])
            ->where('user_id', auth()->user()->id)
            ->where('expired_at', null)
            ->where('canceled_at', null)
            ->exists();
    }

    public function message(): string
    {
        return __(
            'Warning: you have an appointment within 15 minutes of the time you chose.'
        );
    }
}
