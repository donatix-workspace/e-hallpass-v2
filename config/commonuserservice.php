<?php
return [
    'ehp_client_uuid'=>env('CUS_EHP_UUID'),
    'enabled'=>(boolean)env('CUS_ENABLED', false),
    'fallback_login_enabledv' => env('SSO_FALLBACK_ENABLED', false),
    'base_url' => env('CUS_BASE_URL'),
    'sso_base_url' => env('SSO_BASE_URL'),
    'basic_auth_user' => env('CUS_BASIC_AUTH_USER'),
    'basic_auth_password' => env('CUS_BASIC_AUTH_PASSWORD'),
    'client_id' => env('CUS_CLIENT_ID'),
    'client_secret' => env('CUS_CLIENT_SECRET'),
    'auth_providers' => [
        'sso' => 1,
        'google' => 10,
        'microsoft' => 20,
        'classLink' => 101,
        'clever' => 102,
        'gg4l' => 103,
        'superadmin' => 1001,
    ],
    'providers' => [
        'google' => env('GOOGLE_CLIENT_ID'),
        'clever' => env('CLEVER_CLIENT_ID'),
        'office' => env('OFFICE_CLIENT_ID'),
        'classlink' => env('CLASSLINK_CLIENT_ID'),
        'gg4l' => env('GG4L_CLIENT_ID'),
        'sso' => env('SSO_CLIENT_ID'),
        '10' => [
            'name' => 'google',
            'client_id' => env('GOOGLE_CLIENT_ID'),
            'client_secret' => env('GOOGLE_CLIENT_SECRET'),
            'token_url' => 'https://www.googleapis.com/oauth2/v4/token',
            'info_url' => 'https://www.googleapis.com/oauth2/v3/userinfo',
        ]
    ],
    'public_key' => '-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAyZXkBYsgJdy0ofJDKB5n
4Yo/3T73BreMGsCHw4bwNb0/rcU9SN7i4D15AoTnwqx3f2rLGoDZAFJ8ViphhLPr
02La80TnOhiexfEMlCVzYI7yZqBHfcTUsmw6xblxylQqb7dQq0jWIRH6SUtnLCb6
CofgKCQUi0RmetP+WS3pThDfXQI1u3Vxy4th+PWyooAW8FAHejCvwMJmBrkXt0lX
ZyTPS+2c037oQu/6j2Ge6WKAAh9x1toN6oDgF37Tw2TTDojgBWtrrnloXkaMq9Ob
Xo32iRjAdP1QbNfd/RviCUhLDxwlxzWyjT6p6DI6wBeL+hvY+Wxl2sPVhiQ6Qycw
KMjczAS9DXzdo+3qnjpZVSJSdijmOSDHjRBGy7766DRH3TBYMT4XXiUw/Laaefd+
K2Ujw9y55b3UQH5Ps6Wq8E5GkPs7JuOzcpLw9HJ3iC+OxSQuleqrhFdMnc13Vdwz
gPVlmxhAkYXWC8+dOd71vs7g25etfGrPTLrEZOvUG3DV2JsTF57Ey6DVUvLd+Khj
Nvnoa21EVdid9/hkAIWAcF3ML4NuZv8hCEe5g2sKZoJw0i8MDd+XhwkjqZ86jTHH
qCa1Nj9sUtbu64Ms7q2WlFpxHiHQ6joZZQPSzJWqHQZYrFtxgfbmcBOAJIevpR4e
9HxYD3WAFf+TzoIPsT0FhZcCAwEAAQ==
-----END PUBLIC KEY-----',
    'simulate_user' => [
        'last_name' => 'Simulate',
        'username' => 'Simulate user',
        'email_prefix' => 'simulate@'
    ]

    //'public_key' => file_get_contents(storage_path('commonUserService-public.key'))
];
