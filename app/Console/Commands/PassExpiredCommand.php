<?php

namespace App\Console\Commands;

use App\Events\PassExpired;
use App\Models\NurseRoom;
use App\Models\Pass;
use App\Models\Room;
use App\Models\TeacherPassSetting;
use App\Notifications\Push\StudentNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class PassExpiredCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'passes:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for expired passes and inactive them.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $passes = Pass::with('parent')
            ->where(function ($query) {
                $query
                    ->where('expired_at', null)
                    ->where('pass_status', Pass::ACTIVE)
                    ->orWhere('expired_at', '!=', null)
                    ->where('pass_status', Pass::ACTIVE);
            })
            ->where('canceled_at', null)
            ->cursor();

        // Logging
        if ($passes->count() > 0) {
            Log::channel('info')->info(
                'Starting passes:expired command on ' .
                    Carbon::now() .
                    ' with ' .
                    $passes->count() .
                    ' number of passes'
            );
        }
        /**
         * @var Pass $pass
         */
        foreach ($passes as $pass) {
            $pass->setAppends([]);
            $teacherPassSettings = TeacherPassSetting::where(
                'school_id',
                $pass->school_id
            )->first();
            if ($teacherPassSettings !== null) {
                $nurseRoom = NurseRoom::where('room_id', $pass->to_id)
                    ->where('school_id', $pass->school_id)
                    ->first();

                $nurseRoomFrom = NurseRoom::where('room_id', $pass->from_id)
                    ->where('school_id', $pass->school_id)
                    ->first();

                if (
                    $nurseRoomFrom &&
                    $pass->from_type === Room::class &&
                    $pass->isParent()
                ) {
                    $nurseExpireTime = TeacherPassSetting::convertInMinutes(
                        $nurseRoomFrom->end_time
                    );

                    if (
                        TeacherPassSetting::isForExpire(
                            $pass->created_at,
                            $nurseExpireTime
                        )
                    ) {
                        logger(
                            'The pass with ID: ' .
                                $pass->id .
                                ' was expired by extended time (Parent = True) 87 line'
                        );

                        if (!$pass->isApproved()) {
                            $pass->update([
                                'pass_status' => Pass::INACTIVE,
                                'flagged_at' => now(),
                                'extended_at' => now(),
                                'approved_at' => $pass->parent->approved_at->addMinutes(
                                    $nurseExpireTime
                                ),
                                'system_completed' => Pass::ACTIVE,
                                'expired_at' => Carbon::now()
                            ]);
                        } else {
                            $pass->update([
                                'pass_status' => Pass::INACTIVE,
                                'flagged_at' => now(),
                                'extended_at' => now(),
                                'system_completed' => Pass::ACTIVE,
                                'expired_at' => Carbon::now()
                            ]);
                        }

                        event(
                            new PassExpired(
                                Pass::with('child')
                                    ->without('parent')
                                    ->where(
                                        'id',
                                        $pass->parent_id !== null
                                            ? $pass->parent_id
                                            : $pass->id
                                    )
                                    ->firstOrFail()
                            )
                        );

                        //                        Notification::send(
                        //                            $pass,
                        //                            new StudentNotification('PassExpired')
                        //                        );
                    }
                }

                if (
                    $nurseRoom &&
                    $pass->to_type === Room::class &&
                    $pass->isApproved()
                ) {
                    $nurseExpireTime = TeacherPassSetting::convertInMinutes(
                        $nurseRoom->end_time
                    );

                    if (
                        TeacherPassSetting::isForExpire(
                            $pass->created_at,
                            $nurseExpireTime
                        )
                    ) {
                        logger(
                            'The pass with ID: ' .
                                $pass->id .
                                ' was expired by extended time (Parent = False) 122 line'
                        );

                        if ($pass->parent_id === null) {
                            $pass->update([
                                'pass_status' => Pass::INACTIVE,
                                'flagged_at' => now(),
                                'extended_at' => now(),
                                'completed_at' => $pass->approved_at->addMinutes(
                                    $nurseExpireTime
                                ),
                                'system_completed' => Pass::ACTIVE,
                                'expired_at' => Carbon::now()
                            ]);
                        } else {
                            $pass->update([
                                'pass_status' => Pass::INACTIVE,
                                'flagged_at' => now(),
                                'extended_at' => now(),
                                'completed_at' => $pass->parent->approved_at->addMinutes(
                                    $nurseExpireTime
                                ),
                                'system_completed' => Pass::ACTIVE,
                                'expired_at' => Carbon::now()
                            ]);
                        }

                        event(
                            new PassExpired(
                                Pass::with('child')
                                    ->without('parent')
                                    ->where(
                                        'id',
                                        $pass->parent_id !== null
                                            ? $pass->parent_id
                                            : $pass->id
                                    )
                                    ->firstOrFail()
                            )
                        );

                        //                        Notification::send(
                        //                            $pass,
                        //                            new StudentNotification('PassExpired')
                        //                        );
                    }
                }

                if (
                    (($pass->type === Pass::STUDENT_CREATED_PASS ||
                        $pass->type === Pass::PROXY_PASS ||
                        $pass->type === Pass::APPOINTMENT_PASS ||
                        $pass->type === Pass::KIOSK_PASS) &&
                        $pass->isApproved() &&
                        $pass->isNotAnExtended() &&
                        $pass->parent_id === null) ||
                    $pass->isForChildCompletedBySystem()
                ) {
                    $autoExpireTime = TeacherPassSetting::convertInMinutes(
                        $teacherPassSettings->auto_expire_time
                    );

                    if (
                        $pass->parent_id !== null &&
                        TeacherPassSetting::isForExpire(
                            $pass->parent->approved_at,
                            $autoExpireTime
                        ) &&
                        !$pass->isCompleted()
                    ) {
                        if (!$pass->isApproved()) {
                            $pass->update([
                                'pass_status' => Pass::INACTIVE,
                                'approved_at' => $pass->parent->approved_at->addMinutes(
                                    $autoExpireTime
                                ),
                                'system_completed' =>
                                    Pass::SYSTEM_COMPLETED_ACTIVE
                            ]);

                            logger(
                                'The pass with ID: ' .
                                    $pass->id .
                                    ' was expired by extended time (Parent = True) 144 line'
                            );
                        }

                        $pass->update([
                            'pass_status' => Pass::INACTIVE,
                            'completed_at' => $pass->parent->approved_at->addMinutes(
                                $autoExpireTime
                            ),
                            'system_completed' => Pass::SYSTEM_COMPLETED_ACTIVE
                        ]);

                        event(
                            new PassExpired(
                                Pass::with('child')
                                    ->without('parent')
                                    ->where(
                                        'id',
                                        $pass->parent_id !== null
                                            ? $pass->parent_id
                                            : $pass->id
                                    )
                                    ->firstOrFail()
                            )
                        );

                        //                        Notification::send(
                        //                            $pass,
                        //                            new StudentNotification('PassExpired')
                        //                        );
                    }

                    if (
                        $pass->parent_id === null &&
                        TeacherPassSetting::isForExpire(
                            $pass->approved_at,
                            $autoExpireTime
                        ) &&
                        !$pass->isCompleted()
                    ) {
                        logger(
                            'The pass with ID:' . $pass->id . ' is auto expired'
                        );

                        $pass->update([
                            'pass_status' => Pass::INACTIVE,
                            'completed_at' => $pass->approved_at->addMinutes(
                                $autoExpireTime
                            ),
                            'system_completed' => Pass::SYSTEM_COMPLETED_ACTIVE
                        ]);

                        event(
                            new PassExpired(
                                Pass::with('child')
                                    ->without('parent')
                                    ->where(
                                        'id',
                                        $pass->parent_id !== null
                                            ? $pass->parent_id
                                            : $pass->id
                                    )
                                    ->firstOrFail()
                            )
                        );

                        //                        Notification::send(
                        //                            $pass,
                        //                            new StudentNotification('PassExpired')
                        //                        );
                    }
                }

                if (
                    $pass->type === Pass::APPOINTMENT_PASS &&
                    !$pass->isApproved() &&
                    $pass->parent_id === null
                ) {
                    $awaitingPassTime = TeacherPassSetting::convertInMinutes(
                        $teacherPassSettings->awaiting_apt
                    );

                    Log::info('Awaiting pass time min: ' . $awaitingPassTime);

                    if (
                        TeacherPassSetting::isForExpire(
                            $pass->created_at,
                            $awaitingPassTime
                        )
                    ) {
                        $pass->update([
                            'pass_status' => Pass::INACTIVE,
                            'expired_at' => Carbon::now()
                        ]);

                        event(
                            new PassExpired(
                                Pass::with('child')
                                    ->without('parent')
                                    ->where(
                                        'id',
                                        $pass->parent_id !== null
                                            ? $pass->parent_id
                                            : $pass->id
                                    )
                                    ->firstOrFail()
                            )
                        );

                        //                        Notification::send(
                        //                            $pass,
                        //                            new StudentNotification('PassExpired')
                        //                        );
                    }
                }
                if (
                    ($pass->type === Pass::STUDENT_CREATED_PASS ||
                        $pass->type === Pass::KIOSK_PASS) &&
                    !$pass->isApproved() &&
                    $pass->parent_id === null
                ) {
                    $whitePassTime = TeacherPassSetting::convertInMinutes(
                        $teacherPassSettings->white_passes
                    );

                    if (
                        TeacherPassSetting::isForExpire(
                            $pass->created_at,
                            $whitePassTime
                        )
                    ) {
                        logger(
                            'The pass with ID' .
                                $pass->id .
                                'is for expire White pass time, 198 line.'
                        );

                        $pass->update([
                            'pass_status' => Pass::INACTIVE,
                            'expired_at' => Carbon::now()
                        ]);

                        event(
                            new PassExpired(
                                Pass::with('child')
                                    ->without('parent')
                                    ->where(
                                        'id',
                                        $pass->parent_id !== null
                                            ? $pass->parent_id
                                            : $pass->id
                                    )
                                    ->firstOrFail()
                            )
                        );

                        //                        Notification::send(
                        //                            $pass,
                        //                            new StudentNotification('PassExpired')
                        //                        );
                    }
                }

                if (
                    $pass->type === Pass::PROXY_PASS &&
                    !$pass->isApproved() &&
                    $pass->parent_id === null
                ) {
                    $whitePassTime = TeacherPassSetting::convertInMinutes(
                        $teacherPassSettings->white_passes
                    );

                    logger(
                        'The pass with ID' .
                            $pass->id .
                            'is for expire White pass time, 218 line.'
                    );

                    if (
                        TeacherPassSetting::isForExpire(
                            $pass->created_at,
                            $whitePassTime
                        )
                    ) {
                        logger(
                            'The pass with ID' .
                                $pass->id .
                                'is for expire White pass time, 221 line.'
                        );
                        $pass->update([
                            'pass_status' => Pass::INACTIVE,
                            'expired_at' => Carbon::now()
                        ]);

                        Log::info('Pass Object: ', [$pass]);

                        event(
                            new PassExpired(
                                Pass::with('child')
                                    ->without('parent')
                                    ->where(
                                        'id',
                                        $pass->parent_id !== null
                                            ? $pass->parent_id
                                            : $pass->id
                                    )
                                    ->firstOrFail()
                            )
                        );

                        //                        Notification::send(
                        //                            $pass,
                        //                            new StudentNotification('PassExpired')
                        //                        );
                    }
                }

                if ($pass->expired_at !== null && $pass->pass_status) {
                    logger(
                        'The pass with ID' .
                            $pass->id .
                            'is for expire White pass time, 239 line.'
                    );
                    $pass->update([
                        'pass_status' => Pass::INACTIVE
                    ]);

                    event(
                        new PassExpired(
                            Pass::with('child')
                                ->without('parent')
                                ->where(
                                    'id',
                                    $pass->parent_id !== null
                                        ? $pass->parent_id
                                        : $pass->id
                                )
                                ->firstOrFail()
                        )
                    );
                }

                $pass->aSync()->searchable();
                if ($pass->parent !== null) {
                    $pass->parent->aSync()->searchable();
                }
            }
        }
        return 0;
    }
}
