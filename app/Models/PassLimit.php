<?php

namespace App\Models;

use App\Elastic\PassLimitsIndexConfigurator;
use App\Http\Filters\Filterable;
use App\Http\Requests\PassLimitRequest;
use App\Traits\DefaultRecurrence;
use App\Traits\ExtendedSearchable;
use App\Traits\PassLimitsMapping;
use App\Traits\PerPage;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use DateTimeInterface;
use Illuminate\Http\Request;
use ScoutElastic\Searchable;

class PassLimit extends Model implements PushNotifications
{
    use HasFactory, ExtendedSearchable, PassLimitsMapping, PerPage, Filterable;
    use DefaultRecurrence;

    protected $indexConfigurator = PassLimitsIndexConfigurator::class;

    protected $with = ['limitable'];

    protected $appends = ['for'];

    protected $guarded = [];

    protected $dates = ['recurrence_end_at', 'from_date', 'to_date'];

    public const SCHOOL_TYPE = School::class;
    public const STUDENT_TYPE = User::class;
    public const CSV = 'csv';
    public const SCHOOL = 'school';
    public const GRADEYEAR = 'gradeyear';
    public const STUDENT = 'student';
    public const ARCHIVED = 1;

    /**
     * @param DateTimeInterface $date
     * @return string
     */
    public function serializeDate(DateTimeInterface $date): string
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($date, config('app.timezone'))
            ->setTimezone(optional($school)->getTimezone())
            ->format('Y-m-d H:i:s');
    }

    /**
     * @return array
     */
    public function toSearchableArray(): array
    {
        $school = optional(School::findCacheFirst($this->school_id));

        $passLimit = $this->toArray();

        $passLimit['from_date'] = School::convertTimeToCorrectTimezone(
            $this->from_date,
            $school->getTimezone(),
            true,
            false,
            'm/d/Y'
        );
        $passLimit['to_date'] = School::convertTimeToCorrectTimezone(
            $this->to_date,
            $school->getTimezone(),
            true,
            false,
            'm/d/Y'
        );
        if ($this->created_at !== null) {
            $passLimit['created_on'] = Carbon::parse($this->created_at)
                ->setTimezone($school->getTimezone())
                ->format('m/d/Y g:i:s A');
        } else {
            $passLimit['created_on'] = null;
        }

        return $passLimit;
    }

    /**
     * @param $query
     * @param string $type
     * @return mixed
     */
    public function scopeWhereType($query, $type = self::STUDENT_TYPE)
    {
        return $query->where('limitable_type', $type);
    }

    /**
     * @return mixed|string
     */
    public function getForAttribute()
    {
        if ($this->grade_year !== null) {
            $gradeYear = json_decode($this->grade_year, true);
            return implode(',', $gradeYear);
        }

        return optional($this->limitable)->name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @param $query
     * @param $user
     * @return mixed
     */
    public function scopeOfUserId($query, $user)
    {
        return $query->where('user_id', $user);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function limitable()
    {
        return $this->morphTo()->withoutGlobalScopes();
    }

    public function setFromDateAttribute($value)
    {
        $this->attributes['from_date'] = Carbon::parse($value)->setTimezone(
            config('app.timezone')
        );
    }

    public function setToDateAttribute($value)
    {
        $this->attributes['to_date'] = Carbon::parse($value)->setTimezone(
            config('app.timezone')
        );
    }

    /**
     * @param $query
     * @param $fromDate
     * @param $toDate
     * @return mixed
     */
    public function scopeWithValidDates($query, $fromDate, $toDate)
    {
        return $query->where(function ($query) use ($fromDate, $toDate) {
            $query
                ->whereDate('from_date', '<=', Carbon::parse($fromDate))
                ->whereDate('to_date', '>=', Carbon::parse($toDate));
        });
    }

    /**
     * @param  $request
     * @param int $userId
     * @param string $gradeYears
     * @return JsonResponse|void
     */
    public static function checkIfExistsForThatPeriod(
        $request,
        $userId = 0,
        $gradeYears = ''
    ) {
        $fromDate = Carbon::parse($request->input('from_date'));
        $toDate = Carbon::parse($request->input('to_date'));

        $user = auth()->user();
        $samePassLimitExist = PassLimit::fromSchoolId($user->school_id)
            ->withValidDates($fromDate, $toDate)
            ->where('limitable_type', User::class)
            ->where('limitable_id', $userId)
            ->where('archived', 0)
            ->exists();

        if ($request->input('limit_for') === PassLimit::SCHOOL) {
            $samePassLimitExist = PassLimit::fromSchoolId($user->school_id)
                ->withValidDates($fromDate, $toDate)
                ->where('limitable_type', School::class)
                ->where('limitable_id', $user->school_id)
                ->where('grade_year', null)
                ->where('archived', 0)
                ->exists();
        }

        if ($request->input('limit_for') === PassLimit::GRADEYEAR) {
            $samePassLimitExists = PassLimit::fromSchoolId($user->school_id)
                ->withValidDates($fromDate, $toDate)
                ->where('limitable_type', School::class)
                ->where('limitable_id', $user->school_id)
                ->where('grade_year', $gradeYears)
                ->where('archived', 0)
                ->exists();
        }

        if ($samePassLimitExist) {
            return response()->json(
                [
                    'data' => [],
                    'status' => __('fail'),
                    'message' => __('pass.limit.for.same.date')
                ],
                422
            );
        }
    }
    public function getTopics()
    {
        $env = config('services.fcm.env');
        //dev.
        //local.
        //staging.
        //production.
        if ($this->limitable_type === School::class) {
            return "{$env}.pass-limits.{$this->school_id}";
        }

        return "{$env}.user.profile.{$this->user_id}";
    }

    public function getNotificationTitle($event)
    {
        return __($event . 'Title');
    }

    public function getNotificationBody($event)
    {
        return __($event . 'Body');
    }

    public function getData($event, $options)
    {
        return [
            'event' => $event,
            'payload' => [
                'id' => $this->id
            ],
            'options' => $options
        ];
    }

    public function getNotificationSettings($event, $options)
    {
        return [
            'topics' => $this->getTopics(),
            'title' => $this->getNotificationTitle($event),
            'body' => $this->getNotificationBody($event),
            'data' => $this->getData($event, $options)
        ];
    }
}
