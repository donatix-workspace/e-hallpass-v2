<?php

namespace App\Http\Controllers;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\Translators\AnnouncementTranslator;
use App\Http\Services\CommonUserService\Translators\DocumentTranslator;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class StaticController extends Controller
{
    /**
     * Get all required links for training page.
     *
     * @authenticated
     * @return JsonResponse
     */
    public function trainings(): JsonResponse
    {
        $cusClient = new CommonUserService();
        $role = auth()->user()->role->name;
        return response()->json([
            'data' => DocumentTranslator::collection($cusClient->getTrainingDocuments($role)),
            'status' => __('success'),
        ]);
    }

    public function videos(): JsonResponse
    {
        $cusClient = new CommonUserService();
        $role = auth()->user()->role->name;
        return response()->json([
            'data' => DocumentTranslator::collection($cusClient->getTrainingVideos($role)),
            'status' => __('success'),
        ]);
    }

    public function announcements(): JsonResponse
    {
        $cusClient = new CommonUserService();

        $user = auth()->user();
        $date = Carbon::now($user->school->timezone)->format('Y-m-d');
        $role = $user->role->name;
        $school = $user->school->cus_uuid;
        return response()->json([
            'data' => AnnouncementTranslator::collection($cusClient->getAnnouncements($role, $date, $school)),
            'status' => __('success'),
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function helpCenter(): JsonResponse
    {
        return response()->json([
            'data' => config('helpcenter.' . request()->get('key'))
        ]);
    }
}
