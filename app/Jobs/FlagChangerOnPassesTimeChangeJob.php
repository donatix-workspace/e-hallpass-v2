<?php

namespace App\Jobs;

use App\Models\NurseRoom;
use App\Models\Pass;
use App\Models\Room;
use App\Models\TeacherPassSetting;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;

class FlagChangerOnPassesTimeChangeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $teacherPassSettings;
    public $schoolId;

    public function __construct(TeacherPassSetting $teacherPassSetting, int $schoolId)
    {
        $this->teacherPassSettings = $teacherPassSetting;
        $this->schoolId = $schoolId;
    }


    public function handle()
    {
        $timeSettings = $this->teacherPassSettings;

        try {
            logger(
                'Flag out passes job runned at : ' . now()
            );
            DB::beginTransaction();
            Pass::fromSchoolId($this->schoolId)
                ->where('approved_at', '!=', null)
                ->where('parent_id', null)
                ->where('completed_at', '!=', null)
                ->where('pass_status', Pass::INACTIVE)
                ->chunk(1000, function ($passes) use ($timeSettings) {
                    foreach ($passes as $pass) {
                        logger("Flag Out passes", [$pass->total_time, $timeSettings->min_time]);
                        if ($pass->total_time < $timeSettings->min_time) {
                            logger('Total_time: ' . $pass->total_time);
                            $pass->update([
                                'flagged_at' => null,
                            ]);
                        }

                        if ($pass->total_time >= $timeSettings->min_time) {
                            logger('Total_time: ' . $pass->total_time);
                            $pass->update([
                                'flagged_at' => now(),
                            ]);
                        }

                        if ($pass->to_type === Room::class) {
                            $nurseRoom = NurseRoom::where('school_id', $pass->school_id)
                                ->where('room_id', $pass->to_id)->first();

                            if ($nurseRoom !== null && $pass->total_time < $nurseRoom->end_time) {
                                $pass->update([
                                    'extended_at' => null
                                ]);
                            }


                            if ($nurseRoom !== null && $pass->total_time > $nurseRoom->end_time) {
                                $pass->update([
                                    'extended_at' => now(),
                                ]);
                            }
                        }
                    }
                });
            DB::commit();
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return false;
        } catch (\Throwable $e) {
            Log::error($e->getMessage());

            return false;
        }
    }
}
