<?php

namespace App\Http\Controllers\Passes;

use App\Http\Controllers\Controller;
use App\Http\Resources\PassEntityCollection;
use App\Models\AppointmentPass;
use App\Models\Pass;
use App\Models\PassBlock;
use App\Models\Period;
use App\Models\Room;
use App\Models\RoomRestriction;
use App\Models\TeacherPassSetting;
use App\Models\Unavailable;
use App\Models\User;
use App\Policies\PassPolicy;
use Cache;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;

/**
 * @group Passes entities for creation.
 */
class EntityController extends Controller
{
    /**
     * Return the pass entities for the create form
     *
     * @return PassEntityCollection
     * @throws AuthorizationException
     * @apiResourceCollection App\Http\Resources\PassEntityCollection
     * @authenticated
     */
    public function index()
    {
        $this->authorize('create', Pass::class);
        $user = auth()->user();

        $schoolLocationsRestrictionsIds = RoomRestriction::getRestrictedRoomIds();

        $periods = Period::fromSchoolId($user->school_id)
            ->whereStatus(1)
            ->get();

        $teacherPassSettings = TeacherPassSetting::fromSchoolId(
            $user->school_id
        )->first();

        $userRestrictionCounter = RoomRestriction::active()
            ->fromSchoolId($user->school_id)
            ->ofUserId($user->id)
            ->where('from_date', '<=', now()->toDateString())
            ->where('to_date', '>=', now()->toDateString())
            ->where('type', RoomRestriction::ACCESS_DENIED_GROUP)
            ->count();

        $schoolOrStudentPassLimit = PassPolicy::checkPassLimit();
        $unavailableTeachers = Unavailable::fromSchoolId($user->school_id)
            ->active()
            ->get();
        $passBlock = PassBlock::fromSchoolId($user->school_id)
            ->whereStudents(1)
            ->where('from_date', '<=', Carbon::now())
            ->where('to_date', '>=', Carbon::now())
            ->first();

        $appointmentPassOfStudent = AppointmentPass::whereCreatedBy($user->id)
            ->where('confirmed_at', null)
            ->where('canceled_at', null)
            ->whereDate('created_at', today())
            ->where('expired_at', null)
            ->count();

        return new PassEntityCollection([
            'passes' => [
                'settings' => $teacherPassSettings
            ],
            'restriction' => [
                'count' => $userRestrictionCounter
            ],
            'pass_limit' => [
                'has' => $schoolOrStudentPassLimit['remain']['max'] > 0,
                'student_reached_limit' => $schoolOrStudentPassLimit['status'],
                'current' => $schoolOrStudentPassLimit['remain']['current'],
                'max' => $schoolOrStudentPassLimit['remain']['max'],
                'from_date' => Carbon::parse(
                    $schoolOrStudentPassLimit['from_date']
                )->toDateString(),
                'to_date' => Carbon::parse(
                    $schoolOrStudentPassLimit['to_date']
                )->toDateString()
            ],
            'unavailable_teachers' => $unavailableTeachers,
            'pass_block' => $passBlock,
            'appointment_passes' => [
                'created_count' => $appointmentPassOfStudent,
                'max_count' => AppointmentPass::MAX_STUDENT_APPOINTMENTS
            ],
            'periods' => $periods
        ]);
    }

    /**
     * Return the pass entities for the create form
     *
     * @return PassEntityCollection
     * @throws AuthorizationException
     * @apiResourceCollection App\Http\Resources\PassEntityCollection
     * @authenticated
     */
    public function passLimits(): PassEntityCollection
    {
        $this->authorize('create', Pass::class);

        $schoolOrStudentPassLimit = PassPolicy::checkPassLimit();

        return new PassEntityCollection([
            'pass_limit' => [
                'has' => $schoolOrStudentPassLimit['remain']['max'] > 0,
                'student_reached_limit' => $schoolOrStudentPassLimit['status'],
                'current' => $schoolOrStudentPassLimit['remain']['current'],
                'max' => $schoolOrStudentPassLimit['remain']['max'],
                'from_date' => Carbon::parse(
                    $schoolOrStudentPassLimit['from_date']
                )->toDateString(),
                'to_date' => Carbon::parse(
                    $schoolOrStudentPassLimit['to_date']
                )->toDateString()
            ]
        ]);
    }
}
