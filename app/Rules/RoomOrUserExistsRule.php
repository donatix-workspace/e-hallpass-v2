<?php

namespace App\Rules;

use App\Models\Room;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class RoomOrUserExistsRule implements Rule
{
    private $request;

    /**
     * Create a new rule instance.
     *
     * @param $request
     */
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->request['unavailable_type'] === User::class) {
            $user = User::where('id', $value)
                ->belongsToThisSchool()
                ->first();

            $userIsAdminOrTeacher = !optional($user)->isStudent();

            return $user !== null && $userIsAdminOrTeacher;
        }

        if ($this->request['unavailable_type'] === Room::class) {
            $room = Room::where('id', $value)
                ->where('school_id', auth()->user()->school_id)
                ->first();

            return $room !== null;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('passes.user.or.room.not.exists');
    }
}
