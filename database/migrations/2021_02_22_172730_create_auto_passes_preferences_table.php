<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutoPassesPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_passes_preferences', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('auto_pass_id');
            $table->unsignedBigInteger('user_id');
            $table->boolean('status');
            $table->enum('mode', [\App\Models\AutoPassPreference::MODE_START_ONLY, \App\Models\AutoPassPreference::MODE_START_AND_STOP]);
            $table->timestamps();
        });

        Schema::table('auto_passes_preferences', function (Blueprint $table) {
           $table->foreign('auto_pass_id')->references('id')->on('auto_passes');
           $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_passes_preferences', function (Blueprint $table) {
            $table->dropForeign('auto_passes_preferences_auto_pass_id_foreign');
            $table->dropForeign('auto_passes_preferences_user_id_foreign');
        });
        Schema::dropIfExists('auto_passes_preferences');
    }
}
