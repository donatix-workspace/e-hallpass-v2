<?php

namespace App\Traits;

trait PassMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'id' => [
                'type' => 'long'
            ],
            'user_id' => [
                'type' => 'long'
            ],
            'school_id' => [
                'type' => 'long'
            ],
            'from.created_at' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'from_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'to_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'parent_id' => [
                'type' => 'long',
                'null_value' => 0
            ],
            'edited_at' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 0
            ],
            'expired_at' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 0
            ],
            'created_date' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'canceled_at' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 0
            ],
            'extended_at' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 0
            ],
            'flagged_at' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 0
            ],
            'system_completed' => [
                'type' => 'long'
            ],
            'total_time' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'pass_status' => [
                'type' => 'long'
            ],
            'from_id' => [
                'type' => 'long'
            ],
            'to_id' => [
                'type' => 'long'
            ],
            'completed_by' => [
                'type' => 'long'
            ],
            'created_by' => [
                'type' => 'long'
            ],
            'approved_by' => [
                'type' => 'long'
            ],
            'comments' => [
                'type' => 'nested',
                'include_in_parent' => true,
                'properties' => [
                    'comment' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'status' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'approved_at' => [
                'type' => 'date',
                'format' =>
                    'hour_minute||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||HH:mm||HH||epoch_millis'
            ],
            'completed_at' => [
                'type' => 'date',
                'format' =>
                    'hour_minute||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||HH:mm||HH||epoch_millis'
            ],
            'arrived_at' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'created_at' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis'
            ],
            'created_by_user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'email' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'completed_by_user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'email' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'text',
                        'fielddata' => true
                    ]
                ]
            ],
            'requested_by_user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'email' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'approved_by_user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'email' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'text',
                        'fielddata' => true
                    ]
                ]
            ],
            'from' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'to' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'elastic_advanced_fields' => [
                'properties' => [
                    'approve_time_only' => [
                        'type' => 'date',
                        'format' => 'h:m a||epoch_millis'
                    ],
                    'completed_time_only' => [
                        'type' => 'date',
                        'format' => 'h:m a||epoch_millis'
                    ],
                    'approve_child_time_only' => [
                        'type' => 'date',
                        'format' => 'h:m a||epoch_millis'
                    ],
                    'completed_child_time_only' => [
                        'type' => 'date',
                        'format' => 'h:m a||epoch_millis'
                    ]
                ]
            ],
            'tabular_view' => [
                'properties' => [
                    'approve_time' => [
                        'type' => 'keyword',
                        'null_value' => '-'
                    ],
                    'completed_time' => [
                        'type' => 'keyword',
                        'null_value' => '-'
                    ],
                    'approve_child_time' => [
                        'type' => 'keyword',
                        'null_value' => '-'
                    ],
                    'completed_child_time' => [
                        'type' => 'keyword',
                        'null_value' => '-'
                    ]
                ]
            ],
            'user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'email' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'child' => [
                'properties' => [
                    'id' => [
                        'type' => 'long'
                    ],
                    'pass_status' => [
                        'type' => 'long'
                    ],
                    'status' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'canceled_at' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                        'null_value' => 0
                    ],
                    'extended_at' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                        'null_value' => 0
                    ],
                    'flagged_at' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase',
                        'null_value' => 0
                    ],
                    'system_completed' => [
                        'type' => 'long'
                    ],
                    'from.created_at' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'from_id' => [
                        'type' => 'long'
                    ],
                    'to_id' => [
                        'type' => 'long'
                    ],
                    'completed_by' => [
                        'type' => 'long'
                    ],
                    'created_by' => [
                        'type' => 'long'
                    ],
                    'approved_by' => [
                        'type' => 'long'
                    ],
                    'parent_id' => [
                        'type' => 'long',
                        'null_value' => 0
                    ],
                    'approved_at' => [
                        'type' => 'date',
                        'format' =>
                            'hour_minute||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis'
                    ],
                    'completed_at' => [
                        'type' => 'date',
                        'format' =>
                            'hour_minute||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis'
                    ],
                    'type' => [
                        'type' => 'text',
                        'fielddata' => true
                    ],
                    'created_by_user' => [
                        'properties' => [
                            'first_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'last_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'email' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ]
                        ]
                    ],
                    'completed_by_user' => [
                        'properties' => [
                            'first_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'last_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'email' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ]
                        ]
                    ],
                    'requested_by_user' => [
                        'properties' => [
                            'first_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'last_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'email' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ]
                        ]
                    ],
                    'approved_by_user' => [
                        'properties' => [
                            'first_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'last_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'email' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ]
                        ]
                    ],
                    'from' => [
                        'properties' => [
                            'first_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'last_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ]
                        ]
                    ],
                    'to' => [
                        'properties' => [
                            'first_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'last_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ]
                        ]
                    ],
                    'user' => [
                        'properties' => [
                            'first_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'last_name' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ],
                            'email' => [
                                'type' => 'keyword',
                                'normalizer' => 'lowercase'
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ];
}
