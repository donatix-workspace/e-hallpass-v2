<?php

namespace App\Policies;

use App\Models\ActivePassLimit;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ActivePassLimitPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view any global pass limits.
     *
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return !$user->isStudent();
    }

    /**
     * Determine whether the user can view the global pass limit.
     *
     * @param User $user
     * @param ActivePassLimit $globalPassLimit
     * @return bool
     */
    public function view(User $user, ActivePassLimit $globalPassLimit)
    {
        //
    }

    /**
     * Determine whether the user can create global pass limits.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the global pass limit.
     *
     * @param User $user
     * @param ActivePassLimit $globalPassLimit
     * @return bool
     */
    public function update(User $user, ActivePassLimit $globalPassLimit)
    {
        //
    }

    /**
     * Determine whether the user can delete the global pass limit.
     *
     * @param User $user
     * @param ActivePassLimit $globalPassLimit
     * @return bool
     */
    public function delete(User $user, ActivePassLimit $globalPassLimit)
    {
        //
    }

    /**
     * Determine whether the user can restore the global pass limit.
     *
     * @param User $user
     * @param ActivePassLimit $globalPassLimit
     * @return bool
     */
    public function restore(User $user, ActivePassLimit $globalPassLimit)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the global pass limit.
     *
     * @param User $user
     * @param ActivePassLimit $globalPassLimit
     * @return bool
     */
    public function forceDelete(User $user, ActivePassLimit $globalPassLimit)
    {
        //
    }
}
