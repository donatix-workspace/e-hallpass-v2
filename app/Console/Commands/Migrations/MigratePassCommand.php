<?php

namespace App\Console\Commands\Migrations;

use App\Console\Commands\Migrations\ModelsEhpV1\EpassDetail;
use App\Models\Pass;
use App\Models\Room;
use App\Models\School;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Lcobucci\JWT\Exception;

class MigratePassCommand extends Command
{
    protected $signature = 'migrate_sys:pass {--oldConnection=} {--oldTableName=} {--lastId=}';

    protected $description = 'Migrate Passes From EhpV1 to EhpV2';

    public function handle()
    {
        $oldDbConnection = $this->option('oldConnection');
        $oldTableName = $this->option('oldTableName');

        if (!$oldDbConnection) {
            $oldDbConnection = 'oldDB';
        }

        if (!$oldTableName) {
            $oldTableName = 'epass_detail';
        }

        DB::connection($oldDbConnection)->disableQueryLog();
        DB::disableQueryLog();
        $oldTypes = [
            0 => Pass::STUDENT_CREATED_PASS,
            1 => Pass::APPOINTMENT_PASS,
            3 => Pass::KIOSK_PASS,
        ];
        $oldDb = DB::connection('oldDB');
        $newDb = DB::connection('mysql');

        $this->info('--- Migrating Passes (Childs) --- ');

        $epassDetails = DB::connection($oldDbConnection)->table($oldTableName)
            ->orderBy('pass_id', 'asc')
            ->when($this->option('lastId'), function ($query) {
                $query->where('pass_id', '>=', $this->option('lastId'));
            })
            ->when($oldDbConnection === 'oldDB', function ($query) {
                $query->where('migrated', 0);
            })
            ->limit(10000000)
            ->get()
            ->toArray();

        $this->output->progressStart(count($epassDetails));

        $epassDetailsChunked = array_chunk($epassDetails, 1500);

        foreach ($epassDetailsChunked as $epassDetailChunk) {
            $insertablePasses = [];
            $migratedOldIds = [];
            foreach ($epassDetailChunk as $epassDetail) {

                $oldSchool = $oldDb->table('school_schedule')->select('school_id')->where('school_id', optional($epassDetail)->school_id)->first();
                if ($oldSchool) {
                    $newSchool = $newDb->table('schools')->where('old_id', $oldSchool->school_id)->select('id', 'timezone')->first();

                    $oldStudentUser = $oldDb->table('users')->where('user_id', $epassDetail->student_id)->first();

                    $newStudentUser = $newDb->table('users')->where('old_id', optional($oldStudentUser)->user_id)
                        ->where('school_id', $newSchool->id)
                        ->first();

                    $approvedBy = $oldDb->table('users')->where('user_id', $epassDetail->out_by_teacher)
                        ->first();
                    $approvedByNew = $newDb->table('users')->where('old_id', optional($approvedBy)->user_id)
                        ->where('school_id', $newSchool->id)
                        ->first();

                    $createdBy = $oldDb->table('users')->where('user_id', $epassDetail->created_by)
                        ->first();
                    $createdByNew = $newDb->table('users')->where('old_id', optional($createdBy)->user_id)
                        ->where('school_id', $newSchool->id)
                        ->first();

                    $completedBy = $oldDb->table('users')->where('user_id', $epassDetail->in_by_teacher)
                        ->first();
                    $completedBy = $newDb->table('users')->where('old_id', optional($completedBy)->user_id)
                        ->where('school_id', $newSchool->id)
                        ->first();

                    if ($epassDetail->fromteacher_id) {
                        $fromId = $oldDb->table('users')->where('user_id', $epassDetail->fromteacher_id)
                            ->first();
                        $fromIdNew = $newDb->table('users')->where('old_id', optional($fromId)->user_id)
                            ->where('school_id', $newSchool->id)
                            ->first();
                    } else {
                        $fromId = $oldDb->table('rooms')->where('room_id', $epassDetail->fromroom_id)
                            ->first();

                        $fromIdNew = DB::table('rooms')->where('old_id', $fromId->room_id)
                            ->where('school_id', $newSchool->id)
                            ->first();
                    }

                    if ($epassDetail->toteacher_id) {
                        $toId = $oldDb->table('users')->where('user_id', $epassDetail->toteacher_id)
                            ->first();
                        $toIdNew = $newDb->table('users')->where('old_id', optional($toId)->user_id)
                            ->where('school_id', $newSchool->id)
                            ->first();
                    } else {
                        $toId = $oldDb->table('rooms')->where('room_id', $epassDetail->toroom_id)
                            ->first();
                        $toIdNew = DB::table('rooms')->where('old_id', optional($toId)->room_id)
                            ->where('school_id', $newSchool->id)
                            ->first();
                    }

                    $type = $oldTypes[$epassDetail->passtype];

                    if ($epassDetail->passtype !== 1 && optional($createdBy)->role_id !== config('roles.student')) {
                        $type = Pass::PROXY_PASS;
                    }
                    if ($newStudentUser && $fromIdNew && $toIdNew) {
                        $migratedOldIds[] = $epassDetail->pass_id;
                        $insertablePasses[] = [
                            'user_id' => $newStudentUser->id,
                            'school_id' => $newSchool->id,
                            'approved_by' => optional($approvedByNew)->id,
                            'parent_id' => null,
                            'completed_by' => optional($completedBy)->id,
                            'pass_status' => $epassDetail->pass_status > 1 ? 1 : $epassDetail->pass_status,
                            'edited_at' => $epassDetail->edit_status ? now()->toDateTimeString() : null,
                            'created_by' => optional($createdByNew)->id,
                            'approved_at' => School::convertSchoolDatesToUTC($epassDetail->outdate, $newSchool->timezone),
                            'completed_at' => School::convertSchoolDatesToUTC($epassDetail->indate, $newSchool->timezone),
                            'from_type' => $epassDetail->fromteacher_id > 0 ? User::class : Room::class,
                            'from_id' => optional($fromIdNew)->id === null ? 0 : optional($fromIdNew)->id,
                            'to_type' => $epassDetail->toteacher_id > 0 ? User::class : Room::class,
                            'to_id' => optional($toIdNew)->id === null ? 0 : optional($toIdNew)->id,
                            'canceled_at' => $epassDetail->outaction === 4 && $epassDetail->indate === null ? now() : null,
                            'type' => $type,
                            'approved_with' => optional($approvedByNew)->role_id === config('roles.student') ? Pass::BY_AUTOPASS : null,
                            'ended_with' => optional($approvedByNew)->role_id === config('roles.student') ? Pass::BY_AUTOPASS : null,
                            'old_parent_id' => $epassDetail->parent_id,
                            'created_at' => $epassDetail->created_at,
                            'updated_at' => $epassDetail->updated_at,
                            'old_id' => $epassDetail->pass_id
                        ];
                    }
                }
            }
            try {
                // TODO: Insert passes comments
                DB::table('passes')->insert($insertablePasses);

                if ($oldDbConnection === 'oldDB') {
                    DB::connection($oldDbConnection)->table('epass_detail')
                        ->whereIntegerInRaw('pass_id', $migratedOldIds)
                        ->update(['migrated' => 1]);
                }

                $this->output->progressAdvance(count($insertablePasses));
            } catch
            (\Exception $e) {
                $this->output->info($e->getMessage());
            }
        }

        $this->output->progressFinish();
        sleep(2);
    }
}
