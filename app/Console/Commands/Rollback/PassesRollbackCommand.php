<?php

namespace App\Console\Commands\Rollback;

use App\Models\Pass;
use App\Models\Room;
use App\Models\School;
use App\Models\User;
use DB;
use Illuminate\Console\Command;

class PassesRollbackCommand extends Command
{
    protected $signature = 'ehpv1_rollback:passes';

    protected $description = 'Rollback EhpV2 to EhpV1 (Passes Module)';

    /**
     * @return int
     */
    public function handle(): int
    {
        $this->info('--- Starting rollback on Passes module --- ');
        $this->newLine();

        $oldDb = DB::connection('oldDb');
        $newDb = DB::connection('default');

        $oldDb->disableQueryLog();
        $newDb->disableQueryLog();

        $oldTypes = [
            0 => Pass::STUDENT_CREATED_PASS,
            1 => Pass::APPOINTMENT_PASS,
            3 => Pass::KIOSK_PASS,
        ];
        $oldTypes = array_flip($oldTypes);

        $newPasses = $newDb->table('passes')
            ->whereBetween('created_at', [now()->subMonth()->endOfDay()->toDateString(), now()->addMonth()->toDateTimeString()])
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();

        $this->output->progressStart(count($newPasses));

        $newPassesChunk = array_chunk($newPasses, 1500);

        foreach ($newPassesChunk as $newPassChunk) {
            $insertablePassesArray = [];
            foreach ($newPassesChunk as $pass) {
                $newUser = $newDb->table('users')->where('id', $pass->user_id)->select('old_id')->first();
                $newSchool = $newDb->table('schools')->where('id', $pass->school_id)->select('id', 'timezone', 'old_id')->first();

                $oldTeacherFromId = 0;
                $oldTeacherToId = 0;
                $oldRoomFromId = 0;
                $oldRoomToId = 0;

                if ($pass->from_type === User::class) {
                    $oldTeacherFromId = $newDb->table('users')->where('id', $pass->from_id)->select('old_id')->first();
                } else {
                    $oldRoomFromId = $newDb->table('rooms')->where('id', $pass->from_id)->select('old_id')->first();
                }

                if ($pass->to_type === User::class) {
                    $oldTeacherToId = $newDb->table('users')->where('id', $pass->to_id)->select('old_id')->first();
                } else {
                    $oldRoomToId = $newDb->table('rooms')->where('id', $pass->to_id)->select('old_id')->first();
                }

                $approvedByTeacher = $newDb->table('users')->where('id', $pass->approved_by)->select('old_id')->first();

                $completedByTeacher = $newDb->table('users')->where('id', $pass->completed_by)->select('old_id')->first();

                // In Action = 3 default end;
                $inAction = 0;

                // OutAction = 10 approved by teacher
                // OutAction = 4 // Canceled on the first state
                // arrived inaction = 10
                // out action = 10 default approve without parent
                // out date, in date school timezones
                // outaction = 10 with child one approve
                $outAction = 0;
                if ($pass->completed_at !== null && $pass->parent_id === null) {
                    $inAction = 3;
                }

                if ($pass->approved_at !== null && $pass->parent_id === null) {
                    $outAction = 10;
                }

                if ($pass->canceled_at !== null && $pass->approved_by === null) {
                    $outAction = 4;
                }

                $passParentId = null;
                if ($pass->parent_id !== null) {
                    $passParentId = $newDb->table('passes')->select('old_id')->where('id', $pass->parent_id)
                        ->first();
                }

                if ($newUser) {
                    $insertablePassesArray[] = [
                        'student_id' => $newUser->old_id,
                        'outdate' => School::convertTimeToCorrectTimezone($pass->approved_at, $newSchool->timezone),
                        'indate' => School::convertTimeToCorrectTimezone($pass->completed_at, $newSchool->timezone),
                        'fromteacher_id' => $pass->from_type === User::class ? $oldTeacherFromId->old_id : 0,
                        'froomteacher_note' => $pass->from_teacher_note,
                        'created_at' => School::convertTimeToCorrectTimezone($pass->created_at, $newSchool->timezone),
                        'updated_at' => School::convertTimeToCorrectTimezone($pass->updated_at, $newSchool->timezone),
                        'parent_id' => $passParentId ? $passParentId->old_id : null,
                        'school_id' => $newSchool->old_id,
                        'fromroom_id' => $pass->from_type === Room::class ? $oldRoomFromId->old_id : 0,
                        'outroom' => 0,
                        'toroom_id' => $pass->to_type === Room::class ? $oldRoomToId->old_id : 0,
                        'period' => 0,
                        'commenting' => $pass->from_teacher_note,
                        'ordering' => -1,
                        'toteacher_id' => $pass->to_type === User::class ? $oldTeacherToId->old_id : 0,
                        'toteacher_note' => null,
                        'inaction' => $inAction,
                        'in_by_teacher' => $completedByTeacher->old_id,
                        'out_by_teacher' => $approvedByTeacher->old_id,
                        'pass_status' => $pass->pass_status,
                        'edit_status' => $pass->edited_at ? 1 : 0,
                        'passtype' => $oldTypes[$pass->type],
                        'preapproved' => 0,
                        'created_by' => $pass->requested_by,
                        'outaction' => $outAction
                    ];
                }
            }

            try {
                DB::beginTransaction();
                $oldDb->table('epass_detail')
                    ->insert($insertablePassesArray);
                $this->output->progressAdvance(count($insertablePassesArray));
                DB::commit();
            } catch (\Throwable $e) {
                $this->info($e->getMessage());
            }
        }


        $this->output->progressFinish();

        return 0;
    }
}
