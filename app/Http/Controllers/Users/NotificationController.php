<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Resources\AppointmentNotificationCollection;
use App\Models\AppointmentNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * @group User notifications.
 */
class NotificationController extends Controller
{

    /**
     * Get all notifications.
     *
     * @authenticated
     * @apiResourceCollection App\Http\Resources\AppointmentNotificationCollection
     * @apiResourceModel App\Models\AppointmentNotification
     * @return AppointmentNotificationCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $user = auth()->user();

        $this->authorize('viewAny', AppointmentNotification::class);

        $appointmentNotifications = AppointmentNotification::with(['appointmentPass', 'updatedBy:first_name,last_name,email,id'])
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->whereSchoolId($user->school_id)
            ->paginate(config('pagination.default_pagination'));

        return new  AppointmentNotificationCollection($appointmentNotifications);
    }

    /**
     * See all notifications
     *
     * @authenticated
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function seenAll()
    {
        $this->authorize('viewAny', AppointmentNotification::class);

        $user = auth()->user();

        $appointmentsNotifications = AppointmentNotification::whereUserId($user->id)
            ->whereSchoolId($user->school_id)
            ->update([
                'seen_at' => Carbon::now()
            ]);

        return response()->json([
            'data' => [],
            'message' => __('notifications.seen.all.success'),
            'status' => __('success')
        ]);
    }

    public function bulkSeen(Request $request)
    {
        $appointmentsNotifications = $request->validate([
            'notifications' => 'array|required',
            'notifications.*' => 'integer|required'
        ]);
        $this->authorize('viewAny', AppointmentNotification::class);

        AppointmentNotification::whereIn('id', $appointmentsNotifications['notifications'])
            ->whereUserId(auth()->user()->id)
            ->whereSeenAt(NULL)
            ->update([
                'seen_at' => Carbon::now()
            ]);

        return response()->json([
            'data' => [],
            'message' => __('notifications.seen.success'),
            'status' => __('success')
        ]);
    }

    /**
     * Seen a specific notification.
     * @urlParam appointmentNotification integer required The id of the notification.
     *
     * @authenticated
     * @param AppointmentNotification $appointmentNotification
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function seen(AppointmentNotification $appointmentNotification)
    {
        $this->authorize('view', $appointmentNotification);

        $appointmentNotification->update([
            'seen_at' => Carbon::now()
        ]);

        return response()->json([
            'data' => [],
            'message' => __('notifications.seen.success'),
            'status' => __('success')
        ]);
    }

    /**
     * Clear all of the notification.
     *
     * @authenticated
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function clear()
    {

        $this->authorize('delete', AppointmentNotification::class);

        $user = auth()->user();

        $appointmentNotification = AppointmentNotification::whereUserId($user->id)
            ->whereSchoolId($user->school_id)
            ->delete();

        return response()->json([
            'data' => [],
            'message' => __('notifications.clear.success'),
            'status' => __('success')
        ]);
    }
}
