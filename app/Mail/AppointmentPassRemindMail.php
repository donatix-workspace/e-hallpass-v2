<?php

namespace App\Mail;

use App\Models\AppointmentPass;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AppointmentPassRemindMail extends Mailable
{
    use Queueable, SerializesModels;

    public $appointmentPass;
    public $adult;
    public $token;
    public $tokenAdmin;
    public $timeRemain;

    /**
     * Create a new message instance.
     *
     * @param AppointmentPass $appointmentPass
     * @param bool $adult
     * @param string $token
     * @param string $tokenAdmin
     */
    public function __construct(
        AppointmentPass $appointmentPass,
        bool $adult = false,
        string $token = '',
        string $tokenAdmin = ''
    ) {
        $this->appointmentPass = $appointmentPass;
        $this->adult = $adult;
        $this->token = $token;
        $this->tokenAdmin = $tokenAdmin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): AppointmentPassRemindMail
    {
        $schoolName = $this->appointmentPass->school->name;
        $time = $this->appointmentPass->for_date
            ->setTimezone($this->appointmentPass->school->getTimezone())
            ->format('g:i A');
        $reminderNow = $this->appointmentPass->for_date->diffInMinutes(now());
        logger('Apt AppointmentPassRemindMail is adult?: ' . $this->adult);

        if ($reminderNow >= 3 && $reminderNow <= 6) {
            $this->timeRemain = AppointmentPass::SECOND_REMIND_MINUTE;
        } else {
            $this->timeRemain =
                $reminderNow >= 19
                    ? AppointmentPass::FIRST_REMIND_MINUTE
                    : $this->appointmentPass->for_date->diffInMinutes(now());
        }

        $subjectText = "Pass to $schoolName at $time: Ready in $this->timeRemain minutes";

        if ($this->timeRemain <= 2) {
            $subjectText = "Pass to $schoolName at $time: Ready NOW!";
        }

        return $this->view('emails.appointments.remind')->subject($subjectText);
    }
}
