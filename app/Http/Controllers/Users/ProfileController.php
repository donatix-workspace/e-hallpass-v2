<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\NotificationAndAudioPreferencesMobileRequest;
use App\Http\Requests\PinRequest;
use App\Http\Requests\RandomPinsRequest;
use App\Http\Requests\StaffRoomsRequest;
use App\Http\Requests\StoreProfileRequest;
use App\Http\Requests\UserDeviceTokenRequest;
use App\Http\Resources\ProfileCollection;
use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\ActivePassLimit;
use App\Models\AppointmentPass;
use App\Models\Pass;
use App\Models\PassLimit;
use App\Models\Pin;
use App\Models\Polarity;
use App\Models\Room;
use App\Models\RoomPin;
use App\Models\RoomRestriction;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\StaffSchedule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * @group Profile management
 */
class ProfileController extends Controller
{
    /**
     * Get profile collection data
     *
     * @return ProfileCollection
     */
    public function index()
    {
        $teacherPin = Pin::where([
            ['school_id', '=', auth()->user()->school_id],
            ['pinnable_type', '=', Pin::USER_TYPE]
        ])->first();

        $rooms = Room::where([
            ['school_id', '=', auth()->user()->school_id],
            ['status', '=', '1']
        ])->get(['id', 'name']);

        $staffSchedules = StaffSchedule::where([
            ['user_id', '=', auth()->user()->id],
            ['school_id', '=', auth()->user()->school_id]
        ])->get(['room_id']);

        $roomPins = Pin::where('school_id', auth()->user()->school_id)
            ->fromType(Room::class)
            ->get(['pinnable_id', 'pin']);

        $schoolModules = auth()
            ->user()
            ->school->modules();

        $data = [
            'teacherPin' => $teacherPin,
            'rooms' => $rooms,
            'staffSchedule' => $staffSchedules,
            'roomPins' => $roomPins,
            'schoolModules' => $schoolModules
        ];

        return new ProfileCollection($data);
    }

    /**
     * Get pass limit of spec user.
     *
     * @urlParam user integer required The id of the user.
     *
     * @authenticated
     * @param User $user
     * @return JsonResponse
     */
    public function limitInfo(User $user): JsonResponse
    {
        $passLimitOfUser = PassLimit::whereType(User::class)
            ->without('limitable')
            ->select('limit')
            ->where('limitable_id', $user->id)
            ->whereDate('from_date', '<=', now())
            ->whereDate('to_date', '>=', now())
            ->select('limit')
            ->first();

        $polarityCanceledIds = Polarity::active()
            ->ofUserId($user->id)
            ->fromSchoolId($user->school->id)
            ->selectRaw(
                'if(first_user_id = ?, second_user_id, first_user_id) as polarity_ids',
                [$user->id]
            )
            ->pluck('polarity_ids');

        $polarityOutStudents = Pass::activeWithoutApprove()
            ->fromSchoolId($user->school->id)
            ->whereIntegerInRaw('user_id', $polarityCanceledIds)
            ->get();

        $activePassLimitStatus = ActivePassLimit::showActivePassLimitMessageStatus(
            ActivePassLimit::PROXY_PASS
        );

        $passesRemain = 0;

        if (optional($passLimitOfUser)->limit) {
            $passesRemain =
                $passLimitOfUser->limit - $user->passes_for_today_count;
        }

        $restrictedRooms = RoomRestriction::getRestrictedRoomIds($user);
        $existsWithin15Mins = false;

        if (request()->filled('apt_for_time')) {
            $time = request()->get('apt_for_time');

            $dateSubMinutes15 = Carbon::parse($time)
                ->setTimezone(config('app.timezone'))
                ->subMinutes(15);
            $dateWithIn15Minutes = Carbon::parse($time)
                ->setTimezone(config('app.timezone'))
                ->addMinutes(15);

            $existsWithin15Mins = AppointmentPass::whereBetween('for_date', [
                $dateSubMinutes15,
                $dateWithIn15Minutes
            ])
                ->where('user_id', $user->id)
                ->where('expired_at', null)
                ->where('canceled_at', null)
                ->exists();
        }

        return response()->json([
            'passes_remain' =>
                $passesRemain < 0 ? 'reached the limit' : $passesRemain,
            'polarity_active' => !$polarityOutStudents->isEmpty(),
            'show_active_pass_limit_message' => $activePassLimitStatus,
            'restricted_rooms' => $restrictedRooms,
            'appointment_exists_within_15_mins' => $existsWithin15Mins
        ]);
    }

    /**
     * Update user profile
     *
     * @bodyParam avatar file required Avatar file
     * @bodyParam initials string required User initials
     * @bodyParam first_name string required User first name
     * @bodyParam last_name string required User last name
     * @bodyParam is_searchable boolean required Is the user searchable
     * @bodyParam teacher_pin string Teacher pin
     * @bodyParam password string User password
     * @bodyParam allow_appointment_requests boolean Allow appointment requests to this user?
     * @bodyParam allow_passes_to_me boolean Allow passes to this user?
     * @param StoreProfileRequest $request
     * @return JsonResponse
     * @throws Exception
     * @group User management
     */
    public function store(StoreProfileRequest $request): JsonResponse
    {
        $user = auth()->user();

        if ($request->has('avatar') && $request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $filePath =
                "avatars/{$user->id}/" .
                time() .
                '.' .
                $request->avatar->extension();
            Storage::disk('s3')->put($filePath, file_get_contents($file));

            // When the request has file we just upload the file to the storage
            // and set path to the users table database.
            $user->update([
                'avatar' => $filePath
            ]);
        }

        if ($request->has('password')) {
            $user->update([
                'password' => Hash::make($request->input('password'))
            ]);
        }

        $allowAppointmentRequests =
            $request->input('allow_appointment_requests') == 'Yes' ||
            $request->input('allow_appointment_requests') == true;
        $allowPassesToMe =
            $request->input('allow_passes_to_me') == 'Yes' ||
            $request->input('allow_passes_to_me') == true;

        $user->update([
            'user_initials' => $request->input('initials'),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'is_searchable' => (int) $request->input('is_searchable'),
            'allow_appointment_requests' => $allowAppointmentRequests,
            'allow_passes_to_me' => $allowPassesToMe
        ]);

        UpdateCommonUserJob::dispatchSync(
            $user,
            $user,
            $request->input('password') ?? null
        );

        if ($request->has('teacher_pin')) {
            $teacherPinExists = Pin::where('pinnable_id', '!=', $user->id)
                ->whereIn('pinnable_type', [Room::class, User::class])
                ->where('school_id', $user->school_id)
                ->where('pin', $request->input('teacher_pin'))
                ->exists();

            if ($teacherPinExists) {
                return response()->json(
                    [
                        'data' => [],
                        'message' => __(
                            'user.profile.pin.already.exists.for.another.user'
                        ),
                        'status' => __('fail')
                    ],
                    422
                );
            }

            $teacherPin = Pin::where([
                ['pinnable_type', '=', User::class],
                ['pinnable_id', '=', $user->id],
                ['school_id', '=', $user->school_id]
            ])->delete();

            Pin::create([
                'pinnable_type' => User::class,
                'pinnable_id' => $user->id,
                'pin' => $request->input('teacher_pin'),
                'school_id' => $user->school_id
            ]);
        }

        return response()->json([
            'message' => __('user.profile.updated.successfully'),
            'status' => __('success')
        ]);
    }

    /**
     * Delete user avatar
     *
     * @return JsonResponse
     * @group User management
     */
    public function deleteAvatar(): JsonResponse
    {
        $user = auth()->user();
        $user->update(['avatar' => null]);

        return response()->json([
            'message' => __('profile.updated.successfully'),
            'status' => 'success'
        ]);
    }

    /**
     * Check PIN
     *
     * @queryParam  pin string required Pin to check
     * @param Request $request
     * @return JsonResponse
     * @group User management
     */
    public function checkPin(Request $request): JsonResponse
    {
        $pin = Pin::where([
            ['pin', '=', $request->get('pin')],
            ['school_id', '=', auth()->user()->school_id]
        ])->exists();
        if (!$pin) {
            return response()->json([
                'message' => __('pin.invalid'),
                'status' => __('fail')
            ]);
        }

        return response()->json([
            'message' => __('pin.valid'),
            'status' => __('success')
        ]);
    }

    /**
     * Get random PIN
     *
     * @return JsonResponse
     * @group User management
     */
    public function getRandomPin(): JsonResponse
    {
        $randomPin = $this->randomPin();

        return response()->json([
            'random_pin' => $randomPin,
            'status' => __('success')
        ]);
    }

    function randomPin(): string
    {
        $alphabet =
            'abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789';
        $pass = [];
        $alphabetLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphabetLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    /**
     * Update Pin
     * If the authenticated user has a pin this method will update the existing one
     * or create a new one if user pin not exists.
     *
     *
     * @bodyParam pin string required The new pin
     *
     * @param PinRequest $request
     * @bodyParam pin string required The new pin.
     * @return JsonResponse
     *
     * @group User management
     */
    public function setPin(PinRequest $request)
    {
        $user = auth()->user();
        $userPin = Pin::where([
            ['pinnable_type', '=', User::class],
            ['pinnable_id', '=', $user->id],
            ['school_id', '=', auth()->user()->school_id]
        ]);
        if ($userPin->exists()) {
            $userPin->update(['pin' => $request->input('pin')]);
        } else {
            Pin::create([
                'pin' => $request->input('pin'),
                'pinnable_type' => User::class,
                'pinnable_id' => $user->id,
                'school_id' => auth()->user()->school_id
            ]);
        }

        return response()->json([
            'message' => __('pin.set.successfully'),
            'status' => __('success')
        ]);
    }

    /**
     * Set random PINs
     *
     * @bodyParam users string required Comma separated string of user ids
     * @param RandomPinsRequest $request
     * @return JsonResponse
     * @group User management
     */
    public function setRandomPins(RandomPinsRequest $request): JsonResponse
    {
        $users = explode(',', $request->input('users'));
        foreach ($users as $userId) {
            $user = User::where('id', $userId)
                ->withoutGlobalScopes()
                ->where('school_id', auth()->user()->school_id)
                ->first();

            if (!$user) {
                return response()->json([
                    'data' => ['id' => $userId],
                    'message' => __('userId.with.id.not.found'),
                    'status' => __('fail')
                ]);
            }

            if (!$user->isStudent() && $user->isSubstitute()) {
                $teacherPin = Pin::where([
                    ['pinnable_type', '=', User::class],
                    ['pinnable_id', '=', $userId],
                    ['school_id', '=', auth()->user()->school_id]
                ])->first();
                $randomPin = $this->randomPin();
                if ($teacherPin) {
                    $teacherPin->update(['pin' => $randomPin]);
                } else {
                    Pin::create([
                        'pin' => $randomPin,
                        'pinnable_type' => User::class,
                        'pinnable_id' => $userId,
                        'school_id' => auth()->user()->school_id
                    ]);
                }
            }
        }

        return response()->json([
            'message' => __('pins.generated.and.set.successfully'),
            'status' => __('success')
        ]);
    }

    /**
     * Toggle User Audio
     *
     * @return JsonResponse
     * @group User management
     */
    public function audioPreference(): JsonResponse
    {
        $user = auth()->user();
        $user->update(['audio_enabled' => $user->audio_enabled ? 0 : 1]);

        return response()->json([
            'message' => __('user.audio.toggled'),
            'status' => __('success')
        ]);
    }

    /**
     * Toggle User Audio
     *
     * @param NotificationAndAudioPreferencesMobileRequest $request
     * @return JsonResponse
     * @group User management
     */
    public function notificationAndAudioPreferencesMobile(
        NotificationAndAudioPreferencesMobileRequest $request
    ): JsonResponse {
        $user = auth()->user();
        $user->mobileSettings()->updateOrCreate(
            [
                'user_id' => $user->id
            ],
            [
                'notification_settings' => $request->input(
                    'notification_settings'
                ),
                'mobile_sound' => $request->input('mobile_sound')
            ]
        );

        return response()->json([
            'message' => __('user.mobile.preferences.and.audio.adjusted'),
            'status' => __('success')
        ]);
    }

    public function deviceToken(UserDeviceTokenRequest $request): JsonResponse
    {
        $user = auth()->user();
        $validatedData = $request->validated();
        $status = $user->update($validatedData);

        return response()->json([
            'message' => __('user.device.updated'),
            'status' => $status ? __('success') : __('error')
        ]);
    }

    protected function isPinAvailable($roomId = 0, $pin = ''): bool
    {
        if ($roomId > 0 && !empty($pin)) {
            if (
                Pin::where('pin', $pin)
                    ->where('pinnable_type', Room::class)
                    ->where('school_id', auth()->user()->school_id)
                    ->count()
            ) {
                return false;
            }
            return true;
        }
        return false;
    }
}
