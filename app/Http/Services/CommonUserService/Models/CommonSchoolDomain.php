<?php

namespace App\Http\Services\CommonUserService\Models;


use Illuminate\Database\Eloquent\Model;

class CommonSchoolDomain extends Model
{
    protected $connection = 'commonDb';
    protected $table = 'common_bm_school_email_domain';
}
