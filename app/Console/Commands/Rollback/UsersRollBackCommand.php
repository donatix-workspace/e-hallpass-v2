<?php

namespace App\Console\Commands\Rollback;

use App\Models\Pass;
use DB;
use Illuminate\Console\Command;

class UsersRollBackCommand extends Command
{
    protected $signature = 'ehpv1_rollback:users';

    protected $description = 'Rollback users fro EHPV2 to EHPV1';

    public function handle()
    {
        $this->info('--- Starting rollback of users ---');

        $newDb = DB::connection();
        $oldDb = DB::connection('oldDB');

        $users = $newDb->table('users')
            ->orderBy('id', 'desc')
            ->get()
            ->toArray();

        $usersChunks = array_chunk($users, 1500);

        $this->output->progressStart(count($users));
        $roles = [
            1 => 5,
            2 => 2,
            3 => 3,
            4 => 1,
            5 => 4,
        ];

        $now = now();

        foreach ($usersChunks as $userChunk) {
            $insertableUsers = [];
            foreach ($userChunk as $user) {
                $oldSchool = $newDb->table('schools')->select('old_id')->where('id', $user->school_id)->first();
                $insertableUsers[] = [
                    'student_sis_id' => $user->student_sis_id,
                    'email' => $user->email,
                    'username' => $user->username,
                    'password' => $user->password,
                    'first_name' => $user->firstname,
                    'last_name' => $user->lastname,
                    'metatag' => $user->metatag,
                    'role_id' => $roles[$user->role_id],
                    'avatar' => $user->avatar, // TODO see how they store the avatars
                    'grade_year' => $user->gradyear,
                    'status' => $user->status,
                    'school_id' => $oldSchool->old_id,
                    'is_searchable' => $user->is_searchable,
                    'user_initials' => $user->user_initials,
                    'is_loggin' => $user->is_loggin,
                    'clever_id' => $user->clever_id,
                    'updated_role' => $user->updated_role,
                    'last_logged_in_at' => $user->last_logged_in_at,
                    'auth_type' => $user->auth_type,
                    'created_at' => $now,
                    'updated_at' => $now,
                    'old_id' => $user->user_id
                ];
            }
        }

    }
}
