<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSchoolIdToContactTracesTable extends Migration
{
    public function up()
    {
        Schema::table('contact_traces', function (Blueprint $table) {
            $table->unsignedBigInteger('school_id')->index('school_id');
        });
    }

    public function down()
    {
        Schema::table('contact_traces', function (Blueprint $table) {
            $table->dropColumn('school_id');
        });
    }
}
