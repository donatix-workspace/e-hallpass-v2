<?php

namespace App\Http\Controllers\Releases;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReleaseCollection;
use App\Http\Resources\ReleaseResource;
use App\Models\Release;

/**
 * @group Releases
 */
class ReleaseController extends Controller
{

    /**
     * Get all releases.
     *
     * @apiResourceModel App\Models\Release
     * @apiResourceCollection  App\Http\Resources\ReleaseCollection
     *
     * @authenticated
     * @return ReleaseCollection
     *
     * @return ReleaseCollection
     */
    public function index(): ReleaseCollection
    {
        $user = auth()->user();

        $releases = Release::fromSchoolId($user->school_id)
            ->orderBy('created_at', 'desc')
            ->paginate(config('pagination.default_pagination'));

        return new ReleaseCollection($releases);
    }

    /**
     * Get latest release
     *
     * @apiResourceModel App\Models\Release
     * @apiResource App\Http\Resources\ReleaseResource
     *
     * @authenticated
     * @return ReleaseResource
     */
    public function show(): ReleaseResource
    {
        $user = auth()->user();

        $release = Release::fromSchoolId($user->school_id)
            ->whereJsonDoesntContain('seen_by_user_ids', optional($user)->id)
            ->orderBy('created_at', 'desc')
            ->first();

        return new ReleaseResource($release);
    }

    /**
     * Seen user release.
     *
     * @apiResourceModel App\Models\Release
     * @apiResource App\Http\Resources\ReleaseResource
     *
     * @param Release $release
     * @return ReleaseResource
     */
    public function seen(Release $release): ReleaseResource
    {
        $user = auth()->user();
        $userIds = collect(json_decode($release->seen_by_user_ids));
        if (!$userIds->contains($user->id)) {
            $userIds->add($user->id);

            $release->update([
                'seen_by_user_ids' => $userIds
            ]);
        }
        return new ReleaseResource($release);
    }
}
