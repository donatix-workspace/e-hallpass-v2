<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomRestrictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms_restrictions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('room_id')->nullable();
            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->enum('type', ['members_only' => \App\Models\RoomRestriction::MEMBERS_ONLY_GROUP, 'access_denied' => \App\Models\RoomRestriction::ACCESS_DENIED_GROUP])->default('members_only')->comment('Can only be members_only or access_denied');
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->string('recurrence_type')->nullable();
            $table->json('grade_year')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
        Schema::table('rooms_restrictions', function (Blueprint $table) {
            $table->foreign('room_id')->references('id')->on('rooms');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rooms_restrictions', function (Blueprint $table) {
            $table->dropForeign('rooms_restrictions_school_id_foreign');
            $table->dropForeign('rooms_restrictions_user_id_foreign');
            $table->dropForeign('rooms_restrictions_room_id_foreign');
        });
        Schema::dropIfExists('rooms_restrictions');
    }
}
