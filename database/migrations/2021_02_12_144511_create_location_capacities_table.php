<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationCapacitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_capacities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('room_id');
            $table->unsignedBigInteger('school_id');
            $table->boolean('status');
            $table->integer('limit');
            $table->timestamps();
        });

        Schema::table('location_capacities', function (Blueprint $table) {
            $table->foreign('room_id')->references('id')->on('rooms');
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('location_capacities', function (Blueprint $table) {
            $table->foreign('location_capacities_room_id_foreign');
            $table->foreign('location_capacities_school_id_foreign');
        });
        Schema::dropIfExists('location_capacities');
    }
}
