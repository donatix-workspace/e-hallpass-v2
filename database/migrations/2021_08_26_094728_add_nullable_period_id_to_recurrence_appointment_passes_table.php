<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullablePeriodIdToRecurrenceAppointmentPassesTable extends Migration
{
    public function up()
    {
        Schema::table('recurrence_appointment_passes', function (Blueprint $table) {
            $table->unsignedBigInteger('period_id')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('recurrence_appointment_passes', function (Blueprint $table) {
            $table->unsignedBigInteger('period_id');
        });
    }
}
