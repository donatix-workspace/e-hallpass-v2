<?php

namespace App\Traits;

trait RoomRestrictionMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'room' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'location' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'user' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                ]
            ],
            'school_id' => [
                'type' => 'long'
            ],
            'type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'from_date' => [
                'type' => 'date',
                "format" => "MM/dd/yyyy",
                "fields" => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'to_date' => [
                'type' => 'date',
                "format" => "MM/dd/yyyy",
                "fields" => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'recurrence_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'status' => [
                'type' => 'long'
            ],
        ]
    ];
}
