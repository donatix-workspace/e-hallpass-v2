<?php

namespace App\Listeners;

use App\Events\PassArrived;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PassArrivedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PassArrived  $event
     * @return void
     */
    public function handle(PassArrived $event)
    {
        //
    }
}
