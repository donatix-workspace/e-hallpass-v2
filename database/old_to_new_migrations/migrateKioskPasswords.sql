SET FOREIGN_KEY_CHECKS  = 0;
START TRANSACTION ;
TRUNCATE ehp_v2_prod.kiosk_users;
INSERT INTO ehp_v2_prod.kiosk_users (user_id, kpassword, password_processed, school_id, created_at, updated_at)
SELECT
    (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldKu.user_id limit 1),
    (SELECT oldKu.kpassword),
    (SELECT oldKu.password_processed),
    (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldKu.school_id limit 1),
    (SELECT NOW()),
    (SELECT NOW())
FROM ehallpassDB.kiosk_users as oldKu;

COMMIT;
SET FOREIGN_KEY_CHECKS  = 1;

UPDATE
    ehp_v2_prod.kiosk_users
SET
    user_id = (
        SELECT
            to_id
        FROM
            duplicate_users
        WHERE
                from_id = user_id
        LIMIT 1)
WHERE
        user_id IN(
        SELECT
            from_id FROM duplicate_users);
