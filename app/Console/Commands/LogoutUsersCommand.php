<?php

namespace App\Console\Commands;

use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Throwable;

class LogoutUsersCommand extends Command
{
    protected $signature = 'users:logout';

    protected $description = 'Auto logout all users from the system every night at 2-3 am';

    protected const LOGOUT_HOUR = '02';
    protected const LOGOUT_HOUR_SECOND = '03';
    protected const LOGOUT_HOUR_FORTH = '04';

    // Task: https://eduspiresolutions.atlassian.net/browse/EHP2-1404
    public function handle()
    {
        // Get the schools with local time (02:00 AM)
        $schoolIds = School::selectRaw(
            "id,DATE_FORMAT(CONVERT_TZ(utc_timestamp(),'+00:00', schools.timezone_offset),'%H') as logout_hour"
        )
            ->having('logout_hour', self::LOGOUT_HOUR)
            ->orHaving('logout_hour', self::LOGOUT_HOUR_SECOND)
            ->orHaving('logout_hour', self::LOGOUT_HOUR_FORTH)
            ->get()
            ->pluck('id');

        $userIds = User::whereIntegerInRaw('school_id', $schoolIds)
            ->select('id')
            ->where('is_loggin', User::ACTIVE)
            ->get()
            ->pluck('id')
            ->chunk(3000);

        try {
            foreach ($userIds as $userIdChunk) {
                DB::beginTransaction();
                DB::table('oauth_access_tokens')
                    ->where('mobile', '!=', true)
                    ->whereIntegerInRaw('user_id', $userIdChunk)
                    ->delete();

                DB::table('users')
                    ->whereIntegerInRaw('id', $userIdChunk)
                    ->update(['is_loggin' => User::INACTIVE]);

                DB::commit();
            }
        } catch (Throwable $e) {
            logger($e->getMessage());
            $this->error($e->getMessage());
        } catch (\Exception $e) {
            logger($e->getMessage());
            $this->error($e->getMessage());
        }
    }
}
