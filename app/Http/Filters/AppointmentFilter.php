<?php

namespace App\Http\Filters;

use App\Models\Pass;
use App\Models\Room;
use App\Models\School;
use App\Models\StaffSchedule;
use App\Models\Transparency;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class AppointmentFilter extends QueryFilters
{
    protected $request;
    public $user;
    public $schoolTimezone;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->user = auth()->user();
        $this->schoolTimezone = School::findCacheFirst(
            $this->user->school_id
        )->getTimezone();
        $this->request = $request;

        parent::__construct($request);
    }

    /**
     * @param $dates
     * @return mixed
     */
    public function between_dates($dates)
    {
        $dates = explode(',', $dates);

        if ($dates === null || count($dates) <= 1) {
            return $this->builder;
        }

        $schoolTimezone = $this->schoolTimezone;

        $startDate = Carbon::parse(
            $dates[0] . ' 00:00:00',
            $schoolTimezone
        )->setTimezone(config('app.timezone'));
        $endDate = Carbon::parse(
            $dates[1] . ' 23:59:59',
            $schoolTimezone
        )->setTimezone(config('app.timezone'));

        return $this->builder->where(function ($query) use (
            $startDate,
            $endDate
        ) {
            return $query->whereBetween('appointment_passes.for_date', [
                $startDate,
                $endDate
            ]);
        });
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function recurrence_only($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query->where(
                    'appointment_passes.recurrence_appointment_pass_id',
                    '!=',
                    null
                );
            })
            : $this->builder;
    }

    /**
     * @param $status
     * @return mixed
     */
    public function just_my_created($status)
    {
        $user = $this->user;
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) use ($user) {
                return $query->where(
                    'appointment_passes.created_by',
                    $user->id
                );
            })
            : $this->builder;
    }

    /**
     * @param $status
     * @return mixed
     */
    public function from_me($status)
    {
        $user = $this->user;
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) use ($user) {
                $query
                    ->where('appointment_passes.from_type', User::class)
                    ->where('appointment_passes.from_id', $user->id);
            })
            : $this->builder;
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function todays_awaiting($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);
        $user = $this->user;

        $schoolTimezone = $this->schoolTimezone;

        $startDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->startOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));

        $endDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->endOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));

        $staffSchedules = StaffSchedule::where('user_id', $user->id)
            ->get()
            ->pluck('room_id');

        return $status
            ? $this->builder->where(function ($query) use (
                $user,
                $startDate,
                $endDate,
                $schoolTimezone,
                $staffSchedules
            ) {
                $query
                    ->where('appointment_passes.pass_id', null)
                    ->where('appointment_passes.canceled_at', null)
                    ->whereBetween('appointment_passes.for_date', [
                        $startDate,
                        $endDate
                    ])
                    ->where(
                        'appointment_passes.for_date',
                        '>=',
                        Carbon::parse(
                            now()->setTimezone($schoolTimezone),
                            $schoolTimezone
                        )
                            ->setTimezone(config('app.timezone'))
                            ->toDateTimeString()
                    )
                    ->where('appointment_passes.confirmed_by_teacher_at', null)
                    ->where('appointment_passes.expired_at', null)
                    ->where('appointment_passes.ran_at', null)
                    ->awaiting($user, $staffSchedules);
            })
            : $this->builder;
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function only_active($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        $user = $this->user;
        $schoolTimezone = $this->schoolTimezone;

        $startDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->startOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));
        $endDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->endOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));

        return $status
            ? $this->builder->where(function ($query) use (
                $user,
                $startDate,
                $endDate,
                $schoolTimezone
            ) {
                $query
                    ->whereBetween('appointment_passes.for_date', [
                        $startDate,
                        $endDate
                    ])
                    ->where('appointment_passes.expired_at', null)
                    ->where('appointment_passes.ran_at', null)
                    ->where(function ($query) use ($schoolTimezone) {
                        $query
                            ->where(
                                'appointment_passes.confirmed_by_teacher_at',
                                '!=',
                                null
                            )
                            ->orWhere(
                                'appointment_passes.confirmed_by_teacher_at',
                                null
                            )
                            ->where('canceled_at', '!=', null)
                            ->orWhere(function ($query) use ($schoolTimezone) {
                                $query
                                    ->where(
                                        'appointment_passes.confirmed_by_teacher_at',
                                        null
                                    )
                                    ->where(
                                        'appointment_passes.for_date',
                                        '<=',
                                        Carbon::parse(
                                            now()->setTimezone($schoolTimezone),
                                            $schoolTimezone
                                        )
                                            ->setTimezone(
                                                config('app.timezone')
                                            )
                                            ->toDateTimeString()
                                    );
                            });
                    })
                    ->when(!Transparency::isEnabled(), function ($query) use (
                        $user
                    ) {
                        $staffSchedulesFromRoomIds = StaffSchedule::where(
                            'user_id',
                            $user->id
                        )
                            ->get()
                            ->pluck('room_id');

                        $staffSchedulesToRoomIds = StaffSchedule::where(
                            'user_id',
                            $user->id
                        )
                            ->get()
                            ->pluck('room_id');

                        $query->transparency(
                            $user,
                            $staffSchedulesFromRoomIds,
                            $staffSchedulesToRoomIds
                        );
                    });
            })
            : $this->builder;
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function future_awaiting($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);
        $user = $this->user;

        $staffSchedules = StaffSchedule::where('user_id', $user->id)
            ->get()
            ->pluck('room_id');

        return $status
            ? $this->builder->where(function ($query) use (
                $user,
                $staffSchedules
            ) {
                $query
                    ->where('appointment_passes.canceled_at', null)
                    ->where('appointment_passes.expired_at', null)
                    ->where('appointment_passes.ran_at', null)
                    ->where('appointment_passes.confirmed_by_teacher_at', null)
                    ->whereHas('createdByUser', function ($query) {
                        $query->where('role_id', config('roles.student'));
                    })
                    ->awaiting($user, $staffSchedules);
            })
            : $this->builder;
    }

    /**
     * @param $status
     * @return mixed
     */
    public function to_me($status)
    {
        $user = $this->user;
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) use ($user) {
                return $query
                    ->where('appointment_passes.to_type', User::class)
                    ->where('appointment_passes.to_id', $user->id);
            })
            : $this->builder;
    }

    /**
     * @param $status
     * @return mixed
     */
    public function to_and_from_others($status)
    {
        $user = $this->user;
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) use ($user) {
                return $query
                    ->where(function ($query) use ($user) {
                        $query
                            ->where('appointment_passes.from_type', User::class)
                            ->where(
                                'appointment_passes.from_id',
                                '!=',
                                $user->id
                            )
                            ->where('appointment_passes.to_id', '!=', $user->id)
                            ->where('appointment_passes.to_type', User::class);
                    })
                    ->orWhere(function ($query) use ($user) {
                        $query
                            ->where('appointment_passes.from_type', Room::class)
                            ->where('appointment_passes.to_type', Room::class);
                    });
            })
            : $this->builder;
    }

    /**
     * @param $status
     * @return mixed
     */
    public function needs_confirmation($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query->whereHas('pass', function ($query) {
                    $query
                        ->where('pass_status', Pass::ACTIVE)
                        ->where('approved_at', null);
                });
            })
            : $this->builder;
    }

    /**
     * @param $status
     * @return mixed
     */
    public function type_activated($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query
                    ->where('ran_at', '!=', null)
                    ->where('pass_id', '!=', null)
                    ->whereHas('pass', function ($query) {
                        return $query->where(function ($query) {
                            return $query
                                ->where('passes.expired_at', null)
                                ->where('passes.completed_at', null)
                                ->where('passes.canceled_at', null)
                                ->where('passes.expired_at', null)
                                ->orWhere(function ($query) {
                                    return $query->whereHas('child', function (
                                        $query
                                    ) {
                                        $query
                                            ->where('completed_at', null)
                                            ->where(
                                                'pass_status',
                                                Pass::ACTIVE
                                            );
                                    });
                                });
                        });
                    });
            })
            : $this->builder->where(function ($query) {
                return $query
                    ->where('pass_id', null)
                    ->where('expired_at', null)
                    ->orWhere(function ($query) {
                        return $query->whereHas('pass', function ($query) {
                            return $query->where('passes.expired_at', null);
                        });
                    });
            });
    }

    /**
     * @param $filters
     * @return \Illuminate\Database\Query\Builder
     */
    public function filters_list($filters)
    {
        $filters = json_decode($filters);

        return $this->builder->where(function ($query) use ($filters) {
            $query
                ->when(in_array('type_activated', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('ran_at', '!=', null)
                            ->where('pass_id', '!=', null)
                            ->whereHas('pass', function ($query) {
                                return $query->where(function ($query) {
                                    return $query
                                        ->where('passes.expired_at', null)
                                        ->where('passes.completed_at', null)
                                        ->where('passes.canceled_at', null)
                                        ->where('passes.expired_at', null)
                                        ->orWhere(function ($query) {
                                            return $query->whereHas(
                                                'child',
                                                function ($query) {
                                                    $query
                                                        ->where(
                                                            'completed_at',
                                                            null
                                                        )
                                                        ->where(
                                                            'pass_status',
                                                            Pass::ACTIVE
                                                        );
                                                }
                                            );
                                        });
                                });
                            });
                    });
                })
                ->when(in_array('type_missed', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->whereHas('createdByUser', function ($query) {
                                $query->where(
                                    'role_id',
                                    config('roles.student')
                                );
                            })
                            ->where(
                                'appointment_passes.confirmed_by_teacher_at',
                                null
                            )
                            ->where(
                                'appointment_passes.for_date',
                                '<',
                                now()->toDateTimeString()
                            );
                    });
                })
                ->when(in_array('type_missed_apt', $filters), function (
                    $query
                ) {
                    $query->orWhere(function ($query) {
                        $query->whereHas('pass', function ($query) {
                            $query
                                ->where('pass_status', Pass::INACTIVE)
                                ->where('approved_at', null)
                                ->where('expired_at', '!=', null);
                        });
                    });
                })
                ->when(in_array('type_canceled', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where(
                                'appointment_passes.canceled_at',
                                '!=',
                                null
                            )
                            ->orWhereHas('pass', function ($query) {
                                $query->where('passes.canceled_at', '!=', null);
                            });
                    });
                })
                ->when(in_array('type_future', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where(
                                'appointment_passes.for_date',
                                '>=',
                                Carbon::tomorrow()->toDateTimeString()
                            )
                            ->where('canceled_at', null);
                    });
                })
                ->when(in_array('needs_confirmation', $filters), function (
                    $query
                ) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where(
                                'appointment_passes.confirmed_by_teacher_at',
                                null
                            )
                            ->where('appointment_passes.expired_at', null)
                            ->where('appointment_passes.pass_id', null)
                            ->where('appointment_passes.canceled_at', null);
                    });
                })
                ->when(in_array('type_ended', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query->whereHas('pass', function ($query) {
                            $query
                                ->where('pass_status', Pass::INACTIVE)
                                ->where('completed_at', '!=', null);
                        });
                    });
                })
                ->when(in_array('type_on_other_pass', $filters), function (
                    $query
                ) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('appointment_passes.pass_id', null)
                            ->where(
                                'appointment_passes.for_date',
                                '<=',
                                now()->toDateTimeString()
                            )
                            ->where('appointment_passes.canceled_at', null)
                            ->where('appointment_passes.expired_at', null);
                    });
                });
        });
    }

    /**
     * @param $status
     * @return mixed
     */
    public function type_canceled($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query
                    ->where('appointment_passes.canceled_at', '!=', null)
                    ->orWhereHas('pass', function ($query) {
                        $query->where('passes.canceled_at', '!=', null);
                    });
            })
            : $this->builder->where(function ($query) {
                return $query->where('canceled_at', null);
            });
    }

    /**
     * @param $status
     * @return mixed
     */
    public function type_missed($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query->whereHas('pass', function ($query) {
                    $query
                        ->where('pass_status', Pass::INACTIVE)
                        ->where('expired_at', '!=', null);
                });
            })
            : $this->builder->where(function ($query) {
                return $query->whereHas('pass', function ($query) {
                    $query
                        ->where('pass_status', Pass::ACTIVE)
                        ->where('expired_at', null);
                });
            });
    }

    /**
     * @param $status
     * @return mixed
     */
    public function type_future($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query
                    ->where(
                        'appointment_passes.for_date',
                        '>=',
                        Carbon::tomorrow()->toDateTimeString()
                    )
                    ->where('canceled_at', null);
            })
            : $this->builder->where(function ($query) {
                return $query->where(
                    'appointment_passes.for_date',
                    '<=',
                    Carbon::tomorrow()->toDateTimeString()
                );
            });
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function type_ended($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query->whereHas('pass', function ($query) {
                    $query
                        ->where('pass_status', Pass::INACTIVE)
                        ->where('completed_at', '!=', null)
                        ->where(
                            'system_completed',
                            '!=',
                            Pass::SYSTEM_COMPLETED_ACTIVE
                        );
                });
            })
            : $this->builder->where(function ($query) {
                return $query
                    ->whereHas('pass', function ($query) {
                        return $query
                            ->where('pass_status', Pass::ACTIVE)
                            ->where('completed_at', null)
                            ->where('expired_at', null)
                            ->where('system_completed', Pass::INACTIVE);
                    })
                    ->orWhere('ran_at', null);
            });
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function type_on_other_pass($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder
                ->select('*', DB::raw('appointment_passes.id'))
                ->join('passes', function ($join) {
                    $join
                        ->on('appointment_passes.pass_id', '!=', 'passes.id')
                        ->on(
                            'passes.user_id',
                            '=',
                            'appointment_passes.user_id'
                        )
                        ->where('passes.pass_status', Pass::ACTIVE)
                        ->where('passes.completed_at', null);
                })
            : $this->builder;
    }
}
