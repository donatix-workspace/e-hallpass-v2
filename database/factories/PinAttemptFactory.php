<?php

namespace Database\Factories;

use App\Models\PinAttempt;
use Illuminate\Database\Eloquent\Factories\Factory;

class PinAttemptFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PinAttempt::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status' => 1
        ];
    }
}
