<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendInvitationToUserMail extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $userData;

    /**
     * SendInvitationToUserMail constructor.
     * @param $userData
     * @param User $user
     */
    public function __construct($userData, User $user)
    {
        $this->user = $user;
        $this->userData = $userData;
    }

    public function build()
    {
        return $this->view('emails.send-invitation-to-user');
    }
}
