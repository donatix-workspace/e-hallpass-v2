<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexParentIdToPassesTable extends Migration
{
    public function up()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->index('parent_id');
        });
    }

    public function down()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->dropIndex('parent_id');
        });
    }
}
