<?php

namespace App\Http\Services\CommonUserService\Models;


use Illuminate\Database\Eloquent\Model;

class CommonSchool extends Model
{
   protected $connection = 'commonDb';
   protected $table = 'common_bm_school';

    public function domains()
    {
        return $this->hasMany(CommonSchoolDomain::class,'common_bm_school_id','id');
   }

}
