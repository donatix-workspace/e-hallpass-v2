<?php

namespace Database\Factories;

use App\Models\Notification;
use Illuminate\Database\Eloquent\Factories\Factory;

class NotificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Notification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 10),
            'pass_id' => $this->faker->numberBetween(1, 10),
            'message' => $this->faker->text,
            'status' => $this->faker->numberBetween(0, 1),
            'alert' => $this->faker->numberBetween(1, 20),
            'to_teacher_id' => '0', // always 0
            'from_teacher_id' => '0', // always 0
            'school_id' => $this->faker->numberBetween(1, 20),
            'to_teacher_state' => '1',
            'from_teacher_state' => '1',
            'token' => $this->faker->shuffleString('ZzzvbcT2Y1GJXxTH'),
            'bulk_status' => $this->faker->boolean,
            'sms' => $this->faker->boolean,
            'email' => $this->faker->boolean,
        ];
    }
}
