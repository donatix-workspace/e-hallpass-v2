<?php

namespace Database\Factories;

use App\Models\LocationCapacity;
use App\Models\Room;
use App\Models\School;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class LocationCapacityFactory extends Factory
{
    protected $model = LocationCapacity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status' => $this->faker->randomNumber(),
            'limit' => $this->faker->randomNumber(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'room_id' => function () {
                return Room::factory()->create()->id;
            },
            'school_id' => function () {
                return School::factory()->create()->id;
            },
        ];
    }
}
