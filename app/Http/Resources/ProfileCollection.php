<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/** @see \App\Models\User */
class ProfileCollection extends ResourceCollection
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'teacherPin' => $this->collection->get('teacherPin'),
                'rooms' => $this->collection->get('rooms'),
                'staffSchedule' => $this->collection->get('staffSchedule'),
                'roomPins' => $this->collection->get('roomPins'),
                'schoolModules' => $this->collection->get('schoolModules'),
            ],
            'status' => 'success'
        ];
    }
}
