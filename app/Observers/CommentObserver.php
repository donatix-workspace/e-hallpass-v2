<?php

namespace App\Observers;

use App\Models\Comment;
use App\Models\Pass;

class CommentObserver
{
    /**
     * @param Comment $comment
     */
    public function addCommentToPassElasticObject(Comment $comment)
    {
        if ($comment->commentable_type === Pass::class) {
            Pass::onWriteConnection()
                ->find($comment->commentable_id)
                ->searchable();
        }
    }

    /**
     * @param Comment $comment
     */
    public function created(Comment $comment)
    {
        $this->addCommentToPassElasticObject($comment);
    }

    /**
     * @param Comment $comment
     */
    public function updated(Comment $comment)
    {
        $this->addCommentToPassElasticObject($comment);
    }

    /**
     * @param Comment $comment
     */
    public function deleted(Comment $comment)
    {
        //
    }

    /**
     * @param Comment $comment
     */
    public function restored(Comment $comment)
    {
        //
    }

    /**
     * @param Comment $comment
     */
    public function forceDeleted(Comment $comment)
    {
        //
    }
}
