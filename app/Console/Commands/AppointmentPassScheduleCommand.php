<?php

namespace App\Console\Commands;

use App\Events\AdminPassCreated;
use App\Events\AppointmentPassRemind;
use App\Events\AppointmentPassRun;
use App\Events\AppointmentPassSendEmail;
use App\Events\PassCreated;
use App\Models\AppointmentPass;
use App\Models\Pass;
use App\Notifications\Push\StudentNotification;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Str;

class AppointmentPassScheduleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'passes:appointment_schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email on 20 & 5 minutes before the appointment pass for acknowledge';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $appointmentPasses = AppointmentPass::where('expired_at', null)
            ->whereBetween('for_date', [
                now()
                    ->subDay()
                    ->startOfDay()
                    ->toDateTimeString(),
                now()
                    ->addDays(7)
                    ->endOfDay()
                    ->toDateTimeString()
            ])
            ->where('confirmed_by_teacher_at', '!=', null)
            ->whereCanceledAt(null)
            ->cursor();

        foreach ($appointmentPasses as $appointmentPass) {
            $diffInMinutes = now()->diffInMinutes(
                $appointmentPass->for_date,
                false
            );
            $emailToken = md5(
                "$appointmentPass->user_id - $appointmentPass->created_at - $appointmentPass->school_id"
            );
            $emailTokenAdmin =
                Str::uuid() .
                '\\' .
                $appointmentPass->from_id .
                '\\' .
                $appointmentPass->to_id;

            Log::channel('appointments')->info(
                'Appointment email token: ' . $emailToken
            );
            Log::channel('appointments')->info(
                'Appointment with ID: ' . $appointmentPass->id . ' ',
                [
                    $appointmentPass->isForMailReminder(),
                    !$appointmentPass->secondReminder(),
                    $appointmentPass->isMailSended()
                ]
            );

            // If there's an appointment for less than a minute then run it.
            if ($diffInMinutes <= 1) {
                $this->appointmentExecute($appointmentPass);
                Log::channel('appointments')->info(
                    'Running appointment pass with less than a minute ID:' .
                        $appointmentPass->id
                );
            }

            if (
                $appointmentPass->isForMailReminder() &&
                !$appointmentPass->secondReminder()
            ) {
                DB::table('appointments_email_tokens')->insert([
                    'token' => $emailToken,
                    'admin_token' => $emailTokenAdmin,
                    'appointment_pass_id' => $appointmentPass->id,
                    'used_at' => null,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);

                $appointmentPass->update([
                    'mail_send_at' => now()
                ]);
                Log::channel('appointments')->info(
                    'Appointment Pass with Id:' .
                        $appointmentPass->id .
                        ' | Mail Send At: ' .
                        $appointmentPass->mail_send_at
                );

                event(
                    new AppointmentPassSendEmail(
                        $appointmentPass,
                        $emailToken,
                        $emailTokenAdmin
                    )
                );
            }

            if (
                $appointmentPass->firstReminder() &&
                !$appointmentPass->isAcknowledged() &&
                $appointmentPass->isConfirmedByTeacher() &&
                $diffInMinutes > 0 &&
                $appointmentPass->pass_id === null
            ) {
                $appointmentPass->update([
                    'reminded_at' => now()
                ]);

                if ($diffInMinutes >= 1) {
                    event(
                        new AppointmentPassRemind(
                            $appointmentPass,
                            Carbon::parse($appointmentPass->for_date)
                                ->addMinute()
                                ->diffInMinutes(now(), false)
                        )
                    );

                    //                    Notification::send(
                    //                        $appointmentPass,
                    //                        new StudentNotification('AppointmentPassRemind', [
                    //                            'reminder_period' =>
                    //                                AppointmentPass::FIRST_REMIND_MINUTE
                    //                        ])
                    //                    );
                } else {
                    $this->appointmentExecute($appointmentPass);
                }
            }

            if (
                $appointmentPass->secondReminder() &&
                $appointmentPass->isConfirmedByTeacher() &&
                !$appointmentPass->forNow()
            ) {
                Log::channel('appointments')->info(
                    'The appointment pass for now is runned Id: ' .
                        $appointmentPass->id .
                        ' Date:' .
                        $appointmentPass->for_date .
                        ' Pass_id: ' .
                        $appointmentPass->pass_id
                );

                if ($appointmentPass->pass_id === null) {
                    if (!$this->isInAnotherPass($appointmentPass)) {
                        $appointmentPass->createPassFromAppointment();
                        event(new AdminPassCreated($appointmentPass->pass));
                    }

                    Log::channel('appointments')->info(
                        'Creating a pass for appointment with Id: ' .
                            $appointmentPass->id
                    );
                }

                // If it's in this range we send statically 5 minutes remaining email.
                if ($diffInMinutes >= 3 && $diffInMinutes <= 6) {
                    $this->send5thMinuteRemindMail(
                        $emailToken,
                        $appointmentPass,
                        $emailTokenAdmin
                    );
                }

                if ($diffInMinutes > 1) {
                    event(
                        new AppointmentPassRemind(
                            $appointmentPass,
                            $diffInMinutes
                        )
                    );

                    //                    Notification::send(
                    //                        $appointmentPass,
                    //                        new StudentNotification('AppointmentPassRemind', [
                    //                            'reminder_period' =>
                    //                                AppointmentPass::SECOND_REMIND_MINUTE
                    //                        ])
                    //                    );
                } else {
                    $this->appointmentExecute($appointmentPass);
                }

                Log::channel('appointments')->info(
                    'Is it for now: ' . $appointmentPass->forNow()
                );
            }

            if (
                $appointmentPass->forNow() &&
                !$appointmentPass->secondReminder()
            ) {
                if (
                    $appointmentPass->pass_id !== null &&
                    $appointmentPass->pass->pass_status === Pass::ACTIVE &&
                    $appointmentPass->pass->approved_at === null
                ) {
                    Log::channel('appointments')->info(
                        'Running appointment pass for now. ID: ' .
                            $appointmentPass->id .
                            ' with newly created pass from now()'
                    );

                    $this->appointmentExecute($appointmentPass);

                    Log::channel('appointments')->info(
                        'AppointmentPassRun - ' .
                            $appointmentPass->id .
                            ' - pass id ' .
                            $appointmentPass->pass_id
                    );
                }

                $this->appointmentExecute($appointmentPass);
            }
        }

        return 0;
    }

    /**
     * @param $appointmentPass
     */
    public function appointmentExecute($appointmentPass): void
    {
        if ($appointmentPass->pass_id === null) {
            // Check if user is in another pass
            if (!$this->isInAnotherPass($appointmentPass)) {
                $appointmentPass->createPassFromAppointment();

                Log::channel('appointments')->info(
                    'Running appointment pass for now. ID: ' .
                        $appointmentPass->id .
                        ' with newly created pass from now()'
                );
                event(
                    new AppointmentPassRun(
                        $appointmentPass,
                        $appointmentPass->pass_id
                    )
                );

                event(new AdminPassCreated($appointmentPass->pass));

                //                Notification::send(
                //                    $appointmentPass,
                //                    new StudentNotification('AppointmentPassRun')
                //                );

                Log::channel('appointments')->info(
                    'AppointmentPassRun - ' .
                        $appointmentPass->id .
                        ' - pass id ' .
                        $appointmentPass->pass_id
                );
            }
        }
    }

    /**
     * @param $appointmentPass
     * @return bool
     */
    public function isInAnotherPass($appointmentPass): bool
    {
        return Pass::activeWithoutApprove()
            ->where('school_id', $appointmentPass->school_id)
            ->where('user_id', $appointmentPass->user_id)
            ->where('id', '!=', $appointmentPass->pass_id)
            ->exists();
    }

    /**
     * @param $emailToken
     * @param $appointmentPass
     * @param $emailTokenAdmin
     */
    public function send5thMinuteRemindMail(
        $emailToken,
        $appointmentPass,
        $emailTokenAdmin
    ) {
        DB::table('appointments_email_tokens')->insert([
            'token' => $emailToken,
            'appointment_pass_id' => $appointmentPass->id,
            'used_at' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        $appointmentPass->update([
            'mail_send_at' => now()
        ]);

        Log::channel('appointments')->info(
            'Appointment Pass with Id:' .
                $appointmentPass->id .
                ' | Mail Send At: ' .
                $appointmentPass->mail_send_at
        );
        event(
            new AppointmentPassSendEmail(
                $appointmentPass,
                $emailToken,
                $emailTokenAdmin
            )
        );
    }
}
