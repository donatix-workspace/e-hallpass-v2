<?php

namespace App\Notifications\Push;

use App\Http\Resources\PassResource;
use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class StudentNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $event;
    protected $options;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($event, $options = [])
    {
        $this->event = $event;
        $this->options = $options;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm'];
    }

    public function viaQueues()
    {
        return [
            'fcm' => 'notifications'
        ];
    }

    public function toFcm($notifiable)
    {
        $notificationSettings = $notifiable->getNotificationSettings(
            $this->event,
            $this->options
        );
        Log::channel('notifications')->info('NOTIFICATION START', [
            'channel' => 'fcm',
            'topic' => $notificationSettings['topics'],
            'notification' => $notificationSettings['title'],
            'data' => $notificationSettings['data']
        ]);

        //        $user = auth()->user();
        //
        //        if ($user) {
        //            $user = $user->load('mobileSettings');
        //        }
        //
        //        $userMobileSoundPreference = true;
        //
        //        // If there's sound preference, let's use it :)
        //        if ($user->mobileSettings !== null) {
        //            $userMobileSoundPreference = $user->mobileSettings->mobile_sound;
        //        }

        $message = new FcmMessage();
        $message
            ->contentAvailable(true)
            ->condition("'{$notificationSettings['topics']}' in topics")
            ->content([
                'title' => $notificationSettings['title'],
                'body' => $notificationSettings['body'],
                'sound' => array_key_exists('notifyStudent', $this->options)
                    ? $this->options['notifyStudent']
                    : false
            ])
            ->data($notificationSettings['data']);

        return $message;
    }
}
