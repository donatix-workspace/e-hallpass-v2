<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class CheckIfUserAllowsPassToHimRule implements Rule
{
    public $toType;

    /**
     * @param string|null $toType
     */
    public function __construct(?string $toType)
    {
        $this->toType = $toType;
    }

    public function passes($attribute, $value): bool
    {
        if ($this->toType === User::class) {
            $user = User::select('allow_passes_to_me')
                ->where('school_id', optional(auth()->user())->school_id)
                ->where('id', $value)
                ->first();

            if ($user) {
                return $user->allow_passes_to_me;
            }
        }

        return true;
    }

    public function message(): string
    {
        return __('You can not create a pass to that adult.');
    }
}
