<?php

namespace App\Listeners;

use App\Events\PassBlockRunnedEvent;

class PassBlockRunnedEventListener
{
    /**
     * @param PassBlockRunnedEvent $event
     */
    public function handle(PassBlockRunnedEvent $event)
    {
        $event->passBlock->update([
            'notified_at' => now()
        ]);
    }
}
