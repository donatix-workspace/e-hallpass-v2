<?php

namespace Tests\TestFactories;

use App\Models\Modules;
use App\Modules\Periods\Models\Periods;
use Illuminate\Support\Facades\DB;

/**
 * Class SchoolFactory
 * Since the school has some modules in `allmodules` table
 * we need to setup custom school factory because there's no any
 * model relationship between modules and schools.
 *
 * Last edit: Lubomir Stankov.
 */
class SchoolFactory implements TestFactoriesInterface
{
    public $school;

    public function __construct($school)
    {
        $this->initModules();
        $this->school = $school;
    }

    // The default method
    public static function createOne(array $attributes = [])
    {
        $school = factory(\App\Modules\Organizer\Models\Schoolperiods::class)->create($attributes);

        return new SchoolFactory($school);
    }

    /**
     * Init the school modules if there's no any.
     * @return void
     */
    private function initModules()
    {
        $modules = DB::table('allmodules')->get();

        if ($modules->isEmpty()) {
            DB::table('allmodules')->insert([
                [
                    'id' => 1,
                    'name' => 'Appointmentpass',
                    'display_name' => 'Appointment Pass',
                    'option_json' => '{"peer":"Peer Email Notification ","sms":"SMS Notification","email":"Email Alert Notification"}',
                    'status' => 1,
                    'description' => 'Please select the Appointment Pass notification method(s) you would like to activate for your school.'
                ],
                [
                    'id' => 2,
                    'name' => 'Auto Check-In',
                    'display_name' => 'Auto Check-In',
                    'option_json' => null,
                    'status' => 1,
                    'description' => 'Auto Check-In enables assigning a public PIN to a room so that students can Check into room using that public PIN',
                ],
                [
                    'id' => 3,
                    'name' => 'Kiosk',
                    'display_name' => 'Kiosk (EHP Self Service) ',
                    'option_json' => '{"kurl":"Enable URL Klogin method only, highly recommended for use with Chromebooks","usbcard":"Card  As Default Login","barcode":"Barcode As Default Login" ,"spassword":"Hide Student Kiosk Password Field"}',
                    'status' => 1,
                    'description' => 'Kiosk (EHP Self Service)'
                ]
            ]);
        }
    }

    // Create school along with module.
    public function withModule($moduleName)
    {
        $modules = [
            "Kiosk" => 3,
            "Auto-Check-In" => 2,
            "Appointmentpass" => 1
        ];

        $a = factory(Modules::class)->create([
            'school_id' => $this->school->school_id,
            'module_id' => $modules[$moduleName]
        ]);

        return $this;
    }

    // Create a period
    public function withPeriod()
    {
        factory(Periods::class)->create([
            'school_id' => $this->school->school_id,
        ]);


        return $this;
    }

}
