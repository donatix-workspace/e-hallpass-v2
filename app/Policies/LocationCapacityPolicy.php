<?php

namespace App\Policies;

use App\Models\LocationCapacity;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LocationCapacityPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view any location capacities.
     *
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the location capacity.
     *
     * @param User $user
     * @param LocationCapacity $locationCapacity
     * @return bool
     */
    public function view(User $user, LocationCapacity $locationCapacity)
    {
        return $user->isAdmin() &&
            $user->school_id === $locationCapacity->school_id;
    }

    /**
     * Determine whether the user can create location capacities.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isAdmin() || $user->isStaff();
    }

    /**
     * Determine whether the user can update the location capacity.
     *
     * @param User $user
     * @param LocationCapacity $locationCapacity
     * @return bool
     */
    public function update(User $user, LocationCapacity $locationCapacity)
    {
        return !$user->isStudent() &&
            $user->school_id === $locationCapacity->school_id;
    }

    /**
     * Determine whether the user can delete the location capacity.
     *
     * @param User $user
     * @param LocationCapacity $locationCapacity
     * @return bool
     */
    public function delete(User $user, LocationCapacity $locationCapacity)
    {
        return $user->isAdmin() &&
            $user->school_id === $locationCapacity->school_id;
    }

    /**
     * Determine whether the user can restore the location capacity.
     *
     * @param User $user
     * @param LocationCapacity $locationCapacity
     * @return bool
     */
    public function restore(User $user, LocationCapacity $locationCapacity)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the location capacity.
     *
     * @param User $user
     * @param LocationCapacity $locationCapacity
     * @return bool
     */
    public function forceDelete(User $user, LocationCapacity $locationCapacity)
    {
        //
    }
}
