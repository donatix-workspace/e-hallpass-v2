<?php

namespace App\Http\Requests;

use App\Models\Pass;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SummaryFilterRequest extends FormRequest
{
    public function rules()
    {
        $user = auth()->user();

        return [
            'aggregate_by' => ['string', 'required', Rule::in(['highest_pass_takers', 'highest_pass_granters', 'highest_pass_destination'])],
            'teacher_ids' => 'array',
            'teacher_ids.*' => ['required', 'integer', Rule::exists('users', 'id')->where('school_id', optional($user->school)->id)],
            'student_ids' => 'array',
            'student_ids.*' => ['required', 'integer', Rule::exists('users', 'id')->where('school_id', optional($user->school)->id)],
            'room_ids' => 'array',
            'room_ids.*' => ['required', 'integer', Rule::exists('rooms', 'id')->where('school_id', optional($user->school)->id)],
            'by_pass_types' => ['required', 'string', Rule::in(['ALL', Pass::PROXY_PASS, Pass::APPOINTMENT_PASS, Pass::STUDENT_CREATED_PASS, Pass::KIOSK_PASS])],
            'by_grade_years' => 'array',
            'from_date' => 'required|date',
            'to_date' => 'required|date'
        ];
    }

    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}
