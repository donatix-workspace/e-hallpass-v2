<?php

namespace App\Rules;

use App\Models\Domain;
use Illuminate\Contracts\Validation\Rule;

class DomainIsValid implements Rule
{
    protected $schoolId;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($schoolId)
    {
        $this->schoolId = $schoolId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $domain = optional(explode('@', $value))[1];
        return Domain::where('school_id', $this->schoolId)->where('name', $domain)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('domain.not.supported');
    }
}
