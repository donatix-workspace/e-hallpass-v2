<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyEnumToPassesTable extends Migration
{
    public function up()
    {
        $passKsk = \App\Models\Pass::KIOSK_PASS;
        $passProxy = \App\Models\Pass::PROXY_PASS;
        $passStudent = \App\Models\Pass::STUDENT_CREATED_PASS;
        $passApt = \App\Models\Pass::APPOINTMENT_PASS;

        DB::statement("ALTER TABLE passes MODIFY type ENUM('$passKsk', '$passProxy', '$passStudent', '$passApt')");
    }

    public function down()
    {
        $passKsk = \App\Models\Pass::KIOSK_PASS;
        $passProxy = \App\Models\Pass::PROXY_PASS;
        $passStudent = \App\Models\Pass::STUDENT_CREATED_PASS;
        $passApt = \App\Models\Pass::APPOINTMENT_PASS;

        DB::statement("ALTER TABLE passes MODIFY type ENUM('$passKsk', '$passProxy', '$passStudent', '$passApt')");
    }
}
