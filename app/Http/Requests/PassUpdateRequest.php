<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PassUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->isStudent();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'teacher_pin' => 'string|min:4|required_unless:action,6,7,8',
            'approval_message' => 'max:255',
            'action' => 'min:1|max:8|numeric|required',
            'return_type' => 'string|max:10|in:return,inout'
        ];
    }
}
