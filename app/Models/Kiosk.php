<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kiosk extends Model
{
    use HasFactory;

    public const ACTIVE = 1;
    public const INACTIVE = 0;
    public const QRCODE_TYPE = 'qrCode';
    public const BARCODE_TYPE = 'barcode';
    public const STANDARD_TYPE = 'standart';

    protected $with = ['room'];

    protected $guarded = ['id'];
    protected $dates = [
        'expired_date',
        'password_use'
    ];

    /**
     * @param DateTimeInterface $date
     * @return string
     */
    public function serializeDate(DateTimeInterface $date): string
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($date, config('app.timezone'))->setTimezone(optional($school)->getTimezone())->format('Y-m-d H:i:s');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @param $query
     * @param $kioskType
     * @param $kioskTypeId
     * @param $user
     * @return mixed
     */
    public function scopeTypeExists($query, $kioskType, $kioskTypeId, $user)
    {
        return $query->when($kioskType === User::class, function ($query) use ($kioskTypeId) {
            $query->where('user_id', $kioskTypeId)
                ->where('room_id', null);
        })->when($kioskType === Room::class, function ($query) use ($kioskTypeId, $user) {
            $query->where('user_id', $user->id)
                ->where('room_id', $kioskTypeId);
        });
    }
}
