<?php

namespace App\Listeners;

use App\Events\PassCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PassCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PassCreated $event
     * @return void
     */
    public function handle(PassCreated $event)
    {
        //
    }
}
