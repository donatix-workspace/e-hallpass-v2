<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Translators\SchoolTranslator;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class MergeUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:merge {--mergeDuplicated} {--fixParams} {--deleteDuplicates} {--createMemberships} {--mapExistingMemberships} {--verify}';

    protected $admin;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Merge Users from V1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->option('mergeDuplicated')) {
            $this->info('Get duplicate emails, Define toUser');
            $this->mergeDuplicated();
        }
        if ($this->option('fixParams')) {
            $this->info('Add missing params for toUser');
            $this->fixParams();
        }
        if ($this->option('deleteDuplicates')) {
            $this->info('set cus to null and delete fromUsers');
            $this->deleteDuplicates();
        }
        if ($this->option('createMemberships')) {
            $this->info('Loop throuhg from_id and create memberships');
            $this->createMemberships();
        }
        if ($this->option('mapExistingMemberships')) {
            $this->info('Loop throuhg from_id and create memberships');
            $this->mapExistingMemberships();
        }
        if ($this->option('verify')) {
            $this->info('Mark duplicates');
            $this->verify();
        }

        //        $this->updateNotDuplicated(); //Dont do that

        //update auth_type wher Simulate user and system
        //        select * from users where id in (select DISTINCT to_id from duplicate_users)
        //and cus_uuid is null
        //    and deleted_at is null
        //    and username != "Simulate user"
        //    and username != "system"

        //get membership uuid
        //simulate users
    }

    protected function verify()
    {
        $duplicateEmails = DB::connection('oldDB')
            ->table('users')
            ->select('email')
            ->where('status', 1)
            ->groupBy('email')
            ->havingRaw("count(email) > 1");

        $this->output->progressStart($duplicateEmails->count());

        foreach ($duplicateEmails->get() as $duplicateEmail) {

            $duplicateUsers = User::withoutGlobalScopes()
                ->where('email', $duplicateEmail->email)
                ->get();

            $existingDuplicate = DB::table('duplicate_users_clone')
                ->whereIntegerInRaw('from_id', $duplicateUsers->pluck('id'))
                ->first();

            if (!$existingDuplicate) {
                $toUser = $duplicateUsers->first();
            } else {
                $toUser = User::withoutGlobalScopes()
                    ->where('id', $existingDuplicate->to_id)
                    ->first();
            }
            $insertData = [];

            foreach ($duplicateUsers as $duplicateUser) {
                $insertData[] = [
                    'from_id' => $duplicateUser->id,
                    'to_id' => $toUser->id,
                    'old_from_id' => $duplicateUser->old_id,
                ];
            }

            DB::table('duplicate_users_verify')->insert($insertData);


            $this->output->progressAdvance();
        }

    }

    protected function updateNotDuplicated()
    {
        $dbQuery = DB::table('duplocates')
            ->where('count', 1)
            ->whereNotNull('email')
            ->where('email', '!=', '')
            ->orderBy('id');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk('1000', function ($duplicateMails) {
            foreach ($duplicateMails as $duplicateMail) {
                $toUser = DB::table('users')
                    ->select('id', 'email', 'old_id')
                    ->where('email', $duplicateMail->email)
                    ->first();

                DB::table('duplicate_users')->updateOrInsert(
                    [
                        'from_id' => $toUser->id,
                        'to_id' => $toUser->id
                    ],
                    [
                        'old_from_id' => $toUser->old_id
                    ]
                );

                $this->output->progressAdvance();
            }
        });
    }

    protected function mergeDuplicated()
    {
        $dbQuery = DB::table('duplocates')
            ->where('count', '>=', 2)
            ->whereNotNull('email')
            ->where('email', '!=', '')
            ->orderBy('id');
        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($duplicateMails) {
            foreach ($duplicateMails as $duplicateMail) {
                $toUser = DB::table('users')
                    ->select('id', 'old_id', 'email')
                    ->where('email', $duplicateMail->email)
                    ->whereNull('deleted_at')
                    ->first();

                if (!$toUser) {
                    $toUser = DB::table('users')
                        ->select('id', 'old_id', 'email')
                        ->where('email', $duplicateMail->email)
                        ->first();
                }

                $swapToUser = DB::table('duplicate_users_clone')
                    ->where('from_id', $toUser->id)
                    ->where('to_id', '!=', $toUser->id)
                    ->first();

                if ($swapToUser) {
                    $toUser = DB::table('users')
                        ->select('id', 'old_id', 'email')
                        ->where('id', $swapToUser->to_id)
                        ->first();
                }

                DB::table('duplicate_users_clone')->updateOrInsert(
                    [
                        'from_id' => $toUser->id,
                        'to_id' => $toUser->id
                    ],
                    [
                        'old_from_id' => $toUser->old_id
                    ]
                );

                $duplicateUsers = DB::table('users')
                    ->select('id', 'old_id')
                    ->where('email', $toUser->email)
                    ->where('id', '!=', $toUser->id)
                    ->get();

                foreach ($duplicateUsers as $duplicate) {
                    DB::table('duplicate_users_clone')->updateOrInsert(
                        [
                            'from_id' => $duplicate->id,
                            'to_id' => $toUser->id
                        ],
                        [
                            'old_from_id' => $duplicate->old_id
                        ]
                    );
                }

                $this->output->progressAdvance();
            }
        });
    }

    protected function fixParams()
    {
        $dbQuery = DB::table('duplicate_users')
            ->distinct()
            ->select('to_id')
            ->orderBy('to_id');

        $this->output->progressStart($dbQuery->count());
        $dbQuery->chunk(100, function ($toDuplicates) {
            foreach ($toDuplicates as $toDuplicate) {
                $toUser = User::where('id', $toDuplicate->to_id)
                    ->withoutGlobalScopes()
                    ->first();

                $duplicates = DB::table('duplicate_users')
                    ->select('from_id')
                    ->where('to_id', $toUser->id)
                    ->where('from_id', '!=', $toUser->id)
                    ->get()
                    ->pluck('from_id');

                if ($toUser->cus_uuid == null) {
                    $duplicateUserWithCus = User::whereIn('id', $duplicates)
                        ->whereNotNull('cus_uuid')
                        ->withoutGlobalScopes()
                        ->first();
                    if ($duplicateUserWithCus) {
                        $toUser->update([
                            'cus_uuid' => $duplicateUserWithCus->cus_uuid
                        ]);
                    }
                }
                if ($toUser->student_sis_id == null) {
                    $duplicateUserWithSis = User::whereIn('id', $duplicates)
                        ->whereNotNull('student_sis_id')
                        ->withoutGlobalScopes()
                        ->first();
                    if ($duplicateUserWithSis) {
                        $toUser->update([
                            'student_sis_id' =>
                                $duplicateUserWithSis->student_sis_id
                        ]);
                    }
                }
                if ($toUser->username == null) {
                    $duplicateUserWithUsername = User::whereIn(
                        'id',
                        $duplicates
                    )
                        ->whereNotNull('username')
                        ->withoutGlobalScopes()
                        ->first();
                    if ($duplicateUserWithUsername) {
                        $toUser->update([
                            'username' => $duplicateUserWithUsername->username
                        ]);
                    }
                }
                if ($toUser->clever_id == null) {
                    $duplicateUserWithCleverId = User::whereIn(
                        'id',
                        $duplicates
                    )
                        ->whereNotNull('clever_id')
                        ->withoutGlobalScopes()
                        ->first();
                    if ($duplicateUserWithCleverId) {
                        $toUser->update([
                            'clever_id' => $duplicateUserWithCleverId->clever_id
                        ]);
                    }
                }
                if ($toUser->clever_building_id == null) {
                    $duplicateUserWithCleverBuildingId = User::whereIn(
                        'id',
                        $duplicates
                    )
                        ->whereNotNull('clever_building_id')
                        ->withoutGlobalScopes()
                        ->first();
                    if ($duplicateUserWithCleverBuildingId) {
                        $toUser->update([
                            'clever_building_id' =>
                                $duplicateUserWithCleverBuildingId->clever_building_id
                        ]);
                    }
                }
                if ($toUser->deleted_at != null) {
                    $duplicateNotDeleted = User::whereIn('id', $duplicates)
                        ->whereNull('deleted_at')
                        ->withoutGlobalScopes()
                        ->first();
                    if ($duplicateNotDeleted) {
                        $toUser->update([
                            'deleted_at' => null
                        ]);
                    }
                }

                User::whereIn('id', $duplicates)->update([
                    'cus_uuid' => null,
                    'deleted_at' => now()
                ]);

                $this->output->progressAdvance();
            }
        });
    }

    protected function deleteDuplicates()
    {
        $toIds = DB::table('duplicate_users')
            ->distinct()
            ->select('to_id')
            ->orderBy('to_id')
            ->get()
            ->pluck('to_id');
        $fromIds = DB::table('duplicate_users')
            ->distinct()
            ->select('from_id')
            ->orderBy('from_id')
            ->get()
            ->pluck('from_id');

        $deleteQuery = User::withoutGlobalScopes()
            ->whereIntegerInRaw('id', $fromIds)
            ->whereIntegerNotInRaw('id', $toIds)
            ->update([
                'cus_uuid' => null,
                'deleted_at' => now()
            ]);
        dd($deleteQuery);
    }

    protected function createMemberships()
    {
        //        $this->info('Create Duplicated Memberships');
        //        $this->createDuplicatedMemberships();
        $this->info('Create Not Duplicated Memberships');
        $this->createNotDuplicatedMemberships();
    }

    protected function createNotDuplicatedMemberships()
    {
        $roles = [
            0 => 3, //substitute
            -2 => null, //none => null
            1 => 5, //superadmin => superadmin
            2 => 2, //admin => admin
            3 => 3, //teacher => teacher
            4 => 1, //student => student
            5 => 4, //staff => staff
            6 => 2, //schooladmin => admin
            7 => null, //system => null
            8 => null, //norole => null
            10 => null
        ];

        $dbQuery = DB::table('duplocates')
            ->where('count', 1)
            ->whereNotNull('email')
            ->where('email', '!=', '')
            ->orderBy('id');
        $this->output->progressStart($dbQuery->count());
        $dbQuery->chunk('100', function ($duplicateMails) use ($roles) {
            foreach ($duplicateMails as $duplicateMail) {
                $toUser = User::withoutGlobalScopes()
                    ->select('id', 'email', 'old_id', 'school_id', 'role_id')
                    ->where('email', $duplicateMail->email)
                    ->first();

                if ($toUser) {
                    $roleId = $toUser->role_id;

                    $oldUser = \App\Models\V1\User::select('role_id')
                        ->where('user_id', $toUser->old_id)
                        ->first();

                    if ($oldUser) {
                        $roleId = $roles[$oldUser->role_id];

                        $updateData = [
                            'role_id' => $roleId,
                            'is_substitute' => $oldUser->role_id == 0
                        ];

                        if (!$roleId) {
                            $updateData['role_id'] = 1;
                            $updateData['deleted_at'] = now();
                        }
                        $toUser->update($updateData);
                    }

                    DB::table('schools_users')->updateOrInsert(
                        [
                            'user_id' => $toUser->id,
                            'school_id' => $toUser->school_id
                        ],
                        [
                            'role_id' => $roleId
                        ]
                    );
                }

                $this->output->progressAdvance();
            }
        });
    }

    protected function createDuplicatedMemberships()
    {
        $roles = [
            0 => 3, //substitute
            -2 => null, //none => null
            1 => 5, //superadmin => superadmin
            2 => 2, //admin => admin
            3 => 3, //teacher => teacher
            4 => 1, //student => student
            5 => 4, //staff => staff
            6 => 2, //schooladmin => admin
            7 => null, //system => null
            8 => null, //norole => null
            10 => null
        ];

        $dbQuery = DB::table('duplicate_users')
            ->select('from_id', 'to_id')
            ->orderBy('to_id');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk('100', function ($duplicateIds) use ($roles) {
            foreach ($duplicateIds as $duplicateId) {
                $fromUser = User::withoutGlobalScopes()
                    ->select('id', 'email', 'old_id', 'school_id', 'role_id')
                    ->where('id', $duplicateId->from_id)
                    ->first();

                $roleId = $fromUser->role_id;

                $oldUser = \App\Models\V1\User::select('role_id')
                    ->where('user_id', $fromUser->old_id)
                    ->first();

                if ($oldUser) {
                    $roleId = $roles[$oldUser->role_id];

                    $updateData = [
                        'role_id' => $roleId,
                        'is_substitute' => $oldUser->role_id == 0
                    ];
                    if (!$roleId) {
                        $updateData['role_id'] = 1;
                        $updateData['deleted_at'] = now();
                    }
                    $fromUser->update($updateData);
                }

                DB::table('schools_users')->updateOrInsert(
                    [
                        'user_id' => $duplicateId->to_id,
                        'school_id' => $fromUser->school_id
                    ],
                    [
                        'role_id' => $roleId
                    ]
                );

                $this->output->progressAdvance();
            }
        });
    }

    protected function mapExistingMemberships()
    {
        $dbQuery = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', 'u.id')
            ->join('schools as s', 'm.school_id', 's.id')
            ->select(
                'u.id as user_id',
                'u.common_user_id',
                'u.cus_uuid as user_uuid',
                's.id as school_id',
                's.cus_uuid as school_uuid',
                's.common_id as school_common_id'
            )
            ->whereNull('m.membership_uuid')
            ->whereNotNull('u.common_user_id')
            ->orderBy('u.id');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($memberships) {
            foreach ($memberships as $membership) {
                $commonMembership = CommonMembership::where(
                    'auth_user_id',
                    $membership->common_user_id
                )
                    ->where(
                        'common_bm_school_id',
                        $membership->school_common_id
                    )
                    ->where('auth_client_id', 2)
                    ->where(function ($query) {
                        $query
                            ->whereDate('archived_at', '>', '2021-09-20')
                            ->orWhereNull('archived_at');
                    })
                    ->first();

                if ($commonMembership) {
                    DB::table('schools_users')->updateOrInsert(
                        [
                            'user_id' => $membership->user_id,
                            'school_id' => $membership->school_id
                        ],
                        [
                            'membership_uuid' => $commonMembership->uuid
                        ]
                    );
                }
                $this->output->progressAdvance();
            }
        });
    }
}
