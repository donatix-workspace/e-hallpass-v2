<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContactTracingSearchRequest extends FormRequest
{
    public function rules()
    {
        $user = auth()->user();

        return [
            'user_id' => ['required', 'integer', Rule::exists('users', 'id')
                ->where(function ($query) use ($user) {
                    return $query->where('school_id', optional($user)->school_id)
                        ->where('role_id', '=', config('roles.student'));
                })
            ],
            'from_date' => ['required', 'date'],
            'to_date' => ['required', 'date', 'after_or_equal:from_date'],
            'per_page' => 'sometimes|integer|max:130'
        ];
    }

    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}
