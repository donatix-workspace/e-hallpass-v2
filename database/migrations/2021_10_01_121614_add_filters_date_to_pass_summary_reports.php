<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFiltersDateToPassSummaryReports extends Migration
{
    public function up()
    {
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->json('filters_dates')->after('total_time_all');
        });
    }

    public function down()
    {
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->dropColumn('filters_dates');
        });
    }
}
