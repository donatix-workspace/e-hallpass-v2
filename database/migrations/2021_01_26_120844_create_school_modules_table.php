<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_modules', function (Blueprint $table) {
            $table->bigInteger('school_id')->unsigned();
            $table->bigInteger('module_id')->unsigned();
            $table->text('option_json')->nullable();
            $table->boolean('permission')->nullable();
            $table->boolean('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('school_modules', function (Blueprint $table) {
            //
        });
    }
}
