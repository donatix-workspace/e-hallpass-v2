<?php

namespace App\Http\Resources;

use App\Models\Transparency;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function with($request)
    {
        return [
            //            'schools' => auth()->check() ? auth()->user()->schools : null,
            'mobile_settings' => auth()->check()
                ? new UserMobileSettingsResource(auth()->user()->mobileSettings)
                : null,
            'transparency_status' =>
                auth()->check() &&
                !auth()
                    ->user()
                    ->isStudent() &&
                !Transparency::isEnabled(),
            'status' => __('success')
        ];
    }
}
