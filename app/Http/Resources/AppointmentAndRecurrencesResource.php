<?php

namespace App\Http\Resources;

use App\Models\AppointmentPass;
use App\Models\School;
use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentAndRecurrencesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (optional($this)->id === null) {
            return [];
        }

        return [
            'id' => $this->id,
            'recurrence_appointment_pass_id' => optional($this)->is_recurrence ? $this->id : null,
            'created_by' => $this->created_by,
            'created_by_user' => [
                'name' => $this->created_by_user->name,
                'first_name' => $this->created_by_user->first_name,
                'last_name' => $this->created_by_user->last_name,
                'id' => $this->created_by_user->id
            ],
            'from_type' => $this->from_type,
            'from_id' => $this->from_id,
            'can_be_canceled' => $this->canceled_at === null,
            'to_type' => $this->to_type,
            'to_id' => $this->to_id,
            'from' => !$this->from_id ? ['first_name' => 'System', 'last_name' => '', 'id' => 0] : $this->from,
            'to' => $this->to,
            'period' => $this->period,
            'for_date' => $this->for_date,
            'recurrence_type' => optional($this)->recurrence_type,
            'recurrence_days' => optional($this)->recurrence_days,
            'is_recurrence' => $this->is_recurrence,
            'is_for_future' => true,
            'is_for_confirm' => !$this->is_recurrence ? AppointmentPass::canGivenAppointmentBeConfirmed($this->id) : false,
            'is_for_acknowledge' => !$this->is_recurrence ? AppointmentPass::canGivenAppointmentBeAcknowledged($this->id) : false,
            'acknowledged_by_teacher_at' => !$this->is_recurrence ? $this->acknowledged_by_teacher_at : null,
            'acknowledged_by' => !$this->is_recurrence ? $this->acknowledged_by : null,
            'confirmed_by' => !$this->is_recurrence ? $this->confirmed_by : null,
            'confirmed_by_user' => !$this->is_recurrence ? $this->confirmed_by_user : null,
            'acknowledged_by_user' => !$this->is_recurrence ? $this->acknowledged_by_user : null,
            'can_be_edited' => $this->canceled_at === null,
            'recurrence_week' => optional($this)->recurrence_week,
            'recurrence_end_at' => optional($this)->recurrence_end_at,
            'recurrence_appointment_pass' => optional($this)->recurrence_type !== null && $this->is_recurrence ? [
                'recurrence_type' => optional($this)->recurrence_type,
                'recurrence_days' => optional($this)->recurrence_days,
                'recurrence_week' => optional($this)->recurrence_week,
                'recurrence_end_at' => optional($this)->recurrence_end_at,
            ] : null,
            'reason' => $this->reason,
            'expired_at' => $this->expired_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
            'user' => $this->user,
            'is_canceled' => $this->canceled_at !== null,
            'comments' => !$this->is_recurrence ? $this->comments : null,
            'period_id' => $this->period_id,
            'school_id' => $this->school_id,
        ];
    }
}
