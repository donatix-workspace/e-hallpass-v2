<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSchoolsUsersAddMembershipUuid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools_users', function (Blueprint $table) {
            $table->string('membership_uuid')->nullable();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('membership_uuid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schools_users', function (Blueprint $table) {
            $table->dropColumn('membership_uuid');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string('membership_uuid')->nullable();
        });
    }
}
