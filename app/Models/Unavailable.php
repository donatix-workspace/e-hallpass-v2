<?php

namespace App\Models;

use App\Elastic\UnavailablesIndexConfigurator;
use App\Http\Filters\Filterable;
use App\Traits\AdvancedRecurrence;
use App\Traits\ExtendedSearchable;
use App\Traits\PerPage;
use App\Traits\UnavailableMapping;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;
use ScoutElastic\Searchable;

class Unavailable extends Model
{
    use HasFactory;
    use AdvancedRecurrence,
        PerPage,
        ExtendedSearchable,
        UnavailableMapping,
        Filterable;

    public const EXPIRED_STRING = 'expired';
    public const IN_TIME = 'in_time';

    protected $guarded = [];

    protected $with = ['unavailable'];

    protected $appends = ['end_hour', 'start_hour'];

    protected $indexConfigurator = UnavailablesIndexConfigurator::class;

    protected $dates = ['to_date', 'from_date', 'recurrence_end_at'];

    public const ACTIVE = 1;
    public const INACTIVE = 0;

    /**
     * @param DateTimeInterface $date
     * @return string
     */
    public function serializeDate(DateTimeInterface $date)
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($date, config('app.timezone'))
            ->setTimezone(optional($school)->getTimezone())
            ->format('Y-m-d H:i:s');
    }

    public function toSearchableArray()
    {
        $unavailable = $this->toArray();

        $unavailable['start_date_string'] = Carbon::parse(
            $this->from_date
        )->format('m/d/Y');

        return $unavailable;
    }

    /**
     * @param $value
     * @return string|null
     */
    public function getRecurrenceEndAtAttribute($value): ?string
    {
        if ($value === null) {
            return null;
        }
        return Carbon::parse($value)->format('m/d/Y');
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return string
     */
    public function getEndHourAttribute(): string
    {
        $school = optional(School::findCacheFirst($this->school_id));

        return $this->to_date
            ->setTimezone($school->getTimezone())
            ->format('g:i A');
    }

    /**
     * @return string
     */
    public function getStartHourAttribute(): string
    {
        $school = optional(School::findCacheFirst($this->school_id));

        return $this->from_date
            ->setTimezone($school->getTimezone())
            ->format('g:i A');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }

    /**
     * @param null $filters
     * @param int $perPage
     * @param bool|null $pagination
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Query\Builder|Unavailable
     */
    public static function searchableQueries(
        $filters = null,
        int $perPage = 25,
        ?bool $pagination = true
    ) {
        $user = auth()->user();

        $searchQuery =
            request()->get('search_query') === null
                ? config('scout_elastic.all_records')
                : convertElasticQuery(request()->get('search_query'));

        $unavailablesSearchIds = collect(
            self::search($searchQuery)
                ->where('school_id', $user->school_id)
                ->when($user->isTeacher() || $user->isStaff(), function (
                    $query
                ) use ($user) {
                    $query
                        ->where('unavailable_type', User::class)
                        ->where('unavailable_id', $user->id);
                })
                ->when(request()->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('id', 'asc');
                    }
                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                    return $query->orderBy('id', 'asc');
                })
                ->take(self::recordsPerPage())
                ->get()
        )
            ->pluck('id')
            ->toArray();

        $unavailablesSearchIdsImploded = implode(',', $unavailablesSearchIds);

        $query = self::whereIntegerInRaw(
            'id',
            $unavailablesSearchIds
        )->orderByRaw("field(id, $unavailablesSearchIdsImploded)"); // We made that because we want exact same sorting that is returned from elastic search.

        return $pagination ? $query->paginate(self::recordsPerPage()) : $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function unavailable()
    {
        return $this->morphTo();
    }

    public function setToDateAttribute($value)
    {
        $this->attributes['to_date'] = Carbon::parse($value)->setTimezone(
            config('app.timezone')
        );
    }

    public function setFromDateAttribute($value)
    {
        $this->attributes['from_date'] = Carbon::parse($value)->setTimezone(
            config('app.timezone')
        );
    }

    public function setRecurrenceEndAtAttribute($value)
    {
        $this->attributes['recurrence_end_at'] = Carbon::parse(
            $value
        )->setTimezone(config('app.timezone'));
    }
}
