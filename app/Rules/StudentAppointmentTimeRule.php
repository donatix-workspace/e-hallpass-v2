<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class StudentAppointmentTimeRule implements Rule
{
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $date = Carbon::parse($value)
            ->setTimezone(config('app.timezone'))
            ->greaterThanOrEqualTo(now()->addMinutes(30));

        if ($date) {
            return true;
        }

        //Check if the student given date is not greater than a month.
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('appointments.invalid.time');
    }
}
