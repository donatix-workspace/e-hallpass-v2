<?php

namespace Database\Factories;

use App\Models\AutoPass;
use Illuminate\Database\Eloquent\Factories\Factory;

class AutoPassFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AutoPass::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'room_id' => $this->faker->numberBetween(1,10),
            'school_id' => $this->faker->numberBetween(1,10),
        ];
    }
}
