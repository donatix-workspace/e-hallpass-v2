<?php

namespace App\Rules;

use App\Models\TeacherPassSetting;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class ExtendedPassTimeRule implements Rule
{
    public $passSetting;

    public function __construct()
    {
        $this->passSetting = TeacherPassSetting::where('school_id', auth()->user()->school_id)
            ->first();
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $teacherPassSetting = $this->passSetting;

        if ((request()->input('is_extended') && request()->input('extended_pass_time') === null) || TeacherPassSetting::convertInMinutes($value) < TeacherPassSetting::convertInMinutes($teacherPassSetting->auto_expire_time)) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        $teacherPassSetting = $this->passSetting;
        $value = request()->input('extended_pass_time');

        if (request()->input('extended_pass_time') === null) {
            return 'The extended pass time is empty.';
        }

        if (TeacherPassSetting::convertInMinutes($value) < TeacherPassSetting::convertInMinutes($teacherPassSetting->auto_expire_time)) {
            return 'The extended pass time must be a date after system end.';
        }

        return 'You can\'t provide time less than minute';
    }
}
