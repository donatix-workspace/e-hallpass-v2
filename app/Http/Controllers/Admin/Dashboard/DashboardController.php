<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Entities\CountersEntities;
use App\Entities\DashboardEntities;
use App\Http\Controllers\Controller;
use App\Http\Filters\DashboardFilter;
use App\Http\Resources\PassCollection;
use App\Models\Pass;
use App\Models\Room;
use App\Models\School;
use App\Models\StaffSchedule;
use App\Models\Transparency;
use App\Models\User;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\JsonResponse;

/**
 * @group Dashboard
 */
class DashboardController extends Controller
{
    /**
     * Get dashboard entities
     *
     *
     * @authenticated
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        if (!Gate::allows('access-dashboard-actions')) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('invalid.permission'),
                    'status' => __('fail')
                ],
                403
            );
        }

        // Move it to separate class for more clean refactoring after that
        $dashboardEntities = DashboardEntities::get();

        return response()->json([
            'data' => $dashboardEntities,
            'status' => __('success')
        ]);
    }

    /**
     * Dashboard Filter
     *
     * @queryParam ended_passes boolean Get all of the passes that are completed successfully.
     * @queryParam only_my_passes boolean Get only passes which are approved/completed by authenticated user
     * @queryParam only_my_active boolean Get only active passes that are approved by authenticated user.
     * @queryParam only_active boolean Get the only active passes.
     * @queryParam expired boolean Get the only expired passes.
     * @queryParam waiting_approval boolean Get the passes that are waiting for approval.
     * @queryParam by_students string Get the passes created by selected students Example: ?by_students = 1,2,3.
     * @queryParam by_teachers string Get the passes approved, ended and canceled by teachers. Also get the passes from and to the given teachers. Example: ?by_teachers = 1,2,3
     * @queryParam by_rooms string Get all of the passes pointed to current selected rooms Example: ?to_room = 1,2,3
     * @queryParam edited boolean Get all passes that are with edited time
     * @queryParam sort string The sort types: "asc", "desc"
     * @authenticated
     * @apiResourceModel App\Models\Pass
     * @apiResourceCollection App\Http\Resources\PassCollection
     * @param DashboardFilter $filters
     * @return PassCollection|JsonResponse
     */
    public function filter(DashboardFilter $filters)
    {
        if (!Gate::allows('access-dashboard-actions')) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('invalid.permission'),
                    'status' => __('fail')
                ],
                403
            );
        }

        $user = auth()->user();
        $schoolTimezone = School::findCacheFirst(
            $user->school_id
        )->getTimezone();

        $startDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->startOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));
        $endDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->endOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));

        if (request()->has('sort') || request()->has('search_query')) {
            $pass = Pass::searchableQueries(
                Pass::recordsPerPage(),
                !(request()->input('search_query') !== null),
                $filters,
                true
            );
        } else {
            if (!Transparency::isEnabled()) {
                $pass = Pass::where(
                    'passes.school_id',
                    auth()->user()->school_id
                )
                    ->select('passes.*')
                    ->useIndex(Pass::PASS_HISTORY_MYSQL_INDEX)
                    ->where('passes.parent_id', null)
                    ->where('passes.canceled_at', null)
                    ->whereBetween('passes.created_at', [$startDate, $endDate])
                    ->orderBy('passes.created_at', 'desc')
                    ->leftJoin('passes as child', function ($join) {
                        $join
                            ->on('passes.id', '=', 'child.parent_id')
                            ->where(
                                'child.school_id',
                                auth()->user()->school_id
                            );
                    })
                    ->filter($filters)
                    ->paginate(Pass::recordsPerPage());
            } else {
                $staffSchedules = StaffSchedule::where('user_id', $user->id)
                    ->get()
                    ->pluck('room_id');

                $pass = Pass::where(
                    'passes.school_id',
                    auth()->user()->school_id
                )
                    ->select('passes.*')
                    ->useIndex(Pass::PASS_HISTORY_MYSQL_INDEX)
                    ->where('passes.parent_id', null)
                    ->whereBetween('passes.created_at', [$startDate, $endDate])
                    ->where('passes.canceled_at', null)
                    ->leftJoin('passes as child', function ($join) {
                        $join
                            ->on('passes.id', '=', 'child.parent_id')
                            ->where(
                                'child.school_id',
                                auth()->user()->school_id
                            );
                    })
                    ->when(!request()->has('only_active'), function (
                        $query
                    ) use ($user, $staffSchedules) {
                        $query->transparencyWithColumnPrefix(
                            $user,
                            $staffSchedules,
                            $staffSchedules
                        );
                    })
                    ->filter($filters)
                    ->paginate(Pass::recordsPerPage());
            }
        }

        return new PassCollection($pass->appends('badge_flags'));
    }
}
