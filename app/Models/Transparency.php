<?php

namespace App\Models;

use App\Elastic\TransparenciesIndexConfigurator;
use App\Traits\TransparencyMapping;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transparency extends Model
{
    use HasFactory;

    protected $guarded = [];


    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @param $user
     * @return bool
     */
    public static function isEnabled($user = null): bool
    {
        if ($user === null) {
            $user = optional(auth()->user());
        }

        $transparencyExists = self::fromSchoolId($user->school_id)->exists();
        $transparencyUserExists = TransparencyUser::fromSchoolId($user->school_id)->ofUserId($user->id)->exists();

        return $transparencyExists && !$transparencyUserExists && !$user->isAdmin();
    }
}
