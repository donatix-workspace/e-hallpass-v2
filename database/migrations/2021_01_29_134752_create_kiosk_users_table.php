<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKioskUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kiosk_users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned()->index('user_id');
            $table->string('kpassword', 250)->nullable()->index('password');
            $table->smallInteger('password_processed')->nullable()->default(0)->index('password_processed');
            $table->bigInteger('school_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kiosk_users');
    }
}
