<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Today's Pass </title>
    <!-- Lato font include here -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300i,400,400i,700|Roboto" rel="stylesheet">
    <style>
        body {
            font-family: 'Lato', Arial, sans-serif;
        }

        @media screen and (max-width: 600px) {
            .m-center {
                text-align: center;
            }

            .m-button {
                width: 300px !important;
            }

            .pass-content p {
                text-align: center !important;
            }

            .pass-date {
                font-size: 20px !important;
                line-height: 24px !important;
            }

            .pass-info-wrapper h3,
            .pass-info-wrapper h2 {
                font-size: 20px !important;
                line-height: 24px !important;
            }

            .pass-info-wrapper {
                padding: 26px 6px 26px !important;
            }

            .pass-info-wrapper>div {
                width: auto !important;
                padding: 0 12px;
            }

        }

    </style>
</head>

<body>
<div class="main-wrapper" style="max-width: 600px; width: 100%; margin: 0 auto; background-color: #fff">
    <table cellpadding="0" cellspacing="0" style="max-width: 600px; width: 100%;" class="m-center">

        <tr>
            <td style="padding: 30px 0 0px 0;text-align: center;">
                <a href="{{config('app.front_url')}}"><img src="{{Storage::disk('s3')->url('ehp-700x350.png')}}"
                                                           border="0"
                                                           alt="e-hallpass" height="66px" width="125px"/></a>
            </td>
        </tr>

        <tr>
            <td>
                <strong class="pass-date"
                        style="color: #45455A; font-size: 25px; line-height: 30px; font-family: 'Lato', Arial, sans-serif; font-weight: 400; ">Your
                    pass request(s) for {{ now()->format('l, F j, Y') }}.</strong>
                <p
                    style="margin: 10px 0 32px 0; color: #818181; font-size: 16px; line-height: 22px; font-family: 'Lato', Arial, sans-serif;">
                    Here are the details of your appointment pass(es) for today. Please make sure you are on time
                    for your appointment(s). Have a good day!
                </p>


            </td>
        </tr>

                <tr>
                    <td>
                        @php
                           /**
                            * @var $appointment \App\Models\AppointmentPass
                            */
                        @endphp

                        @foreach($todayAppointments as $appointment)
                        <div class="pass-detail"
                             style="border: 1px solid #DCDDDE; border-radius: 8px; background-color: #F9F9F9; box-shadow: 0 0 1px 0 rgba(0,0,0,0.68); margin-bottom: 35px;">
                            <div class="pass-info-wrapper" style="padding: 15px 31px 26px;">
                                <div style="text-align: center">
                                    <strong
                                        style="font-family: 'Lato', Arial, sans-serif; font-size: 19px; line-height: 24px;font-weight: 300; text-align: justify;color: #4990E2; margin-bottom: 13px; display: inline-block;">{{ $appointment->user->name }}</strong>
                                </div>
                                <div style="width: 40%; display: inline-block; vertical-align: middle;">
                                    <h3
                                        style="color: #959595; font-size: 22px; line-height: 27px; text-align: justify; font-family: 'Lato', Arial, sans-serif; font-weight: 400; margin: 0 10px 0 0;">
                                        {{$appointment->from->name}}
                                    </h3>
                                    <h2
                                        style="font-family: 'Lato', Arial, sans-serif; color: #45455A; font-size: 30px; line-height: 36px; text-align: justify; font-weight: 400; margin: 0;">
                                        {{$appointment->for_date->setTimezone($appointment->school->getTimezone())->format('m/d/Y g:i A')}}</h2>
                                </div>
                                <div style="width: 18%; display: inline-block; vertical-align: middle;">
                                    <img src="{{ asset('images/arrow.png')  }}" alt=""
                                         style="border: 0px; width: 38px; height: 28px;">
                                </div>
                                <div style="width: 40%; display: inline-block; vertical-align: middle;">
                                    <h3
                                        style="color: #4990E2; font-size: 30px; line-height: 36px; text-align: right; margin: 0; font-weight: 400;">
                                    {{$appointment->to->name}}
                                </div>
                            </div>

                            <div class="pass-content"
                                 style="background-color: #EBEBEB; padding: 20px; border-top: 1px solid #979797; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px;">
                                <p
                                    style="color: #45455A; font-size: 14px; line-height: 22px; text-align: justify; font-family: 'Lato', Arial, sans-serif;">
                                    {{$appointment->reason}}
                                </p>
                            </div>
                        </div>
                        @endforeach
                    </td>
                </tr>

        <tr>
            <td style="text-align: center; padding: 40px 32px 20px 32px;">
                    <span
                        style="font-family: 'Lato', Arial, sans-serif; color: #B0AEB0; font-size: 14px; letter-spacing: 0.13px; line-height: 17px; border-top: 1px solid #C3C3C3; display: block; padding-top: 30px;">&copy;
                        {{ date('Y') }} e-hallpass. The Online Digital Hall Pass Solution.</span>
            </td>
        </tr>

    </table>
</div>
</body>

</html>
