<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\EntityService;
use App\Http\Services\CommonUserService\Models\CommonDistrict;
use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonSchoolDomain;
use App\Http\Services\CommonUserService\Models\CommonTimezone;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Models\SchoolsEhp;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CancelAndUpdateAppointmentsAfterArchiveJob;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\AuthType;
use App\Models\Domain;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\SchoolUser;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MigrateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:migrate {--migrateUsers} {--fixSubs=} {--fixArchived} {--fixArchivedFromV2} {--fixNegativeStatuses} {--migrateSchoolUsers=} {--fromId=} {--toId=}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate user from v1 to v2 and CUS';

    protected $client;
    protected $oldDb;
    protected $adminUser;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->oldDb = DB::connection('oldDB');

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->adminUser = User::where('email', 'viktor.lalev@donatix.net')->latest()->first();
        $this->client = new CommonUserService($this->adminUser);

        if ($this->option('fixSubs')) {
            $this->info('Fix Subs');

            $from = $this->option('fromId') ?? 0;
            $to = $this->option('toId') ?? 3000;

            $this->fixSubs($to, $from, $this->option('fixSubs'));
        }
        if ($this->option('fixArchived')) {
            $this->info('Fix Archived');
            $from = $this->option('fromId') ?? 0;
            $to = $this->option('toId') ?? 3000000;

            $this->fixArchived($to, $from, $this->option('fixArchived'));
        }
        if ($this->option('fixArchivedFromV2')) {
            $this->info('Check all users in v2 with archived status');
            $this->fixArchivedFromV2();
        }
        if ($this->option('fixNegativeStatuses')) {
            $this->info('Fix statuses < 0');
            $this->fixNegativeStatuses();
        }

        if ($this->option('migrateUsers')) {
            $this->info('Migrate new users');
            $this->migrateUsers();
        }

        if ($this->option('migrateSchoolUsers')) {
            $this->info('Migrate users for school');

            $from = $this->option('fromId') ?? 0;
            $to = $this->option('toId') ?? 3000;

            if ($this->option('migrateSchoolUsers') == 'all') {
                $this->info("Migrate from {$from} to {$to}");

                $verifiedSchools = DB::table('verified_schools')
                    ->where('status', 1)
                    ->get()
                    ->pluck('school_id');


                $schools = School::where('id', '>', $from)
                    ->where('id', '<=', $to)
                    ->whereNotNull('cus_uuid')
                    ->whereNotNull('old_id')
                    ->whereNotNull('common_id')
                    ->where('status', 1)
                    ->whereNotIn('id', $verifiedSchools)
                    ->get();
            } else {
                $schools = School::where('id', $this->option('migrateSchoolUsers'))
                    ->whereNotNull('cus_uuid')
                    ->whereNotNull('old_id')
                    ->whereNotNull('common_id')
                    ->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Migrate for {$school->name} with id {$school->id}");
                $this->migrateSchoolUsers($school);
            }


        }
        //Migrate from v1 to migrated_users
        //Migrate from v1 to migrated_schools_users
//        $this->migrateUsers();

    }

    protected function fixSubs($from, $to, $oldSchoolId = null)
    {

        if ($oldSchoolId) {
            $this->info("Sync Subs for {$oldSchoolId}");
            $dbQuery = $this->oldDb->table('users')
                ->where('role_id', 0)
                ->when($oldSchoolId != 'all', function ($q) use ($oldSchoolId) {
                    $q->where('school_id', $oldSchoolId);
                })
                ->orderBy('user_id');
        } else {
            $this->info("Sync Subs from {$from} to {$to}");
            $dbQuery = $this->oldDb->table('users')
                ->where('role_id', 0)
                ->where('user_id', '>', $from)
                ->where('user_id', '<=', $to)
                ->orderBy('user_id');
        }

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($oldSubsUsers) {

            foreach ($oldSubsUsers as $oldSubsUser) {

                $school = School::where('old_id', $oldSubsUser->school_id)->first();

                if ($school) {
                    $user = User::withoutGlobalScopes()
                        ->where('old_id', $oldSubsUser->user_id)
                        ->first();

                    if (!$user) {
                        $user = \App\Models\User::create([
                            'student_sis_id' => $oldSubsUser->student_sis_id,
                            'email' => trim($oldSubsUser->email),
                            'username' => $oldSubsUser->username,
                            'password' => $oldSubsUser->password,
                            'first_name' => $oldSubsUser->firstname,
                            'last_name' => $oldSubsUser->lastname,
                            'metatag' => $oldSubsUser->metatag,
                            'role_id' => 3,
                            'avatar' => $oldSubsUser->avatar,
                            'grade_year' => $oldSubsUser->gradyear,
                            'status' => $oldSubsUser->status < 1 ? 0 : 1,
                            'school_id' => $school->id,
                            'is_searchable' => $oldSubsUser->is_searchable,
                            'user_initials' => $oldSubsUser->user_initials,
                            'is_loggin' => $oldSubsUser->is_loggin,
                            'clever_id' => $oldSubsUser->clever_id,
                            'updated_role' => $oldSubsUser->updated_role,
                            'last_logged_in_at' => $oldSubsUser->last_logged_in_at,
                            'auth_type' => $oldSubsUser->auth_type,
                            'created_at' => $oldSubsUser->created_at,
                            'updated_at' => now(),
                            'old_id' => $oldSubsUser->user_id,
                            'is_substitute' => 1,
                        ]);
                    }

                    DB::table('schools_users')->updateOrInsert(
                        [
                            'school_id' => $school->id,
                            'user_id' => $user->id
                        ],
                        [
                            'role_id' => 3,
                            'status' => $oldSubsUser->status < 1 ? 0 : 1,
                            'archived_at' => $oldSubsUser->status == -2 ? 1 : 0,
                            'deleted_at' => $oldSubsUser->status < 1 ? $oldSubsUser->updated_at : null,
                            'student_sis_id' => $oldSubsUser->student_sis_id,
                            'old_id'=>$oldSubsUser->user_id,
                        ]
                    );

                    $user->update([
                        'status' => $oldSubsUser->status < 1 ? 0 : 1,
                        'is_substitute' => 1,
                    ]);

                    User::withoutGlobalScopes()
                        ->where('id', '!=', $user->id)
                        ->where('email', $user->email)
                        ->where('school_id', $school->id)
                        ->update([
                            'status' => 0,
                            'deleted_at' => now()
                        ]);
                }

                $user->searchable();
                $this->output->progressAdvance();
            }
        });

    }

    protected function fixArchived($fromId, $toId, $oldSchoolId)
    {
//        $dbQuery = $this->oldDb->table('users')
//            ->where('status', -2)
//            ->when($oldSchoolId != 'all', function ($q) use ($oldSchoolId) {
//                $q->where('school_id', $oldSchoolId);
//            })
//            ->when($fromId, function ($q) use ($fromId) {
//                $q->where('user_id', '>', $fromId);
//            })
//            ->when($toId, function ($q) use ($toId) {
//                $q->where('user_id', '<=', $toId);
//            })
//            ->orderBy('user_id');

        $membershipIds = DB::table('schools_users')
            ->whereNotNull('archived_at')
            ->whereNotNull('old_id')
        ->get()
        ->pluck('old_id');

        $dbQuery = $this->oldDb->table('users')
            ->where('status', -2)
           ->whereIntegerNotInRaw('user_id',$membershipIds)
            ->orderBy('user_id');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($oldSubsUsers) {
            foreach ($oldSubsUsers as $oldSubsUser) {
                $school = School::where('old_id', $oldSubsUser->school_id)->first();

                if ($school) {

                    $membership = DB::table('schools_users')
                        ->where('old_id', $oldSubsUser->user_id)
                        ->where('school_id', $school->id)
                        ->orderBy('id', 'desc')
                        ->first();

                    if (!$membership) {
                        $oUser = \App\Models\V1\User::where('user_id', $oldSubsUser->user_id)->first();
                        $oUser->sync($school);

                    } else {

                        $r = DB::table('schools_users')
                            ->where('id', $membership->id)
                            ->update([
                                'status' => User::INACTIVE,
                                'archived_by' => $this->adminUser->id,
                                'archived_at' => $oldSubsUser->updated_at,
                            ]);

                        User::where('id', $membership->user_id)->searchable();
                    }


                } else {
                    $this->info(json_encode([
                        'old_id' => $oldSubsUser->school_id,
                        'school_id' => $school->id
                    ]));
                }


//                $user = User::withoutGlobalScopes()->where('old_id', $oldSubsUser->user_id)->first();
//
//                if ($user && $school) {
//
//                    $user->update([
//                        'status' => User::INACTIVE,
//                        'archived_by' => $this->adminUser->id,
//                        'archived_at' => $oldSubsUser->updated_at,
//                        'archived_school' => $school->id
//                    ]);
//
//
//                }


                $this->output->progressAdvance();
            }
        });

    }

    protected function fixArchivedFromV2()
    {
        $users = User::withoutGlobalScopes()
            ->where('status', -2);

        $this->output->progressStart($users->count());

        foreach ($users->get() as $user) {

            $oldUser = \App\Models\V1\User::withoutGlobalScopes()
                ->where('email', $user->email)
                ->where('status', -2)
                ->first();
            if ($oldUser) {
                $user->update([
                    'status' => User::INACTIVE,
                    'archived_by' => $this->adminUser->id,
                    'archived_at' => $oldUser->updated_at,
                    'archived_school' => $user->school_id
                ]);
            } else {
                $user->update([
                    'status' => User::INACTIVE,
                    'archived_by' => $this->adminUser->id,
                    'archived_at' => now(),
                    'archived_school' => $user->school_id
                ]);
            }

            $this->output->progressAdvance();
        }


    }

    protected function fixNegativeStatuses()
    {
        $users = User::withoutGlobalScopes()
            ->where('status', '<', 0);

        $this->output->progressStart($users->count());

        foreach ($users->get() as $user) {


            $user->update([
                'status' => User::INACTIVE,
                'auth_type' => $user->username == "Simulate user" ? 1001 : null,
            ]);

            $this->output->progressAdvance();
        }


    }

    protected function migrateUsers()
    {


        $dbQuery = $this->oldDb
            ->table('users')
            ->whereDate('created_at', '>', '2022-03-01')
            ->orderBy('user_id');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($oldUsers) {
            foreach ($oldUsers as $oldUser) {

                $oldUserModel = \App\Models\V1\User::where('user_id', $oldUser->user_id)->first();

                if ($oldUserModel) {
                    $oldUserModel->migrateToV2andCus();
                }

                $this->output->progressAdvance();
            }
        });

    }

    protected function migrateSchoolUsers($school)
    {
        $v2Emails = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select('u.email')
            ->where('m.school_id', $school->id)
            ->where('u.status', 1)
            ->where(function ($query) {
                $query
                    ->where('u.auth_type', '!=', 1001)
                    ->orWhereNull('u.auth_type');
            })
            ->whereNull('m.deleted_at')
            ->whereNull('u.deleted_at')
            ->get()
            ->pluck('email');


        $oldUsers = \App\Models\V1\User::where('status', 1)
            ->where('school_id', $school->old_id)
            ->whereNotIn('email', $v2Emails);

        $this->output->progressStart($oldUsers->count());

        foreach ($oldUsers->get() as $oldUser) {

            $oldUser->migrateToV2andCus();
            $this->output->progressAdvance();
        }


    }

}
