<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('role_id')->references('id')->on('roles');
        });
        Schema::table('school_modules', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('module_id')->references('id')->on('modules');
        });
        Schema::table('pins', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
        });
        Schema::table('kiosks', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->foreign('school_id')->references('id')->on('schools');
        });
        Schema::table('rooms', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('type_id')->references('id')->on('room_types')->onDelete('cascade');;
        });
        Schema::table('room_types', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
        });
        Schema::table('invite_users', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('school_id')->references('id')->on('schools');
        });
        Schema::table('school_codes', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
        });
        Schema::table('staff_schedules', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->foreign('school_id')->references('id')->on('schools');
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('pass_id')->references('id')->on('passes');
            $table->foreign('to_teacher_id')->references('id')->on('users');
            $table->foreign('from_teacher_id')->references('id')->on('users');
            $table->foreign('school_id')->references('id')->on('schools');
        });
        Schema::table('student_favorites', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('school_id')->references('id')->on('schools');
        });
        Schema::table('passes', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('requested_by')->references('id')->on('users');
            $table->foreign('approved_by')->references('id')->on('users');
            $table->foreign('completed_by')->references('id')->on('users');
            $table->foreign('created_by')->references('id')->on('users');
        });
        Schema::table('kiosk_users', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('school_id')->references('id')->on('schools');
        });
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('pass_id')->references('id')->on('passes');
            $table->foreign('from_teacher_id')->references('id')->on('users');
            $table->foreign('from_room_id')->references('id')->on('rooms');
            $table->foreign('to_room_id')->references('id')->on('rooms');
            $table->foreign('in_role_id')->references('id')->on('roles');
            $table->foreign('out_role_id')->references('id')->on('roles');
            $table->foreign('from_teacher_id2')->references('id')->on('users');
            $table->foreign('to_teacher_id2')->references('id')->on('users');
            $table->foreign('from_room_id2')->references('id')->on('rooms');
            $table->foreign('to_room_id2')->references('id')->on('rooms');
        });
        Schema::table('polarities', function (Blueprint $table) {
            $table->foreign('first_user_id')->references('id')->on('users');
            $table->foreign('second_user_id')->references('id')->on('users');
        });
        Schema::table('auto_passes', function (Blueprint $table) {
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->foreign('school_id')->references('id')->on('schools');
        });
        Schema::table('nurse_rooms', function (Blueprint $table) {
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->foreign('school_id')->references('id')->on('schools');
        });
        Schema::table('teacher_pass_settings', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_school_id_foreign');
            $table->dropForeign('users_role_id_foreign');
        });
        Schema::table('school_modules', function (Blueprint $table) {
            $table->dropForeign('school_modules_school_id_foreign');
            $table->dropForeign('school_modules_module_id_foreign');
        });
        Schema::table('pins', function (Blueprint $table) {
            $table->dropForeign('pins_school_id_foreign');
        });
        Schema::table('kiosks', function (Blueprint $table) {
            $table->dropForeign('kiosks_user_id_foreign');
            $table->dropForeign('kiosks_room_id_foreign');
            $table->dropForeign('kiosks_school_id_foreign');
        });
        Schema::table('rooms', function (Blueprint $table) {
            $table->dropForeign('rooms_school_id_foreign');
            $table->dropForeign('rooms_type_id_foreign');
        });
        Schema::table('room_types', function (Blueprint $table) {
            $table->dropForeign('room_types_school_id_foreign');
        });
        Schema::table('invite_users', function (Blueprint $table) {
            $table->dropForeign('invite_users_user_id_foreign');
            $table->dropForeign('invite_users_role_id_foreign');
            $table->dropForeign('invite_users_school_id_foreign');
        });
        Schema::table('school_codes', function (Blueprint $table) {
            $table->dropForeign('school_codes_school_id_foreign');
        });
        Schema::table('staff_schedules', function (Blueprint $table) {
            $table->dropForeign('staff_schedules_user_id_foreign');
            $table->dropForeign('staff_schedules_room_id_foreign');
            $table->dropForeign('staff_schedules_school_id_foreign');
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->dropForeign('notifications_user_id_foreign');
            $table->dropForeign('notifications_pass_id_foreign');
            $table->dropForeign('notifications_to_teacher_id_foreign');
            $table->dropForeign('notifications_from_teacher_id_foreign');
            $table->dropForeign('notifications_school_id_foreign');
        });
        Schema::table('student_favorites', function (Blueprint $table) {
            $table->dropForeign('student_favorites_user_id_foreign');
            $table->dropForeign('student_favorites_school_id_foreign');
        });
        Schema::table('passes', function (Blueprint $table) {
            $table->dropForeign('passes_user_id_foreign');
            $table->dropForeign('passes_requested_by_foreign');
            $table->dropForeign('passes_completed_by_foreign');
            $table->dropForeign('passes_approved_by_foreign');
            $table->dropForeign('passes_created_by_foreign');
            $table->dropForeign('passes_school_id_foreign');
        });
        Schema::table('kiosk_users', function (Blueprint $table) {
            $table->dropForeign('kiosk_users_user_id_foreign');
            $table->dropForeign('kiosk_users_school_id_foreign');
        });
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->dropForeign('pass_summary_reports_user_id_foreign');
            $table->dropForeign('pass_summary_reports_school_id_foreign');
            $table->dropForeign('pass_summary_reports_pass_id_foreign');
            $table->dropForeign('pass_summary_reports_from_teacher_id_foreign');
            $table->dropForeign('pass_summary_reports_from_room_id_foreign');
            $table->dropForeign('pass_summary_reports_to_room_id_foreign');
            $table->dropForeign('pass_summary_reports_in_role_id_foreign');
            $table->dropForeign('pass_summary_reports_out_role_id_foreign');
            $table->dropForeign('pass_summary_reports_from_teacher_id2_foreign');
            $table->dropForeign('pass_summary_reports_to_teacher_id2_foreign');
            $table->dropForeign('pass_summary_reports_from_room_id2_foreign');
            $table->dropForeign('pass_summary_reports_to_room_id2_foreign');
        });
        Schema::table('polarities', function (Blueprint $table) {
            $table->dropForeign('polarities_first_user_id_foreign');
            $table->dropForeign('polarities_second_user_id_foreign');
        });
        Schema::table('auto_passes', function (Blueprint $table) {
            $table->dropForeign('auto_passes_room_id_foreign');
            $table->dropForeign('auto_passes_school_id_foreign');
        });
        Schema::table('nurse_rooms', function (Blueprint $table) {
            $table->dropForeign('nurse_rooms_room_id_foreign');
            $table->dropForeign('nurse_rooms_school_id_foreign');
        });
        Schema::table('teacher_pass_settings', function (Blueprint $table) {
            $table->dropForeign('teacher_pass_settings_school_id_foreign');
        });

    }
}
