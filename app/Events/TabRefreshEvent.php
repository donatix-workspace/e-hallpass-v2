<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;

class TabRefreshEvent implements ShouldBroadcastNow
{
    use Dispatchable;
    private $userId;
    public $counters;

    /**
     * @param int $userId
     * @param array $counters
     */
    public function __construct(int $userId, array $counters)
    {
        $this->userId = $userId;
        $this->counters = $counters;
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn(): PrivateChannel
    {
        return new PrivateChannel('dashboard-stats.' . $this->userId);
    }
}
