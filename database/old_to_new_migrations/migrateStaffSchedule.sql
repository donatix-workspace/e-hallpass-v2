SET FOREIGN_KEY_CHECKS  = 0;
START TRANSACTION ;
TRUNCATE ehp_v2_prod.staff_schedules;
INSERT INTO ehp_v2_prod.staff_schedules (room_id, user_id, school_id, status, created_at, updated_at) SELECT
        (SELECT id FROM ehp_v2_prod.rooms WHERE old_id=oldSs.room_id limit 1),
        (SELECT id FROM ehp_v2_prod.users WHERE old_id=oldSs.staff_id limit 1),
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldSs.school_id limit 1),
        (SELECT oldSs.status),
        (SELECT CONVERT_TZ(oldSs.created_at, 'America/New_York', 'UTC')),
        (SELECT CONVERT_TZ(oldSs.updated_at, 'America/New_York', 'UTC'))
FROM ehallpassDB.staff_schedule as oldSs;

COMMIT ;

SET FOREIGN_KEY_CHECKS  = 1;

UPDATE
    ehp_v2_prod.staff_schedules
SET
    user_id = (
        SELECT
            to_id
        FROM
            duplicate_users
        WHERE
                from_id = user_id
        LIMIT 1)
WHERE
        user_id IN(
        SELECT
            from_id FROM duplicate_users);
