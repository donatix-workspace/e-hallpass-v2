<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/** @mixin \App\Models\UserMobileSetting */
class UserMobileSettingsResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array|null
     */
    public function toArray($request): ?array
    {
        if (!optional($this)->id) {
            return [
                'mobile_sound' => true,
                'notification_settings' => null
            ];
        }

        return [
            'notification_settings' => $this->notification_settings,
            'mobile_sound' => $this->mobile_sound
        ];
    }
}
