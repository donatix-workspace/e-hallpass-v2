<?php

namespace App\Http\Controllers\Rooms;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImportRequest;
use App\Imports\RoomsImport;
use App\Imports\UsersImport;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    /**
     * Import rooms by csv file
     *
     * @bodyParam csv_file file required The csv file for importing the users.
     * @authenticated
     * @param ImportRequest $request
     * @return JsonResponse
     */
    public function import(ImportRequest $request): JsonResponse
    {
        $usersImportCsvFileName = time() . '-' . auth()->user()->id . '-roomschoolid-' . auth()->user()->school_id . '.csv';

        $request->file('csv_file')->storeAs('csv', $usersImportCsvFileName);

        Excel::import(new RoomsImport(auth()->user()->school_id), storage_path('app/csv/' . $usersImportCsvFileName));

        return response()->json([
            'data' => [
                'file_name' => $usersImportCsvFileName
            ],
            'status' => __('success')
        ]);
    }
}
