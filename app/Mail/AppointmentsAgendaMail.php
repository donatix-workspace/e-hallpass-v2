<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AppointmentsAgendaMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $todayAppointments;

    /**
     * @param $todayAppointments
     */
    public function __construct($todayAppointments)
    {
        $this->todayAppointments = $todayAppointments;
    }

    /**
     * @return AppointmentsAgendaMail|\App\Mail\AppointmentsAgendaMail.view
     */
    public function build(): AppointmentsAgendaMail
    {
        return $this->view('emails.appointments.agenda');
    }
}
