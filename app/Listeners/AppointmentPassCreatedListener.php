<?php

namespace App\Listeners;

use App\Events\AppointmentPassCreated;
use App\Models\AppointmentNotification;
use App\Models\RecurrenceAppointmentPass;

class AppointmentPassCreatedListener
{
    public function __construct()
    {
        //
    }

    public function handle(AppointmentPassCreated $event)
    {
//        logger('Payload:', [$event->appointmentPass]);
//        if ($event->appointmentPass instanceof RecurrenceAppointmentPass) {
//            $recurrenceAppointments = $event->appointmentPass->appointmentPasses()->get();
//            foreach ($recurrenceAppointments as $recurrenceAppointment) {
//                $appointmentPassNotification = AppointmentNotification::create([
//                    'notification_type' => AppointmentNotification::CREATED,
//                    'appointment_pass_id' => $recurrenceAppointment->id,
//                    'user_id' => $recurrenceAppointment->user_id,
//                    'school_id' => $recurrenceAppointment->school_id,
//                    'updated_by_id'=> $event->newUserId
//                ]);
//            }
//        } else {
//            $appointmentPassNotification = AppointmentNotification::create([
//                'notification_type' => AppointmentNotification::CREATED,
//                'appointment_pass_id' => $event->appointmentPass->id,
//                'user_id' => $event->appointmentPass->user_id,
//                'school_id' => $event->appointmentPass->school_id,
//                'updated_by_id'=> $event->newUserId
//            ]);
//        }
    }
}
