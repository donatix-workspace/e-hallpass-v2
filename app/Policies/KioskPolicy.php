<?php

namespace App\Policies;

use App\Models\Kiosk;
use App\Models\KioskUser;
use App\Models\Pass;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class KioskPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return !$user->isStudent();
    }

    /**
     * Determine whether the user can create pass from kiosk.
     *
     * @param KioskUser $user
     * @param Kiosk $kiosk
     * @return bool|Response
     */
    public function createPass(KioskUser $user, Kiosk $kiosk)
    {
        if (!$user->isStudent()) {
            return $this->deny(__('You must be a student for this action.'));
        }

        if ($kiosk->school_id !== $user->school_id) {
            return $this->deny(
                __('You cannot be in another school for this Kiosk.')
            );
        }

        if ($kiosk->expired_date === null && $kiosk->password_use !== null) {
            return true;
        }

        return $this->deny(__('The Kiosk was ended.'));
    }

    /**
     * Determine whether the user can create pass from kiosk.
     *
     * @param KioskUser $user
     * @param Kiosk $kiosk
     * @param Pass $pass
     * @return bool|Response
     */
    public function updatePass(KioskUser $user, Kiosk $kiosk, Pass $pass)
    {
        if (!$user->isStudent()) {
            return $this->deny(__('You must be a student for this action.'));
        }

        if ($kiosk->school_id !== $user->school_id) {
            return $this->deny(
                __('You cannot be in another school for this Kiosk.')
            );
        }

        if ($user->user_id !== $pass->user_id) {
            return $this->deny(__('This pass does not belongs to you.'));
        }

        if ($kiosk->expired_date === null && $kiosk->password_use !== null) {
            return true;
        }

        return $this->deny(__('The Kiosk was ended.'));
    }

    /**
     * @param User $user
     * @return bool|Response
     */
    public function create(User $user)
    {
        if ($user->isStudent()) {
            return $this->deny('This action is unauthorized for this role.');
        }

        return true;
    }

    /**
     * @param User $user
     * @param Kiosk $kiosk
     * @return bool|Response
     */
    public function deactivate(User $user, Kiosk $kiosk)
    {
        if ($user->isStudent()) {
            return $this->deny(
                __('This action is unauthorized for this role.')
            );
        }

        if ($user->school_id !== $kiosk->school_id) {
            return $this->deny(
                __(
                    'This action is unauthorized because you are in different school.'
                )
            );
        }

        return true;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Kiosk $kiosk
     * @return bool
     */
    public function delete(User $user, Kiosk $kiosk): bool
    {
        return $user->isAdmin() && $user->school_id === $kiosk->school_id;
    }
}
