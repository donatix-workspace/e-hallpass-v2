<?php

namespace App\Events;

use App\Models\Pass;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PassApproved implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $pass;
    public $fromAdult;
    public $notifyStudent;
    public $notifyAdultsIds;

    /**
     * Create a new event instance.
     *
     * @param $pass
     * @param bool|null $fromAdult
     */
    public function __construct($pass, ?bool $fromAdult = false)
    {
        $this->pass = $pass;

        $this->notifyStudent = $fromAdult;

        if (!$fromAdult) {
            $this->notifyAdultsIds = $pass->getNotifiableAdults();
        }

        $this->fromAdult = $fromAdult;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('passes.' . $this->pass->id),
            new PrivateChannel('user.passes.' . $this->pass->user_id),
        ];
    }
}
