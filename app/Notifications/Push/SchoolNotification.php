<?php

namespace App\Notifications\Push;

use App\Http\Resources\PassResource;
use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SchoolNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $event;
    protected $options;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($event, $options = [])
    {
        $this->event = $event;
        $this->options = $options;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm'];
    }

    public function viaQueues()
    {
        return [
            'fcm' => 'notifications',
        ];
    }
    
    public function toFcm($notifiable)
    {
        $notificationSettings = $notifiable->getNotificationSettings($this->event, $this->options);

        $message = new FcmMessage();
        $message->contentAvailable(true)
            ->condition("'{$notificationSettings['topics']}' in topics")
            ->content([
                'title' => $notificationSettings['title'],
                'body' => $notificationSettings['body'],
            ])->data($notificationSettings['data']);

        return $message;
    }
}
