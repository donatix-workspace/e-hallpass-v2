<?php

namespace App\Policies;

use App\Models\AppointmentPass;
use App\Models\Module;
use App\Models\Room;
use App\Models\StaffSchedule;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Contracts\Auth\Authenticatable;

class AppointmentPassPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin() || $user->isTeacher() || $user->isStaff();
    }

    public function viewStudent(User $user)
    {
        return $user->isStudent();
    }

    /**
     * @param User $user
     * @param AppointmentPass $appointmentPass
     * @return bool
     */
    public function showStudent(
        User $user,
        AppointmentPass $appointmentPass
    ): bool {
        return $user->isStudent() &&
            $user->id === $appointmentPass->user_id &&
            $appointmentPass->school_id === $user->school_id;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\AppointmentPass $appointmentPass
     * @return mixed
     */
    public function view(User $user, AppointmentPass $appointmentPass)
    {
        return !$user->isStudent() &&
            $appointmentPass->school_id === $user->school_id;
    }

    /**
     * @param User $user
     * @param AppointmentPass $appointmentPass
     * @return bool
     */
    public function acknowledge(User $user, AppointmentPass $appointmentPass)
    {
        return $appointmentPass->user_id === $user->id &&
            !$appointmentPass->isCanceled();
    }

    /**
     * @param User $user
     * @param AppointmentPass $appointmentPass
     * @return bool
     */
    public function confirm(User $user, AppointmentPass $appointmentPass)
    {
        return $user->isStudent() &&
            $appointmentPass->user_id === $user->id &&
            !$appointmentPass->isConfirmedByUser() &&
            !$appointmentPass->isCanceled();
    }

    /**
     * @param User $user
     * @param AppointmentPass $appointmentPass
     * @return bool
     */
    public function acknowledgeByTeacher(
        User $user,
        AppointmentPass $appointmentPass
    ): bool {
        $staffSchedulesFrom = StaffSchedule::where(
            'user_id',
            optional($user)->id
        )
            ->where('room_id', $appointmentPass->from_id)
            ->exists();

        if (
            $appointmentPass->from_id === config('roles.system') &&
            !$appointmentPass->isAcknowledgedByTeacher()
        ) {
            return true;
        }

        if (
            $staffSchedulesFrom &&
            !$appointmentPass->isAcknowledgedByTeacher()
        ) {
            return true;
        }

        if (
            $appointmentPass->from_type === User::class &&
            $appointmentPass->from_id === $user->id &&
            !$appointmentPass->isAcknowledgedByTeacher()
        ) {
            return true;
        }

        if (
            !$appointmentPass->from_id &&
            !$appointmentPass->isAcknowledgedByTeacher()
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param AppointmentPass $appointmentPass
     * @return bool
     */
    public function confirmTeacher(User $user, AppointmentPass $appointmentPass)
    {
        $staffSchedulesTo = false;
        if (
            !$appointmentPass->isConfirmedByTeacher() &&
            !$appointmentPass->isCanceled()
        ) {
            //            if ($appointmentPass->to_type === User::class && $appointmentPass->to_id === optional($user)->id) {
            //                return true;
            //            }

            if (
                $appointmentPass->createdByUser->isStudent() &&
                $appointmentPass->to_type === User::class &&
                $appointmentPass->to_id === optional($user)->id
            ) {
                return true;
            }

            if (
                $appointmentPass->createdByUser->isStudent() &&
                $appointmentPass->to_type === Room::class
            ) {
                $staffSchedulesTo = StaffSchedule::where(
                    'user_id',
                    optional($user)->id
                )
                    ->where('room_id', $appointmentPass->to_id)
                    ->exists();
            }

            if (
                $staffSchedulesTo &&
                !$appointmentPass->isConfirmedByTeacher()
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin() || $user->isTeacher() || $user->isStaff();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\AppointmentPass $appointmentPass
     * @return mixed
     */
    public function update(User $user, AppointmentPass $appointmentPass)
    {
        $schoolModuleOption = json_decode(
            $user->school->getModuleOptions(Module::APPOINTMENTPASS)
        );

        $staffSchedulesFrom = false;
        $staffSchedulesTo = false;

        if (
            !$appointmentPass->isCanceled() &&
            $appointmentPass->pass_id === null
        ) {
            if ($user->isAdmin()) {
                if (!optional($schoolModuleOption)->admin_edit) {
                    return true;
                }
            }

            if ($appointmentPass->from_id === config('roles.system')) {
                return true;
            }

            if (
                $appointmentPass->to_type === Room::class ||
                $appointmentPass->from_type === Room::class
            ) {
                $staffSchedulesFrom = StaffSchedule::where(
                    'user_id',
                    optional($user)->id
                )->where('room_id', $appointmentPass->from_id);

                $staffSchedulesTo = StaffSchedule::where(
                    'user_id',
                    optional($user)->id
                )->where('room_id', $appointmentPass->to_id);

                if ($staffSchedulesFrom || $staffSchedulesTo) {
                    return true;
                }
            }

            if ($appointmentPass->created_by === optional($user)->id) {
                return true;
            }

            if (
                $appointmentPass->to_id === optional($user)->id &&
                $appointmentPass->to_type === User::class
            ) {
                return true;
            }

            if (
                $appointmentPass->from_id === optional($user)->id &&
                $appointmentPass->from_type === User::class
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\AppointmentPass $appointmentPass
     * @return bool
     */
    public function delete(User $user, AppointmentPass $appointmentPass)
    {
        if ($user->isAdmin()) {
            return true;
        }

        if (!$user->isStudent() && $appointmentPass->created_by === $user->id) {
            return true;
        }

        if (
            !$user->isStudent() &&
            $appointmentPass->isForFuture() &&
            $appointmentPass->recurrence_appointment_pass_id !== null
        ) {
            return true;
        }

        if (
            (!$user->isStudent() &&
                ($appointmentPass->from_type === User::class &&
                    $appointmentPass->from_id === $user->id)) ||
            ($appointmentPass->to_type === User::class &&
                $appointmentPass->to_type === $user->id &&
                $appointmentPass->isForFuture() &&
                $appointmentPass->recurrence_appointment_pass_id)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param AppointmentPass $appointmentPass
     * @return bool
     */
    public function cancel(User $user, AppointmentPass $appointmentPass)
    {
        if (!$appointmentPass->isCanceled()) {
            if (
                $appointmentPass->from_type === User::class &&
                $appointmentPass->from_id === $user->id
            ) {
                return true;
            }

            if ($user->isAdmin()) {
                return true;
            }

            if ($appointmentPass->from_id === config('roles.system')) {
                return true;
            }

            if ($appointmentPass->created_by === $user->id) {
                return true;
            }

            if (
                $appointmentPass->to_type === User::class &&
                $appointmentPass->to_id === $user->id
            ) {
                return true;
            }

            if (
                $appointmentPass->to_type === Room::class ||
                $appointmentPass->from_type === Room::class
            ) {
                $staffSchedulesFrom = StaffSchedule::where(
                    'user_id',
                    optional($user)->id
                )->where('room_id', $appointmentPass->from_id);

                $staffSchedulesTo = StaffSchedule::where(
                    'user_id',
                    optional($user)->id
                )->where('room_id', $appointmentPass->to_id);

                if ($staffSchedulesFrom || $staffSchedulesTo) {
                    return true;
                }
            }
        }
        return false;
    }

    public function studentCancel(User $user, AppointmentPass $appointmentPass)
    {
        return $user->isStudent() &&
            !$appointmentPass->isCanceled() &&
            !$appointmentPass->isMissed();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function studentRequest(User $user)
    {
        return $user->isStudent();
    }

    /**
     * @param Authenticatable $user
     * @param $locationId
     * @param string $locationType
     * @return bool
     */
    public static function canStudentRequestToRoleOrLocation(
        Authenticatable $user,
        $locationId,
        string $locationType = User::class
    ): bool {
        $locationUser = null;
        $locationRoom = null;

        $schoolModuleOption = $user->school->getModuleOptions(
            Module::APPOINTMENTPASS
        );

        if ($locationType === User::class) {
            $locationUser = User::find($locationId);
        }

        if ($locationType === Room::class) {
            $locationRoom = Room::find($locationId);
        }

        $allowedRolesOption = json_decode($schoolModuleOption);

        return (optional($allowedRolesOption)->staff &&
            optional($locationUser)->isStaff() &&
            optional($locationUser)->allow_appointment_requests) ||
            (optional($allowedRolesOption)->admin &&
                optional($locationUser)->isAdmin() &&
                optional($locationUser)->allow_appointment_requests) ||
            (optional($allowedRolesOption)->teacher &&
                optional($locationUser)->isTeacher() &&
                optional($locationUser)->allow_appointment_requests) ||
            (optional($allowedRolesOption)->location &&
                optional($locationRoom)->enable_appointment_passes);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\AppointmentPass $appointmentPass
     * @return mixed
     */
    public function restore(User $user, AppointmentPass $appointmentPass)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\AppointmentPass $appointmentPass
     * @return mixed
     */
    public function forceDelete(User $user, AppointmentPass $appointmentPass)
    {
        //
    }
}
