<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KioskImportEditRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'kiosk_password' => 'required|min:1|max:6'
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}
