<?php

namespace App\Rules;

use App\Models\Room;
use App\Models\Unavailable;
use App\Models\User;
use App\Policies\PassPolicy;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class CheckIfDestinationIsUnavailableRule implements Rule
{
    public $type;
    public $toId;
    private $unavailable;

    /**
     * @param null|string $type
     */
    public function __construct(?string $type)
    {
        $this->type = $type;
    }

    public function passes($attribute, $value): bool
    {
        $this->toId = $value;
        $staffUnavailability = PassPolicy::checkUnavailable(
            $this->type,
            $value
        );

        if ($staffUnavailability) {
            $this->unavailable = Unavailable::active()
                ->fromSchoolId(auth()->user()->school_id)
                ->where('unavailable_type', $this->type)
                ->where('unavailable_id', $value)
                ->where('from_date', '<=', Carbon::now()->toDateTimeString())
                ->where('to_date', '>=', Carbon::now()->toDateTimeString())
                ->where('canceled_for_today_at', null)
                ->first()
                ->toArray();

            return false;
        }

        return true;
    }

    /**
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function message()
    {
        $unavailableUntil = $this->unavailable;

        if ($this->type === User::class) {
            $location = User::find($this->toId);
        } else {
            $location = Room::find($this->toId);
        }

        return __('passes.teacher.unavailable.with.data', [
            'name' =>
                $this->type === User::class
                    ? $location->name_with_initials
                    : $location->name,
            'date' => Carbon::parse($unavailableUntil['to_date'])->format(
                'm/d/Y g:i A'
            ),
            'reason' => $unavailableUntil['comment']
        ]);
    }
}
