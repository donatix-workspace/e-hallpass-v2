<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserImportsTable extends Migration
{
    public function up()
    {
        Schema::create('users_imports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('email');
            $table->string('last_name');
            $table->boolean('status');
            $table->integer('role_id');
            $table->bigInteger('school_id');
            $table->year('grade_year');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_imports');
    }
}
