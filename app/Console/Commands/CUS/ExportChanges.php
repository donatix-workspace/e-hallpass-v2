<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Translators\SchoolTranslator;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class ExportChanges extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:export {--school=} {--from=} {--to=}';
    protected $client;
    protected $adminUser;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export changes in totals';

    protected $schoolIds = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {


        if ($this->option('school')) {

            $param = $this->option('school');
            $from = $this->option('from') ?? 0;
            $to = $this->option('to') ?? 3000;

            if ($param == 'all') {
                $this->info("Export from {$from} to {$to}");

                $schools = School::where('id', '>', $from)
                    ->where('id', '<=', $to)
                    ->where('status', 1)
                    ->whereNotNull('old_id')->get();

            } else {
                $schools = School::where('id', $param)->whereNotNull('old_id')->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Export for {$school->name} with id {$school->id}");
                $this->export($school);
            }

            dd(json_encode($this->schoolIds));
        }

    }

    protected function export($school)
    {

        $v1Counts = $this->v1Counters($school);
        $v2Counts = $this->v2Counters($school);

        $admin = $v1Counts->admin != $v2Counts->admin;
        $staff = $v1Counts->staff != $v2Counts->staff;
        $teacher = $v1Counts->teacher != $v2Counts->teacher;
        $student = $v1Counts->student != $v2Counts->student;

        if ($admin || $staff || $teacher || $student) {
            $this->schoolIds[] = $school->id;
        }

        return true;

    }

    protected function v1Counters($school)
    {
        $counter = DB::connection('oldDB')->select(
            'SELECT
	count(
		CASE WHEN role_id = 2 THEN
			1
		END) AS admin,
	count(
		CASE WHEN role_id = 3 THEN
			1
		END) AS teacher,
	count(
		CASE WHEN role_id = 4 THEN
			1
		END) AS student,
	count(
		CASE WHEN role_id = 5 THEN
			1
		END) AS staff
		FROM
	ehallpassDB.users
WHERE
	school_id = ?
	AND status = 1
LIMIT 1;',
            [$school->old_id]
        );

        return collect($counter)->first();
    }

    protected function v2Counters($school)
    {
        $counter = DB::select(
            "select
	  count(CASE m.role_id
      WHEN 1 THEN 'student' END ) as student,
      count(CASE m.role_id WHEN 2 THEN 'admin' END) as admin,
      count(case m.role_id WHEN 3 THEN 'teacher' END) as teacher,
      count(case m.role_id WHEN 4 THEN 'staff' END) as staff
     from schools_users m  join schools s on m.school_id = s.id join users u on m.user_id = u.id
 where s.id = ?
 and u.status = 1
 and m.status = 1
   and u.is_substitute = 0
and u.archived_at is null
 and (u.auth_type != 1001 or u.auth_type is null)

and m.deleted_at is null
 and u.deleted_at is null and not (u.username <=> 'system') and (u.first_name != 'System Ended') limit 1;
",
            [$school->id]
        );
        return collect($counter)->first();
    }


}
