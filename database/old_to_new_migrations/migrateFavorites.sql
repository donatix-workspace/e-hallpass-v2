SET FOREIGN_KEY_CHECKS = 0;
START TRANSACTION ;
INSERT INTO ehp_v2_prod.student_favorites (position, favorable_type, favorable_id, user_id, school_id, created_at, updated_at, global, section)
SELECT
    (SELECT oldSf.possition),
    (SELECT IF(oldSf.room_id IS NOT NULL, 'App\\Models\\Room', 'App\\Models\\User')),
    (SELECT IF(oldSf.room_id IS NOT NULL, oldSf.room_id, oldSf.teacher_id)),
    (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldSf.student_id limit 1),
    (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldSf.school_id limit 1),
    (SELECT NOW()),
    (SELECT NOW()),
    (SELECT 0),
    (SELECT NULL)
FROM ehallpassDB.student_favorite as oldSf;
COMMIT ;

SET FOREIGN_KEY_CHECKS  = 1;

UPDATE
    ehp_v2_prod.student_favorites
SET
    favorable_id = (
        SELECT
            to_id
        FROM
            duplicate_users
        WHERE
            from_id = favorable_id
        LIMIT 1)
WHERE
    favorable_type = 'App\\Models\\User'
    AND favorable_id IN(
        SELECT
            from_id FROM duplicate_users);
