<?php

namespace App\Models;

use App\Elastic\RecurrenceAppointmentPassesIndexConfigurator;
use App\Traits\ExtendedSearchable;
use App\Traits\PerPage;
use App\Traits\RecurrenceAppointmentPassMapping;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;
use Carbon\Carbon;

class RecurrenceAppointmentPass extends Model
{
    use HasFactory,
        PerPage,
        ExtendedSearchable,
        RecurrenceAppointmentPassMapping;

    protected $indexConfigurator = RecurrenceAppointmentPassesIndexConfigurator::class;

    protected $with = [
        'createdByUser:first_name,last_name,id,school_id,role_id,avatar,email',
        'user:first_name,last_name,id,school_id,role_id,email,avatar',
        'to',
        'from',
        'comments',
        'period:name,id'
    ];

    protected $dates = ['for_date'];

    protected $guarded = [];

    public const WITH_FEATURE_RECURRENCES = 1;
    public const WITH_CURRENT_WEEK_AND_FEATURE_RECURRENCES = 0;

    /**
     * @param DateTimeInterface $date
     * @return string
     */
    public function serializeDate(DateTimeInterface $date)
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($date, config('app.timezone'))
            ->setTimezone(optional($school)->getTimezone())
            ->format('Y-m-d H:i:s');
    }

    /**
     * @return array
     */
    public function toSearchableArray(): array
    {
        $appointmentPass = $this->toArray();

        $appointmentPass['search_date_string'] = Carbon::parse(
            $this->for_date
        )->format('m/d/Y');
        $appointmentPass['is_recurrence'] = 1;
        $appointmentPass['show_in_beyond'] = true;

        return $appointmentPass;
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return bool
     */
    public function isCoverTheTransparency()
    {
        $user = optional(auth()->user());

        return ($this->from_type === User::class &&
            $this->from_id === $user->id) ||
            ($this->to_type === User::class && $this->to_id === $user->id) ||
            $this->created_by === $user->id;
    }

    /**
     * @param $query
     * @param $user
     * @return mixed
     */
    public function scopeOfUserId($query, $user)
    {
        return $query->where('user_id', $user);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdByUser(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(
            User::class,
            'created_by',
            'id'
        )->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function from()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function to()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function period()
    {
        return $this->belongsTo(Period::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointmentPasses()
    {
        return $this->hasMany(
            AppointmentPass::class,
            'recurrence_appointment_pass_id'
        );
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function setForDateAttribute($value)
    {
        $this->attributes['for_date'] = Carbon::parse($value)->setTimezone(
            config('app.timezone')
        );
    }

    public function setRecurrenceEndAtAttribute($value)
    {
        $this->attributes['recurrence_end_at'] = Carbon::parse(
            $value
        )->setTimezone(config('app.timezone'));
    }
}
