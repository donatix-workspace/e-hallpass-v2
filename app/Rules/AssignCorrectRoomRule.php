<?php

namespace App\Rules;

use App\Models\Pin;
use App\Models\Room;
use Illuminate\Contracts\Validation\Rule;

class AssignCorrectRoomRule implements Rule
{
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $assignRoomExists = Pin::where('pinnable_type', Room::class)
            ->where('pinnable_id', $value)
            ->where('school_id', auth()->user()->school_id)
            ->exists();

        if (!$assignRoomExists) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('passes.assign.invalid.room.specified');
    }
}
