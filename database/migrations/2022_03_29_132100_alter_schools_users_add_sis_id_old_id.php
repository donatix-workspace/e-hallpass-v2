<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSchoolsUsersAddSisIdOldId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools_users', function (Blueprint $table) {
            $table->unsignedBigInteger('old_id')->nullable();
            $table->text('student_sis_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schools_users', function (Blueprint $table) {
            $table->dropColumn('old_id');
            $table->dropColumn('student_sis_id');
        });
    }
}
