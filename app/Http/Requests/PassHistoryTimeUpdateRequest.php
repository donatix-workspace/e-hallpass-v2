<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PassHistoryTimeUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'out_time' => 'required|date_format:"H:i"',
            'in_time' => 'required|date_format:"H:i"',
            'child_out_time' => 'nullable|sometimes|date_format:"H:i"',
            "child_in_time" => 'nullable|sometimes|date_format:"H:i"'
        ];
    }

    public function authorize()
    {
        return auth()->check() && !auth()->user()->isStudent();
    }
}
