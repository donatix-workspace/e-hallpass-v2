SET FOREIGN_KEY_CHECKS  = 0;
START TRANSACTION ;
INSERT INTO ehp_v2_prod.location_capacities (room_id, school_id, status, `limit`, created_at, updated_at)
SELECT
    (SELECT id FROM ehp_v2_prod.rooms WHERE old_id=oldLc.room_id limit 1),
    (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldLc.school_id limit 1),
    (SELECT oldLc.status),
    (SELECT oldLc.`limit`),
    (SELECT CONVERT_TZ(oldLc.created_at, 'America/New_York', 'UTC')),
    (SELECT CONVERT_TZ(oldLc.updated_at, 'America/New_York', 'UTC'))
FROM ehallpassDB.location_max_cap as oldLc;
COMMIT;
SET FOREIGN_KEY_CHECKS  = 1;
