<?php

namespace App\Console\Commands\Migrations;

use App\Models\Domain;
use App\Models\School;
use DB;
use Illuminate\Console\Command;

class MigrateSchoolCommand extends Command
{
    protected $signature = 'migrate_sys:school';

    protected $description = 'Migrate Schools From EhpV1 to EhpV2';

    public function handle()
    {
        $this->info('--- Migrating Schools --- ');
        $this->newLine(1);
        DB::connection('mysql')->disableQueryLog();
        DB::connection()->disableQueryLog();
        $oldDb = DB::connection('oldDB');
        $oldDb->disableQueryLog();
        $deprecatedTimezones = config('timezones.deprecated');


        $this->output->progressStart($oldDb->table('school_schedule')->count());

        $oldDb->table('school_schedule')
            ->whereIn('school_id',[1978,1979,1980,1982,1983,1981,1984,1985,1986,1987,1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007])
            ->orderBy('school_id', 'desc')
            ->chunk(100, function ($oldSchools) use ($deprecatedTimezones) {
                $oldSchools->each(function ($oldSchool) use ($deprecatedTimezones) {

                    $data = [
                        'parent' => $oldSchool->parent,
                        'name' => $oldSchool->school_name,
                        'description' => $oldSchool->description,
                        'logo' => $oldSchool->school_logo,
                        'student_domain' => $oldSchool->student_domain,
                        'school_domain' => $oldSchool->school_domain,
                        'time_format' => $oldSchool->time_format,
                        'status' => $oldSchool->status,
                        'webservice' => $oldSchool->webservice,
                        'avatar_api' => $oldSchool->avatarapi,
                        'api_key' => $oldSchool->apikey,
                        'total_user_role' => $oldSchool->total_user_role,
                        'total_users' => $oldSchool->total_users,
                        'upload_folder' => $oldSchool->upload_folder,
                        'archive_prefix' => $oldSchool->archive_prefix,
                        'csv_headers' => $oldSchool->csv_headers,
                        'csv_roles' => $oldSchool->csv_roles,
                        'if_username' => $oldSchool->if_username,
                        'is_user_csv' => $oldSchool->is_user_csv,
                        'is_sftp_image' => $oldSchool->is_sftp_image,
                        'timezone' => array_key_exists($oldSchool->school_timezone, $deprecatedTimezones) ? $deprecatedTimezones[$oldSchool->school_timezone] : $oldSchool->school_timezone,
                        'is_manual' => $oldSchool->is_manual,
                        'message' => $oldSchool->message,
                        'old_id' => $oldSchool->school_id,
                        'timezone_offset' => array_key_exists($oldSchool->school_timezone, $deprecatedTimezones) ? config('timezones.offsets.' . $deprecatedTimezones[$oldSchool->school_timezone]) : '00:00'
                    ];

                    $school = School::where('name', $oldSchool->school_name)
                        ->where('school_domain', $oldSchool->school_domain)
                        ->where('status', $oldSchool->status)
                        ->first();

                    if ($school) {
                        $school->update($data);
                        Domain::where('school_id', $school->id)->delete();
                    } else {
                        $school = School::create($data);

                    }


                    $domains = [
                        [
                            'school_id' => $school->id,
                            'name' => $oldSchool->school_domain,
                            'created_at' => now(),
                            'updated_at' => now()
                        ],
                        [
                            'school_id' => $school->id,
                            'name' => $oldSchool->student_domain,
                            'created_at' => now(),
                            'updated_at' => now()
                        ]
                    ];

                    Domain::insert($domains);
                    $this->output->progressAdvance();
                });
            });
        sleep(2);
        $this->output->progressFinish();
    }
}
