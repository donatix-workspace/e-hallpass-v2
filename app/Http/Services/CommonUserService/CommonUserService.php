<?php

namespace App\Http\Services\CommonUserService;

use App\Models\User;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use http\Env\Response;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Session;
use Str;

class CommonUserService
{
    protected $client;
    protected $clientId;
    protected $clientSecret;
    protected $baseUrl;

    public function __construct($authenticatedUser = null)
    {
        $this->baseUrl = config('commonuserservice.base_url');
        $this->clientId = config('commonuserservice.client_id');
        $this->clientSecret = config('commonuserservice.client_secret');

        $authUser = $authenticatedUser ?? auth()->user();

        $this->accessToken = optional($authUser)->access_token;
        if ($authUser->access_token_exp < time()) {
            $this->accessToken = $this->refreshUserToken($authUser);
        }

        if ($this->baseUrl !== '') {
            $this->client = new Client([
                'debug' => false,
                'base_uri' => $this->baseUrl,
                'allow_redirects' => true,
                'auth' => 'oauth',
                'cookies' => true,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Access-Control-Allow-Origin' => '*',
                    'Authorization' => 'Bearer ' . $this->accessToken
                ]
            ]);
        } else {
            throw new Exception('Please, define CUS_BASE_URL in the .env file');
        }

        $this->headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];
    }

    public function call($action = 'GET', $url = '', $body = [])
    {
        $data = [];
        if (!empty($body)) {
            $data['body'] = json_encode($body);
        }

        try {
            $response = $this->client->request($action, $url, $data);
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            //logger('POST CALL ERROR', [$responseBodyAsString]);
            return ['status' => false, 'error' => $responseBodyAsString];
        }
        return json_decode($response->getBody(), 1);
    }

    public function createUser($data)
    {
        return $this->call('POST', 'common/authentication/user', [
            'email' => $data['email'],
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'roleCollection' => $data['roleCollection'],
            'status' => $data['status'],
            'password' => $data['password'] ?? null
        ]);
    }

    public function updateUser($uuid, $data)
    {
        if (!$uuid) {
            return false;
        }
        return $this->call('PUT', "common/authentication/user/{$uuid}", $data);
    }

    public function assignUserToSchool($data)
    {
        $url = 'common/authorization/membership';
        //logger('assignUserToSchool', [$this->baseUrl . $url, $data]);
        return $this->call('POST', $url, $data);
    }

    public function modifyMembershipUser($membershipUuid, $userData)
    {
        $url = 'common/authorization/membership/' . $membershipUuid;
//        logger('modifyMembershipUser', [
//            $membershipUuid,
//            $this->baseUrl . $url,
//            $userData
//        ]);
        return $this->call('PATCH', $url, $userData);
    }

    public function deleteUser($membershipUuid)
    {
        $url = 'common/authorization/membership/' . $membershipUuid;
        //logger('deleteUser', [$this->baseUrl . $url]);
        return $this->call('DELETE', $url);
    }

    public function getTrainingDocuments($role)
    {
        $documentsResponse = $this->call(
            'GET',
            "common/support-document?role={$role}&pageSize=1000"
        );

        if ($documentsResponse) {
            return array_filter($documentsResponse, function ($document) {
                return array_filter($document['apps'], function ($app) {
                    return $app['name'] == 'E-Hall Pass';
                });
            });
        }
        return [];
    }

    public function getTrainingVideos($role)
    {
        $videoResponse = $this->call(
            'GET',
            "common/support-video?role={$role}&pageSize=1000"
        );
        if ($videoResponse) {
            return array_filter($videoResponse, function ($video) {
                return array_filter($video['apps'], function ($app) {
                    return $app['name'] == 'E-Hall Pass';
                });
            });
        }

        return [];
    }

    public function getAnnouncements($role, $date, $schoolUuid)
    {
        $uuid = config('commonuserservice.ehp_client_uuid');
        $response = $this->call(
            'GET',
            "common/announcement?status=1&role={$role}&todayDate={$date}&app={$uuid}&school={$schoolUuid}"
        );
        if ($response) {
            return array_filter($response, function ($announcement) {
                return $announcement['app']['name'] == 'E-Hall Pass';
            });
        }
        return [];
    }

    public function refreshToken()
    {
        $refreshClient = new Client();

        $response = $refreshClient->request(
            'POST',
            $this->baseUrl . '/authentication/token/refresh',
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->accessToken,
                    'Content-Type' => 'application/json',
                    'Accept' => '*/*'
                ],
                'body' => json_encode([
                    'clientId' => $this->clientId,
                    'clientSecret' => $this->clientSecret
                ])
            ]
        );

        $this->accessToken = json_decode($response->getBody(), 1)['accessToken'];
        Session::put('accessToken', $this->accessToken);
        return $this->accessToken;
    }

    public function refreshUserToken($user)
    {
        $refreshClient = new Client();
        try {
            $response = $refreshClient->request(
                'POST',
                $this->baseUrl . '/authentication/token/refresh',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $user->access_token,
                        'Content-Type' => 'application/json',
                        'Accept' => '*/*'
                    ],
                    'body' => json_encode([
                        'clientId' => $this->clientId,
                        'clientSecret' => $this->clientSecret
                    ])
                ]
            );
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
//            logger('REFRESH TOKEN', [
//                "endpoint" => $this->baseUrl . '/authentication/token/refresh',
//                "headers" => [
//                    'Authorization' => 'Bearer ' . $user->access_token,
//                    'Content-Type' => 'application/json',
//                    'Accept' => '*/*'
//                ],
//                'body' => [
//                    'clientId' => $this->clientId,
//                    'clientSecret' => $this->clientSecret
//                ],
//                "error" => $responseBodyAsString
//
//            ]);
            return false;
        }

        $this->accessToken = json_decode($response->getBody(), 1)['accessToken'];

        $decodedAccessToken = \App\Http\Services\CommonUserService\JWT::decode($this->accessToken, config('commonuserservice.public_key'), ['RS256']);
        $user->update([
            'access_token' => $this->accessToken,
            'access_token_exp' => $decodedAccessToken->exp ?? 0
        ]);
        Session::put('accessToken', $this->accessToken);
        return $this->accessToken;
    }

    public function listTimeZones($filter)
    {
        try {
            $response = $this->client->request(
                'GET',
                "/common/utc-offset/time-zone?page=1&pageSize=100000&filter={$filter}"
            );
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
//            logger('POST CALL ERROR', [$responseBodyAsString]);
            return false;
        }
        return json_decode($response->getBody(), 1);
    }

    public function listDistricts($filter)
    {
        try {
            $response = $this->client->request(
                'GET',
                "/common/district?page=1&pageSize=100000&filter={$filter}"
            );
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
//            logger('POST CALL ERROR', [$responseBodyAsString]);
            return false;
        }
        return json_decode($response->getBody(), 1);
    }

    public function createDistrict($name, $state)
    {
        try {
            $response = $this->client->request('POST', '/common/district', [
                'body' => json_encode([
                    'name' => $name,
                    'l10nUsTerritorialSubdivisionCodeAnsi' => $state,
                    'status' => 1
                ])
            ]);
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
//            logger('POST CALL ERROR', [$responseBodyAsString]);
            return false;
        }
        return json_decode($response->getBody(), 1);
    }

    public function createSchool($districtUuid, $body)
    {
        try {
            $logBody = $body;
            $logBody['logo'] = 'base64';
//            logger('CREATE SCHOOL REQUEST', $logBody);
            $response = $this->client->request(
                'POST',
                "/common/district/{$districtUuid}/school",
                [
                    'body' => json_encode($body)
                ]
            );
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
//            logger('POST CALL ERROR', [$responseBodyAsString]);
            return false;
        }
        return json_decode($response->getBody(), 1);
    }

    public function getSchool($schoolUuid)
    {
        try {
//            logger('GET SCHOOL', [$schoolUuid]);
            $response = $this->client->request(
                'GET',
                "/common/district/school/{$schoolUuid}");
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
//            logger('GET SCHOOL ERROR', [$responseBodyAsString]);
            return false;
        }
        return json_decode($response->getBody(), 1);
    }

    public function updateSchool($schoolUuid, $body)
    {
        try {
            $logBody = $body;
            $logBody['url'] = $schoolUuid;
            $logBody['logo'] = 'base64';
//            logger('UPDATE SCHOOL REQUEST', $logBody);
            $response = $this->client->request(
                'PUT',
                "/common/district/school/{$schoolUuid}",
                [
                    'body' => json_encode($body)
                ]
            );
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
//            logger('UPDATE SCHOOL ERROR', [$responseBodyAsString]);
            return false;
        }
        return json_decode($response->getBody(), 1);
    }

    public function files($schoolUuid, $type = 0, $page = 1, $perPage = 10)
    {
        $uri = "/common/district/school/{$schoolUuid}/file?type={$type}&page={$page}&pageSize={$perPage}";
        try {
            $response = $this->client->request('GET', $uri);
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
//            logger('GET CALL ERROR', [$responseBodyAsString]);
            return false;
        }
        return json_decode($response->getBody(), 1);
    }
}
