<?php

if (!function_exists('getRoleNameByIdFromV1')) {
    /**
     * @param $roleId
     * @return int|string
     */
    function getRoleNameByIdFromV1($roleId)
    {
        $rolesV1 = array_flip([
            'NONE' => -2,
            'SUPERADMIN' => 1,
            'ADMIN' => 2,
            'TEACHER' => 3,
            'STUDENT' => 4,
            'STAFF' => 5,
            'SYSTEM' => 7,
            'NOROLE' => 8
        ]);

        return array_key_exists($roleId, $rolesV1)
            ? $rolesV1[$roleId]
            : $roleId;
    }
}

if (!function_exists('convertElasticSearchQuery')) {
    /**
     * Convert user string to valid elastic search query
     * We replace most of the unique elastic characters with empty strings.
     * Usage: convertElasticQuery('Room 1 !!');
     *
     * @param string $queryString
     * @param bool $convertInt
     * @param string $operator
     * @return array|string|string[]
     */
    function convertElasticQuery(
        string $queryString,
        bool $convertInt = false,
        string $operator = 'AND'
    ) {
        $operators = ['AND', 'OR', '|'];

        if (
            request()->has('operator') &&
            in_array(request()->get('operator'), $operators)
        ) {
            $operator = request()->get('operator');
        }

        if ($queryString === config('scout_elastic.all_records')) {
            return $queryString;
        }

        $queryString = str_replace('!!', '', $queryString);
        $queryString = str_replace('!', '', $queryString);
        $queryString = str_replace('%', '', $queryString);
        $queryString = str_replace('&', '', $queryString);
        $queryString = str_replace('+', ' ', $queryString);
        $queryString = str_replace('/', '\/', $queryString);

        $queryStringTrimmed = trim($queryString);
        $queryStringTrimmed = str_replace('"', '*', $queryStringTrimmed);
        $multiQueryArray = [];

        if (
            !str_starts_with(
                $queryStringTrimmed,
                config('scout_elastic.all_records')
            )
        ) {
            $queryString = explode(':', $queryStringTrimmed, 2);
            if (isset($queryString[1])) {
                $stringWithoutPrefixes = str_replace('*', '', $queryString[1]);

                if (is_numeric($stringWithoutPrefixes) && $convertInt) {
                    $queryString[1] = str_replace('*', '', $queryString[1]);
                }
            }

            $queryString = str_replace(' ', '?', $queryString);
        }

        if (
            str_starts_with(
                $queryStringTrimmed,
                config('scout_elastic.all_records')
            )
        ) {
            $queryString = addcslashes($queryStringTrimmed, ':');
            $queryString = str_replace(' ', '?', $queryString);
        }

        $finalQuery = $queryString;

        // Check if there's more than 1 colon.
        if (substr_count($queryStringTrimmed, ':') > 1) {
            if (str_contains($queryStringTrimmed, ',')) {
                // Exploding multi columns
                $queryString = explode(',', $queryStringTrimmed);

                foreach ($queryString as $queryAttributes) {
                    $attribute = explode(':', $queryAttributes, 2);
                    $searchValueParsed = addcslashes($attribute[1], ':');
                    $stringWithoutPrefixes = str_replace(
                        '*',
                        '',
                        $searchValueParsed
                    );
                    if (is_numeric($stringWithoutPrefixes)) {
                        $searchValueParsed = str_replace(
                            '*',
                            '',
                            $searchValueParsed
                        );
                    }
                    $searchValueParsed = str_replace(
                        ' ',
                        '?',
                        $searchValueParsed
                    );

                    $multiQueryArray[] =
                        $attribute[0] . ':' . $searchValueParsed;
                }
            }

            //Escape colon char for prevent invalid query exception.
            $queryString[1] = addcslashes($queryString[1], ':');
            $queryString[1] = str_replace(' ', '?', $queryString[1]);

            $stringWithoutPrefixes = str_replace('*', '', $queryString[1]);
            if (is_numeric($stringWithoutPrefixes)) {
                $queryString[1] = str_replace('*', '', $queryString[1]);
            }

            if (count($multiQueryArray)) {
                $finalQuery = $multiQueryArray;
            } else {
                $finalQuery = $queryString;
            }
        }

        if (is_array($multiQueryArray) && count($multiQueryArray)) {
            return implode(" $operator ", $multiQueryArray);
        }

        return is_array($finalQuery) ? implode(':', $finalQuery) : $finalQuery;
    }

    if (!function_exists('elasticWhereInRawString')) {
        function elasticWhereInRawString(string $column, array $values): string
        {
            return "$column:" . implode(" | $column:", $values);
        }
    }

    if (!function_exists('arrayDuplicates')) {
        /**
         * @param array $array
         * @return bool
         */
        function arrayDuplicates(array $array): bool
        {
            return count($array) !== count(array_unique($array));
        }
    }
}
