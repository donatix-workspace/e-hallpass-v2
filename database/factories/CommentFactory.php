<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class CommentFactory extends Factory
{
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'commentable_type' => $this->faker->word,
            'commentable_id' => $this->faker->randomNumber(),
            'school_id' => function () {
                return School::factory()->create()->id;
            },
            'comment' => $this->faker->word,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'user_id' => function () {
                return User::factory()->create()->id;
            },
        ];
    }
}
