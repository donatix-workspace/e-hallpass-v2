<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    public function rules()
    {
        return [
            'type' => 'string|required|in:user,teacher,staff,admin,student,combined,locations,adults,my_assigned,appointment,kiosk',
            'search_query' => 'string|min:1|nullable'
        ];
    }

    public function authorize()
    {
        //TODO:: Find Logic for this otherwise everything can be bypassed

        return ($this->request->get('type') == 'kiosk') ? true : auth()->check();
    }
}
