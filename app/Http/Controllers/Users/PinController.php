<?php

namespace App\Http\Controllers\Users;

use App\Events\UserUnlockedEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\PinAttemptCollection;
use App\Models\Pin;
use App\Models\PinAttempt;
use App\Notifications\Push\StudentNotification;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Notification;

/**
 * @group User lock functionality (Unlock).
 */
class PinController extends Controller
{
    /**
     * Get pin attempts timestamp information
     *
     * @authenticated
     * @apiResourceModel App\Models\PinAttempt
     * @apiResourceCollection App\Http\Resources\PinAttemptCollection
     *
     * @return PinAttemptCollection
     * @throws AuthorizationException
     */
    public function index()
    {
        $user = auth()->user();

        $this->authorize('viewAny', Pin::class);

        $pinAttempts = PinAttempt::whereUserId($user->id)
            ->where('school_id', $user->school_id)
            ->where('status', Pin::ACTIVE)
            ->orderBy('created_at', 'asc')
            ->get(['id', 'created_at']);

        return new PinAttemptCollection($pinAttempts);
    }

    /**
     * Unlock the user with teacher pin
     *
     * @bodyParam teacher_pin string required The pin of the teacher
     *
     * @authenticated
     * @param Request $request
     * @return JsonResponse
     * @response {
     *      data: {},
     *      message: 'users.unlocked.successfully',
     *      status: 'success',
     * }
     *
     * @response status=422 scenario="Wrong pin" {
     *      data: {},
     *      message: 'users.unlocked.invalid',
     *      status: 'fail',
     * }
     * @throws AuthorizationException
     */
    public function unlock(Request $request)
    {
        $this->authorize('update', Pin::class);

        $user = auth()->user();

        $request->validate(['teacher_pin' => 'required|max:15']);

        $teacherPinExist = Pin::fromSchoolId($user->school_id)
            ->fromType(Pin::USER_TYPE)
            ->where('pin', $request->input('teacher_pin'))
            ->exists();

        if ($teacherPinExist) {
            $pinAttemptsUpdate = PinAttempt::fromSchoolId($user->school_id)
                ->whereUserId($user->id)
                ->whereStatus(Pin::ACTIVE)
                ->update([
                    'status' => Pin::INACTIVE
                ]);

            event(new UserUnlockedEvent($user->id));

            //            Notification::send(
            //                $user,
            //                new StudentNotification('UserUnlockedEvent')
            //            );

            return response()->json(
                [
                    'data' => [],
                    'message' => __('users.unlocked.successfully'),
                    'status' => __('success')
                ],
                200
            );
        }

        return response()->json(
            [
                'data' => [],
                'message' => __('users.unlocked.invalid'),
                'status' => __('success')
            ],
            422
        );
    }
}
