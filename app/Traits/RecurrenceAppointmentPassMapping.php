<?php

namespace App\Traits;

trait RecurrenceAppointmentPassMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'id' => [
                'type' => 'long'
            ],
            'recurrence_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 'does not repeat'
            ],
            'show_in_beyond' => [
                'type' => 'boolean'
            ],
            'recurrence_end_at' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||yyyy-MM-dd HH:mm'
            ],
            'user_id' => [
                'type' => 'long'
            ],
            'school_id' => [
                'type' => 'long'
            ],
            'is_recurrence' => [
                'type' => 'long'
            ],
            'created_by' => [
                'type' => 'long'
            ],
            'from_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'to_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'created_by_user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'email' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'period' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'email' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'for_date' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||yyyy-MM-dd HH:mm'
            ],
            'search_date_string' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => '-'
            ],
            'reason' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 'NULL'
            ],
            'from_id' => [
                'type' => 'long'
            ],
            'to_id' => [
                'type' => 'long'
            ],
            'to' => [
                'properties' => [
                    'id' => [
                        'type' => 'long'
                    ],
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'from' => [
                'properties' => [
                    'id' => [
                        'type' => 'long'
                    ],
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ]
        ]
    ];
}
