<?php

namespace App\Policies;

use App\Models\ActivePassLimit;
use App\Models\AutoPass;
use App\Models\AutoPassLimit;
use App\Models\AutoPassPreference;
use App\Models\BlockPass;
use App\Models\LocationCapacity;
use App\Models\Pass;
use App\Models\PassLimit;
use App\Models\Room;
use App\Models\School;
use App\Models\Unavailable;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Contracts\Auth\Authenticatable;

class PassPolicy
{
    use HandlesAuthorization;

    public const STUDENT_PASS = 'student_pass';
    public const PROXY_PASS = 'proxy_pass';
    public const KIOSK_PASS = 'kiosk_pass';

    private const ACTIVE = 1;
    private const DEACTIVE = 0;

    /**
     * Determine whether the user can view any models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    public function cancel(User $user, Pass $pass)
    {
        return $user->isStudent() &&
            $user->school_id === $pass->school_id &&
            $user->id === $pass->user_id;
    }

    /**
     * @param User $user
     * @param Pass $pass
     * @return bool
     */
    public function cancelAdmin(User $user, Pass $pass)
    {
        return $user->school_id === $pass->school_id && !$user->isStudent();
    }

    /**
     * @param User $user
     * @param Pass $pass
     * @return bool
     */
    public function historyAdmin(User $user)
    {
        return !$user->isStudent();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function history(User $user)
    {
        return $user->isStudent();
    }

    public function updateAdmin(User $user, Pass $pass)
    {
        return !$user->isStudent() && $user->school_id === $pass->school_id;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\User $user
     * @param Pass|null $pass
     * @return mixed
     */
    public function view(User $user, ?Pass $pass)
    {
        if ($pass === null) {
            return false;
        }

        return $user->school_id === $pass->school_id && $user->isStudent();
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isStudent();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function createProxy(User $user)
    {
        return $user->isAdmin() || $user->isTeacher() || $user->isStaff();
    }

    /**
     * @param $user
     * @param string $type see *_PASS constants
     * @return boolean
     */
    public static function checkBlockPass(
        $user,
        $type = self::STUDENT_PASS
    ): bool {
        return BlockPass::fromSchoolId($user->school_id)
            ->where('from_date', '<=', Carbon::now()->toDateTimeString())
            ->where('to_date', '>=', Carbon::now()->toDateTimeString())
            ->where($type, self::ACTIVE)
            ->exists();
    }

    /**
     * @param $blockPass
     * @param $autoPassLimit
     * @param $staffUnavailability
     * @param $locationCapacity
     * @param $adultOptionAllowFrom
     * @param $adultOptionAllowTo
     * @param $schoolOrStudentPassLimit
     * @param $schoolLocationsRestrictionsIds
     * @param $roomTo
     * @param $activePassLimit
     * @return array|boolean
     */
    public static function policyValidation(
        $blockPass,
        $autoPassLimit,
        $staffUnavailability,
        $locationCapacity,
        $adultOptionAllowFrom,
        $adultOptionAllowTo,
        $schoolOrStudentPassLimit,
        $schoolLocationsRestrictionsIds,
        $roomTo,
        $activePassLimit
    ) {
        if ($adultOptionAllowTo || $adultOptionAllowFrom) {
            return [
                'data' => [
                    'from' => $adultOptionAllowFrom,
                    'to' => $adultOptionAllowTo
                ],
                'message' => __('passes.user.not.accept.location.to.him'),
                'status' => __('fail')
            ];
        }

        if ($schoolLocationsRestrictionsIds->contains(optional($roomTo)->id)) {
            return [
                'data' => [],
                'message' => __('passes.restricted.room'),
                'status' => __('fail')
            ];
        }

        if ($activePassLimit['status']) {
            return [
                'data' => [],
                'message' => __('passes.active.limit.exceeded'),
                'status' => __('fail')
            ];
        }

        if ($schoolOrStudentPassLimit['status']) {
            return [
                'data' => [],
                'message' => __('passes.limit.exceeded', [
                    'current' => $schoolOrStudentPassLimit['remain']['current'],
                    'max' => $schoolOrStudentPassLimit['remain']['max']
                ]),
                'status' => __('fail')
            ];
        }
        if ($locationCapacity['status']) {
            return [
                'data' => [],
                'message' => __('passes.location.max.capacity', [
                    'count ' => $locationCapacity['count']
                ]),
                'status' => __('fail')
            ];
        }

        if ($staffUnavailability) {
            return [
                'data' => [],
                'message' => __('passes.teacher.unavailable'),
                'status' => __('fail')
            ];
        }

        if ($blockPass) {
            return [
                'data' => [],
                'message' => __('passes.blockpass.active'),
                'status' => __('fail')
            ];
        }

        if ($autoPassLimit['status']) {
            return [
                'data' => [],
                'message' => __('passes.auto.limit.exceeded', [
                    'count' => $autoPassLimit['count']
                ]),
                'status' => __('fail')
            ];
        }

        return false;
    }

    /**
     * @param $user
     * @return bool
     */
    public static function checkIfAdultNotAcceptPassToHim($user)
    {
        if ($user === null) {
            return false;
        }
        return $user->allow_passes_to_me === 0;
    }

    /**
     * @param User|null $user
     * @param Room|null $room
     * @return array
     */
    public static function checkAutoPassLimit(?User $user, ?Room $room)
    {
        $activeAutoPassesCount = 0;
        // Find the teacher with autopass limit
        $autoPassLimit = AutoPassLimit::ofUserId(optional($user)->id)
            ->fromSchoolId(optional($user)->school_id)
            ->withValidLimit()
            ->first(); // We use first here to get limit above. (Record || Null)

        $autoPassPreferencesRooms = AutoPassPreference::with(
            'autoPass:id,room_id'
        )
            ->ofUserId(optional($user)->id)
            ->fromSchoolId(optional($user)->school_id)
            ->get(['auto_pass_id', 'mode', 'school_id'])
            ->pluck('autoPass.room_id')
            ->toArray();

        // Check if it has autopass limit
        if ($autoPassLimit) {
            $activeAutoPassesCount = 0;

            // Count the active passes
            if (in_array(optional($room)->id, $autoPassPreferencesRooms)) {
                $activeAutoPassesCount = Pass::onWriteConnection()
                    ->fromSchoolId(optional($user)->school_id)
                    ->wherePassStatus(Pass::ACTIVE)
                    ->whereFromType(User::class)
                    ->where('from_id', optional($user)->id)
                    ->whereToType(Room::class)
                    ->whereIntegerInRaw('to_id', $autoPassPreferencesRooms)
                    ->count();
            }
        }

        $status =
            optional($autoPassLimit)->limit === null
                ? false
                : $activeAutoPassesCount >= optional($autoPassLimit)->limit;

        if (optional($autoPassLimit)->limit === 0) {
            $status = false;
        }

        return [
            'status' => $status,
            'count' => $activeAutoPassesCount
        ];
    }

    /**
     * @param $type
     * @param $toId
     * @return boolean
     */
    public static function checkUnavailable($type, $toId)
    {
        return Unavailable::active()
            ->fromSchoolId(auth()->user()->school_id)
            ->where('unavailable_type', $type)
            ->where('unavailable_id', $toId)
            ->where('from_date', '<=', Carbon::now()->toDateTimeString())
            ->where('to_date', '>=', Carbon::now()->toDateTimeString())
            ->where('canceled_for_today_at', null)
            ->exists();
    }

    /**
     * @param $toId
     * @return array
     */
    public static function checkLocationCapacity($toId)
    {
        $locationCapacity = LocationCapacity::active()
            ->fromSchoolId(auth()->user()->school_id)
            ->where('limit', '!=', -1)
            ->where('room_id', $toId)
            ->first();

        // Get all active passes to the room
        $passesToTheRoomCount = Pass::where('pass_status', 1)
            ->fromSchoolId(auth()->user()->school_id)
            ->whereToType(Room::class)
            ->where('to_id', $toId)
            ->whereDate('created_at', Carbon::today())
            ->where('completed_at', null)
            ->where('expired_at', null)
            ->where('canceled_at', null)
            ->count();

        return [
            'status' =>
                optional($locationCapacity)->limit !== null &&
                $passesToTheRoomCount >= optional($locationCapacity)->limit,
            'count' => $passesToTheRoomCount
        ];
    }

    /**
     * @return bool[]|false[]
     */
    public static function checkActivePassLimit(bool $kiosk = false): array
    {
        $activePassLimit = ActivePassLimit::canCreatePasses(
            $kiosk ? self::KIOSK_PASS : self::STUDENT_PASS
        );

        if ($activePassLimit) {
            return [
                'status' => true
            ];
        }

        return [
            'status' => false
        ];
    }

    /**
     * @param bool|null $kiosk
     * @return array
     */
    public static function checkPassLimit(?bool $kiosk = false): array
    {
        $passLimitOfGradeYear = PassLimit::whereType(School::class)
            ->where('limitable_id', auth()->user()->school_id)
            ->where('archived', '!=', PassLimit::ARCHIVED)
            ->where('grade_year', '!=', null)
            ->whereDate('from_date', '<=', Carbon::now())
            ->whereDate('to_date', '>=', Carbon::now())
            ->first();
        // Get the pass limit of the school
        $passLimitOfSchool = PassLimit::whereType(School::class)
            ->where('limitable_id', auth()->user()->school_id)
            ->where('archived', '!=', PassLimit::ARCHIVED)
            ->where('grade_year', null)
            ->whereDate('from_date', '<=', Carbon::now())
            ->whereDate('to_date', '>=', Carbon::now())
            ->first();

        // Get the pass limit of the user
        $passLimitOfUser = PassLimit::whereType(User::class)
            ->where(
                'limitable_id',
                !$kiosk ? auth()->user()->id : auth()->user()->user_id
            )
            ->whereDate('from_date', '<=', Carbon::now())
            ->where('archived', '!=', PassLimit::ARCHIVED)
            ->whereDate('to_date', '>=', Carbon::now())
            ->first();

        // Count user passes
        $userPassesCount = Pass::fromSchoolId(auth()->user()->school_id)
            ->ofUserId(!$kiosk ? auth()->user()->id : auth()->user()->user_id)
            ->where('parent_id', null)
            ->whereDate('created_at', Carbon::today())
            ->where('type', '!=', Pass::APPOINTMENT_PASS)
            ->where(function ($query) {
                $query
                    ->where('completed_by', '!=', null)
                    ->orWhere('completed_at', '!=', null);
            })
            ->count();

        // If the school has pass limit and it's smaller than user limit
        // ignore the teacher pass limit
        if ($passLimitOfSchool) {
            if (
                optional($passLimitOfSchool)->limit <
                    optional($passLimitOfUser)->limit ||
                optional($passLimitOfUser)->limit === null
            ) {
                return [
                    'status' =>
                        !(optional($passLimitOfSchool)->limit === null) &&
                        $userPassesCount >= optional($passLimitOfSchool)->limit,
                    'remain' => [
                        'current' => $userPassesCount,
                        'max' =>
                            optional($passLimitOfSchool)->limit === null
                                ? 0
                                : optional($passLimitOfSchool)->limit
                    ],
                    'from_date' => optional($passLimitOfSchool)->from_date,
                    'to_date' => optional($passLimitOfSchool)->to_date
                ];
            }
        }

        if ($passLimitOfGradeYear) {
            $gradeYears = collect(
                json_decode($passLimitOfGradeYear->grade_year, true)
            );
            if (
                optional($passLimitOfGradeYear)->limit <
                    optional($passLimitOfUser)->limit ||
                optional($passLimitOfUser)->limit === null
            ) {
                if ($gradeYears->contains(auth()->user()->grade_year)) {
                    return [
                        'status' =>
                            !(
                                optional($passLimitOfGradeYear)->limit === null
                            ) &&
                            $userPassesCount >= $passLimitOfGradeYear->limit,
                        'remain' => [
                            'current' => $userPassesCount,
                            'max' =>
                                optional($passLimitOfGradeYear)->limit === null
                                    ? 0
                                    : optional($passLimitOfGradeYear)->limit
                        ],
                        'from_date' => optional($passLimitOfGradeYear)
                            ->from_date,
                        'to_date' => optional($passLimitOfGradeYear)->to_date
                    ];
                }
            }
        }

        return [
            'status' =>
                !(optional($passLimitOfUser)->limit === null) &&
                $userPassesCount >= optional($passLimitOfUser)->limit,
            'remain' => [
                'current' => $userPassesCount,
                'max' =>
                    optional($passLimitOfUser)->limit === null
                        ? 0
                        : optional($passLimitOfUser)->limit
            ],
            'from_date' => optional($passLimitOfSchool)->from_date,
            'to_date' => optional($passLimitOfSchool)->to_date
        ];
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Pass $pass
     * @return mixed
     */
    public function update(User $user, Pass $pass)
    {
        return $user->isStudent() &&
            $user->school_id === $pass->school_id &&
            $user->id === $pass->user_id;
    }

    /**
     * @param User $user
     * @param Pass $pass
     * @return bool
     */
    public function autoApprove(User $user, Pass $pass)
    {
        return $user->isStudent() &&
            $user->school_id === $pass->school_id &&
            $pass->user_id === $user->id;
    }

    public function autoCheckIn(User $user, Pass $pass)
    {
        return $user->isStudent() &&
            $pass->to_type === Room::class &&
            $pass->user_id === $user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Pass $pass
     * @return mixed
     */
    public function delete(User $user, Pass $pass)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Pass $pass
     * @return mixed
     */
    public function restore(User $user, Pass $pass)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Pass $pass
     * @return mixed
     */
    public function forceDelete(User $user, Pass $pass)
    {
        //
    }
}
