<!DOCTYPE html>
<html lang="en">
<head>
    <title>Cross check | E-Hallpass V2</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="inset-0">
<div class="flex flex-nowrap justify-center justify-around  m-8">
    <div class="container m-0">
        <div class="grid grid-cols-2 gap-2">

            <div class="box-border shadow-lg rounded border-solid w-128 h=128 bg-cyan-500 grid grid-cols-2 gap-2">
                <div class="content pl-6 pt-2 pr-6">
                    <h1 class="text-stone-100">V1</h1>

                    <p>{{$oldSchool->school_id}}</p>
                    <p>{{$oldSchool->school_name}}</p>
                    <p>{{$oldSchool->school_domain}} | {{$oldSchool->student_domain}}</p>
                    <p>Status {{$oldSchool->status? 'Active' : 'Inactive'}}</p>
                    <p>Description {{$oldSchool->description}}</p>

                    <ul>
                        @foreach($oldSchool->modules as $module)
                            <li>{{$module['name']}} - {{$module['status']? 'Active' : 'Inactive'}}</li>
                        @endforeach
                    </ul>

                </div>
                <div class="content pl-6 pt-2 pr-6">
                    <p>
                        <img width="100" src="{{$oldSchool->logo}}" alt="">
                    </p>
                    @if($school)
                        <h1 class="text-stone-100">V1 Users</h1>
                        <p>Admin - {{$v1Counters->admin}}</p>
                        <p>Teacher - {{$v1Counters->teacher}}</p>
                        <p>Staff - {{$v1Counters->staff}}</p>
                        <p>Student - {{$v1Counters->student}}</p>
                    @endif
                </div>
            </div>
            <div class="box-border shadow-lg rounded border-solid w-128 h=128 bg-cyan-500 grid grid-cols-2 gap-2">
                @if($school)
                    <div class="content pl-6 pt-2 pr-6">
                        <h1 class="text-stone-100">V2</h1>

                        <p>{{$school->id}} | {{$school->cus_uuid}} </p>
                        <p>{{$school->name}}</p>
                        <p>
                            @foreach($school->domains as $domain)
                                {{$domain->name}} |
                            @endforeach
                        </p>
                        <p>Status {{$school->status? 'Active' : 'Inactive'}}</p>
                        <p>Description {{$school->description}} | Zip: {{$school->zip}} | State: {{$school->state}} </p>
                        <ul>
                            @foreach($v2Modules as $module)
                                <li>{{$module->module->display_name}}
                                    - {{$module->status? 'Active' : 'Inactive' }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="content pl-6 pt-2 pr-6">
                        <p>
                            <img width="100" src="{{$school->logo}}" alt="">
                        </p>
                        <h1 class="text-stone-100">V2 Users</h1>
                        <p>Admin - {{$v2Counters->admin}}</p>
                        <p>Teacher - {{$v2Counters->teacher}}</p>
                        <p>Staff - {{$v2Counters->staff}}</p>
                        <p>Student - {{$v2Counters->student}}</p>
                    </div>
                @endif
            </div>


        </div>
    </div>
</div>
<hr/>
<div class="flex flex-nowrap justify-center justify-around  m-8">
    <div class="container m-0">
        <div>

            <div class="box-border shadow-lg rounded border-solid w-128 h=128 bg-cyan-500 py-3">
                <div class="content pl-6 pt-2 pr-6">
                    <h1 class="text-stone-100">Actions</h1>

                    <br>
                    <p class="mt-3 mb-10">

                        <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                           href="{{route('api.v5.crosscheck.schools.verify',[$oldSchool,1])}}">Mark Ready</a>

                        <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                           href="{{route('api.v5.crosscheck.schools.verify',[$oldSchool,0])}}">Mark Not Ready</a>
                        @if($school)
                            <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                               href="{{route('api.v5.crosscheck.schools.image',$oldSchool)}}">Migrate Image</a>
                            <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                               href="{{route('api.v5.crosscheck.schools.modules',$oldSchool)}}">Migrate Modules</a>

                            <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                               href="{{route('api.v5.crosscheck.show',$school->id)}}" target="_blank">User Details</a>

                            <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                               href="{{route('api.v5.crosscheck.migrateAll',[$school->id])}}">Migrate All Users</a>
                        @endif

                    </p>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- V1 -->
<div class="results m-10">
    @if (session()->has('message'))
        <div class="flex flex-nowrap justify-center m-2">
            {!! json_encode(session()->get('message')) !!}
        </div>
    @endif
</div>
<hr/>

</body>
</html>

