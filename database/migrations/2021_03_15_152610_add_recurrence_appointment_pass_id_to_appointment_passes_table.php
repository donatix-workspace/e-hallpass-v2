<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecurrenceAppointmentPassIdToAppointmentPassesTable extends Migration
{
    public function up()
    {
        Schema::table('appointment_passes', function (Blueprint $table) {
            $table->unsignedBigInteger('recurrence_appointment_pass_id')->nullable();
            $table->foreign('recurrence_appointment_pass_id')->references('id')->on('recurrence_appointment_passes');
        });
    }

    public function down()
    {
        Schema::table('appointment_passes', function (Blueprint $table) {
            $table->dropForeign('appointment_passes_recurrence_appointment_pass_id_foreign');

        });
    }
}
