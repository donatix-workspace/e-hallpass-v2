<?php


namespace App\Traits;


use Carbon\Carbon;

trait DefaultRecurrence
{
    /**
     * @return bool
     */
    public function isDaily()
    {
        $today = Carbon::today()->dayOfWeek;

        return $this->recurrence_type === config('recurrences.daily');
    }

    /**
     * @return bool
     */
    public function isWeekly()
    {
        $endOfWeek = $this->from_date->endOfWeek();

        return $this->recurrence_type === config('recurrences.weekly')
            && $endOfWeek->isPast();
    }

    /**
     * @return bool
     */
    public function isMonthly()
    {
        $endOfMonth = $this->from_date->endOfMonth();

        return $this->recurrence_type === config('recurrences.monthly')
            && $endOfMonth->isPast();
    }

    /**
     * @return void
     */
    public function updateByRecurrence()
    {
        // Variable for daily recurrence
        $fromDate = $this->from_date;
        $toDate = $this->to_date;

        // If from_date or to_date is weekend and recurrence is daily
        // skip this days and set it to next monday.
        if ($this->recurrence_type === config('recurrences.daily')) {
            $fromDate = $this->from_date->addDay();
            $toDate = $this->to_date->addDay();

            if ($fromDate->isWeekend()) {
                $fromDate->next(Carbon::MONDAY);
            }
            if ($toDate->isWeekend()) {
                $toDate->next(Carbon::MONDAY);
            }
        }

        // If the recurrence is monthly add month
        if ($this->recurrence_type === config('recurrences.monthly')) {
            $fromDate = $this->from_date->addMonth();
            $toDate = $this->to_date->addMonth();
        }

        // If the recurrence is weekly add week
        if ($this->recurrence_type === config('recurrences.weekly')) {
            $fromDate = $this->from_date->addWeek();
            $toDate = $this->to_date->addWeek();
        }

        $this->update([
            "from_date" => "$fromDate",
            "to_date" => "$toDate"
        ]);
    }
}
