<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Tests\TestCase;

class TimezoneTest extends TestCase
{

    /**
     * @test
     * A carbon is different when date_default_timezone function is used before the execution
     * @return void
     */
    public function a_carbon_is_different_when_date_default_timezone_function_is_used_before_the_execution()
    {
        date_default_timezone_set('Asia/Aden');
        $this->assertNotEquals(
            Carbon::now()->format('Y-m-d H:i:s'),
            Carbon::now('Europe/Sofia')->addHours(1)->format('Y-m-d H:i:s')
        );
    }
}
