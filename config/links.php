<?php

return [
    'reference_cards' => [
        'student' => 'https://docs.google.com/document/d/e/2PACX-1vQ7t0NJwtt5caUEhGIRcNEW1fT6mj-fYIm_UTm-o9EaV5uiwVzFy0e4A-FlYT1XjW9qsWU1MIZ9hu5w/pub',
        'admin' => 'https://docs.google.com/document/d/e/2PACX-1vRFUdO7Q6fjQ42yvZ3NZAP2uzYx2-iwLI5EHJeWwEwrV7-ZXlzJVqLMDn9Rj3b5eapK7KWRX77_5Rn1/pub',
        'teacher'=>'https://docs.google.com/document/d/e/2PACX-1vRFUdO7Q6fjQ42yvZ3NZAP2uzYx2-iwLI5EHJeWwEwrV7-ZXlzJVqLMDn9Rj3b5eapK7KWRX77_5Rn1/pub',
        'staff'=>'https://docs.google.com/document/d/e/2PACX-1vRFUdO7Q6fjQ42yvZ3NZAP2uzYx2-iwLI5EHJeWwEwrV7-ZXlzJVqLMDn9Rj3b5eapK7KWRX77_5Rn1/pub'
    ],
    'video_tutorials' => [
        'student' => 'https://docs.google.com/document/d/e/2PACX-1vRfhtna4v6hBkhBZilpzDEnCjvWQWWTDRnhmkvseoH6FZvmh97ZhEjXutIKAS6Teo5M2KFWiXtFoLyN/pub',
        'admin' => 'https://docs.google.com/document/d/e/2PACX-1vQcZPg9TaN9mHbzul6MnBnutSt18XMdIjLx4E4EI4zF8A4I2nIOs5IYA63QkNUGLukg_mTwu2DB2FR7/pub',
        'teacher'=>'https://docs.google.com/document/d/e/2PACX-1vRsMN8KCgR42IuQF3gSTWQkgi59d6oy3SkHJGtvT6Hk64r0c-rocPWEN1uo4j3YgMr_7-rcMY0S6Kdi/pub',
        'staff'=>'https://docs.google.com/document/d/e/2PACX-1vRsMN8KCgR42IuQF3gSTWQkgi59d6oy3SkHJGtvT6Hk64r0c-rocPWEN1uo4j3YgMr_7-rcMY0S6Kdi/pub'
    ],
    'guide' => [
        'student' => '',
        'admin' => 'https://docs.google.com/document/d/e/2PACX-1vSMIA3ip_fwtSC_Bq97zmxzfZh7NCwgsNLkyAfuNR9QnXycGclBugh9m39Fku9-xf0BWV3DtPsoclfB/pub',
        'teacher'=>'https://docs.google.com/presentation/d/e/2PACX-1vRRoKr1W_SNTMiMn_B88h8K5UviFM4TOJQ76qmqP3DTvO28biIQJwUNGjgtzUpp9KuYmx313LQtgh9S/embed?start=false&loop=false&delayms=5000',
        'staff'=>'https://docs.google.com/presentation/d/e/2PACX-1vRRoKr1W_SNTMiMn_B88h8K5UviFM4TOJQ76qmqP3DTvO28biIQJwUNGjgtzUpp9KuYmx313LQtgh9S/embed?start=false&loop=false&delayms=5000'
    ],
    'teacher_guide'=>[
        'admin'=>'https://docs.google.com/presentation/d/12agT_MUEQdN-yde2s1V9eeD0QStgNn0_0n7Jx9bVrGs/embed',
        'teacher'=>'https://docs.google.com/presentation/d/12agT_MUEQdN-yde2s1V9eeD0QStgNn0_0n7Jx9bVrGs/embed',
    ],
    'helpdesk'=>[
        'admin'=>'https://docs.google.com/document/d/e/2PACX-1vQItRh47TUY33WlOXduW9QKN8OG7X6EwlY6UC7jB5EkrtlQrwR3J7BAUvMZ7z-FL3bnLLB0NBrmsEfe/pub'
    ]

];
