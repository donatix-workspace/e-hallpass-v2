<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransparencyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function createTransparencyUser(User $user)
   {
       return $user->isAdmin();
   }

    /**
     * @param User $user
     * @return bool
     */
    public function deleteTransparencyUser(User $user)
    {
        return $user->isAdmin();
    }
}
