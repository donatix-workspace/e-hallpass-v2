<?php

namespace App\Http\Requests;

use App\Rules\CheckIfItsMidnightRule;
use App\Rules\DaysKeyExistRule;
use App\Rules\PassRoomOrUserExistRule;
use App\Rules\RecurrenceDaysRule;
use App\Rules\RecurrenceMonthlyCorrectStructureRule;
use App\Rules\RecurrenceTypeRule;
use App\Rules\RoomOrUserExistsAppointmentRule;
use App\Rules\RoomOrUserExistsRule;
use App\Rules\RoomOrUserStringRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class RecurrenceAppointmentPassRequest extends FormRequest
{
    public function rules()
    {
        $toType = Request::input('to_type');

        $schoolTimezone = optional(
            optional(auth()->user())->school
        )->getTimezone();

        return [
            'from_id' => ['integer', new RoomOrUserExistsAppointmentRule()],
            'to_id' => ['integer', new PassRoomOrUserExistRule($toType)],
            'from_type' => ['required', 'string', new RoomOrUserStringRule()],
            'to_type' => ['required', 'string', new RoomOrUserStringRule()],
            'for_date' => 'required|date|after_or_equal:' . $this->for_date,
            'for_time' => [
                'required',
                'date_format:H:i',
                new CheckIfItsMidnightRule()
            ],
            'period_id' => 'required|exists:periods,id|integer',
            'reason' => 'required|max:255|string',
            'user_id' => 'required|integer|exists:users,id',
            'recurrence_type' => ['nullable', new RecurrenceTypeRule()],
            'recurrence_days' => [
                'json',
                'nullable',
                new DaysKeyExistRule(),
                new RecurrenceDaysRule()
            ],
            'recurrence_week' => [
                'json',
                'nullable',
                'required_if:recurrence_type,monthly',
                new RecurrenceMonthlyCorrectStructureRule()
            ],
            'recurrence_end_at' => 'date|after:yesterday|nullable'
        ];
    }

    public function authorize()
    {
        return auth()->check() &&
            (auth()
                ->user()
                ->isAdmin() ||
                auth()
                    ->user()
                    ->isTeacher() ||
                auth()
                    ->user()
                    ->isStaff());
    }
}
