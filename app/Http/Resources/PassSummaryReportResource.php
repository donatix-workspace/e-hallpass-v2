<?php

namespace App\Http\Resources;

use App\Models\School;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\PassSummaryReport */
class PassSummaryReportResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (optional($this)->id === null) {
            return [];
        }

        $school = School::findCacheFirst($this->school_id);

        return [
            'id' => $this->id,
            'destination_id' => $this->destination_id,
            'destination_type' => $this->destination_type,
            'destination' => [
                'name' => $this->destination->name,
            ],
            "total_passes" => $this->total_passes,
            "total_time_all" => $this->total_time_all,
            "total_time_ksk" => $this->total_time_ksk,
            "total_time_proxy" => $this->total_time_proxy,
            "total_time_apt" => $this->total_time_apt,
            "from_date" => $this->from_date,
            "to_date" => $this->to_date,
            "total_time_student" => $this->total_time_student,
            "school_id" => $this->school_id,
            "last_synced_at" => School::convertTimeToCorrectTimezone($this->last_synced_at, $school->getTimezone()),
            "created_at" => School::convertTimeToCorrectTimezone($this->created_at, $school->getTimezone()),
            "updated_at" => School::convertTimeToCorrectTimezone($this->updated_at, $school->getTimezone())
        ];
    }
}
