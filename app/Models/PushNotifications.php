<?php

namespace App\Models;

interface PushNotifications
{
    public function getTopics();

    public function getNotificationTitle($event);

    public function getNotificationBody($event);

    public function getData($event, $options);

    public function getNotificationSettings($event, $options);
}
