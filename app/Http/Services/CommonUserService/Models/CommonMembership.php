<?php

namespace App\Http\Services\CommonUserService\Models;

use App\Models\Role;
use Illuminate\Database\Eloquent\Model;

class CommonMembership extends Model
{
    protected $connection = 'commonDb';
    protected $table = 'common_bm_membership';

    protected static $roles = [
        1 => 2,
        2 => 3,
        3 => 3,
        4 => 4,
        5 => 1
    ];

    public function school()
    {
        return $this->hasOne(CommonSchool::class, 'id', 'common_bm_school_id');
    }

    public static function getRoleId($commonRoleId)
    {
        return self::$roles[$commonRoleId];
    }
    public static function isSubstitute($commonRoleId): bool
    {
        return $commonRoleId == 3;
    }
}
