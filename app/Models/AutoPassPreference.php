<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AutoPassPreference extends Model
{
    use HasFactory;

    protected $table = 'auto_passes_preferences';
    protected $with = ['autoPass'];

    protected $guarded = [];

    // AutoPass modes
    public const MODE_START_ONLY = 'start_only';
    public const MODE_START_AND_STOP = 'start_and_stop';
    public const ACTIVE = 1;

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @param $query
     * @param $user
     * @return mixed
     */
    public function scopeOfUserId($query, $user)
    {
        return $query->where('user_id', $user);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autoPass()
    {
        return $this->belongsTo(AutoPass::class, 'auto_pass_id');
    }

    /**
     * @return bool
     */
    public function isInCorrectMode()
    {
        return $this->mode === AutoPassPreference::MODE_START_ONLY || $this->mode === AutoPassPreference::MODE_START_AND_STOP;
    }

    /**
     * @return bool
     */
    public function isModeStartAndStop()
    {
        return $this->mode === AutoPassPreference::MODE_START_AND_STOP;
    }
}
