DELIMITER ;;

DROP PROCEDURE IF EXISTS ehpv2.rollbackPasses;;
CREATE PROCEDURE ehpv2.rollbackPasses()
begin
    DECLARE i,o,n INT DEFAULT 0;
    DECLARE perPage INT DEFAULT 2500;

    SELECT count(id) from ehpv2.passes WHERE created_at BETWEEN '2021-12-01 00:00:00' AND '2021-12-30 23:59:59' into n;

    SET i = 0;
    SET o = 0;
    SET perPage = 2500;
    WHILE i < n / perPage DO
        START TRANSACTION;

        INSERT INTO ehallpassDB.epass_detail (student_id, outdate, indate, fromteacher_id, fromteacher_note, created_at, updated_at, parent_id, school_id, fromroom_id, outroom, toroom_id, period, commenting, ordering, toteacher_id, toteacher_note, outaction, out_by_teacher, inaction, in_by_teacher, pass_status, edit_status, passtype, preapproved, created_by)
            SELECT
                   (SELECT old_id FROM ehpv2.users WHERE id = passes.user_id) as student_id,
                   (SELECT CONVERT_TZ(passes.approved_at,
                                      '+00:00',
                                      (SELECT timezone_offset from schools where id = passes.school_id limit 1))) as outdate,
                   (SELECT CONVERT_TZ(passes.completed_at,
                                      '+00:00',
                                      (SELECT timezone_offset from schools where id = passes.school_id limit 1))) as indate,
                   (SELECT IF(from_type = 'App\\Models\\User', (SELECT old_id from ehpv2.users WHERE id=passes.from_id limit 1), 0)) as fromteacher_id,
                   (SELECT NULL),
                   (SELECT CONVERT_TZ(passes.created_at,
                                      '+00:00',
                                      (SELECT timezone_offset from schools where id = passes.school_id limit 1))) as created_at,
                (SELECT CONVERT_TZ(passes.updated_at,
                                   '+00:00',
                                   (SELECT timezone_offset from schools where id = passes.school_id limit 1))) as updated_at,
                (SELECT IF(passes.parent_id iS NOT NULL AND passes.parent_id != 0, (SELECT old_id FROM ehpv2.passes WHERE id=passes.parent_id limit 1), 0)) AS parent_id,
                   (SELECT old_id FROM schools WHERE id=passes.school_id limit 1) as school_id,
                   (SELECT IF(from_type = 'App\\Models\\Room', (SELECT old_id from ehpv2.rooms WHERE id=passes.from_id limit 1), 0)) as fromroom_id,
                  (SELECT 0),
                   (SELECT IF(to_type = 'App\\Models\\Room', (SELECT old_id from ehpv2.rooms WHERE id=passes.to_id limit 1), 0)) as toroom_id,
                   (SELECT 0),
                   (SELECT NULL),
                   (SELECT NULL),
                   (SELECT IF(to_type = 'App\\Models\\User', (SELECT old_id from ehpv2.users WHERE id=passes.to_id limit 1), 0)) as toteacher_id,
                   (SELECT NULL),
                (SELECT
                CASE
                    WHEN passes.completed_at is NOT NULL AND passes.parent_id is NULL THEN 3
                    WHEN passes.approved_at is NOT NULL AND passes.parent_id is NULL THEN 10
                    WHEN passes.canceled_at is NOT NULL AND passes.parent_id is NULL THEN 4
                    ELSE 0
                END
                    ) as outaction,
                   (SELECT old_id FROM users where id=passes.approved_by limit 1) as out_by_teacher,
                  (SELECT IF(passes.completed_at is not null AND passes.parent_id is null,3,0)) as inaction,
                   (SELECT old_id FROM users where id=passes.completed_by limit 1) as in_by_teacher,
                   (SELECT IF(passes.pass_status >= 1, 1 , 0)) as pass_status,
                   (SELECT IF(passes.pass_status is not null, 1, 0)) as edit_status,
                   (SELECT CASE
                       WHEN passes.type = 'STU' THEN 0
                       WHEN passes.type='APT' THEN 1
                       WHEN passes.type='KSK' THEN 3
                    END) as passtype,
                   (SELECT 0),
                   (SELECT old_id FROM users where id=passes.created_by limit 1) as created_by
        FROM ehpv2.passes as passes
        WHERE created_at BETWEEN '2021-12-01 00:00:00' AND '2021-12-30 23:59:59'
        LIMIT o,perPage;
        COMMIT;

        SET o = ((i + 1) * perPage) + 1;

        SET i = i + 1;

        end while ;;
end ;;

