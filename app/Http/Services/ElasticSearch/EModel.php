<?php
namespace App\Http\Services\ElasticSearch;

use App\Http\Services\ElasticSearch\Interfaces\ElasticModel;
use App\Http\Services\ElasticSearch\Models\AppointmentPassAndRecurrences;
use Exception;
use Illuminate\Pagination\LengthAwarePaginator;

class EModel implements ElasticModel
{
    /**
     * @var string
     */
    protected $indexes = '';
    /**
     * @var \array[][]
     */
    public $query;

    /**
     * @var string $sort
     */
    private $orderBy;

    /**
     * @var Client
     */
    private $client;

    /**
     * @param array $queryParams
     * @param string $search
     * @param bool $applyTransparency
     */
    protected function setQueryParams(
        array $queryParams,
        string $search = '*',
        bool $applyTransparency = false
    ) {
        if ($search !== config('scout_elastic.all_records')) {
            $type = [
                'query_string' => [
                    'query' => $search
                ]
            ];
        } else {
            $type = [
                'match_all' => (object) null
            ];
        }

        $this->query = [
            'query' => [
                'bool' => [
                    'must' => $type,
                    'filter' => [
                        'bool' => [
                            'must' => []
                        ]
                    ]
                ]
            ]
        ];

        foreach ($queryParams as $key => $value) {
            array_push(
                $this->query['query']['bool']['filter']['bool']['must'],
                [
                    'term' => [
                        $key => $value
                    ]
                ]
            );
        }
    }

    /**
     * @param int $size
     * @return array|string|null
     * @throws Exception
     */
    public function get(int $size = 25)
    {
        return $this->buildClient()->client->makeSearchRequest($this->indexes, [
            'size' => $size
        ]);
    }

    /**
     * @param string $column
     * @param string $order
     * @return $this
     * @throws Exception
     */
    public function orderBy(
        string $column,
        string $order
    ): AppointmentPassAndRecurrences {
        $orders = ['asc', 'desc'];

        if (empty($column) || empty($order)) {
            throw new Exception('Please provide a valid order attributes');
        }

        if (!in_array($order, $orders)) {
            $order = 'asc';
        }

        $this->orderBy = "$column:$order";

        return $this;
    }

    /**
     * @param string $column
     * @param array $values
     * @return $this
     */
    public function whereBetween(
        string $column,
        array $values
    ): AppointmentPassAndRecurrences {
        $betweenQuery = [
            'range' => [
                $column => [
                    'gte' => $values[0],
                    'lte' => $values[1]
                ]
            ]
        ];

        array_push(
            $this->query['query']['bool']['filter']['bool']['must'],
            $betweenQuery
        );

        return $this;
    }

    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     * @throws Exception
     */
    public function paginate(int $perPage = 25): LengthAwarePaginator
    {
        $offset = (request()->get('page', 1) - 1) * $perPage;

        // Return length aware pagination here
        $items = $this->buildClient()->client->makeSearchRequest(
            $this->indexes,
            [
                'size' => $perPage,
                'from' => $offset,
                'sort' => $this->orderBy
            ]
        );

        return new LengthAwarePaginator(
            $items,
            $this->count(),
            $perPage,
            request()->get('page', 1),
            ['path' => request()->url()]
        );
    }

    /**
     * @throws Exception
     */
    public function count()
    {
        return $this->buildClient()->client->makeSearchRequest(
            $this->indexes,
            null,
            'count'
        );
    }

    /**
     * @return $this
     */
    public function buildClient(): EModel
    {
        $this->client = new Client(json_encode($this->query));

        return $this;
    }

    /**
     * @param array $queryParam
     * @param string $searchString
     * @return $this
     */
    public function queryParams(
        array $queryParam,
        string $searchString = '*'
    ): EModel {
        $this->setQueryParams($queryParam, $searchString);

        return $this;
    }
}
