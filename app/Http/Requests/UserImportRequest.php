<?php

namespace App\Http\Requests;

use App\Rules\CSVFileRule;
use Illuminate\Foundation\Http\FormRequest;

class UserImportRequest extends FormRequest
{
    public function rules()
    {
        return [
            'csv_file' => ['required', 'file', new CSVFileRule()]
        ];
    }

    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}

