SET FOREIGN_KEY_CHECKS  =0;
START TRANSACTION ;
    truncate ehp_v2_prod.teacher_pass_settings;
    INSERT INTO ehp_v2_prod.teacher_pass_settings (school_id, min_time, white_passes, awaiting_apt, max_time, auto_expire_time, nurse_expire_time, mail_time, created_at, updated_at)
        SELECT
               (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldTps.school_id),
               (SELECT oldTps.min_time),
               (SELECT oldTps.white_passes),
               (SELECT oldTps.awaiting_apt),
               (SELECT oldTps.max_time),
               (SELECT oldTps.auto_expire_time),
               (SELECT oldTps.nurse_auto_expire_time),
               (SELECT oldTps.mail_time),
               (SELECT CONVERT_TZ(oldTps.created_at, 'America/New_York', 'UTC')),
              (SELECT CONVERT_TZ(oldTps.updated_at, 'America/New_York', 'UTC'))
    FROM ehallpassDB.teacher_pass_settings as oldTps;

COMMIT ;

START TRANSACTION;
UPDATE ehp_v2_prod.teacher_pass_settings as tchP, ehallpassDB.ab_polarity_settings as oldPS
SET tchP.polarity_message = oldPS.message_for_student
WHERE tchP.school_id = (SELECT id FROM schools WHERE old_id = oldPS.school_id limit 1);
COMMIT;
SET FOREIGN_KEY_CHECKS=1;
