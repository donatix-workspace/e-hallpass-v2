<?php

namespace App\Models;

use App\Elastic\PassesIndexConfigurator;
use App\Elastic\Rules\DashboardFilterSearchRule;
use App\Elastic\Rules\PassHistoryFilterSearchRule;
use App\Events\PinStatusEvent;
use App\Traits\ExtendedSearchable;
use App\Traits\WhereInRawChunked;
use App\Events\AdminPassApproved;
use App\Events\AdminPassArrived;
use App\Events\AdminPassEnded;
use App\Events\AdminPassInOut;
use App\Events\AdminPassReturning;
use App\Events\DashboardRefreshEvent;
use App\Events\PassApproved;
use App\Events\PassArrived;
use App\Events\PassCanceled;
use App\Events\PassCommentCreatedEvent;
use App\Events\PassEnded;
use App\Events\PassExpired;
use App\Events\PassInOut;
use App\Events\PassReturning;
use App\Http\Filters\Filterable;
use App\Http\Requests\AutoCheckInRequest;
use App\Http\Requests\PassRequest;
use App\Http\Resources\PassResource;
use App\Notifications\Push\StudentNotification;
use App\Policies\PassPolicy;
use App\Traits\AdvancedOrders;
use App\Traits\DashboardRefreshable;
use App\Traits\PassMapping;
use App\Traits\PerPage;
use Carbon\Carbon;
use DateTimeInterface;
use Elasticsearch\Endpoints\Search;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\JsonResponse;
use DB;
use Illuminate\Support\Facades\Notification;
use Log;
use ScoutElastic\Builders\SearchBuilder;

class Pass extends Model implements PushNotifications
{
    use HasFactory,
        Filterable,
        ExtendedSearchable,
        PassMapping,
        PerPage,
        AdvancedOrders,
        DashboardRefreshable,
        WhereInRawChunked;

    protected $with = [
        'comments',
        'latestComment:id,comment,commentable_type,commentable_id',
        'createdByUser:first_name,last_name,id,school_id,role_id,avatar,email',
        'user:first_name,last_name,id,school_id,role_id,email,avatar',
        'requestedByUser:first_name,last_name,id,school_id,avatar,email,role_id',
        'to',
        'from',
        'fromKiosk',
        'toKiosk',
        'createdByKiosk',
        'completedByUser:first_name,last_name,id,school_id,avatar,email,role_id',
        'approvedByUser:first_name,last_name,id,school_id,avatar,email,role_id'
    ];

    protected $indexConfigurator = PassesIndexConfigurator::class;

    public const ATTRIBUTE_TOTAL_TIME_COLUMN = 'total_time';

    public const PASS_HISTORY_MYSQL_INDEX = 'passes_filters_pass_history_index';

    protected $guarded = [];

    protected $appends = [
        'total_time',
        'is_flagged',
        'badge_flags',
        'elastic_advanced_fields',
        'created_date'
    ];

    protected $dates = [
        'approved_at',
        'completed_at',
        'requested_at',
        'expired_at',
        'arrived_at',
        'edited_at',
        'canceled_at'
    ];

    /**
     * @var string[]
     * Update the updated_at timestamp when
     * action in the child is being done.
     */
    protected $touches = ['parent'];

    public const ACTIVE = 1;
    public const INACTIVE = 0;

    public const ACTION_APPROVE = 1;
    public const ACTION_END = 2;
    public const ACTION_ARRIVE = 3;
    public const ACTION_RETURN_IN_OUT = 4;
    public const ACTION_CANCEL = 5;
    public const ACTION_EXPIRE = 6;
    public const ACTION_KIOSK_END = 7;
    public const ACTION_KIOSK_CANCEL = 8;

    public const BY_AUTOPASS = 'autopass';
    public const BY_CHECKIN = 'checkin';
    public const BY_KIOSK = 'kiosk';
    public const PROXY_PASS = 'TCH';
    public const APPOINTMENT_PASS = 'APT';
    public const STUDENT_CREATED_PASS = 'STU';
    public const KIOSK_PASS = 'KSK';
    public const SYSTEM_COMPLETED_ACTIVE = 1;
    public const SYSTEM_COMPLETED_INACTIVE = 0;

    /**
     * @return array
     */
    public function toSearchableArray(): array
    {
        $pass = $this->load(['child', 'comments'])
            ->setAppends([
                'total_time',
                'elastic_advanced_fields',
                'created_date'
            ])
            ->toArray();

        $school = optional(School::findCacheFirst($this->school_id));

        $approveTime = School::convertTimeToCorrectTimezone(
            optional($this)->approved_at,
            $school->getTimezone()
        );
        $completedAt = School::convertTimeToCorrectTimezone(
            optional($this)->completed_at,
            $school->getTimezone()
        );
        $approveChildTime = School::convertTimeToCorrectTimezone(
            optional($this->child)->approved_at,
            $school->getTimezone()
        );
        $completedChildAt = School::convertTimeToCorrectTimezone(
            optional($this->child)->completed_at,
            $school->getTimezone()
        );

        $pass['tabular_view']['approve_time'] =
            $this->approved_at !== null
                ? Carbon::parse($approveTime)->format('g:i:s')
                : null;

        $pass['tabular_view']['completed_time'] =
            $this->completed_at !== null
                ? Carbon::parse($completedAt)->format('g:i:s')
                : null;

        $pass['tabular_view']['approve_child_time'] =
            optional(optional($this)->child)->approved_at !== null
                ? Carbon::parse($approveChildTime)->format('g:i:s')
                : null;

        $pass['tabular_view']['completed_child_time'] =
            optional(optional($this)->child)->completed_at !== null
                ? Carbon::parse($completedChildAt)->format('g:i:s')
                : null;

        return $pass;
    }

    /**
     * @param $query
     * @param $user
     * @param $staffSchedulesRoomIds
     * @param bool $child
     * @return mixed
     */
    public function scopeFlaggedPassesAssignedToMe(
        $query,
        $user,
        $staffSchedulesRoomIds,
        bool $child = false
    ) {
        return $query->where(function ($query) use (
            $user,
            $staffSchedulesRoomIds,
            $child
        ) {
            $builder = $child
                ? $query->whereNotNull('passes.parent_id')
                : $query->whereNull('passes.parent_id');

            $builder
                ->where(function ($query) use ($user) {
                    $query
                        ->where('passes.extended_at', '!=', null)
                        ->orWhere('passes.flagged_at', '!=', null)
                        ->orWhere('passes.system_completed', Pass::ACTIVE);
                })
                ->where(function ($query) use ($user, $staffSchedulesRoomIds) {
                    $query
                        ->where('passes.from_type', User::class)
                        ->where('passes.from_id', $user->id)
                        ->orWhere(function ($query) use ($user) {
                            $query
                                ->where('passes.to_type', User::class)
                                ->where('passes.to_id', $user->id);
                        })
                        ->orWhere(function ($query) use (
                            $staffSchedulesRoomIds,
                            $user
                        ) {
                            $query
                                ->where('passes.to_type', Room::class)
                                ->whereIntegerInRaw(
                                    'passes.to_id',
                                    $staffSchedulesRoomIds
                                );
                        })
                        ->orWhere(function ($query) use (
                            $staffSchedulesRoomIds,
                            $user
                        ) {
                            $query
                                ->where('passes.from_type', Room::class)
                                ->whereIntegerInRaw(
                                    'passes.from_id',
                                    $staffSchedulesRoomIds
                                );
                        })
                        ->orWhere(function ($query) use ($user) {
                            $query
                                ->where('passes.approved_by', $user->id)
                                ->orWhere('passes.completed_by', $user->id);
                        });
                });
        });
    }

    /**
     * @param $user
     * @param $startDate
     * @param $endDate
     * @param $staffSchedulesRoomIds
     * @return int
     */
    public static function getFlaggedPassesCount(
        $user,
        $startDate,
        $endDate,
        $staffSchedulesRoomIds
    ): int {
        $flaggedPassesOnlyParentCount = Pass::where(
            'passes.school_id',
            $user->school_id
        )
            ->whereBetween('passes.created_at', [$startDate, $endDate])
            ->flaggedPassesAssignedToMe($user, $staffSchedulesRoomIds)
            ->count();

        $flaggedPassesChildsCount = Pass::where(
            'passes.school_id',
            $user->school_id
        )
            ->whereBetween('passes.created_at', [$startDate, $endDate])
            ->flaggedPassesAssignedToMe($user, $staffSchedulesRoomIds, true)
            ->count();

        return $flaggedPassesOnlyParentCount + $flaggedPassesChildsCount;
    }

    /**
     * @param $query
     * @param $user
     * @param $staffSchedulesRoomIds
     * @return mixed
     */
    public function scopePassesToMeAndAssignedToMe(
        $query,
        $user,
        $staffSchedulesRoomIds
    ) {
        return $query->where(function ($query) use (
            $user,
            $staffSchedulesRoomIds
        ) {
            $query
                ->where(function ($query) use ($user, $staffSchedulesRoomIds) {
                    $query
                        ->where('passes.from_type', User::class)
                        ->where('passes.from_id', $user->id)
                        ->orWhere(function ($query) use ($user) {
                            $query
                                ->where('passes.to_type', User::class)
                                ->where('passes.to_id', $user->id);
                        })
                        ->orWhere(function ($query) use (
                            $staffSchedulesRoomIds,
                            $user
                        ) {
                            $query
                                ->where('passes.to_type', Room::class)
                                ->whereIntegerInRaw(
                                    'passes.to_id',
                                    $staffSchedulesRoomIds
                                );
                        })
                        ->orWhere(function ($query) use (
                            $staffSchedulesRoomIds,
                            $user
                        ) {
                            $query
                                ->where('passes.from_type', Room::class)
                                ->whereIntegerInRaw(
                                    'passes.from_id',
                                    $staffSchedulesRoomIds
                                );
                        })
                        ->orWhere('passes.approved_by', $user->id)
                        ->orWhere('passes.completed_by', $user->id);
                })
                ->orWhere(function ($query) use (
                    $user,
                    $staffSchedulesRoomIds
                ) {
                    $query->where(function ($query) use (
                        $user,
                        $staffSchedulesRoomIds
                    ) {
                        $query
                            ->where('child.from_type', User::class)
                            ->where('child.from_id', $user->id)
                            ->orWhere(function ($query) use ($user) {
                                $query
                                    ->where('child.to_type', User::class)
                                    ->where('child.to_id', $user->id);
                            })
                            ->orWhere(function ($query) use (
                                $staffSchedulesRoomIds,
                                $user
                            ) {
                                $query
                                    ->where('child.to_type', Room::class)
                                    ->whereIntegerInRaw(
                                        'child.to_id',
                                        $staffSchedulesRoomIds
                                    );
                            })
                            ->orWhere(function ($query) use (
                                $staffSchedulesRoomIds,
                                $user
                            ) {
                                $query
                                    ->where('child.from_type', Room::class)
                                    ->whereIntegerInRaw(
                                        'child.from_id',
                                        $staffSchedulesRoomIds
                                    );
                            })
                            ->orWhere('child.approved_by', $user->id)
                            ->orWhere('child.completed_by', $user->id);
                    });
                });
        });
    }

    /**
     * @param $query
     * @param $schoolId
     * @param $dates
     * @return mixed
     */
    public function scopeOnlySchoolAndToday($query, $schoolId, $dates)
    {
        return $query
            ->where('school_id', $schoolId)
            ->whereBetween('created_at', [$dates[0], $dates[1]]);
    }

    /**
     * @param $query
     * @param $user
     * @param $staffSchedulesFromRoomIds
     * @param $staffSchedulesToRoomIds
     * @return mixed
     */
    public function scopeTransparencyWithColumnPrefix(
        $query,
        $user,
        $staffSchedulesFromRoomIds,
        $staffSchedulesToRoomIds
    ) {
        return $query->where(function ($query) use (
            $user,
            $staffSchedulesFromRoomIds,
            $staffSchedulesToRoomIds
        ) {
            $query
                ->where(function ($query) use ($user) {
                    $query
                        ->where('passes.from_type', User::class)
                        ->where('passes.from_id', $user->id);
                })
                ->orWhere(function ($query) use ($user) {
                    $query
                        ->where('passes.to_type', User::class)
                        ->where('passes.to_id', $user->id);
                })
                ->orWhere(function ($query) use ($user) {
                    $query->orWhere('passes.created_by', $user->id);
                })
                ->orWhere(function ($query) use ($user) {
                    $query->orWhere('passes.completed_by', $user->id);
                })
                ->orWhere(function ($query) use ($user) {
                    $query->orWhere('passes.approved_by', $user->id);
                })
                ->orWhere(function ($query) use ($staffSchedulesFromRoomIds) {
                    $query
                        ->where('passes.from_type', Room::class)
                        ->whereIntegerInRaw(
                            'passes.from_id',
                            $staffSchedulesFromRoomIds
                        );
                })
                ->orWhere(function ($query) use ($staffSchedulesToRoomIds) {
                    $query
                        ->where('passes.to_type', Room::class)
                        ->whereIntegerInRaw(
                            'passes.to_id',
                            $staffSchedulesToRoomIds
                        );
                })
                ->orWhere(function ($query) use (
                    $user,
                    $staffSchedulesFromRoomIds,
                    $staffSchedulesToRoomIds
                ) {
                    $query
                        ->where(function ($query) use ($user) {
                            $query
                                ->where('child.from_type', User::class)
                                ->where('child.from_id', $user->id);
                        })
                        ->orWhere(function ($query) use ($user) {
                            $query
                                ->where('child.to_type', User::class)
                                ->where('child.to_id', $user->id);
                        })
                        ->orWhere(function ($query) use ($user) {
                            $query->orWhere('child.created_by', $user->id);
                        })
                        ->orWhere(function ($query) use ($user) {
                            $query->orWhere('child.completed_by', $user->id);
                        })
                        ->orWhere(function ($query) use ($user) {
                            $query->orWhere('child.approved_by', $user->id);
                        })
                        ->orWhere(function ($query) use (
                            $staffSchedulesFromRoomIds
                        ) {
                            $query
                                ->where('child.from_type', Room::class)
                                ->whereIntegerInRaw(
                                    'child.from_id',
                                    $staffSchedulesFromRoomIds
                                );
                        })
                        ->orWhere(function ($query) use (
                            $staffSchedulesToRoomIds
                        ) {
                            $query
                                ->where('child.to_type', Room::class)
                                ->whereIntegerInRaw(
                                    'child.to_id',
                                    $staffSchedulesToRoomIds
                                );
                        });
                });
        });
    }

    /**
     * @param $query
     * @param $user
     * @param $staffSchedulesFromRoomIds
     * @param $staffSchedulesToRoomIds
     * @return mixed
     */
    public function scopeTransparency(
        $query,
        $user,
        $staffSchedulesFromRoomIds,
        $staffSchedulesToRoomIds
    ) {
        return $query->where(function ($query) use (
            $user,
            $staffSchedulesFromRoomIds,
            $staffSchedulesToRoomIds
        ) {
            $query
                ->where(function ($query) use ($user) {
                    $query
                        ->where('from_type', User::class)
                        ->where('from_id', $user->id);
                })
                ->orWhere(function ($query) use ($user) {
                    $query
                        ->where('to_type', User::class)
                        ->where('to_id', $user->id);
                })
                ->orWhere(function ($query) use ($user) {
                    $query->orWhere('created_by', $user->id);
                })
                ->orWhere(function ($query) use ($user) {
                    $query->orWhere('completed_by', $user->id);
                })
                ->orWhere(function ($query) use ($user) {
                    $query->orWhere('approved_by', $user->id);
                })
                ->orWhere(function ($query) use ($staffSchedulesFromRoomIds) {
                    $query
                        ->where('from_type', Room::class)
                        ->whereIntegerInRaw(
                            'from_id',
                            $staffSchedulesFromRoomIds
                        );
                })
                ->orWhere(function ($query) use ($staffSchedulesToRoomIds) {
                    $query
                        ->where('to_type', Room::class)
                        ->whereIntegerInRaw('to_id', $staffSchedulesToRoomIds);
                })
                ->orWhereHas('child', function ($query) use (
                    $user,
                    $staffSchedulesFromRoomIds,
                    $staffSchedulesToRoomIds
                ) {
                    $query
                        ->where(function ($query) use ($user) {
                            $query
                                ->where('from_type', User::class)
                                ->where('from_id', $user->id);
                        })
                        ->orWhere(function ($query) use ($user) {
                            $query
                                ->where('to_type', User::class)
                                ->where('to_id', $user->id);
                        })
                        ->orWhere(function ($query) use ($user) {
                            $query->orWhere('created_by', $user->id);
                        })
                        ->orWhere(function ($query) use ($user) {
                            $query->orWhere('completed_by', $user->id);
                        })
                        ->orWhere(function ($query) use ($user) {
                            $query->orWhere('approved_by', $user->id);
                        })
                        ->orWhere(function ($query) use (
                            $staffSchedulesFromRoomIds
                        ) {
                            $query
                                ->where('from_type', Room::class)
                                ->whereIntegerInRaw(
                                    'from_id',
                                    $staffSchedulesFromRoomIds
                                );
                        })
                        ->orWhere(function ($query) use (
                            $staffSchedulesToRoomIds
                        ) {
                            $query
                                ->where('to_type', Room::class)
                                ->whereIntegerInRaw(
                                    'to_id',
                                    $staffSchedulesToRoomIds
                                );
                        });
                });
        });
    }

    /**
     * @param DateTimeInterface $date
     * @return string
     */
    public function serializeDate(DateTimeInterface $date): string
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($date, config('app.timezone'))
            ->setTimezone(optional($school)->getTimezone())
            ->format('Y-m-d H:i:s');
    }

    /**
     * @param $value
     * @return string
     */
    public function getCreatedDateAttribute($value): string
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($this->created_at, config('app.timezone'))
            ->setTimezone(optional($school)->getTimezone())
            ->format('m/d/Y');
    }

    /**
     * @param $query
     * @param  $type
     * @return mixed
     */
    public function scopeWhereFromType($query, $type)
    {
        return $query->where('from_type', $type);
    }

    /**
     * @param $query
     * @param  $type
     * @return mixed
     */
    public function scopeWhereToType($query, $type)
    {
        return $query->where('to_type', $type);
    }

    /**
     * @return \Illuminate\Support\Carbon|mixed|null
     */
    public function getEndedAtAttribute()
    {
        $school = School::findCacheFirst($this->school_id);

        $child = $this->child;
        return $child !== null
            ? (optional($child)->completed_at === null
                ? optional($child)->approved_at
                : $child->completed_at)
            : $this->completed_at;
    }

    /**
     * @return array
     */
    public function getElasticAdvancedFieldsAttribute(): array
    {
        $school = optional(School::findCacheFirst($this->school_id));
        $approveTime = School::convertTimeToCorrectTimezone(
            optional($this)->approved_at,
            $school->getTimezone()
        );
        $completedAt = School::convertTimeToCorrectTimezone(
            optional($this)->completed_at,
            $school->getTimezone()
        );
        $approveChildTime = School::convertTimeToCorrectTimezone(
            optional($this->child)->approved_at,
            $school->getTimezone()
        );
        $completedChildAt = School::convertTimeToCorrectTimezone(
            optional($this->child)->completed_at,
            $school->getTimezone()
        );

        return [
            'approve_time_only' =>
                $this->approved_at !== null
                    ? Carbon::parse($approveTime)->format('g:i A')
                    : 0,
            'completed_time_only' =>
                $this->completed_at !== null
                    ? Carbon::parse($completedAt)->format('g:i A')
                    : 0,
            'approve_child_time_only' =>
                optional($this->child)->approved_at !== null
                    ? Carbon::parse($approveChildTime)->format('g:i A')
                    : 0,
            'completed_child_time_only' =>
                optional($this->child)->completed_at !== null
                    ? Carbon::parse($completedChildAt)->format('g:i A')
                    : 0
        ];
    }

    /**
     * @return array
     */
    public function getSignaturesAttribute(): array
    {
        $signatures = 0;

        if ($this->approved_by !== null) {
            $signatures++;
        }

        if ($this->completed_by !== null) {
            $signatures++;
        }

        if ($this->child !== null && $this->child->approved_by !== null) {
            $signatures++;
        }

        if ($this->child !== null && $this->child->completed_by !== null) {
            $signatures++;
        }

        return [
            'count' => $signatures,
            'unexpected' => $this->hasUnexpectedSignature()
        ];
    }

    /**
     * @return bool
     */
    public function hasUnexpectedSignature(): bool
    {
        if ($this->approvedByUser !== null && $this->completedByUser !== null) {
            if (
                $this->approvedByUser->isStudent() &&
                $this->completedByUser->isStudent()
            ) {
                return false;
            }

            if (
                $this->completedByUser->isStudent() &&
                $this->approvedByUser->isStudent()
            ) {
                return false;
            }
        }

        if (
            $this->approved_by &&
            $this->approved_by !== $this->from_id &&
            $this->from_type === User::class
        ) {
            return true;
        }

        if (
            $this->completed_by &&
            $this->completed_by !== $this->to_id &&
            $this->to_type === User::class
        ) {
            return true;
        }

        if (
            optional($this->child)->approved_by &&
            $this->child->approved_by !== $this->child->from_id &&
            $this->child->from_type === User::class
        ) {
            return true;
        }

        if (
            optional($this->child)->approved_by &&
            $this->child->completed_by !== $this->child->to_id &&
            $this->child->to_type === User::class
        ) {
            return true;
        }

        if (
            optional($this)->from_type === Room::class &&
            is_array($this->from->user_ids_assigned) &&
            count($this->from->user_ids_assgined) > 0
        ) {
            if (!in_array($this->approved_by, $this->from->user_ids_assigned)) {
                return true;
            }
        }

        if (
            optional($this)->to_type === Room::class &&
            is_array($this->to->user_ids_assigned) &&
            count($this->to->user_ids_assgined) > 0
        ) {
            if (
                !in_array(
                    $this->completed_by,
                    $this->child->from->user_ids_assigned
                )
            ) {
                return true;
            }
        }

        if (
            optional($this->child)->from_type === Room::class &&
            is_array($this->child->from->user_ids_assigned) &&
            count($this->child->from->user_ids_assigned) > 0
        ) {
            if (
                !in_array(
                    $this->child->approved_by,
                    $this->child->from->user_ids_assigned
                )
            ) {
                return true;
            }
        }

        if (
            optional($this->child)->to_type === Room::class &&
            is_array($this->child->to->user_ids_assigned) &&
            count($this->child->to->user_ids_assigned) > 0
        ) {
            if (
                !in_array(
                    $this->child->completed_by,
                    $this->child->to->user_ids_assigned
                )
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query
            ->where('pass_status', self::ACTIVE)
            ->where('approved_at', '!=', null);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActiveWithoutApprove($query)
    {
        return $query->where('pass_status', self::ACTIVE);
    }

    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->approved_at !== null;
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return $this->expired_at !== null;
    }

    /**
     * @return bool
     */
    public function isArrived()
    {
        return $this->arrived_at !== null;
    }

    /**
     * @return bool
     */
    public function isCompleted()
    {
        return $this->completed_at !== null;
    }

    /**
     * @return bool
     */
    public function isParent()
    {
        return $this->parent_id !== null;
    }

    /**
     * @return bool
     */
    public function isCanceled()
    {
        return $this->canceled_at !== null;
    }

    /**
     * @return bool
     */
    public function hasChild()
    {
        return $this->child !== null;
    }

    /**
     * @return string
     */
    public function getTotalTimeAttribute()
    {
        $totalTime = Carbon::parse('00:00');
        $approvedAt = $this->approved_at;
        $completedAt = $this->completed_at;

        // If both passes are completed and approved we combine total time on both of them.
        if ($this->hasChild()) {
            $childPass = $this->child;

            // Calculate total time on both
            if (
                $childPass->isApproved() &&
                $childPass->isCompleted() &&
                $approvedAt !== null
            ) {
                $currentPassDifferences = $approvedAt->diff(
                    $childPass->completed_at
                );

                return $totalTime
                    ->add($currentPassDifferences)
                    ->format('H:i:s');
            }

            if (
                $childPass->isApproved() &&
                !$childPass->isCompleted() &&
                $approvedAt !== null
            ) {
                $currentPassDifferences = $approvedAt->diff(
                    $childPass->approved_at
                );

                return $totalTime
                    ->add($currentPassDifferences)
                    ->format('H:i:s');
            }
        }

        if ($this->isApproved() && $this->isCompleted()) {
            $differences = $approvedAt->diff($completedAt);
            $totalTime->add($differences);

            // Date Interval formatting here
            return $totalTime->format('H:i:s');
        }

        return $totalTime->format('H:i:s');
    }

    /**
     * @param $query
     * @param $parent
     * @return mixed
     */
    public function scopeFindParent($query, $parent)
    {
        return $query->where('id', $parent);
    }

    /**
     * @param $query
     * @param $user
     * @return mixed
     */
    public function scopeOfUserId($query, $user)
    {
        return $query->where('user_id', $user);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withoutGlobalScopes();
    }

    /**
     * @return BelongsTo
     */
    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function from()
    {
        return $this->morphTo()->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function to()
    {
        return $this->morphTo()->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent()
    {
        return $this->hasOne(self::class, 'id', 'parent_id');
    }

    /**
     * @return BelongsTo
     */
    public function createdByUser()
    {
        return $this->belongsTo(
            User::class,
            'created_by'
        )->withoutGlobalScopes();
    }

    /**
     * @return BelongsTo
     */
    public function approvedByUser()
    {
        return $this->belongsTo(
            User::class,
            'approved_by'
        )->withoutGlobalScopes();
    }

    /**
     * @return BelongsTo
     */
    public function completedByUser()
    {
        return $this->belongsTo(
            User::class,
            'completed_by'
        )->withoutGlobalScopes();
    }

    /**
     * @return BelongsTo
     */
    public function requestedByUser()
    {
        return $this->belongsTo(
            User::class,
            'requested_by'
        )->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pinAttempts()
    {
        return $this->hasMany(PinAttempt::class);
    }

    /**
     * @param Pass $pass
     * @param bool $approved
     * @param int|null $approvedBy
     * @param int|null $passStatus
     * @param bool $child
     * @return array
     */
    public static function createParent(
        self $pass,
        ?bool $approved = false,
        ?int $approvedBy = 0,
        ?int $passStatus = 1,
        bool $checkIn = false
    ) {
        $passWithParent = self::create([
            // Previous pass id
            'parent_id' => $pass->id,

            // Switch from, to places
            'from_type' => $pass->to_type,
            'to_type' => $pass->from_type,
            'from_id' => $pass->to_id,
            'to_id' => $pass->from_id,

            // Other props
            'school_id' => $pass->school_id,
            'user_id' => $pass->user_id,
            'pass_status' => $passStatus,
            'type' => $pass->type,
            'approved_at' => $approved ? Carbon::now() : null,
            'approved_by' => $approved ? $approvedBy : null,
            'requested_at' => $pass->requested_at
        ]);

        return [
            'student' => self::with('parent')
                ->without('child')
                ->where('id', $passWithParent->id)
                ->firstOrFail()
                ->append(['flags', 'expire_after']),
            'admin' => self::with('child')
                ->without('parent')
                ->where('id', $passWithParent->parent_id)
                ->firstOrFail()
                ->append(['flags', 'expire_after'])
        ];
    }

    /**
     * @param Pin|null $pin
     * @param string|null $message
     * @param bool|null $kiosk
     * @param int|null $kioskId
     * @param string|null $approvedWith
     * @return PassResource|JsonResponse
     */
    public function approve(
        ?Pin $pin = null,
        ?string $message = null,
        ?bool $kiosk = false,
        ?int $kioskId = 0,
        ?string $approvedWith = null
    ) {
        // The action is already done
        if (
            $this->isApproved() ||
            $this->isCompleted() ||
            $this->isParent() ||
            $this->isCanceled()
        ) {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('The pass is already approved.'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $userId = $kiosk ? auth()->user()->user_id : auth()->user()->id;

        if ($this->expired_at) {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('passes.approval.timeout'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $this->update([
            'approved_at' => Carbon::now(),
            'approved_by' => $pin !== null ? $pin->pinnable_id : $userId,
            'from_teacher_note' => $message,
            'pass_status' => 1,
            'approved_with' => $approvedWith,
            'from_kiosk_id' => $kiosk ? $kioskId : null
        ]);

        $this->addApprovalComment($pin, $message);

        $pass = self::with('parent')
            ->without('child')
            ->where('id', $this->id)
            ->firstOrFail()
            ->append(['flags', 'expire_after']);

        $approvedFromAdult =
            auth()->check() &&
            !auth()
                ->user()
                ->isStudent();
        event(new PassApproved($pass, $approvedFromAdult));
        event(new AdminPassApproved($pass));
        //        Notification::send(
        //            $pass,
        //            new StudentNotification('PassApproved', [
        //                'notifyStudent' => $approvedFromAdult,
        //                'approvedAt' => $this->approved_at
        //            ])
        //        );

        $this->refreshDashboard();

        event(
            new DashboardRefreshEvent(
                auth()->user()->school_id,
                User::find($pass->approved_by)
            )
        );

        return new PassResource($pass);
    }

    /**
     * @param Pin|null $pin
     * @return PassResource|JsonResponse
     */
    public function kioskCancel(?Pin $pin)
    {
        // the action is already done
        if (
            $this->isCanceled() ||
            $this->isCompleted() ||
            $this->isApproved() ||
            $this->isExpired() ||
            $this->type !== Pass::KIOSK_PASS
        ) {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('The pass is already activated.'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $this->update([
            'pass_status' => 0,
            'canceled_at' => Carbon::now()
        ]);

        $pass = self::with('parent')
            ->without('child')
            ->where('id', $this->id)
            ->firstOrFail();

        event(new PassCanceled($pass));

        //        Notification::send($pass, new StudentNotification('PassCanceled'));

        return new PassResource($pass);
    }

    /**
     * @param Comment|null $comment
     * @return array
     */
    public function getNotifiableAdults(?Comment $comment = null): array
    {
        $notifiableAdultsIds = [];

        if ($comment !== null) {
            if (
                $this->from_type === User::class &&
                $this->from_id !== $comment->user_id
            ) {
                array_push($notifiableAdultsIds, $this->from_id);
            }

            if (
                $this->to_type === User::class &&
                $this->to_id !== $comment->user_id
            ) {
                array_push($notifiableAdultsIds, $this->to_id);
            }

            if (
                $this->approved_by &&
                $this->approved_by !== $comment->user_id
            ) {
                if ($this->canBeNotifiedByComment($comment)) {
                    array_push($notifiableAdultsIds, $this->approved_by);
                }
            }
        } else {
            if ($this->from_type === User::class && !$this->isApproved()) {
                array_push($notifiableAdultsIds, $this->from_id);
            }

            if ($this->to_type === User::class && $this->isApproved()) {
                array_push($notifiableAdultsIds, $this->to_id);
            }

            if ($this->from_type === Room::class && !$this->isApproved()) {
                $staffSchedulesUserIds = StaffSchedule::where(
                    'room_id',
                    $this->from_id
                )
                    ->get()
                    ->pluck('user_id')
                    ->toArray();

                $notifiableAdultsIds = array_merge($staffSchedulesUserIds);
            }

            if ($this->to_type === Room::class && $this->isApproved()) {
                $staffSchedulesToUserIds = StaffSchedule::where(
                    'room_id',
                    $this->to_id
                )
                    ->get()
                    ->pluck('user_id')
                    ->toArray();

                $notifiableAdultsIds = array_merge($staffSchedulesToUserIds);
            }
        }

        return array_values(array_unique($notifiableAdultsIds));
    }

    /**
     * @param Comment $comment
     * @return bool|void
     */
    public function canBeNotifiedByComment(Comment $comment)
    {
        if (
            $this->from_type === User::class &&
            $this->from_id !== $comment->user_id
        ) {
            return true;
        }

        if (
            $this->to_type === User::class &&
            $this->to_id !== $comment->user_id
        ) {
            return true;
        }
    }

    /**
     * @param Pin|null $pin
     * @return PassResource|JsonResponse
     */
    public function cancel(?Pin $pin)
    {
        // the action is already done
        if (
            $this->isCanceled() ||
            $this->isCompleted() ||
            $this->isApproved() ||
            $this->isExpired()
        ) {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('The pass is already activated'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $this->update([
            'canceled_at' => Carbon::now(),
            'approved_by' =>
                $pin !== null ? $pin->pinnable_id : auth()->user()->id,
            'pass_status' => 0
        ]);

        $pass = self::with('parent')
            ->without('child')
            ->where('id', $this->id)
            ->firstOrFail();

        event(new PassCanceled($pass));
        //        Notification::send($pass, new StudentNotification('PassCanceled'));

        return new PassResource($pass);
    }

    /**
     * @return PassResource
     */
    public function expire(): PassResource
    {
        $teacherPassSettings = TeacherPassSetting::where(
            'school_id',
            $this->school_id
        )->first();

        $nurseRoom = NurseRoom::where('room_id', $this->to_id)
            ->where('school_id', $this->school_id)
            ->first();

        $nurseRoomFrom = NurseRoom::where('room_id', $this->from_id)
            ->where('school_id', $this->school_id)
            ->first();

        if (
            $nurseRoomFrom &&
            $this->from_type === Room::class &&
            $this->isParent()
        ) {
            $nurseExpireTime = TeacherPassSetting::convertInMinutes(
                $nurseRoomFrom->end_time
            );

            if (!$this->isApproved()) {
                $this->update([
                    'pass_status' => Pass::INACTIVE,
                    'flagged_at' => now(),
                    'extended_at' => now(),
                    'approved_at' => $this->parent->approved_at->addMinutes(
                        $nurseExpireTime
                    ),
                    'system_completed' => Pass::ACTIVE,
                    'expired_at' => Carbon::now()
                ]);
            } else {
                $this->update([
                    'pass_status' => Pass::INACTIVE,
                    'flagged_at' => now(),
                    'extended_at' => now(),
                    'system_completed' => Pass::ACTIVE,
                    'expired_at' => Carbon::now()
                ]);
            }

            event(
                new PassExpired(
                    Pass::onWriteConnection()
                        ->with('child')
                        ->without('parent')
                        ->where(
                            'id',
                            $this->parent_id !== null
                                ? $this->parent_id
                                : $this->id
                        )
                        ->firstOrFail()
                )
            );

            //            Notification::send($this, new StudentNotification('PassExpired'));
        }

        if (
            $nurseRoom &&
            $this->to_type === Room::class &&
            $this->isApproved()
        ) {
            $nurseExpireTime = TeacherPassSetting::convertInMinutes(
                $nurseRoom->end_time
            );

            if ($this->parent_id === null) {
                $this->update([
                    'pass_status' => Pass::INACTIVE,
                    'flagged_at' => now(),
                    'extended_at' => now(),
                    'completed_at' => $this->approved_at->addMinutes(
                        $nurseExpireTime
                    ),
                    'system_completed' => Pass::ACTIVE,
                    'expired_at' => Carbon::now()
                ]);
            } else {
                $this->update([
                    'pass_status' => Pass::INACTIVE,
                    'flagged_at' => now(),
                    'extended_at' => now(),
                    'completed_at' => $this->parent->approved_at->addMinutes(
                        $nurseExpireTime
                    ),
                    'system_completed' => Pass::ACTIVE,
                    'expired_at' => Carbon::now()
                ]);
            }

            event(
                new PassExpired(
                    Pass::onWriteConnection()
                        ->with('child')
                        ->without('parent')
                        ->where(
                            'id',
                            $this->parent_id !== null
                                ? $this->parent_id
                                : $this->id
                        )
                        ->firstOrFail()
                )
            );

            //            Notification::send($this, new StudentNotification('PassExpired'));
        }

        if (
            (($this->type === Pass::STUDENT_CREATED_PASS ||
                $this->type === Pass::PROXY_PASS ||
                $this->type === Pass::APPOINTMENT_PASS ||
                $this->type === Pass::KIOSK_PASS) &&
                $this->isApproved() &&
                $this->isNotAnExtended() &&
                $this->parent_id === null) ||
            $this->isForChildCompletedBySystem()
        ) {
            $autoExpireTime = TeacherPassSetting::convertInMinutes(
                $teacherPassSettings->auto_expire_time
            );

            if ($this->parent_id !== null && !$this->isCompleted()) {
                if (!$this->isApproved()) {
                    $this->update([
                        'pass_status' => Pass::INACTIVE,
                        'approved_at' => $this->parent->approved_at->addMinutes(
                            $autoExpireTime
                        ),
                        'system_completed' => Pass::SYSTEM_COMPLETED_ACTIVE
                    ]);

                    logger(
                        'The pass with ID: ' .
                            $this->id .
                            ' was expired by extended time (Parent = True) 144 line'
                    );
                }

                $this->update([
                    'pass_status' => Pass::INACTIVE,
                    'completed_at' => $this->parent->approved_at->addMinutes(
                        $autoExpireTime
                    ),
                    'system_completed' => Pass::SYSTEM_COMPLETED_ACTIVE
                ]);

                event(
                    new PassExpired(
                        Pass::onWriteConnection()
                            ->with('child')
                            ->without('parent')
                            ->where(
                                'id',
                                $this->parent_id !== null
                                    ? $this->parent_id
                                    : $this->id
                            )
                            ->firstOrFail()
                    )
                );

                //                Notification::send(
                //                    $this,
                //                    new StudentNotification('PassExpired')
                //                );
            }

            if (
                $this->parent_id === null &&
                TeacherPassSetting::isForExpire(
                    $this->approved_at,
                    $autoExpireTime
                ) &&
                !$this->isCompleted()
            ) {
                $this->update([
                    'pass_status' => Pass::INACTIVE,
                    'completed_at' => $this->approved_at->addMinutes(
                        $autoExpireTime
                    ),
                    'system_completed' => Pass::SYSTEM_COMPLETED_ACTIVE
                ]);

                event(
                    new PassExpired(
                        Pass::onWriteConnection()
                            ->with('child')
                            ->without('parent')
                            ->where(
                                'id',
                                $this->parent_id !== null
                                    ? $this->parent_id
                                    : $this->id
                            )
                            ->firstOrFail()
                    )
                );

                //                Notification::send(
                //                    $this,
                //                    new StudentNotification('PassExpired')
                //                );
            }
        }

        if (
            $this->type === Pass::APPOINTMENT_PASS &&
            !$this->isApproved() &&
            $this->parent_id === null
        ) {
            $awaitingPassTime = TeacherPassSetting::convertInMinutes(
                $teacherPassSettings->awaiting_apt
            );

            $this->update([
                'pass_status' => Pass::INACTIVE,
                'expired_at' => Carbon::now()
            ]);

            event(
                new PassExpired(
                    Pass::onWriteConnection()
                        ->with('child')
                        ->without('parent')
                        ->where(
                            'id',
                            $this->parent_id !== null
                                ? $this->parent_id
                                : $this->id
                        )
                        ->firstOrFail()
                )
            );

            //            Notification::send($this, new StudentNotification('PassExpired'));
        }
        if (
            ($this->type === Pass::STUDENT_CREATED_PASS ||
                $this->type === Pass::KIOSK_PASS) &&
            !$this->isApproved() &&
            $this->parent_id === null
        ) {
            $whitePassTime = TeacherPassSetting::convertInMinutes(
                $teacherPassSettings->white_passes
            );

            logger(
                'The pass with ID' .
                    $this->id .
                    'is for expire White pass time, 198 line.'
            );

            $this->update([
                'pass_status' => Pass::INACTIVE,
                'expired_at' => Carbon::now()
            ]);

            event(
                new PassExpired(
                    Pass::onWriteConnection()
                        ->with('child')
                        ->without('parent')
                        ->where(
                            'id',
                            $this->parent_id !== null
                                ? $this->parent_id
                                : $this->id
                        )
                        ->firstOrFail()
                )
            );

            //            Notification::send($this, new StudentNotification('PassExpired'));
        }

        if (
            $this->type === Pass::PROXY_PASS &&
            !$this->isApproved() &&
            $this->parent_id === null
        ) {
            $whitePassTime = TeacherPassSetting::convertInMinutes(
                $teacherPassSettings->white_passes
            );

            $this->update([
                'pass_status' => Pass::INACTIVE,
                'expired_at' => Carbon::now()
            ]);

            Log::info('Pass Object: ', [$this]);

            event(
                new PassExpired(
                    Pass::onWriteConnection()
                        ->with('child')
                        ->without('parent')
                        ->where(
                            'id',
                            $this->parent_id !== null
                                ? $this->parent_id
                                : $this->id
                        )
                        ->firstOrFail()
                )
            );

            //            Notification::send($this, new StudentNotification('PassExpired'));
        }

        if ($this->expired_at !== null && $this->pass_status) {
            $this->update([
                'pass_status' => Pass::INACTIVE
            ]);

            event(
                new PassExpired(
                    Pass::onWriteConnection()
                        ->with('child')
                        ->without('parent')
                        ->where(
                            'id',
                            $this->parent_id !== null
                                ? $this->parent_id
                                : $this->id
                        )
                        ->firstOrFail()
                )
            );
        }

        /** @var Pass $pass */
        $pass = self::onWriteConnection()
            ->with('parent')
            ->without('child')
            ->where('id', $this->id)
            ->firstOrFail();

        //        Notification::send($pass, new StudentNotification('PassExpired'));

        return new PassResource($pass);
    }

    /**
     * @param Pin|null $pin
     * @param string|null $message
     * @param bool|null $kiosk
     */
    public function addApprovalComment(
        ?Pin $pin,
        ?string $message = null,
        ?bool $kiosk = false
    ) {
        $userId = $kiosk ? auth()->user()->user_id : auth()->user()->id;

        if ($message !== null) {
            if ($this->parent !== null) {
                $this->parent->comments()->create([
                    'comment' => $message,
                    'user_id' => $pin !== null ? $pin->pinnable_id : $userId,
                    'school_id' => auth()->user()->school_id
                ]);
            } else {
                $this->comments()->create([
                    'comment' => $message,
                    'user_id' => $pin !== null ? $pin->pinnable_id : $userId,
                    'school_id' => auth()->user()->school_id
                ]);
            }
            event(new PassCommentCreatedEvent($this, $message));
            //            Notification::send(
            //                $this,
            //                new StudentNotification('PassCommentCreatedEvent', [
            //                    'message' => $message
            //                ])
            //            );
        }
    }

    /**
     * @param null $pin
     * @param string|null $endWith
     * @param bool $child
     * @param int $kioskId
     * @return PassResource|JsonResponse
     */
    public function kioskEnd(
        ?Pin $pin = null,
        ?string $endWith = null,
        bool $child = false,
        int $kioskId = 0
    ) {
        // The action is already done
        if ($this->isCompleted()) {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('The pass is completed already.'),
                    'status' => __('fail')
                ],
                422
            );
        }

        // If the pass is returning parent and pass action "End"
        // is selected, we need to complete the pass and approve it too.
        if ($this->isParent() && !$this->isApproved()) {
            $this->update([
                'approved_at' => Carbon::now(),
                'approved_by' =>
                    optional($pin)->pinnable_id !== null
                        ? $pin->pinnable_id
                        : auth()->user()->user_id,
                'to_teacher_note' => $pin,
                'from_kiosk_id' => $kioskId,
                'pass_status' => Pass::INACTIVE,
                'approved_with' => Pass::BY_KIOSK
            ]);

            $this->addApprovalComment(null, $pin, true);

            $pass = self::with('parent')
                ->without('child')
                ->where('id', $this->id)
                ->firstOrFail();

            event(new PassEnded($pass));
            event(
                new AdminPassEnded(
                    self::with('child')
                        ->without('parent')
                        ->where('id', $this->parent_id)
                        ->firstOrFail()
                )
            );

            $this->refreshDashboard();

            //            Notification::send($pass, new StudentNotification('PassEnded'));

            KioskUser::kioskLogout();

            return new PassResource($pass);
        }

        if (
            !$this->isApproved() &&
            $this->pass_status === Pass::ACTIVE &&
            !$this->isParent()
        ) {
            $this->update([
                'pass_status' => 0,
                'canceled_at' => Carbon::now()
            ]);

            event(new PassCanceled($this, true));
            //            Notification::send($this, new StudentNotification('PassCanceled'));

            return new PassResource($this->refresh());
        }

        if ($this->pass_status === Pass::ACTIVE && $this->isApproved()) {
            $this->update([
                'completed_at' => Carbon::now(),
                'completed_by' =>
                    optional($pin)->pinnable_id !== null
                        ? $pin->pinnable_id
                        : auth()->user()->user_id,
                'to_teacher_note' => $pin,
                'to_kiosk_id' => $kioskId,
                'pass_status' => 0,
                'ended_with' => $endWith
            ]);

            $this->addApprovalComment(null, $pin, true);

            $pass = self::with('parent')
                ->without('child')
                ->where('id', $this->id)
                ->firstOrFail();

            event(new PassEnded($pass));
            event(new AdminPassEnded($pass));

            //            Notification::send($pass, new StudentNotification('PassEnded'));

            KioskUser::kioskLogout();

            return new PassResource($pass);
        } else {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('The pass must be approved first.'),
                    'status' => __('fail')
                ],
                422
            );
        }
    }

    /**
     * @param Pin|null $pin
     * @param string|null $message
     * @param string|null $endWith
     * @param bool $child
     * @return PassResource|JsonResponse
     */
    public function end(
        ?Pin $pin = null,
        ?string $message = null,
        ?string $endWith = null,
        bool $child = false
    ) {
        // The action is already done
        if ($this->isCompleted()) {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('passes.invalid.action'),
                    'status' => __('fail')
                ],
                422
            );
        }

        // If the pass is returning parent and pass action "End"
        // is selected, we need to complete the pass and approve it too.
        if ($this->isParent() && !$this->isApproved()) {
            $this->update([
                'approved_at' => Carbon::now(),
                'approved_by' =>
                    $pin !== null ? $pin->pinnable_id : auth()->user()->id,
                'to_teacher_note' => $message,
                'pass_status' => Pass::INACTIVE
            ]);

            $this->addApprovalComment($pin, $message);

            $pass = self::with('parent')
                ->without('child')
                ->where('id', $this->id)
                ->firstOrFail();

            event(new PassEnded($pass));
            event(
                new AdminPassEnded(
                    self::with('child')
                        ->without('parent')
                        ->where('id', $this->parent_id)
                        ->firstOrFail()
                )
            );

            $this->refreshDashboard();

            //            Notification::send($pass, new StudentNotification('PassEnded'));

            return new PassResource($pass);
        }

        if ($this->isApproved() && $this->pass_status === Pass::ACTIVE) {
            $updated = $this->update([
                'completed_at' => Carbon::now(),
                'completed_by' =>
                    $pin !== null ? $pin->pinnable_id : auth()->user()->id,
                'to_teacher_note' => $message,
                'pass_status' => Pass::INACTIVE,
                'ended_with' => $endWith
            ]);

            $this->addApprovalComment($pin, $message);

            $pass = self::with('parent')
                ->without('child')
                ->where('id', $this->id)
                ->firstOrFail();

            event(new PassEnded($pass));
            event(
                new AdminPassEnded(
                    self::with('child')
                        ->without('parent')
                        ->where('id', $this->parent_id ?: $this->id)
                        ->firstOrFail()
                )
            );

            $this->refreshDashboard();

            event(
                new DashboardRefreshEvent(
                    auth()->user()->school_id,
                    User::find($pass->completed_by)
                )
            );
            //            Notification::send($pass, new StudentNotification('PassEnded'));

            return new PassResource($pass);
        } else {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('passes.invalid.action'),
                    'status' => __('fail')
                ],
                422
            );
        }
    }

    /**
     * @param Pin|null $pin
     * @param string|null $message
     * @param bool $checkIn
     * @param bool $child
     * @return PassResource|JsonResponse
     */
    public function arrive(
        ?Pin $pin = null,
        ?string $message = null,
        bool $checkIn = false,
        bool $child = false
    ) {
        // The action is already done
        if (
            $this->isArrived() ||
            $this->isCompleted() ||
            !$this->isApproved() ||
            $this->isParent()
        ) {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('passes.invalid.action'),
                    'status' => __('fail')
                ],
                422
            );
        }

        // Check if the destination is room
        if ($this->to_type === Room::class) {
            $room = Room::find($this->to_id);
            // Check if user has permission to make Layouver pass
            if ($room->trip_type != Room::TRIP_LAYOVER) {
                return response()->json(
                    [
                        'data' => ['id' => $this->id],
                        'message' => __('passes.destination.not.layover'),
                        'status' => __('fail')
                    ],
                    422
                );
            }
        }

        $this->update([
            'arrived_at' => Carbon::now(),
            'completed_by' =>
                $pin !== null ? $pin->pinnable_id : auth()->user()->id,
            'completed_at' => Carbon::now(),
            'to_teacher_note' => $message,
            'ended_with' => $checkIn ? Pass::BY_CHECKIN : null,
            'pass_status' => 0
        ]);

        if ($this->isArrived()) {
            $passWithParent = self::createParent($this, null, null, 1);

            event(new PassArrived($passWithParent['student']));

            event(new AdminPassArrived($passWithParent['admin']));
            //            Notification::send(
            //                $passWithParent['student'],
            //                new StudentNotification('PassArrived')
            //            );

            $this->addApprovalComment($pin, $message);
            $this->refreshDashboard();

            return new PassResource($passWithParent['student']);
        } else {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('passes.user.not.arrived'),
                    'status' => __('fail')
                ],
                422
            );
        }
    }

    /**
     * @param Pin|null $pin
     * @param string|null $message
     * @param string $type | 'return' || 'inout' default: return
     * @param bool $child
     * @return PassResource|JsonResponse
     */
    public function returningOrInOut(
        ?Pin $pin = null,
        ?string $message = null,
        string $type = 'return',
        bool $child = false
    ) {
        if ($type === 'inout' && !$this->isArrived() && !$this->isParent()) {
            if (!$this->isApproved()) {
                return response()->json(
                    [
                        'data' => ['id' => $this->id],
                        'message' => __('passes.invalid.action'),
                        'status' => __('fail')
                    ],
                    422
                );
            }

            if ($this->to_type === Room::class) {
                $room = Room::where('id', $this->to_id)
                    ->fromSchoolId($this->school_id)
                    ->first();
                if (
                    (($room !== null &&
                        $room->trip_type === Room::TRIP_ONE_WAY) ||
                        $room->trip_type === Room::TRIP_ROUNDTRIP) &&
                    !$this->isParent()
                ) {
                    return response()->json([
                        'data' => ['id' => $this->id],
                        'message' => __('passes.invalid.action'),
                        'status' => __('fail')
                    ]);
                }
            }

            $this->update([
                'arrived_at' => Carbon::now(),
                'completed_by' =>
                    $pin !== null ? $pin->pinnable_id : auth()->user()->id,
                'completed_at' => Carbon::now(),
                'to_teacher_note' => $message,
                'pass_status' => 0
            ]);

            $this->addApprovalComment($pin, $message);

            $passWithParent = self::createParent(
                $this,
                true,
                $pin !== null ? $pin->pinnable_id : auth()->user()->id,
                1
            );
            event(new PassInOut($passWithParent['student']));
            event(new AdminPassInOut($passWithParent['admin']));

            //            Notification::send(
            //                $passWithParent,
            //                new StudentNotification('PassInOut')
            //            );

            return new PassResource($passWithParent['student']);
        }

        if ($type === 'return' && $this->isParent()) {
            if ($this->isParent()) {
                // The pass has already done action: In/Out so it can't be returned again.
                if ($this->isApproved()) {
                    return response()->json(
                        [
                            'data' => ['pass_id' => $this->id],
                            'message' => __('passes.invalid.action'),
                            'status' => __('fail')
                        ],
                        422
                    );
                }

                if ($this->to_type === Room::class) {
                    $room = Room::where('id', $this->to_id)
                        ->fromSchoolId($this->school_id)
                        ->first();
                    if (
                        (($room !== null &&
                            $room->trip_type === Room::TRIP_ONE_WAY) ||
                            $room->trip_type === Room::TRIP_ROUNDTRIP) &&
                        !$this->isParent()
                    ) {
                        return response()->json([
                            'data' => ['id' => $this->id],
                            'message' => __('passes.invalid.action'),
                            'status' => __('fail')
                        ]);
                    }
                }

                $parentPass = self::find($this->parent_id);
                if ($parentPass->isCompleted()) {
                    $this->update([
                        'approved_at' => Carbon::now(),
                        'approved_by' =>
                            $pin !== null
                                ? $pin->pinnable_id
                                : auth()->user()->id,
                        'from_teacher_note' => $message
                    ]);

                    $this->addApprovalComment($pin, $message);

                    $pass = self::with('parent')
                        ->without('child')
                        ->where('id', $this->id)
                        ->firstOrFail()
                        ->append(['expire_after', 'flags']);

                    event(new PassReturning($pass));
                    event(
                        new AdminPassReturning(
                            self::with('child')
                                ->without('parent')
                                ->where('id', $this->parent_id)
                                ->firstOrFail()
                                ->append(['expire_after', 'flags'])
                        )
                    );

                    //                    Notification::send(
                    //                        $pass,
                    //                        new StudentNotification('PassReturning')
                    //                    );

                    return new PassResource($pass);
                }
            }
        }

        return response()->json(
            [
                'data' => ['id' => $this->id],
                'message' => __('passes.invalid.action'),
                'status' => __('fail')
            ],
            422
        );
    }

    /**
     * @param PassRequest $request
     * @param bool $kiosk
     * @return JsonResponse|null
     */
    public static function validate(
        PassRequest $request,
        bool $kiosk = false
    ): ?JsonResponse {
        $user = auth()->user();
        $kioskUserId = optional($user)->user_id;

        $activePasses = self::active()
            ->fromSchoolId($user->school_id)
            ->ofUserId($kiosk ? $kioskUserId : $user->id)
            ->active()
            ->exists();

        $passesWithActiveStatus = self::wherePassStatus(Pass::ACTIVE)
            ->fromSchoolId($user->school_id)
            ->ofUserId($kiosk ? $kioskUserId : $user->id)
            ->exists();

        if ($activePasses || $passesWithActiveStatus) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('passes.already.has.active.pass'),
                    'status' => __('fail')
                ],
                422
            );
        }

        // Get the staff for auto pass limit validations
        $staffFromId =
            $request->input('from_type') === User::class
                ? $request->input('from_id')
                : null; // 0 if the from_type is not User
        $staffFrom = User::fromStaff()
            ->fromSchoolId($user->school_id)
            ->where('is_searchable', User::USER_SEARCHABLE)
            ->where('id', $staffFromId)
            ->first();
        $staffToId =
            $request->input('to_type') === User::class
                ? $request->input('to_id')
                : null; // 0 if the to_type is not User
        $staffTo = User::fromStaff()
            ->fromSchoolId($user->school_id)
            ->where('is_searchable', User::USER_SEARCHABLE)
            ->where('id', $staffToId)
            ->first();

        // Get the room for auto pass limit validations
        $roomToId =
            $request->input('to_type') === Room::class
                ? $request->input('to_id')
                : null; // 0 if the from_type is not Room
        $roomTo = Room::fromSchoolId($user->school_id)
            ->where('id', $roomToId)
            ->first();

        // Validation check
        $blockPass = PassPolicy::checkBlockPass($user, 'student_pass');
        $autoPassLimit = PassPolicy::checkAutoPassLimit($staffFrom, $roomTo);
        $staffUnavailability = PassPolicy::checkUnavailable(
            $request->input('to_type'),
            $request->input('to_id')
        );
        $locationCapacity = PassPolicy::checkLocationCapacity($roomToId);
        $adultOptionFrom = PassPolicy::checkIfAdultNotAcceptPassToHim(
            $staffFrom
        );
        $adultOptionTo = PassPolicy::checkIfAdultNotAcceptPassToHim($staffTo);
        $schoolOrStudentPassLimit = PassPolicy::checkPassLimit($kiosk);

        $schoolLocationsRestrictionsIds = RoomRestriction::getRestrictedRoomIds(
            $kiosk ? User::find($kioskUserId) : null
        );

        $activePassLimit = PassPolicy::checkActivePassLimit($kiosk);

        $policyValidator = PassPolicy::policyValidation(
            $blockPass,
            $autoPassLimit,
            $staffUnavailability,
            $locationCapacity,
            $adultOptionFrom,
            $adultOptionTo,
            $schoolOrStudentPassLimit,
            $schoolLocationsRestrictionsIds,
            $roomTo,
            $activePassLimit
        );

        if ($policyValidator) {
            return response()->json($policyValidator, 422);
        }

        return null;
    }

    /**
     * @return null|JsonResponse
     */
    public function cancelValidator()
    {
        if (
            $this->isCanceled() ||
            $this->isCompleted() ||
            $this->isApproved()
        ) {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('passes.invalid.action'),
                    'status' => __('fail')
                ],
                422
            );
        }

        if ($this->type === Pass::APPOINTMENT_PASS || $this->isParent()) {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('passes.cancel.fail'),
                    'status' => __('fail')
                ],
                422
            );
        }

        return null;
    }

    /**
     * @param AutoCheckInRequest $request
     * @return PassResource|JsonResponse
     */
    public function checkIn(AutoCheckInRequest $request)
    {
        $roomPinExists = Pin::where('pinnable_id', $this->to_id)
            ->where('pinnable_type', Room::class)
            ->where('school_id', auth()->user()->school_id)
            ->where('pin', $request->input('room_pin'))
            ->exists();

        if ($this->isExpired()) {
            return response()->json([
                'data' => [],
                'message' => __('passes.invalid.action'),
                'status' => __('fail')
            ]);
        }

        if (!$roomPinExists) {
            // Log the user into pin attempt
            $createPinAttempt = PinAttempt::create([
                'school_id' => auth()->user()->school_id,
                'pass_id' => $this->id,
                'user_id' => auth()->user()->id
            ]);

            event(
                new PinStatusEvent(
                    auth()->user()->id,
                    auth()->user()->pin_attempts_count,
                    auth()->user()->pin_attempts_count >=
                        PinAttempt::LOGOUT_PIN_ATTEMPTS,
                    auth()->user()->pin_attempts_count ===
                        PinAttempt::LOGOUT_PIN_ATTEMPTS
                )
            );

            //            Notification::send(
            //                $this,
            //                new StudentNotification('PinStatusEvent', [
            //                    'userId' => auth()->user()->id,
            //                    'pinAttempts' => auth()->user()->pin_attempts_count,
            //                    'showPinMessage' =>
            //                        auth()->user()->pin_attempts_count >=
            //                        PinAttempt::LOGOUT_PIN_ATTEMPTS,
            //                    'locked' =>
            //                        auth()->user()->pin_attempts_count ===
            //                        PinAttempt::LOGOUT_PIN_ATTEMPTS
            //                ])
            //            );

            if (
                auth()->user()->pin_attempts_count ===
                PinAttempt::LOGOUT_PIN_ATTEMPTS
            ) {
                auth()
                    ->user()
                    ->token()
                    ->delete();
            }

            return response()->json(
                [
                    'data' => [
                        'id' => $this->id,
                        'logout' =>
                            auth()->user()->pin_attempts_count ===
                            PinAttempt::LOGOUT_PIN_ATTEMPTS
                    ],
                    'message' => __('pin.invalid.checkin'),
                    'status' => __('fail')
                ],
                422
            );
        }
        $room = Room::where('id', $this->to_id)
            ->where('school_id', $this->school_id)
            ->first();

        if ($room->trip_type === Room::TRIP_LAYOVER) {
            return $this->arrive(null, null, true);
        }

        return $this->end(null, null, Pass::BY_CHECKIN);
    }

    /**
     * @param $value
     */
    public function setArrivedAtAttribute($value)
    {
        $this->attributes['arrived_at'] =
            $value !== null
                ? Carbon::parse($value)->setTimezone(config('app.timezone'))
                : null;
    }

    /**
     * @return bool
     */
    public function isNotAnExtended(): bool
    {
        if ($this->to_type === Room::class) {
            return !NurseRoom::whereSchoolId($this->school_id)
                ->where('room_id', $this->to_id)
                ->exists();
        }

        return true;
    }

    public function setRequestedAtAttribute($value)
    {
        $this->attributes['requested_at'] =
            $value !== null
                ? Carbon::parse($value)->setTimezone(config('app.timezone'))
                : null;
    }

    public function setCompletedAtAttribute($value)
    {
        $this->attributes['completed_at'] =
            $value !== null
                ? Carbon::parse($value)->setTimezone(config('app.timezone'))
                : null;
    }

    public function setCanceledAtAttribute($value)
    {
        $this->attributes['canceled_at'] =
            $value !== null
                ? Carbon::parse($value)->setTimezone(config('app.timezone'))
                : null;
    }

    public function setApprovedAtAttribute($value)
    {
        $this->attributes['approved_at'] =
            $value !== null
                ? Carbon::parse($value)->setTimezone(config('app.timezone'))
                : null;
    }

    public function setExpiredAtAttribute($value)
    {
        $this->attributes['expired_at'] =
            $value !== null
                ? Carbon::parse($value)->setTimezone(config('app.timezone'))
                : null;
    }

    /**
     * Adding child relationship to get all of the passes that has parent.
     *
     * @return BelongsTo
     */
    public function child()
    {
        return $this->belongsTo(self::class, 'id', 'parent_id');
    }

    /**
     * @return bool
     */
    public function isCompletedBySystem(): bool
    {
        if ($this->child) {
            if ($this->child->system_completed !== Pass::INACTIVE) {
                return true;
            }
        }

        return $this->system_completed !== 0;
    }

    /**
     * @return mixed
     */
    public function getIsFlaggedAttribute()
    {
        return $this->isCompletedBySystem() ||
            $this->flagged_at !== null ||
            $this->extended_at !== null;
    }

    /**
     * @return array
     */
    public function getFlagsAttribute()
    {
        $teacherPassSettings = TeacherPassSetting::fromSchoolId(
            optional(auth()->user())->school_id
        )->first();

        $autoPass =
            $this->to_type === Room::class &&
            AutoPass::ofRoomId($this->to_id)
                ->where('school_id', optional(auth()->user())->school_id)
                ->where('status', AutoPass::ACTIVE)
                ->exists()
                ? AutoPass::ofRoomId($this->to_id)
                    ->where('school_id', optional(auth()->user())->school_id)
                    ->first()
                : null;

        $autoPassLimit =
            $this->from_type === User::class
                ? AutoPassLimit::where('user_id', $this->from_id)
                    ->where('school_id', $this->school_id)
                    ->first()
                : null;

        $autoPassPreference = AutoPassPreference::where(
            'auto_pass_id',
            optional($autoPass)->id
        )
            ->ofUserId($this->from_id)
            ->first();
        $autoCheckInRoomValid = Pin::wherePinnableType(Room::class)
            ->where('pinnable_id', $this->to_id)
            ->exists();

        return [
            'settings' => $teacherPassSettings,
            'auto_pass' => [
                'is_valid' =>
                    $autoPass !== null &&
                    optional($autoPassLimit)->limit >= 0 &&
                    optional($autoPassLimit)->limit !== null &&
                    optional($autoPassPreference)->mode !== null &&
                    !$this->isParent(),
                'mode' => optional($autoPassPreference)->mode
            ],
            'auto_check_in' => [
                'is_valid' =>
                    $autoCheckInRoomValid &&
                    !$this->isParent() &&
                    $this->to_type === Room::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function getBadgeFlagsAttribute(): array
    {
        //Before change in future:
        //How it works now is that the pass stays yellow until it is system ended at the time of the "extended pass" time.
        //And then the blue background is seen on the ended pass.
        //In other words, the indication that it is an extended pass location is only visible if it has been system ended at that later time.
        //If it is ended while it is still green or yellow - there is no indication that it is an extended pass location.
        //I think it should stay this way.  If it is ended while it is green or yellow that is what we want to bring attention to - either no flag,
        //or a yellow flag.
        // If it is ended at the extended time then it should have a purple flag.
        //  So if yellow is 5 mins, system ended is 10 and extended time is 15 - the pass would show as green until min 5 when it turns to yellow,
        // it would not change at min 10, but if it went to min 15 it would get a purple flag and be extended system ended.
        // Please let me know if that is clear.
        //(wondering if in V2.1 we might have some indication on the pass why it is running beyond the system ended time...but maybe that is just some instructional text and not a symbol on the pass, in the above example from min 10 to 15 to explain why it is still running. Just a thought)
        //We are also concerned with the scrollbar needing several days to be fixed and not being able to test or confirm any tickets while we wait.  Do you see the loss of the last few days and today affecting the deployment date? Please share your thoughts.
        //I see work has begun on the Help text doc I provided.  I will address those comments today.
        //Please confirm on Monday that the system is ready for me.  Thanks!
        //From: Karen
        return [
            'system_completed' =>
                ($this->child === null &&
                    $this->isCompletedBySystem() &&
                    $this->extended_at === null) ||
                $this->childSystemCompleted() ||
                (optional($this->child)->type === self::APPOINTMENT_PASS &&
                    optional($this->child)->expired_at !== null &&
                    !optional($this->child)->system_completed &&
                    optional($this->child)->extended_at === null),
            'extended' => $this->isExtended(),
            'min_time' => $this->minTimeReached() && !$this->isExtended(),
            'ended' =>
                $this->child !== null
                    ? $this->isChildEnded() ||
                        ($this->child->isExtended() &&
                            $this->child->pass_status === Pass::INACTIVE)
                    : ($this->isCompleted() &&
                            $this->pass_status === Pass::INACTIVE &&
                            !$this->isCanceled() &&
                            !$this->isExpired()) ||
                        $this->isExtended(),
            'waiting_approval' =>
                !$this->isApproved() &&
                !$this->isExpired() &&
                !$this->isCanceled() &&
                $this->pass_status === Pass::ACTIVE,
            'missed' =>
                $this->expired_at !== null &&
                $this->pass_status === Pass::INACTIVE &&
                !$this->isCompletedBySystem() &&
                !$this->isApproved(),
            'canceled' => $this->isCanceled(),
            'edited' => $this->edited_at !== null,
            'active' =>
                ($this->pass_status === Pass::ACTIVE && $this->isApproved()) ||
                ($this->child !== null &&
                    $this->child->pass_status === Pass::ACTIVE),
            'missed_request' => false // TODO: Deprecated remove in v2.1
        ];
    }

    /**
     * @return bool
     */
    public function minTimeReached(): bool
    {
        return $this->reachedNormalMinTime() ||
            $this->childMinTime() ||
            $this->extendedWhileActive() ||
            $this->minTimeWhileParentPassIsFlagged() ||
            $this->minTimeWhileChildPassIsFlagged();
    }

    /**
     * @return bool
     */
    public function reachedNormalMinTime(): bool
    {
        return ($this->flagged_at !== null && $this->child === null) ||
            ($this->child !== null && $this->flagged_at !== null);
    }

    /**
     * @return bool
     */
    public function extendedWhileActive(): bool
    {
        return $this->extended_at !== null &&
            $this->flagged_at !== null &&
            $this->pass_status === Pass::ACTIVE;
    }

    /**
     * @return bool
     */
    public function minTimeWhileParentPassIsFlagged(): bool
    {
        return $this->child !== null &&
            $this->extended_at !== null &&
            $this->flagged_at !== null;
    }

    /**
     * @return bool
     */
    public function minTimeWhileChildPassIsFlagged(): bool
    {
        return $this->child !== null &&
            $this->child->extended_at !== null &&
            $this->child->flagged_at !== null &&
            $this->child->pass_status === Pass::ACTIVE;
    }

    /**
     * @return bool
     */
    public function childMinTime(): bool
    {
        return $this->child !== null &&
            ($this->child->flagged_at !== null &&
                $this->child->extended_at === null);
    }

    /**
     * @return bool
     */
    public function childSystemCompleted(): bool
    {
        return $this->isCompletedBySystem() &&
            $this->extended_at === null &&
            optional($this->child)->isCompletedBySystem() &&
            optional($this->child)->extended_at === null;
    }

    /**
     * @return bool
     */
    public function isExtended(): bool
    {
        if ($this->child) {
            if (
                $this->child->extended_at !== null &&
                $this->child->isCompletedBySystem() &&
                $this->pass_status === Pass::INACTIVE
            ) {
                return true;
            }
        }

        return (($this->extended_at !== null &&
            $this->isCompleted() &&
            $this->isCompletedBySystem()) ||
            ($this->extended_at !== null && $this->isCompletedBySystem())) &&
            $this->pass_status === Pass::INACTIVE;
    }

    /**
     * @return bool
     */
    public function isChildEnded(): bool
    {
        if (
            $this->child->isApproved() &&
            !$this->child->isCompleted() &&
            $this->child->pass_status === Pass::INACTIVE &&
            !$this->child->isCanceled() &&
            !$this->child->isExpired()
        ) {
            return true;
        }
        return $this->child->pass_status === Pass::INACTIVE &&
            !$this->child->isCanceled() &&
            !$this->child->isExpired() &&
            $this->child->isCompleted();
    }

    /**
     * @return bool
     */
    public function isForChildCompletedBySystem(): bool
    {
        return ($this->type === Pass::STUDENT_CREATED_PASS ||
            $this->type === Pass::PROXY_PASS ||
            $this->type === Pass::APPOINTMENT_PASS ||
            $this->type === Pass::KIOSK_PASS) &&
            $this->isNotAnExtended() &&
            $this->isNotExtendedChild() &&
            $this->isParent();
    }

    public function isNotExtendedChild()
    {
        if ($this->from_type === Room::class && $this->parent_id !== null) {
            return !NurseRoom::whereSchoolId($this->school_id)
                ->where('room_id', $this->from_id)
                ->exists();
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function latestComment()
    {
        return $this->morphOne(Comment::class, 'commentable')->latest();
    }

    /**
     * @return \Illuminate\Database\Eloquent\HigherOrderBuilderProxy|mixed|string
     * TODO: Add caching.
     */
    public function getExpireAfterAttribute()
    {
        $nurseTimeSettings = NurseRoom::where('room_id', $this->to_id)->first();
        $nurseTimeSettingsFrom = NurseRoom::where(
            'room_id',
            $this->from_id
        )->first();
        $teacherTimeSettings = TeacherPassSetting::where(
            'school_id',
            $this->school_id
        )->first();

        if (
            $nurseTimeSettings &&
            $this->to_type === Room::class &&
            $this->isApproved()
        ) {
            return $nurseTimeSettings->end_time;
        }

        if (
            $nurseTimeSettingsFrom &&
            $this->from_type === Room::class &&
            $this->parent_id !== null
        ) {
            return $nurseTimeSettingsFrom->end_time;
        }

        if (
            $nurseTimeSettings &&
            $this->to_type === Room::class &&
            $this->parent_id !== null
        ) {
            return $nurseTimeSettings->end_time;
        }

        if (
            ($this->type === Pass::STUDENT_CREATED_PASS ||
                $this->type === Pass::PROXY_PASS) &&
            $this->isApproved() &&
            $this->isNotAnExtended()
        ) {
            return $teacherTimeSettings->auto_expire_time;
        }

        if (
            $this->type === Pass::APPOINTMENT_PASS &&
            !$this->isApproved() &&
            $this->parent_id === null
        ) {
            return $teacherTimeSettings->awaiting_apt;
        }

        if (
            ($this->type === Pass::STUDENT_CREATED_PASS ||
                $this->type === Pass::PROXY_PASS ||
                $this->type === Pass::KIOSK_PASS) &&
            !$this->isApproved() &&
            $this->parent_id === null
        ) {
            return $teacherTimeSettings->white_passes;
        }

        return $teacherTimeSettings->auto_expire_time;
    }

    /**
     * @param $query
     * @param bool $all
     * @return mixed
     */
    public function scopeCalculateTotalTimeBySql($query, bool $all = false)
    {
        return $query
            ->selectRaw(
                'CASE
                       WHEN child.completed_at is not null THEN SEC_TO_TIME(SUM(TIME_TO_SEC(child.completed_at) - TIME_TO_SEC(passes.approved_at)))
                       WHEN child.approved_at is not null AND child.completed_at is null
                       THEN SEC_TO_TIME(SUM(TIME_TO_SEC(child.approved_at) - TIME_TO_SEC(passes.approved_at)))
                       ELSE SEC_TO_TIME(SUM(TIME_TO_SEC(passes.completed_at) - TIME_TO_SEC(passes.approved_at)))
                       END'
            )
            ->leftJoin('passes as child', 'passes.id', '=', 'child.parent_id')
            ->from('passes', 'passes');
    }

    /**
     * @param $query
     * @param $table
     * @param $model
     */
    public function scopeQueryByModel($query, $table, $model)
    {
        $query
            ->when($model === User::class, function ($query) use (
                $table,
                $model
            ) {
                $query->where(function ($query) use ($table, $model) {
                    $query
                        ->whereColumn('passes.approved_by', "$table.id")
                        ->orWhereColumn('passes.completed_by', "$table.id");
                });
            })
            ->when($model === Room::class, function ($query) use ($table) {
                $query->whereColumn('passes.to_id', "$table.id");
            });
    }

    /**
     * Return if the pass is with yellow flag (Min Time reached)
     * @param bool $child
     * @return bool
     */
    public function isReachedMinTime(bool $child = false): bool
    {
        $teacherPassSetting = TeacherPassSetting::where(
            'school_id',
            $this->school_id
        )->first();
        $minTime = TeacherPassSetting::convertInMinutes(
            optional($teacherPassSetting)->min_time
        );

        if ($child) {
            return $this->parent->isApproved() &&
                !$this->isCompleted() &&
                $this->parent->approved_at->addMinutes($minTime)->isPast();
        }

        return $this->isApproved() &&
            !$this->isCompleted() &&
            $this->approved_at->addMinutes($minTime)->isPast();
    }

    /**
     * Run massive contact tracing for contact reports.
     *
     * @param $userId
     * @param $fromDate
     * @param $toDate
     * @return array
     */
    public static function runContactTracing($userId, $fromDate, $toDate): array
    {
        $user = auth()->user();

        return DB::select(
            "CALL run_contact_tracing($userId, $user->school_id, '$fromDate', '$toDate')"
        );
    }

    /**
     * @param int $perPage
     * @param bool $all
     * @param null $filters
     * @param bool $forDashboard
     * @param bool $pagination
     * @return Pass|LengthAwarePaginator|\Illuminate\Database\Query\Builder|SearchBuilder
     */
    public static function searchableQueries(
        int $perPage = 25,
        bool $all = false,
        $filters = null,
        bool $forDashboard = false,
        bool $pagination = true
    ) {
        $searchQuery = !$all
            ? convertElasticQuery(request()->get('search_query'))
            : config('scout_elastic.all_records');
        $user = auth()->user();

        $staffSchedules = StaffSchedule::where('user_id', $user->id)
            ->get()
            ->pluck('room_id')
            ->toArray();

        $staffSchedulesFromRoomIds = count($staffSchedules)
            ? elasticWhereInRawString('from_id', $staffSchedules)
            : 0;
        $staffSchedulesToRoomIds = count($staffSchedules)
            ? elasticWhereInRawString('to_id', $staffSchedules)
            : 0;
        $elasticRoomsAssociated = '';

        if ($staffSchedulesToRoomIds && $staffSchedulesFromRoomIds) {
            $elasticRoomsAssociated = "OR (from_type:*Room* AND ($staffSchedulesFromRoomIds))
         OR (to_type:*Room* AND ($staffSchedulesToRoomIds))";
        }

        $elasticTransparencyString = " AND ((from_type:*User* AND from_id:$user->id)
         OR (to_type:*User* AND to_id:$user->id) OR (created_by:$user->id) OR (approved_by:$user->id)
         OR (child.approved_by:$user->id) OR (child.completed_by:$user->id)
         OR (child.from_type:*User* AND child.from_id:$user->id)
         OR (child.to_type:*User* AND child.to_id:$user->id) OR (child.created_by:$user->id)
         OR (completed_by:$user->id) $elasticRoomsAssociated)";

        if (Transparency::isEnabled() && !request()->has('only_active')) {
            $searchQuery .= $elasticTransparencyString;
        }

        $conditionRule = !$forDashboard
            ? PassHistoryFilterSearchRule::class
            : DashboardFilterSearchRule::class;

        $searchResults = self::searchBuilder($searchQuery, $conditionRule);

        if (!$pagination) {
            $resultsIds = $searchResults
                ->take($perPage)
                ->get()
                ->pluck('id')
                ->toArray();

            $explodedResultsIds = implode(',', $resultsIds);

            $csvQuery = Pass::whereIntegerInRaw('id', $resultsIds)->orderByRaw(
                "field(id,$explodedResultsIds)"
            );
        }

        return $pagination ? $searchResults->paginate($perPage) : $csvQuery;
    }

    /**
     * @param string $searchQuery
     * @param string $conditionRule
     * @return SearchBuilder
     */
    public static function searchBuilder(
        string $searchQuery,
        string $conditionRule
    ): SearchBuilder {
        if ($searchQuery === '*') {
            $searchQuery = '__all';
        }
        return self::search($searchQuery)
            ->rule($conditionRule)
            ->when(request()->has('sort'), function ($query) {
                $sort = explode(':', request()->get('sort'));

                if (count($sort) <= 1) {
                    return $query->orderBy('id', 'asc');
                }

                if ($sort[0] === Pass::ATTRIBUTE_TOTAL_TIME_COLUMN) {
                    return $query->orderBy('total_time', $sort[1]);
                }

                $sortingKeywords = ['asc', 'desc'];
                if (in_array($sort[1], $sortingKeywords)) {
                    return $query->orderBy($sort[0], $sort[1]);
                }
                return $query->orderBy('id', 'asc');
            });
    }

    /**
     * @param $filters
     * @param int $perPage
     * @param bool $pagination
     * @param bool $forDashboard
     * @return Builder|LengthAwarePaginator
     */
    public static function nonSearchableQueries(
        $filters,
        int $perPage = 24,
        bool $pagination = true,
        bool $forDashboard = false
    ) {
        if (request()->has('sort') || request()->has('search_query')) {
            return self::searchableQueries(
                Pass::recordsPerPage(),
                !request()->has('search_query'),
                $filters,
                $forDashboard,
                $pagination
            );
        } else {
            if (!Transparency::isEnabled()) {
                $passes = !$forDashboard
                    ? self::with('child')
                        ->select('passes.*')
                        ->useIndex(self::PASS_HISTORY_MYSQL_INDEX)
                        ->where('passes.school_id', auth()->user()->school_id)
                        ->where('passes.pass_status', Pass::INACTIVE)
                        ->withoutMissed()
                        ->where('passes.parent_id', null)
                        ->orderBy('passes.created_at', 'desc')
                        ->leftJoin(
                            'passes as child',
                            'passes.id',
                            '=',
                            'child.parent_id'
                        )
                        ->filter($filters)
                    : self::with('child')
                        ->select('passes.*')
                        ->where('passes.school_id', auth()->user()->school_id)
                        ->where('passes.parent_id', null)
                        ->leftJoin(
                            'passes as child',
                            'passes.id',
                            '=',
                            'child.parent_id'
                        )
                        ->filter($filters);
            } else {
                $user = auth()->user();

                $staffSchedulesFromRoomIds = StaffSchedule::where(
                    'user_id',
                    $user->id
                )
                    ->get()
                    ->pluck('room_id');

                $staffSchedulesToRoomIds = StaffSchedule::where(
                    'user_id',
                    $user->id
                )
                    ->get()
                    ->pluck('room_id');

                $passes = self::with('child')
                    ->select('passes.*')
                    ->useIndex(self::PASS_HISTORY_MYSQL_INDEX)
                    ->where('passes.school_id', $user->school_id)
                    ->where('passes.parent_id', null)
                    ->orderBy('passes.created_at', 'desc')
                    ->leftJoin(
                        'passes as child',
                        'passes.id',
                        '=',
                        'child.parent_id'
                    )
                    ->transparencyWithColumnPrefix(
                        $user,
                        $staffSchedulesFromRoomIds,
                        $staffSchedulesToRoomIds
                    )
                    ->filter($filters);

                if (!$forDashboard) {
                    $passes
                        ->withoutMissed()
                        ->where('passes.pass_status', Pass::INACTIVE);
                }
            }
        }

        return $pagination
            ? $passes->filter($filters)->paginate($perPage)
            : $passes;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithoutMissed($query)
    {
        return $query->where(function ($query) {
            $query
                ->where('passes.approved_at', '!=', null)
                ->where('passes.canceled_at', null)
                ->orWhere('passes.approved_at', null)
                ->where('passes.canceled_at', '!=', null)
                ->orWhere('passes.approved_at', null)
                ->where('passes.type', Pass::APPOINTMENT_PASS)
                ->where('passes.expired_at', '!=', null);
        });
    }

    /**
     * @return BelongsTo
     */
    public function createdByKiosk(): BelongsTo
    {
        return $this->belongsTo(Kiosk::class, 'created_by_kiosk');
    }

    /**
     * @return BelongsTo
     */
    public function fromKiosk(): BelongsTo
    {
        return $this->belongsTo(Kiosk::class, 'from_kiosk_id');
    }

    /**
     * @return BelongsTo
     */
    public function toKiosk(): BelongsTo
    {
        return $this->belongsTo(Kiosk::class, 'to_kiosk_id');
    }

    /**
     * @param $query
     * @param $indexName
     * @return mixed
     */
    public function scopeUseIndex($query, $indexName)
    {
        return $query->from(
            DB::raw('`passes` USE INDEX(`' . $indexName . '`)')
        );
    }

    public function getTopics()
    {
        $env = config('services.fcm.env');
        //dev.
        //local.
        //staging.
        //production.
        return "{$env}.user.passes.{$this->user_id}";
    }

    public function getNotificationTextAndMessages($event, $options = null)
    {
        switch ($event) {
            case 'PassApproved':
                return [
                    'title' => __('Your pass has been approved!'),
                    'body' =>
                        $options !== null
                            ? Carbon::parse($options['approvedAt'])->format(
                                'm/d/Y g:i A'
                            )
                            : ' '
                ];
            case 'PassCommentCreatedEvent':
                return [
                    'title' => __('A comment has been added to your pass.'),
                    'body' => 'Your comment has been created.'
                ];
            case 'PassCreated':
                return [
                    'title' => 'Your pass has successfully been created!',
                    'body' => 'Go to the app for further details.'
                ];
            case 'PassArrived':
                return [
                    'title' => 'Your pass was marked as arrived.',
                    'body' => 'Go to the app for further details.'
                ];
            case 'PassReturning':
            case 'PassInOut':
                return [
                    'title' => 'Your pass was marked as returning.',
                    'body' => 'Go to the app for further details.'
                ];
            case 'PassExpired':
                return [
                    'title' => 'Your pass expired.',
                    'body' => 'Go to the app for further details.'
                ];
            case 'PassCanceled':
                return [
                    'title' => 'Your pass was canceled.',
                    'body' => 'Go to the app for further details.'
                ];
            case 'PassEnded':
                return [
                    'title' => 'Your pass was ended.',
                    'body' => 'Go to the app for further details.'
                ];
        }

        return [
            'title' => '',
            'body' => ''
        ];
    }

    public function getNotificationTitle($event, $options = null)
    {
        $formattedMessages = $this->getNotificationTextAndMessages(
            $event,
            $options
        );

        return optional($formattedMessages)['title'];
    }

    public function getNotificationBody($event, $options = null)
    {
        $formattedMessages = $this->getNotificationTextAndMessages(
            $event,
            $options
        );

        return optional($formattedMessages)['body'];
    }

    public function getData($event, $options)
    {
        return [
            'event' => $event,
            'payload' => [
                'id' => $this->id
            ],
            'options' => $options
        ];
    }

    public function getNotificationSettings($event, $options)
    {
        return [
            'topics' => $this->getTopics(),
            'title' => $this->getNotificationTitle($event, $options),
            'body' => $this->getNotificationBody($event, $options),
            'data' => $this->getData($event, $options)
        ];
    }
}
