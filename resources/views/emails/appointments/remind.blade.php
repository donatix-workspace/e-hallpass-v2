<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>e-hallpass | Pass Notification</title>
    <!-- Lato font include here -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300i,400,400i,700|Roboto" rel="stylesheet">
    <style>
        body {
            font-family: 'Lato', Arial, sans-serif;
        }

        @media screen and (max-width: 600px) {
            .m-center {
                text-align: center;
            }

            .m-button {
                width: 300px !important;
            }

            .pass-content p {
                text-align: center !important;
            }

            .pass-date {
                font-size: 20px !important;
                line-height: 24px !important;
            }

            .pass-info-wrapper h3,
            .pass-info-wrapper h2 {
                font-size: 20px !important;
                line-height: 24px !important;
            }

            .pass-info-wrapper {
                padding: 26px 6px 26px !important;
            }

            .pass-info-wrapper > div {
                width: auto !important;
                padding: 0 12px;
            }

        }
    </style>
</head>
<body>
<div class="main-wrapper" style="max-width: 600px; width: 100%; margin: 0 auto; background-color: #fff">
    <table cellpadding="0" cellspacing="0" style="max-width: 600px; width: 100%;" class="m-center">
        <tr>
            <td style="padding: 30px 0 0px 0;text-align: center;">
                <a href="{{config('app.front_url')}}"><img src="{{Storage::disk('s3')->url('ehp-700x350.png')}}"
                                                           border="0"
                                                           alt="e-hallpass" height="66px" width="125px"/></a>
            </td>
        </tr>
        <tr>
            <td style="padding:0;text-align: center;color:#959595; font-size:20px; line-height: 20px; font-family: 'Lato', Arial, sans-serif; font-weight: 900;">
                Appointment Pass
            </td>
        </tr>

        <tr>
            <td style="padding:0;text-align: center;color:#959595; font-size:15px; line-height: 30px; font-family: 'Lato', Arial, sans-serif; font-weight: 700;">
                {{$appointmentPass->user->first_name. ' '. $appointmentPass->user->last_name}}
            </td>
        </tr>
        <tr>
            <td valign="middle"
                style="text-align: center;color:#49AD5D;font-size:25px; line-height: 30px; font-family: 'Lato', Arial, sans-serif; font-weight: 500;">
                @if(!$appointmentPass->forNow() && $timeRemain >= 4)
                    Pass Ready in
                    roughly {{$timeRemain}}
                    minutes
                @else
                    Pass Ready NOW!
                @endif
            </td>
        </tr>

        <tr>
            <td valign="middle" style="text-align: center;">
                {{-- Acknowledge with email link or with token --}}
                @if(!$adult)
                <a href="{{ url(config('app.front_url').'/acknowledged-pass?token='.$token) }}"
                   target="_blank" class="m-button" style="margin: 23px 0 24px 0; background-color: #49AD5D;
                    height: 32px; width: 391.84px; border-radius: 8px;
                    font-family: 'Roboto', Arial, sans-serif; text-align: center; line-height: 18px; text-decoration: none; color: #ffffff; font-size: 15px; font-weight: 500; letter-spacing: 0.49px; display: inline-block; padding: 12px 0 0 0;">Acknowledge</a>
                @else
                    <a href="{{ url(config('app.front_url').'/acknowledged-pass?token='.$tokenAdmin) }}"
                       target="_blank" class="m-button" style="margin: 23px 0 24px 0; background-color: #49AD5D;
                    height: 32px; width: 391.84px; border-radius: 8px;
                    font-family: 'Roboto', Arial, sans-serif; text-align: center; line-height: 18px; text-decoration: none; color: #ffffff; font-size: 15px; font-weight: 500; letter-spacing: 0.49px; display: inline-block; padding: 12px 0 0 0;">Acknowledge</a>
                @endif
            </td>
        </tr>
        <tr>
            <td>
                <div class="pass-detail"
                     style="border: 1px solid #DCDDDE; border-radius: 8px; background-color: #F9F9F9; box-shadow: 0 0 1px 0 rgba(0,0,0,0.68); margin-bottom: 15px;">
                    <div class="pass-info-wrapper" style="padding: 26px 31px 26px;">
                        <div style="width: 40%; display: inline-block; vertical-align: middle;">
                            @if($appointmentPass->from_id !== 0)
                                <h3 style="color: #959595; font-size: 22px; line-height: 27px; text-align: justify; font-family: 'Lato', Arial, sans-serif; font-weight: 400; margin: 0 10px 0 0;">{{$appointmentPass->from_type === \App\Models\User::class ? $appointmentPass->from->last_name . ' ' . $appointmentPass->from->first_name : $appointmentPass->from->name}}</h3>
                            @else
                                <h3 style="color: #959595; font-size: 22px; line-height: 27px; text-align: justify; font-family: 'Lato', Arial, sans-serif; font-weight: 400; margin: 0 10px 0 0;">{{config('roles.prefixes.system')}}</h3>
                            @endif

                            <h2 style="font-family: 'Lato', Arial, sans-serif; color: #45455A; font-size: 30px; line-height: 36px; text-align: justify; font-weight: 400; margin: 0;">{{$appointmentPass->for_date->setTimezone($appointmentPass->school->getTimezone())->format('m/d/Y g:i A')}}</h2>
                        </div>

                        <div style="width: 18%; display: inline-block; vertical-align: middle;">
                            <div style="width: 18%; display: inline-block; vertical-align: middle;">
                                <img src="{{ asset('images/arrow.png')  }}" alt=""
                                     style="border: 0px; width: 38px; height: 28px;">
                            </div>
                        </div>

                        <div style="width: 30%; display: inline-block; vertical-align: middle;">
                            <h4 style="color: #4990E2; font-size: 30px; line-height: 36px; text-align: right; margin: 0; font-weight: 300;">{{$appointmentPass->to_type === \App\Models\User::class ? $appointmentPass->to->last_name . ' ' . $appointmentPass->to->first_name : $appointmentPass->to->name}}</h4>
                        </div>
                    </div>

                    <div class="pass-content"
                         style="background-color: #E7F4FF; padding: 20px; border-top: 1px solid #74B2E0; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px;">

                        <p style="color: #45455A; font-size: 14px; line-height: 22px; text-align: justify; font-family: 'Lato', Arial, sans-serif;">
                            <span style="font-size: 14px;color: #000;"><strong>Note:</strong></span>
                            {{$appointmentPass->reason}}</p>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td valign="middle"
                style="text-align:center;color:#4d4d4d;font-size:14px; line-height: 10px; font-family: 'Lato', Arial, sans-serif; font-weight: 500;">
                To activate your pass (at the pass time) please log into e-hallpass.
            </td>
        </tr>
        <tr>
            <td style="text-align: center; padding: 40px 32px 20px 32px;">
                <span
                    style="font-family: 'Lato', Arial, sans-serif; color: #B0AEB0; font-size: 14px; letter-spacing: 0.13px; line-height: 17px; border-top: 1px solid #C3C3C3; display: block; padding-top: 30px;">© {{date('Y')}} e-hallpass. The Online Digital Hall Pass Solution.</span>
            </td>
        </tr>

    </table>
</div>
</body>
</html>
