<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdToSchoolsModulesTable extends Migration
{
    public function up()
    {
        Schema::table('school_modules', function (Blueprint $table) {
            $table->id('id')->unsigned()->first();
        });
    }

    public function down()
    {
        Schema::table('school_modules', function (Blueprint $table) {
            $table->dropColumn('id');
        });
    }
}
