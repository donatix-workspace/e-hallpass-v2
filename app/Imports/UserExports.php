<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserExports implements FromQuery, WithHeadings, WithMapping
{
    public $forSubstitute;
    public $forArchived;
    private $userCsvQuery;

    public function __construct(
        bool $forSubstitute = false,
        bool $forArchived = false,
        $userCsvQuery = null
    ) {
        $this->forArchived = $forArchived;
        $this->forSubstitute = $forSubstitute;
        $this->userCsvQuery = $userCsvQuery;
    }

    public function query()
    {
        $user = auth()->user();

        $query =
            $this->userCsvQuery ?:
            User::withoutGlobalScopes()
                ->where('is_substitute', User::INACTIVE)
                ->where('status', User::ACTIVE)
                ->skipHidden()
                ->where('deleted_at', null)
                ->fromSchoolId($user->school_id);

        return $query;
    }

    public function headings(): array
    {
        return [
            'firstname',
            'lastname',
            'email',
            'role',
            'gradyear',
            'status',
            'idnumber'
        ];
    }

    /**
     * @var User $row
     */
    public function map($row): array
    {
        return [
            $row->first_name,
            $row->last_name,
            $row->email,
            $row->role->name,
            $row->grade_year,
            $row->status ? 'Active' : 'Inactive',
            $row->student_sis_id
        ];
    }
}
