<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->string('name', 300)->nullable();
            $table->bigInteger('type_id')->unsigned()->nullable();
            $table->bigInteger('school_id')->unsigned()->nullable()->index('school_id');
            $table->enum('trip_type', [\App\Models\Room::TRIP_LAYOVER, \App\Models\Room::TRIP_ONE_WAY, \App\Models\Room::TRIP_ROUNDTRIP]);
            $table->boolean('status')->nullable()->index('status');
            $table->enum('comment_type', [\App\Models\Room::COMMENT_HIDDEN, \App\Models\Room::COMMENT_MANDATORY, \App\Models\Room::COMMENT_OPTIONAL]);
            $table->string('icon')->nullable()->default(null);
            $table->boolean('favorites')->nullable()->default(0);
            $table->integer('order')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
