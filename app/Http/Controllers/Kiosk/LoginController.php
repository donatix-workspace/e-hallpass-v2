<?php

namespace App\Http\Controllers\Kiosk;

use App\Http\Controllers\Controller;
use App\Http\Requests\KioskLoginRequest;
use App\Models\Kiosk;
use App\Models\KioskUser;
use App\Models\Pass;
use App\Models\Room;
use App\Models\StudentFavorite;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * Login with kiosk machine
     *
     * @bodyParam code string required The code for the kiosk
     * @bodyParam type string required The type of login
     * @bodyParam email string required The email for the kiosk login
     *
     * @authenticated
     * @param KioskLoginRequest $request
     * @return JsonResponse
     */
    public function login(KioskLoginRequest $request): JsonResponse
    {
        $kioskSuccess = false;

        if ($request->input('type') !== Kiosk::STANDARD_TYPE) {
            $user = User::where(
                'student_sis_id',
                $request->input('code')
            )->first();

            $kiosk = KioskUser::where([
                ['user_id', '=', optional($user)->id],
                ['school_id', '=', optional($user)->school_id]
            ])->first();

            $kioskSuccess = $kiosk !== null;
        } else {
            $user = User::where('email', $request->input('email'))->first();

            $kiosk = KioskUser::where([
                ['user_id', '=', optional($user)->id],
                ['school_id', '=', optional($user)->school_id]
            ])->first();

            $kioskSuccess = Hash::check(
                $request->input('code'),
                optional($kiosk)->getRawOriginal('kpassword')
            );
        }

        if (!$kioskSuccess) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('Invalid Login Credentials'),
                    'status' => __('error')
                ],
                422
            );
        }

        $kioskAccessToken = $kiosk->createToken('Kiosk Authentication')
            ->accessToken;

        $pass = Pass::wherePassStatus(Pass::ACTIVE)
            ->whereUserId($user->id)
            ->whereSchoolId($kiosk->school_id)
            ->first();

        return response()->json([
            'data' => [
                'kiosk' => $kiosk,
                'access_token' => $kioskAccessToken,
                'active_pass' => $pass
            ],
            'message' => __('kiosk.logged.in.successfully'),
            'status' => __('success')
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        KioskUser::kioskLogout();

        return response()->json([
            'message' => __(' kiosk.logged.out.successfully'),
            'status' => __('success')
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function directLogin(Request $request)
    {
        $request->validate([
            'password' => 'required|string'
        ]);

        $kiosk = Kiosk::with(['user', 'room', 'school:id,cus_uuid,name'])
            ->where([
                ['password', '=', $request->password],
                ['status', '=', Kiosk::ACTIVE]
            ])
            ->first();

        if (!$kiosk) {
            return response()->json(
                [
                    'message' => __(
                        'The code for is invalid or the kiosk was ended.'
                    ),
                    'status' => __('error')
                ],
                422
            );
        }

        $kiosk->update(['password_use' => Carbon::now()]);

        $defaultFavourites = StudentFavorite::query()
            ->with([
                'favorable' => function ($morphTo) {
                    $morphTo->morphWith([
                        Room::class => ['activeUnavailability', 'capacity'],
                        User::class => ['activeUnavailability']
                    ]);
                }
            ])
            ->where('global', 1)
            ->where('user_id', null)
            ->where('school_id', $kiosk->school_id)
            ->where('section', StudentFavorite::DESTINATION)
            ->orderBy('position', 'asc')
            ->orderBy('section', 'asc')
            ->orderBy('global', 'asc')
            ->get();

        $kiosk = $kiosk->toArray();
        $kiosk['favorites'] = $defaultFavourites;

        return response()->json([
            'data' => $kiosk,
            'message' => __('kiosk.logged.in.successfully'),
            'status' => __('success')
        ]);
    }
}
