<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddConfirmedByToAppointmentPassesTable extends Migration
{
    public function up()
    {
        Schema::table('appointment_passes', function (Blueprint $table) {
            $table->unsignedBigInteger('confirmed_by')->nullable();

            $table->foreign('confirmed_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('appointment_passes', function (Blueprint $table) {
            $table->dropColumn('confirmed_by');
        });
    }
}
