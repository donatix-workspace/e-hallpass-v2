<?php


namespace App\Http\Services\CommonUserService\Translators;


use App\Http\Services\CommonUserService\Models\CommonDistrict;
use App\Http\Services\CommonUserService\Models\CommonTimezone;
use App\Models\School;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class SchoolTranslator
{
    public static function translateDb($schoolModel)
    {
        $deprecatedTimezones = config('timezones.deprecated');
        $commonTimezone = CommonTimezone::where('id', $schoolModel->l10n_datetime_time_zone_id)->first()->r_name;
        $commonDistrict = CommonDistrict::where('id', $schoolModel->common_bm_district_id)->first()->uuid;

        $timezone = array_key_exists($commonTimezone, $deprecatedTimezones) ? $deprecatedTimezones[$commonTimezone] : $commonTimezone;
        return [
            'cus_uuid' => $schoolModel->uuid,
            'district_uuid' => $commonDistrict,
            'name' => $schoolModel->full_name,
            'description' => $schoolModel->description,
            'logo' => self::generateImageFromBase64($schoolModel->logo, $schoolModel->uuid) ?? '',
            'status' => $schoolModel->r_status,
            'archive_prefix' => $schoolModel->sftp_archive_prefix,
            'timezone' => $timezone,
            'timezone_offset' => config('timezones.offsets.' . $timezone),
            "created_at" => $schoolModel->created,
//            "updated_at" => $schoolModel->modified,
            'show_student_photos' => $schoolModel->providing_student_photos ?? false,
            'state'=> $schoolModel->l10n_us_territorial_subdivision_code_ansi,
            'zip'=>$schoolModel->zip_code,
            'common_id'=>$schoolModel->id,
            'sftp_directory' => $schoolModel->sftp_directory,
            'sftp_file_prefix' => $schoolModel->sftp_file_prefix
        ];

    }

    protected static function generateImageFromBase64($base64, $schoolUuid)
    {
        if (!strpos($base64, 'base64')) {
            return null;
        }
        $data = explode(',', $base64);
        $extension = null;
        if (strpos($data[0], 'jpeg') || strpos($data[0], 'jpg')) {
            $extension = 'jpg';
        } else if (strpos($data[0], 'png')) {
            $extension = 'png';
        }
        $fileName = $schoolUuid . time() . '.' . $extension;

        $file = Storage::disk('s3')->put(School::SCHOOL_IMAGE_DIRECTORY . $fileName, base64_decode($data[1]));

        return $file ? $fileName : null;
    }

    protected
    static function setTimeFormat()
    {
        return 0;
    }

    protected
    static function convertTime($time = null)
    {
        return $time ? Carbon::createFromFormat("Y-m-d\TH:i:s\Z", $time) : Carbon::now();
    }


}
