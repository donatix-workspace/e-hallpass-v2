<?php
return [
    'database' => [
        'host' => env('SYSTEM_MIGRATION_DB_HOST'),
        'port' => env('SYSTEM_MIGRATION_DB_PORT'),
        'database' => env('SYSTEM_MIGRATION_DB_NAME'),
        'username' => env('SYSTEM_MIGRATION_DB_USERNAME'),
        'password' => env('SYSTEM_MIGRATION_DB_PASSWORD'),
        'passHistoryDatabase' => [
            'host' => env('SYSTEM_MIGRATION_HISTORY_DB_HOST'),
            'port' => env('SYSTEM_MIGRATION_HISTORY_DB_PORT'),
            'database' => env('SYSTEM_MIGRATION_HISTORY_DB_NAME'),
            'username' => env('SYSTEM_MIGRATION_HISTORY_DB_USERNAME'),
            'password' => env('SYSTEM_MIGRATION_HISTORY_DB_PASSWORD'),
        ]
    ],
];
