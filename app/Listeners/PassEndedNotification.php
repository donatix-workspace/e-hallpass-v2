<?php

namespace App\Listeners;

use App\Events\PassEnded;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PassEndedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PassEnded $event
     * @return void
     */
    public function handle(PassEnded $event)
    {
        //
    }
}
