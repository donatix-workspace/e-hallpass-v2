<?php

namespace Tests\Commands;

use App\Models\AppointmentPass;
use App\Models\Pass;
use App\Models\Period;
use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AppointmentPassExpireCommandTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;
    protected $firstUser;
    protected $teacherUser;
    protected $pass;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);

        $this->teacherUser = User::factory()->createOne([
            'role_id' => config('roles.teacher'),
            'school_id' => $this->school->id
        ]);

        $this->pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->adminUser->id,
            'user_id' => $this->firstUser->id
        ]);
    }

    /**
     * @test
     * The command will expire the appointment pass when the pass is not completed and it's in the pass
     * @return void
     */
    public function the_command_will_expire_the_appointment_pass_when_the_pass_is_not_completed_and_it_s_in_the_pass()
    {

        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => config('roles.system'),
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::yesterday(optional($this->school)->getTimezone()),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'acknowledge_at' => Carbon::now(optional($this->school)->getTimezone())->subMinutes(5),
            'reason' => 'reason'
        ]);

        $this->artisan('passes:appointment_expire')->assertExitCode(0)->run();

        $this->assertNotNull($appointmentPass->refresh()->expired_at);
    }

    /**
     * @test
     * The command will expire the pass when it's not confirmed
     * @return void
     */
    public function the_command_will_expire_the_pass_when_it_s_not_confirmed()
    {

        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => config('roles.system'),
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::yesterday(optional($this->school)->getTimezone()),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'reason' => 'reason'
        ]);

        $this->artisan('passes:appointment_expire')->assertExitCode(0)->run();

        $this->assertNotNull($appointmentPass->refresh()->expired_at);
    }
}
