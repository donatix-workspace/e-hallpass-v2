<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToContactTracesTable extends Migration
{
    public function up()
    {
        Schema::table('contact_traces', function (Blueprint $table) {
            $table->index('search_id');
        });
    }

    public function down()
    {
        Schema::table('contact_traces', function (Blueprint $table) {
            $table->dropIndex('contact_traces_search_id_index}');
        });
    }
}
