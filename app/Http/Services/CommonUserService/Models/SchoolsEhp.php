<?php

namespace App\Http\Services\CommonUserService\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolsEhp  extends Model
{
    protected $table = 'school_ehp';
    protected $guarded = [];
}
