<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_types', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->bigInteger('school_id')->unsigned()->nullable()->index('school_id');
            $table->boolean('comment_type')->nullable();
            $table->text('description')->nullable();
            $table->boolean('status')->nullable()->index('status');
            $table->integer('ordering')->nullable();
            $table->boolean('receiver')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_types');
    }
}
