<?php

namespace Tests\Feature;

use App\Models\Pass;
use App\Models\Pin;
use App\Models\PinAttempt;
use App\Models\School;
use App\Models\User;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PinsTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;
    protected $firstUser;
    protected $pin;
    protected $pass;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => config('roles.student'),
            'school_id' => $this->school->id
        ]);

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);

        $this->pin = Pin::create([
            'pinnable_id' => $this->adminUser->id,
            'pinnable_type' => User::class,
            'pin' => '1234',
            'school_id' => $this->adminUser->school_id
        ]);

        $this->pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->firstUser->id,
            'user_id' => $this->firstUser->id
        ]);
    }


    /**
     * @test
     * An admin user can unlock locked student user
     * @return void
     */
    public function an_admin_user_can_unlock_locked_student_user()
    {
        PinAttempt::factory()->count(PinAttempt::MAXIMUM_PIN_ATTEMPTS)->create([
            'user_id' => $this->firstUser->id,
            'school_id' => $this->firstUser->school_id,
            'pass_id' => $this->pass->id,
            'status' => 1
        ]);

        $this->actingAs($this->firstUser)
            ->post(route('api.v5.users.unlock'), [
                'teacher_pin' => $this->pin->pin
            ])->assertOk()
            ->assertJsonStructure(['data', 'status', 'message']);

        $this->assertDatabaseHas('pin_attempts', ['user_id' => $this->firstUser->id, 'status' => 0]);
    }
}
