<?php

namespace App\Http\Controllers\Kiosk;

use App\Events\AdminPassCreated;
use App\Events\PassCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\KioskAutoApproveRequest;
use App\Http\Requests\PassRequest;
use App\Http\Requests\PassUpdateRequest;
use App\Http\Resources\PassResource;
use App\Models\AutoPass;
use App\Models\AutoPassLimit;
use App\Models\AutoPassPreference;
use App\Models\Kiosk;
use App\Models\Pass;
use App\Models\Pin;
use App\Models\PinAttempt;
use App\Models\Room;
use App\Models\School;
use App\Models\User;
use App\Notifications\Push\StudentNotification;
use App\Policies\PassPolicy;
use Cache;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Notification;

class CreatePassController extends Controller
{
    /**
     * Show the pass
     * @urlParam pass integer required The id of the pass
     * @header Accept application/json
     *
     * @response {
     *      "data": {
     *          "pass": {},
     *      },
     *      "status": "success"
     * }
     * @return JsonResponse
     * @authenticated
     */
    public function show(): JsonResponse
    {
        $user = auth()->user();

        $pass = Pass::without('child')
            ->with('parent')
            ->ofUserId($user->user_id)
            ->fromSchoolId($user->school_id)
            ->where('pass_status', Pass::ACTIVE)
            ->orderBy('created_at', 'desc')
            ->first();

        if ($pass !== null) {
            $pass->append(['flags', 'expire_after']);
        }

        $schoolOrStudentPassLimit = PassPolicy::checkPassLimit(true);

        return response()->json([
            'pass' => $pass,
            'pass_limit' => [
                'has' => $schoolOrStudentPassLimit['remain']['max'] > 0,
                'student_reached_limit' => $schoolOrStudentPassLimit['status'],
                'current' => $schoolOrStudentPassLimit['remain']['current'],
                'max' => $schoolOrStudentPassLimit['remain']['max'],
                'from_date' => Carbon::parse(
                    $schoolOrStudentPassLimit['from_date']
                )->toDateString(),
                'to_date' => Carbon::parse(
                    $schoolOrStudentPassLimit['to_date']
                )->toDateString()
            ]
        ]);
    }

    /**
     * Show the user history.
     *
     * @response status=200 {
     *   "data": [{}]
     *   "status": "Successfully"
     * }
     *
     * @authenticated
     * @return JsonResponse
     */
    public function history(): JsonResponse
    {
        $user = User::find(auth()->user()->user_id);

        $schoolTimezone = optional(
            School::findCacheFirst($user->school_id)
        )->getTimezone();

        $startDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->startOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));
        $endDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->endOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));

        $histories = Pass::ofUserId($user->id)
            ->without([
                'createdByUser',
                'completedByUser',
                'comments',
                'latestComment',
                'requestedByUser',
                'approvedByUser'
            ])
            ->fromSchoolId($user->school_id)
            ->whereParentId(null)
            ->orderBy('created_at', 'desc')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where(function ($query) {
                $query
                    ->where('pass_status', Pass::ACTIVE)
                    ->where('approved_at', '!=', null)
                    ->orWhere(function ($query) {
                        $query
                            ->where('completed_by', '!=', null)
                            ->orWhere('completed_at', '!=', null);
                    });
            })
            ->get()
            ->append(['ended_at', 'signatures']);

        return response()->json([
            'data' => $histories,
            'status' => __('success')
        ]);
    }

    /**
     * Create a pass (Kiosk)
     *
     * @bodyParam from_id integer required Current destination of the user
     * @bodyParam to_id integer required To what destination user wants to go
     * @bodyParam from_type string required Current destination type (Room or User)
     * @bodyParam to_type string required Target destination type (Room or User)
     * @bodyParam comment string required The reason why the user start the pass.
     * @bodyParam kiosk_id integer required The id of the kiosk
     *
     * @param PassRequest $request
     * @return JsonResponse
     * @apiResourceModel App\Models\Pass
     * @apiResource App\Http\Resources\PassResource
     * @authenticated
     * @throws AuthorizationException
     */
    public function store(PassRequest $request): JsonResponse
    {
        $user = auth()->user();

        $kiosk = Kiosk::where('id', $request->input('kiosk_id'))
            ->where('school_id', $user->school_id)
            ->first();

        $this->authorize('createPass', [$kiosk]);

        $passValidation = Pass::validate($request, true);
        if ($passValidation !== null) {
            return $passValidation;
        }

        $schoolOrStudentPassLimit = PassPolicy::checkPassLimit(true);

        $pass = Pass::create([
            'user_id' => $user->user_id,
            'from_id' =>
                $kiosk->room_id === null ? $kiosk->user_id : $kiosk->room_id,
            'from_type' => $kiosk->room_id === null ? User::class : Room::class,
            'to_id' => $request->input('to_id'),
            'to_type' => $request->input('to_type'),
            'school_id' => $user->school_id,
            'requested_by' => $user->user_id,
            'created_by_kiosk' => $kiosk->id,
            'requested_at' => now(),
            'type' => Pass::KIOSK_PASS
        ]);

        if (
            $request->input('comment') !== null &&
            $request->input('comment') !== ''
        ) {
            $pass->comments()->create([
                'comment' => $request->input('comment'),
                'user_id' => $user->user_id,
                'school_id' => $user->school_id
            ]);
        }

        event(new PassCreated($pass));
        event(new AdminPassCreated($pass));

        //        Notification::send($pass, new StudentNotification('PassCreated'));

        return response()->json([
            'pass' => Pass::find($pass->id)->append(['flags']),
            'pass_limit' => [
                'has' => $schoolOrStudentPassLimit['remain']['max'] > 0,
                'student_reached_limit' => $schoolOrStudentPassLimit['status'],
                'current' => $schoolOrStudentPassLimit['remain']['current'],
                'max' => $schoolOrStudentPassLimit['remain']['max'],
                'from_date' => Carbon::parse(
                    $schoolOrStudentPassLimit['from_date']
                )->toDateString(),
                'to_date' => Carbon::parse(
                    $schoolOrStudentPassLimit['to_date']
                )->toDateString()
            ]
        ]);
    }

    /**
     * Auto Approve the kiosk pass when its auto pass.
     *
     * @bodyParam action integer required Pass approve action Example: 1 (APPROVE), 2 (END)
     * @urlParam pass integer required The id of the pass
     *
     * @param KioskAutoApproveRequest $request
     * @param Pass $pass
     * @return PassResource|JsonResponse
     * @apiResourceModel App\Models\Pass
     * @apiResource App\Http\Resources\PassResource
     * @throws AuthorizationException
     */
    public function autoApprove(KioskAutoApproveRequest $request, Pass $pass)
    {
        $kiosk = Kiosk::findOrFail($request->get('kiosk_id'));

        $this->authorize('updatePass', [$kiosk, $pass]);

        /** @var AutoPass $autoPass */
        $autoPass = AutoPass::ofRoomId($pass->to_id)->where(
            'school_id',
            auth()->user()->school_id
        );

        if (!$autoPass->exists()) {
            return response()->json(
                [
                    'data' => ['id' => $pass->id],
                    'message' => __(
                        'passes.auto.approve.for.that.room.not.exists'
                    ),
                    'status' => __('fail')
                ],
                422
            );
        }

        $autoPassRecord = $autoPass->first();

        $autoPassPreference = AutoPassPreference::where(
            'auto_pass_id',
            $autoPassRecord->id
        )
            ->ofUserId($pass->from_id)
            ->where('school_id', auth()->user()->school_id)
            ->first();

        $approveAction = $request->input('action');

        if ($pass->isCanceled()) {
            return response()->json([
                'data' => ['id' => $pass->id],
                'message' => __('passes.invalid.action'),
                'status' => __('fail')
            ]);
        }

        $autoPassLimit =
            $pass->from_type === User::class
                ? AutoPassLimit::where('user_id', $pass->from_id)
                    ->where('school_id', $pass->school_id)
                    ->first()
                : null;

        if (optional($autoPassLimit)->limit < 0) {
            return response()->json([
                'data' => ['id' => $pass->id],
                'message' => __('passes.pass.limit.turned.off'),
                'status' => __('fail')
            ]);
        }

        if ($autoPassPreference === null) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('passes.room.has.no.preference'),
                    'status' => __('fail')
                ],
                422
            );
        }

        // Approve
        if (
            $approveAction === Pass::ACTION_APPROVE &&
            $autoPassPreference->isInCorrectMode()
        ) {
            if ($pass->isCompleted() || $pass->isCanceled()) {
                return response()->json(
                    [
                        'data' => ['id' => $this->id],
                        'message' => __('passes.invalid.action'),
                        'status' => __('fail')
                    ],
                    422
                );
            }

            $pass->update([
                'approved_with' => Pass::BY_AUTOPASS
            ]);

            return $pass->approve(
                null,
                null,
                true,
                $kiosk->id,
                Pass::BY_AUTOPASS
            );
        }

        // End if the pass mode is start_and_stop
        if (
            $approveAction === Pass::ACTION_END &&
            $autoPassPreference->isModeStartAndStop()
        ) {
            return $pass->kioskEnd(null, Pass::BY_AUTOPASS, false, $kiosk->id);
        }

        return response()->json(
            [
                'data' => ['id' => $pass->id],
                'message' => __('passes.approval.not.approved'),
                'status' => __('fail')
            ],
            422
        );
    }

    /**
     * Update the pass status
     *
     * @bodyParam approval_message string Teacher approval message when uses pin
     * @bodyParam action integer required Pass approve action Example: 1 (APPROVE), 2 (END), 3 (Arrived), 4 (Returning/InOut)
     * @bodyParam return_type string Determine whether the user is returning or it's directly out Example: 'inout' OR 'return'
     * @urlParam pass integer required The id of the pass
     *
     * Approval Actions:
     * 1. Approve $approvalAction = 1;
     * 2. End/Keep $approvalAction = 2;
     * 3. Arrived $approvalAction = 3;
     * 4. In/Out & Returning $approvalAction = 4;
     * 5. Cancel
     *
     * @param PassUpdateRequest $request
     * @param Pass $pass
     * @return PassResource|JsonResponse
     * @apiResourceModel App\Models\Pass
     * @apiResource App\Http\Resources\PassResource
     * @authenticated
     * @throws AuthorizationException
     * @throws Exception
     */
    public function update(Pass $pass, PassUpdateRequest $request)
    {
        $kiosk = Kiosk::findOrFail($request->get('kiosk_id'));

        $this->authorize('updatePass', [$kiosk, $pass]);

        $approveAction = $request->input('action');
        $passNoteMessage = $request->input('approval_message');
        $user = auth()->user();

        $pin = Pin::fromSchoolId(auth()->user()->school_id)
            ->fromType(Pin::USER_TYPE)
            ->where('pin', $request->input('teacher_pin'))
            ->first();

        $kioskPinCounterName = 'kiosk_pin_attempts_' . $user->user_id;

        cache()->remember(
            $kioskPinCounterName,
            now()->addMinutes(5),
            function () {
                return 0;
            }
        );

        if (
            $pin === null &&
            $request->input('action') !== Pass::ACTION_KIOSK_END &&
            $request->input('action') !== Pass::ACTION_KIOSK_CANCEL
        ) {
            cache()->increment($kioskPinCounterName);

            if (
                cache()->get($kioskPinCounterName) >=
                PinAttempt::LOGOUT_PIN_ATTEMPTS
            ) {
                cache()->forget($kioskPinCounterName);

                return response()->json(
                    [
                        'message' => __(
                            'The pin attempt was exceeded and the user was logged out.'
                        ),
                        'data' => [
                            'id' => $pass->id
                        ],
                        'status' => __('fail')
                    ],
                    401
                );
            }

            return response()->json(
                [
                    'data' => [
                        'id' => $pass->id
                    ],
                    'message' => __('pin.invalid'),
                    'status' => __('fail')
                ],
                422
            );
        }

        switch ($approveAction) {
            case Pass::ACTION_APPROVE:
                return $pass->approve($pin, $passNoteMessage, true, $kiosk->id);
            case Pass::ACTION_KIOSK_END:
                return $pass->kioskEnd($pin, Pass::BY_KIOSK, false, $kiosk->id);
            case Pass::ACTION_KIOSK_CANCEL:
                return $pass->kioskCancel($pin);
            case Pass::ACTION_EXPIRE:
                return $pass->expire();
        }
        return response()->json(
            [
                'data' => [],
                'message' => __('passes.update.invalid.action'),
                'status' => __('fail')
            ],
            422
        );
    }
}
