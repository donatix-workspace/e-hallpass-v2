<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePinAttemptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pin_attempts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('pass_id');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });

        Schema::table('pin_attempts', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('pass_id')->references('id')->on('passes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pin_attempts', function (Blueprint $table) {
            $table->dropForeign('pin_attempts_user_id_foreign');
            $table->dropForeign('pin_attempts_school_id_foreign');
            $table->dropForeign('pin_attempts_pass_id_foreign');
        });
        Schema::dropIfExists('pin_attempts');
    }
}
