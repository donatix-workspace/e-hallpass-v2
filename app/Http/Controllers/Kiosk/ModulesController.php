<?php

namespace App\Http\Controllers\Kiosk;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreKioskRequest;
use App\Http\Resources\KioskCollection;
use App\Mail\SendKioskPasswordMail;
use App\Models\Kiosk;
use App\Models\Room;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ModulesController extends Controller
{
    /**
     * Return the list of kiosks that are currently being active or
     * deactivated for the current day.
     *
     * @response status=200 {
     *    data: []
     * }
     *
     * @authenticated
     * @return KioskCollection
     * @throws AuthorizationException
     */
    public function index(): KioskCollection
    {
        $user = auth()->user();

        $this->authorize('viewAny', Kiosk::class);

        $kiosks = Kiosk::with([
            'room',
            'user:first_name,last_name,id',
            'school:id,school_domain,student_domain,name'
        ])
            ->where('school_id', $user->school_id)
            ->where(function ($query) {
                $query->where('expired_date', null)->orWhere(
                    'expired_date',
                    '<',
                    now()
                        ->endOfDay()
                        ->toDateTimeString()
                );
            })
            ->where('user_id', $user->id)
            ->orderByRaw(
                'NOT ISNULL(expired_date), NOT ISNULL(password_use) AND ISNULL(expired_date),expired_date asc,password_use asc,created_at desc'
            )
            ->get();

        $data = [
            'kiosks' => $kiosks
        ];

        return new KioskCollection($data);
    }

    /**
     * Create a kiosk.
     *
     * @bodyParam kiosk_type_id integer required The id of the room or user.
     * @bodyParam kiosk_type string required The type of the kiosk.
     *
     * @authenticated
     * @param StoreKioskRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(StoreKioskRequest $request): JsonResponse
    {
        $this->authorize('create', Kiosk::class);

        $user = auth()->user();

        // Get the type and the id.
        $kioskType = $request->input('kiosk_type');
        $kioskTypeId = $request->input('kiosk_type_id');

        // Check if the kiosk exists.
        $kiosks = Kiosk::whereSchoolId($user->school_id)
            ->where('status', 1)
            ->where('expired_date', null)
            ->typeExists($kioskType, $kioskTypeId, $user)
            ->exists();

        if (!$kiosks) {
            $kiosks = Kiosk::create([
                'room_id' => $kioskType === Room::class ? $kioskTypeId : null,
                'user_id' =>
                    $kioskType === User::class ? $kioskTypeId : $user->id,
                'school_id' => $user->school_id,
                'password' => $this->generateCode(),
                'status' => '1',
                'expired_date' => null
            ])->load(['room', 'user']);

            return response()->json([
                'data' => $kiosks,
                'message' => __(
                    'kiosk.mode.enabled.for.this.room.successfully'
                ),
                'status' => __('success')
            ]);
        }

        return response()->json([
            'message' => __('kiosk.already.set.for.this.room'),
            'status' => __('success')
        ]);
    }

    /**
     * @param Kiosk $kiosk
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function deactivateKiosk(Kiosk $kiosk): JsonResponse
    {
        $this->authorize('deactivate', $kiosk);

        $kiosk->update([
            'status' => '0',
            'expired_date' => now()
        ]);

        return response()->json([
            'data' => $kiosk
                ->refresh()
                ->load([
                    'user',
                    'room',
                    'school:id,school_domain,student_domain,name'
                ]),
            'message' => __('kiosk.deacivated.successfully'),
            'status' => __('success')
        ]);
    }

    /**
     * @param Kiosk $kiosk
     * @return JsonResponse
     */
    public function sendKioskPassword(Kiosk $kiosk): JsonResponse
    {
        if (auth()->user()->email) {
            $data = [
                'email' => auth()->user()->email,
                'name' =>
                    auth()->user()->first_name .
                    ' ' .
                    auth()->user()->last_name,
                'password' => $kiosk->password
            ];

            Mail::to(auth()->user(), auth()->user()->school_id)->send(
                new SendKioskPasswordMail($data)
            );
        }

        return response()->json([
            'message' => __('kiosk.password.has.been.sent'),
            'status' => __('success')
        ]);
    }

    /**
     * @param Kiosk $kiosk
     * @return JsonResponse
     */
    public function regenerateKioskPassword(Kiosk $kiosk): JsonResponse
    {
        $password = $this->generateCode();
        $kiosk->update([
            'password' => $password
        ]);

        return response()->json([
            'data' => $kiosk
                ->refresh()
                ->load([
                    'user',
                    'room',
                    'school:id,school_domain,student_domain,name'
                ]),
            'message' => __('kiosk.password.was.regenerated.successfully'),
            'status' => __('success')
        ]);
    }

    /**
     * @return string
     */
    public function generateCode(): string
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
    }
}
