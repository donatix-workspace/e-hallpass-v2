<?php

namespace Database\Factories;

use App\Models\School;
use App\Models\Unavailable;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class UnavailableFactory extends Factory
{
    protected $model = Unavailable::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'unavailable_type' => $this->faker->word,
            'unavailable_id' => $this->faker->boolean,
            'from_date' => Carbon::now(),
            'to_date' => Carbon::now(),
            'status' => $this->faker->boolean,
            'recurrence_type' => $this->faker->word,
            'recurrence_days' => $this->faker->word,
            'recurrence_end_at' => Carbon::now(),
            'comment' => $this->faker->word,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'school_id' => function () {
                return School::factory()->create()->id;
            },
        ];
    }
}
