-- TODO: Add update foreign

-- EDITED FIX
UPDATE passes SET expired_at = NULL
WHERE expired_at is not null and edited_at is not null and approved_at not null and completed_at not null  AND approved_by > 0 AND completed_by > 0;


-- FIX NO BADGES

UPDATE passes SET expired_at = NULL
WHERE expired_at IS NOT NULL AND system_completed = 0 AND approved_by > 0 AND completed_by > 0;

-- (SYSTEM COMPLETE FIX)

-- Approved

UPDATE passes SET system_completed = 1,
                  approved_by = NULL,
                  expired_at = NULL
WHERE (SELECT first_name FROM users WHERE id = passes.approved_by LIMIT 1) = 'System Ended';

-- Completed
UPDATE passes SET system_completed = 1,
                  completed_by = NULL,
                  expired_at = NULL
WHERE (SELECT first_name FROM users WHERE id = passes.completed_by LIMIT 1) = 'System Ended';

-- MINTIME (YELLOW)


UPDATE ehp_v2_prod.passes SET flagged_at = NOW()
WHERE IF((IF((SELECT count(id) FROM (
                                   SELECT id FROM ehp_v2_prod.passes WHERE parent_id = passes.id limit 1
                                        ) as id) > 0, (SELECT TIMEDIFF(
                                                                                                             IF((SELECT completed_at
                                                                                                                 FROM (SELECT completed_at
                                                                                                                       FROM ehp_v2_prod.passes
                                                                                                                       WHERE parent_id = passes.id
                                                                                                                       limit 1) as completed_at) IS NOT NULL,
                                                                                                                (SELECT completed_at
                                                                                                                 FROM (SELECT completed_at
                                                                                                                       FROM ehp_v2_prod.passes
                                                                                                                       WHERE parent_id = passes.id
                                                                                                                       limit 1) as completed_at),
                                                                                                                (SELECT approved_at
                                                                                                                 FROM (SELECT approved_at
                                                                                                                       FROM ehp_v2_prod.passes
                                                                                                                       WHERE parent_id = passes.id
                                                                                                                       limit 1) as approved_at)),
                                                                                                             (SELECT passes.approved_at)
                                                                                                             )),
             (SELECT TIMEDIFF(passes.completed_at, passes.approved_at)))) > (SELECT min_time FROM teacher_pass_settings WHERE school_id=passes.school_id limit 1), 1, 0);


-- Ended on third state fixes (KSK)
UPDATE ehp_v2_prod.passes SET flagged_at=NULL
WHERE passes.parent_id > 0 AND (SELECT role_id FROM users WHERE id=passes.approved_by limit 1) = 1 AND passes.completed_at IS NULL AND passes.pass_status = 0 AND approved_with = 'autopass' AND `type`='KSK';

