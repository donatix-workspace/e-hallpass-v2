<?php


namespace App\Http\Services\CommonUserService\Translators;


use App\Http\Services\CommonUserService\Models\CommonDistrict;
use App\Http\Services\CommonUserService\Models\CommonTimezone;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class AnnouncementTranslator
{

    public static function collection($collection)
    {
        $data = [];
        foreach ($collection as $item) {
            $data[] = [
                'title' => $item['title'],
                'html' => $item['html'],
            ];
        }
        return $data;
    }


}
