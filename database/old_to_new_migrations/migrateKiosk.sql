INSERT INTO ehpv2.kiosks (`room_id`, `user_id`, `school_id`, `status`, `password`, `password_use`, `created_at`, `updated_at`, `expired_date`)
SELECT
                (SELECT IF(oldKiosk.room_id = 0, NULL,(SELECT id FROM rooms where old_id = oldKiosk.room_id limit 1))) as room_id,
                (SELECT IF(oldKiosk.teacher_id = 0,NULL,(SELECT id FROM users where old_id = oldKiosk.teacher_id limit 1))) as user_id,
                (SELECT id FROM schools where old_id = oldKiosk.school_id limit 1) as school_id,
                (SELECT oldKiosk.status),
                (SELECT oldKiosk.password),
                (SELECT CONVERT_TZ(oldKiosk.password_use,
                                   (SELECT timezone from schools where old_id = oldKiosk.school_id limit 1),
                                           'UTC'
                                     )) as password_use,
                (SELECT CONVERT_TZ(oldKiosk.created_at,
                                   (SELECT timezone from schools where old_id = oldKiosk.school_id limit 1),
                                   'UTC'
                            )) as created_at,
                (SELECT CONVERT_TZ(oldKiosk.updated_at,
                    (SELECT timezone from schools where old_id = oldKiosk.school_id limit 1),
                                           'UTC'
                                    )) as updated_at,
                (SELECT CONVERT_TZ(oldKiosk.expired_date,
                                   (SELECT timezone from schools where old_id = oldKiosk.school_id limit 1),
                                   'UTC'
                            )) as expired_date
FROM ehallpassDB.kiosk as oldKiosk
