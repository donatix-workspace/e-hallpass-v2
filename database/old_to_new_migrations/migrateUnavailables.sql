SET FOREIGN_KEY_CHECKS = 0;

START TRANSACTION ;
truncate ehp_v2_prod.unavailables;

INSERT INTO ehp_v2_prod.unavailables (unavailable_type, unavailable_id, school_id, from_date, to_date, status, recurrence_type, recurrence_days, recurrence_week, recurrence_end_at, comment, created_at, updated_at, alerted_at, canceled_for_today_at)
SELECT
(SELECT IF(oldOOO.room_id > 0, 'App\\Models\\Room', 'App\\Models\\User')),
(SELECT IF(oldOOO.room_id > 0, (SELECT id FROM ehp_v2_prod.rooms WHERE old_id=oldOOO.room_id limit 1), (SELECT id FROM ehp_v2_prod.users WHERE old_id=oldOOO.user_id))),
(SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldOOO.school_id),
(SELECT CONVERT_TZ(oldOOO.from_datetime,
                   (SELECT timezone from schools where old_id = oldOOO.school_id limit 1),
                   'UTC'
            )),
(SELECT CONVERT_TZ(oldOOO.to_datetime,
                   (SELECT timezone from schools where old_id = oldOOO.school_id limit 1),
                   'UTC'
            )),
(SELECT oldOOO.status),
(SELECT IF(oldOOO.recurrence != 'none', oldOOO.recurrence, (SELECT NULL))),
(SELECT IF(oldOOO.week_days IS NOT NULL AND oldOOO.week_days != ' ', (SELECT ehp_v2_prod.getRecurrenceDays(oldOOO.week_days)), null)),
(SELECT NULL),
(SELECT IF(oldOOO.recurrence != 'none',
           (SELECT CONVERT_TZ(oldOOO.to_datetime,
                              (SELECT timezone from schools where old_id = oldOOO.school_id limit 1),
                              'UTC'
                       )), NULL
    )),
(SELECT oldOOO.comment),
(SELECT CONVERT_TZ(oldOOO.updated_at,
                   (SELECT timezone from schools where old_id = oldOOO.school_id limit 1),
                   'UTC'
            )),
(SELECT CONVERT_TZ(oldOOO.created_at,
                   (SELECT timezone from schools where old_id = oldOOO.school_id limit 1),
                   'UTC'
            )),
(SELECT NULL),
(SELECT IF(DATE(oldOOO.today_status)=CURDATE(),NOW(),NULL))
FROM ehallpassDB.unavailability as oldOOO;
COMMIT;
SET FOREIGN_KEY_CHECKS = 1;
