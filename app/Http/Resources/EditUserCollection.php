<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EditUserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'user' => $this->collection->get('user'),
                'prevent_archive' => $this->collection->get('prevent_archive'),
                'kiosk' => $this->collection->get('kiosk'),
                'auth_type' => $this->collection->get('auth_type')
            ],
            'status' => 'success'
        ];
    }
}
