<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaffSchedule extends Model
{
    use HasFactory;

    protected $with = ['room', 'user'];

    protected $appends = ['room_pin'];

    protected $guarded = [];

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return \Illuminate\Database\Eloquent\HigherOrderBuilderProxy|mixed|string|null
     */
    public function getRoomPinAttribute()
    {
        return optional(
            Pin::where('pinnable_type', Room::class)
                ->where('pinnable_id', $this->room_id)
                ->first()
        )->pin;
    }

    /**
     * @return LocationCapacity|mixed|null
     */
    public function getLimitAttribute()
    {
        if ($this->room) {
            if ($this->room->capacity !== null) {
                return $this->room->capacity->limit;
            }
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getRoomAdvanceAttribute()
    {
        $roomAdvanceProperties = null;

        if ($this->room !== null) {
            $roomAdvanceProperties = $this->room
                ->capacity()
                ->where('room_id', $this->room_id)
                ->where('school_id', $this->school_id)
                ->with('room:id,name')
                ->first();
        }

        return $roomAdvanceProperties !== null
            ? $roomAdvanceProperties
            : [
                'room' => [
                    'id' => $this->room->id,
                    'name' => $this->room->name
                ]
            ];
    }

    /**
     * @param $query
     * @param $user
     * @return mixed
     */
    public function scopeOfUserId($query, $user)
    {
        return $query->where('user_id', $user);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }
}
