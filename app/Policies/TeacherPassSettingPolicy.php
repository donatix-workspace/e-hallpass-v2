<?php

namespace App\Policies;

use App\Models\TeacherPassSetting;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeacherPassSettingPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return !$user->isStudent();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\TeacherPassSetting $teacherPassSetting
     * @return mixed
     */
    public function view(User $user, TeacherPassSetting $teacherPassSetting)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\TeacherPassSetting $teacherPassSetting
     * @return mixed
     */
    public function update(User $user, TeacherPassSetting $teacherPassSetting)
    {
        return $user->isAdmin() && $user->school_id === $teacherPassSetting->school_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\TeacherPassSetting $teacherPassSetting
     * @return mixed
     */
    public function delete(User $user, TeacherPassSetting $teacherPassSetting)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\TeacherPassSetting $teacherPassSetting
     * @return mixed
     */
    public function restore(User $user, TeacherPassSetting $teacherPassSetting)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\TeacherPassSetting $teacherPassSetting
     * @return mixed
     */
    public function forceDelete(User $user, TeacherPassSetting $teacherPassSetting)
    {
        //
    }
}
