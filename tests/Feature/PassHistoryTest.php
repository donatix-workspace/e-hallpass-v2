<?php

namespace Tests\Feature;

use App\Models\AppointmentPass;
use App\Models\Pass;
use App\Models\Period;
use App\Models\Room;
use App\Models\RoomType;
use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\RolesDatabaseSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PassHistoryTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;
    protected $firstUser;
    protected $teacherUser;
    protected $pass;
    protected $period;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->room = Room::factory()->count(1)->for(RoomType::factory()->create([
            'school_id' => $this->school->id
        ]))->create([
            'school_id' => $this->school->id,
            'trip_type' => 2, // trip one way
        ])->first();

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);

        $this->teacherUser = User::factory()->createOne([
            'role_id' => config('roles.teacher'),
            'school_id' => $this->school->id
        ]);

        $this->pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\Room',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->room->id,
            'user_id' => $this->firstUser->id,
            'approved_at' => Carbon::now()->setTime(12, 00),
            'completed_at' => Carbon::now()->setTime(12, 00),
            'completed_by' => $this->adminUser->id
        ]);
    }

    /**
     * @test
     * An adult can view filters entities
     * @return void
     */
    public function an_adult_can_view_filters_entities()
    {
        $this->actingAs($this->adminUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->get(route('api.v5.admin.passes-histories'))
            ->assertJsonStructure(['data', 'status']);
    }

    /**
     * @test
     * An adult can run filters
     * @return void
     */
    public function an_adult_can_run_filters()
    {
        $this->actingAs($this->adminUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->get(route('api.v5.admin.passes-histories.filter') . '?sort=asc')
            ->assertStatus(200);
    }

    /**
     * @test
     * An adult can edit pass time
     * @return void
     */
    public function an_adult_can_edit_pass_time()
    {
        $outTime = $this->pass->approved_at->setTime(13, 00)->format('H:i');
        $inTime = $this->pass->completed_at->setTime(13, 01)->format('H:i');
        $this->actingAs($this->adminUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->put(route('api.v5.admin.passes-histories.time', ['pass' => $this->pass->id]), [
                'out_time' => $outTime,
                'in_time' => $inTime
            ])
            ->assertOk()
            ->assertJsonStructure(['data', 'status']);

        $this->assertEquals($this->pass->approved_at, $this->pass->refresh()->approved_at);
    }

}
