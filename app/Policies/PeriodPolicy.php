<?php

namespace App\Policies;

use App\Models\Period;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PeriodPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return !$user->isStudent();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function order(User $user)
    {
        return !$user->isStudent();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * @param User $user
     * @param Period $period
     * @return bool
     */
    public function update(User $user, Period $period)
    {
        return $user->isAdmin() && $period->school_id === $user->school_id;
    }

    /**
     * @param User $user
     * @param Period $period
     * @return bool
     */
    public function view(User $user, Period $period)
    {
        return $user->isAdmin() && $period->school_id === $user->school_id;
    }

    /**
     * @param User $user
     * @param Period $period
     * @return bool
     */
    public function delete(User $user, Period $period)
    {
        return $user->isAdmin() && $period->school_id === $user->school_id;
    }

    /**
     * @param User $user
     * @param Period $period
     * @return bool
     */
    public function status(User $user, Period $period)
    {
        return $user->isAdmin() && $period->school_id === $user->school_id;
    }

}
