<?php

namespace App\Http\Requests;

use App\Rules\DaysKeyExistRule;
use App\Rules\RecurrenceMonthlyCorrectStructureRule;
use App\Rules\RecurrenceDaysRule;
use App\Rules\RecurrenceTypeRule;
use App\Rules\RoomOrUserExistsRule;
use App\Rules\RoomOrUserStringRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class UnavailableUpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && (auth()->user()->isAdmin() || auth()->user()->isTeacher() || auth()->user()->isStaff());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unavailable_id' => ['required', 'integer', new RoomOrUserExistsRule(Request::all())],
            'unavailable_type' => ['required', 'string', new RoomOrUserStringRule()],
            'from_date' => "date|required|after_or_equal:{$this->from_date}",
            'from_time' => "date_format:H:i|required",
            'to_date' => 'date|required|after:from_date',
            'to_time' => "date_format:H:i|required",
            'comment' => 'nullable|string|max:255',
            'recurrence_type' => ['nullable', new RecurrenceTypeRule()],
            'recurrence_days' => ['json', 'nullable', new DaysKeyExistRule(), new RecurrenceDaysRule()],
            'recurrence_week' => ['json', 'nullable', 'required_if:recurrence_type,monthly', new RecurrenceMonthlyCorrectStructureRule()],
            'recurrence_end_at' => 'date|after:yesterday|nullable'
        ];
    }
}
