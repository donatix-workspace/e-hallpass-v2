<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTriggeredToPolaritiesTable extends Migration
{
    public function up()
    {
        Schema::table('polarities', function (Blueprint $table) {
            $table->integer('times_triggered')->default(0)->nullable();
        });
    }

    public function down()
    {

    }
}
