<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Translators\SchoolTranslator;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class MapExistingUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:map {--id=} {--users} {--memberships} ';

    protected $admin;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Map Existing Users from CUS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        if ($this->option('users')) {
            $this->info('Map Existing Users with CUS');
            $this->mapUsers();
        }
        if ($this->option('memberships')) {
            $this->info('Map Existing Memberships with CUS');
            $this->mapMemberships();
        }

    }

    protected function mapUsers()
    {

        $dbQuery = DB::table('users')
            ->select('id', 'email')
            ->whereNull('cus_uuid')
            ->whereNull('deleted_at')
            ->where('status','>=',0)
            ->orderBy('id');

        $this->output->progressStart($dbQuery->count());
        $dbQuery->chunk('100', function ($users) {

            foreach ($users as $user) {

                $commonUser = CommonUser::select('id', 'uuid')->where('email', $user->email)->first();

                if ($commonUser) {
                    $existingUser = User::where('cus_uuid',$commonUser->uuid)->first();
                    if(!$existingUser){
                        DB::table('users')
                            ->where('id', $user->id)
                            ->update([
                                'cus_uuid' => $commonUser->uuid,
                                'common_user_id' => $commonUser->id,
                            ]);
                    }

                }

                $this->output->progressAdvance();
            }
        });


    }

    protected function mapMemberships()
    {

        $dbQuery = DB::table('schools_users')
            ->whereNull('membership_uuid')
            ->orderBy('user_id');

        $this->output->progressStart($dbQuery->count());
        $dbQuery->chunk('100', function ($memberships) {

            foreach ($memberships as $membership) {

                $user = User::where('id', $membership->user_id)->where('status','>=',0)->first();
                $school = School::where('id', $membership->school_id)->whereNotNull('cus_uuid')->first();



                if ($user && $school) {
                    $commonSchool = CommonSchool::select('id')->where('uuid',$school->cus_uuid)->first();

                    $commonMembership = CommonMembership::where('auth_client_id',2)
                    ->where('auth_user_id',$user->common_user_id)
                        ->where('common_bm_school_id',$commonSchool->id)->first();

                    if($commonMembership){
                        DB::table('schools_users')
                            ->where('user_id',$user->id)
                            ->where('school_id',$school->id)
                            ->update(['membership_uuid'=>$commonMembership->uuid]);
                    }
                }

                $this->output->progressAdvance();
            }
        });


    }


}
