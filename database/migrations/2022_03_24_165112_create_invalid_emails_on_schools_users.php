<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvalidEmailsOnSchoolsUsers extends Migration
{
    public function up()
    {
        Schema::create('invalid_emails_on_schools_users', function (
            Blueprint $table
        ) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('school_domain')->nullable();
            $table->integer('school_id')->nullable();
            $table->string('user_domain')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('invalid_emails_on_schools_users');
    }
}
