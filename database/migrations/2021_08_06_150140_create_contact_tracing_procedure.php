<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactTracingProcedure extends Migration
{
    public function up()
    {
        $procedure = "DROP PROCEDURE IF EXISTS `run_contact_tracing`;
            CREATE PROCEDURE `run_contact_tracing` (userIdVar INT, schoolIdVar INT, date1Var TEXT, date2Var TEXT)
            BEGIN
            SELECT p2.user_id, p2.first_name, p2.last_name, p2.email,
       count(p2.user_id) as overlap_time_count,
       (
           select if(count(*) > 0, 1, 0)
           WHERE (p2.from_type = p1.from_type AND p2.from_id = p1.from_id) OR (p2.to_type = p1.to_type AND p2.to_id = p1.to_id)
       )                 as share_same_location
FROM (select approved_at,
             completed_at,
             created_at,
             from_type,
             to_type,
             from_id,
             to_id,
             user_id
      FROM passes
      WHERE user_id = userIdVar
        AND school_id = schoolIdVar
        AND created_at BETWEEN date1Var AND date2Var) p1
         JOIN (SELECT user_id,
                      passes.approved_at,
                      passes.completed_at,
                      passes.created_at,
                      passes.from_type,
                      passes.to_type,
                      passes.from_id,
                      passes.to_id,
                      users.first_name,
                      users.last_name,
                      users.email
               FROM passes INNER JOIN users ON users.id = passes.user_id
               WHERE passes.user_id != userIdVar
                 AND passes.school_id = schoolIdVar
                 AND passes.created_at between date1Var AND date2Var) p2
WHERE (p2.approved_at BETWEEN p1.approved_at AND p1.completed_at OR
       p2.completed_at BETWEEN p1.approved_at AND p1.completed_at)
   OR (p1.approved_at BETWEEN p2.approved_at AND p2.completed_at OR
       p1.completed_at BETWEEN p2.approved_at AND p2.completed_at)
GROUP BY p2.user_id;
            END;";
        \DB::unprepared($procedure);
    }

    public function down()
    {
        $procedureDrop = 'DROP PROCEDURE IF EXISTS `run_contact_tracing`';
        \DB::unprepared($procedureDrop);
    }
}
