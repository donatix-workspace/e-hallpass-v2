<?php

namespace Database\Factories;

use App\Models\Kiosk;
use Illuminate\Database\Eloquent\Factories\Factory;

class KioskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Kiosk::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'room_id' => $this->faker->numberBetween(1,10),
            'user_id' => $this->faker->numberBetween(1,10),
            'school_id' => $this->faker->numberBetween(1,10),
            'status' => $this->faker->numberBetween(1,10),
            'password_use' => $this->faker->dateTime,
            'password' => $this->faker->numberBetween(1,10),
        ];
    }
}
