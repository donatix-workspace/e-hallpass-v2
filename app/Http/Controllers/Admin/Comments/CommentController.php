<?php

namespace App\Http\Controllers\Admin\Comments;

use App\Events\PassCommentCreatedEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\CommentResource;
use App\Models\AppointmentPass;
use App\Models\Comment;
use App\Models\Pass;
use App\Notifications\Push\StudentNotification;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Notification;

/**
 * @group Comments
 */
class CommentController extends Controller
{
    /**
     * View all comments on an appointment.
     *
     * @urlParam appointmentPass integer required The id of the appointment
     *
     * @param AppointmentPass $appointmentPass
     *
     * @apiResourceModel App\Models\Comment
     * @apiResourceCollection App\Http\Resources\CommentCollection
     *
     * @authenticated
     * @return CommentCollection
     * @throws AuthorizationException
     */
    public function appointment(AppointmentPass $appointmentPass)
    {
        $user = auth()->user();

        $this->authorize('viewAppointmentComments', [
            Comment::class,
            $appointmentPass
        ]);

        $comments = !$user->isStudent()
            ? $appointmentPass->comments
            : $appointmentPass
                ->comments()
                ->with('user')
                ->where('permission', Comment::ALL_USERS_CAN_SEE)
                ->get();

        return new CommentCollection($comments);
    }

    /**
     * View all comments on a pass.
     *
     *
     * @param Pass $pass
     * @return CommentCollection
     * @throws AuthorizationException
     * @urlParam pass integer required The id of the pass
     *
     * @apiResourceCollection App\Http\Resources\CommentCollection
     * @apiResourceModel App\Models\Comment
     *
     * @authenticated
     */
    public function pass(Pass $pass)
    {
        $user = auth()->user();
        $this->authorize('viewPassComments', [Comment::class, $pass]);

        $comments = !$user->isStudent()
            ? $pass->comments
            : $pass
                ->comments()
                ->with('user')
                ->where('permission', Comment::ALL_USERS_CAN_SEE)
                ->get();

        return new CommentCollection($comments);
    }

    /**
     * Create a comment
     *
     * @urlParam commentableId integer required The id of the entity for comment.
     * @bodyParam commentable_type string required The type of the comment (App\Models\Pass, App\Models\AppointmentPass)
     * @bodyParam comment string required The comment
     * @bodyParam permission boolean Select who can view the comment (Only for admins) Example: 1 -> Only admins & staff, 0 -> Both student and staff.
     *
     * @apiResourceModel App\Models\Comment
     * @apiResource App\Http\Resources\CommentResource
     *
     * @param CommentRequest $request
     * @param $commentableId
     * @return CommentResource
     * @throws AuthorizationException
     */
    public function store(CommentRequest $request, $commentableId)
    {
        $user = auth()->user();
        $commentableType = $request->input('commentable_type');
        $this->authorize('create', [
            Comment::class,
            $commentableId,
            $commentableType
        ]);

        $comment = Comment::create([
            'user_id' => $user->id,
            'commentable_type' => $commentableType,
            'commentable_id' => $commentableId,
            'permission' => !$user->isStudent()
                ? $request->input('permission')
                : 0,
            'school_id' => $user->school_id,
            'comment' => $request->input('comment')
        ]);

        if ($commentableType === Pass::class) {
            $pass = Pass::find($commentableId);
            event(
                new PassCommentCreatedEvent(
                    $pass,
                    $request->input('comment'),
                    $commentableType,
                    !$user->isStudent(),
                    $comment
                )
            );
            //            Notification::send(
            //                $pass,
            //                new StudentNotification('PassCommentCreatedEvent', [
            //                    'message' => $request->input('comment')
            //                ])
            //            );
        }

        if ($commentableType === AppointmentPass::class) {
            $appointmentPass = AppointmentPass::find($commentableId);
            event(
                new PassCommentCreatedEvent(
                    $appointmentPass,
                    $request->input('comment'),
                    $commentableType
                )
            );
            //            Notification::send(
            //                $appointmentPass,
            //                new StudentNotification('PassCommentCreatedEvent', [
            //                    'message' => $request->input('comment')
            //                ])
            //            );
        }

        return new CommentResource($comment);
    }
}
