<?php

namespace App\Http\Controllers\Search;

use App\Elastic\Rules\BelongsToSchoolSearchRule;
use App\Elastic\Rules\UserActiveByConditionRule;
use App\Elastic\Rules\UserActiveRule;
use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Http\Resources\RoomCollection;
use App\Http\Resources\UserCollection;
use App\Models\AuthType;
use App\Models\Module;
use App\Models\Room;
use App\Models\RoomRestriction;
use App\Models\School;
use App\Models\StaffSchedule;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class IndexController extends Controller
{
    protected $type;

    public const USER_TYPE = 'user'; // return all users
    public const TEACHER_TYPE = 'teacher'; // return all teachers;
    public const STAFF_TYPE = 'staff'; // return all staff members
    public const ADMIN_TYPE = 'admin'; // return all admins
    public const STUDENT_TYPE = 'student'; // return all staff members
    public const COMBINED_TYPE = 'combined'; // return locations & users (only teachers,staffs,admins,locations)
    public const LOCATION_TYPE = 'locations'; // return locations & users
    public const ADULTS = 'adults'; // return all adults
    public const ME_AND_MY_ASSIGNED = 'my_assigned';
    public const APPOINTMENT = 'appointment';
    public const KIOSK = 'kiosk';

    /**
     * Initialize the type from the request
     */
    public function __construct()
    {
        $this->type = request()->get('type');
    }

    /**
     * @param array $schoolModuleOption
     * @return int[]
     */
    public function getAllowedAppointmentSearchableRoles(
        array $schoolModuleOption
    ): array {
        $rolesListed = [];

        foreach (
            $schoolModuleOption
            as $moduleOptionRole => $moduleOptionFlag
        ) {
            if (array_key_exists($moduleOptionRole, config('roles'))) {
                if ($moduleOptionFlag) {
                    array_push(
                        $rolesListed,
                        config('roles.' . $moduleOptionRole)
                    );
                }
            }
        }

        return $rolesListed;
    }

    /**
     * Global search
     *
     * @queryParam search_query string required String to search
     * @queryParam type string required The type of the search response.
     *
     *
     * @authenticated
     * @param SearchRequest $request
     * @return RoomCollection|UserCollection|JsonResponse|mixed
     */
    public function index(SearchRequest $request)
    {
        $user = auth()->user();

        $collection = UserCollection::class;
        $query =
            $request->get('search_query') === null
                ? config('scout_elastic.all_records')
                : convertElasticQuery($request->get('search_query'));

        $locations = null;
        $users = null;
        $data = null;

        switch ($this->type) {
            default:
                $data = User::search(
                    $query === config('scout_elastic.all_records')
                        ? '__all'
                        : $query
                )
                    ->rule(BelongsToSchoolSearchRule::class)
                    ->rule(UserActiveByConditionRule::class)
                    ->select([
                        'id',
                        'first_name',
                        'last_name',
                        'email',
                        'role_id'
                    ])
                    ->with(['activeUnavailability'])
                    ->where('auth_type', '!=', AuthType::HIDDEN)
                    ->orderBy('last_name', 'asc')
                    //                    ->when(
                    //                        auth()
                    //                            ->user()
                    //                            ->isStudent() || $request->has('active_only'),
                    //                        function ($query) {
                    //                            $query->where('status', User::ACTIVE);
                    //                        }
                    //                    )
                    ->when(
                        $this->type !== self::USER_TYPE &&
                            $this->type !== self::ADULTS &&
                            $this->type !== self::LOCATION_TYPE,
                        function ($query) {
                            $query->where(
                                'role_id',
                                config('roles.' . $this->type)
                            );
                        }
                    )
                    ->when($this->type === self::ADULTS, function ($query) {
                        $query->where('role_id', '!=', config('roles.student'));
                    })
                    ->when($this->type === self::USER_TYPE, function ($query) {
                        $query->where(
                            'role_id',
                            '!=',
                            config('roles.superadmin')
                        );
                    })
                    ->where('is_substitute', User::INACTIVE)
                    ->paginate(User::recordsPerPage());

                $collection = UserCollection::class;
                break;
            case self::LOCATION_TYPE:
                $data = Room::search($query)
                    ->with(['capacity', 'activeUnavailability'])
                    ->when(
                        !auth()
                            ->user()
                            ->isStudent(),
                        function ($query) {
                            $query->with(['ownerPin', 'capacity']);
                        }
                    )
                    ->orderBy('name', 'asc')
                    ->select(['id', 'name', 'trip_type', 'comment_type'])
                    ->when(
                        auth()
                            ->user()
                            ->isStudent() || $request->has('active_only'),
                        function ($query) {
                            $query->where('status', Room::ACTIVE);
                        }
                    )
                    ->where('school_id', $user->school_id)
                    ->paginate(Room::recordsPerPage());

                $collection = RoomCollection::class;
                break;
            case self::COMBINED_TYPE:
                $locations = Room::search($query)
                    ->with(['capacity', 'activeUnavailability'])
                    ->orderBy('name', 'asc')
                    ->select(['id', 'name', 'trip_type', 'comment_type'])
                    ->when(
                        auth()
                            ->user()
                            ->isStudent() || $request->has('active_only'),
                        function ($query) {
                            $schoolLocationsRestrictionsIds = RoomRestriction::getRestrictedRoomIds();

                            if (
                                $schoolLocationsRestrictionsIds->isNotEmpty() &&
                                auth()
                                    ->user()
                                    ->isStudent()
                            ) {
                                $query->whereNotIn(
                                    'id',
                                    $schoolLocationsRestrictionsIds->toArray()
                                );
                            }

                            $query->where('status', Room::ACTIVE);
                        }
                    )
                    ->when($request->has('only_allowed_apt'), function (
                        $query
                    ) {
                        return $query->where(
                            'enable_appointment_passes',
                            Room::ACTIVE
                        );
                    })
                    ->where('school_id', $user->school_id)
                    ->paginate(Room::recordsPerPage());

                $users = User::search(
                    $query === config('scout_elastic.all_records')
                        ? '__all'
                        : $query
                )
                    ->rule(BelongsToSchoolSearchRule::class)
                    ->rule(UserActiveByConditionRule::class)
                    ->with(['activeUnavailability'])
                    ->select([
                        'id',
                        'first_name',
                        'last_name',
                        'email',
                        'role_id'
                    ])
                    ->orderBy('last_name', 'asc')
                    //                    ->when(
                    //                        auth()
                    //                            ->user()
                    //                            ->isStudent() || $request->has('active_only'),
                    //                        function ($query) {
                    //                            $query->where('status', User::ACTIVE);
                    //                        }
                    //                    )
                    ->when($request->has('only_allowed_apt'), function (
                        $query
                    ) {
                        return $query->where(
                            'allow_appointment_requests',
                            User::ACTIVE
                        );
                    })
                    ->where('role_id', '!=', config('roles.student'))
                    ->where('is_substitute', User::INACTIVE)
                    ->where('auth_type', '!=', AuthType::HIDDEN)
                    ->paginate(User::recordsPerPage());

                $collection = self::COMBINED_TYPE;
                break;
            case self::ME_AND_MY_ASSIGNED:
                $staffSchedules = collect(
                    StaffSchedule::ofUserId($user->id)
                        ->select('room_id')
                        ->get()
                )
                    ->pluck('room_id')
                    ->toArray();

                $data = Room::search($query)
                    ->with(['capacity', 'activeUnavailability'])
                    ->orderBy('name', 'asc')
                    ->select(['id', 'name', 'trip_type', 'comment_type'])
                    ->whereIn('id', $staffSchedules)
                    ->when(
                        auth()
                            ->user()
                            ->isStudent() || $request->has('active_only'),
                        function ($query) {
                            $query->where('status', Room::ACTIVE);
                        }
                    )
                    ->where('school_id', $user->school_id)
                    ->paginate(Room::recordsPerPage());

                $collection = RoomCollection::class;
                break;

            case self::APPOINTMENT:
                $schoolModuleOption = json_decode(
                    $user->school->getModuleOptions(Module::APPOINTMENTPASS),
                    true
                );

                $allowedRoles = $this->getAllowedAppointmentSearchableRoles(
                    $schoolModuleOption
                );

                if (count($allowedRoles)) {
                    $allowedRoles = elasticWhereInRawString(
                        'role_id',
                        $allowedRoles
                    );

                    $queryForUserRoles = $query . " AND ($allowedRoles)";
                }

                $users = User::search(
                    $query === config('scout_elastic.all_records')
                        ? '__all'
                        : $queryForUserRoles
                )
                    ->rule(BelongsToSchoolSearchRule::class)
                    ->rule(UserActiveByConditionRule::class)
                    ->select([
                        'id',
                        'first_name',
                        'last_name',
                        'email',
                        'role_id'
                    ])
                    ->with(['activeUnavailability'])
                    ->orderBy('last_name', 'asc')
                    //                    ->when(
                    //                        auth()
                    //                            ->user()
                    //                            ->isStudent() || $request->has('active_only'),
                    //                        function ($query) {
                    //                            $query->where('status', User::ACTIVE);
                    //                        }
                    //                    )
                    ->when($request->has('only_allowed_apt'), function (
                        $query
                    ) {
                        return $query->where(
                            'allow_appointment_requests',
                            User::ACTIVE
                        );
                    })
                    ->where('is_substitute', User::INACTIVE)
                    ->paginate(User::recordsPerPage());

                if (
                    array_key_exists('location', $schoolModuleOption) &&
                    $schoolModuleOption['location']
                ) {
                    $locations = Room::search($query)
                        ->with(['capacity', 'activeUnavailability'])
                        ->orderBy('name', 'asc')
                        ->select(['id', 'name', 'trip_type', 'comment_type'])
                        ->when(
                            auth()
                                ->user()
                                ->isStudent() || $request->has('active_only'),
                            function ($query) {
                                $query->where('status', Room::ACTIVE);
                            }
                        )
                        ->when($request->has('only_allowed_apt'), function (
                            $query
                        ) {
                            return $query->where(
                                'enable_appointment_passes',
                                Room::ACTIVE
                            );
                        })
                        ->where('school_id', $user->school_id)
                        ->paginate(Room::recordsPerPage());
                } else {
                    // Disable room search
                    $locations = Room::search($query)
                        ->where('school_id', -1)
                        ->paginate(Room::recordsPerPage());
                }

                $collection = self::COMBINED_TYPE;
                break;
            case self::KIOSK:
                $school = School::select('id')
                    ->where('cus_uuid', $request->input('cus_uuid'))
                    ->firstOrFail();

                $locations = Room::search($query)
                    ->with(['capacity', 'activeUnavailability'])
                    ->orderBy('name', 'asc')
                    ->select(['id', 'name', 'trip_type', 'comment_type'])
                    ->where('status', Room::ACTIVE)
                    ->when($request->has('only_allowed_apt'), function (
                        $query
                    ) {
                        return $query->where(
                            'enable_appointment_passes',
                            Room::ACTIVE
                        );
                    })
                    ->where('school_id', $school->id)
                    ->paginate(Room::recordsPerPage());

                $users = User::search(
                    $query === config('scout_elastic.all_records')
                        ? '__all'
                        : $query
                )
                    ->rule(BelongsToSchoolSearchRule::class)
                    ->rule(UserActiveRule::class)
                    ->with(['activeUnavailability'])
                    ->select([
                        'id',
                        'first_name',
                        'last_name',
                        'email',
                        'role_id'
                    ])
                    ->orderBy('last_name', 'asc')
                    //                    ->where('status', User::ACTIVE)
                    ->when($request->has('only_allowed_apt'), function (
                        $query
                    ) {
                        return $query->where(
                            'allow_appointment_requests',
                            User::ACTIVE
                        );
                    })
                    ->where('role_id', '!=', config('roles.student'))
                    ->where('is_substitute', User::INACTIVE)
                    ->paginate(User::recordsPerPage());

                $collection = self::COMBINED_TYPE;
                break;
        }
        if ($collection === self::COMBINED_TYPE) {
            $totalCombinedPages = round(
                ($users->total() + $locations->total()) / Room::recordsPerPage()
            );

            return response()->json([
                'data' => [
                    'users' => new UserCollection($users),
                    'locations' => new RoomCollection($locations)
                ],
                'status' => __('success'),
                'meta' => [
                    'current_page' => $users->currentPage(),
                    'last_page' =>
                        $totalCombinedPages > 0 ? $totalCombinedPages : 1,
                    'per_page' => $users->perPage(),
                    'total' => $users->total() + $locations->total(),
                    'to' => $users->count() + $locations->count()
                ]
            ]);
        }

        return new $collection($data);
    }
}
