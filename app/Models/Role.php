<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    public static function getRoleIdByName($roleName)
    {
        return self::where('name', $roleName)->first()->id;
    }

    public static function getRoleNameById($roleId)
    {
        $roleModel = self::where('id', $roleId)->first();

        if (!$roleModel) {
            return 0;
        }
        return $roleModel->name;
    }

    public static function getRoles()
    {
        return self::where('status', 1)
            ->select('name')
            ->get()
            ->pluck('name');
    }


    public static function v1ToV2($v1Role)
    {
        $roles = [
            0 => 6, //subs
            -2 => null, //none => null
            1 => 5, //superadmin => superadmin
            2 => 2, //admin => admin
            3 => 3, //teacher => teacher
            4 => 1, //student => student
            5 => 4, //staff => staff
            6 => 2, //schooladmin => admin
            7 => null, //system => null
            8 => null, //norole => null
            10 => null
        ];

        return $roles[$v1Role];
    }

    public static function v1ToCUS($v1Role)
    {
        $roles = [
            0 => 3, //subs
            -2 => null, //none => null
            1 => null, //superadmin => superadmin
            2 => 2, //admin => admin
            3 => 2, //teacher => teacher
            4 => 5, //student => student
            5 => 4, //staff => staff
            6 => 1, //schooladmin => admin
            7 => null, //system => null
            8 => null, //norole => null
            10 => null
        ];
        return $roles[$v1Role];

    }

    public static function v2ToV1($v2Role)
    {
        $roles = [
            1 => 4,//student
            2 => 2,//admin
            3 => 3,//teacher
            4 => 5,//staff
        ];
        return $roles[$v2Role];
    }

    public static function v2ToCUS($v2Role)
    {
        $roles = [
            1 => 5,//student
            2 => 1,//admin
            3 => 2,//teacher
            4 => 4,//staff
        ];
        return $roles[$v2Role];
    }

    public static function cusToV1($cusRole)
    {
        $roles = [
            1 => 2,//admin
            2 => 3,//teacher
            3 => 6,//teacher_substitute
            4 => 5,//staff
            5 => 4,//student
        ];
        return $roles[$cusRole];

    }

    public static function cusToV2($cusRole)
    {
        $roles = [
            1 => 2,
            2 => 3,
            3 => 3,
            4 => 4,
            5 => 1
        ];
        return $roles[$cusRole];
    }
}
