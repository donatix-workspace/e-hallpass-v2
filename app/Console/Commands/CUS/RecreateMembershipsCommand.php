<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\EntityService;
use App\Http\Services\CommonUserService\Models\CommonDistrict;
use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonSchoolDomain;
use App\Http\Services\CommonUserService\Models\CommonTimezone;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Models\SchoolsEhp;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CancelAndUpdateAppointmentsAfterArchiveJob;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\AuthType;
use App\Models\Domain;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\SchoolUser;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class RecreateMembershipsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:memberships {--recreate=} {--skip=} {--fromId=} {--toId=} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'RecreateMemberships from CUS';

    protected $client;
    protected $oldDb;
    protected $adminUser;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->oldDb = DB::connection('oldDB');

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->adminUser = User::where('email', 'viktor.lalev@donatix.net')
            ->latest()
            ->first();
        $this->client = new CommonUserService($this->adminUser);

        $skip = $this->option('skip') ?? 0;
        $fromId = $this->option('fromId') ?? 0;
        $toId = $this->option('toId') ?? 3000;

        if ($this->option('recreate')) {
            $this->info('Recreate Memberships from CUS');
            if ($this->option('recreate') == 'all') {
                $this->info("From {$fromId} to {$toId}");

                $schools = School::where('status', 1)
                    ->where('id', '>', $fromId)
                    ->where('id', '<=', $toId)
                    ->orderBy('id')->get();
                foreach ($schools as $school) {
                    $this->recreate($school);
                }
            } else {

                $school = School::where('id', $this->option('recreate'))->where('status', 1)->first();
                if (!$school) {
                    return false;
                }
                $this->recreate($school, $skip);
            }
        }
    }

    protected function recreate($school, $skip = 0)
    {
        $this->info("Recreate Memberships for {$school->name} - {$school->id}");
        $dbQuery = Db::connection('commonDb')
            ->table('common_bm_membership as m')
            ->join('auth_user as u', 'm.auth_user_id', '=', 'u.id')
            ->join(
                'common_bm_school as s',
                'm.common_bm_school_id',
                '=',
                's.id'
            )
            ->leftJoin('auth_title as t', 'u.auth_title_id', '=', 't.id')
            ->selectRaw(
                'u.uuid as user_uuid,
            u.email as email,
            m.sis_id as student_sis_id,
            u.username as username,
            u.first_name as first_name,
            u.last_name as last_name,
            m.common_bm_role_membership_id as role_id,
            u.image as avatar,
            m.graduation_year as grade_year,
            u.r_status as user_status,
            m.r_status as membership_status,
            u.last_login_time as last_login_time,
            u.created as created_at,
            u.modified as updated_at,
            s.uuid as school_uuid,
            m.uuid as membership_uuid,
            t.r_name as title,
            u.id as common_user_id,
            m.archived_at as deleted_at
            '
            )
            ->where('m.auth_client_id', 2)
            ->where('s.uuid', $school->cus_uuid)
            ->where('m.not_archived',1)
            ->orderBy('m.id', 'asc');
        $this->output->progressStart($dbQuery->count() - $skip);

        $dbQuery
            ->when($skip, function ($query) use ($skip) {
                $query->skip($skip);
            })
            ->chunk(10, function ($fullRecords) use ($school) {
                foreach ($fullRecords as $fullRecord) {

                    $data = UserTranslator::translateJoinedRecord(
                        $fullRecord,
                        $school
                    );
                    $user = User::withoutGlobalScopes()
                        ->where('cus_uuid', $data['cus_uuid'])
                        ->first();
                    if ($user) {
                        User::enableMassIndex();


                        $membershipData = [
                            'membership_uuid' => $fullRecord->membership_uuid,
                            'role_id' => CommonMembership::getRoleId(
                                $fullRecord->role_id
                            )
                        ];


                        DB::table('schools_users')->updateOrInsert(
                            [
                                'school_id' => $school->id,
                                'user_id' => $user->id
                            ],
                            $membershipData
                        );

                        $user->searchable();
                        User::disableMassIndex();

                    }
                    $this->output->progressAdvance();
                }
            });

    }

}
