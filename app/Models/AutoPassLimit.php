<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AutoPassLimit extends Model
{
    use HasFactory;

    protected $table = 'auto_passes_limits';
    protected $with = ['user'];


    protected $fillable = [
        'user_id', 'school_id',
        'limit'
    ];

    /**
     * @param $query
     * @param $id
     * @return mixed
     */
    public function scopeOfUserId($query, $id)
    {
        return $query->where('user_id', $id);
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    public function scopeWithValidLimit($query)
    {
        return $query->where('limit', '!=', -1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }
}
