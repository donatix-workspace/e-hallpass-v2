<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class RecurrenceMonthlyCorrectStructureRule implements Rule
{
    /**
     * @var string[]
     */
    public $daysInTheWeek;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->daysInTheWeek = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $inputMonthDay = collect(json_decode($value, true));

        return $inputMonthDay->diff($this->daysInTheWeek)->count() === 0 && Arr::isAssoc($inputMonthDay->toArray());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('monthly.structure.invalid');
    }
}
