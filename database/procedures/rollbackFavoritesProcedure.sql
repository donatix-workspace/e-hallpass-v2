DELIMITER ;;
CREATE PROCEDURE rollbackFavoritesProcedure()
BEGIN
   DECLARE i,n,o INT DEFAULT 0;
   DECLARE perPage INT DEFAULT 2500;


   SELECT count(id) from ehpv2.student_favorites WHERE created_at BETWEEN '2021-12-01 00:00:00' AND '2021-12-30 23:59:59' into n;
   SET i = 0;
   SET o = 0;
   SET perPage = 2500;

   WHILE i < n / perPage
       DO
       START TRANSACTION ;
       INSERT INTO ehallpassDB.student_favorite (possition, room_id, teacher_id, student_id, school_id, `order`, created_at, updated_at)
       SELECT
           (SELECT position) as position,
              (IF(favorable_type = 'App\\Models\\Room', (SELECT old_id FROM ehpv2.rooms WHERE id=favorable_id limit 1) ,0)) as room_id,
              (IF(favorable_type = 'App\\Models\\User', (SELECT old_id FROM ehpv2.users WHERE id=favorable_id limit 1), 0)) as teacher_id,
             (SELECT old_id from ehpv2.users WHERE id=user_id limit 1) as student_id,
              (SELECT old_id from ehpv2.schools WHERE id=school_id limit 1) as school_id,
       (SELECT 0),
           (SELECT CONVERT_TZ(created_at,
                              '+00:00',
                              (SELECT timezone_offset from ehpv2.schools where id = school_id limit 1))) as created_at,
           (SELECT CONVERT_TZ(updated_at,
                              '+00:00',
                              (SELECT timezone_offset from ehpv2.schools where id = school_id limit 1))) as updated_at
       FROM ehpv2.student_favorites
       WHERE created_at BETWEEN '2021-12-01 00:00:00' AND '2021-12-30 23:59:59'
       LIMIT o,perPage;
       COMMIT;
       end while ;;
end ;;
