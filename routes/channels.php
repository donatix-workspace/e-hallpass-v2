<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('test', function ($user) {
    return true;
});

Broadcast::channel('dashboard-stats.{userId}', function ($user, $userId) {
    return !$user->isStudent() && $user->id === (int) $userId;
});

Broadcast::channel('dashboard-building-refresh.{schoolId}', function (
    $user,
    $schoolId
) {
    return $user->school_id === (int) $schoolId;
});

Broadcast::channel('building-comments.{schoolId}', function ($user, $schoolId) {
    return $user->school_id === (int) $schoolId && !$user->isStudent();
});

Broadcast::channel('pass-blocks.{schoolId}', function ($user, $schoolId) {
    return $user->school_id === (int) $schoolId;
});

Broadcast::channel('user.passes.{userId}', function ($user, $userId) {
    return $user->id === (int) $userId && $user->isStudent();
});

Broadcast::channel('user.profile.{userId}', function ($user, $userId) {
    return $user->id === (int) $userId && $user->isStudent();
});

Broadcast::channel('pass-limits.{schoolId}', function ($user, $schoolId) {
    return $user->school_id === (int) $schoolId && $user->isStudent();
});

Broadcast::channel('school.passes.{schoolId}', function ($user, $schoolId) {
    return !$user->isStudent() && $user->school_id === (int) $schoolId;
});

Broadcast::channel('appointments.passes.{schoolId}', function (
    $user,
    $schoolId
) {
    return !$user->isStudent() && $user->school_id === (int) $schoolId;
});

Broadcast::channel('user.appointments.passes.{userId}', function (
    $user,
    $userId
) {
    return $user->isStudent() && $user->id === (int) $userId;
});

Broadcast::channel('user.unavailables.{userId}', function ($user, $userId) {
    return !$user->isStudent() && $user->id === (int) $userId;
});

Broadcast::channel('new-users', function ($user) {
    return $user->isAdmin();
});

Broadcast::channel('modules.{schoolId}', function ($user, $schoolId) {
    return $user->school_id === (int) $schoolId;
});

Broadcast::channel('passes.{schoolId}', function ($user, $schoolId) {
    return $user->school_id === (int) $schoolId && !$user->isStudent();
});

Broadcast::channel('unavailables.{schoolId}', function ($user, $schoolId) {
    return $user->school_id === (int) $schoolId && !$user->isStudent();
});

Broadcast::channel('student.unavailables.{schoolId}', function (
    $user,
    $schoolId
) {
    return $user->school_id === (int) $schoolId;
});
