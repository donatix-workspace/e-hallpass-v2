<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeletedMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deleted_memberships', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('membership_id');
            $table->string('membership_uuid');
            $table->string('created');
            $table->unsignedBigInteger('created_by');
            $table->timestamp('archived_at')->nullable();
            $table->string('email')->nullable();
            $table->string('sis_id')->nullable();
            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('school_cus_id');
            $table->string('school_cus_uuid');
            $table->unsignedBigInteger('school_old_id');
            $table->timestamp('cleared_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deleted_memberships');
    }
}
