<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'id' => 1,
            'name' => 'Appointmentpass',
            'display_name' => 'Appointment Pass',
            'option_json' => '{
                  "peer":"Peer Email Notification ",
                  "sms":"SMS Notification",
                  "email":"Email Alert Notification",
                  "admin":"Can student create appointment to admin",
                  "staff":"Can student create appointment to staff",
                  "location":"Can student create appointment to location",
                  "teacher":"Can student create appointment to teacher"
                  "admin_edit":"Can admin edit other passes that not belongs to him"
            }',
            'description' => 'Please select the Appointment Pass notification method(s) you would like to activate for your school.',
            'status' => '1',
        ]);
        DB::table('modules')->insert([
            'id' => 2,
            'name' => 'Auto Check-In',
            'display_name' => 'Auto Check-In',
            'option_json' => '',
            'description' => 'Auto Check-In enables assigning a public PIN to a room so that students can Check into room using that public PIN',
            'status' => '1',
        ]);
        DB::table('modules')->insert([
            'id' => 3,
            'name' => 'Kiosk',
            'display_name' => 'Kiosk (EHP Self Service) ',
            'option_json' => '{"kurl":"Enable URL Klogin method only, highly recommended for use with Chromebooks","usbcard":"Card  As Default Login","barcode":"Barcode As Default Login" ,"spassword":"Hide Student Kiosk Password Field"}',
            'description' => 'Kiosk (EHP Self Service)',
            'status' => '1',
        ]);
    }
}
