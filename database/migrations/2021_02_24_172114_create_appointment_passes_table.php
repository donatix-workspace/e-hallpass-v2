<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentPassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_passes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('created_by');
            $table->morphs('from');
            $table->morphs('to');
            $table->unsignedBigInteger('user_id');
            $table->timestamp('for_date');
            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('period_id');
            $table->unsignedBigInteger('pass_id')->nullable();
            $table->string('reason');
            $table->string('cancel_reason')->nullable();
            $table->timestamp('ran_at')->nullable();
            $table->timestamp('confirmed_at')->nullable();
            $table->timestamp('acknowledge_at')->nullable();
            $table->timestamp('canceled_at')->nullable();
            $table->timestamp('confirmed_by_teacher_at')->nullable();
            $table->timestamp('acknowledged_by_mail_at')->nullable();
            $table->timestamp('acknowledged_by_teacher_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamps();
        });
        Schema::table('appointment_passes', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('period_id')->references('id')->on('periods');
            $table->foreign('pass_id')->references('id')->on('passes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointment_passes', function (Blueprint $table) {
            $table->dropForeign('appointment_passes_created_by_foreign')->references('id')->on('users');
            $table->dropForeign('appointment_passes_user_id_foreign')->references('id')->on('users');
            $table->dropForeign('appointment_passes_school_id_foreign')->references('id')->on('schools');
            $table->dropForeign('appointment_passes_period_id_foreign')->references('id')->on('periods');
            $table->dropForeign('appointment_passes_pass_id_foreign')->references('id')->on('passes');
        });
        Schema::dropIfExists('appointment_passes');
    }
}
