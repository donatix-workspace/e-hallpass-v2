<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::factory()->createMany([
            ['id' => 1, 'name' => 'student'],
            ['id' => 2, 'name' => 'admin'],
            ['id' => 3, 'name' => 'teacher'],
            ['id' => 4, 'name' => 'staff'],
            ['id' => 5, 'name' => 'superadmin'],
            ['id' => 6, 'name' => 'substitute']
        ]);
    }
}
