<?php

namespace App\Http\Requests;

use App\Rules\ExtendedPassTimeRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:30', Rule::unique('rooms', 'name')
                ->where('school_id', optional(auth()->user())->school_id)
            ],
            'trip_type' => 'required|string|in:Layover,One Way,Roundtrip',
            'status' => 'required|boolean',
            'is_extended' => 'sometimes|boolean',
            'extended_pass_time' => ['required_if:is_extended,true', 'date_format:"H:i:s"', 'nullable', new ExtendedPassTimeRule()],
            'comment_type' => 'sometimes|string|in:Mandatory,Optional,Hidden',
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'extended_pass_time.required_if' => 'The extended pass time field is required',
            'name.unique' => 'This room name already exists',
            'comment_type.string' => 'Comment type must be chosen.'
        ];
    }
}
