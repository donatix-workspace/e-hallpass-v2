<?php

return [
    'canceled' => 1,
    'confirmed' => 2,
    'both' => 3
];
