<?php

namespace App\Http\Controllers\Admin\Unavailables;

use App\Events\FavoriteUnavailableEvent;
use App\Events\UnavailableUpdate;
use App\Http\Controllers\Controller;
use App\Http\Requests\UnavailableRequest;
use App\Http\Requests\UnavailableUpdateRequest;
use App\Http\Resources\UnavailableEntityCollection;
use App\Http\Resources\UnavailableResource;
use App\Models\Room;
use App\Models\StaffSchedule;
use App\Models\Unavailable;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @group Unavailable Teacher (Out Of Office)
 */
class UnavailableController extends Controller
{
    /**
     * Get unavailable form create details
     *
     * @queryParam search_query string Elastic search query
     * @queryParam per_page integer How many items will be shown per page.
     * @authenticated
     * @return UnavailableEntityCollection
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Unavailable::class);

        $user = auth()->user();

        if (
            (request()->has('search_query') &&
                request()->get('search_query') !== null) ||
            request()->has('sort')
        ) {
            $unavailables = Unavailable::searchableQueries();
        } else {
            $unavailables = Unavailable::with('unavailable')
                ->orderBy('from_date', 'desc')
                ->fromSchoolId($user->school_id)
                ->paginate(Unavailable::recordsPerPage());
        }

        if ($user->isTeacher() || $user->isStaff()) {
            if (
                (request()->has('search_query') &&
                    request()->get('search_query') !== null) ||
                request()->has('sort')
            ) {
                $unavailables = Unavailable::searchableQueries();
            } else {
                $unavailables = Unavailable::with('unavailable')
                    ->where('unavailable_type', User::class)
                    ->where('unavailable_id', $user->id)
                    ->orderBy('from_date', 'desc')
                    ->fromSchoolId($user->school_id)
                    ->paginate(Unavailable::recordsPerPage());
            }
        }

        return new UnavailableEntityCollection([
            'unavailables' => $unavailables
        ]);
    }

    /**
     * Create an unavailable (Out Of Office).
     *
     * @bodyParam unavailable_id integer required The id of the user/room
     * @bodyParam unavailable_type string required The type of the unavailable.
     * @bodyParam from_date date required From date unavailable enter in action.
     * @bodyParam to_date date required The date the unavailable ends.
     * @bodyParam recurrence_type string The type of the recurrence: (daily, weekly, monthly)
     * @bodyParam recurrence_days json What days from the week the recurrence should be run (as json with array key "days")
     * @bodyParam recurrence_week json Monthly week number for recurrence Example: {"2": "Monday"}
     * @bodyParam recurrence_end_at date The date on which recurrence won't be executed .
     * @bodyParam comment string Reason for the unavailable
     *
     *
     * @param UnavailableRequest $request
     * @return UnavailableResource|JsonResponse
     * @apiResourceModel App\Models\Unavailable
     * @apiResource App\Http\Resources\UnavailableResource
     * @throws AuthorizationException
     * @authenticated
     */
    public function store(UnavailableRequest $request)
    {
        $this->authorize('create', Unavailable::class);

        $user = auth()->user();
        // If the user is teacher or staff and the request user_id isn't equals to authenticated user id, abort.
        if (
            ($user->isTeacher() || $user->isStaff()) &&
            $request->input('unavailable_type') === User::class &&
            $user->id !== $request->input('unavailable_id')
        ) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('unavailable.undefined.user'),
                    'status' => __('fail')
                ],
                422
            );
        }

        if (
            $user->isTeacher() &&
            $request->input('unavailable_type') === Room::class
        ) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('unavailable.invalid.permission'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $unavailableExist = Unavailable::where(
            'from_date',
            '<=',
            Carbon::parse($request->input('from_date'))
                ->setTimezone(config('app.timezone'))
                ->toDateTimeString()
        )
            ->where(
                'to_date',
                '>=',
                Carbon::parse($request->input('to_date'))
                    ->setTimezone(config('app.timezone'))
                    ->toDateTimeString()
            )
            ->where('unavailable_type', $request->input('unavailable_type'))
            ->where('unavailable_id', $request->input('unavailable_id'))
            ->where('status', Unavailable::ACTIVE)
            ->fromSchoolId($user->school_id)
            ->exists();

        if ($unavailableExist) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('unavailable.already.exist'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $unavailable = Unavailable::create([
            'school_id' => $user->school_id,
            'from_date' => $request->input('from_date'),
            'to_date' => $request->input('to_date'),
            'status' => 1,
            'comment' => $request->input('comment'),
            'unavailable_id' => $request->input('unavailable_id'),
            'unavailable_type' => $request->input('unavailable_type'),
            'recurrence_type' => $request->input('recurrence_type'),
            'recurrence_days' => $request->input('recurrence_days'),
            'recurrence_week' => $request->input('recurrence_week'),
            'recurrence_end_at' => $request->input('recurrence_end_at'),
            'canceled_for_today_at' => null
        ]);

        if (now()->diffInMinutes($unavailable->from_date, false) <= 1) {
            event(new UnavailableUpdate($unavailable, Unavailable::ACTIVE));
            $unavailabilityObject =
                $unavailable->unavailable->activeUnavailability;
            logger('Unavailability object: ', [$unavailabilityObject]);
            event(
                new FavoriteUnavailableEvent($user->school_id, [
                    'type' => $unavailable->unavailable_type,
                    'id' => $unavailable->unavailable_id,
                    'unavailability' =>
                        $unavailabilityObject !== null
                            ? $unavailabilityObject->toArray()
                            : null
                ])
            );
        }

        return new UnavailableResource($unavailable->load('unavailable'));
    }

    /**
     * Update unavailable record
     *
     * @bodyParam unavailable_id integer required The id of the user/room
     * @bodyParam unavailable_type string required The type of the unavailable.
     * @bodyParam from_date date required From date unavailable enter in action.
     * @bodyParam to_date date required The date the unavailable ends.
     * @bodyParam recurrence_type string The type of the recurrence: (daily, weekly, monthly)
     * @bodyParam recurrence_days json What days from the week the recurrence should be run (as json with array key "days")
     * @bodyParam recurrence_week json Monthly week number for recurrence Example: {"2": "Monday"}
     * @bodyParam recurrence_end_at date The date on which recurrence won't be executed.
     * @bodyParam comment string Reason for the unavailable
     *
     *
     * @param UnavailableUpdateRequest $request
     * @param Unavailable $unavailable
     * @return UnavailableResource|JsonResponse
     * @throws AuthorizationException
     * @apiResourceModel App\Models\Unavailable
     * @apiResource App\Http\Resources\UnavailableResource
     * @authenticated
     */
    public function update(
        UnavailableUpdateRequest $request,
        Unavailable $unavailable
    ) {
        $this->authorize('update', $unavailable);

        $user = auth()->user();

        if (
            ($user->isTeacher() || $user->isStaff()) &&
            $request->input('unavailable_type') === User::class &&
            $user->id !== $request->input('unavailable_id')
        ) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('unavailable.undefined.user'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $unavailable->update([
            'school_id' => $user->school_id,
            'from_date' => $request->input('from_date'),
            'to_date' => $request->input('to_date'),
            'comment' => $request->input('comment'),
            'unavailable_id' => $request->input('unavailable_id'),
            'unavailable_type' => $request->input('unavailable_type'),
            'recurrence_type' => $request->input('recurrence_type'),
            'recurrence_days' => $request->input('recurrence_days'),
            'recurrence_week' => $request->input('recurrence_week'),
            'recurrence_end_at' => $request->input('recurrence_end_at'),
            'alerted_at' => null,
            'canceled_for_today_at' => null
        ]);

        if (Carbon::parse($request->input('from_date'))->isFuture()) {
            event(new UnavailableUpdate($unavailable, false));
        }

        return new UnavailableResource($unavailable->refresh());
    }

    /**
     * Update unavailable status
     *
     * @param Unavailable $unavailable
     * @return UnavailableResource
     * @throws AuthorizationException
     * @apiResourceModel App\Models\Unavailable
     * @apiResource App\Http\Resources\UnavailableResource
     * @authenticated
     */
    public function status(Unavailable $unavailable)
    {
        $this->authorize('update', $unavailable);

        // When the status is canceled through the dashboard,
        // we cancel only the OOO for the current day.
        if (request()->has('dashboard')) {
            if ($unavailable->recurrence_type === null) {
                $unavailable->update([
                    'status' => Unavailable::INACTIVE,
                    'canceled_for_today_at' => now()
                ]);
            } else {
                $unavailable->update([
                    'canceled_for_today_at' => now()
                ]);
            }
        } else {
            $unavailable->update(['status' => Unavailable::INACTIVE]); // end the unavailable
        }
        event(
            new FavoriteUnavailableEvent($unavailable->school_id, [
                'type' => $unavailable->unavailable_type,
                'id' => $unavailable->unavailable_id,
                'unavailability' => null
            ])
        );

        event(new UnavailableUpdate($unavailable, false));

        return new UnavailableResource($unavailable->refresh());
    }

    /**
     * Delete an unavailable record.
     *
     * @urlParam unavailable integer required The id of the unavailable.
     *
     * @authenticated
     * @param Unavailable $unavailable
     * @return UnavailableResource
     * @apiResourceModel App\Models\Unavailable
     * @apiResource App\Http\Resources\UnavailableResource
     * @throws AuthorizationException
     */
    public function delete(Unavailable $unavailable)
    {
        $this->authorize('delete', $unavailable);
        $unavailableForUserId =
            $unavailable->unavailable_type === User::class
                ? $unavailable->unavailable_id
                : null;

        event(new UnavailableUpdate(null, false, $unavailableForUserId));
        event(
            new FavoriteUnavailableEvent($unavailable->school_id, [
                'type' => $unavailable->unavailable_type,
                'id' => $unavailable->unavailable_id,
                'unavailability' => null
            ])
        );

        $unavailable->delete();

        return new UnavailableResource($unavailable);
    }
}
