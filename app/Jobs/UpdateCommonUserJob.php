<?php

namespace App\Jobs;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Models\School;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class UpdateCommonUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $user;
    protected $password;
    protected $authenticatedUser;
    protected $stickyRole;
    protected $preventArchive;
    protected $roleId;

    public function __construct(
        $userForUpdate,
        $authenticatedUser,
        $password = null,
        $stickyRole = null,
        $preventArchive = null,
        $roleId = null
    )
    {
        $this->user = $userForUpdate;
        $this->authenticatedUser = $authenticatedUser;
        $this->password = $password;
        $this->stickyRole = $stickyRole;
        $this->preventArchive = $preventArchive;
        $this->roleId = $roleId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new CommonUserService($this->authenticatedUser);

        $membership = DB::table('schools_users')
            ->where('user_id', $this->user->id)
            ->where('school_id', $this->user->school->id)
            ->orderBy('id', 'desc')
            ->first();

        if (!$membership) {
            return false;
        }

        $school = School::where('id', $membership->school_id)->first();

        $roleCollection = CommonUser::getRoleName(
            $this->roleId ?? $membership->role_id
        );

        $userData = [
            'uuid' => $this->user->cus_uuid,
            'email' => $this->user->email,
            'firstName' => $this->user->first_name,
            'lastName' => $this->user->last_name
            //            'status' => $this->user->status,// dont update
        ];
        if ($this->password) {
            $userData['password'] = $this->password; //TODO set default password
        }
        $gradeYear = $this->user->grade_year;
        if (!$gradeYear && $this->user->isStudent()) {
            $gradeYear = 0;
        }

        // check if membership status e 0
        $membershipData = [
            'status' => $membership->status,
            'school' => $school->cus_uuid,
            'authClientId' => 2,
            'user' => $userData,
            'role' => $roleCollection[0],
            'sisId' => $membership->student_sis_id,
            'graduationYear' => $gradeYear
        ];

        if ($this->stickyRole) {
            $membershipData['sisPreventFromChangingRole'] = $this->stickyRole;
        }

        if ($this->preventArchive !== null) {
            $membershipData['preventFromArchiving'] = $this->preventArchive;
        }

        $client->modifyMembershipUser(
            $membership->membership_uuid,
            $membershipData
        );
    }
}
