<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminPermissionsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();

        return (optional($user)->isAdmin() || optional($user)->isSuperAdmin()) ? $next($request) : response()->json([
            'message' => __('invalid.permission'),
            'status' => __('fail')
        ], 403);
    }
}
