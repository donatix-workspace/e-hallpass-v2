<?php

namespace App\Console\Commands\Migrations;

use App\Models\Room;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class MigrateAppointmentCommand extends Command
{
    protected $signature = 'migrate_sys:apt';

    protected $description = 'Migrate appointment passes';

    public function handle()
    {
        $oldDb = DB::connection('oldDB');
        $newDb = DB::connection('mysql');

        $oldRecApt = $oldDb
            ->table('appointment_pass')
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();
        $this->output->info('-- Migrating Apt --');
        $this->output->progressStart(count($oldRecApt));
        $oldRecAptChunks = array_chunk($oldRecApt, 1000);
        $now = now();

        $oldDb->disableQueryLog();
        $newDb->disableQueryLog();
        foreach ($oldRecAptChunks as $oldRecAptChunk) {
            $insertableApt = [];
            foreach ($oldRecAptChunk as $oldApt) {
                $oldSchool = $oldDb
                    ->table('school_schedule')
                    ->where('school_id', $oldApt->school_id)
                    ->first();
                $monthlyWeeks = null;
                $toDate = null;
                $fromDate = null;
                $recurrenceWeeks = null;
                $newToRoomId = null;
                $newToTeacherId = null;
                $newFromTeacherId = null;
                $newFromRoomId = null;

                if ($oldSchool) {
                    $newSchool = $newDb
                        ->table('schools')
                        ->where('old_id', $oldSchool->school_id)
                        ->first();

                    $newCreatedBy = $newDb
                        ->table('users')
                        ->where('old_id', $oldApt->created_by)
                        ->first();

                    $newCanceledBy = $newDb
                        ->table('users')
                        ->where('old_id', $oldApt->cancel_by)
                        ->first();

                    $newAcknowledgedBy = $newDb
                        ->table('users')
                        ->where('old_id', $oldApt->acknowledge_by)
                        ->first();

                    $newPeriod = $newDb
                        ->table('periods')
                        ->where('old_id', $oldApt->period_id)
                        ->where('school_id', $newSchool->id)
                        ->first();

                    if ($oldApt->from_teacherid > 0) {
                        $newFromTeacherId = $newDb
                            ->table('users')
                            ->where('old_id', $oldApt->from_teacherid)
                            ->first();
                    }

                    if ($oldApt->to_teacherid > 0) {
                        $newToTeacherId = $newDb
                            ->table('users')
                            ->where('old_id', $oldApt->to_teacherid)
                            ->first();
                    }

                    if ($oldApt->to_roomid > 0) {
                        $newToRoomId = $newDb
                            ->table('rooms')
                            ->where('old_id', $oldApt->to_roomid)
                            ->where('school_id', $newSchool->id)
                            ->first();
                    }

                    if ($oldApt->from_roomid > 0) {
                        $newFromRoomId = $newDb
                            ->table('rooms')
                            ->where('old_id', $oldApt->from_roomid)
                            ->where('school_id', $newSchool->id)
                            ->first();
                    }

                    $comment = json_decode($oldApt->pass_note);
                    $newStudentId = $newDb
                        ->table('users')
                        ->where('old_id', $oldApt->student_id)
                        ->first();

                    if ($newSchool && $newCreatedBy) {
                        $fromDate = Carbon::parse(
                            $oldApt->pass_date . ' ' . $oldApt->pass_time,
                            $newSchool->timezone
                        )->setTimezone(config('app.timezone'));
                    }

                    if (
                        ($newFromTeacherId !== null ||
                            $newFromRoomId !== null) &&
                        ($newToTeacherId !== null || $newToRoomId !== null) &&
                        $newCreatedBy &&
                        $newStudentId
                    ) {
                        $insertableApt[] = [
                            'created_by' => $newCreatedBy->id,
                            'from_type' =>
                                $oldApt->from_teacherid > 0
                                    ? User::class
                                    : Room::class,
                            'from_id' =>
                                $oldApt->from_teacherid > 0
                                    ? $newFromTeacherId->id
                                    : $newFromRoomId->id,
                            'to_type' =>
                                $oldApt->to_teacherid > 0
                                    ? User::class
                                    : Room::class,
                            'to_id' =>
                                $oldApt->to_teacherid > 0
                                    ? $newToTeacherId->id
                                    : $newToRoomId->id,
                            'user_id' => $newStudentId->id,
                            'school_id' => $newSchool->id,
                            'for_date' => $fromDate,
                            'period_id' => optional($newPeriod)->id,
                            'canceled_by' =>
                                $oldApt->cancel_by !== 0
                                    ? optional($newCanceledBy)->id
                                    : null,
                            'canceled_at' =>
                                $oldApt->cancel_by !== 0 ? now() : null,
                            'pass_id' =>
                                $oldApt->epass_id !== 0
                                    ? optional(
                                        $newDb
                                            ->table('passes')
                                            ->where('old_id', $oldApt->epass_id)
                                            ->first()
                                    )->id
                                    : null,
                            'reason' => $oldApt->pass_note,
                            'expired_at' => null,
                            'acknowledged_by_teacher_at' =>
                                optional($newAcknowledgedBy) !== null
                                    ? now()
                                    : null,
                            'confirmed_by_teacher_at' =>
                                optional($newAcknowledgedBy) !== null
                                    ? now()
                                    : null,
                            'acknowledged_by' =>
                                $oldApt->acknowledge_by !== null
                                    ? optional($newAcknowledgedBy)->id
                                    : null,
                            'confirmed_by' =>
                                $oldApt->acknowledge_by !== null
                                    ? optional($newAcknowledgedBy)->id
                                    : null,
                            'mail_send_at' => $fromDate->isPast()
                                ? now()
                                : null,
                            'created_at' => Carbon::parse(
                                $oldApt->created_at,
                                'America/New_York'
                            )
                                ->setTimezone('UTC')
                                ->toDateTimeString(),
                            'updated_at' => Carbon::parse(
                                $oldApt->updated_at,
                                'America/New_York'
                            )
                                ->setTimezone('UTC')
                                ->toDateTimeString()
                        ];
                    }
                }
            }

            $this->output->progressAdvance(count($insertableApt));
            DB::table('appointment_passes')->insert($insertableApt);
        }

        $this->output->progressFinish();
    }
}
