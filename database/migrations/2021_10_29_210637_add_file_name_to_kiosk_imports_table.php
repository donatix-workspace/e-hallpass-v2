<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFileNameToKioskImportsTable extends Migration
{
    public function up()
    {
        Schema::table('kiosk_imports', function (Blueprint $table) {
            $table->string('file_name');
        });
    }

    public function down()
    {
        Schema::table('kiosk_imports', function (Blueprint $table) {
            $table->dropColumn('file_name');
        });
    }
}
