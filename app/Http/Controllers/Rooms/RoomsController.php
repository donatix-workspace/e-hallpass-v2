<?php

namespace App\Http\Controllers\Rooms;

use App\Http\Controllers\Controller;
use App\Http\Filters\RoomFilter;
use App\Http\Requests\AddToFavoritesRequest;
use App\Http\Requests\RoomsIconRequest;
use App\Http\Requests\RoomUpdateRequest;
use App\Http\Requests\StoreRoomRequest;
use App\Http\Resources\RoomCollection;
use App\Http\Resources\RoomResource;
use App\Http\Resources\StudentFavoriteResource;
use App\Http\Resources\TeacherPassSettingResource;
use App\Models\AutoPass;
use App\Models\AutoPassPreference;
use App\Models\NurseRoom;
use App\Models\Room;
use App\Models\StaffSchedule;
use App\Models\StudentFavorite;
use App\Models\TeacherPassSetting;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @group Room management
 *
 * APIs for managing rooms
 */
class RoomsController extends Controller
{
    /**
     * Get all rooms for the user's school
     *
     * @queryParam per_page integer How many records will be shown on rooms
     * @queryParam sort string Sort rooms by name 'asc', 'desc'
     * @queryParam search_query string Search by keyword (only for the names of the rooms).
     *
     * This endpoint returns Rooms and Nurse Rooms
     * @return RoomCollection
     * @group Room management
     */
    public function index(RoomFilter $filters)
    {
        $schoolId = Auth::user()->school_id;
        $searchInputAvailable =
            request()->has('search_query') &&
            request()->get('search_query') !== null;
        $sort = explode(':', request()->get('sort'));

        // We leave this spaghetti code like that
        // because in FE we need many changes to remove search_query from the request
        if ($searchInputAvailable && count($sort) <= 1) {
            $rooms = Room::search(
                convertElasticQuery(request()->get('search_query'))
            )
                ->with(['nurses', 'roomType', 'kiosks'])
                ->where('school_id', $schoolId)
                ->paginate(Room::recordsPerPage());
        } elseif (
            ($searchInputAvailable && count($sort) > 1) ||
            (!$searchInputAvailable && count($sort) > 1)
        ) {
            $rooms = Room::search(
                convertElasticQuery(
                    request()->get('search_query') !== null
                        ? request()->get('search_query')
                        : config('scout_elastic.all_records')
                )
            )
                ->with(['nurses', 'roomType', 'kiosks'])
                ->when(request()->has('sort'), function ($query) use ($sort) {
                    if (count($sort) <= 1) {
                        return $query
                            ->orderBy('admin_favorite', 'desc')
                            ->orderBy('name', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                })
                ->where('school_id', $schoolId)
                ->paginate(Room::recordsPerPage());
        } else {
            $rooms = Room::where('school_id', $schoolId)
                ->with(['nurses', 'roomType', 'kiosks', 'studentFavorites'])
                ->orderBy('status', 'desc')
                ->orderBy('admin_favorite', 'desc')
                ->orderBy('name', 'asc')
                ->where('school_id', $schoolId)
                ->filter($filters)
                ->paginate(Room::recordsPerPage());
        }

        $rooms->append(['extended_pass_time']);
        return new RoomCollection($rooms);
    }

    /**
     * Getting a form data for the rooms.
     *
     * @authenticated
     * @return TeacherPassSettingResource
     */
    public function create()
    {
        $teacherPassSetting = TeacherPassSetting::where(
            'school_id',
            Auth::user()->school_id
        )->first();

        return new TeacherPassSettingResource($teacherPassSetting);
    }

    /**
     * Create a room
     *
     * @bodyParam name string required The name of the Room.
     * @bodyParam trip_type integer required Trip type (1-3).
     * @bodyParam status boolean required Room Status.
     * @bodyParam is_extended boolean Is extended
     * @bodyParam extended_pass_time time If the room has extended Pass time
     * @bodyParam comment_type integer Comment type Example: Hidden, Mandatory, Optional (1-3)
     *
     * @param StoreRoomRequest $request
     * @return RoomResource|\Illuminate\Http\JsonResponse
     * @authenticated
     * @group Room management
     */
    public function store(StoreRoomRequest $request)
    {
        $schoolId = Auth::user()->role_id > 1 ? Auth::user()->school_id : 0;
        $validatedData = $request->validated();
        $roomAttributes = array_merge($validatedData, [
            'school_id' => $schoolId
        ]);

        $roomExists = Room::fromSchoolId(auth()->user()->school_id)
            ->where('name', $request->input('name'))
            ->exists();

        if ($roomExists) {
            return response()->json([
                'data' => ['name' => $request->input('name')],
                'message' => __('room.with.that.name.exists'),
                'status' => __('fail')
            ]);
        }

        $room = Room::create($roomAttributes);

        if (
            isset($request->extended_pass_time) &&
            isset($request->is_extended) &&
            $request->is_extended &&
            $request->extended_pass_time !== null
        ) {
            NurseRoom::create([
                'room_id' => $room->id,
                'school_id' => $schoolId,
                'end_time' => $request->extended_pass_time
            ]);
        }

        return new RoomResource($room);
    }

    /**
     * Get specific room
     *
     * This endpoint returns data for specific room
     * @param Room $room
     * @return RoomResource
     * @authenticated
     * @group Room management
     * @throws AuthorizationException
     */
    public function show(Room $room): RoomResource
    {
        $this->authorize('view', $room);

        return new RoomResource($room);
    }

    /**
     * Update a room
     *
     * @urlParam room integer required The id of the room.
     * @bodyParam name string required The name of the Room.
     * @bodyParam trip_type integer required Trip type (1-3).
     * @bodyParam status boolean required Room Status.
     * @bodyParam is_extended boolean Is extended
     * @bodyParam extended_pass_time time If the room has extended Pass time
     * @bodyParam comment_type integer Comment type Example: Hidden, Mandatory, Optional (1-3)
     * @bodyParam icon string Icon: flaticon-nurse, flaticon-book, etc
     * @param Room $room
     * @param RoomUpdateRequest $request
     * @return RoomResource|\Illuminate\Http\JsonResponse
     * @authenticated
     * @group Room management
     * @throws AuthorizationException
     */
    public function update(Room $room, RoomUpdateRequest $request)
    {
        $this->authorize('update', $room);

        $schoolId = Auth::user()->role_id > 1 ? Auth::user()->school_id : 0;
        $teacherPassSetting = TeacherPassSetting::where(
            'school_id',
            $schoolId
        )->first();
        if (!$teacherPassSetting) {
            $teacherPassSetting = TeacherPassSetting::where(
                'school_id',
                0
            )->first();
        }

        if ($room->name !== $request->input('name')) {
            $roomExists = Room::fromSchoolId(auth()->user()->school_id)
                ->where('name', $request->input('name'))
                ->exists();

            if ($roomExists) {
                return response()->json([
                    'data' => ['name' => $request->input('name')],
                    'message' => __('room.with.that.name.exists'),
                    'status' => __('fail')
                ]);
            }
        }

        if (
            isset($request->extended_pass_time) &&
            isset($request->is_extended) &&
            $request->input('is_extended')
        ) {
            $room->nurses()->delete();
            $room->nurses()->create([
                'school_id' => $schoolId,
                'end_time' => $request->extended_pass_time
            ]);
        }

        if (!$request->input('is_extended')) {
            $room->nurses()->delete();
        }

        if ($request->input('status') !== Room::ACTIVE) {
            $autoPass = AutoPass::where('room_id', $room->id)
                ->where('status', AutoPass::ACTIVE)
                ->where('school_id', $room->school_id)
                ->first();

            if ($autoPass !== null) {
                AutoPassPreference::where(
                    'auto_pass_id',
                    $autoPass->id
                )->delete();

                AutoPass::where('id', $autoPass->id)->update([
                    'status' => AutoPass::INACTIVE
                ]);
            }

            $room->removeFromFavorites();
        } else {
            $autoPass = AutoPass::where('room_id', $room->id)
                ->where('school_id', $room->school_id)
                ->update(['status' => AutoPass::ACTIVE]);
        }

        $validatedData = $request->validated();
        $roomAttributes = array_merge($validatedData, [
            'school_id' => $schoolId
        ]);
        if ($room->isInactive()) {
            $roomAttributes = array_merge($roomAttributes, [
                'favorites' => 0,
                'icon' => null,
                'order' => 0
            ]);
            StaffSchedule::where([
                ['room_id', '=', $room->id],
                ['school_id', '=', $schoolId]
            ])->delete();
        }
        $room->update($roomAttributes);

        return new RoomResource($room);
    }

    /**
     * Update room icon
     *
     * @urlParam room integer required The id of the room.
     * @bodyParam icon string required The name of the Room.
     *
     * @param RoomsIconRequest $request
     * @param Room $room
     * @return RoomResource
     * @throws AuthorizationException
     * @authenticated
     * @group Room management
     */
    public function icon(RoomsIconRequest $request, Room $room): RoomResource
    {
        $this->authorize('update', $room);

        $room->update(['icon' => $request->input('icon')]);

        return new RoomResource($room);
    }

    /**
     * Make room favorite for all users
     *
     * This endpoint will toggle favorite state for the room.
     *
     * @bodyParam favorable_id Room id
     *
     * @param AddToFavoritesRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     * @group Room management
     * @throws AuthorizationException
     */
    public function toggleFavorites(Request $request)
    {
        $this->authorize('favorite', Room::class);

        $validatedData = $request->validate([
            'favorable_id' => 'integer'
        ]);

        $user = auth()->user();

        $room = Room::where('id', $validatedData['favorable_id'])
            ->where('school_id', $user->school_id)
            ->first();

        $favoriteRoom = StudentFavorite::where('school_id', $user->school_id)
            ->where('favorable_id', $validatedData['favorable_id'])
            ->where('favorable_type', Room::class)
            ->where('global', 1)
            ->whereNull('user_id')
            ->first();

        if ($favoriteRoom) {
            $favoriteRoom->delete();
            $room->toggleAdminFavorite()->searchable();
        } else {
            if (!$room->isInactive()) {
                StudentFavorite::create([
                    'school_id' => $user->school_id,
                    'global' => 1,
                    'favorable_type' => Room::class,
                    'favorable_id' => $validatedData['favorable_id'],
                    'section' => 'destination'
                ]);
                $room->toggleAdminFavorite()->searchable();
            }
        }

        return response()->json([
            'data' => [],
            'message' => __('favorites.toggled.successfully'),
            'status' => __('success')
        ]);
    }

    public function toggleAPTRequests(Room $room)
    {
        $room->update([
            'enable_appointment_passes' =>
                $room->enable_appointment_passes === 1 ? 0 : 1
        ]);
        return response()->json([
            'data' => [],
            'message' => __('rooms.apt_requests.toggled'),
            'status' => __('success')
        ]);
    }

    /**
     * Delete a global favorite room.
     *
     * @urlParam studentFavorite integer required The id of the favorite.
     *
     * @authenticated
     * @param StudentFavorite $studentFavorite
     * @return StudentFavoriteResource
     * @throws AuthorizationException
     */
    public function removeFavorites(StudentFavorite $studentFavorite)
    {
        $this->authorize('deleteAdmin', $studentFavorite);

        $studentFavorite->delete();

        return new StudentFavoriteResource($studentFavorite);
    }

    /**
     * Delete a room
     *
     * @urlParam room integer required The id of the room.
     *
     * @param Room $room
     * @return RoomResource
     * @throws AuthorizationException
     * @throws \Exception
     * @authenticated
     * @group Room management
     */
    public function delete(Room $room)
    {
        $this->authorize('delete', $room);

        $room->delete();

        return new RoomResource($room);
    }

    /**
     * Search for rooms
     *
     * @queryParam query string required Search query
     *
     * @param Request $request
     * @return RoomCollection
     * @authenticated
     * @group Room management
     */
    public function search(Request $request)
    {
        $query = $request->get('query');
        $rooms = Room::where([
            ['name', 'LIKE', "%{$query}%"],
            ['school_id', '=', auth()->user()->school_id]
        ])
            ->orderBy('name', 'asc')
            ->get();

        return new RoomCollection($rooms);
    }
}
