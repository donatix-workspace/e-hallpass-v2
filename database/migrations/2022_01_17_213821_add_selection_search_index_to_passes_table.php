<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSelectionSearchIndexToPassesTable extends Migration
{
    public function up()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->index(
                [
                    'school_id',
                    'created_at',
                    'approved_at',
                    'completed_at',
                    'pass_status',
                    'canceled_at',
                    'expired_at',
                    'type',
                    'parent_id',
                    'system_completed',
                    'extended_at'
                ],
                'passes_filters_pass_history_index'
            );
        });
    }

    public function down()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->dropIndex('passes_filters_pass_history_index');
        });
    }
}
