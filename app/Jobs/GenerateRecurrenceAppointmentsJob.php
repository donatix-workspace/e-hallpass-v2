<?php

namespace App\Jobs;

use App\Models\AppointmentPass;
use App\Models\RecurrenceAppointmentPass;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Throwable;

class GenerateRecurrenceAppointmentsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $recurrenceAppointmentPass;
    public $appointmentPass;

    public function __construct(RecurrenceAppointmentPass $recurrenceAppointmentPass, AppointmentPass $appointmentPass)
    {
        $this->recurrenceAppointmentPass = $recurrenceAppointmentPass;
        $this->appointmentPass = $appointmentPass;
    }

    public function handle()
    {
        $appointmentPass = $this->appointmentPass;
        $recurrenceAppointmentPass = $this->recurrenceAppointmentPass;
        $appointmentPassesForTheDay = [];
        $appointmentPassesForTheWeek = [];
        $appointmentPassesForTheMonth = [];

        if ($recurrenceAppointmentPass->recurrence_type) {
            if ($recurrenceAppointmentPass->recurrence_type === config('recurrences.daily')) {
                $days = CarbonPeriod::create(
                    $appointmentPass->for_date->addWeek()->startOfWeek(),
                    Carbon::parse($appointmentPass->for_date)->addWeek()->endOfWeek()
                );

                foreach ($days as $day) {
                    $forDateTimeFormat = Carbon::parse($recurrenceAppointmentPass->for_date)
                        ->setTimezone(config('app.timezone'))
                        ->format('H:i');
                    if (!in_array($day->dayOfWeek, [Carbon::SATURDAY, Carbon::SUNDAY])) {
                        $appointmentPassesForTheDay[] = [
                            'created_by' => $recurrenceAppointmentPass->created_by,
                            'from_id' => $recurrenceAppointmentPass->from_id,
                            'from_type' => $recurrenceAppointmentPass->from_type,
                            'to_id' => $recurrenceAppointmentPass->to_id,
                            'to_type' => $recurrenceAppointmentPass->to_type,
                            'user_id' => $recurrenceAppointmentPass->user_id,
                            'recurrence_appointment_pass_id' => $recurrenceAppointmentPass->id,
                            'for_date' => Carbon::parse($day->toDateString() . " $forDateTimeFormat"),
                            'confirmed_by_teacher_at' => now(),
                            'school_id' => $recurrenceAppointmentPass->school_id,
                            'period_id' => $recurrenceAppointmentPass->period_id,
                            'pass_id' => null,
                            'reason' => $recurrenceAppointmentPass->reason,
                            'created_at' => Carbon::now()->setTimezone(config('app.timezone')),
                            'updated_at' => Carbon::now()->setTimezone(config('app.timezone')),
                        ];
                    }
                }
            }

            if ($recurrenceAppointmentPass->recurrence_type === config('recurrences.weekly')) {
                $days = CarbonPeriod::create(
                    $appointmentPass->for_date->addWeek()->startOfWeek(),
                    Carbon::parse($appointmentPass->for_date)->addWeek()->endOfWeek()
                );

                $recurrenceDays = collect(json_decode($recurrenceAppointmentPass->recurrence_days))->get('days');
                $endRecurrenceDate = Carbon::parse($recurrenceAppointmentPass->recurrence_end_at);
                $forDateTimeFormat = Carbon::parse($recurrenceAppointmentPass->for_date)->format('H:i');

                if (!$endRecurrenceDate->isPast()) {
                    foreach ($days as $day) {
                        if (in_array($day->englishDayOfWeek, $recurrenceDays)) {
                            $appointmentPassesForTheWeek[] = [
                                'created_by' => $recurrenceAppointmentPass->created_by,
                                'from_id' => $recurrenceAppointmentPass->from_id,
                                'from_type' => $recurrenceAppointmentPass->from_type,
                                'to_id' => $recurrenceAppointmentPass->to_id,
                                'to_type' => $recurrenceAppointmentPass->to_type,
                                'user_id' => $recurrenceAppointmentPass->user_id,
                                'recurrence_appointment_pass_id' => $recurrenceAppointmentPass->id,
                                'for_date' => Carbon::parse($day->toDateString() . " $forDateTimeFormat")->setTimezone(config('app.timezone')),
                                'confirmed_by_teacher_at' => now(),
                                'school_id' => $recurrenceAppointmentPass->school_id,
                                'period_id' => $recurrenceAppointmentPass->period_id,
                                'pass_id' => null,
                                'reason' => $recurrenceAppointmentPass->reason,
                                'created_at' => Carbon::now()->setTimezone(config('app.timezone')),
                                'updated_at' => Carbon::now()->setTimezone(config('app.timezone')),
                            ];
                        }
                    }
                }
            }

            if ($recurrenceAppointmentPass->recurrence_type === config('recurrences.monthly')) {
                $days = CarbonPeriod::create(
                    $appointmentPass->for_date->addMonth()->startOfMonth(),
                    Carbon::parse($appointmentPass->for_date)->addMonth()->endOfMonth()
                );
                $recurrenceWeek = json_decode($recurrenceAppointmentPass->recurrence_week, true);
                $forDateTimeFormat = Carbon::parse($recurrenceAppointmentPass->for_date)->format('H:i');

                foreach ($days as $day) {
                    if (array_key_exists($day->weekNumberInMonth, $recurrenceWeek) && $recurrenceWeek[$day->weekNumberInMonth] === $day->englishDayOfWeek) {
                        $appointmentPassesForTheMonth[] = [
                            'created_by' => $recurrenceAppointmentPass->created_by,
                            'from_id' => $recurrenceAppointmentPass->from_id,
                            'from_type' => $recurrenceAppointmentPass->from_type,
                            'to_id' => $recurrenceAppointmentPass->to_id,
                            'to_type' => $recurrenceAppointmentPass->to_type,
                            'user_id' => $recurrenceAppointmentPass->user_id,
                            'recurrence_appointment_pass_id' => $recurrenceAppointmentPass->id,
                            'for_date' => Carbon::parse($day->toDateString() . " $forDateTimeFormat")->setTimezone(config('app.timezone')),
                            'school_id' => $recurrenceAppointmentPass->school_id,
                            'period_id' => $recurrenceAppointmentPass->period_id,
                            'pass_id' => null,
                            'confirmed_by_teacher_at' => now(),
                            'reason' => $recurrenceAppointmentPass->reason,
                            'created_at' => Carbon::now()->setTimezone(config('app.timezone')),
                            'updated_at' => Carbon::now()->setTimezone(config('app.timezone')),
                        ];
                    }
                }
            }
            try {
                DB::beginTransaction();

                if (count($appointmentPassesForTheMonth) > 0) {
                    foreach ($appointmentPassesForTheMonth as $appointmentPass) {
                        $appointment = AppointmentPass::create($appointmentPass);

                        $appointment->comments()->create([
                            'comment' => $appointment->reason,
                            'user_id' => $appointment->created_by,
                            'school_id' => $appointment->school_id
                        ]);
                    }
                }

                if (count($appointmentPassesForTheWeek) > 0) {
                    foreach ($appointmentPassesForTheWeek as $appointmentPass) {
                        $appointment = AppointmentPass::create($appointmentPass);

                        $appointment->comments()->create([
                            'comment' => $appointment->reason,
                            'user_id' => $appointment->created_by,
                            'school_id' => $appointment->school_id
                        ]);
                    }
                }

                if (count($appointmentPassesForTheDay) > 0) {
                    foreach ($appointmentPassesForTheDay as $appointmentPass) {
                        $appointment = AppointmentPass::create($appointmentPass);

                        $appointment->comments()->create([
                            'comment' => $appointment->reason,
                            'user_id' => $appointment->created_by,
                            'school_id' => $appointment->school_id
                        ]);
                    }
                }

                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();

                Log::error($e->getMessage());
            }
        }

    }
}
