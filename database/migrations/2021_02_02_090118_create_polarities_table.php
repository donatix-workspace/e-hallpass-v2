<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePolaritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polarities', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('first_user_id')->unsigned();
            $table->bigInteger('second_user_id')->unsigned();
            $table->boolean('status')->default(0);
            $table->unsignedBigInteger('school_id');
            $table->text('message')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polarities');
    }
}
