<?php

namespace App\Console\Commands\Migrations;

use Illuminate\Console\Command;

class MigrateAutoPassCommand extends Command
{
    protected $signature = 'migrate_sys:auto_pass';

    protected $description = 'Migrate auto passes in EHPV1 to EHPV2';

    public function handle()
    {
        $this->info('--- Migrating Auto Passes ---');

        $newDb = \DB::connection('mysql');
        $oldDb = \DB::connection('oldDB');

        $oldDb->disableQueryLog();
        $newDb->disableQueryLog();

        $autoPasses = $oldDb->table('auto_pass')
            ->orderBy('created_at', 'asc')
            ->get()
            ->toArray();

        $this->output->progressStart(count($autoPasses));

        $autoPassesChunks = array_chunk($autoPasses, 1500);

        foreach ($autoPassesChunks as $autoPassChunk) {
            $insertAutoPassesChunk = [];
            foreach ($autoPassChunk as $autoPass) {
                $oldSchool = $oldDb->table('school_schedule')->where('school_id', $autoPass->school_id)
                    ->first();

                if ($oldSchool) {
                    $newSchool = $newDb->table('schools')->select('id')->where('old_id', $oldSchool->school_id)->first();

                    if ($newSchool) {
                        $oldRoom = $oldDb->table('rooms')->select('room_id')->where('school_id', $oldSchool->school_id)
                            ->where('room_id', $autoPass->room_id)
                            ->first();

                        $newRoom = $newDb->table('rooms')->where('old_id', optional($oldRoom)->room_id)
                            ->select('id')
                            ->where('school_id', $newSchool->id)
                            ->first();

                        if ($newRoom) {
                            $insertAutoPassesChunk[] = [
                                'room_id' => $newRoom->id,
                                'school_id' => $newSchool->id,
                                'created_at' => now(),
                                'updated_at' => now()
                            ];
                        }
                    }
                }
            }

            try {
                $newDb->table('auto_passes')
                    ->insert($insertAutoPassesChunk);
                $this->output->progressAdvance(count($insertAutoPassesChunk));
            } catch (\Exception $e) {
                $this->info($e->getMessage());
            }
        }

        $this->output->progressFinish();
    }
}
