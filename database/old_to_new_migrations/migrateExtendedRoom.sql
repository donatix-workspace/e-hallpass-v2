SET FOREIGN_KEY_CHECKS = 0;
START TRANSACTION ;
    INSERT INTO ehp_v2_prod.nurse_rooms (room_id, school_id, end_time, created_at, updated_at)
    SELECT
           (SELECT id FROM ehp_v2_prod.rooms WHERE old_id=oldNr.room_id limit 1),
           (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldNr.school_id limit 1),
           (SELECT oldNr.end_time),
           (SELECT NOW()),
           (SELECT NOW())
    FROM ehallpassDB.nurse_rooms as oldNr;
COMMIT ;
SET FOREIGN_KEY_CHECKS = 1;
