<?php

namespace App\Models;

class AuthType
{
    public const INHOUSESSO = 1;
    public const HIDDEN = 1001;
    public const GSUITE = 10;
    public const MICROSOFT = 20;
    public const CLASSLINK = 101;
    public const CLEVER = 102;
    public const GG4L = 103;

    protected static $names = [
        'manual' => self::INHOUSESSO,
        'google' => self::GSUITE,
        'office365' => self::MICROSOFT,
    ];

    public static function nameToCode($name)
    {
        return self::$names[$name] ?? null;
    }
}
