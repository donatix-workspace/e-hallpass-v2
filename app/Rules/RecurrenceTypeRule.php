<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RecurrenceTypeRule implements Rule
{
    public $recurrenceTypes;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->recurrenceTypes = collect([
            "null",
            config('recurrences.daily'),
            config('recurrences.weekly'),
            config('recurrences.monthly')
        ]);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->recurrenceTypes->contains($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('recurrence.invalid.type');
    }
}
