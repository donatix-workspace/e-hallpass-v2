<?php

namespace App\Rules;

use App\Models\Room;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class RoomOrUserExistsAppointmentRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value === config('roles.system')) {
            return true;
        }

        $user = User::where('id', $value)
            ->belongsToThisSchool()
            ->first();

        $userIsAdminOrTeacher = !optional($user)->isStudent();
        $room = Room::where('id', $value)
            ->where('school_id', auth()->user()->school_id)
            ->first();

        return ($user !== null && $userIsAdminOrTeacher) ||
            $room !== null ||
            $value === config('roles.system');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('passes.user.or.room.not.exists');
    }
}
