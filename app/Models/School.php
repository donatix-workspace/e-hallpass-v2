<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class School extends Model implements PushNotifications
{
    use HasFactory;

    public const PERMISSION_ACTIVE = 1;
    public const PERMISSION_INACTIVE = 0;
    public const SCHOOL_IMAGE_DIRECTORY = 'schoolimage/';

    protected $appends = [
        'active_modules',
        'hide_student_kiosk_password_field',
        'active_domains',
        'show_photos',
        'active_school'
    ];

    protected $hidden = [
        'avatar_api',
        'total_user_role',
        'is_sftp_image',
        'csv_roles',
        'csv_headers',
        'archive_prefix',
        'upload_folder',
        'total_users',
        'if_username',
        'is_user_csv',
        'is_manual',
        'timezone_offset',
        'stop_status',
        'webservice',
        'api_key'
    ];

    protected $casts = [
        'auth_providers' => 'array'
    ];

    protected $with = ['modules'];

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function schoolUser(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class, 'schools_users')->withPivot(
            'school_id'
        );
    }

    /**
     * @param int|null $schoolId
     * @return mixed
     */
    public static function findCacheFirst(?int $schoolId)
    {
        if ($schoolId === null) {
            return null;
        }

        return Cache::remember(
            'school_' . $schoolId,
            now()->addMinutes(5),
            function () use ($schoolId) {
                return self::whereId($schoolId)
                    ->setEagerLoads([])
                    ->select(['timezone', 'id', 'timezone_offset'])
                    ->first()
                    ->setAppends([]);
            }
        );
    }

    /**
     * @return bool
     */
    public function getHideStudentKioskPasswordFieldAttribute(): bool
    {
        $moduleKiosk = $this->modules()
            ->wherePivot('status', 1)
            ->where('name', Module::KIOSK)
            ->first();
        if ($moduleKiosk) {
            $kioskPasswordFieldFlag = json_decode(
                optional($moduleKiosk)->pivot->option_json
            );
            if ($kioskPasswordFieldFlag !== null) {
                return (bool) optional($kioskPasswordFieldFlag)->spassword;
            }
        }
        return false;
    }

    /**
     * @param $moduleName
     * @return bool
     */
    public function hasModule($moduleName)
    {
        return $this->modules->pluck('name')->contains($moduleName);
    }

    public function getActiveModulesAttribute(): \Illuminate\Support\Collection
    {
        return $this->modules()
            ->wherePivot('status', 1)
            ->pluck('name');
    }

    public function getActiveDomainsAttribute(): \Illuminate\Support\Collection
    {
        return $this->domains()->pluck('name');
    }

    public function getShowPhotosAttribute()
    {
        return $this->show_student_photos && $this->hasStudentsWithPhotos();
    }

    public function hasStudentsWithPhotos()
    {
        return $this->students()
            ->whereNotNull('avatar')
            ->exists();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function passes()
    {
        return $this->hasMany(Pass::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roomTypes()
    {
        return $this->hasMany(RoomType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function autoPassesLimits()
    {
        return $this->hasMany(AutoPassLimit::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function autoPasses()
    {
        return $this->hasMany(AutoPass::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blockPasses()
    {
        return $this->hasMany(BlockPass::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locationCapacities()
    {
        return $this->hasMany(LocationCapacity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function passLimits()
    {
        return $this->morphMany(PassLimit::class, 'limitable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restrictions()
    {
        return $this->hasMany(RoomRestriction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pinAttempts()
    {
        return $this->hasMany(PinAttempt::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pins()
    {
        return $this->hasMany(Pin::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function staffSchedules()
    {
        return $this->hasMany(StaffSchedule::class);
    }

    /**
     * @param $date
     * @param string|null $timezone
     * @param bool $onlyDate
     * @param bool $onlyTime
     * @param string|null $format
     * @return string|null
     */
    public static function convertTimeToCorrectTimezone(
        $date,
        ?string $timezone,
        bool $onlyDate = false,
        bool $onlyTime = false,
        ?string $format = null
    ): ?string {
        if ($timezone === null) {
            return $date;
        }

        $onlyDateFormat = Carbon::parse($date, config('app.timezone'))->format(
            $format === null ? 'Y-m-d' : $format
        );
        $dateFormatted = !$onlyDate
            ? Carbon::parse($date, config('app.timezone'))
                ->setTimezone($timezone)
                ->format($format === null ? 'Y-m-d H:i:s' : $format)
            : $onlyDateFormat;

        if ($onlyTime) {
            $dateFormatted = Carbon::parse($date, config('app.timezone'))
                ->setTimezone($timezone)
                ->format('g:i A');
        }

        return $date !== null ? $dateFormatted : null;
    }

    /**
     * @param $date
     * @param string $timezone
     * @return string|null
     */
    public static function convertSchoolDatesToUTC($date, string $timezone)
    {
        return $date !== null
            ? Carbon::parse($date, $timezone)
                ->setTimezone(config('app.timezone'))
                ->format('Y-m-d H:i:s')
            : null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function passBlocks()
    {
        return $this->hasMany(PassBlock::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activePassBlock(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(PassBlock::class)
            ->where('students', 1)
            ->where('from_date', '<=', now())
            ->where('to_date', '>=', now());
    }

    public function domains()
    {
        return $this->hasMany(Domain::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function students(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(User::class)->where(
            'role_id',
            Role::getRoleIdByName(User::Student)
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointmentPasses()
    {
        return $this->hasMany(AppointmentPass::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transparencies()
    {
        return $this->hasMany(Transparency::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transparencyUsers()
    {
        return $this->hasMany(TransparencyUser::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function modules()
    {
        return $this->belongsToMany(Module::class, 'school_modules')
            //            ->wherePivot('status', Module::ACTIVE)
            ->withPivot('permission', 'option_json', 'status');
    }

    /**
     * @param $moduleName
     * @return mixed
     */
    public function getModuleOptions($moduleName)
    {
        $pivotModuleOptions = optional(
            $this->modules()
                ->where('name', $moduleName)
                ->wherePivot('status', Module::ACTIVE)
                ->first()
        )->pivot;

        return optional($pivotModuleOptions)->option_json !== null
            ? $pivotModuleOptions->option_json
            : null;
    }

    /**
     * @return mixed|null
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function globalPassLimits()
    {
        return $this->hasMany(ActivePassLimit::class);
    }

    /**
     * @param $value
     * @return string|null
     */
    public function getLogoAttribute($value): ?string
    {
        if (!$value) {
            return null;
        }

        if (
            !Storage::disk('s3')->exists(self::SCHOOL_IMAGE_DIRECTORY . $value)
        ) {
            return null;
        }

        return Storage::disk('s3')->url(self::SCHOOL_IMAGE_DIRECTORY . $value);
    }

    /**
     * @return bool
     */
    public function getActiveSchoolAttribute(): bool
    {
        return optional(auth()->user())->school_id == $this->id;
    }

    public function getTopics()
    {
        $env = config('services.fcm.env');
        //dev.
        //local.
        //staging.
        //production.
        return "{$env}.schools.{$this->id}";
    }

    public function getNotificationTitle($event)
    {
        return __($event . 'Title');
    }

    public function getNotificationBody($event)
    {
        return __($event . 'Body');
    }

    public function getData($event, $options)
    {
        return [
            'payload' => [
                'id' => $this->id
            ],
            'options' => $options
        ];
    }

    public function getNotificationSettings($event, $options)
    {
        return [
            'topics' => $this->getTopics(),
            'title' => $this->getNotificationTitle($event),
            'body' => $this->getNotificationBody($event),
            'data' => $this->getData($event, $options)
        ];
    }
}
