<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRollbackColumnsToUsersTable extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('clever_id', 100)->nullable()->index('clever_id');
            $table->string('clever_building_id', 100)->nullable()->comment('Clever School ID');
            $table->text('metatag')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('clever_id');
            $table->dropColumn('clever_building_id');
            $table->dropColumn('metatag');
        });
    }
}
