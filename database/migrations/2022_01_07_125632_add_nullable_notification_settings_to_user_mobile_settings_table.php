<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullableNotificationSettingsToUserMobileSettingsTable extends Migration
{
    public function up()
    {
        Schema::table('user_mobile_settings', function (Blueprint $table) {
            $table
                ->json('notification_settings')
                ->nullable()
                ->change();
        });
    }

    public function down()
    {
        Schema::table('user_mobile_settings', function (Blueprint $table) {
            $table->json('notification_settings')->change();
        });
    }
}
