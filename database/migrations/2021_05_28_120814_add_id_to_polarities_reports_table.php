<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdToPolaritiesReportsTable extends Migration
{
    public function up()
    {
        Schema::table('polarities_reports', function (Blueprint $table) {
            $table->id()->first();
        });
    }

    public function down()
    {

    }
}
