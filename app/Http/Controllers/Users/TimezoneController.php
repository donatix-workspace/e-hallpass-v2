<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * @group Timezone Information.
 */
class TimezoneController extends Controller
{
    /**
     * Get the user timezone
     *
     * @authenticated
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *       "data": {
     * "timezone": "UTC"
     *       },
     *      "status": "success"
     * }
     */
    public function index()
    {
        return response()->json([
            'data' => Carbon::now(auth()->user()->school->timezone)->timezone,
            'status' => __('success')
        ]);
    }
}
