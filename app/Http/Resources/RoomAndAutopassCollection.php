<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RoomAndAutopassCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                "rooms" => $this->collection->get('rooms'),
                "auto_pass_rooms" => $this->collection->get('auto_pass_rooms'),
                "auto_pass_preference_to_user" => $this->collection->get('auto_pass_preference_to_user')
            ],
            'status' => 'success',
        ];
    }
}
