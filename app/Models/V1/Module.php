<?php

namespace App\Models\V1;


use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $connection = 'oldDB';
    protected $table = 'modules';
    protected $primaryKey = 'id';

}
