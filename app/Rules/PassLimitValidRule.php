<?php

namespace App\Rules;

use App\Models\PassLimit;
use Illuminate\Contracts\Validation\Rule;

class PassLimitValidRule implements Rule
{
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $acceptedForTypes = [PassLimit::CSV, PassLimit::STUDENT, PassLimit::SCHOOL, PassLimit::GRADEYEAR];

        return in_array($value, $acceptedForTypes);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('passes.limit.invalid.for.type');
    }
}
