<?php

namespace Database\Factories;

use App\Models\School;
use App\Models\TeacherPassSetting;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class TeacherPassSettingFactory extends Factory
{
    protected $model = TeacherPassSetting::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'min_time' => '00:10:00',
            'white_passes' => '00:05:00',
            'awaiting_apt' => '00:15:00',
            'max_time' => '01:30:00',
            'auto_expire_time' => '00:30:00',
            'nurse_expire_time' => '00:20:00',
            'mail_time' => '00:00:00',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'school_id' => function () {
                return School::factory()->create()->id;
            },
        ];
    }
}
