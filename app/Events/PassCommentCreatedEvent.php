<?php

namespace App\Events;

use App\Models\Comment;
use App\Models\Pass;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PassCommentCreatedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $comment;
    public $pass;
    public $type;
    public $notifyAdultsIds;
    public $notifyStudent;

    /**
     * @param $pass
     * @param $comment
     * @param string $type
     * @param bool|null $fromAdult
     * @param Comment|null $commentObject
     */
    public function __construct(
        $pass,
        $comment,
        string $type = Pass::class,
        ?bool $fromAdult = false,
        ?Comment $commentObject = null
    ) {
        $this->comment = $comment;
        $this->pass = $pass;
        $this->type = $type;

        $this->notifyStudent = $fromAdult;

        if ($pass instanceof Pass) {
            $this->notifyAdultsIds = $pass->getNotifiableAdults($commentObject);
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel(
                $this->type === Pass::class
                    ? 'passes.' . $this->pass->school_id
                    : 'appointments.passes.' . $this->pass->school_id
            ),
            new PrivateChannel(
                $this->type === Pass::class
                    ? 'user.passes.' . $this->pass->user_id
                    : 'user.appointments.passes.' . $this->pass->user_id
            ),
            new PrivateChannel('building-comments.' . $this->pass->school_id)
        ];
    }
}
