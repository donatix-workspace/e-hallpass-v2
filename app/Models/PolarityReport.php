<?php

namespace App\Models;

use App\Elastic\PolarityReportsIndexConfigurator;
use App\Traits\ExtendedSearchable;
use App\Traits\PolarityReportMapping;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use ScoutElastic\Searchable;

class PolarityReport extends Model
{
    use HasFactory, ExtendedSearchable, PolarityReportMapping;

    protected $table = 'polarities_reports';
    protected $guarded = [];
    protected $indexConfigurator = PolarityReportsIndexConfigurator::class;
    protected $with = ['polarity', 'user', 'outUser', 'destination'];

    public const MOBILE_AGENT = 'mobile';
    public const WEB_AGENT = 'web';

    /**
     * @return array
     */
    public function toSearchableArray(): array
    {
        $polarityReport = $this->toArray();

        $polarityReport['prevented_time'] = Carbon::parse($polarityReport['prevented_at'])->format('g:i A');
        $polarityReport['prevented_at'] = Carbon::parse($polarityReport['prevented_at'])->format('m/d/Y');

        return $polarityReport;
    }

    /**
     * @param $value
     * @return string
     */
    public function getPreventedAtAttribute($value): string
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($value)->setTimezone(optional($school)->getTimezone())->format('Y-m-d H:i:s');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function outUser()
    {
        return $this->belongsTo(User::class, 'out_user_id')->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function destination()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function polarity()
    {
        return $this->belongsTo(Polarity::class);
    }
}
