<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlockPassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_passes', function (Blueprint $table) {
            $table->id();
            $table->timestamp('from_date');
            $table->timestamp('to_date');
            $table->boolean('student_pass');
            $table->boolean('proxy_pass');
            $table->boolean('kiosk_pass');
            $table->string('reason')->nullable();
            $table->text('message')->nullable();
            $table->unsignedBigInteger('school_id');
            $table->timestamps();
        });

        Schema::table('block_passes', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('block_passes', function (Blueprint $table) {
            $table->dropForeign('block_passes_school_id_foreign');
        });
        Schema::dropIfExists('block_passes');
    }
}
