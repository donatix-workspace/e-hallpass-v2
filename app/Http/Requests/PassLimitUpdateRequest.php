<?php

namespace App\Http\Requests;

use App\Rules\DaysKeyExistRule;
use App\Rules\RecurrenceDaysRule;
use App\Rules\RecurrenceMonthlyCorrectStructureRule;
use App\Rules\RecurrenceTypeRule;
use Illuminate\Foundation\Http\FormRequest;

class PassLimitUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'from_date' => 'date|required',
            'to_date' => 'date|required|after_or_equal:from_date',
            'limit' => 'required|integer|min:0',
            'reason' => 'max:255|nullable|sometimes',
            'recurrence_type' => ['nullable', new RecurrenceTypeRule()],
            'recurrence_days' => ['json', 'nullable', new DaysKeyExistRule(), new RecurrenceDaysRule()],
            'recurrence_end_at' => 'date|after:yesterday|nullable'
        ];
    }

    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}
