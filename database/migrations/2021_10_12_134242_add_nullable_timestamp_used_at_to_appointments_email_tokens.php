<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullableTimestampUsedAtToAppointmentsEmailTokens extends Migration
{
    public function up()
    {
        Schema::dropColumns('appointments_email_tokens', 'used_at');
        Schema::table('appointments_email_tokens', function (Blueprint $table) {
            $table->timestamp('used_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('appointments_email_tokens', function (Blueprint $table) {
            //
        });
    }
}
