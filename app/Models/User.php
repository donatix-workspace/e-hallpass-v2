<?php

namespace App\Models;

use App\Elastic\UserIndexConfigurator;
use App\Events\AppointmentPassPageRefreshEvent;
use App\Http\Filters\Filterable;
use App\Scopes\NotArchivedUser;
use App\Scopes\NotDeletedUser;
use App\Scopes\NotSystemUser;
use App\Scopes\WithValidMembership;
use App\Traits\ExtendedSearchable;
use App\Traits\PerPage;
use App\Traits\UserMapping;
use Cache;
use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements PushNotifications
{
    use Notifiable,
        HasApiTokens,
        Filterable,
        PerPage,
        HasFactory,
        UserMapping,
        ExtendedSearchable;

    protected $with = ['role:id,name', 'roleUser'];
    protected $appends = [
        'passes_for_today_count',
        'comment_type',
        'name',
        'auth_type_name',
        'is_synced',
        'current_role_in_school'
    ];

    protected $indexConfigurator = UserIndexConfigurator::class;

    /**
     * Role constants
     *
     * @var string
     */
    public const Admin = 'admin';
    public const Teacher = 'teacher';
    public const Student = 'student';
    public const Staff = 'staff';
    public const SuperAdmin = 'superadmin';
    public const USER_SEARCHABLE = 1;
    public const TYPE_FROM = 'from';
    public const TYPE_TO = 'to';

    public const ACTIVE = 1;
    public const INACTIVE = 0;

    public const ACTIVE_STATUS_NAME = 'active';
    public const INACTIVE_STATUS_NAME = 'archived';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Route notifications for the FCM channel.
     *
     * @param \Illuminate\Notifications\Notification $notification
     * @return string
     */
    public function routeNotificationForFcm($notification): string
    {
        return $this->device_token ?? '';
    }

    /**
     * @return array
     */
    public function toSearchableArray(): array
    {
        $userArray = $this->setAppends([
            'comment_type',
            'name',
            'auth_type_name'
        ])
            ->load(['schoolUser', 'roleUser'])
            ->toArray();

        return $userArray;
    }

    /**
     * @param $value
     * @return string|null
     */
    public function getArchivedAtAttribute($value): ?string
    {
        if ($value === null) {
            return null;
        }

        $schoolTimezone = optional(
            School::findCacheFirst(optional($this)->school_id)
        )->getTimezone();

        return Carbon::parse($value, config('app.timezone'))
            ->setTimezone($schoolTimezone)
            ->format('m/d/Y');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeBelongsToThisSchool($query)
    {
        return $query->whereHasIn('schoolUser', function ($query) {
            $query->where('school_id', optional(auth()->user())->school_id);
        });
    }

    /**
     * @return $this
     */
    public function archive(): User
    {
        $this->schoolUser()
            ->where('user_id', $this->id)
            ->where('school_id', auth()->user()->school_id)
            ->update([
                'archived_at' => now(),
                'archived_by' => auth()->user()->id
            ]);

        $this->update([
            //                    'status' => User::INACTIVE,
            //                    'archived_by' => auth()->user()->id,
            //                    'archived_at' => now(),
            'archived_school' => auth()->user()->school_id
        ]);

        return $this;
    }

    /**
     * @param $query
     * @param $roleId
     * @return mixed
     */
    public function scopeBelongsToThisSchoolAndGivenRole($query, $roleId)
    {
        return $query->whereHasIn('schoolUser', function ($query) use (
            $roleId
        ) {
            $query
                ->where(
                    'schools_users.school_id',
                    optional(auth()->user())->school_id
                )
                ->where('schools_users.role_id', $roleId)
                ->where('schools_users.status', User::ACTIVE)
                ->whereNull('schools_users.deleted_at')
                ->whereNull('schools_users.archived_at');
        });
    }

    /**
     * @param int|null $schoolId
     * @return bool
     */
    public function isWithWrongSchoolDomain(?int $schoolId): bool
    {
        if (!$schoolId) {
            return false;
        }

        $emailDomain = optional(explode('@', $this->email))[1];

        return !Domain::whereSchoolId($schoolId)
            ->where('name', $emailDomain)
            ->exists();
    }

    /**
     * @param $query
     * @return mixed
     */
    public static function makeAllSearchableUsing($query)
    {
        return $query
            ->withoutGlobalScopes()
            ->skipHidden()
            ->where('deleted_at', null);
    }

    /**
     * @return string
     */
    public function getNameWithInitialsAttribute(): string
    {
        $initials = $this->user_initials;

        if ($initials === 'null' || is_null($initials)) {
            $initials = '';
        }

        return "$initials $this->first_name $this->last_name";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function passSummaryReport(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(PassSummaryReport::class, 'destination');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'initial_password',
        'access_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    /**
     * Override booted method
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new NotArchivedUser());
        static::addGlobalScope(new NotSystemUser());
        static::addGlobalScope(new NotDeletedUser());
        static::addGlobalScope(new WithValidMembership());
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSkipSystem($query)
    {
        return $query->whereRaw("not (username <=> 'system')");
    }

    /**
     * @param DateTimeInterface $date
     * @return string
     */
    public function serializeDate(DateTimeInterface $date)
    {
        $school = Cache::remember(
            'school_' . $this->school_id,
            now()->addMinutes(5),
            function () {
                return School::find($this->school_id);
            }
        );

        return Carbon::parse($date, config('app.timezone'))
            ->setTimezone(optional($school)->getTimezone())
            ->format('Y-m-d H:i:s');
    }

    /**
     * @return string|null
     */
    public function getAuthTypeNameAttribute(): ?string
    {
        if ($this->auth_type === null) {
            return config('authtype.1');
        }

        return config('authtype.' . $this->auth_type);
    }

    /**
     * @param $value
     * @return string
     */
    public function getWebLastLoginAtAttribute($value): ?string
    {
        // If value is null then return null
        if ($value === null) {
            return null;
        }
        $schoolTimezone = optional(
            School::findCacheFirst(optional($this)->school_id)
        )->getTimezone();

        return Carbon::parse($value, config('app.timezone'))
            ->setTimezone($schoolTimezone)
            ->format('m-d-Y / g:i:s A');
    }

    /**
     * @param $value
     * @return string
     *archived_date
     */
    public function getMobileLastLoginAtAttribute($value): ?string
    {
        // If value is null then return null
        if ($value === null) {
            return null;
        }

        $schoolTimezone = optional(
            School::findCacheFirst(optional($this)->school_id)
        )->getTimezone();

        return Carbon::parse($value, config('app.timezone'))
            ->setTimezone($schoolTimezone)
            ->format('m-d-Y / g:i:s A');
    }

    /**
     * @param $value
     * @return string
     */
    public function getMobileLastUsedAtAttribute($value): ?string
    {
        // If value is null then return null
        if ($value === null) {
            return null;
        }

        $schoolTimezone = optional(
            School::findCacheFirst(optional($this)->school_id)
        )->getTimezone();

        return Carbon::parse($value, config('app.timezone'))
            ->setTimezone($schoolTimezone)
            ->format('m-d-Y / g:i:s A');
    }

    /**
     * @return string
     */
    public function getNameAttribute(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return mixed|null
     */
    public function getStudentSisIdAttribute()
    {
        $memberShipStudentSisId = collect($this->roleUser)
            ->where('pivot.user_id', $this->id)
            ->where('pivot.school_id', optional(auth()->user())->school_id)
            ->pluck('pivot.student_sis_id')
            ->first();

        return $memberShipStudentSisId !== null ? $memberShipStudentSisId : '0';
    }

    /**
     * @return mixed
     */
    public function getCurrentRoleInSchoolAttribute()
    {
        return collect($this->roleUser)
            ->where('pivot.school_id', optional(auth()->user())->school_id)
            ->first();
    }

    /**
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'id';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public static function getStatuses()
    {
        return [self::ACTIVE_STATUS_NAME, self::INACTIVE_STATUS_NAME];
    }

    /**
     * @return string
     */
    public function getCommentTypeAttribute()
    {
        return Room::COMMENT_MANDATORY;
    }

    public function getIsSyncedAttribute()
    {
        return !!$this->cus_uuid || !$this->initial_password;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role->name === self::Admin;
    }

    /**
     * @return bool
     */
    public function isStaff()
    {
        return $this->role->name === self::Staff;
    }

    /**
     * @return bool
     */
    public function isTeacher()
    {
        return $this->role->name === self::Teacher;
    }

    public function isSuperAdmin()
    {
        return $this->role->name === self::SuperAdmin;
    }

    /**
     * @return bool
     */
    public function isStudent()
    {
        return optional($this->role)->name === self::Student;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFromStaff($query)
    {
        return $query->where('role_id', '!=', config('roles.student'));
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFromStudents($query)
    {
        return $query->where('role_id', '=', config('roles.student'));
    }

    public function scopeSkipHidden($query)
    {
        return $query->where(function ($subQuery) {
            $subQuery
                ->where('auth_type', '!=', AuthType::HIDDEN)
                ->orWhereNull('auth_type');
        });
    }

    public function commonUserServices()
    {
        return $this->hasMany(CommonUserService::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function schools()
    {
        return $this->belongsToMany(School::class, 'schools_users')
            ->where('schools_users.status', 1)
            ->whereNull('schools_users.deleted_at')
            ->whereNull('schools_users.archived_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function passSummaryReports()
    {
        return $this->hasMany(PassSummaryReport::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function staffSchedules()
    {
        return $this->hasMany(StaffSchedule::class);
    }

    /**
     * @return bool
     */
    public function getIsLockedAttribute(): bool
    {
        if ($this->role_id === config('roles.student')) {
            $pinAttemptsCount = $this->pinAttempts()
                ->where('status', Pin::ACTIVE)
                ->count();
            if ($pinAttemptsCount >= PinAttempt::MAXIMUM_PIN_ATTEMPTS) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return int
     */
    public function getPinAttemptsCountAttribute(): int
    {
        if ($this->role_id === config('roles.student')) {
            return $this->pinAttempts()
                ->where('status', Pin::ACTIVE)
                ->count();
        }

        return 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function pin()
    {
        return $this->morphOne(Pin::class, 'pinnable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function studentFavourites()
    {
        return $this->hasMany(StudentFavorite::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kiosks()
    {
        return $this->hasMany(Kiosk::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function kioskUsers()
    {
        return $this->hasOne(KioskUser::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function passes()
    {
        return $this->hasMany(Pass::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function fromPass()
    {
        return $this->morphMany(Pass::class, 'from');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function toPass()
    {
        return $this->morphMany(Pass::class, 'to');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function autoPassesLimits()
    {
        return $this->hasMany(AutoPassLimit::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function schoolUser(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(School::class, 'schools_users');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roleUser(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'schools_users')->withPivot(
            'school_id',
            'status',
            'deleted_at',
            'archived_at',
            'archived_by',
            'student_sis_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function autoRoomTeachers()
    {
        return $this->hasMany(AutoRoomTeacher::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function unavailable()
    {
        return $this->morphMany(Unavailable::class, 'unavailable');
    }

    public function activeUnavailability(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(Unavailable::class, 'unavailable')
            ->without('unavailable')
            ->where('status', Unavailable::ACTIVE)
            ->where('from_date', '<=', Carbon::now()->toDateTimeString())
            ->where('to_date', '>=', Carbon::now()->toDateTimeString())
            ->where('canceled_for_today_at', null);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function unArchive()
    {
        return $this->hasOne(UnArchive::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function passLimits()
    {
        return $this->morphMany(PassLimit::class, 'limitable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function pins()
    {
        return $this->morphMany(Pin::class, 'pinnable');
    }

    public function getPinAttribute()
    {
        if ($this->role_id !== config('roles.student')) {
            if (
                $pinObject = $this->pins()
                    ->where('school_id', $this->school_id)
                    ->first()
            ) {
                return $pinObject->pin;
            }
        }

        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roomRestrictions()
    {
        return $this->hasMany(RoomRestriction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pinAttempts()
    {
        return $this->hasMany(PinAttempt::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointmentPasses()
    {
        return $this->hasMany(AppointmentPass::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function fromAppointment()
    {
        return $this->morphMany(AppointmentPass::class, 'from');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function toAppointment()
    {
        return $this->morphMany(AppointmentPass::class, 'to');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transparancies()
    {
        return $this->hasMany(TransparencyUser::class);
    }

    public function assignedRooms()
    {
        return $this->belongsToMany(
            Room::class,
            'staff_schedules',
            'user_id',
            'room_id'
        )->wherePivot('school_id', $this->school_id);
    }

    /**
     * @return bool
     */
    public function isSubstitute()
    {
        return $this->is_substitute !== 0;
    }

    /**
     * @return array|false
     */
    public function getIsAvailableAttribute()
    {
        if ($this->role_id !== config('roles.student')) {
            $unavailable = Unavailable::where('school_id', $this->school_id)
                ->active()
                ->where('unavailable_type', User::class)
                ->where('unavailable_id', $this->id)
                ->where('from_date', '<=', Carbon::now()->toDateTimeString())
                ->where('to_date', '>=', Carbon::now()->toDateTimeString())
                ->where('canceled_for_today_at', null)
                ->first();

            if ($unavailable !== null) {
                return [
                    'unavailable_from' => School::convertTimeToCorrectTimezone(
                        $unavailable->from_date,
                        $unavailable->school->getTimezone()
                    ),
                    'unavailable_to' => School::convertTimeToCorrectTimezone(
                        $unavailable->to_date,
                        $unavailable->school->getTimezone()
                    ),
                    'comment' => $unavailable->comment
                ];
            }
        }

        return null;
    }

    /**
     * @return integer|null
     */
    public function getPassesForTodayCountAttribute(): ?int
    {
        if ($this->role_id === config('roles.student')) {
            $schoolTimezone = optional(
                School::findCacheFirst($this->school_id)
            )->getTimezone();

            $startDate = Carbon::parse(
                now()
                    ->setTimezone($schoolTimezone)
                    ->startOfDay(),
                $schoolTimezone
            )->setTimezone(config('app.timezone'));
            $endDate = Carbon::parse(
                now()
                    ->setTimezone($schoolTimezone)
                    ->endOfDay(),
                $schoolTimezone
            )->setTimezone(config('app.timezone'));

            return Pass::ofUserId($this->id)
                ->fromSchoolId(optional(auth()->user())->school_id)
                ->whereParentId(null)
                ->without([
                    'createdByUser',
                    'completedByUser',
                    'comments',
                    'latestComment',
                    'requestedByUser',
                    'approvedByUser'
                ])
                ->whereBetween('created_at', [$startDate, $endDate])
                ->where(function ($query) {
                    $query
                        ->where('pass_status', Pass::ACTIVE)
                        ->where('approved_at', '!=', null)
                        ->orWhere(function ($query) {
                            $query
                                ->where('completed_by', '!=', null)
                                ->orWhere('completed_at', '!=', null)
                                ->where('pass_status', Pass::INACTIVE);
                        });
                })
                ->count();
        }

        return null;
    }

    /**
     * @param $value
     * @return string|null
     */
    public function getAvatarAttribute($value): ?string
    {
        if (!$value) {
            return null;
        }

        $disk = 's3';

        if ($this->isStudent()) {
            $disk = 's3common';
        }

        if (!Storage::disk($disk)->exists($value)) {
            return null;
        }

        return Storage::disk($disk)->temporaryUrl(
            $value,
            now()->addMinutes(60)
        );
    }

    //    public function getIsAvailableAttribute()
    //    {
    //        return !Unavailable::where('unavailable_type', User::class)
    //            ->where('unavailable_id', $this->id)
    //            ->fromSchoolId($this->school_id)
    //            ->whereTime('from_date', '<=', Carbon::now())
    //            ->whereTime('to_date', '>=', Carbon::now())
    //            ->exists();
    //    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function studentFavorites()
    {
        return $this->morphMany(StudentFavorite::class, 'favorable');
    }

    /**
     * @return bool
     */
    public function cancelAppointmentPasses(): bool
    {
        $toAppointments = $this->toAppointment()
            ->where('school_id', $this->school_id)
            ->where('canceled_at', null)
            ->where('expired_at', null);

        $fromAppointments = $this->fromAppointment()
            ->where('school_id', $this->school_id)
            ->where('canceled_at', null)
            ->where('expired_at', null);

        $appointmentPassesUser = $this->appointmentPasses()
            ->where('school_id', $this->school_id)
            ->where('canceled_at', null)
            ->where('expired_at', null);

        $recurrenceAppointmentPassesTo = RecurrenceAppointmentPass::where(
            'to_type',
            self::class
        )
            ->where('to_id', $this->id)
            ->where('school_id', $this->school_id);

        $recurrenceAppointmentPassesTo->update([
            'canceled_at' => now(),
            'reason' => 'Canceled (Teacher is archived)'
        ]);
        $recurrenceAppointmentPassesTo->searchable();

        $recurrenceAppointmentPassesFrom = RecurrenceAppointmentPass::where(
            'from_type',
            self::class
        )
            ->where('canceled_at', null)
            ->where('from_id', $this->id)
            ->where('school_id', $this->school_id);

        $recurrenceAppointmentPassesFrom->update([
            'from_type' => self::class,
            'from_id' => config('roles.system')
        ]);
        $recurrenceAppointmentPassesFrom->searchable();

        $recurrenceAppointmentPassStudent = RecurrenceAppointmentPass::where(
            'canceled_at',
            null
        )
            ->where('user_id', $this->id)
            ->where('school_id', $this->school_id);

        $recurrenceAppointmentPassStudent->update([
            'canceled_at' => now(),
            'reason' => 'Canceled (Student is archived)'
        ]);
        $recurrenceAppointmentPassStudent->searchable();

        $appointmentsIdsTo = $toAppointments
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();

        $fromAppointments->select('id')->each(function ($item) {
            $item->update([
                'from_type' => User::class,
                'from_id' => config('roles.system')
            ]);
            $item->searchable();
        });

        $appointmentsIdsUsers = $appointmentPassesUser
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();

        $toAppointments->update(['canceled_at' => now()]);
        $appointmentPassesUser->update(['canceled_at' => now()]);

        $commentsArray = [];

        foreach ($appointmentsIdsUsers as $appointmentId) {
            array_push($commentsArray, [
                'commentable_id' => $appointmentId,
                'commentable_type' => AppointmentPass::class,
                'comment' => 'Canceled (Student is archived)',
                'user_id' => $this->id,
                'permission' => Comment::ONLY_STAFF,
                'school_id' => $this->school_id,
                'created_at' => now(),
                'updated_at' => now()
            ]);

            AppointmentPass::find($appointmentId)->searchable();
        }

        foreach ($appointmentsIdsTo as $appointmentId) {
            array_push($commentsArray, [
                'commentable_id' => $appointmentId,
                'commentable_type' => AppointmentPass::class,
                'user_id' => $this->id,
                'comment' => 'Canceled (Teacher is archived)',
                'permission' => Comment::ONLY_STAFF,
                'school_id' => $this->school_id,
                'created_at' => now(),
                'updated_at' => now()
            ]);

            AppointmentPass::find($appointmentId)->searchable();
        }

        try {
            \DB::beginTransaction();
            Comment::insert($commentsArray);
            \DB::commit();

            event(new AppointmentPassPageRefreshEvent($this->school_id));
        } catch (\Throwable $e) {
            logger($e->getMessage());
        }

        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function destinationPolarityReport()
    {
        return $this->morphMany(PolarityReport::class, 'destination');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mobileSettings(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(UserMobileSetting::class);
    }

    /**
     * @param $total
     * @param $todaysCreated
     * @param $invited
     * @param $loggedIn
     * @return array
     */
    public static function countStatisticCollection(
        $total,
        $todayCreated,
        $invited,
        $loggedIn
    ) {
        return [
            'total' => $total,
            'todaysCreated' => $todayCreated,
            'invited' => $invited,
            'loggedIn' => $loggedIn
        ];
    }

    /**
     * @return array
     */
    public static function countStatistics(): array
    {
        $user = auth()->user();

        $students = User::belongsToThisSchoolAndGivenRole(
            config('roles.student')
        )
            ->skipHidden()
            ->get();

        $teachers = User::belongsToThisSchoolAndGivenRole(
            config('roles.teacher')
        )
            ->where('is_substitute', User::INACTIVE)
            ->skipHidden()
            ->get();

        $staffs = User::belongsToThisSchoolAndGivenRole(config('roles.staff'))
            ->skipHidden()
            ->get();

        $admins = User::belongsToThisSchoolAndGivenRole(config('roles.admin'))
            ->skipHidden()
            ->get();

        return [
            'students' => self::countStatisticCollection(
                $students->count(),
                $students
                    ->where('created_at', '>', Carbon::today()->startOfDay())
                    ->where('created_at', '<', Carbon::today()->endOfDay())
                    ->count(),
                InviteUser::getInvitedCountByRole(config('roles.student')),
                $students->where('is_loggin', 1)->count()
            ),

            'teachers' => self::countStatisticCollection(
                $teachers->count(),
                $teachers
                    ->where('created_at', '>', Carbon::today()->startOfDay())
                    ->where('created_at', '<', Carbon::today()->endOfDay())
                    ->count(),
                InviteUser::getInvitedCountByRole(config('roles.teacher')),
                $teachers->where('is_loggin', 1)->count()
            ),

            'staff' => self::countStatisticCollection(
                $staffs->count(),
                $staffs
                    ->where('created_at', '>', Carbon::today()->startOfDay())
                    ->where('created_at', '<', Carbon::today()->endOfDay())
                    ->count(),
                InviteUser::getInvitedCountByRole(config('roles.staff')),
                $staffs->where('is_loggin', 1)->count()
            ),

            'admin' => self::countStatisticCollection(
                $admins->count(),
                $admins
                    ->where('created_at', '>', Carbon::today()->startOfDay())
                    ->where('created_at', '<', Carbon::today()->endOfDay())
                    ->count(),
                InviteUser::getInvitedCountByRole(config('roles.admin')),
                $admins->where('is_loggin', 1)->count()
            )
        ];
    }

    public function getTopics()
    {
        $env = config('services.fcm.env');
        //dev.
        //local.
        //staging.
        //production.
        return "{$env}.user.profile.{$this->id}";
    }

    public function getNotificationTitle($event)
    {
        return __($event . 'Title');
    }

    public function getNotificationBody($event)
    {
        return __($event . 'Body');
    }

    public function getData($event, $options)
    {
        return [
            'event' => $event,
            'payload' => [
                'id' => $this->id
            ],
            'options' => $options
        ];
    }

    public function getNotificationSettings($event, $options)
    {
        return [
            'topics' => $this->getTopics(),
            'title' => $this->getNotificationTitle($event),
            'body' => $this->getNotificationBody($event),
            'data' => $this->getData($event, $options)
        ];
    }

    public function isBlacklisted()
    {
        return DB::connection('commonDb')
            ->table('auth_user_email_bounce')
            ->where('auth_user_id', $this->common_user_id)
            ->exists();
    }

    public function isArchivePrevented($school)
    {
        return UnArchive::where('user_id', $this->id)
            ->where('school_id', $school->id)
            ->exists();
    }
}
