<?php

namespace App\Http\Resources;

use App\Models\School;
use Illuminate\Http\Resources\Json\JsonResource;

class PolarityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $school = School::findCacheFirst(auth()->user()->school_id);


        return [
            'id' => $this->id,
            'first_user' => $this->firstUser,
            'second_user' => $this->secondUser,
            'status' => $this->status,
            'message' => $this->message,
            'times_triggered' => $this->times_triggered,
            'created_at' => School::convertTimeToCorrectTimezone($this->created_at, $school->getTimezone()),
            'updated_at' => School::convertTimeToCorrectTimezone($this->updated_at, $school->getTimezone())
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array|string[]
     */
    public function with($request)
    {
        return [
            'status' => 'success'
        ];
    }
}
