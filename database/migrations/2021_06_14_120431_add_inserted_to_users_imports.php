<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInsertedToUsersImports extends Migration
{
    public function up()
    {
        Schema::table('users_imports', function (Blueprint $table) {
            $table->boolean('inserted')->default(0);
        });
    }

    public function down()
    {
        Schema::table('users_imports', function (Blueprint $table) {
            //
        });
    }
}
