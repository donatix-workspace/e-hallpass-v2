<?php

namespace App\Traits;

trait ContactTracingMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'email' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => '-'
            ],
            'first_name' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => '-'
            ],
            'last_name' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => '-'
            ],
            'overlap_time_count' => [
                'type' => 'long'
            ],
            'share_same_location' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => '-'
            ],
            'search_id' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => '-'
            ],
            'school_id' => [
                'type' => 'long'
            ]
        ]
    ];
}
