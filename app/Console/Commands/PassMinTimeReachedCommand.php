<?php

namespace App\Console\Commands;

use App\Events\PassMinTimeReached;
use App\Models\Pass;
use Carbon\Carbon;
use Illuminate\Console\Command;

class PassMinTimeReachedCommand extends Command
{
    protected $signature = 'passes:mintime';

    protected $description = 'Turn the pass in yellow, when the min time is reached.';

    public function handle()
    {
        $passes = Pass::with('child')
            ->whereBetween('created_at', [
                now()
                    ->startOfDay()
                    ->toDateTimeString(),
                now()
                    ->endOfDay()
                    ->toDateTimeString()
            ])
            ->where('expired_at', null)
            ->where('canceled_at', null)
            ->where('pass_status', Pass::ACTIVE)
            ->where(function ($query) {
                $query
                    ->where('approved_at', '!=', null)
                    ->where('flagged_at', null)
                    ->orWhere(function ($query) {
                        $query
                            ->where('approved_at', null)
                            ->where('flagged_at', null)
                            ->where('parent_id', '!=', null);
                    });
            })
            ->orderBy('created_at', 'desc')
            ->cursor();

        foreach ($passes as $pass) {
            $pass->setAppends([]);
            if ($pass->child !== null) {
                if ($pass->child->isReachedMinTime(true)) {
                    $pass->update([
                        'flagged_at' => now()
                    ]);

                    $pass->child->update([
                        'flagged_at' => now()
                    ]);

                    event(
                        new PassMinTimeReached(
                            Pass::with('child')
                                ->without('parent')
                                ->where(
                                    'id',
                                    $pass->parent_id !== null
                                        ? $pass->parent_id
                                        : $pass->id
                                )
                                ->firstOrFail()
                                ->append(['expire_after', 'flags'])
                        )
                    );
                }
            } else {
                if ($pass->isReachedMinTime($pass->parent_id !== null)) {
                    $pass->update([
                        'flagged_at' => now()
                    ]);

                    event(
                        new PassMinTimeReached(
                            Pass::with('child')
                                ->without('parent')
                                ->where(
                                    'id',
                                    $pass->parent_id !== null
                                        ? $pass->parent_id
                                        : $pass->id
                                )
                                ->firstOrFail()
                                ->append(['expire_after', 'flags'])
                        )
                    );
                }
            }

            $pass->searchable();
            if ($pass->parent !== null) {
                $pass->parent->searchable();
            }
        }

        return 0;
    }
}
