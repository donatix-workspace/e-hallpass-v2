<?php

namespace App\Http\Controllers\Admin\Polarities;

use App\Http\Controllers\Controller;
use App\Http\Requests\PolarityMessageRequest;
use App\Http\Requests\PolarityRequest;
use App\Http\Resources\PolarityCollection;
use App\Http\Resources\PolarityReportsCollection;
use App\Http\Resources\PolarityResource;
use App\Models\Polarity;
use App\Models\PolarityReport;
use App\Models\TeacherPassSetting;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;

/**
 * @group Polarities
 */
class PolarityController extends Controller
{
    /**
     * Return all polarities records
     *
     * @return PolarityCollection
     * @apiResourceModel App\Models\Polarity
     * @apiResourceCollection App\Http\Resources\PolarityCollection
     * @authenticated
     * @throws AuthorizationException
     */
    public function index()
    {
        $user = auth()->user();
        $this->authorize('viewAny', Polarity::class);

        if (request()->has('search_query') || request()->has('sort')) {
            $polarities = collect(
                Polarity::search(
                    request()->get('search_query') === null
                        ? config('scout_elastic.all_records')
                        : convertElasticQuery(request()->get('search_query'))
                )
                    ->where('school_id', $user->school_id)
                    ->when(request()->has('sort'), function ($query) {
                        $sort = explode(':', request()->get('sort'));
                        if (count($sort) <= 1) {
                            return $query->orderBy('id', 'asc');
                        }

                        $sortingKeywords = ['asc', 'desc'];
                        if (in_array($sort[1], $sortingKeywords)) {
                            return $query->orderBy($sort[0], $sort[1]);
                        }
                        return $query->orderBy('id', 'asc');
                    })
                    ->take(Polarity::recordsPerPage())
                    ->get()
            )
                ->pluck('id')
                ->toArray();

            $polaritiesIdsImploded = implode(',', $polarities);

            $polarities = Polarity::whereIntegerInRaw('id', $polarities)
                ->orderByRaw("field(id,$polaritiesIdsImploded)")
                ->fromSchoolId($user->school_id)
                ->paginate(Polarity::recordsPerPage());
        } else {
            $polarities = Polarity::fromSchoolId($user->school->id)->paginate(
                Polarity::recordsPerPage()
            );
        }

        return new PolarityCollection($polarities);
    }

    /**
     * Get polarity advanced reports
     *
     * @authenticated
     * @apiResourceModel App\Models\PolarityReport
     * @apiResourceCollection App\Http\Resources\PolarityReportsCollection
     * @return PolarityReportsCollection
     * @throws AuthorizationException
     */
    public function reports(): PolarityReportsCollection
    {
        $user = auth()->user();
        $this->authorize('viewAny', Polarity::class);

        $searchQuery =
            request()->get('search_query') !== null
                ? convertElasticQuery(request()->get('search_query'))
                : config('scout_elastic.all_records');

        if (request()->has('sort') || request()->has('search_query')) {
            $polarityReports = PolarityReport::search($searchQuery)
                ->when(request()->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('id', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                    return $query->orderBy('id', 'asc');
                })
                ->where('school_id', $user->school_id)
                ->paginate(config('pagination.default_pagination'));
        } else {
            $polarityReports = PolarityReport::whereSchoolId(
                $user->school_id
            )->paginate(config('pagination.default_pagination'));
        }

        return new PolarityReportsCollection($polarityReports);
    }

    /**
     * Create a polarity restriction
     *
     * @bodyParam first_user_id integer required The id of the first user
     * @bodyParam second_user_id integer required The id of the second user
     * @bodyParam status boolean required Polarity active statuTest polarity messages
     * @bodyParam message string Polarity restriction reason (Message)
     *
     * @param PolarityRequest $request
     * @return JsonResponse
     * @authenticated
     * @response {
     *      "data": {"id": 1},
     *      "status": "success"
     * }
     * @throws AuthorizationException
     */
    public function store(PolarityRequest $request)
    {
        $user = auth()->user();

        $this->authorize('create', Polarity::class);

        $polarityExist = Polarity::where(
            'first_user_id',
            $request->input('first_user_id')
        )
            ->where('second_user_id', $request->input('second_user_id'))
            ->whereStatus(Polarity::POLARITY_STATUS_ACTIVE)
            ->fromSchoolId($user->school_id)
            ->exists();

        if ($polarityExist) {
            return response()->json([
                'data' => [],
                'message' => __('polarity.already.exist'),
                'status' => __('fail')
            ]);
        }

        // Save new polarity
        $polarity = Polarity::create([
            'first_user_id' => $request->input('first_user_id'),
            'second_user_id' => $request->input('second_user_id'),
            'message' => $request->input('message'),
            'status' => Polarity::POLARITY_STATUS_ACTIVE,
            'school_id' => $user->school->id
        ]);

        return response()->json(
            [
                'data' => ['id' => $polarity->id],
                'status' => __('success')
            ],
            201
        );
    }

    /**
     * Show the polarity restriction
     *
     * @urlParam polarity integer required The id of the polarity
     *
     * @param Polarity $polarity
     * @return PolarityResource
     * @apiResource App\Http\Resources\PolarityResource
     * @apiResourceModel App\Models\Polarity
     * @throws AuthorizationException
     * @authenticated
     */
    public function show(Polarity $polarity)
    {
        $this->authorize('view', $polarity);

        return new PolarityResource($polarity);
    }

    /**
     * Update the polarity restriction record
     *
     * @urlParam polarity integer required The id of the polarity record
     * @bodyParam first_user_id integer required The id of the first user
     * @bodyParam second_user_id integer required The id of the second user
     * @bodyParam status boolean required Polarity active status
     * @bodyParam message string Polarity restriction reason (Message)
     *
     * @param Polarity $polarity
     * @param PolarityRequest $request
     * @return PolarityResource
     * @apiResource App\Http\Resources\PolarityResource
     * @apiResourceModel App\Models\Polarity
     * @throws AuthorizationException
     * @authenticated
     */
    public function update(Polarity $polarity, PolarityRequest $request)
    {
        $this->authorize('update', $polarity);

        $polarity->update([
            'first_user_id' => $request->input('first_user_id'),
            'second_user_id' => $request->input('second_user_id'),
            'message' => $request->input('message'),
            'status' => $request->input('status')
        ]);
        return new PolarityResource($polarity);
    }

    /**
     * Update the polarity active status
     *
     * @urlParam polarity integer required The id of the polarity record
     *
     * @param Polarity $polarity
     * @return JsonResponse
     * @throws AuthorizationException
     * @response {
     *      'data': {"id": 1, "status": 1},
     *      'status': 'success'
     * }
     * @authenticated
     */
    public function status(Polarity $polarity)
    {
        $this->authorize('updateStatus', $polarity);

        $polarity->status === $polarity::POLARITY_STATUS_ACTIVE
            ? $polarity->update([
                'status' => $polarity::POLARITY_STATUS_INACTIVE
            ])
            : $polarity->update([
                'status' => $polarity::POLARITY_STATUS_ACTIVE
            ]);

        return response()->json(
            [
                'data' => [
                    'id' => $polarity->id,
                    'status' => $polarity->status
                ],
                'status' => __('success')
            ],
            200
        );
    }

    /**
     * Delete a polarity restriction
     *
     * @urlParam polarity integer required The id of the polarity record
     * @param Polarity $polarity
     * @return PolarityResource
     * @apiResource App\Http\Resources\PolarityResource
     * @apiResourceModel App\Models\Polarity
     * @throws AuthorizationException
     * @authenticated
     */
    public function delete(Polarity $polarity): PolarityResource
    {
        $this->authorize('delete', $polarity);

        $polarity->delete();

        return new PolarityResource($polarity);
    }

    /**
     * Update the polarity message
     *
     * @bodyParam message string The ab polarity message that need to be shown.
     *
     * @param PolarityMessageRequest $request
     * @return JsonResponse
     * @authenticated
     */
    public function saveMessage(PolarityMessageRequest $request): JsonResponse
    {
        $user = auth()->user();

        $updateTeacherPassSettingMessage = TeacherPassSetting::where(
            'school_id',
            $user->school_id
        )->update(['polarity_message' => $request->input('message')]);

        return response()->json([
            'data' => [
                'message' => $request->input('message')
            ],
            'status' => __('success')
        ]);
    }

    /**
     * Get the polarity message
     *
     * @return JsonResponse
     * @authenticated
     */
    public function message()
    {
        $user = auth()->user();

        $message = TeacherPassSetting::where('school_id', $user->school_id)
            ->select('polarity_message')
            ->first();

        return response()->json([
            'data' => [
                'message' => optional($message)->polarity_message,
                'initial_default_value' => __('passes.polarities.student')
            ],
            'status' => __('success')
        ]);
    }
}
