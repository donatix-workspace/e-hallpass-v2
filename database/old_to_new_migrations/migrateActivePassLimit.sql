SET FOREIGN_KEY_CHECKS  = 0;
START TRANSACTION ;
TRUNCATE ehp_v2_prod.active_pass_limits;
INSERT INTO ehp_v2_prod.active_pass_limits ( student_pass, kiosk_pass, proxy_pass, teacher_override, `limit`, school_id, created_at, updated_at)  SELECT
        (SELECT oldApl.stu_status),
        (SELECT oldApl.ksk_status),
        (SELECT IF(oldApl.prx_status >= 1, 1 ,0)),
        (SELECT IF(oldApl.prx_status > 1, 1 ,0)),
        (SELECT oldApl.`limit`),
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldApl.school_id limit 1),
        (SELECT NOW()),
        (SELECT NOW())
FROM ehallpassDB.pass_limit_concurrent as oldApl;
COMMIT;
