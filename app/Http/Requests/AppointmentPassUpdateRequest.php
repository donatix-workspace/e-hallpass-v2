<?php

namespace App\Http\Requests;

use App\Rules\CheckIfItsMidnightRule;
use App\Rules\PassRoomOrUserExistRule;
use App\Rules\RoomOrUserExistsRule;
use App\Rules\RoomOrUserStringRule;
use App\Rules\TimeAfterNowRule;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class AppointmentPassUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() &&
            !auth()
                ->user()
                ->isStudent();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $fromType = Request::input('from_type');
        $toType = Request::input('to_type');

        return [
            'from_id' => ['integer', new PassRoomOrUserExistRule($fromType)],
            'to_id' => ['integer', new PassRoomOrUserExistRule($toType)],
            'from_type' => ['required', 'string', new RoomOrUserStringRule()],
            'to_type' => ['required', 'string', new RoomOrUserStringRule()],
            'for_date' => 'required|date',
            'for_time' => [
                'required',
                'date_format:H:i',
                new CheckIfItsMidnightRule()
            ],
            'period_id' => 'required|exists:periods,id|integer',
            'reason' => 'required|max:255|string',
            'user_id' => [
                'required',
                'integer',
                Rule::exists('users', 'id')->where(function ($query) {
                    $query
                        ->where('school_id', auth()->user()->school_id)
                        ->where('status', 1)
                        ->where('role_id', config('roles.student'));
                })
            ]
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'appointments.*.for_date.after' =>
                'Please provide valid time and date.',
            'appointments.*.from_time.after' =>
                'Please provide valid time and date.'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validatorOnTime = \Illuminate\Support\Facades\Validator::make(
            $this->all(),
            [
                'for_date' => 'after_or_equal:now -1 minute'
            ]
        );

        $validator->after(function ($validator) use ($validatorOnTime) {
            if ($validatorOnTime->fails()) {
                $validator
                    ->errors()
                    ->add(
                        'for_time',
                        'The time must be after or equal to now!'
                    );
            }
        });
    }
}
