SET FOREIGN_KEY_CHECKS  = 0;
START TRANSACTION ;
TRUNCATE ehp_v2_prod.pins;
INSERT INTO ehp_v2_prod.pins (school_id, pinnable_type, pinnable_id, pin, created_at, updated_at)
SELECT
       (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldTp.school_id limit 1),
       (SELECT 'App\\Models\\User'),
       (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldTp.teacher_id limit 1),
       (SELECT oldTp.pin),
       (SELECT CONVERT_TZ(oldTp.created_at, 'America/New_York', 'UTC')),
       (SELECT CONVERT_TZ(oldTp.updated_at, 'America/New_York', 'UTC'))
FROM ehallpassDB.teacher_pin as oldTp;

INSERT INTO ehp_v2_prod.pins (school_id, pinnable_type, pinnable_id, pin, created_at, updated_at)
SELECT
    (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldTp.school_id limit 1),
    (SELECT 'App\\Models\\Room'),
    (SELECT id FROM ehp_v2_prod.rooms WHERE old_id = oldTp.room_id limit 1),
    (SELECT oldTp.pin),
    (SELECT CONVERT_TZ(oldTp.created_at, 'America/New_York', 'UTC')),
    (SELECT CONVERT_TZ(oldTp.updated_at, 'America/New_York', 'UTC'))
FROM ehallpassDB.room_pins as oldTp;

COMMIT ;

UPDATE
    ehp_v2_prod.pins
SET
    pinnable_id = (
        SELECT
            to_id
        FROM
            duplicate_users
        WHERE
            from_id = pinnable_id
        LIMIT 1)
WHERE
    pins.pinnable_type = 'App\\Models\\User'
    AND pinnable_id IN(
        SELECT
            from_id FROM duplicate_users);
