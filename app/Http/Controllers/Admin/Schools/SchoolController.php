<?php

namespace App\Http\Controllers\Admin\Schools;

use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group School manage (Admin/Teacher/Staff)
 */
class SchoolController extends Controller
{
    /**
     * Change user current school.
     *
     * @urlParam school integer required The id of the school.
     *
     * @response status=200 scenario="Successful change" {
     *      "data": {
     *          "school_id": 1
     *      }
     *      "status": "success"
     * }
     *
     * @response status=403 scenario="Undefined school." {
     *      "data": {
     *          "school_id": 1
     *      },
     *      "message": "invalid.school.for.change"
     *      "status": "fail"
     * }
     *
     * @param School $school
     * @return JsonResponse
     */
    public function change(School $school)
    {
        $schoolId = $school->id;

        $membership = DB::table('schools_users')->where([
            'user_id' => auth()->user()->id,
            'school_id' => $schoolId
        ])->first();

        //Update the assigned school.
        if ($membership) {
            $roleId = $membership->role_id ?? auth()->user()->role_id;
            $updateUserSchool = auth()->user()->update([
                'school_id' => $schoolId,
                'role_id' => $roleId
            ]);

            return response()->json([
                'data' => [
                    'school_id' => $schoolId,
                    'role_id' => $membership->role_id
                ],
                'status' => __('success')
            ]);
        }

        return response()->json([
            'data' => ['school_id' => $schoolId],
            'message' => __('invalid.school.for.change'),
            'status' => __('fail')
        ], 403);

    }
}
