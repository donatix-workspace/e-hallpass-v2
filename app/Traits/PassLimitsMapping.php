<?php

namespace App\Traits;

trait PassLimitsMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'id' => [
                'type' => 'long'
            ],
            'limitable' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'limit' => [
                'type' => 'long',
                'null_value' => 0
            ],
            'for_date' => [
                'type' => 'date',
                'null_value' => 'NULL'
            ],
            'school_id' => [
                'type' => 'long'
            ],
            'for' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'archived' => [
                'type' => 'long',
            ],
            'created_at' => [
                'type' => 'date',
                "format" => "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||HH:mm||HH||epoch_millis"
            ],
            'created_on' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'to_date' => [
                'type' => 'date',
                "format" => "MM/dd/yyyy",
                "fields" => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'from_date' => [
                'type' => 'date',
                "format" => "MM/dd/yyyy",
                "fields" => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'reason' => [
                'type' => 'text',
                'fielddata' => true,
            ],
            'recurrence_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 'does not repeat'
            ]
        ]
    ];
}
