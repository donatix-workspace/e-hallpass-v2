<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeletedAtToPolaritiesTable extends Migration
{
    public function up()
    {
        Schema::table('polarities', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::table('polarities', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
