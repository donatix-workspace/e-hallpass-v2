<?php

namespace App\Jobs;

use App\Events\AppointmentPassCreated;
use App\Events\AppointmentPassPageRefreshEvent;
use App\Models\AppointmentPass;
use App\Models\RecurrenceAppointmentPass;
use App\Models\Transparency;
use App\Notifications\Push\StudentNotification;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;
use Log;
use Throwable;

class GenerateAppointmentsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $appointmentsArray;

    private $updatedAppointmentId;

    /**
     * @var Authenticatable
     */
    private $user;

    public function __construct(
        array $appointmentsArray,
        Authenticatable $user,
        ?int $updatedAppointmentId = 0
    ) {
        $this->appointmentsArray = $appointmentsArray;
        $this->user = $user;
        $this->updatedAppointmentId = $updatedAppointmentId;
    }

    /**
     * @throws Throwable
     */
    public function handle()
    {
        $user = $this->user;

        $appointmentInputs = $this->appointmentsArray;
        $appointmentPassData = [];
        $appointmentPassesForTheDay = [];
        $appointmentPassesForTheWeek = [];
        $appointmentPassesForTheMonth = [];

        // If there's update on the recurrence of a normal apt pass.
        // We delete it and replace it with the new one that contains the recurrence
        // data.
        if ($this->updatedAppointmentId) {
            AppointmentPass::where('id', $this->updatedAppointmentId)
                ->where('school_id', $user->school_id)
                ->delete();
        }

        foreach ($appointmentInputs as $appointmentInput) {
            if (
                array_key_exists('recurrence_type', $appointmentInput) &&
                $appointmentInput['recurrence_type'] !== null
            ) {
                if (
                    $appointmentInput['recurrence_type'] ===
                    config('recurrences.daily')
                ) {
                    $startDate = Carbon::parse($appointmentInput['for_date']);

                    if ($startDate->isSaturday() || $startDate->isSunday()) {
                        $startDate = $startDate->addWeek();
                        $startDate = $startDate->startOfWeek();
                    }

                    logger('StartDate: ', [
                        $startDate,
                        Carbon::parse($appointmentInput['recurrence_end_at'])
                    ]);

                    $days = CarbonPeriod::create(
                        $startDate,
                        Carbon::parse(
                            $appointmentInput['recurrence_end_at']
                        )->endOfDay()
                    );

                    logger('Recurrence Days: ', [$days]);

                    $recurrenceAppointmentPass = RecurrenceAppointmentPass::create(
                        [
                            'created_by' => $user->id,
                            'user_id' => $appointmentInput['user_id'],
                            'from_id' => $appointmentInput['from_id'],
                            'from_type' => $appointmentInput['from_type'],
                            'to_id' => $appointmentInput['to_id'],
                            'to_type' => $appointmentInput['to_type'],
                            'period_id' => $appointmentInput['period_id'],
                            'recurrence_type' =>
                                $appointmentInput['recurrence_type'],
                            'recurrence_end_at' =>
                                $appointmentInput['recurrence_end_at'],
                            'reason' => $appointmentInput['reason'],
                            'for_date' => $appointmentInput['for_date'],
                            'school_id' => $user->school_id
                        ]
                    );

                    $insertedForThatWeekCount = 0;
                    foreach ($days as $day) {
                        $forDateTimeFormat = Carbon::parse(
                            $appointmentInput['for_date']
                        )
                            ->setTimezone(config('app.timezone'))
                            ->format('H:i');
                        logger('Condition daily: ', [
                            !in_array($day->dayOfWeek, [
                                Carbon::SATURDAY,
                                Carbon::SUNDAY
                            ])
                        ]);
                        if (
                            !in_array($day->dayOfWeek, [
                                Carbon::SATURDAY,
                                Carbon::SUNDAY
                            ]) &&
                            $insertedForThatWeekCount <=
                                AppointmentPass::GENERATE_DAILY_PER_JOB
                        ) {
                            $appointmentPassesForTheDay[] = [
                                'created_by' => $user->id,
                                'from_id' => $appointmentInput['from_id'],
                                'confirmed_by' => $user->id,
                                'from_type' => $appointmentInput['from_type'],
                                'to_id' => $appointmentInput['to_id'],
                                'to_type' => $appointmentInput['to_type'],
                                'user_id' => $appointmentInput['user_id'],
                                'recurrence_appointment_pass_id' =>
                                    $recurrenceAppointmentPass->id,
                                'for_date' => Carbon::parse(
                                    $day->toDateString() . " $forDateTimeFormat"
                                )->setTimezone(config('app.timezone')),
                                'school_id' => $user->school_id,
                                'period_id' => $appointmentInput['period_id'],
                                'confirmed_by_teacher_at' => now(),
                                'pass_id' => null,
                                'reason' => $appointmentInput['reason'],
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ];

                            $insertedForThatWeekCount++;
                        }
                    }

                    if (
                        !Transparency::isEnabled() ||
                        (Transparency::isEnabled() &&
                            $recurrenceAppointmentPass->isCoverTheTransparency())
                    ) {
                        event(
                            new AppointmentPassCreated(
                                $recurrenceAppointmentPass->refresh(),
                                $recurrenceAppointmentPass->created_by
                            )
                        );
                        //                        Notification::send(
                        //                            $recurrenceAppointmentPass,
                        //                            new StudentNotification('AppointmentPassCreated')
                        //                        );
                    }
                }

                if (
                    $appointmentInput['recurrence_type'] ===
                    config('recurrences.weekly')
                ) {
                    $days = CarbonPeriod::create(
                        Carbon::parse($appointmentInput['for_date']),
                        Carbon::parse(
                            $appointmentInput['recurrence_end_at']
                        )->endOfDay()
                    );

                    $recurrenceAppointmentPass = RecurrenceAppointmentPass::create(
                        [
                            'created_by' => $user->id,
                            'user_id' => $appointmentInput['user_id'],
                            'from_id' => $appointmentInput['from_id'],
                            'from_type' => $appointmentInput['from_type'],
                            'to_id' => $appointmentInput['to_id'],
                            'to_type' => $appointmentInput['to_type'],
                            'period_id' => $appointmentInput['period_id'],
                            'recurrence_type' =>
                                $appointmentInput['recurrence_type'],
                            'recurrence_days' =>
                                $appointmentInput['recurrence_days'],
                            'recurrence_end_at' =>
                                $appointmentInput['recurrence_end_at'],
                            'reason' => $appointmentInput['reason'],
                            'for_date' => $appointmentInput['for_date'],
                            'school_id' => $user->school_id
                        ]
                    );

                    $recurrenceDays = collect(
                        json_decode($recurrenceAppointmentPass->recurrence_days)
                    )->get('days');
                    $endRecurrenceDate = Carbon::parse(
                        $recurrenceAppointmentPass->recurrence_end_at
                    );
                    $forDateTimeFormat = Carbon::parse(
                        $appointmentInput['for_date']
                    )
                        ->setTimezone(config('app.timezone'))
                        ->format('H:i');

                    if (!$endRecurrenceDate->isPast()) {
                        foreach ($days as $day) {
                            if (
                                in_array(
                                    $day->englishDayOfWeek,
                                    $recurrenceDays
                                )
                            ) {
                                $appointmentPassesForTheWeek[] = [
                                    'created_by' => $user->id,
                                    'from_id' => $appointmentInput['from_id'],
                                    'from_type' =>
                                        $appointmentInput['from_type'],
                                    'to_id' => $appointmentInput['to_id'],
                                    'recurrence_appointment_pass_id' =>
                                        $recurrenceAppointmentPass->id,
                                    'confirmed_by' => $user->id,
                                    'to_type' => $appointmentInput['to_type'],
                                    'user_id' => $appointmentInput['user_id'],
                                    'for_date' => Carbon::parse(
                                        $day->toDateString() .
                                            " $forDateTimeFormat"
                                    )->setTimezone(config('app.timezone')),
                                    'school_id' => $user->school_id,
                                    'period_id' =>
                                        $appointmentInput['period_id'],
                                    'confirmed_by_teacher_at' => now(),
                                    'pass_id' => null,
                                    'reason' => $appointmentInput['reason'],
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now()
                                ];
                            }
                        }
                        event(
                            new AppointmentPassCreated(
                                $recurrenceAppointmentPass->refresh(),
                                $recurrenceAppointmentPass->created_by
                            )
                        );
                        //                        Notification::send(
                        //                            $recurrenceAppointmentPass,
                        //                            new StudentNotification('AppointmentPassCreated')
                        //                        );
                    }
                }

                if (
                    $appointmentInput['recurrence_type'] ===
                    config('recurrences.monthly')
                ) {
                    $days = CarbonPeriod::create(
                        Carbon::parse($appointmentInput['for_date'])->addDay(),
                        Carbon::parse(
                            $appointmentInput['recurrence_end_at']
                        )->endOfDay()
                    );

                    $recurrenceAppointmentPass = RecurrenceAppointmentPass::create(
                        [
                            'created_by' => $user->id,
                            'user_id' => $appointmentInput['user_id'],
                            'from_id' => $appointmentInput['from_id'],
                            'from_type' => $appointmentInput['from_type'],
                            'to_id' => $appointmentInput['to_id'],
                            'to_type' => $appointmentInput['to_type'],
                            'period_id' => $appointmentInput['period_id'],
                            'recurrence_type' =>
                                $appointmentInput['recurrence_type'],
                            'recurrence_week' =>
                                $appointmentInput['recurrence_week'],
                            'recurrence_end_at' =>
                                $appointmentInput['recurrence_end_at'],
                            'reason' => $appointmentInput['reason'],
                            'for_date' => $appointmentInput['for_date'],
                            'school_id' => $user->school_id
                        ]
                    );

                    $recurrenceWeek = json_decode(
                        $recurrenceAppointmentPass->recurrence_week,
                        true
                    );
                    $forDateTimeFormat = Carbon::parse(
                        $appointmentInput['for_date']
                    )
                        ->setTimezone(config('app.timezone'))
                        ->format('H:i');

                    Log::channel('appointments')->info(
                        'Recurrence Appointment With Id: ' .
                            $recurrenceAppointmentPass->id .
                            ', will be inserted with recurrence week arr: ',
                        $recurrenceWeek
                    );

                    $appointmentPassesForTheMonth[] = [
                        'created_by' => $user->id,
                        'from_id' => $appointmentInput['from_id'],
                        'from_type' => $appointmentInput['from_type'],
                        'confirmed_by' => $user->id,
                        'recurrence_appointment_pass_id' =>
                            $recurrenceAppointmentPass->id,
                        'to_id' => $appointmentInput['to_id'],
                        'to_type' => $appointmentInput['to_type'],
                        'user_id' => $appointmentInput['user_id'],
                        'for_date' => Carbon::parse(
                            $appointmentInput['for_date']
                        )->setTimezone(config('app.timezone')),
                        'school_id' => $user->school_id,
                        'period_id' => $appointmentInput['period_id'],
                        'pass_id' => null,
                        'confirmed_by_teacher_at' => now(),
                        'reason' => $appointmentInput['reason'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];

                    $insertedForThatMonth = 0;
                    foreach ($days as $day) {
                        Log::channel('appointments')->info(
                            'Inserting monthly appointment (IF condition status): ',
                            [
                                array_key_exists(
                                    $day->weekNumberInMonth,
                                    $recurrenceWeek
                                ) &&
                                $recurrenceWeek[$day->weekNumberInMonth] ===
                                    $day->englishDayOfWeek,
                                $insertedForThatMonth <=
                                AppointmentPass::GENERATE_MONTHLY_PER_JOB,
                                $recurrenceWeek
                            ]
                        );
                        if (
                            array_key_exists(
                                $day->weekNumberInMonth,
                                $recurrenceWeek
                            ) &&
                            $recurrenceWeek[$day->weekNumberInMonth] ===
                                $day->englishDayOfWeek &&
                            $insertedForThatMonth <=
                                AppointmentPass::GENERATE_MONTHLY_PER_JOB
                        ) {
                            $appointmentPassesForTheMonth[] = [
                                'created_by' => $user->id,
                                'from_id' => $appointmentInput['from_id'],
                                'from_type' => $appointmentInput['from_type'],
                                'confirmed_by' => $user->id,
                                'recurrence_appointment_pass_id' =>
                                    $recurrenceAppointmentPass->id,
                                'to_id' => $appointmentInput['to_id'],
                                'to_type' => $appointmentInput['to_type'],
                                'user_id' => $appointmentInput['user_id'],
                                'for_date' => Carbon::parse(
                                    $day->toDateString() . " $forDateTimeFormat"
                                )->setTimezone(config('app.timezone')),
                                'school_id' => $user->school_id,
                                'period_id' => $appointmentInput['period_id'],
                                'pass_id' => null,
                                'confirmed_by_teacher_at' => now(),
                                'reason' => $appointmentInput['reason'],
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ];
                            $insertedForThatMonth++;
                        }
                    }

                    if (
                        !Transparency::isEnabled() ||
                        (Transparency::isEnabled() &&
                            $recurrenceAppointmentPass->isCoverTheTransparency())
                    ) {
                        event(
                            new AppointmentPassCreated(
                                $recurrenceAppointmentPass->refresh(),
                                $recurrenceAppointmentPass->created_by
                            )
                        );
                        //                        Notification::send(
                        //                            $recurrenceAppointmentPass,
                        //                            new StudentNotification('AppointmentPassCreated')
                        //                        );
                    }
                }
            } else {
                $appointmentPassData[] = [
                    'created_by' => $user->id,
                    'from_id' => $appointmentInput['from_id'],
                    'from_type' => $appointmentInput['from_type'],
                    'confirmed_by' => $user->id,
                    'to_id' => $appointmentInput['to_id'],
                    'to_type' => $appointmentInput['to_type'],
                    'user_id' => $appointmentInput['user_id'],
                    'for_date' => Carbon::parse(
                        $appointmentInput['for_date']
                    )->setTimezone(config('app.timezone')),
                    'school_id' => $user->school_id,
                    'period_id' => $appointmentInput['period_id'],
                    'confirmed_by_teacher_at' => now(),
                    'pass_id' => null,
                    'reason' => $appointmentInput['reason'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }
        }
        try {
            DB::beginTransaction();

            foreach ($appointmentPassData as $appointmentPass) {
                $appointmentPassExists = AppointmentPass::where(
                    'user_id',
                    $appointmentPass['user_id']
                )
                    ->where(
                        'for_date',
                        '=',
                        Carbon::parse($appointmentPass['for_date'])
                            ->setTimezone(config('app.timezone'))
                            ->toDateTimeString()
                    )
                    ->where('school_id', $user->school_id)
                    ->exists();

                $appointmentPassExists15Min = AppointmentPass::where(
                    'user_id',
                    $appointmentPass['user_id']
                )
                    ->where(
                        'for_date',
                        '=',
                        Carbon::parse($appointmentPass['for_date'])
                            ->setTimezone(config('app.timezone'))
                            ->addMinutes(15)
                            ->toDateTimeString()
                    )
                    ->where('school_id', $user->school_id)
                    ->exists();

                if (!$appointmentPassExists || !$appointmentPassExists15Min) {
                    $appointment = AppointmentPass::create($appointmentPass);
                    $appointment->executeIfItsForNow();

                    $appointment->comments()->create([
                        'comment' => $appointment->reason,
                        'user_id' => $appointment->created_by,
                        'school_id' => $appointment->school_id
                    ]);
                    if (
                        !Transparency::isEnabled() ||
                        (Transparency::isEnabled() &&
                            $appointment->isCoverTheTransparency())
                    ) {
                        event(
                            new AppointmentPassCreated(
                                $appointment,
                                $appointment->created_by
                            )
                        );
                        //                        Notification::send(
                        //                            $appointment,
                        //                            new StudentNotification('AppointmentPassCreated')
                        //                        );
                    }
                }
            }

            if (count($appointmentPassesForTheMonth) > 0) {
                foreach ($appointmentPassesForTheMonth as $appointmentPass) {
                    Log::channel('appointments')->info(
                        'Insert monthly apt passes now!: ' .
                            now()->toDateTimeString() .
                            ' UTC'
                    );
                    $appointment = AppointmentPass::create($appointmentPass);
                    $appointment->executeIfItsForNow();

                    $appointment->comments()->create([
                        'comment' => $appointment->reason,
                        'user_id' => $appointment->created_by,
                        'school_id' => $appointment->school_id
                    ]);

                    if (
                        !Transparency::isEnabled() ||
                        (Transparency::isEnabled() &&
                            $appointment->isCoverTheTransparency())
                    ) {
                        event(
                            new AppointmentPassCreated(
                                $appointment,
                                $appointment->created_by
                            )
                        );
                        //                        Notification::send(
                        //                            $appointment,
                        //                            new StudentNotification('AppointmentPassCreated')
                        //                        );
                    }
                }
            }

            if (count($appointmentPassesForTheWeek) > 0) {
                foreach ($appointmentPassesForTheWeek as $appointmentPass) {
                    $appointment = AppointmentPass::create($appointmentPass);

                    $appointment->comments()->create([
                        'comment' => $appointment->reason,
                        'user_id' => $appointment->created_by,
                        'school_id' => $appointment->school_id
                    ]);

                    $appointment->executeIfItsForNow();

                    if (
                        !Transparency::isEnabled() ||
                        (Transparency::isEnabled() &&
                            $appointment->isCoverTheTransparency())
                    ) {
                        event(
                            new AppointmentPassCreated(
                                $appointment,
                                $appointment->created_by
                            )
                        );
                        //                        Notification::send(
                        //                            $appointment,
                        //                            new StudentNotification('AppointmentPassCreated')
                        //                        );
                    }
                }
            }

            if (count($appointmentPassesForTheDay) > 0) {
                foreach ($appointmentPassesForTheDay as $appointmentPass) {
                    $appointment = AppointmentPass::create($appointmentPass);

                    $appointment->comments()->create([
                        'comment' => $appointment->reason,
                        'user_id' => $appointment->created_by,
                        'school_id' => $appointment->school_id
                    ]);

                    $appointment->executeIfItsForNow();

                    if (
                        !Transparency::isEnabled() ||
                        (Transparency::isEnabled() &&
                            $appointment->isCoverTheTransparency())
                    ) {
                        event(
                            new AppointmentPassCreated(
                                $appointment,
                                $appointment->created_by
                            )
                        );
                        //                        Notification::send(
                        //                            $appointment,
                        //                            new StudentNotification('AppointmentPassCreated')
                        //                        );
                    }
                }
            }

            DB::commit();
            Log::info(
                'Inserting appointment on: ' .
                    Carbon::now() .
                    '| Timezone: ' .
                    Carbon::now()->getTimezone()
            );
            event(new AppointmentPassPageRefreshEvent($user->school_id));
        } catch (Throwable $e) {
            DB::rollBack();
            Log::error($e);
        }
    }
}
