<?php

namespace App\Http\Requests;

use App\Rules\AssignCorrectRoomRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AssignDeleteRequest extends FormRequest
{
    public function rules()
    {
        return [
            'room_ids' => 'required|array',
            'room_ids.*' => ['integer', Rule::exists('rooms', 'id')->where(function ($query) {
                $query->where('school_id', auth()->user()->school_id);
            }), new AssignCorrectRoomRule()],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin() || auth()->user()->isStaff();
    }

}
