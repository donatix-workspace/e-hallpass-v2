<?php

namespace App\Console\Commands;

use App\Models\School;
use Illuminate\Console\Command;

class FixDeprecatedSchoolTimezonesCommand extends Command
{
    protected $signature = 'ehpv2:fix_deprecated_timezones';

    protected $description = 'Command description';

    public function handle()
    {
        $schools = \DB::table('schools')
            ->orderBy('id', 'asc')
            ->select('id', 'timezone')
            ->get()
            ->toArray();
        $db = \DB::connection('mysql');
        $deprecatedTimezones = config('timezones.deprecated');
        $this->info('Fixing timezones. Please wait...');

        $schoolsChunks = array_chunk($schools, 1000);
        $deprecatedSchoolsCount = 0;
        foreach ($schoolsChunks as $schoolsChunk) {
            foreach ($schoolsChunk as $school) {
                if (array_key_exists($school->timezone, $deprecatedTimezones)) {
                    $db->table('schools')->where('id', $school->id)->update([
                        'timezone' => $deprecatedTimezones[$school->timezone]
                    ]);
                    $deprecatedSchoolsCount++;
                }
            }
        }
        $this->newLine();
        $this->info("Fixed $deprecatedSchoolsCount wrong timezones.");
    }
}
