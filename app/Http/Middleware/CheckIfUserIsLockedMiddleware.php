<?php

namespace App\Http\Middleware;

use App\Models\Pin;
use App\Models\PinAttempt;
use Closure;
use Illuminate\Http\Request;

class CheckIfUserIsLockedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();
        $pinAttemptsCount = optional($user)->pinAttempts()->whereStatus(Pin::ACTIVE)->count();

        // The pin attempts exceeded.
        if ($pinAttemptsCount >= PinAttempt::MAXIMUM_PIN_ATTEMPTS) {
            return response()->json([
                'data' => [
                    'pin_attempts' => $pinAttemptsCount,
                    'locked' => true
                ],
                'message' => __('user.locked'),
                'status' => __('fail')
            ], 423); // Locked status code 423
        }
        return $next($request);
    }
}
