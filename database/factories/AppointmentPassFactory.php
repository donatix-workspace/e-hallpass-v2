<?php

namespace Database\Factories;

use App\Models\AppointmentPass;
use App\Models\Pass;
use App\Models\Period;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class AppointmentPassFactory extends Factory
{
    protected $model = AppointmentPass::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'created_by' => $this->faker->word,
            'from_type' => $this->faker->word,
            'from_id' => $this->faker->randomNumber(),
            'to_type' => $this->faker->word,
            'to_id' => $this->faker->randomNumber(),
            'for_date' => $this->faker->word,
            'reason' => $this->faker->word,
            'cancel_reason' => $this->faker->word,
            'ran_at' => Carbon::now(),
            'confirmed_at' => Carbon::now(),
            'acknowledge_at' => Carbon::now(),
            'canceled_at' => Carbon::now(),
            'confirmed_by_teacher_at' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'user_id' => function () {
                return User::factory()->create()->id;
            },
            'school_id' => function () {
                return School::factory()->create()->id;
            },
            'period_id' => function () {
                return Period::factory()->create()->id;
            },
            'pass_id' => function () {
                return Pass::factory()->create()->id;
            },
        ];
    }
}
