<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddConfirmedByAndCanceledByToAppointmentPasses extends Migration
{
    public function up()
    {
        Schema::table('appointment_passes', function (Blueprint $table) {
            $table->unsignedBigInteger('canceled_by')->nullable();
            $table->unsignedBigInteger('acknowledged_by')->nullable();

            $table->foreign('canceled_by')->references('id')->on('users');
            $table->foreign('acknowledged_by')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::table('appointment_passes', function (Blueprint $table) {
            $table->dropColumn('canceled_by');
            $table->dropColumn('acknowledged_by');
        });
    }
}
