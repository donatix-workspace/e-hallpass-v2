<?php

namespace App\Rules;

use App\Models\Kiosk;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class KioskCodeIsValidRule implements Rule
{
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $request = request();

        if ($request->input('type') !== Kiosk::STANDARD_TYPE) {
            $userWithThatCodeExists = User::where('student_sis_id', $value)
                ->exists();

            if (!$userWithThatCodeExists) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please provide a valid QR code or bar code.';
    }
}
