<?php

namespace App\Jobs;

use App\Mail\SendInvitationToUserMail;
use App\Models\InviteUser;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendUserInviteJob implements ShouldQueue
{
    private $userData;
    private $user;
    private $preventArchive;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct($userData, User $user, $preventArchive = false)
    {
        $this->userData = $userData;
        $this->user = $user;
        $this->preventArchive = $preventArchive;
    }

    public function handle()
    {
        $user = $this->user;
        $userData = $this->userData;

        $inviteUser = InviteUser::create([
            'user_id' => $user->id,
            'school_id' => $user->school_id,
            'role_id' => $user->role_id,
            'login' => $user->auth_type,
            'archive' => $this->preventArchive,
            'status' => 1,
            'invite' => 1,
            'invite_error' => 0,
            'invited' => 1
        ]);

        // Send an email to the user.
        Mail::to($user, $user->school_id)->send(
            new SendInvitationToUserMail($userData, $user)
        );
    }
}
