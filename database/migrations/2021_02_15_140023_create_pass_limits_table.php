<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePassLimitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pass_limits', function (Blueprint $table) {
            $table->id();
            $table->morphs('limitable');
            $table->integer('limit')->default(1);
            $table->date('from_date');
            $table->date('to_date');
            $table->json('grade_year')->nullable();
            $table->string('reason');
            $table->string('recurrence_type')->nullable();
            $table->boolean('archived')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pass_limits');
    }
}
