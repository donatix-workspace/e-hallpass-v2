<?php

namespace App\Http\Filters;

use Illuminate\Http\Request;

class RoomFilter extends QueryFilters
{

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    /**
     * @param $sort
     * @return mixed
     */
    public function sort($sort = "id:asc")
    {
        $sortExploding = explode(":", $sort);

        if (count($sortExploding) <= 1) {
            return $this->builder;
        }

        $sortingKeywords = ['asc', 'desc'];
        $joinTables = ['nurses'];

        if (in_array($sortExploding[1], $sortingKeywords)) {
            if (strpos($sortExploding[0], '.') > -1) {

                $joins = explode(".", $sortExploding[0]);
                if (in_array($method = $joins[0], $joinTables)) {
                    return $this->$method($joins[1], $sortExploding[1]);
                }

            } else {
                return $this->builder->orderBy($sortExploding[0], $sortExploding[1]);
            }
        }

        return $this->builder;
    }

    private function nurses($col, $direction)
    {
        //TODO: find relationship filters solution
        return $this->builder->with('nurses', function ($q) use ($col, $direction) {
            return $q->orderBy($col, $direction);
        });

    }

}
