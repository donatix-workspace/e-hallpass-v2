<?php

namespace App\Http\Filters;

use App\Models\RoomRestriction;
use Illuminate\Http\Request;

class RestrictionFilter extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function inactive($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status ? $this->builder->where(function ($query) {
            return $query->where('status', RoomRestriction::INACTIVE);
        }) : $this->builder;
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function active($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status ? $this->builder->where(function ($query) {
            return $query->where('status', RoomRestriction::ACTIVE);
        }) : $this->builder;
    }
}
