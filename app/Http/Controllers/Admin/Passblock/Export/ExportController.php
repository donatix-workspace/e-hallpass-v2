<?php

namespace App\Http\Controllers\Admin\Passblock\Export;

use App\Http\Controllers\Controller;
use App\Http\Filters\RestrictionFilter;
use App\Imports\LimitStudentImport;
use App\Imports\LocationCapacityImport;
use App\Imports\PassBlockExport;
use App\Imports\RoomRestrictionsImport;
use App\Models\LocationCapacity;
use App\Models\RoomRestriction;
use Excel;

class ExportController extends Controller
{
    /**
     * Download pass limit csv
     *
     *
     * @authenticated
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function index()
    {
        return Excel::download(new LimitStudentImport(), 'pass-limits.csv');
    }

    /**
     * Download location capacities csv
     *
     *
     * @authenticated
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function capacityCsv(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        $user = auth()->user();

        if (request()->has('search_query') || request()->has('sort')) {
            $locationCapacitiesQuery = collect(
                LocationCapacity::search(
                    request()->get('search_query') === null
                        ? config('scout_elastic.all_records')
                        : convertElasticQuery(
                            request()->get('search_query'),
                            request()->has('convert_int')
                        )
                )
                    ->with(['room:id,name'])
                    ->where('school_id', $user->school_id)
                    ->when(request()->has('sort'), function ($query) {
                        $sort = explode(':', request()->get('sort'));
                        if (count($sort) <= 1) {
                            return $query->orderBy('id', 'asc');
                        }

                        $sortingKeywords = ['asc', 'desc'];
                        if (in_array($sort[1], $sortingKeywords)) {
                            return $query->orderBy($sort[0], $sort[1]);
                        }
                        return $query->orderBy('id', 'asc');
                    })
                    ->take(LocationCapacity::recordsPerPage())
                    ->get()
            )
                ->pluck('id')
                ->toArray();

            $locationCapacitiesIds = implode(',', $locationCapacitiesQuery);
            $locationCapacitiesQuery = LocationCapacity::whereIntegerInRaw(
                'id',
                $locationCapacitiesQuery
            )
                ->orderByRaw("field(id,$locationCapacitiesIds)")
                ->fromSchoolId($user->school_id);
        } else {
            $locationCapacitiesQuery = LocationCapacity::with(
                'room:id,name'
            )->fromSchoolId($user->school_id);
        }

        return Excel::download(
            new LocationCapacityImport($locationCapacitiesQuery),
            'location-capacities.csv'
        );
    }

    /**
     * Download restriction csv
     *
     *
     * @authenticated
     * @param RestrictionFilter $filters
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function restrictionCsv(
        RestrictionFilter $filters
    ): \Symfony\Component\HttpFoundation\BinaryFileResponse {
        $user = auth()->user();

        if (request()->has('search_query') || request()->has('sort')) {
            $roomRestriction = collect(
                RoomRestriction::search(
                    request()->get('search_query') === null
                        ? config('scout_elastic.all_records')
                        : convertElasticQuery(request()->get('search_query'))
                )
                    ->where('school_id', $user->school_id)
                    ->when(request()->has('sort'), function ($query) {
                        $sort = explode(':', request()->get('sort'));
                        if (count($sort) <= 1) {
                            return $query->orderBy('id', 'asc');
                        }

                        $sortingKeywords = ['asc', 'desc'];
                        if (in_array($sort[1], $sortingKeywords)) {
                            return $query->orderBy($sort[0], $sort[1]);
                        }
                        return $query->orderBy('id', 'asc');
                    })
                    ->take(RoomRestriction::recordsPerPage())
                    ->get()
            )
                ->pluck('id')
                ->toArray();

            $roomRestrictionImploded = implode(',', $roomRestriction);

            $roomRestrictionQuery = RoomRestriction::whereIntegerInRaw(
                'id',
                $roomRestriction
            )
                ->orderByRaw("field(id,$roomRestrictionImploded)")
                ->fromSchoolId($user->school_id)
                ->filter($filters);
        } else {
            $roomRestrictionQuery = RoomRestriction::with([
                'user:id,first_name,last_name',
                'room:id,name'
            ])
                ->orderBy('from_date', 'desc')
                ->fromSchoolId($user->school_id)
                ->filter($filters);
        }

        return Excel::download(
            new RoomRestrictionsImport($roomRestrictionQuery),
            'room-restrictions.csv'
        );
    }

    /**
     * Download pass block csv
     *
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function passBlock()
    {
        return Excel::download(new PassBlockExport(), 'pass-blocks.csv');
    }
}
