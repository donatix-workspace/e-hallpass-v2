CREATE PROCEDURE `transferData`()
BEGIN

    DECLARE last_room_id BIGINT DEFAULT 9999999999;
    SELECT old_id FROM ehp_v2_prod.rooms ORDER BY id DESC LIMIT 1 INTO last_room_id;

-- Auto Pass Sync
    SET foreign_key_checks =0;
    START TRANSACTION ;
    truncate ehp_v2_prod.auto_passes;
    INSERT INTO ehp_v2_prod.auto_passes (room_id,school_id,created_at,updated_at) SELECT
                                                                                      (SELECT id FROM ehp_v2_prod.rooms WHERE old_id=oldAp.room_id limit 1),
                                                                                      (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldAp.school_id limit 1),
                                                                                      (SELECT CONVERT_TZ(oldAp.created_at, 'America/New_York', 'UTC')),
                                                                                      (SELECT CONVERT_TZ(oldAp.updated_at, 'America/New_York', 'UTC'))
    FROM ehallpassDB.auto_pass as oldAp;
    COMMIT ;
    SET foreign_key_checks =1;


-- START Location Capacities

    SET FOREIGN_KEY_CHECKS  = 0;
    START TRANSACTION ;
    TRUNCATE ehp_v2_prod.location_capacities;

    INSERT INTO ehp_v2_prod.location_capacities (room_id, school_id, status, `limit`, created_at, updated_at)
    SELECT
        (SELECT id FROM ehp_v2_prod.rooms WHERE old_id=oldLc.room_id limit 1),
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldLc.school_id limit 1),
        (SELECT oldLc.status),
        (SELECT oldLc.`limit`),
        (SELECT CONVERT_TZ(oldLc.created_at, 'America/New_York', 'UTC')),
        (SELECT CONVERT_TZ(oldLc.updated_at, 'America/New_York', 'UTC'))
    FROM ehallpassDB.location_max_cap as oldLc;
    COMMIT;
    SET FOREIGN_KEY_CHECKS  = 1;


-- END Location Capacities

    SET FOREIGN_KEY_CHECKS=0;
    TRUNCATE ehp_v2_prod.polarities;
    INSERT INTO ehp_v2_prod.polarities (`first_user_id`,`second_user_id`,`status`,`school_id`,`message`,`created_at`,
                                        `updated_at`)
    SELECT
        (SELECT id FROM ehp_v2_prod.users where old_id=oldPolarity.first_user_id limit 1),
        (SELECT id FROM ehp_v2_prod.users where old_id=oldPolarity.second_user_id limit 1),
        (SELECT oldPolarity.status),
        (SELECT id FROM schools where old_id=oldPolarity.school_id limit 1),
        (SELECT oldPolarity.message_for_admin),
        (SELECT CONVERT_TZ(oldPolarity.created_at,
                           (SELECT timezone from schools where old_id = oldPolarity.school_id limit 1),
                           'UTC'
                    )) as created_at,
        (SELECT CONVERT_TZ(oldPolarity.updated_at,
                           (SELECT timezone from schools where old_id = oldPolarity.school_id limit 1),
                           'UTC'
                    )) as updated_at

    FROM ehallpassDB.ab_polarity as oldPolarity;
    SET FOREIGN_KEY_CHECKS=1;

    START TRANSACTION ;
    UPDATE
        ehp_v2_prod.polarities
    SET
        first_user_id = (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = first_user_id
            LIMIT 1)
    WHERE first_user_id IN(
        SELECT
            from_id FROM duplicate_users_clone);

    UPDATE
        ehp_v2_prod.polarities
    SET
        second_user_id = (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = second_user_id
            LIMIT 1)
    WHERE second_user_id IN(
        SELECT
            from_id FROM duplicate_users_clone);



-- START POLARITY REPORT
    SET FOREIGN_KEY_CHECKS  = 0;
    START TRANSACTION ;
    TRUNCATE ehp_v2_prod.polarities_reports;
    INSERT INTO ehp_v2_prod.polarities_reports (user_id, destination_type, destination_id, out_user_id, prevented_from, prevented_at, created_at, updated_at, polarity_id, school_id)
    SELECT
        (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldPr.student_prevented limit 1),
        (SELECT IF(oldPr.is_teacher = 1, 'App\\Models\\User', 'App\\Models\\Room')),
        (SELECT IF(oldPr.is_teacher = 1, (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldPr.prevented_destination limit 1), (SELECT id FROM ehp_v2_prod.rooms WHERE old_id = oldPr.prevented_destination limit 1))),
        (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldPr.active_student_id limit 1),
        (SELECT oldPr.origin),
        (SELECT CONVERT_TZ(oldPr.created_at, 'America/New_York', 'UTC')),
        (SELECT oldPr.created_at),
        (SELECT oldPr.updated_at),
        (SELECT getPolarityId(oldPr.student_prevented, oldPr.active_student_id)),
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldPr.school_id limit 1)
    FROM ehallpassDB.ab_polarity_report as oldPr;
    COMMIT ;



    SET FOREIGN_KEY_CHECKS = 1;

    START TRANSACTION ;
    UPDATE
        ehp_v2_prod.polarities_reports
    SET
        user_id= (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = polarities_reports.user_id
            LIMIT 1)
    WHERE user_id IN(
        SELECT
            from_id FROM duplicate_users_clone);

    UPDATE
        ehp_v2_prod.polarities_reports
    SET
        out_user_id= (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = polarities_reports.out_user_id
            LIMIT 1)
    WHERE out_user_id IN(
        SELECT
            from_id FROM duplicate_users_clone);

    UPDATE
        ehp_v2_prod.polarities_reports
    SET
        destination_id= (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = polarities_reports.destination_id
            LIMIT 1)
    WHERE destination_type='App\\Models\\User' AND destination_id IN(
        SELECT
            from_id FROM duplicate_users_clone);
    COMMIT;

-- Room Restriction START
    SET FOREIGN_KEY_CHECKS = 0;
    START TRANSACTION ;
    truncate ehp_v2_prod.rooms_restrictions;
    INSERT INTO ehp_v2_prod.rooms_restrictions (room_id, school_id, user_id, type, from_date, to_date, recurrence_type, grade_year, status, created_at, updated_at) SELECT
                                                                                                                                                                        (SELECT id FROM ehp_v2_prod.rooms WHERE old_id = oldLa.room_id limit 1),
                                                                                                                                                                        (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldLa.school_id limit 1),
                                                                                                                                                                        (SELECT id FROM ehp_v2_prod.users WHERE old_id=oldLa.student_id limit 1),
                                                                                                                                                                        (SELECT IF(oldLa.limitation_type = 1, 'access_denied', 'members_only')),
                                                                                                                                                                        (SELECT DATE(oldLa.from_datetime)),
                                                                                                                                                                        (SELECT DATE(oldLa.to_datetime)),
                                                                                                                                                                        (SELECT NULL),
                                                                                                                                                                        (SELECT IF(oldLa.gradeyear > 0, CONCAT('["',oldLa.gradeyear,'"]'), (SELECT NULL))),
                                                                                                                                                                        (SELECT oldLa.status),
                                                                                                                                                                        (SELECT CONVERT_TZ(oldLa.updated_at,
                                                                                                                                                                                           (SELECT timezone from ehp_v2_prod.schools where old_id = oldLa.school_id limit 1),
                                                                                                                                                                                           'UTC'
                                                                                                                                                                                    )),
                                                                                                                                                                        (SELECT CONVERT_TZ(oldLa.created_at,
                                                                                                                                                                                           (SELECT timezone from ehp_v2_prod.schools where old_id = oldLa.school_id limit 1),
                                                                                                                                                                                           'UTC'
                                                                                                                                                                                    ))

    FROM ehallpassDB.location_availability as oldLa;
    COMMIT ;
    SET FOREIGN_KEY_CHECKS = 1;

-- Update foreigns
    UPDATE
        ehp_v2_prod.rooms_restrictions
    SET
        user_id= (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = rooms_restrictions.user_id
            LIMIT 1)
    WHERE user_id IN(
        SELECT
            from_id FROM duplicate_users_clone);

-- End room restrictions

    SET FOREIGN_KEY_CHECKS = 0;

    START TRANSACTION ;
    truncate ehp_v2_prod.unavailables;

    INSERT INTO ehp_v2_prod.unavailables (unavailable_type, unavailable_id, school_id, from_date, to_date, status, recurrence_type, recurrence_days, recurrence_week, recurrence_end_at, comment, created_at, updated_at, alerted_at, canceled_for_today_at)
    SELECT
        (SELECT IF(oldOOO.room_id > 0, 'App\\Models\\Room', 'App\\Models\\User')),
        (SELECT IF(oldOOO.room_id > 0, (SELECT id FROM ehp_v2_prod.rooms WHERE old_id=oldOOO.room_id limit 1), (SELECT id FROM ehp_v2_prod.users WHERE old_id=oldOOO.user_id limit 1))),
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldOOO.school_id limit 1),
        (SELECT CONVERT_TZ(oldOOO.from_datetime,
                           (SELECT timezone from schools where old_id = oldOOO.school_id limit 1),
                           'UTC'
                    )),
        (SELECT CONVERT_TZ(oldOOO.to_datetime,
                           (SELECT timezone from schools where old_id = oldOOO.school_id limit 1),
                           'UTC'
                    )),
        (SELECT oldOOO.status),
        (SELECT IF(oldOOO.recurrence != 'none', oldOOO.recurrence, (SELECT NULL))),
        (SELECT IF(oldOOO.week_days IS NOT NULL AND oldOOO.week_days != ' ', (SELECT ehp_v2_prod.getRecurrenceDays(oldOOO.week_days)), null)),
        (SELECT NULL),
        (SELECT IF(oldOOO.recurrence != 'none',
                   (SELECT CONVERT_TZ(oldOOO.to_datetime,
                                      (SELECT timezone from schools where old_id = oldOOO.school_id limit 1),
                                      'UTC'
                               )), NULL
                    )),
        (SELECT oldOOO.comment),
        (SELECT CONVERT_TZ(oldOOO.updated_at,
                           (SELECT timezone from schools where old_id = oldOOO.school_id limit 1),
                           'UTC'
                    )),
        (SELECT CONVERT_TZ(oldOOO.created_at,
                           (SELECT timezone from schools where old_id = oldOOO.school_id limit 1),
                           'UTC'
                    )),
        (SELECT NULL),
        (SELECT IF(DATE(oldOOO.today_status)=CURDATE(),NOW(),NULL))
    FROM ehallpassDB.unavailability as oldOOO;
    COMMIT;
    SET FOREIGN_KEY_CHECKS = 1;

    UPDATE
        ehp_v2_prod.unavailables
    SET
        unavailable_id = (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = unavailable_id
            LIMIT 1)
    WHERE
            unavailable_type = 'App\\Models\\User'
      AND unavailable_id IN(
        SELECT
            from_id FROM duplicate_users_clone);

    SET FOREIGN_KEY_CHECKS = 0;
    START TRANSACTION ;
    TRUNCATE ehp_v2_prod.transparencies;
    INSERT INTO ehp_v2_prod.transparencies (school_id, created_at, updated_at) SELECT
                                                                                   (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldPs.school_id limit 1),
                                                                                   (SELECT CONVERT_TZ(oldPs.updated_at,
                                                                                                      (SELECT timezone from ehp_v2_prod.schools where old_id = oldPs.school_id limit 1),
                                                                                                      'UTC'
                                                                                               )),
                                                                                   (SELECT CONVERT_TZ(oldPs.created_at,
                                                                                                      (SELECT timezone from ehp_v2_prod.schools where old_id = oldPs.school_id limit 1),
                                                                                                      'UTC'
                                                                                               ))
    FROM ehallpassDB.privacy_setting as oldPs;
    COMMIT ;
    SET FOREIGN_KEY_CHECKS = 1;

    START TRANSACTION ;
    SET foreign_key_checks =0;
    START TRANSACTION ;
    truncate ehp_v2_prod.auto_passes;
    INSERT INTO ehp_v2_prod.auto_passes (room_id,school_id,created_at,updated_at) SELECT
                                                                                      (SELECT id FROM ehp_v2_prod.rooms WHERE old_id=oldAp.room_id limit 1),
                                                                                      (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldAp.school_id limit 1),
                                                                                      (SELECT CONVERT_TZ(oldAp.created_at, 'America/New_York', 'UTC')),
                                                                                      (SELECT CONVERT_TZ(oldAp.updated_at, 'America/New_York', 'UTC'))
    FROM ehallpassDB.auto_pass as oldAp;
    COMMIT ;

    START TRANSACTION ;
    SET FOREIGN_KEY_CHECKS = 0;
    TRUNCATE ehp_v2_prod.kiosks;
    INSERT INTO ehp_v2_prod.kiosks (`room_id`, `user_id`, `school_id`, `status`, `password`, `password_use`, `created_at`, `updated_at`, `expired_date`)
    SELECT
        (SELECT IF(oldKiosk.room_id = 0, NULL,(SELECT id FROM rooms where old_id = oldKiosk.room_id limit 1))) as room_id,
        (SELECT IF(oldKiosk.teacher_id = 0,NULL,(SELECT id FROM users where old_id = oldKiosk.teacher_id limit 1))) as user_id,
        (SELECT id FROM schools where old_id = oldKiosk.school_id limit 1) as school_id,
        (SELECT oldKiosk.status),
        (SELECT oldKiosk.password),
        (SELECT CONVERT_TZ(oldKiosk.password_use,
                           (SELECT timezone from schools where old_id = oldKiosk.school_id limit 1),
                           'UTC'
                    )) as password_use,
        (SELECT CONVERT_TZ(oldKiosk.created_at,
                           (SELECT timezone from schools where old_id = oldKiosk.school_id limit 1),
                           'UTC'
                    )) as created_at,
        (SELECT CONVERT_TZ(oldKiosk.updated_at,
                           (SELECT timezone from schools where old_id = oldKiosk.school_id limit 1),
                           'UTC'
                    )) as updated_at,
        (SELECT CONVERT_TZ(oldKiosk.expired_date,
                           (SELECT timezone from schools where old_id = oldKiosk.school_id limit 1),
                           'UTC'
                    )) as expired_date
    FROM ehallpassDB.kiosk as oldKiosk;
    COMMIT;

    START TRANSACTION ;
    UPDATE
        ehp_v2_prod.kiosks
    SET
        user_id = (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = kiosks.user_id
            LIMIT 1)
    WHERE user_id IN(
        SELECT
            from_id FROM duplicate_users_clone);
    COMMIT;

    SET FOREIGN_KEY_CHECKS  =0;
    START TRANSACTION ;
    truncate ehp_v2_prod.teacher_pass_settings;
    INSERT INTO ehp_v2_prod.teacher_pass_settings (school_id, min_time, white_passes, awaiting_apt, max_time, auto_expire_time, nurse_expire_time, mail_time, created_at, updated_at)
    SELECT
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldTps.school_id limit 1),
        (SELECT oldTps.min_time),
        (SELECT oldTps.white_passes),
        (SELECT oldTps.awaiting_apt),
        (SELECT oldTps.max_time),
        (SELECT oldTps.auto_expire_time),
        (SELECT oldTps.nurse_auto_expire_time),
        (SELECT oldTps.mail_time),
        (SELECT CONVERT_TZ(oldTps.created_at, 'America/New_York', 'UTC')),
        (SELECT CONVERT_TZ(oldTps.updated_at, 'America/New_York', 'UTC'))
    FROM ehallpassDB.teacher_pass_settings as oldTps;

    COMMIT ;

    START TRANSACTION;
    UPDATE ehp_v2_prod.teacher_pass_settings as tchP, ehallpassDB.ab_polarity_settings as oldPS
    SET tchP.polarity_message = oldPS.message_for_student
    WHERE tchP.school_id = (SELECT id FROM schools WHERE old_id = oldPS.school_id limit 1);
    COMMIT;
    SET FOREIGN_KEY_CHECKS=1;


    SET FOREIGN_KEY_CHECKS = 0;
    START TRANSACTION ;
    truncate ehp_v2_prod.transparency_users;
    INSERT INTO ehp_v2_prod.transparency_users (school_id, user_id, created_at, updated_at) SELECT
                                                                                                (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldPs.school_id limit 1),
                                                                                                IF((SELECT id FROM ehp_v2_prod.users WHERE old_id=oldPs.teacher_id limit 1) IS NOT NULL,(SELECT id FROM ehp_v2_prod.users WHERE old_id=oldPs.teacher_id limit 1),0),
                                                                                                (SELECT CONVERT_TZ(oldPs.updated_at,
                                                                                                                   (SELECT timezone from ehp_v2_prod.schools where old_id = oldPs.school_id limit 1),
                                                                                                                   'UTC'
                                                                                                            )),
                                                                                                (SELECT CONVERT_TZ(oldPs.created_at,
                                                                                                                   (SELECT timezone from ehp_v2_prod.schools where old_id = oldPs.school_id limit 1),
                                                                                                                   'UTC'
                                                                                                            ))
    FROM ehallpassDB.privacy_setting_teacher as oldPs;
    COMMIT ;
    SET FOREIGN_KEY_CHECKS = 1;

    START TRANSACTION ;
    UPDATE
        ehp_v2_prod.transparency_users
    SET
        user_id = (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = transparency_users.user_id
            LIMIT 1)
    WHERE user_id IN(
        SELECT
            from_id FROM duplicate_users_clone);
    COMMIT;

    SET FOREIGN_KEY_CHECKS = 0;
    START TRANSACTION ;
    INSERT INTO ehp_v2_prod.student_favorites (position, favorable_type, favorable_id, user_id, school_id, created_at, updated_at, global, section) SELECT
                                                                                                                                                        (SELECT NULL),
                                                                                                                                                        (SELECT 'App\\Models\\Room'),
                                                                                                                                                        (SELECT id FROM ehp_v2_prod.rooms WHERE old_id = oldR.room_id limit 1),
                                                                                                                                                        (SELECT NULL),
                                                                                                                                                        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldR.school_id limit 1),
                                                                                                                                                        (SELECT NOW()),
                                                                                                                                                        (SELECT NOW()),
                                                                                                                                                        (SELECT 1),
                                                                                                                                                        (SELECT 'destination')
    FROM ehallpassDB.rooms as oldR WHERE oldR.room_id > last_room_id;
    COMMIT ;
    SET FOREIGN_KEY_CHECKS = 1;

    UPDATE ehp_v2_prod.rooms SET admin_favorite = 0;
    UPDATE ehp_v2_prod.rooms SET admin_favorite = 1
    WHERE id IN (SELECT favorable_id FROM ehp_v2_prod.student_favorites WHERE favorable_type = 'App\\Models\\Room' AND global = 1);

    SET FOREIGN_KEY_CHECKS = 0;
    START TRANSACTION ;
    TRUNCATE ehp_v2_prod.nurse_rooms;
    INSERT INTO ehp_v2_prod.nurse_rooms (room_id, school_id, end_time, created_at, updated_at)
    SELECT
        (SELECT id FROM ehp_v2_prod.rooms WHERE old_id=oldNr.room_id limit 1),
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldNr.school_id limit 1),
        (SELECT oldNr.end_time),
        (SELECT NOW()),
        (SELECT NOW())
    FROM ehallpassDB.nurse_rooms as oldNr;
    COMMIT ;
    SET FOREIGN_KEY_CHECKS = 1;

    SET FOREIGN_KEY_CHECKS =0;
    TRUNCATE ehp_v2_prod.pass_limits;
    INSERT INTO ehp_v2_prod.pass_limits (limitable_type, limitable_id, `limit`, from_date, to_date, grade_year, reason, recurrence_type, archived, created_at, updated_at, school_id) SELECT
                                                                                                                                                                                          (SELECT CASE
                                                                                                                                                                                                      WHEN oldPl.type = 'school' THEN ('App\\Models\\School')
                                                                                                                                                                                                      WHEN oldPl.type = 'student' THEN ('App\\Models\\User')
                                                                                                                                                                                                      WHEN oldPl.type = 'gradyear' THEN ('App\\Models\\School')
                                                                                                                                                                                                      END),
                                                                                                                                                                                          (SELECT CASE
                                                                                                                                                                                                      WHEN oldPl.type = 'school' THEN (SELECT id FROM schools WHERE old_id=oldPl.school_id limit 1)
                                                                                                                                                                                                      WHEN oldPl.type = 'student' THEN (SELECT id FROM users WHERE old_id=oldPl.student_id limit 1)
                                                                                                                                                                                                      WHEN oldPl.type = 'gradyear' THEN (SELECT id FROM schools WHERE old_id=oldPl.school_id limit 1)
                                                                                                                                                                                                      END),
                                                                                                                                                                                          (SELECT oldPl.limit),
                                                                                                                                                                                          (SELECT oldPl.`from`),
                                                                                                                                                                                          (SELECT oldPl.`to`),
                                                                                                                                                                                          (SELECT IF(oldPl.type='gradyear',JSON_ARRAY(oldPl.gradyear),null)),
                                                                                                                                                                                          (SELECT oldPl.reason),
                                                                                                                                                                                          (SELECT oldPl.`recursive`),
                                                                                                                                                                                          (SELECT 0),
                                                                                                                                                                                          (SELECT CONVERT_TZ(oldPl.created_at,
                                                                                                                                                                                                             (SELECT timezone from schools where old_id = oldPl.school_id limit 1),
                                                                                                                                                                                                             'UTC'
                                                                                                                                                                                                      )) as created_at,
                                                                                                                                                                                          (SELECT NULL),
                                                                                                                                                                                          (SELECT id FROM schools WHERE old_id=oldPl.school_id limit 1)

    FROM ehallpassDB.pass_limits as oldPl;
    UPDATE
        pass_limits
    SET
        limitable_id = (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = pass_limits.limitable_id
            LIMIT 1)
    WHERE
            pass_limits.limitable_type = 'App\\Models\\User'
      AND limitable_id IN(
        SELECT
            from_id FROM duplicate_users_clone);
    SET FOREIGN_KEY_CHECKS=1;

    SET FOREIGN_KEY_CHECKS  = 0;
    START TRANSACTION ;
    TRUNCATE ehp_v2_prod.auto_passes_preferences;
    INSERT INTO ehp_v2_prod.auto_passes_preferences (auto_pass_id, user_id, school_id, status, mode, created_at, updated_at)  SELECT
                                                                                                                                  (SELECT id FROM ehp_v2_prod.auto_passes WHERE auto_passes.room_id = (SELECT id FROM ehp_v2_prod.rooms WHERE old_id = oldAp.room_id limit 1) LIMIT 1),
                                                                                                                                  (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldAp.teacher_id limit 1),
                                                                                                                                  (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldAp.school_id limit 1),
                                                                                                                                  (SELECT oldAp.status),
                                                                                                                                  (SELECT IF(oldAp.mode >= 1,'start_and_stop', 'start_only')),
                                                                                                                                  (SELECT CONVERT_TZ(oldAp.created_at,'America/New_York', 'UTC')),
                                                                                                                                  (SELECT CONVERT_TZ(oldAp.updated_at,'America/New_York', 'UTC'))
    FROM ehallpassDB.auto_pass_preference as oldAp;


    UPDATE
        ehp_v2_prod.auto_passes_preferences
    SET
        user_id = (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = user_id
            LIMIT 1)
    WHERE
            user_id IN(
            SELECT
                from_id FROM duplicate_users_clone);
    SET FOREIGN_KEY_CHECKS  = 0;

    START TRANSACTION ;
    truncate ehp_v2_prod.auto_passes_limits;
    INSERT INTO ehp_v2_prod.auto_passes_limits (user_id, school_id, `limit`, created_at, updated_at)  SELECT
                                                                                                          (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldAp.teacher_id limit 1),
                                                                                                          (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldAp.school_id limit 1),
                                                                                                          (SELECT oldAp.`limit`),
                                                                                                          (SELECT NOW()),
                                                                                                          (SELECT NOW())
    FROM ehallpassDB.auto_pass_limits as oldAp;
    SET FOREIGN_KEY_CHECKS  = 1;
    UPDATE
        ehp_v2_prod.auto_passes_limits
    SET
        user_id = (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = user_id
            LIMIT 1)
    WHERE
            user_id IN(
            SELECT
                from_id FROM duplicate_users_clone);




    SET FOREIGN_KEY_CHECKS  = 0;
    START TRANSACTION ;
    TRUNCATE ehp_v2_prod.staff_schedules;
    INSERT INTO ehp_v2_prod.staff_schedules (room_id, user_id, school_id, status, created_at, updated_at) SELECT
                                                                                                              (SELECT id FROM ehp_v2_prod.rooms WHERE old_id=oldSs.room_id limit 1),
                                                                                                              (SELECT id FROM ehp_v2_prod.users WHERE old_id=oldSs.staff_id limit 1),
                                                                                                              (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldSs.school_id limit 1),
                                                                                                              (SELECT oldSs.status),
                                                                                                              (SELECT CONVERT_TZ(oldSs.created_at, 'America/New_York', 'UTC')),
                                                                                                              (SELECT CONVERT_TZ(oldSs.updated_at, 'America/New_York', 'UTC'))
    FROM ehallpassDB.staff_schedule as oldSs;

    COMMIT ;

    SET FOREIGN_KEY_CHECKS  = 1;

    UPDATE
        ehp_v2_prod.staff_schedules
    SET
        user_id = (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = user_id
            LIMIT 1)
    WHERE
            user_id IN(
            SELECT
                from_id FROM duplicate_users_clone);

    SET FOREIGN_KEY_CHECKS  = 0;
    TRUNCATE ehp_v2_prod.periods;
    INSERT INTO ehp_v2_prod.periods (school_id, name, `order`, status, created_at, updated_at, deleted, old_id)
    SELECT
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldPeriod.school_id limit 1),
        (SELECT oldPeriod.period_name),
        (SELECT oldPeriod.ordering),
        (SELECT IF(oldPeriod.status <= 0, 0, 1)),
        (SELECT CONVERT_TZ(oldPeriod.created_at, 'America/New_York', 'UTC')),
        (SELECT CONVERT_TZ(oldPeriod.updated_at, 'America/New_York', 'UTC')),
        (SELECT 0),
        (SELECT oldPeriod.id)
    FROM ehallpassDB.school_period as oldPeriod;
    SET FOREIGN_KEY_CHECKS  = 1;


    SET FOREIGN_KEY_CHECKS  = 0;
    START TRANSACTION ;
    TRUNCATE ehp_v2_prod.pins;
    INSERT INTO ehp_v2_prod.pins (school_id, pinnable_type, pinnable_id, pin, created_at, updated_at)
    SELECT
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldTp.school_id limit 1),
        (SELECT 'App\\Models\\User'),
        (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldTp.teacher_id limit 1),
        (SELECT oldTp.pin),
        (SELECT CONVERT_TZ(oldTp.created_at, 'America/New_York', 'UTC')),
        (SELECT CONVERT_TZ(oldTp.updated_at, 'America/New_York', 'UTC'))
    FROM ehallpassDB.teacher_pin as oldTp;

    INSERT INTO ehp_v2_prod.pins (school_id, pinnable_type, pinnable_id, pin, created_at, updated_at)
    SELECT
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldTp.school_id limit 1),
        (SELECT 'App\\Models\\Room'),
        (SELECT id FROM ehp_v2_prod.rooms WHERE old_id = oldTp.room_id limit 1),
        (SELECT oldTp.pin),
        (SELECT CONVERT_TZ(oldTp.created_at, 'America/New_York', 'UTC')),
        (SELECT CONVERT_TZ(oldTp.updated_at, 'America/New_York', 'UTC'))
    FROM ehallpassDB.room_pins as oldTp;

    COMMIT ;

    UPDATE
        ehp_v2_prod.pins
    SET
        pinnable_id = (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = pinnable_id
            LIMIT 1)
    WHERE
            pins.pinnable_type = 'App\\Models\\User'
      AND pinnable_id IN(
        SELECT
            from_id FROM duplicate_users_clone);

    SET FOREIGN_KEY_CHECKS  = 0;
    START TRANSACTION ;
    TRUNCATE ehp_v2_prod.active_pass_limits;
    INSERT INTO ehp_v2_prod.active_pass_limits ( student_pass, kiosk_pass, proxy_pass, teacher_override, `limit`, school_id, created_at, updated_at)  SELECT
                                                                                                                                                          (SELECT oldApl.stu_status),
                                                                                                                                                          (SELECT oldApl.ksk_status),
                                                                                                                                                          (SELECT IF(oldApl.prx_status >= 1, 1 ,0)),
                                                                                                                                                          (SELECT IF(oldApl.prx_status > 1, 1 ,0)),
                                                                                                                                                          (SELECT oldApl.`limit`),
                                                                                                                                                          (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldApl.school_id limit 1),
                                                                                                                                                          (SELECT NOW()),
                                                                                                                                                          (SELECT NOW())
    FROM ehallpassDB.pass_limit_concurrent as oldApl;
    COMMIT;
    SET FOREIGN_KEY_CHECKS  = 1;

    SET FOREIGN_KEY_CHECKS  = 0;
    START TRANSACTION ;
    TRUNCATE ehp_v2_prod.kiosk_users;
    INSERT INTO ehp_v2_prod.kiosk_users (user_id, kpassword, password_processed, school_id, created_at, updated_at)
    SELECT
        (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldKu.user_id limit 1),
        (SELECT oldKu.kpassword),
        (SELECT oldKu.password_processed),
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldKu.school_id limit 1),
        (SELECT NOW()),
        (SELECT NOW())
    FROM ehallpassDB.kiosk_users as oldKu;
    COMMIT;
    SET FOREIGN_KEY_CHECKS  = 1;

    UPDATE
        ehp_v2_prod.kiosk_users
    SET
        user_id = (
            SELECT
                to_id
            FROM
                duplicate_users_clone
            WHERE
                    from_id = user_id
            LIMIT 1)
    WHERE
            user_id IN(
            SELECT
                from_id FROM duplicate_users_clone);

END
