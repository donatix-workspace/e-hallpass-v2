<?php

namespace App\Http\Controllers\Admin\Passblock;

use App\Http\Controllers\Controller;
use App\Http\Requests\LocationCapacityRequest;
use App\Http\Resources\LocationCapacityCollection;
use App\Http\Resources\LocationCapacityResource;
use App\Models\LocationCapacity;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Location;


/**
 * @group Location Capacities manage.
 */
class LocationCapacityController extends Controller
{
    /**
     * Get all location capacities record.
     *
     * @apiResourceModel App\Models\LocationCapacity
     * @apiResourceCollection  App\Http\Resources\LocationCapacityCollection
     * @return LocationCapacityCollection
     * @authenticated
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $user = auth()->user();

        $this->authorize('viewAny', LocationCapacity::class);

        if (request()->has('search_query') || request()->has('sort')) {
            $locationCapacities = collect(LocationCapacity::search(request()->get('search_query') === null ? config('scout_elastic.all_records') : convertElasticQuery(request()->get('search_query'), request()->has('convert_int')))
                ->with(['room:id,name'])
                ->where('school_id', $user->school_id)
                ->when(request()->has('sort'), function ($query) {
                    $sort = explode(':', request()->get('sort'));
                    if (count($sort) <= 1) {
                        return $query->orderBy('id', 'asc');
                    }

                    $sortingKeywords = ['asc', 'desc'];
                    if (in_array($sort[1], $sortingKeywords)) {
                        return $query->orderBy($sort[0], $sort[1]);
                    }
                    return $query->orderBy('id', 'asc');
                })
                ->take(LocationCapacity::recordsPerPage())->get())
                ->pluck('id')
                ->toArray();

            $locationCapacitiesIds = implode(',', $locationCapacities);
            $locationCapacities = LocationCapacity::whereIntegerInRaw('id', $locationCapacities)
                ->orderByRaw("field(id,$locationCapacitiesIds)")
                ->fromSchoolId($user->school_id)
                ->paginate(LocationCapacity::recordsPerPage());
        } else {
            $locationCapacities = LocationCapacity::with('room:id,name')
                ->fromSchoolId($user->school_id)
                ->paginate(LocationCapacity::recordsPerPage());
        }

        return new LocationCapacityCollection($locationCapacities);
    }

    /**
     * View a location capacity record.
     *
     * @urlParam locationCapacity integer required The id of the location capacity.
     *
     * @authenticated
     * @param LocationCapacity $locationCapacity
     * @return LocationCapacityResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(LocationCapacity $locationCapacity)
    {
        $this->authorize('view', $locationCapacity);

        return new LocationCapacityResource($locationCapacity);
    }

    /**
     * Create a location capacity limit.
     *
     * @bodyParam limit integer required The location capacity limit
     * @bodyParam room_id integer required The id of the location (Room).
     * @apiResourceModel App\Models\LocationCapacity
     * @apiResource App\Http\Resources\LocationCapacityResource
     *
     * @authenticated
     * @param LocationCapacityRequest $request
     * @return LocationCapacityResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(LocationCapacityRequest $request)
    {
        $user = auth()->user();

        $this->authorize('create', LocationCapacity::class);

        $locationCapacity = LocationCapacity::create([
            'limit' => $request->input('limit') === null ? -1 : $request->input('limit'),
            'school_id' => $user->school_id,
            'status' => 1,
            'room_id' => $request->input('room_id')
        ]);

        return new LocationCapacityResource($locationCapacity);
    }

    /**
     * Update an existing location capacity limit.
     *
     * @urlParam locationCapacity integer required The id of the location capacity limit
     * @bodyParam limit integer required The new location limit.
     *
     * @authenticated
     * @apiResourceModel App\Models\LocationCapacity
     * @apiResource App\Http\Resources\LocationCapacityResource
     * @param Request $request
     * @param LocationCapacity $locationCapacity
     * @return LocationCapacityResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, LocationCapacity $locationCapacity)
    {
        $this->authorize('update', $locationCapacity);

        $request->validate(['limit' => 'integer|min:-1|nullable|sometimes']);

        $locationCapacity->update([
            'limit' => $request->input('limit') === null ? -1 : $request->input('limit')
        ]);

        return new LocationCapacityResource($locationCapacity->refresh());
    }

    /**
     * Delete a location capacity limitation.
     *
     * @apiResourceModel App\Models\LocationCapacity
     * @apiResource App\Http\Resources\LocationCapacityResource
     * @urlParam locationCapacity integer required The id of the location capacity limit
     * @authenticated
     * @param LocationCapacity $locationCapacity
     * @return LocationCapacityResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function delete(LocationCapacity $locationCapacity)
    {
        $this->authorize('delete', $locationCapacity);
        $locationCapacity->delete();

        return new LocationCapacityResource($locationCapacity);
    }
}
