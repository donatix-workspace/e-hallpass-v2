<?php

namespace App\Listeners;

use App\Events\PassReturning;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PassReturningNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PassReturning  $event
     * @return void
     */
    public function handle(PassReturning $event)
    {
        //
    }
}
