<?php

namespace App\Imports;

use App\Models\Pass;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ContactTracingImport implements FromQuery, WithHeadings, WithMapping
{
    public $userId;
    private $contactTracingQuery;

    /**
     * ContactTracingImport constructor.
     * @param $userId
     * @param $contactTracingQuery
     */
    public function __construct($userId, $contactTracingQuery)
    {
        $this->userId = $userId;

        $this->contactTracingQuery = $contactTracingQuery;
    }

    /**
     * @return string[]
     */
    public function headings(): array
    {
        return [
            'Last Name',
            'First Name',
            'Email',
            'Number of times passes overlapped',
            'Did they share a location?'
        ];
    }

    /**
     * @param $row
     * @return array
     */
    public function map($row): array
    {
        return [
            $row->last_name,
            $row->first_name,
            $row->email,
            $row->overlap_time_count,
            $row->share_same_location ? 'Y' : 'N'
        ];
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return $this->contactTracingQuery;
    }
}
