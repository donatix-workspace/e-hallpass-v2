<?php

return [
    'client' => [
        'hosts' => [env('SCOUT_ELASTIC_HOST', 'localhost')]
    ],
    'update_mapping' => env('SCOUT_ELASTIC_UPDATE_MAPPING', true),
    'indexer' => env('SCOUT_ELASTIC_INDEXER', 'single'),
    'document_refresh' => env('SCOUT_ELASTIC_DOCUMENT_REFRESH'),
    'es_indexes' => [
        'appointment_passes_index' => [
            'configurator' =>
                'App\\Elastic\\AppointmentPassesIndexConfigurator',
            'model' => 'App\\Models\\AppointmentPass'
        ],
        'passes_index' => [
            'configurator' => 'App\\Elastic\\PassesIndexConfigurator',
            'model' => 'App\\Models\\Pass'
        ],
        'rooms_index' => [
            'configurator' => 'App\\Elastic\\RoomIndexConfigurator',
            'model' => 'App\\Models\\Room'
        ],
        'users_index' => [
            'configurator' => 'App\\Elastic\\UserIndexConfigurator',
            'model' => 'App\\Models\\User'
        ],
        'unavailables' => [
            'configurator' => 'App\\Elastic\\UnavailablesIndexConfigurator',
            'model' => 'App\\Models\\Unavailable'
        ],
        'transparencies_index' => [
            'configurator' => 'App\\Elastic\\TransparenciesIndexConfigurator',
            'model' => 'App\\Models\\TransparencyUser'
        ],
        'pass_limits' => [
            'configurator' => 'App\\Elastic\\PassLimitsIndexConfigurator',
            'model' => 'App\\Models\\PassLimit'
        ],
        'location_capacities' => [
            'configurator' =>
                'App\\Elastic\\LocationCapacitiesIndexConfigurator',
            'model' => 'App\\Models\\LocationCapacity'
        ],
        'room_restrictions' => [
            'configurator' => 'App\\Elastic\\RoomRestrictionsIndexConfigurator',
            'model' => 'App\\Models\\RoomRestriction'
        ],
        'polarities' => [
            'configurator' => \App\Elastic\PolaritiesIndexConfigurator::class,
            'model' => \App\Models\Polarity::class
        ],
        'polarity_reports' => [
            'configurator' =>
                \App\Elastic\PolarityReportsIndexConfigurator::class,
            'model' => \App\Models\PolarityReport::class
        ],
        'summary_report' => [
            'configurator' =>
                \App\Elastic\SummaryReportIndexConfigurator::class,
            'model' => \App\Models\PassSummaryReport::class
        ],
        'pass_block' => [
            'configurator' => \App\Elastic\PassBlockIndexConfigurator::class,
            'model' => \App\Models\PassBlock::class
        ],
        'recurrence_appointment_passes' => [
            'configurator' =>
                \App\Elastic\RecurrenceAppointmentPassesIndexConfigurator::class,
            'model' => \App\Models\RecurrenceAppointmentPass::class
        ],
        'contact_tracing' => [
            'configurator' =>
                \App\Elastic\ContactTracingIndexConfigurator::class,
            'model' => \App\Models\ContactTracing::class
        ]
    ],

    'all_records' => '*'
];
