<?php

namespace App\Console\Commands\Migrations;

use DB;
use Illuminate\Console\Command;

class LinkUsersToSchoolsCommand extends Command
{
    protected $signature = 'migrate_sys:link_users_to_school';

    protected $description = 'Import Users from EHpV1 to EHpV2';

    public function handle()
    {
        $this->info('--- Linking users to school --- ');
        $this->output->progressStart(DB::table('users')->count());

        $users = DB::table('users')
            ->orderBy('id', 'desc')
            ->select(['id', 'school_id', 'role_id'])
            ->get()
            ->toArray();

        $usersChunk = array_chunk($users, 10000);

        foreach ($usersChunk as $userChunk) {
            $userInsertable = [];
            foreach ($userChunk as $user) {
                $userInsertable[] = [
                    'school_id' => $user->school_id,
                    'user_id' => $user->id,
                    'role_id' => $user->role_id
                ];
            }

            DB::table('schools_users')->insert($userInsertable);
            $this->output->progressAdvance(count($userInsertable));
        }

        sleep(2);
        $this->output->progressFinish();
    }
}
