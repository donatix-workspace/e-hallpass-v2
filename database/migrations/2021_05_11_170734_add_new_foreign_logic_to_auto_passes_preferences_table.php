<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewForeignLogicToAutoPassesPreferencesTable extends Migration
{
    public function up()
    {
        Schema::table('auto_passes_preferences', function (Blueprint $table) {
            $table->dropForeign(['auto_pass_id']);
            $table->foreign('auto_pass_id')
                ->references('id')
                ->on('auto_passes')
                ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::table('auto_passes_preferences', function (Blueprint $table) {
            $table->dropForeign(['auto_pass_id']);
        });
    }
}
