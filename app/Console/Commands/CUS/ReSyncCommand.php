<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\EntityService;
use App\Http\Services\CommonUserService\Models\CommonDistrict;
use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonSchoolDomain;
use App\Http\Services\CommonUserService\Models\CommonTimezone;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Models\SchoolsEhp;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CancelAndUpdateAppointmentsAfterArchiveJob;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\AuthType;
use App\Models\Domain;
use App\Models\Module;
use App\Models\Role;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\SchoolUser;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ReSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:resync {--allSchools} {--syncWithCUS=} {--deleteInCUS} {--fromId=}
    {--toId=} {--syncWithV2=} {--full=} {--memberships=} {--onchange=} {--action=} {--syncStatuses=} {--deleteMembership=} {--deleteForSchool=} {--archived=} {--dupOldIds=} {--missingInCus=} {--roleId=} {--deleteExtra} {--updateOldAndSis=} {--updateEmails} {--migrateNewUsers=} {--studentNumber=} {--updateStudentNumber=} {--clear=} {--cus=} {--schoolId=} {--userId=} {--memId=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resync users per school from v1 to v2';

    protected $client;
    protected $oldDb;
    protected $adminUser;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->oldDb = DB::connection('oldDB');

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->adminUser = User::where('email', 'viktor.lalev@donatix.net')
            ->latest()
            ->first();
        $this->client = new CommonUserService($this->adminUser);

//        if ($this->option('schoolId')) {
//            $this->info('Resync users for school');
//            $this->fixUsersForSchool($this->option('schoolId'));
//        }
        if ($this->option('allSchools')) {
            $this->info('Fix School Modules For All Schools');
            $schools = School::select('id')
                ->whereNotNull('cus_uuid')
                ->where('status', 1)
                ->get();
            foreach ($schools as $school) {
                $this->fixUsersForSchool($school->id);
            }
        }
        if ($this->option('syncWithCUS')) {
            $this->info('Sync Memberships with CUS');

            $from = $this->option('fromId', 0);
            $to = $this->option('toId', 3000);

            if ($this->option('syncWithCUS') == 'all') {
                $this->info("Resync from {$from} to {$to}");

                $schools = School::where('id', '>', $from)
                    ->where('id', '<=', $to)
                    ->where('status', 1)
                    ->whereNotNull('cus_uuid')
                    ->whereNotNull('old_id')
                    ->whereNotNull('common_id')
                    ->get();
            } else {
                $schools = School::where('id', $this->option('syncWithCUS'))
                    ->whereNotNull('cus_uuid')
                    ->whereNotNull('old_id')
                    ->whereNotNull('common_id')
                    ->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Resync for {$school->name} with id {$school->id}");
                $this->syncWithCUS($school);
            }


        }
        if ($this->option('syncWithV2')) {
            $this->info('Sync Memberships with V2');

            $from = $this->option('fromId', 0);
            $to = $this->option('toId', 3000);

            if ($this->option('syncWithV2') == 'all') {
                $this->info("Resync from {$from} to {$to}");

                $schools = School::where('id', '>', $from)
                    ->where('id', '<=', $to)
                    ->whereNotNull('cus_uuid')
                    ->whereNotNull('old_id')
                    ->whereNotNull('common_id')
                    ->get();
            } else {
                $schools = School::where('id', $this->option('syncWithV2'))
                    ->whereNotNull('cus_uuid')
                    ->whereNotNull('old_id')
                    ->whereNotNull('common_id')
                    ->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Resync for {$school->name} with id {$school->id}");
                $this->syncWithV2($school);
            }


        }
        if ($this->option('full')) {
            $this->info('Full Resync V1 -> V2 -> CUS');

            $from = $this->option('fromId', 0);
            $to = $this->option('toId', 3000);

            if ($this->option('full') == 'all') {
                $this->info("Resync from {$from} to {$to}");

                $schools = \App\Models\V1\School::where('school_id', '>', $from)
                    ->where('school_id', '<=', $to)->get();

            } elseif (strpos($this->option('full'), ",") !== false) {

                $ids = explode(',', $this->option('full'));

                $schools = \App\Models\V1\School::whereIn('school_id', $ids)->get();
            } else {
                $schools = \App\Models\V1\School::where('school_id', $this->option('full'))->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Resync for {$school->school_name} with id {$school->school_id}");
                $this->fullResync($school);
            }


        }
        if ($this->option('deleteForSchool')) {
            if (strpos($this->option('deleteForSchool'), ",") !== false) {

                $ids = explode(',', $this->option('deleteForSchool'));

                $schools = School::whereIn('id', $ids)
                    ->whereNotNull('old_id')
                    ->get();
            } else {
                $schools = School::where('id', $this->option('deleteForSchool'))
                    ->whereNotNull('old_id')
                    ->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Delete Extra Memberships for {$school->name} with id {$school->id}");
                $this->deleteExtraMembershipForSchool($school);
            }
        }

        if ($this->option('deleteMembership')) {
            $this->info('Delete memberships');


            if (strpos($this->option('deleteMembership'), ",") !== false) {

                $membershipsUUids = explode(',', $this->option('deleteMembership'));

                $memberships = DB::table('schools_users')
                    ->whereIn('membership_uuid', $membershipsUUids)
                    ->get();
            } else {
                $memberships = DB::table('schools_users')
                    ->where('membership_uuid', $this->option('deleteMembership'))
                    ->get();
            }
            foreach ($memberships as $membership) {
                if (!$membership) {
                    return false;
                }

                $this->deleteMembership($membership->membership_uuid);

            }


        }

        if ($this->option('archived')) {
            $this->info('Get Archived Users from V1 and mark them as archived in V2');


            $from = $this->option('fromId') ?? 0;
            $to = $this->option('toId') ?? 3000;

            if ($this->option('archived') == 'all') {
                $this->info("Resync from {$from} to {$to}");

                $schools = \App\Models\V1\School::where('school_id', '>', $from)
                    ->where('school_id', '<=', $to)->get();

            } elseif (strpos($this->option('full'), ",") !== false) {

                $ids = explode(',', $this->option('archived'));

                $schools = \App\Models\V1\School::whereIn('school_id', $ids)->get();
            } else {
                $schools = \App\Models\V1\School::where('school_id', $this->option('archived'))->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Resync for {$school->school_name} with id {$school->school_id}");
                $this->getArchives($school);
            }


        }

        if ($this->option('syncStatuses')) {
            $this->info('Check V2 created users with V1 - change statuses');

            $from = $this->option('fromId') ?? 0;
            $to = $this->option('toId') ?? 3000;

            if ($this->option('syncStatuses') == 'all') {
                $this->info("Status sync from {$from} to {$to}");

                $schools = \App\Models\V1\School::where('school_id', '>', $from)
                    ->where('school_id', '<=', $to)->get();

            } else {
                $schools = \App\Models\V1\School::where('school_id', $this->option('syncStatuses'))->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Status sync for {$school->school_name} with id {$school->school_id}");
                $this->syncStatuses($school);
            }


        }

        if ($this->option('memberships')) {
            $this->info('Resync Users V1 -> V2 -> CUS');

            $from = $this->option('fromId') ?? 0;
            $to = $this->option('toId') ?? 2000;

            if ($this->option('memberships') == 'all') {
                $this->info("Resync Users from {$from} to {$to}");

                $schools = \App\Models\V1\School::where('school_id', '>', $from)
                    ->where('school_id', '<=', $to)
                    ->where('status', 1)->get();

            } else {
                $schools = \App\Models\V1\School::where('school_id', $this->option('memberships'))->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Resync Users  for {$school->school_name} with id {$school->school_id}");
                $this->membershipsResync($school);
            }


        }

        if ($this->option('deleteInCUS')) {
            $this->info('Resync users for school');
            $this->deleteInCUS();
        }
        if ($this->option('onchange')) {
            $this->onchange($this->option('onchange'), $this->option('action'));
        }
        if ($this->option('updateOldAndSis')) {

            $this->info('Update sis and old id in school_users');

            $from = $this->option('fromId') ?? 0;
            $to = $this->option('toId') ?? 3000;

            if ($this->option('updateOldAndSis') == 'all') {
                $this->info("Update from {$from} to {$to}");

                $oldSchools = \App\Models\V1\School::where('school_id', '>', $from)
                    ->where('school_id', '<=', $to)
                    ->where('status', 1)
                    ->get();
            } else {
                $oldSchools = \App\Models\V1\School::where('school_id', $this->option('updateOldAndSis'))
                    ->where('status', 1)
                    ->get();
            }
            foreach ($oldSchools as $oldSchool) {
                if (!$oldSchool) {
                    return false;
                }
                $this->info("Resync for {$oldSchool->school_name} with id {$oldSchool->school_id}");
                $this->updateOldAndSis($oldSchool);
            }


        }


        if ($this->option('dupOldIds')) {

            if ($this->option('dupOldIds') == 'all') {

                $dupQuery = DB::table('users')
                    ->select('old_id')
                    ->groupBy('old_id')
                    ->havingRaw('count(old_id) > 1')
                    ->orderBy('old_id');
                $this->output->progressStart($dupQuery->count());
                $dupQuery->chunk(100, function ($oldUserIds) {
                    foreach ($oldUserIds as $oldUserId) {

                        $oldUser = \App\Models\V1\User::where('user_id', $oldUserId->old_id)->first();
                        $this->duplicatedOldIds($oldUser);
                        $this->output->progressAdvance();
                    }
                });
                exit();


            } elseif (strpos($this->option('dupOldIds'), ",") !== false) {

                $ids = explode(',', $this->option('dupOldIds'));

                $oldUsers = \App\Models\V1\User::whereIn('user_id', $ids)->get();
            } else {
                $oldUsers = \App\Models\V1\User::where('user_id', $this->option('dupOldIds'))->get();
            }
            foreach ($oldUsers as $oldUser) {
                if (!$oldUser) {
                    return false;
                }
                $this->duplicatedOldIds($oldUser);
            }


        }
        if ($this->option('missingInCus')) {
            $this->info('Missing In Cus');

            $school = School::where('id', $this->option('missingInCus'))->first();

            $this->missingInCus($school, $this->option('roleId'));
        }
        if ($this->option('migrateNewUsers')) {
            $this->info('Create new users ');

            $from = $this->option('fromId') ?? 0;
            $to = $this->option('toId') ?? 2000;

            if ($this->option('migrateNewUsers') == 'all') {
                $this->info("Create new users for schools from {$from} to {$to}");

                $schools = School::where('id', '>', $from)
                    ->where('id', '<=', $to)
                    ->where('status', 1)->get();

            } else {
                $schools = School::where('id', $this->option('migrateNewUsers'))->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Create new users for school  for {$school->name} with id {$school->id}");
                $this->migrateNewUsers($school);
            }


        }
        if ($this->option('studentNumber')) {
            $this->info('Sync student number ');

            $from = $this->option('fromId') ?? 0;
            $to = $this->option('toId') ?? 3000;

            if ($this->option('studentNumber') == 'all') {
                $this->info("Sync student number schools from {$from} to {$to}");

                $schools = School::where('id', '>', $from)
                    ->where('id', '<=', $to)
                    ->where('status', 1)->get();

            } else {
                $schools = School::where('id', $this->option('studentNumber'))->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Sync student number schools for {$school->name} with id {$school->id}");
                $this->studentNumber($school);
            }


        }
        if ($this->option('updateStudentNumber')) {
            $this->info('Sync student number to CUS');

            $from = $this->option('fromId') ?? 0;
            $to = $this->option('toId') ?? 3000;

            if ($this->option('updateStudentNumber') == 'all') {
                $this->info("Sync student number schools from {$from} to {$to}");

                $schools = School::where('id', '>', $from)
                    ->where('id', '<=', $to)
                    ->where('status', 1)->get();

            } else {
                $schools = School::where('id', $this->option('updateStudentNumber'))->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Sync student number schools for {$school->name} with id {$school->id}");
                $this->updateStudentNumber($school);
            }


        }
        if ($this->option('clear')) {
            $this->info('Clear');

            $from = $this->option('fromId') ?? 0;
            $to = $this->option('toId') ?? 3000;

            if ($this->option('clear') == 'all') {
                $this->info("Clear schools from {$from} to {$to}");

                $schools = School::where('id', '>', $from)
                    ->where('id', '<=', $to)
                    ->whereNotNull('old_id')
                    ->where('status', 1)->get();

            } else {
                $schools = School::where('id', $this->option('clear'))->whereNotNull('old_id')->get();
            }
            foreach ($schools as $school) {
                if (!$school) {
                    return false;
                }
                $this->info("Clear for {$school->name} with id {$school->id}");
                $this->clear($school);
            }


        }
        if ($this->option('cus')) {

            $schoolId = $this->option('schoolId') ?? null;
            $userId = $this->option('userId') ?? null;
            $membershipId = $this->option('memId') ?? null;

            switch ($this->option('cus')) {
                case 'create':
                    return $this->createMembership($schoolId, $userId, $membershipId);
                case 'update':
                    return $this->updateMembership($schoolId, $userId, $membershipId);
                case 'delete':
                    return $this->deleteWrongMembership($schoolId, $userId, $membershipId);
                case 'old':
                    return $this->syncOld($userId);
            }
        }
    }

    protected function syncOld($oldUserId)
    {
        $oldUser = \App\Models\V1\User::where('user_id', $oldUserId)->first();

        if ($oldUser) {
            return $oldUser->sync();
        }
    }

    protected function createMembership($schoolId, $userId, $membershipId)
    {
        $user = User::where('id', $userId)->first();
        $school = School::where('id', $schoolId)->first();
        if (!$membershipId) {
            $membership = DB::table('schools_users')
                ->where('school_id', $schoolId)
                ->where('user_id', $userId)
                ->orderBy('id', 'desc')
                ->first();
        } else {
            $membership = DB::table('schools_users')
                ->where('id', $membershipId)
                ->first();
        }

        return $this->createCusMembership($user, $school, $membership->role_id, $membership->status, $membership);
    }

    protected function updateMembership($schoolId, $userId, $membershipId)
    {
        $user = User::where('id', $userId)->first();
        $school = School::where('id', $schoolId)->first();
        if (!$membershipId) {
            $membership = DB::table('schools_users')
                ->where('school_id', $schoolId)
                ->where('user_id', $userId)
                ->orderBy('id', 'desc')
                ->first();
        } else {
            $membership = DB::table('schools_users')
                ->where('id', $membershipId)
                ->first();
        }
        $commonMembership = CommonMembership::where('uuid', $membership->membership_uuid)->first();


        return $this->updateCusMembership($membership, $user, $school, $membership->role_id, $membership->status, $commonMembership);
    }

    protected function deleteWrongMembership($schoolId, $userId, $membershipId)
    {
        if (!$membershipId) {
            $membership = DB::table('schools_users')
                ->where('school_id', $schoolId)
                ->where('user_id', $userId)
                ->orderBy('id', 'desc')
                ->first();
        } else {
            $membership = DB::table('schools_users')
                ->where('id', $membershipId)
                ->first();
        }

        return $this->deleteMembership($membership->membership_uuid);
    }

    public function studentNumber($school)
    {
        $oldSchool = \App\Models\V1\School::where('school_id', $school->old_id)->first();

        if (!$oldSchool) {
            return true;
        }

        $isCleverSchool = $this->oldDb->table('schools_clever')
            ->where('ehp_school_id', $school->old_id)
            ->exists();
//        $isClassLinkSchool = $this->oldDb->table('schools_classlink')
//            ->where('ehp_school_id', $school->old_id)
//            ->exists();

        if (!$isCleverSchool) {
            return true;
        }

        $memberships = DB::table('schools_users')
            ->where('school_id', $school->id)
            ->whereNull('deleted_at')
            ->whereNull('student_number')
            ->whereNotNull('old_id')
            ->where('role_id', 1);

        $this->output->progressStart($memberships->count());

        foreach ($memberships->get() as $membership) {
            $oldUser = \App\Models\V1\User::where('user_id', $membership->old_id)
                ->whereNotNull('student_sis_id')
                ->whereNotIn('student_sis_id', ['', '0'])
                ->first();

            if ($oldUser) {
                dd($oldUser);
                $sisId = optional(explode('@', $oldUser->email))[0];

                DB::table('schools_users')
                    ->where('id', $membership->id)
                    ->update([
                        'student_number' => $oldUser->student_sis_id,
                        'student_sis_id' => $sisId,
                        'updated_at' => now()
                    ]);

            }
            $this->output->progressAdvance();
        }
    }

    public function updateStudentNumber($school)
    {

        $memberships = DB::table('schools_users')
            ->where('school_id', $school->id)
            ->whereNull('deleted_at')
            ->whereNotNull('student_number')
            ->whereNotIn('id', [1867549, 1880837])
            ->where('role_id', 1);

        $this->output->progressStart($memberships->count());

        foreach ($memberships->get() as $membership) {

            $user = User::where('id', $membership->user_id)->first();
            if ($user) {

                $commonMembershipNotArchived = CommonMembership::where('auth_client_id', 2)
                    ->where('uuid', $membership->membership_uuid)
                    ->where('not_archived', 1)
                    ->orderBy('id', 'desc')
                    ->first();

                $commonMembership = CommonMembership::where('auth_client_id', 2)
                    ->where('common_bm_school_id', $school->common_id)
                    ->where('sis_id', $membership->student_sis_id)
                    ->where('not_archived', 1)
                    ->orderBy('id', 'desc')
                    ->first();

                if (!$commonMembershipNotArchived) {
                    if ($commonMembership) {

                        DB::table('schools_users')
                            ->where('id', $membership->id)
                            ->update([
                                'membership_uuid' => $commonMembership->uuid,
                                'status' => $commonMembership->r_status,
                                'archived_at' => $commonMembership->archived_at,
                            ]);

                    } else {
                        $this->createCusMembership($user, $school, $membership->role_id, $membership->status, $membership);
                    }
                } else {
                    if ($commonMembership) {

                        $this->client->deleteUser($commonMembership->uuid);
                    }
                    $this->client->deleteUser($membership->membership_uuid);
                    $this->createCusMembership($user, $school, $membership->role_id, $membership->status, $membership);
                }

            }

            $this->output->progressAdvance();
        }
    }

    protected function createCusMembership($user, $school, $roleId, $status, $membership = null)
    {
        $client = new CommonUserService($this->adminUser);

        $roleCollection = CommonUser::getRoleName($roleId);

        $userData = [
            'email' => trim($user->email),
            'firstName' => $user->first_name != '' ? $user->first_name : '.',
            'lastName' => $user->last_name != '' ? $user->last_name : '.',
            'status' => $status,
            'password' => null
        ];

        //        if ($user->user_initials) {
        //            $userData['title'] = $user->user_initials;
        //        }

        $gradeYear = $user->grade_year;

        if (!$gradeYear && $roleId == 1) {
            $gradeYear = 0;
        }

        $sisId = $user->student_sis_id ?? 0;

        if ($membership) {
            $sisId = $membership->student_sis_id ?? $sisId;
        }

        $membershipData = [
            'status' => $status,
            'school' => $school->cus_uuid,
            'authClientId' => 2,
            'user' => $userData,
            'role' => $roleCollection[0],
            'graduationYear' => $gradeYear,
            'sisId' => $sisId,
            'sendConfirmationEmail' => false,
            'preventFromArchiving' => true
        ];
        $membership = $client->assignUserToSchool($membershipData);

        // logger('MEMBERSHIP', [$membership]);
        if (isset($membership['id'])) {
            $userCommonId = CommonMembership::where(
                'id',
                $membership['id']
            )->first()->auth_user_id;

            DB::table('schools_users')->updateOrInsert(
                [
                    'school_id' => $school->id,
                    'user_id' => $user->id
                ],
                [
                    'membership_uuid' => $membership['uuid'],
                    'role_id' => $roleId,
                    'deleted_at' => null,
                    'status' => $status
                ]
            );

            $user->update([
                'cus_uuid' => CommonUser::find($userCommonId)->uuid,
                'initial_password' => null
            ]);
        }
        return [$membershipData, $membership];
    }

    protected function updateCusMembership($membership, $user, $school, $roleId, $status, $commonMembership = null)
    {

        $client = new CommonUserService($this->adminUser);

        $roleCollection = CommonUser::getRoleName($roleId);

        $userData = [
            'uuid' => $user->cus_uuid,
            'email' => trim($user->email),
            'firstName' => $user->first_name != '' ? $user->first_name : '.',
            'lastName' => $user->last_name != '' ? $user->last_name : '.'
        ];
        $gradeYear = $user->grade_year;
        if (!$gradeYear && $roleId == 1) {
            $gradeYear = 0;
        }

        $membershipData = [
            'status' => $status,
            'school' => $school->cus_uuid,
            'authClientId' => 2,
            'user' => $userData,
            'role' => $roleCollection[0],
            'sisId' => $roleId == 1 ? $membership->student_sis_id : null,
            'graduationYear' => $gradeYear
        ];

        $membershipUuid = $membership->membership_uuid;
        if ($commonMembership) {
            $membershipUuid = $membership->membership_uuid == $commonMembership->uuid ? $membership->membership_uuid : $commonMembership->uuid;
        }
        $response = $client->modifyMembershipUser(
            $membershipUuid,
            $membershipData
        );
        DB::table('schools_users')->updateOrInsert(
            [
                'school_id' => $school->id,
                'user_id' => $user->id
            ],
            [
                'membership_uuid' => $membershipUuid,
                'role_id' => $roleId,
                'deleted_at' => null,
                'status' => $status
            ]
        );

        $user->searchable();

        return !isset($response['status']);
    }

    public function updateOldAndSis($oldSchool)
    {
        $school = School::where('old_id', $oldSchool->school_id)
            ->where('status', 1)->first();

        if ($school) {

            $memsWithOldId = DB::table('schools_users')
                ->where('school_id', $school->id)
                ->whereNotNull('old_id');

            $oldUsers = \App\Models\V1\User::where('school_id', $oldSchool->school_id)
                ->where('status', 1)
                ->whereNotIn('user_id', $memsWithOldId->get()->pluck('old_id'));

            $this->output->progressStart($oldUsers->count());

            foreach ($oldUsers->get() as $oldUser) {

                $users = User::withoutGlobalScopes()
                    ->where('email', $oldUser->email)
                    ->get();

                if ($users->count() > 0) {
                    $user = $users->first();

                    if ($users->count() > 1) {

                        $duplicatedUser = DB::table('duplicate_users_clone')
                            ->where('from_id', $user->id)
                            ->first();

                        if ($duplicatedUser) {
                            $dUser = \App\Models\User::withoutGlobalScopes()
                                ->where('id', $duplicatedUser->to_id)
                                ->first();

                            if (trim($dUser->email) == trim($user->email)) {
                                $user = $dUser;
                            } else {
                                DB::table('duplicate_users_clone')
                                    ->where('from_id', $user->id)
                                    ->delete();
                            }

                        } else {
                            DB::table('duplicate_users_clone')->updateOrInsert(
                                [
                                    'from_id' => $user->id,
                                    'to_id' => $user->id
                                ],
                                [
                                    'old_from_id' => $user->old_id
                                ]
                            );
                        }
                    }

                    DB::table('schools_users')
                        ->where('user_id', $user->id)
                        ->where('school_id', $school->id)
                        ->update([
                            'student_sis_id' => $oldUser->student_sis_id,
                            'old_id' => $oldUser->user_id,
                            'updated_at' => now()
                        ]);
                }


                $this->output->progressAdvance();
            }
        }
    }

    public function missingInCus($school, $roleId)
    {
        $memberships = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select('u.id as user_id', 'm.membership_uuid', 'm.role_id')
            ->where('m.school_id', $school->id)
            ->where('m.role_id', $roleId)
            ->where('m.status', 1)
            ->where('u.is_substitute', 0)
            ->whereNull('m.archived_at')
            ->whereNull('m.deleted_at')
            ->whereNull('u.deleted_at')
            ->where('u.first_name', '!=', 'System Ended');

        $this->output->progressStart($memberships->count());

        foreach ($memberships->get() as $membership) {
            if ($membership->membership_uuid) {
                $commonMembership = CommonMembership::where('uuid', $membership->membership_uuid)
                    ->where('not_archived', 1)
                    ->first();
                if (!$commonMembership) {
                    $user = User::where('id', $membership->user_id)->first();
                    $this->migrateToCus($user, $school, $membership->role_id, 1);
                }

            } else {
                $user = User::where('id', $membership->user_id)->first();
                $this->migrateToCus($user, $school, $membership->role_id, 1);
            }
            $this->output->progressAdvance();

        }

    }

    protected function migrateToCus($user, $school, $roleId, $status)
    {
        $client = new CommonUserService($this->adminUser);

        $roleCollection = CommonUser::getRoleName($roleId);

        $userData = [
            'email' => trim($user->email),
            'firstName' => $user->first_name != '' ? $user->first_name : '.',
            'lastName' => $user->last_name != '' ? $user->last_name : '.',
            'status' => $status,
            'password' => null
        ];

        //        if ($user->user_initials) {
        //            $userData['title'] = $user->user_initials;
        //        }

        $gradeYear = $user->grade_year;

        if (!$gradeYear && $roleId == 1) {
            $gradeYear = 0;
        }

        $membershipData = [
            'status' => $status,
            'school' => $school->cus_uuid,
            'authClientId' => 2,
            'user' => $userData,
            'role' => $roleCollection[0],
            'graduationYear' => $gradeYear,
            'sisId' => $user->isStudent() ? $user->student_sis_id : null,
            'sendConfirmationEmail' => false,
            'preventFromArchiving' => true
        ];

        $membership = $client->assignUserToSchool($membershipData);
        //logger('MEMBERSHIP', [$membership]);
        if (isset($membership['id'])) {
            $userCommonId = CommonMembership::where(
                'id',
                $membership['id']
            )->first()->auth_user_id;

            DB::table('schools_users')->updateOrInsert(
                [
                    'school_id' => $school->id,
                    'user_id' => $user->id
                ],
                [
                    'membership_uuid' => $membership['uuid'],
                    'role_id' => $roleId,
                    'deleted_at' => null,
                    'status' => $status
                ]
            );

            $user->update([
                'cus_uuid' => CommonUser::find($userCommonId)->uuid,
                'initial_password' => null
            ]);
        }
        return [$membershipData, $membership];
    }

    protected function updateCus($membership, $user, $school, $roleId, $status)
    {
        $client = new CommonUserService($this->adminUser);

        $roleCollection = CommonUser::getRoleName($roleId);

        $userData = [
            'uuid' => $user->cus_uuid,
            'email' => trim($user->email),
            'firstName' => $user->first_name != '' ? $user->first_name : '.',
            'lastName' => $user->last_name != '' ? $user->last_name : '.'
        ];
        $gradeYear = $user->grade_year;
        if (!$gradeYear && $roleId == 1) {
            $gradeYear = 0;
        }

        $membershipData = [
            'status' => $status,
            'school' => $school->cus_uuid,
            'authClientId' => 2,
            'user' => $userData,
            'role' => $roleCollection[0],
            'sisId' => $roleId == 1 ? $user->student_sis_id : null,
            'graduationYear' => $gradeYear
        ];

        $response = $client->modifyMembershipUser(
            $membership->membership_uuid,
            $membershipData
        );
        DB::table('schools_users')->updateOrInsert(
            [
                'school_id' => $school->id,
                'user_id' => $user->id
            ],
            [
                'role_id' => $roleId,
                'deleted_at' => null,
                'status' => $status
            ]
        );

        $user->searchable();

        return !isset($response['status']);
    }

    public function duplicatedOldIds($oldUser)
    {
        $school = School::where('old_id', $oldUser->school_id)->first();

        if ($school) {

            $this->duplicatedOldIds($oldUser);

            $correctMembership = DB::table('schools_users as m')
                ->join('users as u', 'm.user_id', '=', 'u.id')
                ->select('u.id as user_id')
                ->where('u.old_id', $oldUser->user_id)
                ->where('m.school_id', $school->id)
                ->first();
            if (!$correctMembership) {
                $oldUser->migrateToV2andCus();

            } else {

                $duplicatedUsers = User::withoutGlobalScopes()
                    ->where('old_id', $oldUser->user_id)
                    ->where('id', '!=', $correctMembership->user_id)
                    ->get();


                foreach ($duplicatedUsers as $duplicatedUser) {
                    $duplicatedUser->update([
                        'old_id' => null
                    ]);

                    $duplicatedMembership = DB::table('schools_users as m')
                        ->where('m.user_id', $duplicatedUser->id)
                        ->where('m.school_id', $school->id)->first();

                    if ($duplicatedMembership) {
                        if ($duplicatedMembership->membership_uuid) {
                            $this->deleteMembership($duplicatedMembership->membership_uuid);
                        } else {
                            \Illuminate\Support\Facades\DB::table('schools_users')
                                ->where('id', $duplicatedMembership->id)
                                ->update([
                                    'deleted_at' => now(),
                                    'status' => 0
                                ]);
                        }
                    }


                }
            }

        }

    }

    public function getArchives($oldSchool)
    {
        $oldUsers = \App\Models\V1\User::where('school_id', $oldSchool->school_id)
            ->where('status', -2);

        $this->output->progressStart($oldUsers->count());

        foreach ($oldUsers->get() as $oldUser) {

            $oldUser->sync();

            $this->output->progressAdvance();

        }
    }

    protected function deleteMembership($membershipUuid)
    {
        $response = $this->client->deleteUser($membershipUuid);

        \Illuminate\Support\Facades\DB::table('schools_users')
            ->where('membership_uuid', $membershipUuid)
            ->update([
                'deleted_at' => now(),
                'status' => 0
            ]);
        return true;
    }

    protected function deleteExtraMembershipForSchool($school)
    {

        $v1Emails = DB::connection('oldDB')
            ->table('users')
            ->select('email')
            ->where('school_id', $school->old_id)
            ->where('status', 1)
            ->whereNotNull('email')
            ->get()
            ->pluck('email');


        $memberships = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select([
                'm.*',
            ])
            ->where('m.school_id', $school->id)
            ->where('u.status', 1)
            ->where(function ($query) {
                $query
                    ->where('u.auth_type', '!=', 1001)
                    ->orWhereNull('u.auth_type');
            })
            ->whereNull('u.deleted_at')
            ->whereNull('m.deleted_at')
            ->where(function ($query) use ($v1Emails) {
                $query->whereNull('u.email')
                    ->orWhereNotIn('u.email', $v1Emails);
            })
            ->get();
        $this->output->progressStart($memberships->count());
        foreach ($memberships as $membership) {
            if ($membership->membership_uuid) {
                $this->deleteMembership($membership->membership_uuid);
            } else {
                \Illuminate\Support\Facades\DB::table('schools_users')
                    ->where('id', $membership->id)
                    ->update([
                        'deleted_at' => now(),
                        'status' => 0
                    ]);
            }
            $this->output->progressAdvance();
        }


    }

    public function onchange($mins = 10, $on = 'create')
    {
        if ($mins == 'all') {
            $mins = null;
        }

        $cols = [
            'create' => 'created_at',
            'update' => 'updated_at',
        ];

        $col = $cols[$on] ?? 'created_at';

        $date = now()->timezone('America/New_York')->subMinutes($mins);
        $this->info("Changes after {$date}");
        $oldUsers = \App\Models\V1\User::where('status', 1)
            ->when($mins, function ($q) use ($col, $date) {
                $q->where($col, '>=', $date);
            });

        $this->output->progressStart($oldUsers->count());
        foreach ($oldUsers->get() as $oldUser) {
            $oldUser->sync();
            $this->output->progressAdvance();
        }


    }

    public function migrateNewUsers($school)
    {

        $v2Emails = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select('u.email')
            ->where('m.school_id', $school->id)
            ->where('u.status', 1)
            ->where(function ($query) {
                $query
                    ->where('u.auth_type', '!=', 1001)
                    ->orWhereNull('u.auth_type');
            })
            ->whereNull('m.deleted_at')
            ->where('m.status', 1)
            ->whereNull('u.deleted_at')
            ->whereNotNull('u.email')
            ->get()
            ->pluck('email');

        $oldUsers = \App\Models\V1\User::where('status', 1)->where('role_id', '!=', 0)
            ->where('role_id', '!=', 7)
            ->where('school_id', $school->old_id)
            ->whereNotIn('email', $v2Emails)
            ->get();

        $this->output->progressStart($oldUsers->count());
        foreach ($oldUsers as $oldUser) {

            $oldUser->sync($school);
            $this->output->progressAdvance();
        }

    }

    protected function membershipsResync($oldSchool)
    {

        $school = School::where('old_id', $oldSchool->school_id)->first();

        $v2Emails = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select('u.email')
            ->where('m.school_id', $school->id)
            ->where('u.status', 1)
            ->where(function ($query) {
                $query
                    ->where('u.auth_type', '!=', 1001)
                    ->orWhereNull('u.auth_type');
            })
            ->whereNull('m.deleted_at')
            ->whereNull('u.deleted_at')
            ->get()
            ->pluck('email');

        $oldUsers = \App\Models\V1\User::where('status', 1)
            ->where('school_id', $oldSchool->school_id)
            ->whereNotIn('email', $v2Emails)
            ->get();
        $this->output->progressStart($oldUsers->count());
        foreach ($oldUsers as $oldUser) {
            $oldUser->migrateToV2andCus();
            $this->output->progressAdvance();
        }
    }

    protected function fullResync($oldSchool)
    {
        $dbQuery = $this->oldDb
            ->table('users')
            ->select('user_id', 'email', 'school_id', 'status', 'role_id')
            ->where('school_id', $oldSchool->school_id)
            ->where('status', '>', 0)
            ->when($this->option('roleId'), function ($q) {
                $q->where('role_id', $this->option('roleId'));
            })
            ->orderBy('user_id');

        $school = School::where('old_id', $oldSchool->school_id)->first();
        if (!$school) {
            return false;
        }

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($oldUsers) use ($school) {
            foreach ($oldUsers as $oldUser) {
                $oldUserModel = \App\Models\V1\User::where('user_id', $oldUser->user_id)->first();
                $oldUserModel->migrateToV2andCus();
                $this->output->progressAdvance();
            }

        });
        if ($this->option('deleteExtra')) {
            $this->info("Delete not updated Memberships for  {$school->name} with id {$school->id}");

            $this->deleteNotUpdatedMemberships($school);
        }


    }

    protected function syncStatuses($oldSchool)
    {
        $school = School::where('old_id', $oldSchool->school_id)->first();

        if ($school) {


            $v1Emails = DB::connection('oldDB')
                ->table('users')
                ->select('email')
                ->where('school_id', $school->old_id)
                ->where('status', 1)
                ->whereNotNull('email')
                ->get()
                ->pluck('email');

            $dbQuery = DB::table('schools_users as m')
                ->join('users as u', 'm.user_id', '=', 'u.id')
                ->select([
                    'u.id',
                    'u.old_id',
                ])
                ->where('m.school_id', $school->id)
                ->where('u.status', 1)
                ->where('m.status', 1)
                ->where(function ($query) {
                    $query
                        ->where('u.auth_type', '!=', 1001)
                        ->orWhereNull('u.auth_type');
                })
                ->whereNull('u.deleted_at')
                ->whereNull('m.deleted_at')
                ->where(function ($query) use ($v1Emails) {
                    $query->whereNull('u.email')
                        ->orWhereNotIn('u.email', $v1Emails);
                })
                ->orderBy('u.id');
            $this->output->progressStart($dbQuery->count());
            $dbQuery->chunk(10, function ($userIds) use ($school) {
                foreach ($userIds as $userId) {

                    $oldUser = \App\Models\V1\User::withoutGlobalScopes()
                        ->where('user_id', $userId->old_id)
                        ->first();
                    if ($oldUser && $oldUser->status <= 0) {
                        $user = User::where('id', $userId->id)->first();
                        if ($user) {

                            DB::table('schools_users')
                                ->where('user_id', $user->id)
                                ->where('school_id', $school->id)
                                ->update([
                                    'archived_by' => $this->adminUser->id,
                                    'archived_at' => $oldUser->updated_at,
                                    'status' => 0,
                                ]);


                        }

                    } else {
                        $user = User::where('id', $userId->id)->first();
                        if ($user) {

                            DB::table('schools_users')
                                ->where('user_id', $user->id)
                                ->where('school_id', $school->id)
                                ->update([
                                    'deleted_at' => now(),
                                    'status' => 0,
                                ]);
                        }
                    }

                    $this->output->progressAdvance();
                }
            });
        }
    }

    protected function deleteNotUpdatedMemberships($school)
    {
        $role = Role::v1ToV2($this->option('roleId')) ?? null;
        $dbQuery = Db::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->join('schools as s', 'm.school_id', '=', 's.id')
            ->selectRaw('u.old_id as u_old_id,
                                  u.email as email,
                                  s.old_id as s_old_id,
                                  m.membership_uuid as membership_uuid,
                                  m.id as membership_id,
                                  u.created_at as created,
                                  u.deleted_at as deleted_at,
                                  u.student_sis_id as student_sis_id,
                                  s.id as s_id,
                                  s.common_id as common_id,
                                  s.cus_uuid as s_cus_uuid
                        ',
            )
            ->where('s.id', $school->id)
            ->whereNull('m.updated_at')
            ->when($role, function ($q) use ($role) {
                $q->where('m.role_id', $role);
            })
            ->orderBy('m.id');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($fullRecords) use ($school) {
            foreach ($fullRecords as $fullRecord) {


                DB::table('deleted_memberships')->insert(
                    [
                        'membership_id' => $fullRecord->membership_id,
                    ], [
                        'created' => $fullRecord->created,
                        'created_by' => 38,
                        'archived_at' => $fullRecord->deleted_at,
                        'email' => $fullRecord->email,
                        'sis_id' => $fullRecord->student_sis_id,
                        'school_id' => $school->id,
                        'school_cus_id' => $school->common_id,
                        'school_cus_uuid' => $school->cus_uuid,
                        'school_old_id' => $school->old_id,
                        'cleared_at' => null,
                    ]
                );

                $cusMembership = CommonMembership::where('auth_client_id', 2)
                    ->where('uuid', $fullRecord->membership_uuid)
                    ->first();

                if ($cusMembership) {

                    if ($cusMembership->not_archived) {
                        $response = $this->client->deleteUser($fullRecord->membership_uuid);
                    }
                    DB::table('deleted_memberships')
                        ->where('membership_id', $fullRecord->membership_id)
                        ->update([
                            'cleared_at' => now(),
                        ]);
                }
                DB::table('schools_users')
                    ->where('id', $fullRecord->membership_id)
                    ->update([
                        'deleted_at' => now(),
                        'status' => 0,
                    ]);

//                User::where('email', $fullRecord->email)->update([
//                    'deleted_at' => now(),
//                    'status' => 0
//                ]);

                $this->output->progressAdvance();
            }


        });
    }

    protected function syncWithCUS($school)
    {


        $dbQuery = Db::connection('commonDb')
            ->table('common_bm_membership as m')
            ->join('auth_user as u', 'm.auth_user_id', '=', 'u.id')
            ->join('common_bm_school as s', 'm.common_bm_school_id', '=', 's.id')
            ->leftJoin('auth_title as t', 'u.auth_title_id', '=', 't.id')
            ->selectRaw('u.uuid as user_uuid,
                        u.email as email,
                        m.sis_id as student_sis_id,
                        u.username as username,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        m.common_bm_role_membership_id as role_id,
                        u.image as avatar,
                        m.graduation_year as grade_year,
                        u.r_status as user_status,
                        m.r_status as membership_status,
                        u.last_login_time as last_login_time,
                        u.created as created_at,
                        u.modified as updated_at,
                        s.uuid as school_uuid,
                        m.uuid as membership_uuid,
                        t.r_name as title,
                        u.id as common_user_id,
                        m.archived_at as deleted_at,
                        m.created_by as created_by,
                        m.created as m_created,
                        m.id as m_id
                        ',
            )
            ->where('m.auth_client_id', 2)
            ->where('m.common_bm_school_id', $school->common_id)
            ->where('u.email', "braithwaiteh26@ucfsd.net")
            ->whereNull('m.archived_at')
            ->orderBy('m.id');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($fullRecords) use ($school) {
            foreach ($fullRecords as $fullRecord) {


                $oldUser = \App\Models\V1\User::withoutGlobalScopes()
                    ->where('email', $fullRecord->email)
                    ->where('school_id', $school->old_id)
                    ->first();

                if (!$oldUser) {
                    DB::table('deleted_memberships')->updateOrInsert(
                        [
                            'membership_id' => $fullRecord->m_id,
                        ], [
                            'membership_uuid' => $fullRecord->membership_uuid,
                            'created' => $fullRecord->m_created,
                            'created_by' => $fullRecord->created_by,
                            'archived_at' => $fullRecord->deleted_at,
                            'email' => $fullRecord->email,
                            'sis_id' => $fullRecord->student_sis_id,
                            'school_id' => $school->id,
                            'school_cus_id' => $school->common_id,
                            'school_cus_uuid' => $school->cus_uuid,
                            'school_old_id' => $school->old_id,
                            'cleared_at' => null,
                        ]
                    );
                }
                $this->output->progressAdvance();

            }
        });

    }

    protected function syncWithV2($school)
    {


        $dbQuery = Db::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->join('schools as s', 'm.school_id', '=', 's.id')
            ->selectRaw('u.old_id as u_old_id,
                                  u.email as email,
                                  s.old_id as s_old_id,
                                  m.membership_uuid as membership_uuid,
                                  u.created_at as created,
                                  u.deleted_at as deleted_at,
                                  u.student_sis_id as student_sis_id,
                                  s.id as s_id,
                                  s.common_id as common_id,
                                  s.cus_uuid as s_cus_uuid
                        ',
            )
            ->where('s.id', $school->id)
            ->orderBy('m.id');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($fullRecords) use ($school) {
            foreach ($fullRecords as $fullRecord) {


                $oldUser = \App\Models\V1\User::withoutGlobalScopes()
                    ->where('email', $fullRecord->email)
                    ->where('school_id', $school->old_id)
                    ->first();

                if (!$oldUser) {

                    DB::table('deleted_memberships')->updateOrInsert(
                        [
                            'membership_uuid' => $fullRecord->membership_uuid,
                        ], [
                            'created' => $fullRecord->created,
                            'created_by' => 38,
                            'archived_at' => $fullRecord->deleted_at,
                            'email' => $fullRecord->email,
                            'sis_id' => $fullRecord->student_sis_id,
                            'school_id' => $school->id,
                            'school_cus_id' => $school->common_id,
                            'school_cus_uuid' => $school->cus_uuid,
                            'school_old_id' => $school->old_id,
                            'cleared_at' => null,
                        ]
                    );
                }
                $this->output->progressAdvance();

            }
        });

    }

    protected function deleteInCUS()
    {
        $dbQuery = DB::table('deleted_memberships')
            ->whereNull('cleared_at')
            ->orderBy('id');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($memberships) {

            foreach ($memberships as $membership) {
                dd($membership->membership_uuid);
                $response = $this->client->deleteUser($membership->membership_uuid);

                if (!isset($response['status'])) {
                    DB::table('deleted_memberships')
                        ->where('id', $membership->id)
                        ->orderBy('id')->update([
                            'cleared_at' => now(),
                        ]);
                } else {
                    DB::table('deleted_memberships')
                        ->where('id', $membership->id)
                        ->orderBy('id')->update([
                            'cleared_at' => now(),
                        ]);
                }

                $this->output->progressAdvance();
            }

        });


    }

    protected function fixUsersForSchool($schoolId)
    {
        $school = School::where('id', $schoolId)->first();
        if (!$school) {
            return false;
        }

        $oldSchool = \App\Models\V1\School::where(
            'school_id',
            $school->old_id
        )->first();
        if (!$oldSchool) {
            return false;
        }

        $this->info("Fix Users for School: {$school->name} with id {$school->id}");

//        $userEmails = DB::table('schools_users as m')
//            ->join('users as u', 'm.user_id', 'u.id')
//            ->select('email')
//            ->where('m.school_id', $school->id)
//            ->whereNull('u.deleted_at')
//            ->where(function ($query) {
//                $query->where('u.auth_type', '!=', 1001)
//                    ->orWhereNull('u.auth_type');
//            })
//            ->get()
//            ->pluck('email');

        $dbQuery = \App\Models\V1\User::where('school_id', $oldSchool->school_id)
            ->when($this->option('roleId'), function ($q) {
                $q->where('role_id', $this->option('roleId'));
            })
            ->where('status', '>=', 0)
            ->orderBy('user_id');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk('100', function ($oldUsers) use ($school) {
            foreach ($oldUsers as $oldUser) {
                $oldUser->sync($school);
                $this->output->progressAdvance();
            }
        });

    }

    public function clear($school)
    {


        $v2Emails = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select('u.email')
            ->where('m.school_id', $school->id)
            ->where('u.status', 1)
            ->where('m.status', 1)
            ->where(function ($query) {
                $query
                    ->where('u.auth_type', '!=', 1001)
                    ->orWhereNull('u.auth_type');
            })
            ->whereNull('m.deleted_at')
            ->whereNull('u.deleted_at')
            ->whereNotNull('u.email')
            ->get()
            ->pluck('email');


        $oldUsers = \App\Models\V1\User::where('status', 1)
            ->where('role_id', '!=', 0)
            ->where('role_id', '!=', 7)
            ->where('school_id', $school->old_id)
            ->whereNotIn('email', $v2Emails)
            ->get();

        $this->info('Sync v1 Users');
        $this->output->progressStart($oldUsers->count());

        foreach ($oldUsers as $oldUser) {
            $oldUser->sync();
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

        $v1Emails = DB::connection('oldDB')
            ->table('users')
            ->select('email')
            ->where('school_id', $school->old_id)
            ->where('status', 1)
            ->whereNotNull('email')
            ->get()
            ->pluck('email');

        $diffsV2 = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select([
                'u.id',
            ])
            ->where('m.school_id', $school->id)
            ->where('u.status', 1)
            ->where('m.status', 1)
            ->where('u.is_substitute', 0)
            ->where(function ($query) {
                $query
                    ->where('u.auth_type', '!=', 1001)
                    ->orWhereNull('u.auth_type');
            })
            ->whereNull('u.deleted_at')
            ->whereNull('m.deleted_at')
            ->where(function ($query) use ($v1Emails) {
                $query->whereNull('u.email')
                    ->orWhereNotIn('u.email', $v1Emails);
            })
            ->get()
            ->pluck('id');
        $this->info('Delete from V2');
        $this->output->progressStart(count($diffsV2));

        foreach ($diffsV2 as $id) {

            $user = \App\Models\User::withoutGlobalScopes()
                ->where('id', $id)->first();
            if ($user) {
                $this->deleteSingle($school, $user);
            }
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

        $diffsCus = DB::connection('commonDb')
            ->table('common_bm_membership as m')
            ->join('auth_user as u', 'm.auth_user_id', '=', 'u.id')
            ->select([
                'm.uuid as cus_uuid',
            ])
            ->where('m.common_bm_school_id', $school->common_id)
            ->where('m.not_archived', 1)
            ->where(function ($query) use ($v2Emails) {
                $query->whereNull('u.email')
                    ->orWhereNotIn('u.email', $v2Emails);
            })
            ->get()
            ->pluck('cus_uuid');
        $this->info('Delete from CUS');
        $this->output->progressStart(count($diffsCus));
        foreach ($diffsCus as $membership) {

            $this->deleteSingleMembership($membership);
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

    }

    protected function deleteSingle(School $school, \App\Models\User $user)
    {
        $membership = \Illuminate\Support\Facades\DB::table('schools_users')
            ->where('user_id', $user->id)
            ->where('school_id', $school->id)
            ->whereNull('deleted_at')
            ->first();

        if ($membership) {
            $adminUser = \App\Models\User::where('email', 'viktor.lalev@donatix.net')
                ->latest()
                ->first();
            $client = new CommonUserService($adminUser);


            $response = $client->deleteUser($membership->membership_uuid);

            \Illuminate\Support\Facades\DB::table('schools_users')
                ->where('user_id', $user->id)
                ->where('school_id', $school->id)
                ->update([
                    'deleted_at' => now(),
                    'status' => 0
                ]);

            $user->searchable();
        }

        return true;
    }

    protected function deleteSingleMembership($membership)
    {
        $adminUser = \App\Models\User::where('email', 'viktor.lalev@donatix.net')
            ->latest()
            ->first();
        $client = new CommonUserService($adminUser);


        return $client->deleteUser($membership);
    }
}
