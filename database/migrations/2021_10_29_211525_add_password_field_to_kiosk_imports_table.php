<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPasswordFieldToKioskImportsTable extends Migration
{
    public function up()
    {
        Schema::table('kiosk_imports', function (Blueprint $table) {
            $table->string('kiosk_password');
        });
    }

    public function down()
    {
        Schema::table('kiosk_imports', function (Blueprint $table) {
            $table->dropColumn('kiosk_password');
        });
    }
}
