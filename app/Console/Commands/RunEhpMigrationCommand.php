<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RunEhpMigrationCommand extends Command
{
    protected $signature = 'migrate_sys:run {--after_system_data} {----after_rooms_import}';

    protected $description = 'Migrate system from EhpV1 to EhpV2';

    /**
     * Before migrating run database/recurrenceDaysConverterFunction.sql function.
     */
    public function handle()
    {
        if (!$this->option('after_system_data')) {
            $this->call('migrate_sys:school'); // migrated
            sleep(3);
            $this->call('migrate_sys:modules'); // migrated
            //            $this->call('migrate_sys:time_settings'); // (Converted to sql)
            //            sleep(3);
        }
        $this->call('migrate_sys:school_user'); // miggrated
        sleep(5);
        $this->call('migrate_sys:link_users_to_school');
        sleep(3);
        $this->call('migrate_sys:rooms'); // migrated
        sleep(5);
        if (!$this->option('after_rooms_import')) {
            //            $this->call('migrate_sys:location_cap'); //migrated (Moved to sql script)
            //            $this->call('migrate_sys:fav'); //migrated (moved to sql script)
            //            $this->call('migrate_sys:period'); // migrated (Moved to sql script)
            $this->call('migrate_sys:recurrence_apt'); //
            sleep(3);
            $this->call('migrate_sys:apt'); //
            sleep(3);
            $this->call('migrate_sys:comment');
            //            sleep(3);
            //            $this->call('migrate_sys:pin'); // migrated (Moved to Sql script)
            //            $this->call('migrate_sys:auto_pass'); // migrated (Moved to sql)
            //            $this->call('migrate_sys:auto_pass_preference'); //moved to sql
            //            sleep(3);
            //            $this->call('migrate_sys:room_pin'); //migrated (Moved to sql script)
            //            sleep(3);
            //            $this->call('migrate_sys:auto_pass_limit'); //migrated (moved to sql script)
            //            sleep(3);
            //            $this->call('migrate_sys:active_pass_limit'); // migrated (moved to sql script)
            sleep(3);
            $this->call('migrate_sys:pass_block'); // migrated
            sleep(3);
            //            $this->call('migrate_sys:kpassword'); // migrated (moved to sql script)
            $this->newLine();
            $this->info(
                'Migration was executed successfully. Please check if everything is ok.'
            );
        }
    }
}
