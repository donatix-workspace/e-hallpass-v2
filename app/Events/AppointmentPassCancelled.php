<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AppointmentPassCancelled implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $appointment;
    public $newUserId;

    /**
     * Create a new event instance.
     *
     * @param $appointmentPass
     * @param null $newUserId
     */
    public function __construct($appointmentPass, $newUserId = null)
    {
        $this->appointment = $appointmentPass;
        $this->newUserId = $newUserId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PrivateChannel('appointments.passes.' . $this->appointment->school_id),
            new PrivateChannel('user.appointments.passes.' . $this->appointment->user_id),
        ];
    }
}
