<?php

namespace App\Policies;

use App\Models\StudentFavorite;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentFavoritePolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view any student favorites.
     *
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->isStudent();
    }

    /**
     * Determine whether the user can view the student favorite.
     *
     * @param User $user
     * @param StudentFavorite $studentFavorite
     * @return bool
     */
    public function view(User $user, StudentFavorite $studentFavorite)
    {
        //
    }

    /**
     * Determine whether the user can create student favorites.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {

    }

    /**
     * Determine whether the user can update the student favorite.
     *
     * @param User $user
     * @param StudentFavorite $studentFavorite
     * @return bool
     */
    public function update(User $user, StudentFavorite $studentFavorite)
    {
        return $studentFavorite->school_id === $user->school_id && $studentFavorite->user_id === $user->id &&
            $studentFavorite->global === 0;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function updateBulk(User $user)
    {
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function updateBulkGlobal(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the student favorite.
     *
     * @param User $user
     * @param StudentFavorite $studentFavorite
     * @return bool
     */
    public function delete(User $user, StudentFavorite $studentFavorite)
    {
        return $studentFavorite->school_id === $user->school_id && $studentFavorite->user_id === $user->id;
    }

    /**
     * @param User $user
     * @param StudentFavorite $studentFavorite
     * @return bool
     */
    public function deleteAdmin(User $user, StudentFavorite $studentFavorite)
    {
        return !$user->isStudent() && $studentFavorite->school_id === $user->school_id && $studentFavorite->global === 1;
    }

    /**
     * Determine whether the user can restore the student favorite.
     *
     * @param User $user
     * @param StudentFavorite $studentFavorite
     * @return bool
     */
    public function restore(User $user, StudentFavorite $studentFavorite)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the student favorite.
     *
     * @param User $user
     * @param StudentFavorite $studentFavorite
     * @return bool
     */
    public function forceDelete(User $user, StudentFavorite $studentFavorite)
    {
        //
    }
}
