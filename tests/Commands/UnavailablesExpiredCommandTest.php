<?php

namespace Tests\Commands;

use App\Models\Pass;
use App\Models\School;
use App\Models\Unavailable;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UnavailablesExpiredCommandTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;
    protected $firstUser;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);
    }

    /**
     * @test
     * The command will change the status of expired unavailables record
     * @return void
     */
    public function the_command_will_change_the_status_of_expired_unavailables_record()
    {
        $unavailable = Unavailable::create([
            'school_id' => $this->school->id,
            'from_date' => Carbon::yesterday()->subDays(5),
            'to_date' => Carbon::yesterday(),
            'status' => 1,
            'comment' => 'Comment',
            'unavailable_id' => $this->adminUser->id,
            'unavailable_type' => User::class
        ]);

        $this->artisan('unavailables:expired')->assertExitCode(0)->run();

        $this->assertEquals($unavailable->refresh()->status, 0);
    }

}
