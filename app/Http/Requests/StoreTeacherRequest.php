<?php

namespace App\Http\Requests;

use App\Rules\isPinUnique;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreTeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'user_initials' => 'string|nullable',
            'email' => [
                Rule::requiredIf(function () {
                    return !request()->has('is_substitute') &&
                        request()->get('is_substitute') === null;
                }),
                'email',
                'unique:users,email,' . optional($this->user)->id
            ],
            'pin' => [
                'required',
                'string',
                Rule::unique('pins', 'pin')
                    ->where(function ($query) {
                        $query->where(
                            'school_id',
                            optional(auth()->user())->school_id
                        );
                    })
                    ->ignore($this->pin, 'pin'),
                'min:4',
                'max:6'
            ],
            'status' => 'boolean|required'
        ];
    }
}
