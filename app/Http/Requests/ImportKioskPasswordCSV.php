<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImportKioskPasswordCSV extends FormRequest
{
    public function rules(): array
    {
        return [
            'csv_file' => 'required|file|mimes:csv,txt,xlsx,doc,docx'
        ];
    }

    public function authorize(): bool
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}
