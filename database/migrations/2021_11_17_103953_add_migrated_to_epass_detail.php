<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMigratedToEpassDetail extends Migration
{
    public function up()
    {
        if (config('app.migrate_mode')) {
            if (!Schema::connection('oldDB')->hasColumn('epass_detail', 'migrated')) {
                Schema::connection('oldDB')->table('epass_detail', function (Blueprint $table) {
                    $table->boolean('migrated')->default(0);
                });
            }
        }
    }

    public function down()
    {
        if (config('app.migrate_mode')) {
            if (!Schema::connection('oldDB')->hasColumn('epass_detail', 'migrated')) {
                Schema::connection('oldDB')->table('epass_detail', function (Blueprint $table) {
                    $table->dropColumn('migrated');
                });
            }
        }
    }
}
