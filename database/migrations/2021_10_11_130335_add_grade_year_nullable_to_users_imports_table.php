<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGradeYearNullableToUsersImportsTable extends Migration
{
    public function up()
    {
        Schema::table('users_imports', function (Blueprint $table) {
            if (!Schema::hasColumn('users_imports','grade_year')) {
                $table->year('grade_year')->nullable();
            }
        });
    }

    public function down()
    {
        Schema::table('users_imports', function (Blueprint $table) {
            //
        });
    }
}
