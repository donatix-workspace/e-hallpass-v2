<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNurseRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nurse_rooms', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('room_id')->unsigned()->index('room_id');
            $table->bigInteger('school_id')->unsigned()->index('school_id');
            $table->string('end_time', 25);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nurse_rooms');
    }
}
