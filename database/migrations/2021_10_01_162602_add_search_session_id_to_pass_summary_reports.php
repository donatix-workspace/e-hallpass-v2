<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSearchSessionIdToPassSummaryReports extends Migration
{
    public function up()
    {
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->string('search_session_id');
        });
    }

    public function down()
    {
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->dropColumn('search_session_id');
        });
    }
}
