<?php

namespace App\Models;

use App\Elastic\LocationCapacitiesIndexConfigurator;
use App\Traits\ExtendedSearchable;
use App\Traits\LocationCapacityMapping;
use App\Traits\PerPage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use ScoutElastic\Searchable;

class LocationCapacity extends Model
{
    use HasFactory, ExtendedSearchable, LocationCapacityMapping, PerPage;

    protected $indexConfigurator = LocationCapacitiesIndexConfigurator::class;

    protected $appends = ['capacity_reached'];

    protected $with = ['room:id,name'];

    protected $fillable = ['room_id', 'status', 'limit', 'school_id'];

    public const ACTIVE = 1;
    public const INACTIVE = 0;

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * Specify the limit to be saved as -1 (Idicates unlimited)
     * Issue: https://eduspiresolutions.atlassian.net/browse/EHP2-1229
     * @return array
     */
    public function toSearchableArray(): array
    {
        $item = $this->toArray();
        $item['limit'] = $item['limit'] === '-' ? -1 : $item['limit'];

        return $item;
    }

    /**
     * @return bool
     */
    public function getCapacityReachedAttribute(): bool
    {
        //        $countingPassesWithoutParentTo = Pass::where(
        //            'school_id',
        //            $this->school_id
        //        )
        //            ->where('pass_status', Pass::ACTIVE)
        //            ->where('to_type', Room::class)
        //            ->where('to_id', $this->room_id)
        //            ->where('parent_id', null)
        //            ->count();
        //
        //        $countingPassesWithParentTo = Pass::where('school_id', $this->school_id)
        //            ->where('pass_status', Pass::ACTIVE)
        //            ->where('parent_id', '!=', null)
        //            ->where('to_type', Room::class)
        //            ->where('to_id', $this->room_id)
        //            ->count();
        //
        //        $countingPassesFrom = Pass::where('school_id', $this->school_id)
        //            ->where('pass_status', Pass::ACTIVE)
        //            ->where('parent_id', '!=', null)
        //            ->where('from_type', Room::class)
        //            ->where('from_id', $this->room_id)
        //            ->count();

        return false;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    /**
     * @param $value
     * @return int|string
     */
    public function getLimitAttribute($value)
    {
        return $value < 0 ? '-' : (int) $value;
    }
}
