<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GlobalPassLimitRequest extends FormRequest
{
    public function rules()
    {
        return [
            'proxy_pass' => 'boolean|required',
            'student_pass' => 'boolean|required',
            'kiosk_pass' => 'boolean|required',
            'limit' => 'nullable',
            'teacher_override' => 'boolean|required',
        ];
    }

    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}
