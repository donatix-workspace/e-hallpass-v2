<?php

namespace App\Rules;

use App\Models\PassLimit;
use App\Models\RoomRestriction;
use Illuminate\Contracts\Validation\Rule;

class RoomRestrictionValidRule implements Rule
{
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $acceptedForTypes = [RoomRestriction::CSV, RoomRestriction::STUDENT, RoomRestriction::GRADEYEAR];

        return in_array($value, $acceptedForTypes);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('room.restriction;invalid.for.type');
    }
}
