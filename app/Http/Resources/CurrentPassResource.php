<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Pass */
class CurrentPassResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = parent::toArray($request);
        if (count($resource)) {
            if (!$resource['from_id']) {
                $resource['from'] = [
                    'id' => optional($resource['from'])['id'] === null ? 0 : $resource['from']['id'],
                    'name' => optional($resource['from'])['name'] === null ? config('roles.prefixes.system') : $resource['from']['name'],
                    'first_name' => optional($resource['from'])['first_name'],
                    'last_name' => optional($resource['from'])['last_name'],
                    'comment_type' => optional($resource['from'])['comment_type'],
                    'trip_type' => optional($resource['from'])['trip_type'],
                    'enable_appointment_passes' => optional($resource['from'])['enable_appointment_passes'],
                    'user_ids_assigned' => optional($resource['from'])['user_ids_assigned'] === null ? [] : optional($resource['from'])['user_ids_assigned']
                ];
            }
            if (!$resource['to_id']) {
                $resource['to'] = [
                    'id' => optional($resource['to'])['id'] === null ? 0 : $resource['to']['id'],
                    'name' => optional($resource['to'])['name'] === null ? config('roles.prefixes.system') : $resource['to']['name'],
                    'first_name' => optional($resource['to'])['first_name'],
                    'last_name' => optional($resource['to'])['last_name'],
                    'comment_type' => optional($resource['to'])['comment_type'],
                    'trip_type' => optional($resource['to'])['trip_type'],
                    'enable_appointment_passes' => optional($resource['to'])['enable_appointment_passes'],
                    'user_ids_assigned' => optional($resource['to'])['user_ids_assigned'] === null ? [] : optional($resource['to'])['user_ids_assigned']
                ];
            }

            if (array_key_exists('parent', $resource) && $resource['parent'] !== null) {
                if (!$resource['parent']['from_id']) {
                    $resource['parent']['from'] = [
                        'id' => optional(optional($resource['parent'])['from'])['id'] === null ? 0 : optional(optional($resource['parent'])['from'])['id'],
                        'name' => optional(optional($resource['parent'])['from'])['name'] === null ? config('roles.prefixes.system') : optional(optional($resource['parent'])['from'])['name'],
                        'first_name' => optional(optional($resource['parent'])['from'])['first_name'],
                        'last_name' => optional(optional($resource['parent'])['from'])['last_name'],
                        'comment_type' => optional(optional($resource['parent'])['from'])['comment_type'],
                        'trip_type' => optional(optional($resource['parent'])['from'])['trip_type'],
                        'enable_appointment_passes' => optional(optional($resource['parent'])['from'])['enable_appointment_passes'],
                        'user_ids_assigned' => optional(optional($resource['parent'])['from'])['user_ids_assigned'] === null ? [] : optional(optional($resource['parent'])['from'])['user_ids_assigned']
                    ];
                }
                if (!$resource['parent']['to_id']) {
                    $resource['parent']['to'] = [
                        'id' => optional(optional($resource['parent'])['to'])['id'] === null ? 0 : optional(optional($resource['parent'])['to'])['id'],
                        'name' => optional(optional($resource['parent'])['to'])['name'] === null ? config('roles.prefixes.system') : optional(optional($resource['parent'])['to'])['name'],
                        'first_name' => optional(optional($resource['parent'])['to'])['first_name'],
                        'last_name' => optional(optional($resource['parent'])['to'])['last_name'],
                        'comment_type' => optional(optional($resource['parent'])['to'])['comment_type'],
                        'trip_type' => optional(optional($resource['parent'])['to'])['trip_type'],
                        'enable_appointment_passes' => optional(optional($resource['parent'])['to'])['enable_appointment_passes'],
                        'user_ids_assigned' => optional(optional($resource['parent'])['to'])['user_ids_assigned'] === null ? [] : optional(optional($resource['parent'])['to'])['user_ids_assigned']
                    ];
                }
            }
        }
        return $resource;
    }
}
