<?php

namespace App\Http\Controllers\Passes;

use App\Events\AppointmentPassAcknowledged;
use App\Events\AppointmentPassCancelled;
use App\Events\AppointmentPassConfirmed;
use App\Events\AppointmentPassCreated;
use App\Http\Controllers\Controller;
use App\Http\Filters\AppointmentFilter;
use App\Http\Requests\StudentAppointmentRequest;
use App\Http\Resources\AppointmentPassCollection;
use App\Http\Resources\AppointmentPassResource;
use App\Models\AppointmentPass;
use App\Models\School;
use App\Models\Transparency;
use App\Models\User;
use App\Notifications\Push\StudentNotification;
use App\Rules\DateIsPassBlockedRule;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

/**
 * @group Appointment Passes (Students)
 */
class AppointmentController extends Controller
{
    /**
     * Get all of the student appointment passes for today.
     *
     * @queryParam between_dates array Get records between two dates Example: ?between_dates=["04/20/2021", "06/20/2021"]
     * @queryParam sort string The sort types: "asc", "desc"
     * @queryParam recurrence_only boolean Get only recurring appointments
     * @queryParam per_page integer The number of records per page
     *
     * @authenticated
     * @param AppointmentFilter $filters
     * @return AppointmentPassCollection
     * @throws AuthorizationException
     * @apiResourceCollection App\Http\Resources\AppointmentPassCollection
     * @apiResourceModel App\Models\AppointmentPass
     */
    public function index(AppointmentFilter $filters)
    {
        $this->authorize('viewStudent', AppointmentPass::class);
        $user = auth()->user();
        $schoolTimezone = School::findCacheFirst(
            auth()->user()->school_id
        )->getTimezone();

        if (request()->has('sort') || request()->has('search_query')) {
            $searchQuery =
                request()->get('search_query') === null
                    ? config('scout_elastic.all_records')
                    : convertElasticQuery(request()->get('search_query'));

            $appointmentPassesElasticIds = collect(
                AppointmentPass::search($searchQuery)
                    ->with(['from', 'to', 'pass'])
                    ->where('user_id', $user->id)
                    ->where('school_id', $user->school_id)
                    ->when(request()->has('sort'), function ($query) {
                        $sort = explode(':', request()->get('sort'));
                        if (count($sort) <= 1) {
                            return $query->orderBy('id', 'asc');
                        }

                        $sortingKeywords = ['asc', 'desc'];

                        if (in_array($sort[1], $sortingKeywords)) {
                            return $query->orderBy($sort[0], $sort[1]);
                        }

                        return $query->orderBy('id', 'asc');
                    })
                    ->when(request()->has('between_dates'), function (
                        $query
                    ) use ($schoolTimezone) {
                        $dates = explode(
                            ',',
                            request()->input('between_dates')
                        );

                        $startDate = Carbon::parse($dates[0], $schoolTimezone)
                            ->setTimezone(config('app.timezone'))
                            ->startOfDay();
                        $endDate = Carbon::parse($dates[1], $schoolTimezone)
                            ->setTimezone(config('app.timezone'))
                            ->endOfDay();

                        if ($dates === null || count($dates) <= 1) {
                            return $query->whereBetween('for_date', [
                                now()
                                    ->startOfDay()
                                    ->toDateTimeString(),
                                now()
                                    ->endOfDay()
                                    ->toDateTimeString()
                            ]);
                        }

                        return $query->whereBetween('for_date', [
                            $startDate->toDateTimeString(),
                            $endDate->toDateTimeString()
                        ]);
                    })
                    ->take(
                        AppointmentPass::where('school_id', $user->school_id)
                            ->ofUserId($user->id)
                            ->count()
                    )
                    ->get()
            )
                ->pluck('id')
                ->toArray();

            $appointmentPassesElasticIdsImploded = implode(
                ',',
                $appointmentPassesElasticIds
            );

            $appointmentPasses = AppointmentPass::with(['from', 'to', 'pass'])
                ->without('pass')
                ->whereIntegerInRaw('id', $appointmentPassesElasticIds)
                ->ofUserId(auth()->user()->id)
                ->orderByRaw("field(id, $appointmentPassesElasticIdsImploded)")
                ->filter($filters)
                ->paginate(AppointmentPass::recordsPerPage());
        } else {
            $appointmentPasses = AppointmentPass::with(['from', 'to', 'pass'])
                ->ofUserId(auth()->user()->id)
                ->orderBy('for_date', 'asc')
                ->fromSchoolId(auth()->user()->school_id)
                ->filter($filters)
                ->paginate(AppointmentPass::recordsPerPage());
        }

        return new AppointmentPassCollection($appointmentPasses);
    }

    /**
     * Show the appointment
     *
     * @urlParam appointmentPass integer required The id of the appointment
     *
     * @authenticated
     * @param AppointmentPass $appointmentPass
     * @return AppointmentPassResource
     * @apiResource App\Http\Resources\AppointmentPassResource
     * @apiResourceModel App\Models\AppointmentPass
     * @throws AuthorizationException
     */
    public function show(
        AppointmentPass $appointmentPass
    ): AppointmentPassResource {
        $this->authorize('showStudent', $appointmentPass);

        return new AppointmentPassResource($appointmentPass);
    }

    /**
     * Acknowledge the appointment
     *
     * @urlParam appointmentPass integer required The id of the appointment
     *
     * @authenticated
     * @param AppointmentPass $appointmentPass
     * @return AppointmentPassResource|JsonResponse
     * @apiResource App\Http\Resources\AppointmentPassResource
     * @apiResourceModel App\Models\AppointmentPass
     * @throws AuthorizationException
     */
    public function acknowledge(AppointmentPass $appointmentPass)
    {
        $this->authorize('acknowledge', $appointmentPass);
        $currentTime = Carbon::now();

        if (
            $appointmentPass->for_date->between(
                $appointmentPass->for_date,
                $currentTime->endOfDay()
            )
        ) {
            $appointmentPass->update([
                'acknowledge_at' => Carbon::now()
            ]);
            event(
                new AppointmentPassAcknowledged(
                    $appointmentPass->id,
                    auth()->user()->school_id
                )
            );
        } else {
            return response()->json(
                [
                    'data' => ['id' => $appointmentPass->id],
                    'message' => __(
                        'passes.appointment.invalid.acknowledge.date'
                    ),
                    'status' => __('fail')
                ],
                422
            );
        }

        return new AppointmentPassResource($appointmentPass->refresh());
    }

    /**
     * Confirm the appointment pass by student
     *
     * @urlParam appointmentPass integer required The id of the pass
     *
     * @param AppointmentPass $appointmentPass
     * @return AppointmentPassResource|JsonResponse
     * @apiResource App\Http\Resources\AppointmentPassResource
     * @apiResourceModel App\Models\AppointmentPass
     * @throws AuthorizationException
     */
    public function confirm(AppointmentPass $appointmentPass)
    {
        $this->authorize('confirm', $appointmentPass);

        if ($appointmentPass->secondReminder() || $appointmentPass->forNow()) {
            $appointmentPass->update([
                'confirmed_at' => Carbon::now(),
                'ran_at' => Carbon::now()
            ]);

            event(
                new AppointmentPassConfirmed(
                    $appointmentPass->id,
                    $appointmentPass->school_id
                )
            );

            //            Notification::send(
            //                $appointmentPass,
            //                new StudentNotification('AppointmentPassConfirmed', [
            //                    'confirmedAt' => School::convertTimeToCorrectTimezone(
            //                        $appointmentPass->for_date,
            //                        auth()
            //                            ->user()
            //                            ->school->getTimezone()
            //                    )
            //                ])
            //            );
        } else {
            return response()->json(
                [
                    'data' => ['id' => $appointmentPass->id],
                    'message' => __('passes.appointment.invalid.confirm.date'),
                    'status' => __('fail')
                ],
                422
            );
        }

        return new AppointmentPassResource($appointmentPass->refresh());
    }

    /**
     *
     * Acknowledge the appointment pass by email from student
     *
     * @urlParam appointmentPass integer required The id of the pass
     *
     * @return JsonResponse
     * @apiResource App\Http\Resources\AppointmentPassResource
     * @apiResourceModel App\Models\AppointmentPass
     */
    public function acknowledgeMail()
    {
        $emailToken = request()->get('token', null);

        if ($emailToken !== null) {
            $emailTokenDb = DB::table('appointments_email_tokens')->where(
                'token',
                $emailToken
            );

            $emailTokenAdmin = DB::table('appointments_email_tokens')->where(
                'admin_token',
                $emailToken
            );

            if (!$emailTokenDb->exists() && !$emailTokenAdmin->exists()) {
                return response()->json(
                    [
                        'data' => ['token' => $emailToken],
                        'status' => __('fail'),
                        'message' => __('Already acknowledged!')
                    ],
                    403
                );
            }

            $appointmentPass = AppointmentPass::where(
                'id',
                optional($emailTokenDb->first())->appointment_pass_id
            )
                ->orWhere(
                    'id',
                    optional($emailTokenAdmin->first())->appointment_pass_id
                )
                ->first();

            // The email token array contains after explode:
            // $emailToken = ['XXX-XXX-XXX-XXX-TOKEN', 'from_id', 'to_id']
            $acknowledgedById = optional(explode('\\', $emailToken))[1]; // Get the teacher id by \ limiter (second place)

            if (!$acknowledgedById) {
                $acknowledgedById = optional(explode('\\', $emailToken))[2]; // Get the toId by \ limiter (third place)
            }

            if ($emailTokenAdmin->exists()) {
                $appointmentPass->update([
                    'acknowledged_by_teacher_at' => now(),
                    'acknowledged_by' => $acknowledgedById
                ]);

                // If the appointment pass is from the system, we replace the "from: location with the acknowledged adult
                if ($appointmentPass->from_id === config('roles.system')) {
                    $appointmentPass->update([
                        'from_id' => $acknowledgedById,
                        'from_type' => User::class
                    ]);
                }
            }

            if (!$emailTokenAdmin->exists()) {
                $appointmentPass->update([
                    'acknowledged_by_mail_at' => Carbon::now()
                ]);
            }

            return response()->json([
                'data' => [],
                'status' => __('success'),
                'message' => __(
                    'You\'ve successfully acknowledge your appointment pass!'
                )
            ]);
        }

        return response()->json([
            'data' => [],
            'status' => __('fail'),
            'message' => 'Please provide valid email token to the request.'
        ]);
    }

    /**
     * Request a student appointment pass
     *
     * @param StudentAppointmentRequest $request
     *
     * @response status=422, scenario='Module option for role is not activated' {
     *     "data": {
     *         "from": true,
     *         "to": false,
     *     },
     *     "message": "passes.appointment.can.create.to.that.location",
     *     "status": "fail"
     * }
     *
     * @bodyParam from_id integer required Current destination of the user
     * @bodyParam to_id integer required To what destination user wants to go
     * @bodyParam from_type string required Current destination type (Room or User)
     * @bodyParam to_type string required Target destination type (Room or User)
     * @bodyParam for_date date required Date for the appointment pass
     * @bodyParam period_id integer required The period that appointment is being created
     * @bodyParam reason string required The reason that appointment is being created
     *
     * @apiResource App\Http\Resources\AppointmentPassResource
     * @apiResourceModel App\Models\AppointmentPass
     *
     * @authenticated
     * @return AppointmentPassResource|JsonResponse
     * @throws AuthorizationException
     */
    public function request(StudentAppointmentRequest $request)
    {
        $user = auth()->user();

        $this->authorize('studentRequest', AppointmentPass::class);

        $doesPassBlockExistsValidator = Validator::make($request->all(), [
            'for_date' => [new DateIsPassBlockedRule()]
        ]);

        if ($doesPassBlockExistsValidator->fails()) {
            return response()->json(
                [
                    'data' => [],
                    'message' => $doesPassBlockExistsValidator
                        ->errors()
                        ->first(),
                    'status' => __('fail')
                ],
                422
            );
        }

        $validateStudentAppointmentRequest = AppointmentPass::validateStudentAppointmentRequest(
            $user,
            $request
        );

        if ($validateStudentAppointmentRequest !== null) {
            return $validateStudentAppointmentRequest;
        }

        $appointmentPass = AppointmentPass::create([
            'created_by' => $user->id,
            'from_id' => $request->input('from_id'),
            'from_type' => $request->input('from_type'),
            'to_id' => $request->input('to_id'),
            'to_type' => $request->input('to_type'),
            'user_id' => $user->id,
            'for_date' => Carbon::parse($request->input('for_date')),
            'school_id' => $user->school_id,
            'period_id' => $request->input('period_id'),
            'pass_id' => null,
            'reason' => $request->input('reason', '')
        ]);

        if ($request->input('reason', '')) {
            $appointmentPass->comments()->create([
                'comment' => $appointmentPass->reason,
                'user_id' => $appointmentPass->created_by,
                'school_id' => $appointmentPass->school_id
            ]);
        }

        $appointmentPass->refreshDashboard();

        if (
            !Transparency::isEnabled() ||
            (Transparency::isEnabled() &&
                $appointmentPass->isCoverTheTransparency())
        ) {
            event(new AppointmentPassCreated($appointmentPass, $user->id));
            //            Notification::send(
            //                $appointmentPass,
            //                new StudentNotification('AppointmentPassCreated')
            //            );
        }

        return new AppointmentPassResource($appointmentPass);
    }

    /**
     * @param AppointmentPass $appointmentPass
     * @return AppointmentPassResource
     * @throws AuthorizationException
     */
    public function cancel(
        AppointmentPass $appointmentPass
    ): AppointmentPassResource {
        $this->authorize('studentCancel', $appointmentPass);

        $appointmentPass->update([
            'canceled_at' => now(),
            'canceled_by' => auth()->user()->id
        ]);

        $appointmentPass->refreshDashboard();

        $appointmentPass->comments()->create([
            'comment' => request()->input('cancel_reason'),
            'user_id' => $appointmentPass->created_by,
            'school_id' => $appointmentPass->school_id
        ]);

        event(
            new AppointmentPassCancelled(
                $appointmentPass,
                $appointmentPass->user_id
            )
        );
        //        Notification::send(
        //            $appointmentPass,
        //            new StudentNotification('AppointmentPassCancelled')
        //        );

        return new AppointmentPassResource($appointmentPass);
    }
}
