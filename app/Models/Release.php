<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Release extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = [
        'seen'
    ];

    /**
     * @return mixed
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return bool
     */
    public function getSeenAttribute()
    {
        return collect(json_decode($this->seen_by_user_ids, true))->contains(optional(auth()->user())->id);
    }
}
