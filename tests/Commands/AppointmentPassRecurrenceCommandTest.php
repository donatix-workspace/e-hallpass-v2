<?php

namespace Tests\Commands;

use App\Models\AppointmentPass;
use App\Models\Pass;
use App\Models\Period;
use App\Models\Pin;
use App\Models\RecurrenceAppointmentPass;
use App\Models\Room;
use App\Models\RoomType;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\ModuleSeeder;
use Database\Seeders\RolesDatabaseSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AppointmentPassRecurrenceCommandTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;
    protected $staffUser;
    protected $teacherUser;
    protected $firstUser;
    protected $secondUser;


    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();
        (new ModuleSeeder())->run();

        $this->school = School::factory()->createOne(['timezone' => 'Europe/Sofia']);

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->secondUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);

        $this->staffUser = User::factory()->createOne([
            'role_id' => config('roles.staff'),
            'school_id' => $this->school->id
        ]);

        $this->teacherUser = User::factory()->createOne([
            'role_id' => 3,
            'school_id' => $this->school->id
        ]);

        $this->room = Room::factory()->count(1)->for(RoomType::factory()->create([
            'school_id' => $this->school->id
        ]))->create([
            'school_id' => $this->school->id,
            'trip_type' => 2, // trip one way
        ])->first();

        SchoolModule::create([
            'school_id' => $this->school->id,
            'module_id' => 1, // apt pass
            'option_json' => json_encode(['hello' => 'world']),
            'permission' => 1,
            'status' => 1,
        ]);

        SchoolModule::create([
            'school_id' => $this->school->id,
            'module_id' => 2, // auto check in
            'option_json' => json_encode(['hello' => 'world']),
            'permission' => 1,
            'status' => 1,
        ]);
    }

    /**
     * @test
     * The command will create correct appointment passes when it's daily recurrence
     * @return void
     */
    public function the_command_will_create_correct_appointment_passes_when_it_s_daily_recurrence()
    {
        Carbon::setTestNow(Carbon::parse('first Monday of March'));

        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);

        $recurrenceAppointmentPass = RecurrenceAppointmentPass::create([
            'user_id' => $this->firstUser->id,
            'created_by' => $this->adminUser->id,
            'from_id' => $this->teacherUser->id,
            'from_type' => User::class,
            'to_id' => $this->adminUser->id,
            'to_type' => User::class,
            'period_id' => $period->id,
            'recurrence_type' => config('recurrences.daily'),
            'recurrence_end_at' => Carbon::now()->addMonths(5),
            'reason' => "Test",
            'for_date' => Carbon::yesterday(optional($this->school)->getTimezone())->setTime(16, 00, 00),
            'school_id' => $this->school->id,
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::yesterday(optional($this->school)->getTimezone())->setTime(16, 00, 00),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'reason' => 'reason',
            'recurrence_appointment_pass_id' => $recurrenceAppointmentPass->id
        ]);

        $this->artisan('passes:appointment_recurrence_schedule')->assertExitCode(0)->run();

        $this->assertDatabaseHas('appointment_passes', [
            'for_date' => $appointmentPass->for_date
                ->setTimezone(config('app.timezone'))
                ->addWeek()
                ->subDays(2)
                ->format('Y-m-d H:i:s')
        ]);
    }

    /**
     * @test
     * The command will create the recurrence on weekly  selected date
     * @return void
     */
    public function the_command_will_create_the_recurrence_on_weekly_selected_date()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);

        $recurrenceAppointmentPass = RecurrenceAppointmentPass::create([
            'user_id' => $this->firstUser->id,
            'created_by' => $this->adminUser->id,
            'from_id' => $this->teacherUser->id,
            'from_type' => User::class,
            'to_id' => $this->adminUser->id,
            'to_type' => User::class,
            'period_id' => $period->id,
            'recurrence_type' => config('recurrences.weekly'),
            'recurrence_days' => collect(["days" => [Carbon::now()->englishDayOfWeek]])->toJson(),
            'recurrence_end_at' => Carbon::now()->addMonths(5),
            'reason' => "Test",
            'for_date' => Carbon::yesterday(optional($this->school)->getTimezone())->setTime(16, 00, 00),
            'school_id' => $this->school->id,
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::yesterday(optional($this->school)->getTimezone())->setTime(16, 00, 00),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'reason' => 'reason',
            'recurrence_appointment_pass_id' => $recurrenceAppointmentPass->id
        ]);

        $this->artisan('passes:appointment_recurrence_schedule')->assertExitCode(0)->run();
    }
}
