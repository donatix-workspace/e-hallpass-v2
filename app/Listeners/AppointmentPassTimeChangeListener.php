<?php

namespace App\Listeners;

use App\Events\AppointmentPassTimeChanged;
use App\Models\AppointmentNotification;

class AppointmentPassTimeChangeListener
{
    public function __construct()
    {
        //
    }

    public function handle(AppointmentPassTimeChanged $event)
    {
        $appointmentPassNotification = AppointmentNotification::create([
            'notification_type' => AppointmentNotification::TIME_CHANGE,
            'appointment_pass_id' => $event->appointment->id,
            'user_id' => $event->appointment->user_id,
            'old_time' => $event->oldTime,
            'new_time' => $event->newTime,
            'school_id' => $event->appointment->school_id,
            'updated_by_id' => $event->newUserId
        ]);
    }
}
