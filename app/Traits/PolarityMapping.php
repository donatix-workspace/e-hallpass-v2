<?php

namespace App\Traits;

trait PolarityMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'school_id' => [
                'type' => 'long'
            ],
            'status' => [
                'type' => 'long'
            ],
            'status_string' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'message' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'first_user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'second_user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'times_triggered' => [
                'type' => 'long'
            ],
            'created_at' => [
                'type' => 'date',
                'format' => 'dd/mm/YYYY',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'deleted_at' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 0
            ]
        ]
    ];
}
