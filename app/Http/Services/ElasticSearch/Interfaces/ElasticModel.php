<?php

namespace App\Http\Services\ElasticSearch\Interfaces;

interface ElasticModel
{
    public function get(int $size = 25);

    public function paginate(int $perPage = 25);

    public function count();

    public function buildClient();

    public function queryParams(array $queryParam, string $searchString = '*');
}
