<?php

namespace App\Rules;

use App\Models\AutoPass;
use Illuminate\Contracts\Validation\Rule;

class AutoPassValidRoomRule implements Rule
{
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $autoPassCheck = AutoPass::where('room_id', $value)
            ->where('school_id', auth()->user()->school_id)
            ->exists();

        if (!$autoPassCheck) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('passes.auto.invalid.room.specified');
    }
}
