<?php

namespace App\Imports;

use App\Models\KioskUser;
use App\Models\User;
use DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class KioskPasswordsImport implements ToCollection, WithHeadingRow, WithValidation
{
    public $fileName;

    /**
     * @param $fileName
     */
    public function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'studentemail' => ['required', 'email'],
            'password' => 'required|min:4'
        ];
    }

    /**
     * @inheritDoc
     * @throws \Throwable
     */
    public function collection(Collection $collection)
    {
        $user = auth()->user();
        $insertableArray = [];

        foreach ($collection as $kioskItem) {
            $userExists = User::whereEmail($kioskItem['studentemail'])
                ->select('email')
                ->where('school_id', $user->school_id)
                ->exists();

            $insertableArray[] = [
                'email' => $kioskItem['studentemail'],
                'status' => $userExists ? User::ACTIVE : User::INACTIVE,
                'school_id' => $user->school_id,
                'edited' => User::INACTIVE,
                'created_at' => now(),
                'updated_at' => now(),
                'file_name' => $this->fileName,
                'kiosk_password' => $kioskItem['password']
            ];
        }

        DB::beginTransaction();
        DB::table('kiosk_imports')->insert($insertableArray);
        DB::commit();
    }
}
