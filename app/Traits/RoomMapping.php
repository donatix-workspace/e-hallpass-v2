<?php

namespace App\Traits;

trait RoomMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'id' => [
                'type' => 'long'
            ],
            'school_id' => [
                'type' => 'long'
            ],
            'name' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
            ],
            'trip_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
            ],
            'comment_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
            ],
            'extended_pass_time' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
            ],
            'enable_appointment_passes' => [
                'type' => 'long'
            ],
            'status' => [
                'type' => 'long',
            ],
            'admin_favorite' => [
                'type' => 'long'
            ]
        ]
    ];
}
