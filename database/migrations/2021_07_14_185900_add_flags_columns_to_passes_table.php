<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlagsColumnsToPassesTable extends Migration
{
    public function up()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->timestamp('flagged_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->dropColumn('flagged_at');
        });
    }
}
