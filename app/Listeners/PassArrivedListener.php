<?php

namespace App\Listeners;

use App\Events\PassArrived;

class PassArrivedListener
{
    public function __construct()
    {
        //
    }

    public function handle(PassArrived $event)
    {
        $event->pass->refreshDashboard();
    }
}
