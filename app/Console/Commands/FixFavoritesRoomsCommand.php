<?php

namespace App\Console\Commands;

use App\Models\Room;
use App\Models\StudentFavorite;
use Illuminate\Console\Command;

class FixFavoritesRoomsCommand extends Command
{
    protected $signature = 'rooms:favorites';

    protected $description = 'Fix rooms favorites';

    /**
     * @return int
     */
    public function handle()
    {
        $favoritesRoomIds = StudentFavorite::where('user_id', null)
            ->where('global', 1)
            ->where('favorable_type', Room::class)
            ->get()
            ->pluck('favorable_id')
            ->toArray();

        $rooms = Room::whereIntegerInRaw('id', $favoritesRoomIds);
        $rooms->update(['admin_favorite' => Room::ACTIVE]);
        $rooms->searchable();

        $this->output->info(
          'Rooms added to favorites successfully.'
        );

        return 0;
    }
}
