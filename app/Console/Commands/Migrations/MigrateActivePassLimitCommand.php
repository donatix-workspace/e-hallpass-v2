<?php

namespace App\Console\Commands\Migrations;

use Illuminate\Console\Command;

class MigrateActivePassLimitCommand extends Command
{
    protected $signature = 'migrate_sys:active_pass_limit';

    protected $description = 'Migrate active pass limits from EHPv1 to EHPv2';

    public function handle()
    {
        $this->info('--- Migrating Active Pass Limits---');

        $newDb = \DB::connection('mysql');
        $oldDb = \DB::connection('oldDB');

        $oldDb->disableQueryLog();
        $newDb->disableQueryLog();

        $activePassLimits = $oldDb
            ->table('pass_limit_concurrent')
            ->get()
            ->toArray();

        $activePassLimitsChunks = array_chunk($activePassLimits, 1500);

        $this->output->progressStart(count($activePassLimits));

        foreach ($activePassLimitsChunks as $activePassLimitChunk) {
            $insertableActivePassLimitArray = [];
            foreach ($activePassLimitChunk as $activePassLimit) {
                $oldSchool = $oldDb
                    ->table('school_schedule')
                    ->where('school_id', $activePassLimit->school_id)
                    ->first();

                if ($oldSchool) {
                    $newSchool = $newDb
                        ->table('schools')
                        ->select('id')
                        ->where('old_id', $oldSchool->school_id)
                        ->first();
                    if ($newSchool) {
                        $insertableActivePassLimitArray[] = [
                            'student_pass' => $activePassLimit->stu_status,
                            'proxy_pass' =>
                                $activePassLimit->prx_status >= 1 ? 1 : 0, // We check if prx_status is greater than 1 because of combined in ehpv1 teacher override feature.
                            'kiosk_pass' => $activePassLimit->ksk_status,
                            'limit' => $activePassLimit->limit,
                            'teacher_override' =>
                                $activePassLimit->prx_status > 1 ? 1 : 0, // If prx_status option is greater than 1 teh the teacher override is enabled.
                            'school_id' => $newSchool->id,
                            'created_at' => now(),
                            'updated_at' => now()
                        ];
                    }
                }
            }

            try {
                $newDb
                    ->table('active_pass_limits')
                    ->insert($insertableActivePassLimitArray);
                $this->output->progressAdvance(
                    count($insertableActivePassLimitArray)
                );
            } catch (\Exception $e) {
                $this->info($e->getMessage());
            }
        }

        $this->output->progressFinish();
    }
}
