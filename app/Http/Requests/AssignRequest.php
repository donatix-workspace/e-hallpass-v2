<?php

namespace App\Http\Requests;

use App\Rules\CheckIfRoomExistOnAssigmentRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AssignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin() || auth()->user()->isStaff();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'room_ids_and_pins' => ['array', 'sometimes'],
            'room_ids_and_pins.*.room_id' => [
                'required', 'integer', Rule::exists('rooms', 'id')->where('school_id', optional(auth()->user())->school_id), 'distinct'
            ],
            'room_ids_and_pins.*.pin' => ['string', 'nullable', 'max:6'],
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'room_ids_and_pins.*.room_id.distinct' => 'Warning! Same room is selected multiple times',
        ];
    }
}
