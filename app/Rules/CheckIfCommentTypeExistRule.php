<?php

namespace App\Rules;

use App\Models\AppointmentPass;
use App\Models\Pass;
use App\Models\RecurrenceAppointmentPass;
use Illuminate\Contracts\Validation\Rule;

class CheckIfCommentTypeExistRule implements Rule
{
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value === Pass::class || $value === AppointmentPass::class;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
