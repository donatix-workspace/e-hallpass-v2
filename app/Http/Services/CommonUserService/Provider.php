<?php

namespace App\Http\Services\CommonUserService;


class Provider
{

    protected $clientId;
    protected $clientSecret;
    protected $tokenUrl;
    protected $infoUrl;


    public function __construct($providerId)
    {
        $providerConfig = config("commonuserservice.providers.{$providerId}");
        $this->clientId = $providerConfig['client_id'];
        $this->clientSecret = $providerConfig['client_secret'];
        $this->tokenUrl = $providerConfig['token_url'];
        $this->infoUrl = $providerConfig['info_url'];
    }

    public function getClientId()
    {
        return $this->clientId;
    }

    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    public function getTokenUrl()
    {
        return $this->tokenUrl;
    }

    public function getInfoUrl()
    {
        return $this->infoUrl;
    }
}
