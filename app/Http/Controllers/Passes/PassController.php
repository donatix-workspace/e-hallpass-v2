<?php

namespace App\Http\Controllers\Passes;

use App\Events\AdminPassCreated;
use App\Events\PassCanceled;
use App\Events\PassCreated;
use App\Events\PinStatusEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\AutoApproveRequest;
use App\Http\Requests\AutoCheckInRequest;
use App\Http\Requests\PassUpdateRequest;
use App\Http\Requests\PassRequest;
use App\Http\Resources\CurrentPassResource;
use App\Http\Resources\PassCollection;
use App\Http\Resources\PassEntityCollection;
use App\Http\Resources\PassResource;
use App\Models\AutoPass;
use App\Models\AutoPassLimit;
use App\Models\AutoPassPreference;
use App\Models\Pass;
use App\Models\Pin;
use App\Models\PinAttempt;
use App\Models\School;
use App\Models\User;
use App\Notifications\Push\PassModified;
use App\Notifications\Push\StudentNotification;
use Cache;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Notification;
use Log;

/**
 * @group Passes functionality
 */
class PassController extends Controller
{
    /**
     * Create a pass
     *
     * @bodyParam from_id integer required Current destination of the user
     * @bodyParam to_id integer required To what destination user wants to go
     * @bodyParam from_type string required Current destination type (Room or User)
     * @bodyParam to_type string required Target destination type (Room or User)
     * @bodyParam comment string required The reason why the user start the pass.
     *
     * @param PassRequest $request
     * @return PassResource|JsonResponse
     * @throws AuthorizationException
     * @apiResourceModel App\Models\Pass
     * @apiResource App\Http\Resources\PassResource
     * @authenticated
     */
    public function store(PassRequest $request)
    {
        $this->authorize('create', Pass::class);

        $requestLock = Cache::lock('lock.passes.user.' . auth()->user()->id, 2);

        try {
            $requestLock->block(2);

            $passValidation = Pass::validate($request);

            if ($passValidation !== null) {
                return $passValidation;
            }

            $pass = Pass::create([
                'user_id' => auth()->user()->id,
                'from_id' => $request->input('from_id'),
                'from_type' => $request->input('from_type'),
                'to_id' => $request->input('to_id'),
                'to_type' => $request->input('to_type'),
                'school_id' => auth()->user()->school_id,
                'requested_by' => auth()->user()->id,
                'requested_at' => Carbon::now(),
                'type' => Pass::STUDENT_CREATED_PASS
            ])->refresh();

            if (
                $request->input('comment') !== null &&
                $request->input('comment') !== ''
            ) {
                $pass->comments()->create([
                    'comment' => $request->input('comment'),
                    'user_id' => auth()->user()->id,
                    'school_id' => auth()->user()->school_id
                ]);
            }

            event(new PassCreated($pass, false));

            event(
                new AdminPassCreated(
                    Pass::with('parent')
                        ->without('child')
                        ->where('id', $pass->id)
                        ->firstOrFail()
                        ->append(['flags', 'expire_after'])
                )
            );

            //            Notification::send(
            //                $pass,
            //                new StudentNotification('PassCreated', [
            //                    'notifyStudent' => false
            //                ])
            //            );

            $pass->refreshDashboard();

            return new PassResource($pass);
        } catch (LockTimeoutException $e) {
            Log::error($e);
        } finally {
            $requestLock->release();
        }

        return response()->json(
            [
                'data' => [],
                'status' => __('fail'),
                'message' => __(
                    'Please wait before making another pass request!'
                )
            ],
            422
        );
    }

    /**
     * Show the pass
     * @urlParam pass integer required The id of the pass
     * @header Accept application/json
     *
     * @response {
     *      "data": {
     *          "pass": {},
     *          "is_parent": true,
     *          "has_parent": false,
     *          "parent": {},
     *          "settings": {},
     *          "auto_pass": {"is_valid": false, "mode": null},
     *          "auto_check_in": {"is_valid": true}
     *      },
     *      "status": "success"
     * }
     * @return CurrentPassResource
     * @authenticated
     * @throws AuthorizationException
     */
    public function show()
    {
        $this->authorize('view', [auth()->user(), Pass::class]);

        // If there's no pass provided find the active one from the day.
        $passActiveWithoutAppointment = Pass::onWriteConnection()
            ->without('child')
            ->with('parent')
            ->ofUserId(auth()->user()->id)
            ->fromSchoolId(auth()->user()->school_id)
            ->orderBy('created_at', 'desc')
            ->where('type', '!=', Pass::APPOINTMENT_PASS)
            ->first();

        $pass = Pass::onWriteConnection()
            ->without('child')
            ->with('parent')
            ->ofUserId(auth()->user()->id)
            ->fromSchoolId(auth()->user()->school_id)
            ->orderBy('created_at', 'desc')
            ->first();

        if ($pass !== null) {
            if (
                $pass->type === Pass::APPOINTMENT_PASS &&
                optional($passActiveWithoutAppointment)->pass_status === 1
            ) {
                $pass = $passActiveWithoutAppointment;
            }
            $pass->append(['flags', 'expire_after']);
        }

        return new CurrentPassResource($pass);
    }

    /**
     * Get student pass history.
     *
     * @authenticated
     * @return PassCollection
     * @throws AuthorizationException
     */
    public function history()
    {
        $this->authorize('history', Pass::class);

        $schoolTimezone = optional(
            School::findCacheFirst(auth()->user()->school_id)
        )->getTimezone();

        $startDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->startOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));
        $endDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->endOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));

        $passes = Pass::ofUserId(auth()->user()->id)
            ->without([
                'createdByUser',
                'completedByUser',
                'comments',
                'latestComment',
                'requestedByUser',
                'approvedByUser'
            ])
            ->fromSchoolId(auth()->user()->school_id)
            ->whereParentId(null)
            ->orderBy('created_at', 'desc')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where(function ($query) {
                $query
                    ->where('pass_status', Pass::ACTIVE)
                    ->where('approved_at', '!=', null)
                    ->orWhere(function ($query) {
                        $query
                            ->where('completed_by', '!=', null)
                            ->orWhere('completed_at', '!=', null);
                    });
            })
            ->get()
            ->append(['ended_at', 'signatures']);

        return new PassCollection($passes);
    }

    /**
     * Update the pass status
     *
     * @bodyParam approval_message string Teacher approval message when uses pin
     * @bodyParam action integer required Pass approve action Example: 1 (APPROVE), 2 (END), 3 (Arrived), 4 (Returning/InOut)
     * @bodyParam return_type string Determine whether the user is returning or it's directly out Example: 'inout' OR 'return'
     * @urlParam pass integer required The id of the pass
     *
     * Approval Actions:
     * 1. Approve $approvalAction = 1;
     * 2. End/Keep $approvalAction = 2;
     * 3. Arrived $approvalAction = 3;
     * 4. In/Out & Returning $approvalAction = 4;
     * 5. Cancel
     *
     * @param PassUpdateRequest $request
     * @param Pass $pass
     * @return PassResource|JsonResponse
     * @throws AuthorizationException
     * @apiResourceModel App\Models\Pass
     * @apiResource App\Http\Resources\PassResource
     * @authenticated
     */
    public function update(PassUpdateRequest $request, Pass $pass)
    {
        $this->authorize('update', $pass);

        $approveAction = $request->input('action');
        $passNoteMessage = $request->input('approval_message');

        $pin = Pin::fromSchoolId(auth()->user()->school_id)
            ->fromType(Pin::USER_TYPE)
            ->where('pin', $request->input('teacher_pin'))
            ->first();

        if ($pin !== null && $pin->pinnable->status === User::INACTIVE) {
            return response()->json(
                [
                    'data' => [
                        'id' => $pass->id
                    ],
                    'message' => __(
                        'This user is no longer active. Please try again with another PIN.'
                    ),
                    'status' => __('fail')
                ],
                422
            );
        }

        if (
            $pin === null &&
            $request->input('action') !== Pass::ACTION_EXPIRE
        ) {
            $createPinAttempt = PinAttempt::create([
                'school_id' => auth()->user()->school_id,
                'pass_id' => $pass->id,
                'status' => Pin::ACTIVE,
                'user_id' => auth()->user()->id
            ]);

            event(
                new PinStatusEvent(
                    auth()->user()->id,
                    auth()->user()->pin_attempts_count,
                    auth()->user()->pin_attempts_count >=
                        PinAttempt::LOGOUT_PIN_ATTEMPTS,
                    auth()->user()->pin_attempts_count ===
                        PinAttempt::LOGOUT_PIN_ATTEMPTS
                )
            );
            //            Notification::send(
            //                $pass,
            //                new StudentNotification('PinStatusEvent', [
            //                    'userId' => auth()->user()->id,
            //                    'pinAttempts' => auth()->user()->pin_attempts_count,
            //                    'showPinMessage' =>
            //                        auth()->user()->pin_attempts_count >=
            //                        PinAttempt::LOGOUT_PIN_ATTEMPTS,
            //                    'locked' =>
            //                        auth()->user()->pin_attempts_count ===
            //                        PinAttempt::LOGOUT_PIN_ATTEMPTS
            //                ])
            //            );

            if (
                auth()->user()->pin_attempts_count ===
                PinAttempt::LOGOUT_PIN_ATTEMPTS
            ) {
                auth()
                    ->user()
                    ->token()
                    ->delete();
            }

            return response()->json(
                [
                    'data' => [
                        'id' => $pass->id,
                        'logout' =>
                            auth()->user()->pin_attempts_count ===
                            PinAttempt::LOGOUT_PIN_ATTEMPTS
                    ],
                    'message' => __('pin.invalid'),
                    'status' => __('fail')
                ],
                422
            );
        }

        if (auth()->user()->pin_attempts_count >= 1) {
            // Reset pin attempt on successfull pin.
            PinAttempt::whereUserId(auth()->user()->id)
                ->where('school_id', auth()->user()->school_id)
                ->update(['status' => Pin::INACTIVE]);

            event(
                new PinStatusEvent(
                    auth()->user()->id,
                    auth()->user()->pin_attempts_count,
                    false,
                    false
                )
            );
            //            Notification::send(
            //                $pass,
            //                new StudentNotification('PinStatusEvent', [
            //                    'userId' => auth()->user()->id,
            //                    'pinAttempts' => auth()->user()->pin_attempts_count,
            //                    'showPinMessage' => false,
            //                    'locked' => false
            //                ])
            //            );
        }

        switch ($approveAction) {
            case Pass::ACTION_APPROVE:
                return $pass->approve($pin, $passNoteMessage);
            case Pass::ACTION_END:
                return $pass->end($pin, $passNoteMessage);
            case Pass::ACTION_ARRIVE:
                return $pass->arrive($pin, $passNoteMessage);
            case Pass::ACTION_RETURN_IN_OUT:
                return $pass->returningOrInOut(
                    $pin,
                    $passNoteMessage,
                    $type = $request->input('return_type')
                );
            case Pass::ACTION_CANCEL:
                return $pass->cancel($pin);
            case Pass::ACTION_EXPIRE:
                return $pass->expire();
        }
        return response()->json(
            [
                'data' => [],
                'message' => __('passes.update.invalid.action'),
                'status' => __('fail')
            ],
            422
        );
    }

    /**
     * Auto Approve the pass when its auto pass.
     *
     * @bodyParam action integer required Pass approve action Example: 1 (APPROVE), 2 (END)
     * @urlParam pass integer required The id of the pass
     *
     * @param AutoApproveRequest $request
     * @param Pass $pass
     * @return PassResource|JsonResponse
     * @apiResourceModel App\Models\Pass
     * @apiResource App\Http\Resources\PassResource
     * @throws AuthorizationException
     */
    public function autoApprove(AutoApproveRequest $request, Pass $pass)
    {
        $this->authorize('autoApprove', $pass);

        $autoPass = AutoPass::ofRoomId($pass->to_id)->where(
            'school_id',
            auth()->user()->school_id
        );

        if (!$autoPass->exists()) {
            return response()->json(
                [
                    'data' => ['id' => $pass->id],
                    'message' => __(
                        'passes.auto.approve.for.that.room.not.exists'
                    ),
                    'status' => __('fail')
                ],
                422
            );
        }

        $autoPassRecord = $autoPass->first();

        $autoPassPreference = AutoPassPreference::where(
            'auto_pass_id',
            $autoPassRecord->id
        )
            ->ofUserId($pass->from_id)
            ->where('school_id', auth()->user()->school_id)
            ->first();

        $approveAction = $request->input('action');

        if ($pass->isCanceled()) {
            return response()->json([
                'data' => ['id' => $pass->id],
                'message' => __('passes.invalid.action'),
                'status' => __('fail')
            ]);
        }

        $autoPassLimit =
            $pass->from_type === User::class
                ? AutoPassLimit::where('user_id', $pass->from_id)
                    ->where('school_id', $pass->school_id)
                    ->first()
                : null;

        if (optional($autoPassLimit)->limit < 0) {
            return response()->json([
                'data' => ['id' => $pass->id],
                'message' => __('passes.pass.limit.turned.off'),
                'status' => __('fail')
            ]);
        }

        if ($autoPassPreference === null) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('passes.room.has.no.preference'),
                    'status' => __('fail')
                ],
                422
            );
        }

        // Approve
        if (
            $approveAction === Pass::ACTION_APPROVE &&
            $autoPassPreference->isInCorrectMode()
        ) {
            if ($pass->isCompleted() || $pass->isCanceled()) {
                return response()->json(
                    [
                        'data' => ['id' => $this->id],
                        'message' => __('passes.invalid.action'),
                        'status' => __('fail')
                    ],
                    422
                );
            }

            return $pass->approve(null, null, false, 0, Pass::BY_AUTOPASS);
        }

        // End if the pass mode is start_and_stop
        if (
            $approveAction === Pass::ACTION_END &&
            $autoPassPreference->isModeStartAndStop()
        ) {
            return $pass->end(null, null, Pass::BY_AUTOPASS);
        }

        return response()->json(
            [
                'data' => ['id' => $pass->id],
                'message' => __('passes.approval.not.approved'),
                'status' => __('fail')
            ],
            422
        );
    }

    /**
     * Auto Check-In when the school has Auto-checkIn module enabled (Pass being auto check by user)
     *
     * @urlParam pass integer required The id of the pass
     *
     * @param AutoCheckInRequest $request
     * @param Pass $pass
     * @return PassResource|JsonResponse
     * @apiResourceModel App\Models\Pass
     * @apiResource App\Http\Resources\PassResource
     * @throws AuthorizationException
     * @authenticated
     */
    public function autoCheckIn(AutoCheckInRequest $request, Pass $pass)
    {
        $this->authorize('autoCheckIn', $pass);

        return $pass->checkIn($request);
    }

    /**
     * Cancel a pass.
     *
     * @urlParam pass integer required The id of the pass.
     *
     * @authenticated
     * @param Pass $pass
     * @return PassResource|JsonResponse
     * @apiResourceModel App\Models\Pass
     * @apiResource App\Http\Resources\PassResource
     * @throws AuthorizationException
     */
    public function cancel(Pass $pass)
    {
        $this->authorize('cancel', $pass);

        if ($pass->cancelValidator() !== null) {
            return $pass->cancelValidator();
        }

        if (
            $pass->isCanceled() ||
            $pass->isCompleted() ||
            $pass->isApproved() ||
            $pass->isExpired()
        ) {
            return response()->json(
                [
                    'data' => ['id' => $this->id],
                    'message' => __('passes.invalid.action'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $pass->update([
            'pass_status' => 0,
            'canceled_at' => Carbon::now()
        ]);

        event(new PassCanceled($pass, true));
        //        Notification::send($pass, new StudentNotification('PassCanceled'));

        return new PassResource($pass->refresh());
    }

    /**
     * Get users with active passes.
     *
     * @authenticated
     * @return PassEntityCollection
     */
    public function activePasses()
    {
        $activeUsersIds = Pass::fromSchoolId(auth()->user()->school_id)
            ->active()
            ->whereCanceledAt(null)
            ->get(['id', 'user_id', 'school_id']);

        return new PassEntityCollection([
            'users_ids' => $activeUsersIds
        ]);
    }
}
