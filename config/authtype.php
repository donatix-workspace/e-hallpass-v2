<?php

return [
    1 => 'Manual',
    1001 => 'Hidden',
    10 => 'Google',
    20 => 'Microsoft',
    101 => 'ClassLink',
    102 => 'Clever',
    103 => 'GG4L'
];
