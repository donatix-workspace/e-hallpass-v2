<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kiosk Password Template</title>
    <!-- Lato font include here -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300i,400,400i,700|Roboto" rel="stylesheet">
    <style>
        body {
            font-family: 'Lato', Arial, sans-serif;
        }

        @media screen and (max-width: 600px) {
            .m-center {
                text-align: center;
            }

            .m-button {
                width: 300px !important;
            }

            .pass-content p {
                text-align: center !important;
            }

            .pass-date {
                font-size: 20px !important;
                line-height: 24px !important;
            }

            .pass-info-wrapper h3,
            .pass-info-wrapper h2 {
                font-size: 20px !important;
                line-height: 24px !important;
            }

            .pass-info-wrapper {
                padding: 26px 6px 26px !important;
            }

            .pass-info-wrapper > div {
                width: auto !important;
                padding: 0 12px;
            }

        }
    </style>
</head>
<body>
<div class="main-wrapper" style="max-width: 600px; width: 100%; margin: 0 auto; background-color: #fff">
    <table cellpadding="0" cellspacing="0" style="max-width: 600px; width: 100%;" class="m-center">
        <tr>
            <td style="padding: 30px 0 37px 0;text-align: center;">
                <a href="{{config('app.front_url').'/login'}}"><img src="{{Storage::disk('s3')->url('ehp-700x350.png')}}" border="0"
                                                                    alt="Ehallpass Logo" height="66px"
                                                                    width="125px"/></a>
            </td>
        </tr>
        <tr>
            <td>
                <strong class="pass-date"
                        style="color: #45455A; font-size: 25px; line-height: 30px; font-family: 'Lato', Arial, sans-serif; font-weight: 400;">Kiosk
                    Password Request.</strong>
                <p style="color: #818181; font-size: 30px; line-height: 22px; font-family: 'Lato', Arial, sans-serif;">{{$data['password']}}</p>
                </br></br>
                <p style="margin: 10px 0 32px 0; color: #818181; font-size: 16px; line-height: 22px; font-family: 'Lato', Arial, sans-serif;">
                    You're receiving this email because you requested a kiosk password for your e-hallpass Account.
                    </br><strong style="color: #A2A2A2;">If you did not request this change, you can safely ignore this
                        email</strong>.</p>

        </tr>
        <tr>
            <td style="text-align: center; padding: 40px 32px 20px 32px;">
                <span
                    style="font-family: 'Lato', Arial, sans-serif; color: #B0AEB0; font-size: 14px; letter-spacing: 0.13px; line-height: 17px; border-top: 1px solid #C3C3C3; display: block; padding-top: 30px;">© {{now()->year}} e-hallpass. The Online Digital Hall Pass Solution.</span>
            </td>
        </tr>

    </table>
</div>
</body>
</html>
