<?php

namespace App\Http\Resources;

use App\Models\School;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\RecurrenceAppointmentPass */
class RecurrenceAppointmentPassResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (optional($this)->id === null) {
            return [];
        }

        $school = School::findCacheFirst($this->school_id);

        return [
            'id' => $this->id,
            'created_by' => $this->created_by,
            'created_by_user' => [
                'name' => $this->createdByUser->name,
                'first_name' => $this->createdByUser->first_name,
                'last_name' => $this->createdByUser->last_name,
                'id' => $this->createdByUser->id
            ],
            'from_type' => $this->from_type,
            'from_id' => $this->from_id,
            'to_type' => $this->to_type,
            'to_id' => $this->to_id,
            'for_date' => $this->for_date,
            'recurrence_type' => $this->recurrence_type,
            'recurrence_days' => $this->recurrence_days,
            'recurrence_week' => $this->recurrence_week,
            'recurrence_end_at' => School::convertTimeToCorrectTimezone($this->recurrence_end_at, $school->getTimezone(), true),
            'reason' => $this->reason,
            'expired_at' => School::convertTimeToCorrectTimezone($this->expired_at, $school->getTimezone()),
            'created_at' => School::convertTimeToCorrectTimezone($this->created_at, $school->getTimezone()),
            'updated_at' => School::convertTimeToCorrectTimezone($this->updated_at, $school->getTimezone()),
            'appointment_passes_count' => $this->appointment_passes_count,
            'comments_count' => $this->comments_count,

            'user_id' => $this->user_id,
            'period_id' => $this->period_id,
            'school_id' => $this->school_id,

            'comments' => CommentCollection::collection($this->whenLoaded('comments')),
        ];

    }

    /**
     * @param Request $request
     * @return array|string[]
     */
    public function with($request)
    {
        return [
            'status' => 'success'
        ];
    }
}
