<?php

namespace Database\Factories;

use App\Models\PolarityReport;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PolarityReportFactory extends Factory
{
    protected $model = PolarityReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'destination_type' => $this->faker->word,
            'destination_id' => $this->faker->randomNumber(),
            'prevented_from' => $this->faker->word,
            'prevented_at' => $this->faker->word,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'user_id' => function () {
                return User::factory()->create()->id;
            },
            'out_user_id' => function () {
                return User::factory()->create()->id;
            },

            'school_id' => function () {
                return School::factory()->create()->id;
            },
        ];
    }
}
