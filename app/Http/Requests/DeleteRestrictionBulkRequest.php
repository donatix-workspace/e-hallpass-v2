<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DeleteRestrictionBulkRequest extends FormRequest
{
    public function rules()
    {
        return [
            'room_restriction_ids' => 'array|required',
            'room_restriction_ids.*' => ['integer', 'required', 'min:1', Rule::exists('rooms_restrictions', 'id')->where('school_id', optional(auth()->user())->school_id)]
        ];
    }
}
