<?php

namespace App\Jobs;

use App\Events\AppointmentPassCreated;
use App\Events\AppointmentPassPageRefreshEvent;
use App\Models\AppointmentPass;
use App\Models\RecurrenceAppointmentPass;
use App\Models\Transparency;
use App\Notifications\Push\StudentNotification;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DB;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Throwable;

class UpdateAppointmentsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $recurrenceAppointmentPass;

    /**
     * Create a new job instance.
     *
     * @param RecurrenceAppointmentPass $recurrenceAppointmentPass
     */
    public function __construct(
        RecurrenceAppointmentPass $recurrenceAppointmentPass
    ) {
        $this->recurrenceAppointmentPass = $recurrenceAppointmentPass;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception|Throwable
     */
    public function handle()
    {
        $recurrenceAppointmentPass = $this->recurrenceAppointmentPass;

        AppointmentPass::where(
            'recurrence_appointment_pass_id',
            $recurrenceAppointmentPass->id
        )
            ->where('for_date', '>', now()->toDateTimeString())
            ->where('pass_id', null)
            ->delete();

        $appointmentPassesForTheDay = [];
        $appointmentPassesForTheWeek = [];
        $appointmentPassesForTheMonth = [];

        if ($recurrenceAppointmentPass->recurrence_type) {
            if (
                $recurrenceAppointmentPass->recurrence_type ===
                config('recurrences.daily')
            ) {
                $days = CarbonPeriod::create(
                    $recurrenceAppointmentPass->for_date,
                    Carbon::parse(
                        $recurrenceAppointmentPass->recurrence_end_at
                    )->endOfDay()
                );

                $insertedForThatWeekCount = 0;
                foreach ($days as $day) {
                    $forDateTimeFormat = Carbon::parse(
                        $recurrenceAppointmentPass->for_date
                    )
                        ->setTimezone(config('app.timezone'))
                        ->format('H:i');
                    if (
                        !in_array($day->dayOfWeek, [
                            Carbon::SATURDAY,
                            Carbon::SUNDAY
                        ]) &&
                        $insertedForThatWeekCount <=
                            AppointmentPass::GENERATE_DAILY_PER_JOB &&
                        Carbon::parse(
                            $day->toDateString() . " $forDateTimeFormat"
                        )->isFuture()
                    ) {
                        $appointmentPassesForTheDay[] = [
                            'created_by' =>
                                $recurrenceAppointmentPass->created_by,
                            'from_id' => $recurrenceAppointmentPass->from_id,
                            'from_type' =>
                                $recurrenceAppointmentPass->from_type,
                            'to_id' => $recurrenceAppointmentPass->to_id,
                            'to_type' => $recurrenceAppointmentPass->to_type,
                            'user_id' => $recurrenceAppointmentPass->user_id,
                            'recurrence_appointment_pass_id' =>
                                $recurrenceAppointmentPass->id,
                            'for_date' => Carbon::parse(
                                $day->toDateString() . " $forDateTimeFormat"
                            )->setTimezone(config('app.timezone')),
                            'school_id' =>
                                $recurrenceAppointmentPass->school_id,
                            'period_id' =>
                                $recurrenceAppointmentPass->period_id,
                            'confirmed_by_teacher_at' => now(),
                            'pass_id' => null,
                            'reason' => $recurrenceAppointmentPass->reason,
                            'created_at' => Carbon::now()->setTimezone(
                                config('app.timezone')
                            ),
                            'updated_at' => Carbon::now()->setTimezone(
                                config('app.timezone')
                            )
                        ];
                        $insertedForThatWeekCount++;
                    }
                }

                if (
                    !Transparency::isEnabled() ||
                    (Transparency::isEnabled() &&
                        $recurrenceAppointmentPass->isCoverTheTransparency())
                ) {
                    event(
                        new AppointmentPassCreated(
                            $recurrenceAppointmentPass,
                            $recurrenceAppointmentPass->created_by
                        )
                    );
                    //                    Notification::send(
                    //                        $recurrenceAppointmentPass,
                    //                        new StudentNotification('AppointmentPassCreated')
                    //                    );
                }
            }

            if (
                $recurrenceAppointmentPass->recurrence_type ===
                config('recurrences.weekly')
            ) {
                $days = CarbonPeriod::create(
                    $recurrenceAppointmentPass->for_date,
                    Carbon::parse(
                        $recurrenceAppointmentPass->recurrence_end_at
                    )->endOfDay()
                );

                $recurrenceDays = collect(
                    json_decode($recurrenceAppointmentPass->recurrence_days)
                )->get('days');
                $endRecurrenceDate = Carbon::parse(
                    $recurrenceAppointmentPass->recurrence_end_at
                );
                $forDateTimeFormat = Carbon::parse(
                    $recurrenceAppointmentPass->for_date
                )
                    ->setTimezone(config('app.timezone'))
                    ->format('H:i');

                if (!$endRecurrenceDate->isPast()) {
                    foreach ($days as $day) {
                        if (
                            in_array($day->englishDayOfWeek, $recurrenceDays) &&
                            Carbon::parse(
                                $day->toDateString() . " $forDateTimeFormat"
                            )->isFuture()
                        ) {
                            $appointmentPassesForTheWeek[] = [
                                'created_by' =>
                                    $recurrenceAppointmentPass->created_by,
                                'from_id' =>
                                    $recurrenceAppointmentPass->from_id,
                                'from_type' =>
                                    $recurrenceAppointmentPass->from_type,
                                'to_id' => $recurrenceAppointmentPass->to_id,
                                'to_type' =>
                                    $recurrenceAppointmentPass->to_type,
                                'user_id' =>
                                    $recurrenceAppointmentPass->user_id,
                                'recurrence_appointment_pass_id' =>
                                    $recurrenceAppointmentPass->id,
                                'for_date' => Carbon::parse(
                                    $day->toDateString() . " $forDateTimeFormat"
                                )->setTimezone(config('app.timezone')),
                                'school_id' =>
                                    $recurrenceAppointmentPass->school_id,
                                'confirmed_by_teacher_at' => now(),
                                'period_id' =>
                                    $recurrenceAppointmentPass->period_id,
                                'pass_id' => null,
                                'reason' => $recurrenceAppointmentPass->reason,
                                'created_at' => Carbon::now()->setTimezone(
                                    config('app.timezone')
                                ),
                                'updated_at' => Carbon::now()->setTimezone(
                                    config('app.timezone')
                                )
                            ];
                        }
                    }
                }

                if (
                    !Transparency::isEnabled() ||
                    (Transparency::isEnabled() &&
                        $recurrenceAppointmentPass->isCoverTheTransparency())
                ) {
                    event(
                        new AppointmentPassCreated(
                            $recurrenceAppointmentPass,
                            $recurrenceAppointmentPass->created_by
                        )
                    );
                    //                    Notification::send(
                    //                        $recurrenceAppointmentPass,
                    //                        new StudentNotification('AppointmentPassCreated')
                    //                    );
                }
            }

            if (
                $recurrenceAppointmentPass->recurrence_type ===
                config('recurrences.monthly')
            ) {
                $days = CarbonPeriod::create(
                    $recurrenceAppointmentPass->for_date,
                    Carbon::parse(
                        $recurrenceAppointmentPass->recurrence_end_at
                    )->endOfDay()
                );
                $recurrenceWeek = json_decode(
                    $recurrenceAppointmentPass->recurrence_week,
                    true
                );
                $forDateTimeFormat = Carbon::parse(
                    $recurrenceAppointmentPass->for_date
                )
                    ->setTimezone(config('app.timezone'))
                    ->format('H:i');

                $insertedForTheMonth = 0;

                foreach ($days as $day) {
                    if (
                        array_key_exists(
                            $day->weekNumberInMonth,
                            $recurrenceWeek
                        ) &&
                        $recurrenceWeek[$day->weekNumberInMonth] ===
                            $day->englishDayOfWeek &&
                        $insertedForTheMonth <=
                            AppointmentPass::GENERATE_MONTHLY_PER_JOB &&
                        Carbon::parse(
                            $day->toDateString() . " $forDateTimeFormat"
                        )->isFuture()
                    ) {
                        $appointmentPassesForTheMonth[] = [
                            'created_by' =>
                                $recurrenceAppointmentPass->created_by,
                            'from_id' => $recurrenceAppointmentPass->from_id,
                            'from_type' =>
                                $recurrenceAppointmentPass->from_type,
                            'to_id' => $recurrenceAppointmentPass->to_id,
                            'to_type' => $recurrenceAppointmentPass->to_type,
                            'user_id' => $recurrenceAppointmentPass->user_id,
                            'recurrence_appointment_pass_id' =>
                                $recurrenceAppointmentPass->id,
                            'for_date' => Carbon::parse(
                                $day->toDateString() . " $forDateTimeFormat"
                            )->setTimezone(config('app.timezone')),
                            'confirmed_by_teacher_at' => now(),
                            'school_id' =>
                                $recurrenceAppointmentPass->school_id,
                            'period_id' =>
                                $recurrenceAppointmentPass->period_id,
                            'pass_id' => null,
                            'reason' => $recurrenceAppointmentPass->reason,
                            'created_at' => Carbon::now()->setTimezone(
                                config('app.timezone')
                            ),
                            'updated_at' => Carbon::now()->setTimezone(
                                config('app.timezone')
                            )
                        ];
                        $insertedForTheMonth++;
                    }
                }

                if (
                    !Transparency::isEnabled() ||
                    (Transparency::isEnabled() &&
                        $recurrenceAppointmentPass->isCoverTheTransparency())
                ) {
                    event(
                        new AppointmentPassCreated(
                            $recurrenceAppointmentPass,
                            $recurrenceAppointmentPass->created_by
                        )
                    );
                    //                    Notification::send(
                    //                        $recurrenceAppointmentPass,
                    //                        new StudentNotification('AppointmentPassCreated')
                    //                    );
                }
            }

            try {
                DB::beginTransaction();

                if (count($appointmentPassesForTheMonth) > 0) {
                    foreach (
                        $appointmentPassesForTheMonth
                        as $appointmentPass
                    ) {
                        $appointment = AppointmentPass::create(
                            $appointmentPass
                        );

                        $appointment->comments()->create([
                            'comment' => $appointment->reason,
                            'user_id' => $appointment->created_by,
                            'school_id' => $appointment->school_id
                        ]);
                    }
                }

                if (count($appointmentPassesForTheWeek) > 0) {
                    foreach ($appointmentPassesForTheWeek as $appointmentPass) {
                        $appointment = AppointmentPass::create(
                            $appointmentPass
                        );

                        $appointment->comments()->create([
                            'comment' => $appointment->reason,
                            'user_id' => $appointment->created_by,
                            'school_id' => $appointment->school_id
                        ]);
                    }
                }

                if (count($appointmentPassesForTheDay) > 0) {
                    foreach ($appointmentPassesForTheDay as $appointmentPass) {
                        $appointment = AppointmentPass::create(
                            $appointmentPass
                        );

                        $appointment->comments()->create([
                            'comment' => $appointment->reason,
                            'user_id' => $appointment->created_by,
                            'school_id' => $appointment->school_id
                        ]);
                    }
                }

                DB::commit();

                event(
                    new AppointmentPassPageRefreshEvent(
                        $recurrenceAppointmentPass->school_id
                    )
                );
            } catch (Throwable $e) {
                DB::rollBack();

                Log::error($e->getMessage());
            }
        }
    }
}
