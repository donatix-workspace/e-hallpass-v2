-- BEFORE RUN THIS SCRIPT PLEASE ADD updated_at = NOW() (Just in case you don't want to migrate all of the passes again to the ES

START TRANSACTION ;

SET FOREIGN_KEY_CHECKS=0;
START TRANSACTION;
UPDATE ehp_v2_prod.`passes` as child, ehp_v2_prod.`passes` as parent
SET child.parent_id = parent.id, child.updated_at = NOW(), parent.updated_at = NOW()
WHERE child.old_parent_id = parent.old_id AND child.old_parent_id > 0 AND parent.school_id = 152 AND child.school_id = 152;
COMMIT;
SET FOREIGN_KEY_CHECKS=1;


-- Completed by
UPDATE
    ehp_v2_prod.passes
SET
    user_id = (
        SELECT
            to_id
        FROM
            duplicate_users_clone
        WHERE
                from_id = passes.user_id
        LIMIT 1)
WHERE
        school_id = 152
  AND user_id IN(
    SELECT
        from_id FROM duplicate_users_clone);

-- Requested by
UPDATE
    ehp_v2_prod.passes
SET
    requested_by = (
        SELECT
            to_id
        FROM
            duplicate_users_clone
        WHERE
                from_id = passes.requested_by
        LIMIT 1)
WHERE
        school_id = 152
  AND requested_by IN(
    SELECT
        from_id FROM duplicate_users_clone);

-- Approved by
UPDATE
    ehp_v2_prod.passes
SET
    approved_by = (
        SELECT
            to_id
        FROM
            duplicate_users_clone
        WHERE
                from_id = passes.approved_by
        LIMIT 1)
WHERE
        school_id = 152
  AND approved_by IN(
    SELECT
        from_id FROM duplicate_users_clone);

-- Completed by
UPDATE
    ehp_v2_prod.passes
SET
    completed_by = (
        SELECT
            to_id
        FROM
            duplicate_users_clone
        WHERE
                from_id = passes.completed_by
        LIMIT 1)
WHERE
        school_id = 152
  AND completed_by IN(
    SELECT
        from_id FROM duplicate_users_clone);

-- Created By
UPDATE
    ehp_v2_prod.passes
SET
    created_by = (
        SELECT
            to_id
        FROM
            duplicate_users_clone
        WHERE
                from_id = passes.created_by
        LIMIT 1)
WHERE
        school_id = 152
  AND created_by IN(
    SELECT
        from_id FROM duplicate_users_clone);


-- From Id
UPDATE
    ehp_v2_prod.passes
SET
    from_id = (
        SELECT
            to_id
        FROM
            duplicate_users_clone
        WHERE
                from_id = passes.from_id
        LIMIT 1)
WHERE
        school_id = 152
  AND from_type = 'App\\Models\\User'
  AND from_id IN(
    SELECT
        from_id FROM duplicate_users_clone);

-- To ID

UPDATE
    ehp_v2_prod.passes
SET
    to_id = (
        SELECT
            to_id
        FROM
            duplicate_users_clone
        WHERE
                from_id = passes.to_id
        LIMIT 1)
WHERE
        school_id = 152
  AND to_type = 'App\\Models\\User'
  AND to_id IN(
    SELECT
        from_id FROM duplicate_users_clone);

# UPDATE
#     passes
# SET
#     approved_at = (
#         SELECT
#             CONVERT_TZ((
#                            SELECT
#                                outdate FROM ph.epass_temp
#                            WHERE epass_temp.pass_id = passes.old_id
#                            LIMIT 1
#                        ), 'America/New_York', 'UTC')),
#     completed_at = (
#         SELECT
#             CONVERT_TZ((
#                            SELECT
#                                indate FROM ph.epass_temp
#                            WHERE epass_temp.pass_id = passes.old_id
#                            LIMIT 1
#                        ), 'America/New_York', 'UTC'))
# WHERE school_id = 152 AND (SELECT timezone FROM schools WHERE schools.id = passes.school_id limit 1) != 'America/New_York';


-- EDITED FIX
UPDATE passes SET expired_at = NULL
WHERE expired_at is not null and edited_at is not null and approved_at is not null and completed_at is not null  AND approved_by > 0 AND completed_by > 0 and school_id = 152;


-- FIX NO BADGES

UPDATE passes SET expired_at = NULL
WHERE expired_at IS NOT NULL AND system_completed = 0 AND approved_by > 0 AND completed_by > 0 and school_id = 152;

UPDATE passes SET expired_at = NULL
WHERE expired_at IS NOT NULL AND system_completed = 0 AND approved_by > 0 AND completed_by is null  and parent_id is not null and school_id = 152;

-- (SYSTEM COMPLETE FIX)

-- Approved

UPDATE passes SET system_completed = 1,
                  approved_by = NULL,
                  expired_at = NULL
WHERE (SELECT first_name FROM users WHERE id = passes.approved_by LIMIT 1) = 'System Ended' and school_id = 152;

-- Completed
UPDATE passes SET system_completed = 1,
                  completed_by = NULL,
                  expired_at = NULL
WHERE (SELECT first_name FROM users WHERE id = passes.completed_by LIMIT 1) = 'System Ended' and school_id = 152;

-- MINTIME (YELLOW)


UPDATE ehp_v2_prod.passes SET flagged_at = NOW()
WHERE IF((IF((SELECT count(id) FROM (
                                        SELECT id FROM ehp_v2_prod.passes WHERE parent_id = passes.id limit 1
                                    ) as id) > 0, (SELECT TIMEDIFF(
                                                              IF((SELECT completed_at
                                                                  FROM (SELECT completed_at
                                                                        FROM ehp_v2_prod.passes
                                                                        WHERE parent_id = passes.id
                                                                        limit 1) as completed_at) IS NOT NULL,
                                                                 (SELECT completed_at
                                                                  FROM (SELECT completed_at
                                                                        FROM ehp_v2_prod.passes
                                                                        WHERE parent_id = passes.id
                                                                        limit 1) as completed_at),
                                                                 (SELECT approved_at
                                                                  FROM (SELECT approved_at
                                                                        FROM ehp_v2_prod.passes
                                                                        WHERE parent_id = passes.id
                                                                        limit 1) as approved_at)),
                                                              (SELECT passes.approved_at)
                                                              )),
             (SELECT TIMEDIFF(passes.completed_at, passes.approved_at)))) > (SELECT min_time FROM teacher_pass_settings WHERE school_id=passes.school_id limit 1), 1, 0) and school_id = 152;


-- Ended on third state fixes (KSK)
UPDATE ehp_v2_prod.passes SET flagged_at=NULL
WHERE passes.parent_id > 0 AND (SELECT role_id FROM users WHERE id=passes.approved_by limit 1) = 1 AND passes.completed_at IS NULL AND passes.pass_status = 0 AND approved_with = 'autopass' AND `type`='KSK' and school_id = 152 ;
COMMIT ;
