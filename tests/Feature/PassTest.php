<?php

namespace Tests\Feature;

use App\Jobs\GenerateAppointmentsJob;
use App\Models\AppointmentPass;
use App\Models\AutoPass;
use App\Models\AutoPassPreference;
use App\Models\BlockPass;
use App\Models\LocationCapacity;
use App\Models\Module;
use App\Models\Pass;
use App\Models\PassBlock;
use App\Models\PassLimit;
use App\Models\Period;
use App\Models\Pin;
use App\Models\PinAttempt;
use App\Models\RecurrenceAppointmentPass;
use App\Models\Room;
use App\Models\RoomPin;
use App\Models\RoomRestriction;
use App\Models\RoomType;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\StaffSchedule;
use App\Models\TeacherPassSetting;
use App\Models\Transparency;
use App\Models\Unavailable;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\ModuleSeeder;
use Database\Seeders\RolesDatabaseSeeder;
use Database\Seeders\RoomTypeSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class PassTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;
    protected $staffUser;
    protected $teacherUser;
    protected $firstUser;
    protected $secondUser;
    protected $teacherPassSettings;
    protected $pin;
    protected $pass;
    protected $room;


    protected function setUp(): void
    {
        parent::setUp();


        // Run roles migration
        (new RolesDatabaseSeeder())->run();
        (new ModuleSeeder())->run();

        $this->school = School::factory()->createOne(['timezone' => 'Europe/Sofia']);

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->secondUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);

        $this->staffUser = User::factory()->createOne([
            'role_id' => config('roles.staff'),
            'school_id' => $this->school->id
        ]);

        $this->teacherUser = User::factory()->createOne([
            'role_id' => 3,
            'school_id' => $this->school->id
        ]);

        $this->teacherPassSettings = TeacherPassSetting::create([
            'school_id' => $this->school->id,
        ]);

        $this->pin = Pin::create([
            'pinnable_id' => $this->adminUser->id,
            'pinnable_type' => User::class,
            'pin' => '1234',
            'school_id' => $this->adminUser->school_id
        ]);

        $this->pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->secondUser->id,
            'user_id' => $this->firstUser->id
        ]);

        $this->room = Room::factory()->count(1)->for(RoomType::factory()->create([
            'school_id' => $this->school->id,
        ]))->create([
            'school_id' => $this->school->id,
            'trip_type' => 2, // trip one way
            'status' => 1
        ])->first();

        SchoolModule::create([
            'school_id' => $this->school->id,
            'module_id' => 1, // apt pass
            'option_json' => json_encode(['admin_edit' => 0]),
            'permission' => 1,
            'status' => 1,
        ]);

        SchoolModule::create([
            'school_id' => $this->school->id,
            'module_id' => 2, // auto check in
            'option_json' => json_encode(['hello' => 'world']),
            'permission' => 1,
            'status' => 1,
        ]);
    }

    /** @test
     * A student user can create a pass
     * @return void
     */
    public function a_student_user_can_create_a_pass()
    {
        $this->actingAs($this->secondUser)->withHeaders([
            'Accept' => 'application/json',
        ])
            ->post(route('api.v5.passes.create'), [
                'from_id' => $this->adminUser->id,
                'to_id' => $this->teacherUser->id,
                'from_type' => 'App\Models\User',
                'to_type' => 'App\Models\User',
                'comment' => 'Test Comment'
            ])->assertStatus(201) // created
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('passes', ['to_id' => $this->teacherUser->id, 'from_id' => $this->adminUser->id]);
    }

    /** @test
     * A student user can't create a pass with different type
     * @return void
     */
    public function a_student_user_can_t_create_with_different_type()
    {
        $this->actingAs($this->secondUser)->withHeaders([
            'Accept' => 'application/json',
        ])
            ->post(route('api.v5.passes.create'), [
                'from_id' => $this->adminUser->id,
                'to_id' => $this->teacherUser->id,
                'from_type' => 'NoExistingType',
                'to_type' => 'User',
                'comment' => 'Test'
            ])->assertJsonStructure(['errors']);

        $this->assertDatabaseMissing('passes', ['to_id' => $this->firstUser->id, 'from_id' => $this->adminUser->id]);
    }

    /**
     * @test
     * A student user can view pass create resources
     * @return void
     */
    public function a_student_user_can_view_pass_create_resources()
    {
        $this->actingAs($this->secondUser)
            ->withHeaders([
                'Accept' => 'application/json'
            ])
            ->get(route('api.v5.passes'))
            ->assertJsonStructure([
                'data' => [
                    'users',
                    'rooms',
                    'passes' => [
                        'info',
                        'settings'
                    ],
                    'restriction' => [
                        'count'
                    ]
                ],
                'status'
            ])
            ->assertOk();
    }

    /**
     * @test
     * A student user can view own pass resource
     * @return void
     */
    public function a_student_user_can_view_own_pass_resource()
    {
        $this->actingAs($this->firstUser)
            ->get(route('api.v5.passes.show', ['pass' => $this->pass->id]))
            ->assertJsonStructure([
                'data' => [
                    'pass' => [
                        'to_type',
                        'from_type',
                        'created_at',
                        'approved_at',
                        'approved_by',
                        'completed_at',
                        'completed_by',
                        'from',
                        'to'
                    ],
                    'settings' => [
                        'min_time',
                        'awaiting_apt',
                        'white_passes',
                        'max_time',
                        'auto_expire_time',
                        'nurse_expire_time'
                    ],
                    'auto_pass' => [
                        'is_valid',
                        'mode'
                    ],
                    'auto_check_in' => [
                        'is_valid'
                    ]
                ]
            ])
            ->assertOk();
    }

    /**
     * @test
     * A student can't create a pass without comment
     * @return void
     */
    public function a_student_can_t_create_a_pass_without_comment()
    {
        $this->actingAs($this->secondUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->post(route('api.v5.passes.create'), [
                'from_type' => 'App\Models\User',
                'from_id' => 1,
                'to_type' => 'App\Models\User',
                'to_id' => 2,
                'comment' => null
            ])
            ->assertStatus(422)
            ->assertJsonStructure(['errors']);
    }

    /**
     * @test
     * A student can t create a pass if school had blocked passes
     * @return void
     */
    public function a_student_can_t_create_a_pass_if_school_had_blocked_passes()
    {
        $blockPass = BlockPass::create([
            'school_id' => $this->school->id,
            'from_date' => Carbon::yesterday(),
            'to_date' => Carbon::tomorrow(),
            'student_pass' => 1,
            'proxy_pass' => 0,
            'kiosk_pass' => 0,
            'reason' => 'example',
            'message' => 'message'
        ]);


        $this->actingAs($this->secondUser)->withHeaders([
            'Accept' => 'application/json',
        ])
            ->post(route('api.v5.passes.create'), [
                'from_id' => $this->adminUser->id,
                'to_id' => $this->firstUser->id,
                'from_type' => 'App\Models\User',
                'to_type' => 'App\Models\User',
                'comment' => 'Test Comment'
            ])->assertStatus(422);

        $this->assertDatabaseMissing('passes', ['from_id' => $this->adminUser->id, 'to_id' => $this->firstUser->id]);
    }

    /**
     * @test
     * A student user can  approve pass with teacher pin
     * @return void
     */
    public function a_student_user_can_approve_pass_with_teacher_pin()
    {
        $pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->secondUser->id,
            'user_id' => $this->firstUser->id
        ]);

        $this->actingAs($this->firstUser)
            ->withHeaders([
                'Accept' => 'application/json'
            ])
            ->put(route('api.v5.passes.update', ['pass' => $pass]), [
                'teacher_pin' => $this->pin->pin,
                'action' => 1,
            ])
            ->assertSessionHasNoErrors()
            ->assertStatus(200)
            ->assertJsonStructure(['status', 'data' => ['approved_at']]);

        $this->assertDatabaseHas('passes', ['id' => $pass->id]);
        $this->assertNotNull(Pass::find($pass->id)->approved_at);
    }

    /**
     * @test
     * A student user cant auto approve pass with wrong teacher pin
     * @return void
     */
    public function a_student_user_cant_auto_approve_pass_with_wrong_teacher_pin()
    {

        $this->actingAs($this->firstUser)
            ->withHeaders([
                'Accept' => 'application/json'
            ])
            ->put(route('api.v5.passes.update', ['pass' => $this->pass->id]), [
                'teacher_pin' => '1111',
                'action' => 1,
            ])
            ->assertStatus(422)
            ->assertJsonStructure(['status', 'data', 'message']);

        $this->assertDatabaseHas('passes', ['id' => $this->pass->id]);
        $this->assertNull(Pass::find($this->pass->id)->approved_at);
    }

    /**
     * @test
     * A student user can end active pass with teacher pin
     * @return void
     */
    public function a_student_user_can_end_active_pass_with_teacher_pin()
    {
        // Auto approve the pass for the test
        Pass::find($this->pass->id)->update(['approved_at' => Carbon::now(), 'approved_by' => $this->adminUser->id]);

        $this->actingAs($this->firstUser)
            ->withHeaders([
                'Accept' => 'application/json'
            ])->put(route('api.v5.passes.update', ['pass' => $this->pass->id]), [
                'teacher_pin' => $this->pin->pin,
                'action' => 2 // End action
            ])->assertStatus(200)
            ->assertJsonStructure(['data' => ['approved_at', 'completed_at'], 'status']);

        $this->assertDatabaseHas('passes', ['id' => $this->pass->id, 'completed_by' => $this->adminUser->id]);
        $this->assertNotNull(Pass::find($this->pass->id)->completed_at);
        $this->assertNotNull(Pass::find($this->pass->id)->approved_at);
    }

    /**
     * @test
     * A student user can mark pass as arrived
     * @return void
     */
    public function a_student_user_can_mark_pass_as_arrived()
    {
        // Auto approve the pass for the test
        Pass::find($this->pass->id)->update(['approved_at' => Carbon::now(), 'approved_by' => $this->adminUser->id]);

        $this->actingAs($this->firstUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->put(route('api.v5.passes.update', ['pass' => $this->pass->id]), [
                'teacher_pin' => $this->pin->pin,
                'action' => 3 // Arrived
            ])
            ->assertJsonStructure(['data' => [
                'id',
                'parent_id',
            ], 'status'])
            ->assertJson(['data' => [
                'parent_id' => $this->pass->id
            ]])
            ->assertStatus(201); // New parent is created

        $this->assertDatabaseHas('passes', ['parent_id' => $this->pass->id]);
    }

    /**
     * @test
     * A student user can mark pass as returning
     * @return void
     */
    public function a_student_user_can_mark_pass_as_returning()
    {
        // Complete the parent pass
        Pass::find($this->pass->id)->update([
            'approved_at' => Carbon::now(),
            'approved_by' => $this->adminUser->id,
            'completed_at' => Carbon::now(),
            'completed_by' => $this->adminUser->id
        ]);

        // Create pass with parent
        $parentPass = Pass::create([
            'parent_id' => $this->pass->id,
            'from_type' => $this->pass->to_type,
            'to_type' => $this->pass->from_type,
            'from_id' => $this->pass->to_id,
            'to_id' => $this->pass->from_id,
            'user_id' => $this->firstUser->id,
            'school_id' => $this->firstUser->school_id
        ]);

        $this->actingAs($this->firstUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->put(route('api.v5.passes.update', ['pass' => $parentPass->id]), [
                'teacher_pin' => $this->pin->pin,
                'action' => 4, // Returning / In out
                'return_type' => 'return'
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'approved_at',
                    'approved_by',
                ], 'status'
            ]);
        $this->assertDatabaseHas('passes', ['id' => $parentPass->id]);
        $this->assertNotNull($parentPass->refresh()->approved_by);
    }

    /**
     * @test
     * A student user can't approve pas when has maximum pin attempts
     * @return void
     */
    public function a_student_user_can_t_approve_pass_when_has_maximum_pin_attempts()
    {
        //Disapprove  the pass
        Pass::find($this->pass->id)->update([
            'approved_by' => null,
            'approved_at' => null
        ]);

        // Create pin attempts
        PinAttempt::factory()->count(PinAttempt::MAXIMUM_PIN_ATTEMPTS)->create([
            'user_id' => $this->firstUser->id,
            'school_id' => $this->firstUser->school_id,
            'pass_id' => $this->pass->id
        ]);

        $this->actingAs($this->firstUser)
            ->put(route('api.v5.passes.update', ['pass' => $this->pass->id]), [
                'teacher_pin' => $this->pin->pin,
                'action' => 1
            ])
            ->assertJsonStructure(['data' => ['pin_attempts'], 'message', 'status'])
            ->assertStatus(423);

        $this->assertDatabaseHas('passes', ['id' => $this->pass->id, 'approved_by' => null]);
    }

    /**
     * @test
     * A student user can't arrive in room which isn't roundtrip
     * @return void
     */
    public function a_student_user_can_t_arrive_in_room_which_isn_t_roundtrip()
    {
        $passToRoom = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\Room',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->secondUser->school_id,
            'to_id' => $this->room->id,
            'user_id' => $this->secondUser->id
        ]);

        $this->actingAs($this->secondUser)
            ->put(route('api.v5.passes.update', ['pass' => $passToRoom->id]), [
                'teacher_pin' => $this->pin->pin,
                'action' => 3 // arrived action
            ])
            ->assertJsonStructure(['data' => ['id'], 'message', 'status'])
            ->assertStatus(422);

        $this->assertDatabaseHas('passes', ['id' => $passToRoom->id, 'arrived_at' => null]);
    }

    /**
     * @test
     * A student user can't create a pass with a restricted location
     * @return void
     */
    public function a_student_user_can_t_create_a_pass_with_a_restricted_location()
    {
        $restriction = RoomRestriction::create([
            'room_id' => $this->room->id,
            'school_id' => $this->secondUser->school_id,
            'user_id' => $this->secondUser->id,
            'type' => 2,
            'from_date' => Carbon::yesterday(),
            'to_date' => Carbon::tomorrow(),
            'status' => 1
        ]);

        $this->actingAs($this->secondUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->post(route('api.v5.passes.create'), [
                'from_id' => $this->adminUser->id,
                'to_id' => $this->room->id,
                'from_type' => 'App\Models\User',
                'to_type' => 'App\Models\Room',
                'comment' => 'Example comment'
            ])
            ->assertJsonStructure(['data', 'message', 'status'])
            ->assertStatus(422);

        $this->assertDatabaseMissing('passes', ['user_id' => $this->secondUser->id, 'to_id' => $this->room->id]);
    }

    /**
     * @test
     * A student can't create pass when the teacher is unavailable
     * @return void
     */
    public function a_student_can_t_create_pass_when_the_teacher_is_unavailable()
    {
        // Delete all available passes from the previous tests
        Pass::where('user_id', $this->secondUser->id)->delete();
        $unavailableAdminUser = Unavailable::create([
            'unavailable_type' => 'App\Models\User',
            'unavailable_id' => $this->adminUser->id,
            'school_id' => $this->school->id,
            'from_date' => Carbon::yesterday(),
            'to_date' => Carbon::tomorrow(),
            'status' => 1,
            'comment' => 'Example comment'
        ]);

        $this->actingAs($this->secondUser)
            ->post(route('api.v5.passes.create'), [
                'from_id' => $this->adminUser->id,
                'to_id' => $this->adminUser->id,
                'from_type' => 'App\Models\User',
                'to_type' => 'App\Models\User',
                'comment' => 'Example comment'
            ])
            ->assertJsonStructure(['data', 'message', 'status'])
            ->assertStatus(422);
    }

    /**
     * @test
     * A student can auto approve his pass when the pass is auto
     * @return void
     */
    public function a_student_can_auto_approve_his_pass_when_the_pass_is_auto()
    {
        // Delete all previos passes of that user.
        Pass::where('user_id', $this->secondUser->id)->delete();
        $autoPass = AutoPass::create([
            'room_id' => $this->room->id,
            'school_id' => $this->room->school_id,
        ]);

        $autoPassPreference = AutoPassPreference::create([
            'school_id' => $this->room->school_id,
            'user_id' => $this->adminUser->id,
            'auto_pass_id' => $autoPass->id,
            'mode' => 1,
            'status' => 1
        ]);


        $passToAutoRoom = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\Room',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->secondUser->school_id,
            'to_id' => $this->room->id,
            'user_id' => $this->secondUser->id
        ]);

        $this->actingAs($this->secondUser)
            ->put(route('api.v5.passes.auto.approve', ['pass' => $passToAutoRoom->id]), [
                'action' => 1
            ])->assertOk()
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('passes', ['id' => $passToAutoRoom->id]);
        $this->assertNotNull($passToAutoRoom->refresh()->approved_at);
    }

    /**
     * @test
     * A student can't start his pass when the pass isnot auto pass
     * @return void
     */
    public function a_student_can_t_start_his_pass_when_the_pass_is_not_auto_pass()
    {
        // Delete all previos passes of that user.
        Pass::where('user_id', $this->secondUser->id)->delete();
        $passToAutoRoom = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\Room',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->secondUser->school_id,
            'to_id' => $this->room->id,
            'user_id' => $this->secondUser->id
        ]);


        $this->actingAs($this->secondUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->put(route('api.v5.passes.auto.approve', ['pass' => $passToAutoRoom->id]), [
                'action' => 1
            ])->assertStatus(403);

        $this->assertDatabaseHas('passes', ['id' => $passToAutoRoom->id]);
        $this->assertNull($passToAutoRoom->refresh()->approved_at);
    }

    /**
     * @test
     * A student user can't stop pass when the pass is only start mode
     * @return void
     */
    public function a_student_user_can_t_stop_pass_when_the_pass_is_only_start_mode()
    {
        // Delete all previos passes of that user.
        Pass::where('user_id', $this->secondUser->id)->delete();

        $autoPass = AutoPass::create([
            'room_id' => $this->room->id,
            'school_id' => $this->room->school_id,
        ]);
        $autoPassPreference = AutoPassPreference::create([
            'school_id' => $this->room->school_id,
            'user_id' => $this->adminUser->id,
            'auto_pass_id' => $autoPass->id,
            'mode' => 1,
            'status' => 1
        ]);

        $passToAutoRoom = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\Room',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->secondUser->school_id,
            'to_id' => $this->room->id,
            'approved_at' => Carbon::now(),
            'approved_by' => $this->adminUser->id,
            'user_id' => $this->secondUser->id
        ]);

        $this->actingAs($this->secondUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->put(route('api.v5.passes.auto.approve', ['pass' => $passToAutoRoom->id]), [
                'action' => 2
            ])->assertStatus(422);

        $this->assertDatabaseHas('passes', ['id' => $passToAutoRoom->id]);
        $this->assertNull($passToAutoRoom->refresh()->completed_at);

    }

    /**
     * @test
     * A student user can stop the auto pass
     * @return void
     */
    public function a_student_user_can_stop_the_auto_pass()
    {
        // Delete all previos passes of that user.
        Pass::where('user_id', $this->secondUser->id)->delete();

        $autoPass = AutoPass::create([
            'room_id' => $this->room->id,
            'school_id' => $this->room->school_id,
        ]);
        $autoPassPreference = AutoPassPreference::create([
            'school_id' => $this->room->school_id,
            'user_id' => $this->adminUser->id,
            'auto_pass_id' => $autoPass->id,
            'mode' => 2,
            'status' => 1
        ]);

        $passToAutoRoom = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\Room',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->secondUser->school_id,
            'to_id' => $this->room->id,
            'approved_at' => Carbon::now(),
            'approved_by' => $this->adminUser->id,
            'user_id' => $this->secondUser->id
        ]);

        $this->actingAs($this->secondUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->put(route('api.v5.passes.auto.approve', ['pass' => $passToAutoRoom->id]), [
                'action' => 2
            ])->assertStatus(200);

        $this->assertDatabaseHas('passes', ['id' => $passToAutoRoom->id]);
        $this->assertNotNull($passToAutoRoom->refresh()->completed_at);

    }

    /**
     * @test
     * A student user can't create a pass when the capacity of the location is full
     * @return void
     */
    public function a_student_user_can_t_create_a_pass_when_the_capacity_of_the_location_is_full()
    {
        // Delete all previos passes of that user.
        Pass::where('user_id', $this->secondUser->id)->delete();
        Pass::where('user_id', $this->firstUser->id)->delete();

        $locationCapacity = LocationCapacity::create([
            'room_id' => $this->room->id,
            'school_id' => $this->school->id,
            'status' => 1,
            'limit' => 1
        ]);
        $pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\Room',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->secondUser->school_id,
            'to_id' => $this->room->id,
            'approved_at' => Carbon::now(),
            'approved_by' => $this->adminUser->id,
            'user_id' => $this->secondUser->id
        ]);

        $this->actingAs($this->firstUser)
            ->post(route('api.v5.passes.create'), [
                'from_id' => $this->adminUser->id,
                'to_id' => $this->room->id,
                'from_type' => 'App\Models\User',
                'to_type' => 'App\Models\Room',
                'comment' => 'Test Comment'
            ])->assertStatus(422)
            ->assertJsonStructure(['data', 'status', 'message']);

        $this->assertDatabaseMissing('passes', ['user_id' => $this->firstUser->id]);
    }

    /**
     * @test
     * A student user can auto checkIn from room
     * @return void
     */
    public function a_student_user_can_auto_check_in_from_room()
    {
        Pass::where('user_id', $this->secondUser->id)->delete();
        LocationCapacity::where('room_id', $this->room->id)->delete();

        $roomPin = Pin::create([
            'pinnable_id' => $this->room->id,
            'pinnable_type' => Room::class,
            'school_id' => $this->school->id,
            'pin' => '2222'
        ]);

        $pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\Room',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->secondUser->school_id,
            'to_id' => $this->room->id,
            'approved_at' => Carbon::now(),
            'approved_by' => $this->adminUser->id,
            'user_id' => $this->secondUser->id
        ]);

        $this->actingAs($this->secondUser)
            ->put(route('api.v5.passes.auto.checkin', ['pass' => $pass->id]), [
                'room_pin' => '2222'
            ])->assertOk()
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('passes', ['id' => $pass->id]);
        $this->assertNotNull($pass->refresh()->completed_at);
    }

    /**
     * @test
     * A student user can't auto checkIn with wrong pin
     * @return void
     */
    public function a_student_user_can_t_auto_check_in_with_wrong_pin()
    {
        $pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\Room',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->secondUser->school_id,
            'to_id' => $this->room->id,
            'approved_at' => Carbon::now(),
            'approved_by' => $this->adminUser->id,
            'user_id' => $this->secondUser->id
        ]);

        $this->actingAs($this->secondUser)
            ->put(route('api.v5.passes.auto.checkin', ['pass' => $pass->id]), [
                'room_pin' => '1111'
            ])->assertStatus(422)
            ->assertJsonStructure(['data', 'status', 'message']);

        $this->assertDatabaseHas('passes', ['id' => $pass->id]);
        $this->assertNull($pass->refresh()->completed_at);
    }

    /**
     * @test
     * A staff or teacher user can view appointment pass data when it's to them and it's from system
     * @return void
     */
    public function a_staff_or_teacher_user_can_view_appointment_pass_data_when_it_s_to_them_and_it_s_from_system()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => config('roles.system'),
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::tomorrow(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->teacherUser)
            ->get(route('api.v5.admin.passes.appointments'))
            ->assertJson([
                'data' => [
                    'appointments' => [
                        'data' => [
                            [
                                'created_by' => $this->adminUser->id,
                                'to_id' => $this->teacherUser->id
                            ]
                        ]
                    ]
                ]
            ]);
    }

//    /**
//     * @test
//     * a
//     * @return void
//     */
//    public function a()
//    {
//        SchoolModule::create([
//            'school_id' => $this->school->id,
//            'module_id' => 1,
//            'option_json' => json_encode(['hello' => 'world']),
//            'permission' => 1,
//            'status' =>1 ,
//        ]);
//        dd($this->actingAs($this->teacherUser)
//            ->get(route('api.v5.admin.passes.appointments'))
//            ->getContent());
//    }

    /**
     * @test
     * A teacher or staff user can't view appointment pass data when it's not to them or from them
     * @return void
     */
    public function a_teacher_or_staff_user_can_t_view_appointment_pass_data_when_it_s_not_to_them_or_from_them()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->staffUser->id,
            'from_type' => User::class,
            'to_id' => $this->adminUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::tomorrow(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->teacherUser)
            ->get(route('api.v5.admin.passes.appointments'))
            ->assertJson([
                'data' => [
                    'appointments' => [
                        'data' => []
                    ]
                ]
            ]);

    }

    /**
     * @test
     * A staff or teacher user can view appointment pass data properly
     * @return void
     */
    public function a_staff_or_teacher_user_can_view_appointment_pass_data_properly()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->teacherUser->id,
            'from_type' => User::class,
            'to_id' => $this->staffUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::tomorrow(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->teacherUser)
            ->get(route('api.v5.admin.passes.appointments'))
            ->assertJson([
                'data' => [
                    'appointments' => [
                        'data' => [
                            [
                                'created_by' => $this->adminUser->id
                            ]
                        ]
                    ]
                ]
            ]);
    }

    /**
     * @test
     * A staff or admin user can see their assigned locations for acknowledge
     * @return void
     */
    public function a_staff_or_admin_user_can_see_their_assigned_locations_for_acknowledge()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $staffSchedule = StaffSchedule::create([
            'room_id' => $this->room->id,
            'user_id' => $this->staffUser->id,
            'school_id' => $this->staffUser->school_id,
            'status' => 1
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->room->id,
            'to_type' => Room::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::tomorrow(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->staffUser)
            ->get(route('api.v5.admin.passes.appointments'))
            ->assertJson([
                'data' => [
                    'appointments' => [
                        'data' => [
                            [
                                'created_by' => $this->adminUser->id
                            ]
                        ]
                    ]
                ]
            ]);
    }

    /**
     * @test
     * A teacher user can't see not assigned room to him appointments
     * @return void
     */
    public function a_teacher_user_can_t_see_not_assigned_room_to_him_appointments()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $staffSchedule = StaffSchedule::create([
            'room_id' => $this->room->id,
            'user_id' => $this->staffUser->id,
            'school_id' => $this->staffUser->school_id,
            'status' => 1
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->room->id,
            'to_type' => Room::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::tomorrow(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->teacherUser)
            ->get(route('api.v5.admin.passes.appointments'))
            ->assertJson([
                'data' => [
                    'appointments' => [
                        'data' => []
                    ]
                ]
            ]);
    }

    /**
     * @test
     * A admin user can view appointemnt passes list.
     * @return void
     */
    public function a_admin_user_can_view_appointemnt_passes_list()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.admin.passes.appointments'))
            ->assertOk()
            ->assertJsonStructure(['data' => ['rooms', 'users', 'students', 'periods'], 'status']);
    }

    /**
     * @test
     * An admin user can edit appointment pass record
     * @return void
     */
    public function an_admin_user_can_edit_appointment_pass_record()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::tomorrow(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->adminUser)
            ->put(route('api.v5.admin.passes.appointments.update', ['appointmentPass' => $appointmentPass->id]), [
                'created_by' => $this->adminUser->id,
                'from_id' => $this->adminUser->id,
                'from_type' => User::class,
                'to_id' => $this->room->id,
                'to_type' => Room::class,
                'user_id' => $this->firstUser->id,
                'for_date' => Carbon::tomorrow(),
                'school_id' => $this->school->id,
                'period_id' => $period->id,
                'reason' => 'Example reason'
            ])
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();

        $this->assertDatabaseHas('appointment_passes', ['id' => $appointmentPass->id, 'to_id' => $this->room->id]);
    }

    /**
     * @test
     * An admin user can cancel an appointment pass
     * @return void
     */
    public function an_admin_user_can_cancel_an_appointment_pass()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::tomorrow(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason',
            'confirmed_by_teacher_at' => Carbon::now()
        ]);

        $this->actingAs($this->adminUser)
            ->put(route('api.v5.admin.passes.appointments.cancel', ['appointmentPass' => $appointmentPass]),
                [
                    'cancel_reason' => 'reason'
                ])
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();

        $this->assertDatabaseHas('appointment_passes', ['id' => $appointmentPass->id]);
        $this->assertNotNull($appointmentPass->refresh()->canceled_at);
    }

    /**
     * @test
     * An admin user can confirm a appointment pass
     * @return void
     */
    public function an_admin_user_can_confirm_a_appointment_pass()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->teacherUser->id,
            'from_type' => User::class,
            'to_id' => $this->adminUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::tomorrow(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->adminUser)
            ->put(route('api.v5.admin.passes.appointments.confirm', ['appointmentPass' => $appointmentPass]))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();

        $this->assertDatabaseHas('appointment_passes', ['id' => $appointmentPass->id]);
        $this->assertNotNull($appointmentPass->refresh()->confirmed_by_teacher_at);
    }

    /**
     * @test
     * A teacher user can confirm an appointment pass
     * @return void
     */
    public function a_teacher_user_can_confirm_an_appointment_pass()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::tomorrow(),
            'school_id' => $this->teacherUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->teacherUser)
            ->put(route('api.v5.admin.passes.appointments.confirm', ['appointmentPass' => $appointmentPass]))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();

        $this->assertDatabaseHas('appointment_passes', ['id' => $appointmentPass->id]);
        $this->assertNotNull($appointmentPass->refresh()->confirmed_by_teacher_at);
    }

    /**
     * @test
     * An admin user can view appointment pass data
     * @return void
     */
    public function an_admin_user_can_view_appointment_pass_data()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::tomorrow(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->teacherUser)
            ->get(route('api.v5.admin.passes.appointments.show', ['appointmentPass' => $appointmentPass->id]))
            ->assertOk()
            ->assertJsonStructure(['data', 'status']);
    }

    /**
     * @test
     * An admin user can create an appointement pass
     * @return void
     */
    public function an_admin_user_can_create_an_appointement_pass()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);


        $this->actingAs($this->adminUser)
            ->post(route('api.v5.admin.passes.appointments.create'), [
                'appointments' => [
                    [
                        'created_by' => $this->adminUser->id,
                        'from_id' => $this->adminUser->id,
                        'from_type' => User::class,
                        'to_id' => $this->room->id,
                        'to_type' => Room::class,
                        'user_id' => $this->firstUser->id,
                        'for_date' => Carbon::tomorrow(),
                        'school_id' => $this->school->id,
                        'period_id' => $period->id,
                        'reason' => 'Example reason'
                    ]
                ]
            ])
            ->assertStatus(201)
            ->assertJsonStructure(['status']);
        $this->assertDatabaseHas('appointment_passes', ['user_id' => $this->firstUser->id]);
    }

    /**
     * @test
     * An adults can create many appointments
     * @return void
     */
    public function an_adults_can_create_many_appointments()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);
        $dataInputs = [];

        for ($i = 0; $i < 2; $i++) {
            $dataInputs[] = [
                'created_by' => $this->adminUser->id,
                'from_id' => $this->adminUser->id,
                'from_type' => User::class,
                'to_id' => $this->room->id,
                'to_type' => Room::class,
                'user_id' => $this->firstUser->id,
                'for_date' => Carbon::tomorrow()->addDay($i + 1),
                'school_id' => $this->school->id,
                'period_id' => $period->id,
                'reason' => "Example reason $i",
            ];
        }

        $this->actingAs($this->adminUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->post(route('api.v5.admin.passes.appointments.create'), [
                'appointments' => $dataInputs
            ])->assertStatus(201)
            ->assertJsonStructure(['status']);

        $this->assertDatabaseHas('appointment_passes', [
            'user_id' => $this->firstUser->id,
            'reason' => 'Example reason 0',
            'for_date' => $dataInputs[0]['for_date']
        ]);
        $this->assertDatabaseHas('appointment_passes', [
            'user_id' => $this->firstUser->id,
            'reason' => 'Example reason 1',
            'for_date' => $dataInputs[1]['for_date']
        ]);
    }

//
//    /**
//     * @test
//     * An adults can create many appointments above 1000 records
//     * @return void
//     */
//    public function an_adults_can_create_many_appointments_above_1000_records()
//    {
//        $period = Period::create([
//            'school_id' => $this->school->id,
//            'status' => 1,
//            'name' => 'Period 1'
//        ]);
//        $dataInputs = [];
//
//        for ($i = 0; $i < 3; $i++) {
//            $dataInputs[] = [
//                'created_by' => $this->adminUser->id,
//                'from_id' => $this->adminUser->id,
//                'from_type' => User::class,
//                'to_id' => $this->room->id,
//                'to_type' => Room::class,
//                'user_id' => $this->firstUser->id,
//                'for_date' => Carbon::tomorrow()->setTime(16,00)->addDay($i + 1),
//                'school_id' => $this->school->id,
//                'period_id' => $period->id,
//                'recurrence_type' => 'daily',
//                'recurrence_end_at' => Carbon::now()->addMonth(6),
//                'reason' => "Example reason $i",
//            ];
//        }
//
//        $this->actingAs($this->adminUser)
//            ->withHeaders(['Accept' => 'application/json'])
//            ->post(route('api.v5.admin.passes.appointments.create'), [
//                'appointments' => $dataInputs
//            ])->assertStatus(201)
//            ->assertJsonStructure(['status']);
//
//        $this->assertDatabaseHas('appointment_passes', [
//            'user_id' => $this->firstUser->id,
//            'reason' => 'Example reason 0',
//            'for_date' => $dataInputs[0]['for_date']
//        ]);
//        $this->assertDatabaseHas('appointment_passes', [
//            'user_id' => $this->firstUser->id,
//            'reason' => 'Example reason 1',
//            'for_date' => $dataInputs[1]['for_date']
//        ]);
//    }

    /**
     * @test
     * A teacher user can acknowledge his pass
     * @return void
     */
    public function a_teacher_user_can_acknowledge_his_pass()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::today(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->teacherUser)
            ->put(route('api.v5.admin.passes.appointments.acknowledge', ['appointmentPass' => $appointmentPass->id]))
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('appointment_passes', ['id' => $appointmentPass->id]);
        $this->assertNotNull($appointmentPass->refresh()->acknowledged_by_teacher_at);
    }

    /**
     * @test
     * An adult can create appointment pass with daily recurrence
     * @return void
     */
    public function an_adult_can_create_appointment_pass_with_daily_recurrence()
    {
        Carbon::setTestNow('first Monday of March');

        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);

        $dataInput = [
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->room->id,
            'to_type' => Room::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::now()->addMinutes(1),
            'school_id' => $this->school->id,
            'period_id' => $period->id,
            'recurrence_type' => config('recurrences.daily'),
            'recurrence_end_at' => Carbon::tomorrow()->addMonth(6),
            'reason' => 'Example reason'
        ];

        $this->actingAs($this->adminUser)
            ->post(route('api.v5.admin.passes.appointments.create'), [
                'appointments' => [
                    $dataInput
                ]
            ])->assertStatus(201)
            ->assertJsonStructure(['status']);

        $this->assertDatabaseHas('appointment_passes', [
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::parse($dataInput['for_date'])->format('Y-m-d H:i')
        ]);

        $this->assertDatabaseHas('appointment_passes', [
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::parse($dataInput['for_date'])->addDays(1)->format('Y-m-d H:i')
        ]);

        $this->assertDatabaseHas('appointment_passes', [
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::parse($dataInput['for_date'])->addDays(2)->format('Y-m-d H:i')
        ]);
    }

    /**
     * @test
     * An adult can create an appointment pass with weekly
     * @return void
     */
    public function an_adult_can_create_an_appointment_pass_with_weekly()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);

        $dataInput = [
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->room->id,
            'to_type' => Room::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::now()->addMinutes(1),
            'school_id' => $this->school->id,
            'period_id' => $period->id,
            'recurrence_type' => config('recurrences.weekly'),
            'recurrence_days' => collect(['days' => [Carbon::now()->englishDayOfWeek]])->toJson(),
            'recurrence_end_at' => Carbon::tomorrow()->addDays(14),
            'reason' => 'Example reason'
        ];

        $this->actingAs($this->adminUser)
            ->post(route('api.v5.admin.passes.appointments.create'), [
                'appointments' => [
                    $dataInput
                ]
            ])->assertStatus(201)
            ->assertJsonStructure(['status']);

        $this->assertDatabaseHas('appointment_passes', [
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::parse($dataInput['for_date'])->format('Y-m-d H:i')
        ]);
    }

    /**
     * @test
     * An adult user can create an appointment pass with monthly recurrence
     * @return void
     */
    public function an_adult_user_can_create_an_appointment_pass_with_monthly_recurrence()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);
        $today = Carbon::now()->addMinutes(1);
        $dataInput = [
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->room->id,
            'to_type' => Room::class,
            'user_id' => $this->firstUser->id,
            'for_date' => $today,
            'school_id' => $this->school->id,
            'period_id' => $period->id,
            'recurrence_type' => config('recurrences.monthly'),
            'recurrence_week' => collect([Carbon::now()->weekNumberInMonth => Carbon::now()->englishDayOfWeek])->toJson(),
            'recurrence_end_at' => Carbon::now()->addMonths(2),
            'reason' => 'Example reason'
        ];

        $this->actingAs($this->adminUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->post(route('api.v5.admin.passes.appointments.create'), [
                'appointments' => [
                    $dataInput
                ]
            ])->assertStatus(201)
            ->assertJson(['status' => 'success']);
    }

    /**
     * @test
     * An adult can update recurrence pass
     * @return void
     */
    public function an_adult_can_update_recurrence_pass()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);

        $recurrenceAppointmentPass = RecurrenceAppointmentPass::create([
            'user_id' => $this->firstUser->id,
            'created_by' => $this->adminUser->id,
            'from_id' => $this->teacherUser->id,
            'from_type' => User::class,
            'to_id' => $this->adminUser->id,
            'to_type' => User::class,
            'period_id' => $period->id,
            'recurrence_type' => config('recurrences.monthly'),
            'recurrence_end_at' => Carbon::now()->addMonths(5),
            'reason' => "Test",
            'for_date' => Carbon::now()->addMinutes(1),
            'school_id' => $this->school->id,
        ]);

        $this->actingAs($this->adminUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->put(route('api.v5.admin.appointments.recurrence.edit', [
                'recurrenceAppointmentPass' => $recurrenceAppointmentPass->id
            ]), [
                'user_id' => $recurrenceAppointmentPass->user_id,
                'from_id' => $recurrenceAppointmentPass->from_id,
                'from_type' => $recurrenceAppointmentPass->from_type,
                'to_id' => $recurrenceAppointmentPass->to_id,
                'to_type' => $recurrenceAppointmentPass->to_type,
                'for_date' => $recurrenceAppointmentPass->for_date,
                'period_id' => $recurrenceAppointmentPass->period_id,
                'reason' => 'Edited reason',
                'recurrence_type' => $recurrenceAppointmentPass->recurrence_type,
                'recurrence_end_at' => $recurrenceAppointmentPass->recurrence_end_at,
                'recurrence_week' => collect(['2' => 'Monday'])->toJson(),
                'recurrence_days' => $recurrenceAppointmentPass->recurrence_days
            ])
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();

        $this->assertDatabaseHas('recurrence_appointment_passes', ['id' => $recurrenceAppointmentPass->id, 'reason' => 'Edited reason']);
        $this->assertDatabaseHas('appointment_passes', ['reason' => 'Edited Reason']);
    }

    /**
     * @test
     * An adult can view recurrence appointment pass
     * @return void
     */
    public function an_adult_can_view_recurrence_appointment_pass()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);

        $recurrenceAppointmentPass = RecurrenceAppointmentPass::create([
            'user_id' => $this->firstUser->id,
            'created_by' => $this->adminUser->id,
            'from_id' => $this->teacherUser->id,
            'from_type' => User::class,
            'to_id' => $this->adminUser->id,
            'to_type' => User::class,
            'period_id' => $period->id,
            'recurrence_type' => config('recurrences.monthly'),
            'recurrence_end_at' => Carbon::now()->addMonths(5),
            'reason' => "Test",
            'for_date' => Carbon::today(),
            'school_id' => $this->school->id,
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::now()->addDays(10),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason',
            'recurrence_appointment_pass_id' => $recurrenceAppointmentPass->id
        ]);

        $this->actingAs($this->adminUser)
            ->delete(route('api.v5.admin.appointments.recurrence.delete', ['recurrenceAppointmentPass' => $recurrenceAppointmentPass->id]), [
                'action' => RecurrenceAppointmentPass::WITH_FEATURE_RECURRENCES
            ])
            ->assertOk()
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseMissing('recurrence_appointment_passes', ['id' => $recurrenceAppointmentPass->id]);
        $this->assertDatabaseMissing('appointment_passes', ['id' => $appointmentPass->id]);
    }

    /**
     * @test
     * An adult can delete feature recurrence with current week and feature
     * @return void
     */
    public function an_adult_can_delete_feature_recurrence_with_current_week_and_feature()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);

        $recurrenceAppointmentPass = RecurrenceAppointmentPass::create([
            'user_id' => $this->firstUser->id,
            'created_by' => $this->adminUser->id,
            'from_id' => $this->teacherUser->id,
            'from_type' => User::class,
            'to_id' => $this->adminUser->id,
            'to_type' => User::class,
            'period_id' => $period->id,
            'recurrence_type' => config('recurrences.monthly'),
            'recurrence_end_at' => Carbon::now()->addMonths(5),
            'reason' => "Test",
            'for_date' => Carbon::today(),
            'school_id' => $this->school->id,
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::now()->addDays(10),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason',
            'recurrence_appointment_pass_id' => $recurrenceAppointmentPass->id
        ]);

        $this->actingAs($this->adminUser)
            ->delete(route('api.v5.admin.appointments.recurrence.delete', ['recurrenceAppointmentPass' => $recurrenceAppointmentPass->id]), [
                'action' => RecurrenceAppointmentPass::WITH_CURRENT_WEEK_AND_FEATURE_RECURRENCES
            ])
            ->assertOk()
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseMissing('recurrence_appointment_passes', ['id' => $recurrenceAppointmentPass->id]);
        $this->assertDatabaseMissing('appointment_passes', ['id' => $appointmentPass->id]);
    }

    /**
     * @test
     * An admin user can create an appointement pass from system
     * @return void
     */
    public function an_admin_user_can_create_an_appointement_pass_from_system()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);


        $this->actingAs($this->adminUser)
            ->post(route('api.v5.admin.passes.appointments.create'), [
                'appointments' => [
                    [
                        'created_by' => $this->adminUser->id,
                        'from_id' => config('roles.system'),
                        'from_type' => User::class,
                        'to_id' => $this->room->id,
                        'to_type' => Room::class,
                        'user_id' => $this->firstUser->id,
                        'for_date' => Carbon::tomorrow(),
                        'school_id' => $this->school->id,
                        'period_id' => $period->id,
                        'reason' => 'Example reason'
                    ]
                ]
            ])->assertStatus(201)
            ->assertJsonStructure(['status']);

        $this->assertDatabaseHas('appointment_passes', ['user_id' => $this->firstUser->id]);
    }

    /**
     * @test
     * An admin user can create a proxy pass
     * @return void
     */
    public function an_admin_user_can_create_a_proxy_pass()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.admin.passes.proxy.create'), [
                'user_id' => $this->secondUser->id,
                'to_id' => $this->room->id,
                'to_type' => Room::class,
                'comment' => 'Example comment',
            ])->assertStatus(201)
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('passes', ['user_id' => $this->firstUser->id]);
    }

    /**
     * @test
     * An admin user can view form create detail.
     * @return void
     */
    public function an_admin_user_can_view_form_create_details()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.admin.passes.proxy'))
            ->assertJsonStructure(['data' => ['rooms', 'users', 'students', 'school_pass_limit']]);
    }

    /**
     * @test
     * A student user can view his appointment passes for the day
     * @return void
     */
    public function a_student_user_can_view_his_appointment_passes_for_the_day()
    {
        $this->actingAs($this->firstUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->get(route('api.v5.passes.appointments'))
            ->assertJsonStructure(['data', 'status']);
    }

    /**
     * @test
     * A student user can acknowledge the appointment pass
     * @return void
     */
    public function a_student_user_can_acknowledge_the_appointment_pass()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::today(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->firstUser)
            ->put(route('api.v5.passes.appointments.acknowledge', ['appointmentPass' => $appointmentPass->id]))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();

        $this->assertDatabaseHas('appointment_passes', ['id' => $appointmentPass->id]);
        $this->assertNotNull($appointmentPass->refresh()->acknowledge_at);

    }

    /**
     * @test
     * A student user can acknowledge mail his appointment pass
     * @return void
     */
    public function a_student_user_can_acknowledge_mail_his_appointment_pass()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::today(),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->firstUser)
            ->get(route('api.v5.passes.appointments.acknowledge.mail', ['appointmentPass' => $appointmentPass->id]))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();
        $this->assertDatabaseHas('appointment_passes', ['id' => $appointmentPass->id]);
        $this->assertNotNull($appointmentPass->refresh()->acknowledged_by_mail_at);
    }


    /**
     * @test
     * A student user can acknowledge the appointment pass
     * @return void
     */
    public function a_student_user_can_confirm_the_appointment_pass()
    {
        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period'
        ]);

        $appointmentPass = AppointmentPass::create([
            'created_by' => $this->adminUser->id,
            'from_id' => $this->adminUser->id,
            'from_type' => User::class,
            'to_id' => $this->teacherUser->id,
            'to_type' => User::class,
            'user_id' => $this->firstUser->id,
            'for_date' => Carbon::today(optional($this->school)->getTimezone()),
            'school_id' => $this->firstUser->school_id,
            'period_id' => $period->id,
            'pass_id' => $this->pass->id,
            'reason' => 'reason'
        ]);

        $this->actingAs($this->firstUser)
            ->put(route('api.v5.passes.appointments.confirm', ['appointmentPass' => $appointmentPass->id]))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();
        $this->assertDatabaseHas('appointment_passes', ['id' => $appointmentPass->id]);
        $this->assertNotNull($appointmentPass->refresh()->confirmed_at);
    }

    /**
     * @test
     * A student user can view pass history
     * @return void
     */
    public function a_student_user_can_view_pass_history()
    {
        $this->actingAs($this->firstUser)
            ->get(route('api.v5.passes.history'))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();
    }

    /**
     * @test
     * A student user can cancel his pass
     * @return void
     */
    public function a_student_user_can_cancel_his_pass()
    {
        $this->actingAs($this->firstUser)
            ->put(route('api.v5.passes.cancel', ['pass' => $this->pass->id]))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();

        $this->assertNotNull($this->pass->refresh()->canceled_at);
    }

    /**
     * @test
     * An admin user can get users with active passes
     * @return void
     */
    public function an_admin_user_can_get_users_with_active_passes()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.passes.actives'))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();
    }

    /**
     * @test
     * A student user can't create a pass when it's blocked
     * @return void
     */
    public function a_student_user_can_t_create_a_pass_when_it_s_blocked()
    {
        PassBlock::create([
            'from_date' => Carbon::yesterday(),
            'to_date' => Carbon::tomorrow()->addDays(10),
            'reason' => 'PassBlock',
            'students' => 1,
            'kiosk' => 0,
            'school_id' => $this->school->id,
            'appointments' => 0,
            'proxy' => 0,
            'message' => 'PassBlock'
        ]);

        $this->actingAs($this->secondUser)->withHeaders([
            'Accept' => 'application/json',
        ])
            ->post(route('api.v5.passes.create'), [
                'from_id' => $this->adminUser->id,
                'to_id' => $this->teacherUser->id,
                'from_type' => 'App\Models\User',
                'to_type' => 'App\Models\User',
                'comment' => 'Test'
            ])->assertStatus(403);

        $this->assertDatabaseMissing('passes', ['to_id' => $this->firstUser->id, 'from_id' => $this->adminUser->id]);
    }

    /**
     * @test
     * An admin user can't create a proxy pass when passblock exist
     * @return void
     */
    public function an_admin_user_can_t_create_a_proxy_pass_when_passblock_exist()
    {
        PassBlock::create([
            'from_date' => Carbon::yesterday(),
            'to_date' => Carbon::tomorrow()->addDays(10),
            'reason' => 'PassBlock',
            'students' => 0,
            'kiosk' => 0,
            'school_id' => $this->school->id,
            'appointments' => 0,
            'proxy' => 1,
            'message' => 'PassBlock'
        ]);

        $this->actingAs($this->adminUser)
            ->post(route('api.v5.admin.passes.proxy.create'), [
                'user_id' => $this->firstUser->id,
                'to_id' => $this->room->id,
                'to_type' => Room::class,
            ])->assertStatus(403);

        $this->assertDatabaseMissing('passes', ['user_id' => $this->firstUser->id, 'to_id' => $this->room->id]);
    }

    /**
     * @test
     * An admin user can't create an appointement pass when passblock exist
     * @return void
     */
    public function an_admin_user_can_t_create_an_appointement_pass_when_passblock_exist()
    {
        PassBlock::create([
            'from_date' => Carbon::yesterday(),
            'to_date' => Carbon::tomorrow()->addDays(10),
            'reason' => 'PassBlock',
            'students' => 0,
            'kiosk' => 0,
            'school_id' => $this->school->id,
            'appointments' => 1,
            'proxy' => 0,
            'message' => 'PassBlock'
        ]);

        $period = Period::create([
            'school_id' => $this->school->id,
            'status' => 1,
            'name' => 'Period 1'
        ]);


        $this->actingAs($this->adminUser)
            ->post(route('api.v5.admin.passes.appointments.create'), [
                'appointments' => [
                    [
                        'created_by' => $this->adminUser->id,
                        'from_id' => $this->adminUser->id,
                        'from_type' => User::class,
                        'to_id' => $this->room->id,
                        'to_type' => Room::class,
                        'user_id' => $this->firstUser->id,
                        'for_date' => Carbon::tomorrow(),
                        'school_id' => $this->school->id,
                        'period_id' => $period->id,
                        'reason' => 'Example reason'
                    ]
                ]
            ])->assertStatus(403);
    }
}
