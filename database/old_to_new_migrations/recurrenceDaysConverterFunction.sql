CREATE FUNCTION `getRecurrenceDays`( val VARCHAR(80) ) RETURNS varchar(155) CHARSET utf8
BEGIN
    -- Loop increment
    DECLARE inc INT;

    -- Default json value before adding the week & days
    DECLARE jsonVal VARCHAR(155) DEFAULT '[]';

    -- Week day names.
    DECLARE days VARCHAR(80) DEFAULT '["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]';

    -- Current date state
    DECLARE _day VARCHAR(10);

    -- Day int number
    DECLARE dayNum INT DEFAULT 0;

    -- Convert comma separated values to array
    SET val = CONCAT('["', REPLACE(val, ',', '","'), '"]');

    SET inc = 0;

    IF JSON_VALID(val) THEN
        loopThroughDays:
        WHILE inc <= JSON_LENGTH(val) - 1 DO
                -- Extract the current day number
                SET dayNum = (JSON_EXTRACT(val, CONCAT('$[',inc,']')));

                -- Get the date number
                set _day = (JSON_EXTRACT(days, CONCAT('$[',dayNum,']')));

                -- Remove the double quotes from string.
                set _day = (TRIM(BOTH '"' FROM _day));

                -- Insert the date name into the array
                SET jsonVal = (JSON_ARRAY_INSERT(jsonVal, CONCAT('$[',inc,']'), _day));

                SET inc = inc + 1;
            END
                WHILE loopThroughDays;

    END IF;


    RETURN CONCAT('{"days":',jsonVal,'}');
END
