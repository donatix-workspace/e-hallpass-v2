DELIMITER ;;
DROP PROCEDURE IF EXISTS ehp_v2_prod.migratePasses;;
CREATE PROCEDURE ehp_v2_prod.migratePasses()
BEGIN
    DECLARE i,n,o INT DEFAULT 0;
    DECLARE perPage INT DEFAULT 2500;

    SELECT count(pass_id) from passhistory.epass_temp WHERE created_at BETWEEN '2021-01-01 00:00:00' AND '2022-02-17 23:59:59' into n;
    SET i = 0;
    SET o = 0;
    SET perPage = 4000000;

    WHILE i < n / perPage
        DO
            START TRANSACTION;
            INSERT INTO ehp_v2_prod.passes (user_id, parent_id, from_teacher_note, school_id,
                                            from_type, from_id, to_type, to_id, requested_by,
                                            approved_by, completed_by, created_by, reason, ordering,
                                            to_teacher_note, pass_status, approved_with, ended_with,
                                            approved_at, completed_at, requested_at, arrived_at, type,
                                            expired_at, canceled_at, created_at, updated_at, edited_at,
                                            system_completed, flagged_at, extended_at, old_parent_id, old_id)
            SELECT (SELECT id from users WHERE old_id = oldPass.student_id limit 1)                  as user_id,

                   (SELECT NULL)                                                                     as parent_id,

                   (SELECT oldPass.fromteacher_note)                                                 as from_teacher_note,

                   (SELECT id from schools WHERE old_id = oldPass.school_id limit 1)                 as school_id,

                   (SELECT IF(oldPass.fromteacher_id > 0, 'App\\Models\\User', 'App\\Models\\Room')) as from_type,

                   (SELECT IF(oldPass.fromteacher_id > 0,
                              (SELECT id FROM users WHERE old_id = oldPass.fromteacher_id limit 1),
                              (SELECT id FROM rooms WHERE old_id = oldPass.fromroom_id limit 1)))    as from_id,

                   (SELECT IF(oldPass.toteacher_id > 0, 'App\\Models\\User', 'App\\Models\\Room'))   as to_type,

                   (SELECT IF(oldPass.toteacher_id > 0,
                              (SELECT id FROM users WHERE old_id = oldPass.toteacher_id limit 1),
                              (SELECT id FROM rooms WHERE old_id = oldPass.toroom_id limit 1)))      as to_id,

                   (SELECT NULL)                                                                     as requested_by,

                   (SELECT id from users WHERE old_id = oldPass.out_by_teacher limit 1)              as approved_by,

                   (SELECT id from users WHERE old_id = oldPass.in_by_teacher AND first_name != 'System Ended' limit 1)               as completed_by,

                   (SELECT id from users WHERE old_id = oldPass.created_by limit 1)                  as created_by,
                   (SELECT NULL)                                                                     as reason,
                   (SELECT NULL)                                                                     as ordering,
                   (SELECT NULL)                                                                     as to_teacher_note,
                   (SELECT IF(oldPass.pass_status >= 1, 1, 0))                                       as pass_status,
                   (SELECT IF(oldPass.out_by_teacher = oldPass.student_id AND oldPass.outaction != 4, 'autopass',
                       NULL))                                                               as approved_with,
                   (SELECT CASE
                       WHEN oldPass.student_id = oldPass.in_by_teacher THEN 'autopass'
                       WHEN oldPass.inaction = 1 AND oldPass.in_by_teacher = 0 THEN 'checkin'
                            END)                                                               as ended_with,
                   (SELECT CONVERT_TZ(oldPass.outdate,
                                   'America/New_York',
                                      'UTC'))                                                     as approved_at,
                   (SELECT CONVERT_TZ(oldPass.indate,
                                      'America/New_York',
                                      'UTC'))                                                     as completed_at,
                   (SELECT NULL)                                                                     as requested_at,
                   (SELECT NULL)                                                                     as arrived_at,
                   (SELECT CASE
                               WHEN oldPass.passtype = 0 THEN 'STU'
                               WHEN oldPass.passtype = 1 THEN 'APT'
                               WHEN oldPass.passtype = 3 THEN 'KSK'
                               WHEN oldPass.created_by = 0 AND
                                    oldPass.passtype != 1 THEN 'TCH'
                               END)                                                                  as type,
                   (SELECT IF(oldPass.pass_status = 0 AND (oldPass.outaction in (3,4) OR oldPass.inaction in (3,4)) AND (oldPass.inaction != 4 AND oldPass.outaction != 4) AND (oldPass.edit_status != -1),
                              (SELECT CONVERT_TZ(oldPass.updated_at,
                                              'America/New_York',
                                                 'UTC'))
                               ,
                              (SELECT NULL)))                                                        as expired_at,
                   (SELECT IF(oldPass.outaction = 4 AND oldPass.in_by_teacher = 0, CONVERT_TZ(oldPass.created_at,
                                                            'America/New_York',
                                                               'UTC'),
                              (SELECT NULL)))                                                        as canceled_at,
                   (SELECT CONVERT_TZ(oldPass.created_at,
                                      'America/New_York',
                                      'UTC'))                                                     as created_at,
                   (SELECT CONVERT_TZ(oldPass.created_at,
                                     'America/New_York',
                                      'UTC'))                                                     as updated_at,
                   (SELECT IF(oldPass.edit_status = 5, CONVERT_TZ(oldPass.updated_at,
                                                                 'America/New_York',
                                                                  'UTC'), (SELECT NULL)))         as edited_at,
                   (SELECT IF((SELECT role_id from ehallpassDB.users WHERE user_id = oldPass.in_by_teacher limit 1) = 7 AND (oldPass.inaction in (3,4) OR oldPass.outaction in (3,4)),
                              1,
                              0))                                                                    as system_completed,
                   (SELECT NULL)                                                                     as flagged_at,
                   (SELECT NULL)                                                                     as extended_at,
                   (SELECT oldPass.parent_id)                                                        as old_parent_id,
                   (SELECT oldPass.pass_id)                                                          as old_id
            FROM passhistory.epass_temp as oldPass
            WHERE created_at BETWEEN '2021-01-01 00:00:00' AND '2022-02-17 23:59:59'
            LIMIT o,perPage;
            COMMIT;

            SET o = ((i + 1) * perPage) + 1;
            SET i = i + 1;
        END WHILE;
END;;
DELIMITER ;
