<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Resources\SearchCollection;
use App\Http\Resources\TeacherCollection;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UsersListController extends Controller
{
    /**
     * Get Teachers list
     * @bodyParam query string Search query
     * @param Request $request
     * @return TeacherCollection
     */
    public function teacherList(Request $request)
    {
        $location = trim($request->input('location'));
        $searchString = strtolower(trim($request->input('query')));
        $teachers = User::where([
            ['school_id', '=', auth()->user()->school_id],
            ['status', '=', 1],
            ['is_searchable', '=', 1],
        ])
            ->where('first_name', 'LIKE', '%' . $searchString . '%')
            ->orWhere('last_name', 'LIKE', '%' . $searchString . '%')
            ->whereIntegerNotInRaw('role_id', [config('role.student'), config('role.system')])
            ->orderBy('last_name', 'ASC')
            ->take(5)
            ->get();

        return new TeacherCollection($teachers);
    }

    /**
     * Student search
     *
     * @bodyParam query string Search query
     * @param Request $request
     * @return SearchCollection
     */
    public function studentSearch(Request $request)
    {
        $searchString = strtolower(trim($request->input('query')));
        $students = User::where([
            ['school_id', '=', auth()->user()->school_id],
            ['status', '=', 1],
            ['is_searchable', '=', 1],
            ['role_id', '=', Role::class],
        ])
            ->where('first_name', 'LIKE', '%' . $searchString . '%')
            ->orWhere('last_name', 'LIKE', '%' . $searchString . '%')
            ->orderBy('first_name', 'ASC')
            ->get();

        return new SearchCollection($students);
    }

    /**
     * @param Request $request
     */
    public function teacherSearch(Request $request)
    {
        $searchString = strtolower(trim($request->input('query')));
        $teachers = User::where([
            ['school_id', '=', auth()->user()->school_id],
            ['status', '=', 1],
            ['role_id', '!=', 'student'],
        ])
            ->where('first_name', 'LIKE', '%' . $searchString . '%')
            ->orWhere('last_name', 'LIKE', '%' . $searchString . '%')
            ->orderBy('first_name', 'ASC')
            ->get();

        return new SearchCollection($teachers);
    }
}
