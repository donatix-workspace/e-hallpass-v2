<?php

namespace Tests\Feature;

use App\Models\Room;
use App\Models\RoomType;
use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\DatabaseSeeder;
use Database\Seeders\RolesDatabaseSeeder;
use Database\Seeders\SchoolSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RoomTest extends TestCase
{
    use RefreshDatabase;

    protected $adminUser;
    protected $firstUser;
    protected $secondUser;
    protected $school;
    protected $room;

    protected function setUp(): void
    {
        parent::setUp();

        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        // Create example school
        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);
        $this->secondUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);
        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);
        $this->room = Room::factory()->for(RoomType::factory()->createOne(['school_id' => $this->school->id]))->createOne([
            'trip_type' => Room::TRIP_ONE_WAY,
            'comment_type' => Room::COMMENT_HIDDEN,
            'school_id' => $this->school->id,
        ]);
    }

    /** @test */
    /* an admin can view all rooms */
    public function an_admin_can_view_all_rooms()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.rooms'))
            ->assertJsonStructure(['data', 'status']);
    }

    /** @test */
    /* a student can view all rooms */
    public function a_student_can_view_all_rooms()
    {
        $this->actingAs($this->firstUser)
            ->get(route('api.v5.rooms'))
            ->assertJsonStructure(['data', 'status']);
    }

    /** @test */
    /* an admin user can view specific room */
    public function an_admin_user_can_view_specific_room()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.rooms.show', ['room' => $this->room->id]))
            ->assertJsonStructure(['data', 'status']);
        $this->assertDatabaseHas('rooms', ['id' => $this->room->id]);
    }

    /** @test */
    /* a student can view specific room */
    public function a_student_can_view_specific_room()
    {
        $this->actingAs($this->firstUser)
            ->get(route('api.v5.rooms.show', ['room' => $this->room->id]))
            ->assertJsonStructure(['data', 'status']);
        $this->assertDatabaseHas('rooms', ['id' => $this->room->id]);
    }

    /** @test */
    /* an admin can create room */
    public function an_admin_can_create_room()
    {
        $extendedPassTime = Carbon::now()->addHours(2)->format('H:i:s');
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.rooms.store'), [
                'name' => 'Test',
                'trip_type' => Room::TRIP_ONE_WAY,
                'status' => '0',
                'is_extended' => '1',
                'extended_pass_time' => $extendedPassTime,
                'status' => 1,
                'comment_type' => Room::COMMENT_OPTIONAL,
            ])->assertJson(['status' => 'success']);
        $this->assertDatabaseHas('rooms', ['school_id' => $this->firstUser->school_id, 'name' => 'Test']);
        $this->assertDatabaseHas('nurse_rooms', ['school_id' => $this->firstUser->school_id, 'end_time' => $extendedPassTime]);
    }

    /** @test */
    /* a student cant create room */
    public function a_student_cant_create_room()
    {
        $extendedPassTime = Carbon::now()->addHours(2)->format('H:i:s');
        $this->actingAs($this->firstUser)
            ->post(route('api.v5.rooms.store'), [
                'name' => 'Test',
                'trip_type' => Room::TRIP_ONE_WAY,
                'status' => '0',
                'is_extended' => '1',
                'extended_pass_time' => $extendedPassTime,
                'comment_type' => Room::COMMENT_OPTIONAL,
            ])->assertStatus(403);
    }

    /** @test */
    /* an admin can update room */
    public function an_admin_can_update_room()
    {
        $this->actingAs($this->adminUser)
            ->put(route('api.v5.rooms.update', ['room' => $this->room]), [
                'status' => '-1'
            ])
            ->assertJson(['status' => 'success']);
        $this->assertDatabaseHas('rooms', ['id' => $this->room->id, 'status' => '-1']);
    }

    /** @test */
    /* a student cant update room */
    public function a_student_cant_update_room()
    {
        $this->actingAs($this->firstUser)
            ->put(route('api.v5.rooms.update', ['room' => $this->room]), [
                'status' => '-1'
            ])->assertStatus(403);
    }

    /** @test */
    /* an admin can delete room */
    public function an_admin_can_delete_room()
    {
        $this->actingAs($this->adminUser)
            ->delete(route('api.v5.rooms.delete', ['room' => $this->room]))
            ->assertJson(['status' => __('success'), 'data' => ['id' => $this->room->id]]);

        $this->assertDatabaseMissing('rooms', ['id' => $this->room->id]);
    }

    /** @test */
    /* a student cant delete room */
    public function a_student_cant_delete_room()
    {
        $this->actingAs($this->firstUser)
            ->delete(route('api.v5.rooms.delete', ['room' => $this->room->id]))
            ->assertStatus(403);

        $this->assertDatabaseHas('rooms', ['id' => $this->room->id]);
    }

    /** @test */
    /* an admin can make room favorite for all students */
    public function an_admin_can_make_room_favorite_for_all_students()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.rooms.toggle.favorites', ['room' => $this->room]), [
                'favorites' => '1'
            ])
            ->assertJson(['status' => __('success'), 'data' => ['id' => $this->room->id]]);

        $this->assertDatabaseHas('rooms', ['id' => $this->room->id, 'favorites' => '1']);
    }

    /** @test */
    /* a student cant make room favorite for all students */
    public function a_student_cant_make_room_favorite_for_all_students()
    {
        $this->actingAs($this->firstUser)
            ->post(route('api.v5.rooms.toggle.favorites', ['room' => $this->room]))
            ->assertStatus(403);
    }

    /** @test */
    /* an admin can search for rooms */
    public function an_admin_can_search_for_rooms()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.rooms.search', ['query' => 'test']))
            ->assertJsonStructure(['data', 'status']);
    }

    /** @test */
    /* a student can search for rooms */
    public function a_student_can_search_for_rooms()
    {
        $this->actingAs($this->firstUser)
            ->post(route('api.v5.rooms.search', ['query' => 'test']))
            ->assertJsonStructure(['data', 'status']);
    }

    /** @test */
    /* non logged user cannot use the search */
    public function non_logged_user_cannot_use_the_search()
    {
        $this->post(route('api.v5.rooms.search', ['query' => 'test']))
            ->assertStatus(403);
    }

    /** @test */
    /* admin can view autopass settings page */
    public function admin_can_view_autopass_settings_page()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.rooms.autopass'))
            ->assertJsonStructure(['data' => ['rooms', 'auto_pass_rooms'], 'status']);
    }

    /** @test */
    /* admin can create new rooms autopasses */
    public function admin_can_create_new_rooms_autopasses()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.rooms.autopass.store'), ['autopass_rooms' => [$this->room->id]])
            ->assertJson(['status' => __('auto.pass.rooms.updated')])
            ->assertStatus(201);

        $this->assertDatabaseHas('auto_passes', ['room_id' => $this->room->id, 'school_id' => $this->adminUser->school_id]);
    }

    /** @test */
    /* student cant create new rooms autopasses */
    public function student_cant_create_new_rooms_autopasses()
    {
        $this->actingAs($this->firstUser)
            ->post(route('api.v5.rooms.autopass.store'), ['autopass_rooms' => [$this->room->id]])
            ->assertStatus(403);
    }

    /** @test */
    /* validation will fail if invalid room is passed to autopass */
    public function validation_will_fail_if_invalid_room_is_passed_to_autopass()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.rooms.autopass.store'), ['autopass_rooms' => ['202000', $this->room->id]])
            ->assertStatus(302);
    }

    /** @test */
    /* admin can open passtime settings page */
    public function admin_can_open_passtime_settings_page()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.rooms.passtime.settings'))
            ->assertJsonStructure(['data', 'status']);
    }

    /** @test */
    /* student cant open passtime settings page */
    public function student_cant_open_passtime_settings_page()
    {
        $this->actingAs($this->firstUser)
            ->post(route('api.v5.rooms.passtime.settings'))
            ->assertStatus(403);
    }

    /** @test */
    /* admin can export example csv template for rooms */
    public function admin_can_export_example_csv_template_for_rooms()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.rooms.export.csv.template'))
            ->assertOk();
    }

    /**
     * @test
     * An admin user can assign location to himself
     * @return void
     */
    public function an_admin_user_can_assign_location_to_himself()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.rooms.assign'), [
                'room_id' => $this->room->id,
                'pin' => 'test'
            ])
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();

        $this->assertDatabaseHas('rooms_pins', ['room_id' => $this->room->id]);
    }

    /**
     * @test
     * An admin user can view assign location form data
     * @return void
     */
     public function an_admin_user_can_view_assign_location_form_data()
     {
        $this->actingAs($this->adminUser)
        ->get(route('api.v5.rooms.assign'))
        ->assertJsonStructure(['data' => ['not_assigned_rooms', 'assigned_rooms'], 'status']);
     }
}
