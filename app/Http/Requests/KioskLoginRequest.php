<?php

namespace App\Http\Requests;

use App\Rules\KioskCodeIsValidRule;
use Illuminate\Foundation\Http\FormRequest;

class KioskLoginRequest extends FormRequest
{
    public function rules()
    {
        return [
            'code' => ['required', 'string', new KioskCodeIsValidRule()],
            'type' => 'required|in:standart,qrCode,barcode',
            'email' => 'required_if:type,standart|string'
        ];
    }

    public function authorize(): bool
    {
        return !auth()->check();
    }
}
