<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserFavoriteBulkReorderRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'favorites' => ['array', 'required'],
            'favorites.*.id' => ['required', 'integer',
                Rule::exists('student_favorites', 'id')->where(function ($query) {
                $query->where('school_id', optional(auth()->user())->school_id)
                    ->where(function ($query) {
                        return $query->where('user_id', optional(auth()->user())->id)
                            ->orWhere('user_id', null);
                    });
            })
            ],
            'favorites.*.position' => 'required|numeric|min:0|max:5000'
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }
}
