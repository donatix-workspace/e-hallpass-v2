<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;

class DeleteTempFilesFromStorageCommand extends Command
{
    protected $signature = 'storage:delete_temp';

    protected $description = 'Delete temp directory files';

    public function handle()
    {
        $storageTempPath = storage_path('/tmp/');

        File::deleteDirectory($storageTempPath);
    }
}
