<?php

namespace App\Rules;

use App\Models\Pin;
use Illuminate\Contracts\Validation\Rule;

class isPinUnique implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $pin = Pin::where([
            ['school_id', '=', auth()->user()->school_id],
            ['pin', '=', $value]
        ])->exists();

        return !$pin;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('pin.already.exists');
    }
}
