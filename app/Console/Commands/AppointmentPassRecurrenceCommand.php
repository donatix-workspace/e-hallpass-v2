<?php

namespace App\Console\Commands;

use App\Jobs\GenerateRecurrenceAppointmentsJob;
use App\Models\AppointmentPass;
use App\Models\RecurrenceAppointmentPass;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Console\Command;

class AppointmentPassRecurrenceCommand extends Command
{
    protected $signature = 'passes:appointment_recurrence_schedule';

    protected $description = 'Inserting recurrence appointments for the current time.';

    public function handle()
    {
        $recurrenceAppointmentPasses = RecurrenceAppointmentPass::where('recurrence_end_at', '>=', now()->toDateTimeString())
            ->where('recurrence_type', '!=', null)
            ->where('expired_at', null)
            ->where('canceled_at', null)
            ->get();

        foreach ($recurrenceAppointmentPasses as $recurrenceAppointmentPass) {

            $latestAppointmentPass = $recurrenceAppointmentPass
                ->appointmentPasses()
                ->latest('for_date')
                ->first();

            if ($latestAppointmentPass !== null) {
                if ($latestAppointmentPass->for_date->isPast()) {
                    GenerateRecurrenceAppointmentsJob::dispatch($recurrenceAppointmentPass, $latestAppointmentPass)
                        ->delay(now()->addSeconds(1));
                }
            }
        }
    }
}
