<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContactTracingCsvExportRequest extends FormRequest
{
    public function rules()
    {
        $user = auth()->user();

        return [
            'user_id' => ['required', 'integer', Rule::exists('users', 'id')->where('school_id', optional($user)->school_id)],
            'from_date' => ['required', 'date'],
            'to_date' => ['required', 'date','after_or_equal:from_date'],
        ];
    }

    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}
