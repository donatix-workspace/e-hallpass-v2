<?php

namespace App\Imports;

use App\Models\UserImport;
use App\Rules\DomainIsValid;
use App\Rules\InCaseInsesitive;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;
use Throwable;

class UsersImport implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure
{
    use SkipsFailures, Importable;

    public $schoolId;
    public $fileName;

    public function __construct($schoolId, $fileName)
    {
        $this->schoolId = $schoolId;
        $this->fileName = $fileName;
    }

    /**
     * @param array $row
     * @return UserImport
     */
    public function model(array $row): UserImport
    {
        return new UserImport([
            'first_name' => trim($row['firstname']),
            'last_name' => trim($row['lastname']),
            'email' => trim($row['email']),
            'role_id' => config('roles.' . strtolower($row['role'])),
            'status' => $this->getStatus($row['status']),
            'grade_year' => $row['gradyear'],
            'is_substitute' => $this->isSubstitute(request()->input('is_substitute')),
            'school_id' => $this->schoolId,
            'file_name' => $this->fileName,
            'is_failed' => false
        ]);
    }

    /**
     * @return int
     */
    public function batchSize(): int
    {
        return 1500;
    }

    /**
     * @param Throwable $e
     */
    public function failed(Throwable $e): void
    {
        Log::error($e);
    }


    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['string', Rule::requiredIf(function () {
                return !(request()->has('is_substitute') && request()->get('is_substitute'));
            }),
                new DomainIsValid($this->schoolId),
                Rule::notIn($this->getDuplicateEmails())
            ],
            'firstname' => 'required|string|max:30',
            'lastname' => 'required|string|max:30',
            'role' => ['required',
                new InCaseInsesitive(['student', 'teacher', 'admin', 'staff', 'superadmin'])
            ],
            'status' => ['required',
                new InCaseInsesitive(['active', 'inactive', 'archived', '1', '0'])
            ],
            'gradyear' => 'required_if:role,student|min:2000|numeric|max:2060|nullable'
        ];
    }

    protected function getStatus($status)
    {
        return in_array($status, [1, 'Active', 'active']);
    }

    protected function isSubstitute($isSubstitute)
    {
        if (!$isSubstitute) {
            return 0;
        }
        return in_array($isSubstitute, [1, 'Yes', 'yes']);
    }

    protected function getDuplicateEmails()
    {
        $emails = $this->getEmails();

        return $this->getDuplicates($emails);

    }

    protected function getEmails()
    {
        $rows = $this->toArray(storage_path('app/csv/' . $this->fileName))[0];
        return array_map(function ($row) {
            return $row['email'];
        }, $rows);
    }

    protected function getDuplicates($array)
    {
        return array_unique(array_diff_assoc($array, array_unique($array)));
    }


}
