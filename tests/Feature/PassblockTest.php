<?php

namespace Tests\Feature;

use App\Models\PassBlock;
use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PassblockTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;

    protected function setUp(): void
    {
        parent::setUp();

        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        $this->school = School::factory()->createOne();

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);
    }

    /**
     * @test
     * An admin user can view all of the pass blocks
     * @return void
     */
    public function an_admin_user_can_view_all_of_the_pass_blocks()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.admin.passes.blocks'))
            ->assertJsonStructure(['data', 'status'])->assertOk();
    }

    /**
     * @test
     * An admin user can create a pass block
     * @return void
     */
    public function an_admin_user_can_create_a_pass_block()
    {
        $this->actingAs($this->adminUser)
            ->post(route('api.v5.admin.passes.blocks.create'), [
                'from_date' => Carbon::now(optional($this->school)->getTimezone())->addMinute(1),
                'to_date' => Carbon::tomorrow(optional($this->school)->getTimezone()),
                'school_id' => $this->school->id,
                'reason' => 'Reason',
                'message' => 'Message',
                'kiosk' => 1,
                'students' => 0,
                'proxy' => 0,
                'appointments' => 0
            ])->assertJsonStructure(['data', 'status'])
            ->assertStatus(201);

        $this->assertDatabaseHas('pass_blocks', ['school_id' => $this->school->id]);
    }

    /**
     * @test
     * An admin user can delete a pass block record
     * @return void
     * @throws \Exception
     */
    public function an_admin_user_can_delete_a_pass_block_record()
    {
        PassBlock::where('school_id', $this->school->id)->delete();

        $passBlock = PassBlock::create([
            'from_date' => Carbon::now(),
            'to_date' => Carbon::tomorrow(),
            'school_id' => $this->school->id,
            'reason' => 'Reason',
            'message' => 'Message',
            'kiosk' => 1,
            'students' => 0,
            'proxy' => 0,
            'appointments' => 0
        ]);

        $this->actingAs($this->adminUser)
            ->delete(route('api.v5.admin.passes.blocks.delete', ['passBlock' => $passBlock->id]))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();
    }

    /**
     * @test
     * An admin user can create a pass block
     * @return void
     */
    public function an_admin_user_can_update_a_pass_block()
    {
        $passBlock = PassBlock::create([
            'from_date' => Carbon::now(),
            'to_date' => Carbon::tomorrow(),
            'school_id' => $this->school->id,
            'reason' => 'Reason',
            'message' => 'Message',
            'kiosk' => 1,
            'students' => 0,
            'proxy' => 0,
            'appointments' => 0
        ]);

        $this->actingAs($this->adminUser)
            ->put(route('api.v5.admin.passes.blocks.update', ['passBlock' => $passBlock->id]), [
                'from_date' => Carbon::now()->addMinute(),
                'to_date' => Carbon::tomorrow(),
                'school_id' => $this->school->id,
                'reason' => 'Reason',
                'message' => 'Message2',
                'kiosk' => 1,
                'students' => 0,
                'proxy' => 0,
                'appointments' => 0
            ])->assertJsonStructure(['data', 'status'])
            ->assertStatus(200);

        $this->assertDatabaseHas('pass_blocks', ['id' => $passBlock->id, 'school_id' => $this->school->id]);
    }
}
