<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;
use Carbon\Carbon;

class BlockPass extends Model
{
    use HasFactory;

    protected $table = 'block_passes';
    protected $with = ['school'];

    protected $dates = [
        'from_date',
        'to_date'
    ];

    protected $fillable = [
        'school_id', 'from_date',
        'to_date', 'student_pass',
        'proxy_pass', 'kiosk_pass',
        'reason', 'message'
    ];


    /**
     * @param DateTimeInterface $date
     * @return string
     */
    public function serializeDate(DateTimeInterface $date)
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($date, config('app.timezone'))->setTimezone(optional($school)->getTimezone())->format('Y-m-d H:i:s');
    }


    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function setFromDateAttribute($value)
    {
        $this->attributes['from_date'] = Carbon::parse($value)->setTimezone(config('app.timezone'));
    }

    public function setToDateAttribute($value)
    {
        $this->attributes['to_date'] = Carbon::parse($value)->setTimezone(config('app.timezone'));
    }
}
