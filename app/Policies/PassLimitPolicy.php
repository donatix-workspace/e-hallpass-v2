<?php

namespace App\Policies;

use App\Models\PassLimit;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PassLimitPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view any pass limits.
     *
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the pass limit.
     *
     * @param User $user
     * @param PassLimit $passLimit
     * @return bool
     */
    public function view(User $user, PassLimit $passLimit)
    {
        return $user->isAdmin() && $user->school_id === $passLimit->school_id;
    }

    /**
     * Determine whether the user can create pass limits.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the pass limit.
     *
     * @param User $user
     * @param PassLimit $passLimit
     * @return bool
     */
    public function update(User $user, PassLimit $passLimit)
    {
        return $user->isAdmin() && $user->school_id === $passLimit->school_id;
    }

    /**
     * Determine whether the user can delete the pass limit.
     *
     * @param User $user
     * @param PassLimit $passLimit
     * @return bool
     */
    public function delete(User $user, PassLimit $passLimit)
    {
        return $user->isAdmin() && $passLimit->school_id === $user->school_id;
    }

    /**
     * Determine whether the user can restore the pass limit.
     *
     * @param User $user
     * @param PassLimit $passLimit
     * @return bool
     */
    public function restore(User $user, PassLimit $passLimit)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the pass limit.
     *
     * @param User $user
     * @param PassLimit $passLimit
     * @return bool
     */
    public function forceDelete(User $user, PassLimit $passLimit)
    {
        //
    }
}
