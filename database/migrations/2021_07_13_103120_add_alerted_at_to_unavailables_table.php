<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAlertedAtToUnavailablesTable extends Migration
{
    public function up()
    {
        Schema::table('unavailables', function (Blueprint $table) {
            $table->timestamp('alerted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('unavailables', function (Blueprint $table) {
            $table->dropColumn('alerted_at');
        });
    }
}
