<?php

namespace Tests\Feature;

use App\Models\School;
use App\Models\User;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransparencyTest extends TestCase
{
    use RefreshDatabase;

    protected $adminUser;
    protected $teacherUser;
    protected $school;

    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        // Run roles migration
        (new RolesDatabaseSeeder())->run();
        // Create example school
        $this->school = School::factory()->createOne();


        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);

        $this->teacherUser = User::factory()->createOne([
            'role_id' => config('roles.teacher'),
            'school_id' => $this->school->id
        ]);
    }

    /**
     * @test
     * An admin user can change pass transparency state
     * @return void
     */
    public function an_admin_user_can_change_pass_transparency_state()
    {
        $this->actingAs($this->adminUser)
            ->put(route('api.v5.admin.transparencies.update', ['status' => 1]))
            ->assertStatus(201)
            ->assertJsonStructure(['data', 'status']);
    }

    /**
     * @test
     * An admin user can add user to except the transparency
     * @return void
     */
    public function an_admin_user_can_add_user_to_except_the_transparency()
    {
        $this->actingAs($this->adminUser)
            ->withHeaders(['Accept' => 'application/json'])
            ->post(route('api.v5.admin.transparencies.create', ['user_id' => $this->teacherUser->id]))
            ->assertStatus(201)
            ->assertJsonStructure(['data', 'status']);

        $this->assertDatabaseHas('transparency_users', ['user_id' => $this->teacherUser->id]);
    }

    /**
     * @test
     * An admin user can view transparency form properties
     * @return void
     */
    public function an_admin_user_can_view_transparency_form_properties()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.admin.transparencies'))
            ->assertJsonStructure(['data' => ['users', 'transparency_users'], 'status'])
            ->assertOk();
    }

}
