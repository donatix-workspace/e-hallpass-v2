<?php

namespace Tests\Commands;

use App\Models\Pass;
use App\Models\PassLimit;
use App\Models\School;
use App\Models\Unavailable;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\RolesDatabaseSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PassLimitRecurrenceCommandTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;
    protected $firstUser;
    protected $teacherUser;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);
    }

    /**
     * @test
     * A pass limit record will update the dates when it's weekly
     * @return void
     */
    public function a_pass_limit_will_update_the_dates_when_it_s_weekly()
    {
        Carbon::setTestNow(Carbon::parse('first Monday of April'));

        $passLimit = PassLimit::create([
            'school_id' => $this->school->id,
            'from_date' => Carbon::today(optional($this->school)->getTimezone())->subWeek(),
            'to_date' => Carbon::tomorrow(optional($this->school)->getTimezone())->addHours(4),
            'limitable_type' => School::class,
            'limitable_id' => $this->school->id,
            'limit' => 1,
            'reason' => 'Reason',
            'recurrence_type' => config('recurrences.weekly'),
        ]);

        $this->artisan('passes:limit_recurrence')->assertExitCode(0)->run();
        $this->assertEquals(
            Carbon::now(optional($this->school)->getTimezone())->format('Y-m-d'),
            $passLimit->refresh()->from_date->format('Y-m-d')
        );
    }

    /**
     * @test
     * A pass limit will update the dates when it's monthly
     * @return void
     */
    public function a_pass_limit_will_update_the_dates_when_it_s_monthly()
    {
        Carbon::setTestNow('first Monday Of April');

        $passLimit = PassLimit::create([
            'school_id' => $this->school->id,
            'from_date' => Carbon::today(optional($this->school)->getTimezone())->subMonth(),
            'to_date' => Carbon::yesterday(optional($this->school)->getTimezone())->addHours(4),
            'limitable_type' => School::class,
            'limitable_id' => $this->school->id,
            'limit' => 1,
            'reason' => 'Reason',
            'recurrence_type' => config('recurrences.monthly'),
        ]);

        $this->artisan('passes:limit_recurrence')->assertExitCode(0)->run();

        $this->assertEquals(
            Carbon::now(optional($this->school)->getTimezone())->format('Y-m-d'),
            $passLimit->refresh()->from_date->format('Y-m-d')
        );
    }

    /**
     * @test
     * A pass limit will update the dates when it's daily
     * @return void
     */
    public function a_pass_limit_will_update_the_dates_when_it_s_daily()
    {
        Carbon::setTestNow('first Monday of April');

        $passLimit = PassLimit::create([
            'school_id' => $this->school->id,
            'from_date' => Carbon::yesterday(optional($this->school)->getTimezone())->subHours(6),
            'to_date' => Carbon::yesterday(optional($this->school)->getTimezone())->subHours(4),
            'limitable_type' => School::class,
            'limitable_id' => $this->school->id,
            'limit' => 1,
            'reason' => 'Reason',
            'recurrence_type' => config('recurrences.daily'),
        ]);

        $this->artisan('passes:limit_recurrence')
            ->assertExitCode(0)
            ->run();

        $this->assertEquals(
            Carbon::now(optional($this->school)->getTimezone())->isWeekend() ? Carbon::now(optional($this->school)->getTimezone())
                ->next(Carbon::MONDAY)->format('Y-m-d') :
                Carbon::now(optional($this->school)->getTimezone())->format('Y-m-d'),
            $passLimit->refresh()->from_date->format('Y-m-d')
        );
    }
}
