<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFromAndToKioskToPassesTable extends Migration
{
    public function up()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table
                ->unsignedBigInteger('from_kiosk_id')
                ->after('from_type')
                ->nullable();

            $table
                ->unsignedBigInteger('to_kiosk_id')
                ->after('to_type')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->dropColumn('from_kiosk_id');
            $table->dropColumn('to_kiosk_id');
        });
    }
}
