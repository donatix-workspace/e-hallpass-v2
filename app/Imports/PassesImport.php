<?php

namespace App\Imports;

use App\Models\Pass;
use App\Models\School;
use App\Models\StaffSchedule;
use App\Models\Transparency;
use App\Models\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PassesImport implements FromQuery, WithHeadings, WithMapping
{
    public $filters;
    public $forDashboard;

    public function __construct($filters, $forDashboard = false)
    {
        $this->filters = $filters;
        $this->forDashboard = $forDashboard;
    }

    public function query()
    {
        $startDate = Carbon::parse(
            now()
                ->setTimezone(
                    auth()
                        ->user()
                        ->school->getTimezone()
                )
                ->startOfDay(),
            auth()
                ->user()
                ->school->getTimezone()
        )->setTimezone(config('app.timezone'));
        $endDate = Carbon::parse(
            now()
                ->setTimezone(
                    auth()
                        ->user()
                        ->school->getTimezone()
                )
                ->endOfDay(),
            auth()
                ->user()
                ->school->getTimezone()
        )->setTimezone(config('app.timezone'));

        if ($this->forDashboard) {
            if (request()->has('sort') || request()->has('search_query')) {
                return Pass::searchableQueries(
                    Pass::recordsPerPage(),
                    !(request()->input('search_query') !== null),
                    $this->filters,
                    true,
                    false
                );
            } elseif (!Transparency::isEnabled()) {
                return Pass::where(
                    'passes.school_id',
                    auth()->user()->school_id
                )
                    ->select('passes.*')
                    ->useIndex(Pass::PASS_HISTORY_MYSQL_INDEX)
                    ->where('passes.parent_id', null)
                    ->where('passes.canceled_at', null)
                    ->whereBetween('passes.created_at', [$startDate, $endDate])
                    ->orderBy('passes.created_at', 'desc')
                    ->leftJoin(
                        'passes as child',
                        'passes.id',
                        '=',
                        'child.parent_id'
                    )
                    ->filter($this->filters);
            } else {
                $user = auth()->user();

                $staffSchedules = StaffSchedule::where('user_id', $user->id)
                    ->get()
                    ->pluck('room_id');

                return Pass::where(
                    'passes.school_id',
                    auth()->user()->school_id
                )
                    ->select('passes.*')
                    ->useIndex(Pass::PASS_HISTORY_MYSQL_INDEX)
                    ->where('passes.parent_id', null)
                    ->whereBetween('passes.created_at', [$startDate, $endDate])
                    ->where('passes.canceled_at', null)
                    ->leftJoin(
                        'passes as child',
                        'passes.id',
                        '=',
                        'child.parent_id'
                    )
                    ->transparencyWithColumnPrefix(
                        $user,
                        $staffSchedules,
                        $staffSchedules
                    )
                    ->filter($this->filters);
            }
        }

        return Pass::nonSearchableQueries($this->filters, 0, false);
    }

    public function headings(): array
    {
        return [
            'Date',
            'Total',
            'First Name',
            'Last Name',
            'Email',
            'Out Location',
            'Out Time',
            'In Location',
            'In Time',
            'Out Location',
            'Out Time',
            'In Location',
            'In Time',
            'Type',
            'Comment'
        ];
    }

    public function map($row): array
    {
        $childFrom = null;
        $childApproved = null;
        $childTo = null;
        $childToCompleted = null;
        $comment = null;
        $school = School::findCacheFirst($row->school_id);

        if ($row->child !== null) {
            $childFrom =
                $row->child->from_type === User::class
                    ? optional($row->child->from)->first_name .
                        ' ' .
                        optional($row->child->from)->last_name
                    : optional($row->child->from)->name;
            $childApproved = School::convertTimeToCorrectTimezone(
                optional($row->child->approved_at)->toDateTimeString(),
                $school->getTimezone(),
                false,
                true
            );
            $childTo =
                $row->child->to_type === User::class
                    ? optional($row->child->to)->first_name .
                        ' ' .
                        optional($row->child->to)->last_name
                    : optional($row->child->to)->name;
            $childToCompleted = School::convertTimeToCorrectTimezone(
                optional($row->child->completed_at)->toDateTimeString(),
                $school->getTimezone(),
                false,
                true
            );

            if ($row->comments()->first() !== null) {
                $comment = $row->comments()->first()->comment;
            }
        }

        return [
            $row->created_at->toDateString(),
            $row->total_time,
            optional($row->user)->first_name,
            optional($row->user)->last_name,
            optional($row->user)->email,
            $row->from_type === User::class
                ? optional($row->from)->first_name .
                    ' ' .
                    optional($row->from)->last_name
                : optional($row->from)->name,
            School::convertTimeToCorrectTimezone(
                optional($row->approved_at)->toDateTimeString(),
                $school->getTimezone(),
                false,
                true
            ),
            $row->to_type === User::class
                ? optional($row->to)->first_name .
                    ' ' .
                    optional($row->to)->last_name
                : optional($row->to)->name,
            School::convertTimeToCorrectTimezone(
                optional($row->completed_at)->toDateTimeString(),
                $school->getTimezone(),
                false,
                true
            ),
            $childFrom,
            $childApproved,
            $childTo,
            $childToCompleted,
            $row->type,
            $comment
        ];
    }
}
