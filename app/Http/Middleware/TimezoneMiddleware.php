<?php

namespace App\Http\Middleware;

use Closure;

class TimezoneMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $timezone = auth()->check() ? auth()->user()->school->timezone : config('app.timezone');

        return $next($request);
    }
}
