<?php

namespace App\Http\Middleware;

use App\Models\PassBlock;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class CheckIfKioskPassesIsFullBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();

        $passFullBlock = PassBlock::fromSchoolId($user->school_id)
            ->whereKiosk(1)
            ->where('from_date', '<=', Carbon::now())
            ->where('to_date', '>=', Carbon::now());

        if ($passFullBlock->exists()) {
            // Pass full block serialization
            // to convert dates in correct timestamp
            $passFullBlock = $passFullBlock->first()->toArray();

            $fromDate = Carbon::parse(
                $passFullBlock['from_date']
            )->toDayDateTimeString();
            $toDate = Carbon::parse(
                $passFullBlock['to_date']
            )->toDayDateTimeString();

            return response()->json(
                [
                    'data' => [
                        'type' => 'student',
                        'reason' => $passFullBlock['reason'],
                        'alt_message' => $passFullBlock['message']
                    ],
                    'message' => __('passes.full.block', [
                        'from_date' => $fromDate,
                        'to_date' => $toDate,
                        'reason' => $passFullBlock['reason']
                    ]),
                    'status' => __('fail')
                ],
                422
            );
        }

        return $next($request);
    }
}
