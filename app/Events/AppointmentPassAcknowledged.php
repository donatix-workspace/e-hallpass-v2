<?php

namespace App\Events;

use App\Models\AppointmentPass;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AppointmentPassAcknowledged implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $appointmentPassId;
    public $schoolId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($appointmentPassId, $schoolId)
    {
        $this->appointmentPassId = $appointmentPassId;
        $this->schoolId = $schoolId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $appointmentPass = AppointmentPass::find($this->appointmentPassId);

        return [
            new PrivateChannel('appointments.passes.' . $this->schoolId),
            new PrivateChannel('user.appointments.passes.' . optional($appointmentPass)->user_id),
        ];
    }
}
