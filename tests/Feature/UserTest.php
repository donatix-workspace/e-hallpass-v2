<?php

namespace Tests\Feature;

use App\Models\Room;
use App\Models\RoomType;
use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    protected $adminUser;
    protected $firstUser;
    protected $secondUser;
    protected $school;
    protected $room;

    protected function setUp(): void
    {
        parent::setUp();

        // Run roles migration
        (new DatabaseSeeder())->run();

        // Create example school
        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);
        $this->secondUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);
        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);
    }

    /** @test */
    /* an admin can view users index page */
    public function an_admin_can_view_users_index_page()
    {
          $this->actingAs($this->adminUser)
              ->get(route('api.v5.users'))
              ->assertJsonStructure([
                  'data' => [
                      'kioskUsers',
                      'users',
                      'school',
                      'usersSummary'
                  ],
                  'status'
              ]);
    }

    /** @test */
    /* a student can't view users index page */
    public function a_student_cant_view_users_index_page()
    {
        $this->actingAs($this->firstUser)
            ->get(route('api.v5.users'))
            ->assertStatus(403);
    }
}
