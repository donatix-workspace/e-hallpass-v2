<?php

namespace App\Http\Requests;

use App\Models\Room;
use App\Models\User;
use App\Rules\PassRoomOrUserExistRule;
use App\Rules\RoomOrUserStringRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;

class StorePassKioskRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        $toType = Request::input('to_type');

        return [
            'kiosk_id' => 'required|integer|exists:kiosks,id',
            'kiosk_password' => 'required|string',
            'to_id' => ['integer', new PassRoomOrUserExistRule($toType)],
            'to_type' => ['required', 'string', new RoomOrUserStringRule],
        ];
    }
}
