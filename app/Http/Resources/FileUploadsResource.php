<?php

namespace App\Http\Resources;


class FileUploadsResource
{
    protected static $types = [
        0 => 'users',
        1 => 'sis attendance',
        2 => 'rosters',
        3 => 'bulk rosters',
        4 => 'catch all',
    ];

    protected static $status = [
        0 => 'STATUS_PENDING',
        1 => 'STATUS_PROCESSED',
        2 => 'STATUS_FAILED',
        3 => 'STATUS_IN_PROCESS',
    ];

    public static function collection($data)
    {
        return array_map(function ($element) {
            return self::transform($element);
        }, $data);
    }

    public static function transform($element)
    {
        return [
            'uuid' => $element['uuid'],
            'title' => $element['title'],
            'file' => $element['file'],
            'type' => self::$types[$element['type']],
            'status' => self::$status[$element['status']],
            'created' => $element['created']['date'],
            'modified' => optional($element['modified'])['date'],
        ];
    }


}
