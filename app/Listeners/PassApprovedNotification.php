<?php

namespace App\Listeners;

use App\Events\PassApproved;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PassApprovedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PassApproved  $event
     * @return void
     */
    public function handle(PassApproved $event)
    {
       // TODO: Mails, update the dashboard
    }
}
