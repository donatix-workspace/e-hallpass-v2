<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SummaryImports implements FromQuery, WithHeadings, WithMapping
{
    public $user;
    public const STUDENT_SIS_ID_IN_ARRAY = 1;
    public const STUDENT_SIS_ID_IN_ARRAY_MAPS = 1;
    public $aggregator;

    /**
     *
     */
    public function __construct($aggregator)
    {
        $this->user = auth()->user();
        $this->aggregator = $aggregator;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|Builder
     * @throws \Exception
     */
    public function query()
    {
        return $this->aggregator;
    }

    /**
     * @return string[]
     */
    public function headings(): array
    {
        $aggregateBy = request()->get('aggregate_by');

        if ($aggregateBy === PassSummaryReport::HIGHEST_PASS_DESTINATION) {
            $this->columnType = 'Location';
        }

        if ($aggregateBy === PassSummaryReport::HIGHEST_PASS_GRANTERS) {
            $this->columnType = 'Teacher';
        }

        $headings = [
            $this->columnType,
            'Student Number',
            'Total Time',
            'Total Passes',
            'Appointment Pass Total Time',
            'Kiosk Pass Total Time',
            'Proxy Pass Total Time',
            'Student Pass Total Time'
        ];
    }

    public function map($row): array
    {
        return [
            $row->destination_type === User::class
                ? $row->destination->first_name .
                    ' ' .
                    $row->destination->last_name
                : $row->destination->name,
            optional($row)->total_time_all === null
                ? '00:00:00'
                : $row->total_time_all,
            optional($row)->total_passes === null
                ? '00:00:00'
                : $row->total_passes,
            optional($row)->total_time_apt === null
                ? '00:00:00'
                : $row->total_time_apt,
            optional($row)->total_time_ksk === null
                ? '00:00:00'
                : $row->total_time_ksk,
            optional($row)->total_time_proxy === null
                ? '00:00:00'
                : $row->total_time_proxy,
            optional($row)->total_time_student === null
                ? '00:00:00'
                : $row->total_time_student
        ];
    }
}
