<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserImport extends Model
{
    use HasFactory;

    protected $table = 'users_imports';

    public const NOT_IMPORTED = 0;
    public const IMPORTED = 1;

    protected $guarded = [];

    public static function markInserted($insertedUserEmails, $fileName)
    {
        self::where('file_name', $fileName)->whereIn('email', $insertedUserEmails)->update(['inserted' => true]);
    }

    public static function importFailed($failures, $schoolId, $fileName)
    {
        $failuresModels = [];
        $failureRows = [];
        foreach ($failures as $failure) {
            $failureRows[$failure->row()] = [
                'user' => $failure->values(),
                'errors' => json_encode([
                    'attribute' => $failure->attribute(),
                    'errors' => $failure->errors(),
                ])
            ];
        }
        foreach ($failureRows as $row) {
            $failuresModels[] = [
                'first_name' => isset($row['user']['firstname']) ? trim($row['user']['firstname']) : '',
                'last_name' => isset($row['user']['lastname']) ? trim($row['user']['lastname']) : '',
                'email' => isset($row['user']['email']) ? trim($row['user']['email']) : '',
                'role_id' => isset($row['user']['role']) && config('roles.' . $row['user']['role']) ? config('roles.' . $row['user']['role']) : 0,
                'status' => isset($row['user']['status']) ? (int)($row['user']['status']) : 0,
                'grade_year' => isset($row['user']['gradyear']) ? (int)($row['user']['gradyear']) : null,
                'is_substitute' => isset($row['user']['is_substitute']) ? (int)($row['user']['is_substitute']) : 0,
                'school_id' => $schoolId,
                'file_name' => $fileName,
                'is_failed' => true,
                'errors'=>$row['errors']
            ];

        }

        self::insert($failuresModels);

    }
}
