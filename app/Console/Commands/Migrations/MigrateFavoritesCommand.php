<?php

namespace App\Console\Commands\Migrations;

use App\Models\Room;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateFavoritesCommand extends Command
{
    protected $signature = 'migrate_sys:fav';

    protected $description = 'Migrate favorites from ehpv1 to ehpv2';

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $oldDB = DB::connection('oldDB');
        $this->info('--- Migrating Favorites --- ');
        $oldDB->disableQueryLog();

        // Manual change the limit to migrate with less memory usage
        $oldFavorites = $oldDB
            ->table('student_favorite')
            ->orderBy('id', 'desc')
            ->limit(1000000)
            ->where('migrated', 0)
            ->get()
            ->toArray();

        $this->output->progressStart(count($oldFavorites));

        $oldFavoritesChunk = array_chunk($oldFavorites, 2000);

        foreach ($oldFavoritesChunk as $oldFavoriteChunk) {
            $favoriteInsertableArray = [];
            $migratedIds = [];

            foreach ($oldFavoriteChunk as $oldFavorite) {
                $oldSchool = $oldDB
                    ->table('school_schedule')
                    ->where('school_id', $oldFavorite->school_id)
                    ->first();
                if ($oldSchool) {
                    $newSchool = DB::table('schools')
                        ->where('old_id', $oldSchool->school_id)
                        ->first();
                    if ($newSchool) {
                        $oldUserId = $oldDB
                            ->table('users')
                            ->where('user_id', $oldFavorite->student_id)
                            ->where('school_id', $oldFavorite->school_id)
                            ->first();
                        $newUserId = optional(
                            DB::table('users')
                                ->where('email', optional($oldUserId)->email)
                                ->where('school_id', $newSchool->id)
                                ->first()
                        )->id;

                        $destinationId = 0;

                        if ($oldFavorite->room_id === 0) {
                            $oldUser = $oldDB
                                ->table('users')
                                ->where('user_id', $oldFavorite->teacher_id)
                                ->where('school_id', $oldFavorite->school_id)
                                ->first();

                            $newUser = DB::table('users')
                                ->where('email', optional($oldUser)->email)
                                ->where('school_id', $newSchool->id)
                                ->first();
                            $destinationId = optional($newUser)->id;
                        } else {
                            $oldRoom = $oldDB
                                ->table('rooms')
                                ->where('room_id', $oldFavorite->room_id)
                                ->where('school_id', $oldFavorite->school_id)
                                ->first();

                            $newRoom = DB::table('rooms')
                                ->where('name', optional($oldRoom)->room_name)
                                ->where('school_id', $newSchool->id)
                                ->first();

                            $destinationId = optional($newRoom)->id;
                        }

                        if (
                            $newUserId !== null &&
                            ($destinationId !== null || $destinationId !== 0)
                        ) {
                            array_push($migratedIds, $oldFavorite->id);
                            $favoriteInsertableArray[] = [
                                'position' => $oldFavorite->possition,
                                'favorable_type' =>
                                    $oldFavorite->room_id !== null
                                        ? User::class
                                        : Room::class,
                                'favorable_id' => $destinationId,
                                'user_id' => $newUserId,
                                'school_id' => $newSchool->id,
                                'global' => 0,
                                'section' => null
                            ];
                        }
                    }
                }
            }

            $this->output->progressAdvance(count($favoriteInsertableArray));
            try {
                DB::table('student_favorites')->insert(
                    $favoriteInsertableArray
                );
                $oldDB
                    ->table('student_favorite')
                    ->whereIntegerInRaw('id', $migratedIds)
                    ->update(['migrated' => 1]);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
            sleep(1);
        }

        $this->output->progressFinish();
    }
}
