<?php

namespace App\Scopes;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class WithValidMembership implements Scope
{
    /**
     * @param Builder $builder
     * @param Model $model
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereHasIn('schoolUser', function ($query) {
            $query
                ->whereNull('schools_users.deleted_at')
                ->where('schools_users.status', User::ACTIVE)
                ->whereNull('schools_users.archived_at');
        });
    }
}
