<?php

namespace App\Console\Commands;

use App\Events\FavoriteUnavailableEvent;
use App\Events\UnavailableUpdate;
use App\Models\Unavailable;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Log;

class UnavailableInTimeCommand extends Command
{
    protected $signature = 'unavailables:in_time';

    protected $description = 'Check if Unavailable is in time';

    public function handle()
    {
        $unavailables = Unavailable::where(
            'from_date',
            '<=',
            Carbon::now()->toDateTimeString()
        )
            ->where('to_date', '>=', Carbon::now()->toDateTimeString())
            ->where('alerted_at', null)
            ->where('canceled_for_today_at', null)
            ->active()
            ->get();

        foreach ($unavailables as $unavailable) {
            event(new UnavailableUpdate($unavailable, Unavailable::ACTIVE));
            if (
                optional($unavailable->unavailable)->activeUnavailability !==
                null
            ) {
                event(
                    new FavoriteUnavailableEvent($unavailable->school_id, [
                        'type' => $unavailable->unavailable_type,
                        'id' => $unavailable->unavailable_id,
                        'unavailability' => $unavailable->unavailable->activeUnavailability->toArray()
                    ])
                );
            }

            //TODO:: ADD FCM ADULT
            Log::info('Unavailable update INTIME: ' . $unavailable->id);
            $unavailable->update([
                'alerted_at' => now()
            ]);
        }
    }
}
