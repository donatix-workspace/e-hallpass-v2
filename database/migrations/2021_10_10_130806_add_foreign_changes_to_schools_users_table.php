<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignChangesToSchoolsUsersTable extends Migration
{
    public function up()
    {
        Schema::table('schools_users', function (Blueprint $table) {
            $table->dropForeign('schools_users_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('schools_users', function (Blueprint $table) {
            //
        });
    }
}
