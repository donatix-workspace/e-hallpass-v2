<?php

namespace Database\Factories;

use App\Models\AppointmentNotification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class AppointmentNotificationFactory extends Factory
{
    protected $model = AppointmentNotification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'notification_type' => AppointmentNotification::CANCELED,
            'payload' => collect(["appointment" => ["id" => 1]])->toJson(),
            'seen_at' => Carbon::now()
        ];
    }
}
