SET FOREIGN_KEY_CHECKS = 0;
START TRANSACTION ;
    INSERT INTO ehp_v2_prod.student_favorites (position, favorable_type, favorable_id, user_id, school_id, created_at, updated_at, global, section) SELECT
        (SELECT NULL),
        (SELECT 'App\\Models\\Room'),
        (SELECT id FROM ehp_v2_prod.rooms WHERE old_id = oldR.room_id limit 1),
        (SELECT NULL),
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldR.school_id limit 1),
        (SELECT NOW()),
        (SELECT NOW()),
        (SELECT 1),
        (SELECT 'destination')
    FROM ehallpassDB.rooms as oldR WHERE oldR.favorites = 1;
COMMIT ;
SET FOREIGN_KEY_CHECKS = 1;
