<?php

namespace App\Http\Services\CommonUserService;

use App\Http\Services\CommonUserService\JWT;
use App\Models\AuthType;
use App\Models\Role;
use App\Models\School;
use App\Models\Status;
use App\Models\User;
use Exception;
use Firebase\JWT\ExpiredException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use http\Env\Response;
use http\Exception\InvalidArgumentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Log;
use Str;

class AuthenticationService
{

    protected $clientId;
    protected $clientSecret;
    protected $user;
    protected $userAccessToken;
    protected $baseUrl;
    protected $headers;
    public static $timestamp = null;

    public function __construct($clientId = null, $clientSecret = null)
    {
        $this->clientId = $clientId ?: config('commonuserservice.client_id');
        $this->clientSecret = $clientSecret ?: config('commonuserservice.client_secret');
        $this->baseUrl = config('commonuserservice.base_url');
        $this->ssoBaseUrl = config('commonuserservice.sso_base_url');
        $this->client = new Client([
                'allow_redirects' => true,
                'auth' => 'oauth',
                'cookies' => true,
                'verify' => false
            ]
        );
        $this->headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Access-Control-Allow-Origin' => "*",
        ];
        JWT::$leeway = 20;
    }

    public function call($method, $url, $data)
    {
        //logger('CALL', ['url' => $url, 'data' => $data]);
        try {
            $response = $this->client->request($method, $url, [
                'body' => json_encode($data),
                'headers' => $this->headers
            ]);
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            //logger('CALL ERROR', [$responseBodyAsString]);
            return false;
        }
        $cookieJar = $this->client->getConfig('cookies');

        //logger('CALL RESPONSE', [json_decode($response->getBody()), 'cookie' => $cookieJar->toArray()]);

        return json_decode($response->getBody(), 1);
    }

    public function login($request, $credentials)
    {
        Cache::put($state = Str::random(40), [
            'code_verifier' => $code_verifier = Str::random(128)
        ], now()->addDay());

        $codeChallenge = strtr(rtrim(
            base64_encode(hash('sha256', $code_verifier, true))
            , '='), '+/', '-_');

        $params = [
            'client_id' => config('commonuserservice.providers.sso'),
            'redirect_uri' => route('authcallback'),
            'response_type' => 'code',
            'scope' => 'email profile openid offline_acces',
            'state' => $state,
            'username' => $credentials['username'],
            'password' => $credentials['password'],
            'code_challenge' => $codeChallenge,
            'code_challenge_method' => 'S256',
        ];
        $url = $this->ssoBaseUrl . '/sso/login';
        $data = [
            'form_params' => $params,
            'cookies' => \GuzzleHttp\Cookie\CookieJar::fromArray([
                'laravel_session' => request()->cookie('laravel_session'),
            ], config('app.domain')),
        ];
        //logger('REDIRECT', ['url' => $url, 'data' => $data]);
        try {
            $response = $this->client->request('POST', $url, $data);
        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            //logger('REDIRECT ERROR', [$responseBodyAsString]);
            return null;
        }

        return json_decode($response->getBody()->getContents(), 1);

    }

    public function callback($request)
    {

        $cacheKey = $request->get('state');
        if (in_array($cacheKey, array_values(config('commonuserservice.auth_providers')))) {
            $authProviderId = $cacheKey;
            $cacheKey = 'state' . $cacheKey;
            $codeVerifier = $request->get('codeVerifier');
            $redirectUrl = $request->redirectUri;
        } else {
            $authProviderId = config('commonuserservice.auth_providers.sso');
            $cachedData = Cache::get($cacheKey);
            $codeVerifier = $cachedData['code_verifier'];
            $redirectUrl = route('authcallback');
        }

        $data = [
            'clientId' => $this->clientId,
            'authProviderId' => (int)$authProviderId,
            'clientSecret' => $this->clientSecret,
            'code' => $request->code,
            'codeVerifier' => $codeVerifier,
            'redirectUri' => $redirectUrl,
        ];
        //logger('SSO CALLBACK', [$data, $request->all()]);


        $commonUserService = $this->call('post', $this->baseUrl . '/authentication/token', $data);

        if (!$commonUserService) {
            return false;
        }

        $accessToken = $commonUserService['accessToken'];

        $user = self::getUserByAccessToken($accessToken);
        Session::put('accessToken', $accessToken);
        return $user['id'] ?? null;
    }

    public function socialLogin($request)
    {
        $provider = new Provider($request->get('state'));

        $fields = [
            'grant_type' => 'authorization_code',
            'client_id' => $provider->getClientId(),
            'client_secret' => $provider->getClientSecret(),
            'code' => $request->get('code'),
            'redirect_uri' => $request->get('redirectUri'),
            'code_verifier' => $request->get('codeVerifier'),
        ];

        $tokenResponse = $this->client->request('POST', $provider->getTokenUrl(), [
            'headers' => ['Accept' => 'application/json'],
            'form_params' => $fields,
        ]);
        //logger('SOCIAL GET TOKEN', json_decode($tokenResponse->getBody(), 1));

        $response = $this->client->request('GET', $provider->getInfoUrl(), [
            'query' => [
                'prettyPrint' => 'false',
            ],
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . Arr::get(json_decode($tokenResponse->getBody(), true), 'access_token'),
            ],
        ]);
        //logger('SOCIAL GET INFO', json_decode($response->getBody(), 1));

        return User::where('email', Arr::get(json_decode($response->getBody(), true), 'email'))->first();

    }

    public static function getUserByAccessToken($accessToken)
    {
        try {
            $decodedAccessToken = JWT::decode($accessToken, config('commonuserservice.public_key'), ['RS256']);
        } catch (Exception $e) {
            dd($e->getMessage());
            Log::error('Error decode access token: ' . $e->getMessage());
            return response()->json([
                'message' => __('auth.failed')
            ], 401);
        }
        $user = User::where('cus_uuid', $decodedAccessToken->uuid)->skipHidden()->first();

        if (!$user) {
            return null;
        }

        $membership = DB::table('schools_users')
            ->where('user_id', $user->id)
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->whereNull('archived_at')
            ->first();

        if (!$membership) {
            return null;
        }

        $user->update([
            'school_id' => $membership->school_id,
            'role_id' => $membership->role_id,
            'access_token' => $accessToken,
            'access_token_exp' => $decodedAccessToken->exp ?? 0
        ]);

        return $user->refresh();
    }

    public function hiddenLogin($accessToken, $schoolUuid)
    {

//        $accessToken = $this->refreshToken($accessToken);
//        dd($accessToken);


        session()->put('accessToken', $accessToken);
        try {
            $decodedAccessToken = JWT::decode($accessToken, config('commonuserservice.public_key'), ['RS256']);

        } catch (ExpiredException $e) {
            $accessToken = $this->refreshToken($accessToken);
            if ($accessToken) {
                $decodedAccessToken = JWT::decode($accessToken, config('commonuserservice.public_key'), ['RS256']);
            } else {
                return false;
            }
        } catch (Exception $e) {
            Log::error('Error decode access token: ' . $e->getMessage());
            return null;
        }
        $school = School::where('cus_uuid', $schoolUuid)->first();

        if (!$school) {
            return null;
        }

        $domain = $school->domains()->first();

        if (!$domain) {
            $domainName = Str::slug($school->name, '') . '.com';
        } else {
            $domainName = $school->domains()->first()->name;
        }

        $email = config('commonuserservice.simulate_user.email_prefix') . $domainName;

        $user = User::updateOrCreate(
            [
                'email' => $email,
                'school_id' => $school->id,
            ],
            [
                'cus_uuid' => null, //$decodedAccessToken->uuid remove actual uuid to prevent conflicts
                'first_name' => $school->name,
                'last_name' => config('commonuserservice.simulate_user.last_name'),
                'username' => config('commonuserservice.simulate_user.username'),
                'role_id' => Role::getRoleIdByName(User::Admin),
                'status' => User::ACTIVE,
                'auth_type' => AuthType::HIDDEN,
                'access_token' => $accessToken,
                'access_token_exp' => $decodedAccessToken->exp ?? 0
            ]
        );
        DB::table('schools_users')->updateOrInsert(
            [
                'school_id' => $user->school_id,
                'user_id' => $user->id,
            ],
            [
                'status' => User::ACTIVE,
                'archived_at' => null,
                'archived_by' => null,
                'deleted_at' => null,
                'role_id' => Role::getRoleIdByName(User::Admin),
            ]);


        return $user;
    }

    //    TODO

    public function passwordResetInitiate($email)
    {
        $url = $this->ssoBaseUrl . '/sso/user/password-reset/initiate';
        $data = [
            'form_params' => [
                'clientId' => $this->clientId,
                'email' => $email
            ],
        ];
        try {
            $response = $this->client->request('POST', $url, $data);
            //logger('PASSWORD RESET ERROR', [$response->getStatusCode(), $response->getBody()]);

        } catch (Exception $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            //logger('PASSWORD RESET ERROR', [$e->getCode(), $e->getMessage(), $responseBodyAsString]);
            return 400;
        }
        return $response->getStatusCode();

    }

    public function passwordResetComplete($rpCode)
    {
        $url = $this->ssoBaseUrl . '/sso/user/password-reset/complete';
        $data = [
            'form_params' => [
                'rpCode' => $rpCode
            ],
        ];
        try {
            $response = $this->client->request('POST', $url, $data);
        } catch (Exception $e) {
            //logger('Password Reset COMPLETE ERROR', [$e->getMessage()]);
            return null;
        }
        //logger('Password Reset COMPLETE RESPONSE', [$response->getBody()]);

    }


    public function getUser()
    {
        return $this->user;
    }


    public function getUserAccessToken()
    {
        return $this->userAccessToken;
    }

    protected function getPKCE($codeVerifier = null)
    {
        if (!$codeVerifier) {
            $codeVerifier = \Illuminate\Support\Str::random(128);
        }
        $encoded = base64_encode(hash('sha256', $codeVerifier, true));

        return strtr(rtrim($encoded, '='), '+/', '-_');

    }

    public function refreshToken($accessToken)
    {
        $refreshClient = new Client();
        $url = $this->baseUrl . '/authentication/token/refresh';
        $content = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
                'Content-Type' => 'application/json',
                'Accept' => '*/*'
            ],
            'body' => json_encode([
                'clientId' => $this->clientId,
                'clientSecret' => $this->clientSecret
            ])
        ];
        //logger('REFRESH TOKEN REQUEST', ["url" => $url, "content" => $content]);
        try {
            $response = $refreshClient->request("POST", $url, $content);
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            //logger('REFRESH TOKEN ERROR', [$responseBodyAsString]);
            return false;
        }


        return json_decode($response->getBody(), 1)['accessToken'];
    }
}
