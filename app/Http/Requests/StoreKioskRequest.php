<?php

namespace App\Http\Requests;

use App\Rules\RoomOrUserStringRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreKioskRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'kiosk_type_id' => 'required|integer|min:1',
            'kiosk_type' => ['required', 'string', new RoomOrUserStringRule()]
        ];
    }

    public function authorize(): bool
    {
        return auth()->check() && !auth()->user()->isStudent();
    }
}
