<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPassStatusIndexToPassesTable extends Migration
{
    public function up()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->index(
                ['pass_status', 'expired_at', 'canceled_at'],
                'passes_expired_condition_index'
            );
            $table->dropIndex('passes_canceled_at_index');
        });
    }

    public function down()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->dropIndex('passes_expired_condition_index');
        });
    }
}
