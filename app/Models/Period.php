<?php

namespace App\Models;

use App\Scopes\NotArchivedUser;
use App\Scopes\OnlyNotDeletedPeriod;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Override booted method
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new OnlyNotDeletedPeriod());
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointmentPasses()
    {
        return $this->hasMany(AppointmentPass::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recurrenceAppointmentPasses()
    {
        return $this->hasMany(RecurrenceAppointmentPass::class);
    }
}
