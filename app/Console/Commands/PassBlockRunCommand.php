<?php

namespace App\Console\Commands;

use App\Events\PassBlockRunnedEvent;
use App\Models\PassBlock;
use App\Models\School;
use App\Notifications\Push\SchoolNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class PassBlockRunCommand extends Command
{
    protected $signature = 'pass-blocks:run';

    protected $description = 'Run event to notify the users that there\'s a pass block.';

    /**
     * @return int
     */
    public function handle(): int
    {
        $schoolWithPassBlocks = School::with('activePassBlock')
            ->whereHas('activePassBlock', function ($query) {
                $query->where('notified_at', null);
            })
            ->get();

        foreach ($schoolWithPassBlocks as $schoolWithPassBlock) {
            event(
                new PassBlockRunnedEvent($schoolWithPassBlock->activePassBlock)
            );
            //            Notification::send($schoolWithPassBlock->activePassBlock, new SchoolNotification('PassBlockRunnedEvent'));
        }

        return 0;
    }
}
