<?php

namespace App\Http\Controllers\Admin\Passes;

use App\Http\Controllers\Controller;
use App\Http\Requests\PassTimeSettingRequest;
use App\Http\Resources\TeacherPassSettingResource;
use App\Jobs\FlagChangerOnPassesTimeChangeJob;
use App\Models\PassSetting;
use App\Models\TeacherPassSetting;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

/**
 * @group Passes Time settings
 */
class TimeController extends Controller
{
    /**
     * Get teacher pass time settings.
     *
     *
     * @return TeacherPassSettingResource
     * @apiResource App\Http\Resources\TeacherPassSettingResource
     * @apiResourceModel App\Models\TeacherPassSetting
     * @throws AuthorizationException
     * @authenticated
     */
    public function index()
    {
        $this->authorize('viewAny', TeacherPassSetting::class);

        $user = auth()->user();

        $teacherPassSettings = TeacherPassSetting::fromSchoolId($user->school_id)->first();
        $passDescription = PassSetting::where('type', 'passdescription')->first();

        return new TeacherPassSettingResource([
            'time_settings' => $teacherPassSettings,
            'description' => $passDescription
        ]);
    }

    /**
     * Update pass time settings
     *
     * @bodyParam white_passes_time time required Approval time
     * @bodyParam min_time time required Long running passes time
     * @bodyParam auto_expire_time time required Auto system expire time
     * @bodyParam max_time time required Extended pass time
     * @bodyParam awaiting_apt_time time required Awaiting apt approval time.
     * @bodyParam nurse_expire_time time required Nurse approval time
     *
     * @authenticated
     * @param PassTimeSettingRequest $request
     * @return TeacherPassSettingResource
     * @apiResource App\Http\Resources\TeacherPassSettingResource
     * @apiResourceModel App\Models\TeacherPassSetting
     * @throws AuthorizationException
     */
    public function update(PassTimeSettingRequest $request)
    {
        $teacherPassSettings = TeacherPassSetting::where('school_id', auth()->user()->school_id)->first();

        $this->authorize('update', $teacherPassSettings);

        $teacherPassSettings->update([
            'white_passes' => $request->input('white_passes_time') !== "00:00:00" ? $request->input('white_passes_time') : $teacherPassSettings->white_passes,
            'min_time' => $request->input('min_time') !== "00:00:00" ? $request->input('min_time') : $teacherPassSettings->min_time,
            'auto_expire_time' => $request->input('auto_expire_time') !== "00:00:00" ? $request->input('auto_expire_time') : $teacherPassSettings->auto_expire_time,
            'max_time' => $request->input('max_time') !== "00:00:00" ? $request->input('max_time') : $teacherPassSettings->max_time,
            'awaiting_apt' => $request->input('awaiting_apt_time') !== "00:00:00" ? $request->input('awaiting_apt_time') : $teacherPassSettings->awaiting_apt,
            'nurse_expire_time' => $request->input('nurse_expire_time') !== "00:00:00" ? $request->input('nurse_expire_time') : $teacherPassSettings->nurse_expire_time
        ]);
// TODO: Will do it if we need it
//        FlagChangerOnPassesTimeChangeJob::dispatch($teacherPassSettings->refresh(), $teacherPassSettings->school_id)
//            ->delay(now()->addSeconds(5))
//            ->onQueue('default');

        return new TeacherPassSettingResource($teacherPassSettings->refresh());
    }
}
