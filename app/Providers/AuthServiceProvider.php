<?php

namespace App\Providers;

use App\Models\ActivePassLimit;
use App\Models\Room;
use App\Models\StudentFavorite;
use App\Models\TeacherPassSetting;
use App\Models\User;
use App\Policies\ActivePassLimitPolicy;
use App\Policies\PolarityPolicy;
use App\Policies\RoomPolicy;
use App\Policies\StudentFavoritePolicy;
use App\Policies\TeacherPassSettingPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\Polarity;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Polarity::class => PolarityPolicy::class,
        Room::class => RoomPolicy::class,
        User::class => UserPolicy::class,
        ActivePassLimit::class => ActivePassLimitPolicy::class,
        StudentFavorite::class => StudentFavoritePolicy::class,
        TeacherPassSetting::class => TeacherPassSettingPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //Pass history gates
        Gate::define('pass-history-view', 'App\Policies\PassHistoryPolicy@viewAny');
        Gate::define('pass-history-update-time', 'App\Policies\PassHistoryPolicy@update');
        Gate::define('pass-history-run-filters', 'App\Policies\PassHistoryPolicy@runFilters');
        //Dashboard gates
        Gate::define('access-dashboard-actions', 'App\Policies\DashboardPolicy@actions');
        //Reports gates
        Gate::define('admin-reports-view-contact-tracing', 'App\Policies\ContactTracingPolicy@viewAny');

        if (!$this->app->routesAreCached()) {
            Passport::routes();
        }
    }
}
