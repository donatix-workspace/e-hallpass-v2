<?php

namespace App\Console\Commands\Migrations;

use Illuminate\Console\Command;

class MigrateRoleCommand extends Command
{
    protected $signature = 'migrate_sys:role';

    protected $description = 'Migrate Role From EhpV1 to EhpV2';

    public function handle()
    {
        $this->info('--- Migrating Roles --- ');
        $bar = $this->output->createProgressBar();
        $bar->start();
    }
}
