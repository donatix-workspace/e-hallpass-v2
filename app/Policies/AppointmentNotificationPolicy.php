<?php

namespace App\Policies;

use App\Models\AppointmentNotification;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AppointmentNotificationPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can view any appointment notifications.
     *
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->isStudent();
    }

    /**
     * Determine whether the user can view the appointment notification.
     *
     * @param User $user
     * @param AppointmentNotification $appointmentNotification
     * @return bool
     */
    public function view(User $user, AppointmentNotification $appointmentNotification)
    {
        return $user->isStudent() && $appointmentNotification->user_id === $user->id && $appointmentNotification->seen_at === null;
    }

    /**
     * Determine whether the user can create appointment notifications.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the appointment notification.
     *
     * @param User $user
     * @param AppointmentNotification $appointmentNotification
     * @return bool
     */
    public function update(User $user, AppointmentNotification $appointmentNotification)
    {
        return $user->isStudent() && $appointmentNotification->user_id === $user->id;
    }

    /**
     * Determine whether the user can delete the appointment notification.
     *
     * @param User $user
     * @param AppointmentNotification $appointmentNotification
     * @return bool
     */
    public function delete(User $user)
    {
        return $user->isStudent();
    }

    /**
     * Determine whether the user can restore the appointment notification.
     *
     * @param User $user
     * @param AppointmentNotification $appointmentNotification
     * @return bool
     */
    public function restore(User $user, AppointmentNotification $appointmentNotification)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the appointment notification.
     *
     * @param User $user
     * @param AppointmentNotification $appointmentNotification
     * @return bool
     */
    public function forceDelete(User $user, AppointmentNotification $appointmentNotification)
    {
        //
    }
}
