<?php

namespace App\Models;

use App\Elastic\PassBlockIndexConfigurator;
use App\Traits\ExtendedSearchable;
use App\Traits\PassBlockMapping;
use App\Traits\PerPage;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class PassBlock extends Model implements PushNotifications
{
    use HasFactory, ExtendedSearchable, PerPage, PassBlockMapping;

    protected $guarded = [];

    protected $indexConfigurator = PassBlockIndexConfigurator::class;

    protected $dates = [
        'from_date', 'to_date'
    ];

    /**
     * @param DateTimeInterface $date
     * @return string
     */
    public function serializeDate(DateTimeInterface $date)
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($date, config('app.timezone'))->setTimezone($school->getTimezone())->format('Y-m-d H:i:s');
    }

    public function toSearchableArray(): array
    {
        $passBlock = $this->toArray();

        $passBlock['from_date'] = Carbon::parse($passBlock['from_date'])->format('m/d/Y g:i A');
        $passBlock['to_date'] = Carbon::parse($passBlock['to_date'])->format('m/d/Y g:i A');

        return $passBlock;
    }

    /**
     * @param string $searchQuery
     * @param int $perPage
     * @param bool $pagination
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\ScoutElastic\Builders\FilterBuilder|\ScoutElastic\Builders\SearchBuilder
     */
    public static function searchableQuery(string $searchQuery, int $perPage = 25, bool $pagination = true)
    {
        $query = PassBlock::search($searchQuery)
            ->when(request()->has('sort'), function ($query) {
                $sort = explode(':', request()->get('sort'));
                if (count($sort) <= 1) {
                    return $query->orderBy('id', 'asc');
                }

                $sortingKeywords = ['asc', 'desc'];
                if (in_array($sort[1], $sortingKeywords)) {
                    return $query->orderBy($sort[0], $sort[1]);
                }
                return $query->orderBy('id', 'asc');
            })
            ->where('school_id', auth()->user()->school_id);

        return $pagination ? $query->paginate($perPage) : $query;
    }


    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }


    public function setFromDateAttribute($value)
    {
        $this->attributes['from_date'] = Carbon::parse($value)->setTimezone(config('app.timezone'));
    }

    public function setToDateAttribute($value)
    {
        $this->attributes['to_date'] = Carbon::parse($value)->setTimezone(config('app.timezone'));
    }

    public function getTopics()
    {
        $env = config('services.fcm.env');
        //dev.
        //local.
        //staging.
        //production.
        return "{$env}.pass-blocks.{$this->school_id}";
    }

    public function getNotificationTitle($event)
    {
        return __($event . 'Title');
    }

    public function getNotificationBody($event)
    {
        return __($event . 'Body');
    }

    public function getData($event, $options)
    {
        $passBlock = $this->toArray();

        return [
            'event' => $event,
            'payload' => [
                'id' => $passBlock['id'],
                'from_date' => $passBlock['from_date'],
                'to_date' => $passBlock['to_date'],
                'reason' => $passBlock['reason'],
                'message' => $passBlock['message']
            ],
            'options' => $options
        ];
    }

    public function getNotificationSettings($event, $options)
    {
        return [
            'topics' => $this->getTopics(),
            'title' => $this->getNotificationTitle($event),
            'body' => $this->getNotificationBody($event),
            'data' => $this->getData($event, $options),
        ];
    }
}
