<?php

namespace App\Elastic\Rules;

use App\Models\Pass;
use App\Models\Room;
use App\Models\School;
use App\Models\StaffSchedule;
use App\Models\User;
use Carbon\Carbon;
use ScoutElastic\Builders\SearchBuilder;
use ScoutElastic\SearchRule;

/**
 * Filter checkboxes search rule
 */
class PassHistoryFilterSearchRule extends SearchRule
{
    private $user;
    private $schoolTimezone;
    private $globalSearchLimitation;
    private $baseConditions;

    public function __construct(SearchBuilder $builder)
    {
        $this->user = auth()->user();

        $this->schoolTimezone = School::findCacheFirst(
            $this->user->school_id
        )->getTimezone();

        $this->globalSearchLimitation =
            request()->filled('global_search') &&
            filter_var(
                request()->get('global_search'),
                FILTER_VALIDATE_BOOLEAN
            );

        parent::__construct($builder);
    }

    /**
     * @return $this
     */
    public function setBaseConditions($queryString): PassHistoryFilterSearchRule
    {
        $dates = request()->get('dates');

        // We pass Carbon object directly to the whereBetween() below
        // To correct counters being reset by UTC time.
        $datesArray = explode(',', $dates);

        $schoolTimezone = $this->schoolTimezone;

        $startDate = now()->toDateTimeString();
        $endDate = now()->toDateTimeString();

        if (count($datesArray) === 2) {
            $startDate = Carbon::parse(
                $datesArray[0] . ' 00:00:00',
                $schoolTimezone
            )->toDateTimeString();

            $endDate = Carbon::parse(
                $datesArray[1] . ' 23:59:59',
                $schoolTimezone
            )->toDateTimeString();
        }

        $this->baseConditions = [
            [
                'match' => [
                    'school_id' => $this->user->school_id
                ]
            ],
            [
                'match' => [
                    'parent_id' => 0
                ]
            ],
            [
                'match' => [
                    'pass_status' => Pass::INACTIVE
                ]
            ],
            [
                'match' => [
                    'pass_status' => Pass::INACTIVE
                ]
            ],
            [
                'range' => [
                    'created_at' => [
                        'gte' => $startDate,
                        'lte' => $endDate
                    ]
                ]
            ]
        ];

        if (request()->filled('by_students')) {
            $byStudentIds = explode(',', request()->get('by_students'));
            $this->baseConditions[] = [
                'terms' => [
                    'user_id' => $byStudentIds
                ]
            ];
        }

        if (request()->filled('by_teachers')) {
            $byTeacherIds = explode(',', request()->get('by_teachers'));
            $this->baseConditions[] = [
                'bool' => [
                    'should' => [
                        [
                            'bool' => [
                                'must' => [
                                    'terms' => [
                                        'approved_by' => $byTeacherIds
                                    ]
                                ]
                            ]
                        ],
                        [
                            'bool' => [
                                'must' => [
                                    'terms' => [
                                        'completed_by' => $byTeacherIds
                                    ]
                                ]
                            ]
                        ],
                        [
                            'bool' => [
                                'must' => [
                                    [
                                        'match' => [
                                            'from_type' => User::class
                                        ]
                                    ],
                                    [
                                        'terms' => [
                                            'from_id' => $byTeacherIds
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            'bool' => [
                                'must' => [
                                    [
                                        'match' => [
                                            'to_type' => User::class
                                        ]
                                    ],
                                    [
                                        'terms' => [
                                            'to_id' => $byTeacherIds
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        if (request()->filled('by_rooms')) {
            $byRoomIds = explode(',', request()->get('by_rooms'));
            $this->baseConditions[] = [
                'bool' => [
                    'should' => [
                        [
                            'bool' => [
                                'must' => [
                                    [
                                        'match' => [
                                            'from_type' => Room::class
                                        ]
                                    ],
                                    [
                                        'terms' => [
                                            'from_id' => $byRoomIds
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            'bool' => [
                                'must' => [
                                    [
                                        'match' => [
                                            'to_type' => Room::class
                                        ]
                                    ],
                                    [
                                        'terms' => [
                                            'to_id' => $byRoomIds
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        if ($queryString === '__all') {
            $this->baseConditions[] = [
                'match_all' => (object) []
            ];
        } else {
            if ($this->globalSearchLimitation) {
                $this->baseConditions[] = [
                    'query_string' => [
                        'query' => $this->builder->query,
                        'fields' => [
                            'created_date',
                            'user.name',
                            'from.name',
                            'to.name',
                            'child.from.name',
                            'child.to.name',
                            'type'
                        ]
                    ]
                ];
            } else {
                $this->baseConditions[] = [
                    'query_string' => ['query' => $this->builder->query]
                ];
            }
        }

        // Condition for removing the missed passes.
        $this->baseConditions[] = [
            'bool' => [
                'should' => [
                    [
                        'bool' => [
                            'must' => [
                                [
                                    'exists' => [
                                        'field' => 'approved_at'
                                    ]
                                ],
                                [
                                    'match' => [
                                        'canceled_at' => 0
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        'bool' => [
                            'must' => [],
                            'must_not' => [
                                [
                                    'exists' => [
                                        'field' => 'approved_at'
                                    ]
                                ],
                                [
                                    'match' => [
                                        'canceled_at' => 0
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        'bool' => [
                            'must' => [
                                [
                                    'match' => [
                                        'type' => Pass::APPOINTMENT_PASS
                                    ]
                                ]
                            ],
                            'must_not' => [
                                [
                                    'exists' => [
                                        'field' => 'approved_at'
                                    ]
                                ],
                                [
                                    'match' => [
                                        'expired_at' => 0
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        return $this;
    }

    /**
     * @return \array[][]
     */
    public function systemEndedFilter(): array
    {
        return [
            [
                'bool' => [
                    'must' => [
                        [
                            'match' => [
                                'system_completed' => Pass::ACTIVE
                            ]
                        ],
                        [
                            'match' => [
                                'extended_at' => 0
                            ]
                        ]
                    ]
                ]
            ],
            [
                'bool' => [
                    'must' => [
                        [
                            'exists' => [
                                'field' => 'child'
                            ]
                        ],
                        [
                            'match' => [
                                'child.system_completed' => Pass::ACTIVE
                            ]
                        ],
                        [
                            'match' => [
                                'system_completed' => Pass::INACTIVE
                            ]
                        ],
                        [
                            'match' => [
                                'extended_at' => 0
                            ]
                        ],
                        [
                            'match' => [
                                'child.extended_at' => 0
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @return \array[][]
     */
    public function yellowTimeFilter(): array
    {
        return [
            [
                'bool' => [
                    'must_not' => [
                        [
                            'match' => [
                                'flagged_at' => 0
                            ]
                        ],
                        [
                            'exists' => [
                                'field' => 'child'
                            ]
                        ]
                    ],
                    'must' => [
                        [
                            'match' => [
                                'system_completed' => Pass::INACTIVE
                            ]
                        ]
                    ]
                ]
            ],
            [
                'bool' => [
                    'must_not' => [
                        [
                            'match' => [
                                'child.flagged_at' => 0
                            ]
                        ]
                    ],
                    'must' => [
                        [
                            'exists' => [
                                'field' => 'child'
                            ]
                        ],
                        [
                            'match' => [
                                'child.system_completed' => Pass::INACTIVE
                            ]
                        ]
                    ]
                ]
            ],
            [
                'bool' => [
                    'must_not' => [
                        [
                            'match' => [
                                'flagged_at' => 0
                            ]
                        ]
                    ],
                    'must' => [
                        [
                            'exists' => [
                                'field' => 'child'
                            ]
                        ],
                        [
                            'match' => [
                                'child.system_completed' => Pass::INACTIVE
                            ]
                        ],
                        [
                            'match' => [
                                'child.flagged_at' => 0
                            ]
                        ],
                        [
                            'match' => [
                                'child.extended_at' => 0
                            ]
                        ],
                        [
                            'match' => [
                                'system_completed' => Pass::INACTIVE
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @return \array[][]
     */
    public function extendedTimeFilter(): array
    {
        return [
            [
                'bool' => [
                    'must_not' => [
                        [
                            'match' => [
                                'extended_at' => 0
                            ]
                        ]
                    ],
                    'must' => [
                        [
                            'match' => [
                                'system_completed' => Pass::ACTIVE
                            ]
                        ]
                    ]
                ]
            ],
            [
                'bool' => [
                    'must_not' => [
                        [
                            'match' => [
                                'child.extended_at' => 0
                            ]
                        ]
                    ],
                    'must' => [
                        [
                            'exists' => [
                                'field' => 'child'
                            ]
                        ],
                        [
                            'match' => [
                                'child.system_completed' => Pass::ACTIVE
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @return \array[][]
     */
    public function editedFilter(): array
    {
        return [
            [
                'bool' => [
                    'must_not' => [
                        [
                            'match' => [
                                'edited_at' => 0
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @return \array[][]
     */
    public function onlyMissedPassesFilter(): array
    {
        return [
            [
                'bool' => [
                    'must_not' => [
                        [
                            'match' => [
                                'expired_at' => 0
                            ]
                        ],
                        [
                            'exists' => [
                                'field' => 'approved_at'
                            ]
                        ]
                    ],
                    'must' => [
                        [
                            'match' => [
                                'type' => Pass::APPOINTMENT_PASS
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @return \array[][]
     */
    public function onlyCanceledPassesFilter(): array
    {
        return [
            [
                'bool' => [
                    'must_not' => [
                        [
                            'match' => [
                                'canceled_at' => 0
                            ]
                        ],
                        [
                            'exists' => [
                                'field' => 'approved_at'
                            ]
                        ]
                    ],
                    'must' => [
                        [
                            'match' => [
                                'expired_at' => 0
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @return \array[][]
     */
    public function missedRequestFilter(): array
    {
        return [
            [
                'bool' => [
                    'must_not' => [
                        [
                            'match' => [
                                'expired_at' => 0
                            ]
                        ],
                        [
                            'exists' => [
                                'field' => 'approved_at'
                            ]
                        ]
                    ],
                    'must' => [
                        [
                            'match' => [
                                'pass_status' => Pass::INACTIVE
                            ]
                        ],
                        [
                            'match' => [
                                'type' => Pass::APPOINTMENT_PASS
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @return \array[][]
     */
    public function onlyMyPassesCriteria(): array
    {
        $staffSchedules = StaffSchedule::where('user_id', $this->user->id)
            ->get()
            ->pluck('room_id');

        return [
            'bool' => [
                'should' => [
                    [
                        'bool' => [
                            'must' => [
                                [
                                    'match' => [
                                        'approved_by' => $this->user->id
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        'bool' => [
                            'must' => [
                                [
                                    'match' => [
                                        'completed_by' => $this->user->id
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        'bool' => [
                            'must' => [
                                [
                                    'match' => [
                                        'from_id' => $this->user->id
                                    ]
                                ],
                                [
                                    'match' => [
                                        'from_type' => User::class
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        'bool' => [
                            'must' => [
                                [
                                    'match' => [
                                        'to_id' => $this->user->id
                                    ]
                                ],
                                [
                                    'match' => [
                                        'to_type' => User::class
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        'bool' => [
                            'must' => [
                                [
                                    'terms' => [
                                        'to_id' => $staffSchedules
                                    ]
                                ],
                                [
                                    'match' => [
                                        'to_type' => Room::class
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        'bool' => [
                            'must' => [
                                [
                                    'terms' => [
                                        'from_id' => $staffSchedules
                                    ]
                                ],
                                [
                                    'match' => [
                                        'from_type' => Room::class
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function buildQueryPayload(): array
    {
        /**
         * Setting up the base condition such as default
         * where logic clauses.
         */
        $this->setBaseConditions($this->builder->query);

        /*
         * Decode the filters from the request
         */
        $filters = collect(
            json_decode(request()->get('filters_list'))
        )->toArray();

        /**
         * The array that contains the payload for the rule.
         */
        $filtersElasticArray = [];

        /**
         * Building base pass switches conditions.
         */
        if (
            filter_var(
                request()->get('only_my_passes'),
                FILTER_VALIDATE_BOOLEAN
            )
        ) {
            $myPassesCriteria = $this->buildFilterMethodName(
                'only_my_passes',
                true
            );

            $this->baseConditions[] = $this->$myPassesCriteria();
        }

        /**
         * If there's any filters we add it to the query.
         */
        if (count($filters)) {
            foreach ($filters as $filter) {
                $filterFunctionName = $this->buildFilterMethodName($filter);

                if (method_exists($this, $filterFunctionName)) {
                    foreach (
                        $this->$filterFunctionName()
                        as $conditionElements
                    ) {
                        $filtersElasticArray['filter']['bool']['must']['bool'][
                            'should'
                        ][] = $conditionElements;
                    }
                }
            }
        }

        $filtersElasticArray['must'] = $this->baseConditions;

        return $filtersElasticArray;
    }

    /**
     * @param string $filter
     * @param bool $criteria
     * @return string
     */
    public function buildFilterMethodName(
        string $filter,
        bool $criteria = false
    ): string {
        $filterFunctionName =
            str_replace(' ', '', ucwords(str_replace('_', ' ', $filter))) .
            (!$criteria ? 'Filter' : 'Criteria');

        return lcfirst($filterFunctionName);
    }

    /**
     * @param $filtersElasticArray
     * @param $method
     */
    public function addToQuery(&$filtersElasticArray, $method)
    {
        foreach ($this->$method() as $criteria) {
            $filtersElasticArray['filter']['bool']['should'][] = $criteria;
        }
    }
}
