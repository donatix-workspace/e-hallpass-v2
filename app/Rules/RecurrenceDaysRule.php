<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RecurrenceDaysRule implements Rule
{
    public $daysInTheWeek;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->daysInTheWeek = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $daysInTheWeek = collect($this->daysInTheWeek);
        $inputDates = collect(json_decode($value, true)['days']);

        return $inputDates->diff($daysInTheWeek)->count() === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('days.invalid.week.day');
    }
}
