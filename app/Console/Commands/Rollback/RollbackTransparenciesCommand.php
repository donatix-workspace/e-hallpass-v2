<?php

namespace App\Console\Commands\Rollback;

use DB;

use Illuminate\Console\Command;

class RollbackTransparenciesCommand extends Command
{
    protected $signature = 'ehpv1_rollback:transparencies';

    protected $description = 'Rollback the transparencies from EHPV2 to EHPV1';

    /**
     * @return int
     */
    public function handle(): int
    {
        $this->info('--- Rollback Transparencies from EHPV2 to EHPV1 ---');

        $newDb = DB::connection();
        $oldDb = DB::connection('oldDB');

        $newDb->disableQueryLog();
        $oldDb->disableQueryLog();

        $transparencies = $newDb->table('transparencies')
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();

        $this->output->progressStart(count($transparencies));

        $transparenciesChunk = array_chunk($transparencies, 1500);

        foreach ($transparenciesChunk as $transparencyChunk) {
            $insertableTransparencyArray = [];
            foreach ($transparencyChunk as $transparency) {
                $newSchool = $newDb->table('schools')->select('old_id')->where('id', $transparency->school_id)
                    ->first();

                if ($newSchool) {
                    $insertableTransparencyArray[] = [
                        'school_id' => $newSchool->old_id,
                        'created_at' => now(),
                        'updated_at' => now()
                    ];
                }
            }

            try {
                DB::beginTransaction();

                $oldDb->table('privacy_setting')
                    ->insert($insertableTransparencyArray);

                $this->output->progressAdvance(count($insertableTransparencyArray));
                DB::commit();
            } catch (\Throwable $e) {
                $this->info($e->getMessage());
            }

        }

        $this->output->progressFinish();

        return 0;
    }
}
