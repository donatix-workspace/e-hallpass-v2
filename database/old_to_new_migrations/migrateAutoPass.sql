SET foreign_key_checks =0;
START TRANSACTION ;
    INSERT INTO ehp_v2_prod.auto_passes (room_id,school_id,created_at,updated_at) SELECT
                                          (SELECT id FROM ehp_v2_prod.rooms WHERE old_id=oldAp.room_id limit 1),
                                          (SELECT id FROM ehp_v2_prod.schools WHERE old_id=oldAp.school_id limit 1),
                                          (SELECT CONVERT_TZ(oldAp.created_at, 'America/New_York', 'UTC')),
                                          (SELECT CONVERT_TZ(oldAp.updated_at, 'America/New_York', 'UTC'))
    FROM ehallpassDB.auto_pass as oldAp;
COMMIT ;
