<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SummaryResultsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'search_id' => 'required|string'
        ];
    }

    public function authorize(): bool
    {
        return auth()->check() && auth()->user()->isAdmin();
    }
}
