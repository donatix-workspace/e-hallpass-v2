<?php

namespace App\Http\Resources;

use App\Models\School;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\PassLimit */
class PassLimitResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $school = School::findCacheFirst($this->school_id);

        return [
            'id' => $this->id,
            'limitable_type' => $this->limitable_type,
            'limitable_id' => $this->limitable_id,
            'limitable' => $this->limitable,
            'for' => $this->for,
            'limit' => $this->limit,
            'from_date' => School::convertTimeToCorrectTimezone($this->from_date, $school->getTimezone(), true, false, 'm/d/Y'),
            'to_date' => School::convertTimeToCorrectTimezone($this->to_date, $school->getTimezone(), true, false, 'm/d/Y'),
            'grade_year' => $this->grade_year,
            'reason' => $this->reason,
            'recurrence_type' => $this->recurrence_type,
            'created_at' => School::convertTimeToCorrectTimezone($this->created_at, $school->getTimezone()),
            'updated_at' => School::convertTimeToCorrectTimezone($this->updated_at, $school->getTimezone()),
            'school_id' => $this->school_id,
        ];
    }

    public function with($request)
    {
        return [
            'status' => 'success'
        ];
    }
}
