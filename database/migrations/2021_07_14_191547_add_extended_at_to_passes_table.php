<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtendedAtToPassesTable extends Migration
{
    public function up()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->timestamp('extended_at')->nullable();
        });
    }

    public function down()
    {
        Schema::table('passes', function (Blueprint $table) {
            $table->dropColumn('extended_at');
        });
    }
}
