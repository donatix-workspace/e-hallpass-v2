<?php

namespace App\Rules;

use App\Models\RoomRestriction;
use Illuminate\Contracts\Validation\Rule;

class RoomRestrictionValidTypeRule implements Rule
{
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value === RoomRestriction::ACCESS_DENIED_GROUP || $value === RoomRestriction::MEMBERS_ONLY_GROUP;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('room.restriction.invalid.type');
    }
}
