<?php

namespace App\Console\Commands\Migrations;

use App\Models\School;
use App\Models\SchoolModule;
use DB;
use Illuminate\Console\Command;

class MigrateModuleCommand extends Command
{
    protected $signature = 'migrate_sys:modules';

    protected $description = 'Migrate the modules from EHPV1 to EHPV2';

    public function handle()
    {
        $this->info('--- Migrating Modules --- ');
        $oldDb = DB::connection('oldDB');
        $oldDb->disableQueryLog();
        DB::connection('mysql')->disableQueryLog();
        DB::connection()->disableQueryLog();

        $this->output->progressStart($oldDb->table('modules')->count());

        $oldDb
            ->table('modules')
            ->whereIn('school_id',[1842,1844,1845,1846,1847,1848,1849,1850,1851,1852,1853,1854,1855,1856,1857,1858,1859,1860,1861,1863,1864,1865,1866,1867,1868,1869,1870])
            ->chunkById(150, function ($schoolModules) use ($oldDb) {
                $schoolModules->each(function ($schoolModule) use ($oldDb) {
                    $oldSchool = $oldDb
                        ->table('school_schedule')
                        ->where('school_id', $schoolModule->school_id)
                        ->first();
                    if ($oldSchool) {
                        $newSchool = School::where(
                            'old_id',
                            $oldSchool->school_id
                        )->first();
                        if ($newSchool) {
                            $oldSchoolModuleJson = json_decode(
                                $schoolModule->option_json,
                                true
                            );
                            if ($schoolModule->module_id === 1) {
                                $aptModuleJson = json_decode(
                                    $schoolModule->option_json,
                                    true
                                );
                                $nowJson = [
                                    'admin_edit' => 0,
                                    'teacher' => 0,
                                    'staff' => 0,
                                    'location' => 0
                                ];
                                if ($aptModuleJson !== null) {
                                    if (
                                        array_key_exists(
                                            'editadmin',
                                            $aptModuleJson
                                        ) &&
                                        $aptModuleJson['editadmin'] !== null
                                    ) {
                                        $nowJson['admin_edit'] = 1;
                                    }

                                    if (
                                        array_key_exists(
                                            'Teachers',
                                            $aptModuleJson
                                        ) &&
                                        $aptModuleJson['Teachers'] !== null
                                    ) {
                                        $nowJson['teacher'] = 1;
                                    }

                                    if (
                                        array_key_exists(
                                            'Admins',
                                            $aptModuleJson
                                        ) &&
                                        $aptModuleJson['Admins'] !== null
                                    ) {
                                        $nowJson['admin'] = 1;
                                    }

                                    if (
                                        array_key_exists(
                                            'Staff',
                                            $aptModuleJson
                                        ) &&
                                        $aptModuleJson['Staff'] !== null
                                    ) {
                                        $nowJson['staff'] = 1;
                                    }

                                    if (
                                        array_key_exists(
                                            'Locations',
                                            $aptModuleJson
                                        ) &&
                                        $aptModuleJson['Locations'] !== null
                                    ) {
                                        $nowJson['location'] = 1;
                                    }
                                }
                            }

                            SchoolModule::create([
                                'module_id' => $schoolModule->module_id,
                                'school_id' => $newSchool->id,
                                'status' => $schoolModule->status,
                                'permission' => $schoolModule->permission,
                                'option_json' =>
                                    $schoolModule->module_id === 1
                                        ? collect($nowJson)->toJson()
                                        : $schoolModule->option_json
                            ]);
                        }
                    }

                    $this->output->progressAdvance();
                });
            });

        sleep(2);
        $this->output->progressFinish();
    }
}
