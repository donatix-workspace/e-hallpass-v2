<?php

namespace App\Imports;

use App\Models\TransparencyUser;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TransparenciesExport implements FromQuery, WithHeadings, WithMapping
{
    public function query()
    {
        $user = auth()->user();
        return TransparencyUser::where('school_id', $user->school_id);
    }

    public function headings(): array
    {
        return [
            'Last Name',
            'First Name',
        ];
    }

    public function map($row): array
    {
        return [
            $row->user->first_name,
            $row->user->last_name,
        ];
    }
}
