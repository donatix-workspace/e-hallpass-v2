<?php

namespace App\Http\Requests;

use App\Models\Room;
use App\Models\User;
use App\Rules\CheckIfAppointmentExistsWithin15MinRule;
use App\Rules\CheckIfItsMidnightRule;
use App\Rules\PassRoomOrUserExistRule;
use App\Rules\RoomOrUserExistsAppointmentRule;
use App\Rules\RoomOrUserExistsRule;
use App\Rules\RoomOrUserStringRule;
use App\Rules\StudentAppointmentTimeRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;

class StudentAppointmentRequest extends FormRequest
{
    public function rules()
    {
        $toType = Request::input('to_type');

        return [
            'for_date' => [
                'required',
                'date',
                'after:now',
                new StudentAppointmentTimeRule(),
                new CheckIfItsMidnightRule(true)
            ],
            'time' => [
                'required',
                'date',
                new CheckIfAppointmentExistsWithin15MinRule()
            ],
            'reason' => [
                'max:255',
                Rule::requiredIf(function () use ($toType) {
                    return $toType === User::class ||
                        !Room::where('id', request()->input('to_id'))
                            ->where(function ($query) {
                                return $query
                                    ->where(
                                        'comment_type',
                                        Room::COMMENT_HIDDEN
                                    )
                                    ->orWhere(
                                        'comment_type',
                                        Room::COMMENT_OPTIONAL
                                    );
                            })
                            ->exists();
                })
            ],
            'from_id' => [
                'integer',
                new RoomOrUserExistsAppointmentRule(),
                'required'
            ],
            'to_id' => [
                'integer',
                new PassRoomOrUserExistRule($toType),
                'required'
            ],
            'from_type' => ['required', 'string', new RoomOrUserStringRule()],
            'to_type' => ['required', 'string', new RoomOrUserStringRule()],
            'period_id' => 'integer|required|exists:periods,id'
        ];
    }

    public function authorize()
    {
        return auth()
            ->user()
            ->isStudent();
    }
}
