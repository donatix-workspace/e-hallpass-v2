<?php

namespace App\Models;

use App\Http\Filters\Filterable;
use App\Elastic\RoomIndexConfigurator;
use App\Traits\ExtendedSearchable;
use App\Traits\PerPage;
use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\RoomMapping;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use ScoutElastic\Searchable;

class Room extends Model
{
    use HasFactory, Filterable, ExtendedSearchable, RoomMapping, PerPage;

    protected $indexConfigurator = RoomIndexConfigurator::class;
    //    protected $with = ['studentFavorites']; //Deprecated

    protected $guarded = ['id', 'created_at'];

    protected $appends = ['user_ids_assigned', 'extended_pass_time'];

    const TRIP_LAYOVER = 'Layover';
    const TRIP_ONE_WAY = 'One Way';
    const TRIP_ROUNDTRIP = 'Roundtrip';
    const ACTIVE = 1;

    const COMMENT_MANDATORY = 'Mandatory';
    const COMMENT_OPTIONAL = 'Optional';
    const COMMENT_HIDDEN = 'Hidden';

    /**
     * @param DateTimeInterface $date
     * @return string
     */
    public function serializeDate(DateTimeInterface $date)
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($date, config('app.timezone'))
            ->setTimezone(optional($school)->getTimezone())
            ->format('Y-m-d H:i:s');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function passSummaryReport(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(PassSummaryReport::class, 'destination');
    }

    /**
     * @return bool
     */
    public function getAdminFavoritiesAttribute(): bool
    {
        return $this->studentFavorites
            ->where('global', Room::ACTIVE)
            ->where('school_id', $this->school_id)
            ->where('user_id', null)
            ->first() !== null;
    }

    /**
     * @param $query
     * @param int $status
     * @return mixed
     */
    public function scopeStatus($query, $status = 1)
    {
        return $query->where('status', $status);
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kiosks()
    {
        return $this->hasMany(Kiosk::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function nurses()
    {
        return $this->hasOne(NurseRoom::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function roomType()
    {
        return $this->belongsTo(RoomType::class, 'type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function fromPass()
    {
        return $this->morphMany(Pass::class, 'from');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function destinationPolarityReport()
    {
        return $this->morphMany(PolarityReport::class, 'destination');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function toPass()
    {
        return $this->morphMany(Pass::class, 'to');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function pins()
    {
        return $this->morphMany(Pin::class, 'pinnable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function ownerPin(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(Pin::class, 'pinnable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function autoPasses()
    {
        return $this->hasMany(AutoPass::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function autoRoomTeachers()
    {
        return $this->hasMany(AutoRoomTeacher::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function unavailable()
    {
        return $this->morphMany(Unavailable::class, 'unavailable');
    }

    public function activeUnavailability()
    {
        return $this->morphOne(Unavailable::class, 'unavailable')
            ->without('unavailable')
            ->where('status', Unavailable::ACTIVE)
            ->where('from_date', '<=', Carbon::now()->toDateTimeString())
            ->where('to_date', '>=', Carbon::now()->toDateTimeString())
            ->where('canceled_for_today_at', null);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function capacity()
    {
        return $this->hasOne(LocationCapacity::class)->where('limit', '!=', -1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function restrictions()
    {
        return $this->hasMany(RoomRestriction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function staffSchedules()
    {
        return $this->hasMany(StaffSchedule::class);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getUserIdsAssignedAttribute(): \Illuminate\Support\Collection
    {
        $staffSchedule = $this->staffSchedules()
            ->without('user', 'room')
            ->select('user_id')
            ->where('room_id', $this->id)
            ->where('school_id', $this->school_id)
            ->get('user_id')
            ->pluck('user_id');

        return $staffSchedule !== null ? $staffSchedule : [];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function fromAppointment()
    {
        return $this->morphMany(AppointmentPass::class, 'from');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function toAppointment()
    {
        return $this->morphMany(AppointmentPass::class, 'to');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function studentFavorites()
    {
        return $this->morphMany(StudentFavorite::class, 'favorable');
    }

    /**
     * @return mixed
     */
    public function getFavoritesAttribute()
    {
        return StudentFavorite::select(['id'])
            ->where('favorable_type', Room::class)
            ->where('favorable_id', $this->id)
            ->whereSchoolId($this->school_id)
            ->exists();
    }

    /**
     * @return mixed|string
     */
    public function getExtendedPassTimeAttribute()
    {
        return $this->nurses ? $this->nurses->end_time : '00:00:00';
    }

    public function toggleAdminFavorite()
    {
        $this->update([
            'admin_favorite' => DB::raw('NOT admin_favorite')
        ]);

        return $this;
    }

    public function removeFromFavorites()
    {
        $this->update([
            'admin_favorite' => 0
        ]);
        StudentFavorite::where('favorable_id', $this->id)
            ->where('favorable_type', Room::class)
            ->delete();
        return $this;
    }

    public function isInactive()
    {
        return $this->status <= 0;
    }
}
