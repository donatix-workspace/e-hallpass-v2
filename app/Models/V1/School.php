<?php

namespace App\Models\V1;


use App\Http\Services\CommonUserService\CommonUserService;
use App\Models\Module;
use App\Models\SchoolModule;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class School extends Model
{
    protected $connection = 'oldDB';
    protected $table = 'school_schedule';
    protected $primaryKey = 'school_id';

    protected $attributes = ['logo', 'modules'];

    public function getLogoAttribute()
    {
        $path = 'public/schoolimage/' . $this->school_logo;
        if (Storage::disk('s3v1')->exists($path)) {
            return Storage::disk('s3v1')->temporaryUrl(
                $path,
                now()->addMinutes(60)
            );
        }
        return null;
    }

    public function getModulesAttribute()
    {
        $moduleNames = [
            1 => "Appointment Pass",
            2 => "Auto Check-In",
            3 => "Kiosk",
        ];

        $modules = $this->getModules();

        $result = [];

        foreach ($modules as $module) {

            $result[] = [
                'name' => $moduleNames[$module->module_id],
                'status' => $module->status,
            ];
        }

        return $result;
    }

    protected function getModules()
    {
        return \App\Models\V1\Module::where(
            'school_id',
            $this->school_id
        )->get();
    }

    public function migrateModules()
    {

        $school = \App\Models\School::where('old_id', $this->school_id)->first();
        if (!$school) {
            return false;
        }

        $v1Modules = $this->getModules();

        SchoolModule::where('school_id', $school->id)->delete();
        //Update or Create modules from v1 to v2
        foreach ($v1Modules as $v1Module) {
            $oldSchoolModuleJson = json_decode($v1Module->option_json, true);
            $nowJson = null;

            // Apt module
            if ($v1Module->module_id === 1) {
                $aptModuleJson = json_decode($v1Module->option_json, true);
                $nowJson = [
                    'admin_edit' => 0,
                    'teacher' => 0,
                    'staff' => 0,
                    'location' => 0,
                    'email' => 0
                ];
                if ($aptModuleJson !== null) {
                    if (
                        array_key_exists('editadmin', $aptModuleJson) &&
                        $aptModuleJson['editadmin'] !== null
                    ) {
                        $nowJson['admin_edit'] = 1;
                    }

                    if (
                        array_key_exists('Teachers', $aptModuleJson) &&
                        $aptModuleJson['Teachers'] !== null
                    ) {
                        $nowJson['teacher'] = 1;
                    }

                    if (
                        array_key_exists('Admins', $aptModuleJson) &&
                        $aptModuleJson['Admins'] !== null
                    ) {
                        $nowJson['admin'] = 1;
                    }

                    if (
                        array_key_exists('Staff', $aptModuleJson) &&
                        $aptModuleJson['Staff'] !== null
                    ) {
                        $nowJson['staff'] = 1;
                    }

                    if (
                        array_key_exists('Locations', $aptModuleJson) &&
                        $aptModuleJson['Locations'] !== null
                    ) {
                        $nowJson['location'] = 1;
                    }

                    if (
                        array_key_exists('email', $aptModuleJson) &&
                        $aptModuleJson['email'] !== null
                    ) {
                        $nowJson['email'] = 1;
                    }
                }
            }

            // Kiosk module
            if ($v1Module->module_id === 3) {
                $nowJson = [
                    'kurl' => 0,
                    'usbcard' => 0,
                    'barcode' => 0,
                    'spassword' => 0
                ];
                if ($oldSchoolModuleJson !== null) {
                    if (
                        array_key_exists('kurl', $oldSchoolModuleJson) &&
                        $oldSchoolModuleJson['kurl'] !== null
                    ) {
                        $nowJson['kurl'] = 1;
                    }

                    if (
                        array_key_exists('barcode', $oldSchoolModuleJson) &&
                        $oldSchoolModuleJson['barcode'] !== null
                    ) {
                        $nowJson['barcode'] = 1;
                    }

                    if (
                        array_key_exists('usbcard', $oldSchoolModuleJson) &&
                        $oldSchoolModuleJson['usbcard'] !== null
                    ) {
                        $nowJson['usbcard'] = 1;
                    }
                }
            }

            $moduleJson = collect($nowJson)->toJson();

            SchoolModule::create(
                [
                    'module_id' => $v1Module->module_id,
                    'school_id' => $school->id,
                    'status' => $v1Module->status ?? 0,
                    'permission' => $v1Module->permission,
                    'option_json' =>
                        $moduleJson !== null
                            ? $moduleJson
                            : $v1Module->option_json
                ]
            );

        }

        $school->refresh();


        $body = [
            'appointmentPass' =>
                $school->hasModule(Module::APPOINTMENTPASS),
            'autoCheckIn' =>
                $school->hasModule(Module::AUTO_CHECKIN),
            'kiosk' =>
                $school->hasModule(Module::KIOSK)
        ];


        $this->updateSchool($school, $body);
    }

    public function migrateLogo()
    {
        $school = \App\Models\School::where('old_id', $this->school_id)->first();
        if (!$school) {
            return false;
        }
        if ($this->school_logo == '') {
            return false;
        }
        $extArray = explode('.', $this->school_logo);
        $logo = null;
        if (count($extArray) > 1) {
            $path = 'public/schoolimage/' . $this->school_logo;
            if (Storage::disk('s3v1')->exists($path)) {
                $logo =
                    "data:image/{$extArray[1]};base64," .
                    base64_encode(
                        Storage::disk('s3v1')->get($path)
                    );
            }
        }
        if (!$logo) {
            return false;
        }
        $body['logo'] = $logo;
        return $this->updateSchool($school, $body);

    }

    protected function updateSchool($school, $body = null)
    {
        $adminUser = User::where('email', 'viktor.lalev@donatix.net')
            ->latest()
            ->first();
        $client = new CommonUserService($adminUser);

        $projectManagerUuid = '7990a773-1064-4ed3-9571-cb3a6d1056ca';
        $ehpClient = 'ba63e471-0b6a-4437-afab-ff38bc36c092';

        if (!$body) {
            $body = [];

            $domains = [$school->school_domain];
            if ($school->school_domain != $school->student_domain) {
                $domains[] = $school->student_domain;
            }

            $body = [
                'clientOptionCollection' => [
                    [
                        'client' => $ehpClient,
                        'projectManager' => $projectManagerUuid,
                        'relationshipManager' => $projectManagerUuid
                    ]
                ],
                'authProviderIdCollection' => $this->getAtuhProviders($school),
                'appointmentPass' => $school->hasModule(
                    Module::APPOINTMENTPASS
                ),
                'autoCheckIn' => $school->hasModule(Module::AUTO_CHECKIN),
                'kiosk' => $school->hasModule(Module::KIOSK),
                'buildingCollection' => null,
                'description' => $school->description,
                //            "district" => $excellSchool->district,
                'emailDomainCollection' => $domains,
                'fullName' => $school->name,
                'name' => $school->name,
                //            "timeZone" => $excellSchool->timezone_uuid,
                'l10nUsTerritorialSubdivisionCodeAnsi' => $school->state,
                'zipCode' => $school->zip,
                //            "sftpCredentialsEmail" => "",
                //            "sftpSendCredentials" => "",
                'userProviderCollection' => $this->getUserProviderCollection(
                    $school
                ),
                'status' => $school->status,
                //            "providingStudentPhotos" => 0,
                'sftpDirectory' => $this->getSftpDirectory(
                    $school->name,
                    $school
                )
            ];

            if ($oldSchool) {
                $extArray = explode('.', $oldSchool->school_logo);

                $logo = null;
                if (count($extArray) > 1) {
                    $logo =
                        "data:image/{$extArray[1]};base64," .
                        base64_encode(
                            Storage::disk('s3v1')->get(
                                'public/schoolimage/' . $oldSchool->school_logo
                            )
                        );
                }
                $body['logo'] = $logo;
                $body['tier'] = $oldSchool->total_users;
                $body['sftpFilePrefix'] = $oldSchool->archive_prefix;
            }
        }
        //        unset($body['logo']);
        //        dd($school->cus_uuid, $body);
        $result = $client->updateSchool($school->cus_uuid, $body);

        if ($result) {
            return true;
        }
        return false;
    }
}
