<?php

namespace App\Console\Commands;

use App\Models\Pass;
use App\Models\InviteUser;
use App\Models\Kiosk;
use App\Models\KioskUser;
use App\Models\Module;
use App\Models\Notification;
use App\Models\PassSummaryReport;
use App\Models\Role;
use App\Models\Room;
use App\Models\RoomType;
use App\Models\School;
use App\Models\StaffSchedule;
use App\Models\StudentFavorite;
use App\Models\tmp;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateSystem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will connect to your old database and will transfer the information into the new database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//                $this->importSchools();
//                $this->importModules();
//                $this->importSchoolModules();
//                $this->importRoles();
        $this->importUsers();
//                $this->importRoomTypes();
//                $this->importKiosk();
//        $this->importPasses();
    }

    protected function importSchools(): void
    {
        $oldSchools = DB::connection('oldDB')->table('school_schedule');

        $this->info("\n" . 'Importing schools');
        $bar = $this->output->createProgressBar($oldSchools->count());
        $bar->start();

        $oldSchools->orderBy('school_id')->chunk(200, function ($schoolsChunk) use ($bar) {
            $schoolsChunk->each(function ($oldSchool) use ($bar) {
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();
                if ($newSchool) {
                    return true;
                }
                School::create([
                    'parent' => $oldSchool->parent,
                    'name' => $oldSchool->school_name,
                    'description' => $oldSchool->description,
                    'logo' => $oldSchool->school_logo,
                    'stop_status' => $oldSchool->stop_status,
                    'student_domain' => $oldSchool->student_domain,
                    'school_domain' => $oldSchool->school_domain,
                    'time_format' => $oldSchool->time_format,
                    'status' => $oldSchool->status,
                    'webservice' => $oldSchool->webservice,
                    'avatar_api' => $oldSchool->avatarapi,
                    'api_key' => $oldSchool->apikey,
                    'total_user_role' => $oldSchool->total_user_role,
                    'total_users' => $oldSchool->total_users,
                    'upload_folder' => $oldSchool->upload_folder,
                    'archive_prefix' => $oldSchool->archive_prefix,
                    'csv_headers' => $oldSchool->csv_headers,
                    'csv_roles' => $oldSchool->csv_roles,
                    'if_username' => $oldSchool->if_username,
                    'is_user_csv' => $oldSchool->is_user_csv,
                    'is_sftp_image' => $oldSchool->is_sftp_image,
                    'timezone' => $oldSchool->school_timezone,
                    'is_manual' => $oldSchool->is_manual,
                    'message' => $oldSchool->message,
                ]);

                $bar->advance();
            });
        });

        $this->info("\n" . 'Schools imported successfully.');

    }

    protected function importModules(): void
    {
        $oldModules = DB::connection('oldDB')->table('allmodules')->get();

        $this->info("\n" . 'Importing modules');
        $bar = $this->output->createProgressBar($oldModules->count());
        $bar->start();

        $oldModules->each(function ($oldModule) use ($bar) {
            $newModule = Module::where('name', $oldModule->name)->first();
            if ($newModule) {
                return true;
            }
            Module::create([
                'name' => $oldModule->name,
                'display_name' => $oldModule->display_name,
                'option_json' => $oldModule->option_json,
                'status' => $oldModule->status,
                'description' => $oldModule->description,
            ]);

            $bar->advance();
        });

        $this->info("\n" . 'Modules imported successfully.');
    }

    protected function importSchoolModules(): void
    {
        $oldSchoolModules = DB::connection('oldDB')->table('modules');

        $this->info("\n" . 'Importing school modules');
        $bar = $this->output->createProgressBar($oldSchoolModules->count());
        $bar->start();

        $oldSchoolModules->orderBy('id')->chunk(200, function ($oldSchoolModulesChunk) use ($bar) {
            $oldSchoolModulesChunk->each(function ($oldSchoolModule) use ($bar) {

                $oldModule = DB::connection('oldDB')->table('allmodules')->where('id', $oldSchoolModule->module_id)->first();
                if (!$oldModule) {
                    return true;
                }
                $newModule = Module::where('name', $oldModule->name)->first();
                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldSchoolModule->school_id)->first();
                if (!$oldSchool) {
                    return true;
                }
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();
                if (!$newSchool || !$newModule) {
                    return true;
                }

                $newSchoolModules = DB::table('school_modules')->where('school_id', $newSchool->id)->where('module_id', $newModule->id)->first();
                if ($newSchoolModules) {
                    return true;
                }

                DB::table('school_modules')->insert([
                    'school_id' => $newSchool->id,
                    'module_id' => $newModule->id,
                    'option_json' => $oldSchoolModule->option_json,
                    'permission' => $oldSchoolModule->permission,
                    'status' => $oldSchoolModule->status,
                ]);

                $bar->advance();
            });
        });

        $this->info("\n" . 'School Modules imported successfully.');

    }

    protected function importRoles(): void
    {
        $oldRoles = DB::connection('oldDB')->table('user_roles')->get();

        $this->info("\n" . 'Importing roles');
        $bar = $this->output->createProgressBar($oldRoles->count());
        $bar->start();

        $oldRoles->each(function ($oldRole) use ($bar) {
            $newRole = Role::where('name', $oldRole->role_name)->first();
            if ($newRole) {
                return true;
            }
            Role::create([
                'name' => $oldRole->role_name,
                'status' => $oldRole->status,
            ]);

            $bar->advance();
        });

        $this->info("\n" . 'Roles imported successfully.');
    }

    protected function importUsers(): void
    {
        $oldUsers = DB::connection('oldDB')->table('users');

        $this->info("\n" . 'Importing users');
        $bar = $this->output->createProgressBar($oldUsers->count());
        $bar->start();

        $oldUsers->orderBy('user_id')->chunk(300, function ($usersChunk) use ($bar) {
            $usersChunk->each(function ($oldUser) use ($bar) {
                $userDuplicated = User::where('email', $oldUser->email)->first();
                if ($userDuplicated) {
                    //$this->info('Duplicated user -> '.$oldUser->email . "\n");
                    DB::table('schools_users')->updateOrInsert(
                        [
                            'school_id' => $userDuplicated->school_id,
                            'user_id' => $userDuplicated->id,
                        ],
                        [
                            'role_id' => $userDuplicated->role_id,
                        ]);
                    return true;
                }

                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldUser->school_id)->first();
                if (!$oldSchool) {
                    return true;
                }
                $newSchool = School::where('school_domain', $oldSchool->school_domain)->first();

                $oldRole = DB::connection('oldDB')->table('user_roles')->where('role_id', $oldUser->role_id)->first();
                if (!$oldRole) {
                    return true;
                }

                $newRole = Role::where('name', $oldRole->role_name)->first();

                if ($userDuplicated || is_null($oldUser->firstname) || is_null($oldUser->lastname) || !$newSchool || !$newRole) {
                    return true;
                }


                try {
                    $user = User::create([
                        'student_sis_id' => $oldUser->student_sis_id,
                        'email' => $oldUser->email,
                        'username' => $oldUser->username,
                        'password' => $oldUser->password,
                        'first_name' => $oldUser->firstname,
                        'last_name' => $oldUser->lastname,
                        'metatag' => $oldUser->metatag,
                        'role_id' => $newRole->id,
                        'avatar' => $oldUser->avatar,
                        'grade_year' => $oldUser->gradyear,
                        'status' => $oldUser->status,
                        'school_id' => $newSchool->id,
                        'is_searchable' => $oldUser->is_searchable,
                        'user_initials' => $oldUser->user_initials,
                        'is_loggin' => $oldUser->is_loggin,
                        'clever_id' => $oldUser->clever_id,
                        'clever_building_id' => $oldUser->clever_building_id,
                        'updated_role' => $oldUser->updated_role,
                        'last_logged_in_at' => $oldUser->last_logged_in_at,
                        'auth_type' => $oldUser->auth_type,
                    ]);

                    DB::table('schools_users')->updateOrInsert(
                        [
                            'school_id' => $newSchool->id,
                            'user_id' => $user->id,
                        ],
                        [
                            'role_id' => $newRole->id,
                        ]);
                } catch (\Exception $exception) {
                    $userDuplicated = User::where('email', $oldUser->email)->exists();
                    $tmp = tmp::create([
                        'old_db_user_id' => $oldUser->user_id,
                        'email' => $oldUser->email,
                        'exception' => $exception->getMessage()
                    ]);
                    return true;
                }
                $bar->advance();
            });
        });
        $this->info("\n" . 'Users imported successfully.');
    }

    protected function importRoomTypes(): void
    {
        $oldRoomTypes = DB::connection('oldDB')->table('room_type');

        $this->info("\n" . 'Importing room types');
        $bar = $this->output->createProgressBar($oldRoomTypes->count());
        $bar->start();

        $oldRoomTypes->orderBy('type_id')->chunk(200, function ($roomTypesChunk) use ($bar) {
            $roomTypesChunk->each(function ($oldRoomType) use ($bar) {

                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldRoomType->school_id)->first();
                if (!$oldSchool) {
                    return true;
                }
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();

                if (!$newSchool) {
                    return true;
                }

                $newRoomType = RoomType::where('name', $oldRoomType->type_name)->where('school_id', $newSchool->id)->first();
                if ($newRoomType) {
                    return true;
                }

                RoomType::create([
                    'name' => $oldRoomType->type_name,
                    'school_id' => $newSchool->id,
                    'comment_type' => $oldRoomType->comment_type,
                    'description' => $oldRoomType->discription,
                    'status' => $oldRoomType->status,
                    'ordering' => $oldRoomType->ordering,
                    'receiver' => $oldRoomType->receiver,
                ]);

                $bar->advance();
            });
        });

        $this->info("\n" . 'Room types imported successfully.');

    }

    protected function importRooms(): void
    {
        $oldRooms = DB::connection('oldDB')->table('rooms');

        $this->info("\n" . 'Importing rooms');
        $bar = $this->output->createProgressBar($oldRooms->count());
        $bar->start();

        $oldRooms->orderBy('room_id')->chunk(200, function ($roomsChunk) use ($bar) {
            $roomsChunk->each(function ($oldRoom) use ($bar) {
                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldRoom->school_id)->first();
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();

                $oldRoomType = DB::connection('oldDB')->table('room_type')->where('type_id', $oldRoom->type_id)->first();
                $newRoomType = RoomType::where([
                    ['name', '=', $oldRoomType->type_name],
                    ['school_id', '=', $newSchool->id],
                ])->first();

                if (!$newSchool || !$newRoomType) {
                    return;
                }
                $newRoom = Room::where('name', $oldRoom->room_name)->where('type_id', $newRoomType->id)->where('school_id', $newSchool->id)->first();
                if ($newRoom) {
                    return true;
                }
                Room::create([
                    'name' => $oldRoom->room_name,
                    'type_id' => $newRoomType->id,
                    'school_id' => $newSchool->id,
                    'two_way' => $oldRoom->two_way,
                    'status' => $oldRoom->status,
                    'comment_type' => $oldRoom->comment_type,
                    'icon' => $oldRoom->icon ?? null,
                    'favorites' => $oldRoom->favorites,
                    'order' => $oldRoom->order,
                ]);
            });
        });
    }

    protected function importKiosk(): void
    {
        $oldKiosks = DB::connection('oldDB')->table('kiosk');

        $this->info("\n" . 'Importing kiosk');
        $bar = $this->output->createProgressBar($oldKiosks->count());
        $bar->start();

        $oldKiosks->orderBy('id')->chunk(200, function ($kiosksChunk) use ($bar) {
            $kiosksChunk->each(function ($oldKiosk) use ($bar) {

                $oldUser = DB::connection('oldDB')->table('users')->where('user_id', $oldKiosk->teacher_id)->first();
                if (!$oldUser) {
                    return true;
                }
                $newUser = User::where('email', $oldUser->email)->first();
                if (!$newUser) {
                    return true;
                }

                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldKiosk->school_id)->first();
                if (!$oldSchool) {
                    return true;
                }
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();
                if (!$newSchool) {
                    return true;
                }

                $oldRoom = DB::connection('oldDB')->table('rooms')->where('room_id', $oldKiosk->room_id)->first();
                if (!$oldRoom) {
                    return true;
                }
                if (isset($oldRoom->type_id) && !is_null($oldRoom->type_id)) {
                    $oldRoomType = DB::connection('oldDB')->table('room_type')->where('type_id', $oldRoom->type_id)->first();
                    if (!$oldRoomType) {
                        return true;
                    }
                    $newRoomType = RoomType::where([
                        ['name', '=', $oldRoomType->type_name],
                        ['school_id', '=', $newSchool->id],
                    ])->first();
                    if (!$newRoomType) {
                        return true;
                    }
                    $newRoom = Room::where('name', $oldRoom->room_name)->where('type_id', $newRoomType->id)->where('school_id', $newSchool->id)->first();
                } else {
                    $newRoom = Room::where('name', $oldRoom->room_name)->where('school_id', $newSchool->id)->first();

                }
                if (!$newRoom) {
                    return true;
                }

                $newKiosk = Kiosk::where('room_id', $newRoom->id)->where('school_id', $newSchool->id)->first();
                if ($newKiosk) {
                    return true;
                }

                Kiosk::create([
                    'room_id' => $newRoom->id,
                    'user_id' => $newUser->id,
                    'school_id' => $newSchool->id,
                    'status' => $oldKiosk->status,
                    'password_use' => $oldKiosk->password_use,
                    'password' => $oldKiosk->password,
                    'expired_date' => $oldKiosk->expired_date,
                ]);
            });
        });

        $this->info("\n" . 'Kiosks imported successfully.');

    }

    protected function importInviteUsers(): void
    {
        $oldInviteUsers = DB::connection('oldDB')->table('invite_users');

        $this->info("\n" . 'Importing invited users');
        $bar = $this->output->createProgressBar($oldInviteUsers->count());
        $bar->start();

        $oldInviteUsers->orderBy('id')->chunk(200, function ($inviteUsersChunk) use ($bar) {
            $inviteUsersChunk->each(function ($oldInviteUser) use ($bar) {

                $oldUser = DB::connection('oldDB')->table('users')->where('user_id', $oldInviteUser->user_id)->first();
                $newUser = User::where('email', $oldUser->email)->first();

                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldInviteUser->school_id)->first();
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();

                $oldRole = DB::connection('oldDB')->table('user_roles')->where('role_id', $oldInviteUser->role_id)->first();
                $newRole = Role::where('name', $oldRole->role_name)->first();

                if (!$newUser || $newSchool || $newRole) {
                    return;
                }

                InviteUser::create([
                    'user_id' => $newUser->id,
                    'school_id' => $newSchool->id,
                    'role_id' => $newRole->id,
                    'status' => $oldInviteUser->status,
                    'login' => $oldInviteUser->login,
                    'invite' => $oldInviteUser->invite,
                    'archive' => $oldInviteUser->archive,
                    'invite_error' => $oldInviteUser->inviteError,
                ]);

                $bar->advance();
            });
        });

        $this->info("\n" . 'Invited users imported successfully.');

    }

    protected function importStaffSchedule(): void
    {
        $oldStaffSchedule = DB::connection('oldDB')->table('staff_schedule');

        $this->info("\n" . 'Importing staff schedule');
        $bar = $this->output->createProgressBar($oldStaffSchedule->count());
        $bar->start();

        $oldStaffSchedule->orderBy('id')->chunk(200, function ($staffScheduleChunk) use ($bar) {
            $staffScheduleChunk->each(function ($oldStaffSchedule) use ($bar) {

                $oldUser = DB::connection('oldDB')->table('users')->where('user_id', $oldInviteUser->user_id)->first();
                $newUser = User::where('email', $oldUser->email)->first();

                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldInviteUser->school_id)->first();
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();

                $oldRoom = DB::connection('oldDB')->table('rooms')->where('room_id', $oldStaffSchedule->room_id)->first();
                $newRoom = Room::where('name', $oldRoom->room_name)->first();

                if (!$newUser || !$newSchool || !$newRoom) {
                    return;
                }

                StaffSchedule::create([
                    'room_id' => $newRoom->id,
                    'user_id' => $newUser->id,
                    'school_id' => $newSchool->id,
                    'status' => $oldStaffSchedule->status,
                ]);

                $bar->advance();
            });
        });

        $this->info("\n" . 'Staff schedule imported successfully.');
    }

    protected function importKioskUsers(): void
    {
        $oldKioskUsers = DB::connection('oldDB')->table('kiosk_users');

        $this->info("\n" . 'Importing kiosk users');
        $bar = $this->output->createProgressBar($oldKioskUsers->count());
        $bar->start();


        $oldKioskUsers->orderBy('id')->chunk(200, function ($kioskUsersChunk) use ($bar) {
            $kioskUsersChunk->each(function ($oldKioskUser) use ($bar) {

                $oldUser = DB::connection('oldDB')->table('users')->where('user_id', $oldKioskUser->user_id)->first();
                $newUser = User::where('email', $oldUser->email)->first();

                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldKioskUser->school_id)->first();
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();

                if (!$newUser || !$newSchool) {
                    return;
                }

                KioskUser::create([
                    'user_id' => $newUser->id,
                    'kpassword' => $oldKioskUser->kpassword,
                    'password_processed' => $oldKioskUser->password_processed,
                    'school_id' => $newSchool->id,
                ]);
            });
        });
    }

    protected function importStudentFavorite(): void
    {
        $oldStudentFavorite = DB::connection('oldDB')->table('student_favorite');

        $this->info("\n" . 'Importing student favorite');
        $bar = $this->output->createProgressBar($oldStudentFavorite->count());
        $bar->start();

        $oldStudentFavorite->orderBy('id')->chunk(200, function ($studentFavoriteChunk) use ($bar) {
            $studentFavoriteChunk->each(function ($oldStudentFavorite) use ($bar) {

                $oldStudentUser = DB::connection('oldDB')->table('users')->where('user_id', $oldStudentFavorite->student_id)->first();
                $newStudentUser = User::where('email', $oldStudentUser->email)->first();

                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldStudentFavorite->school_id)->first();
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();

                if (!$newStudentUser || !$newSchool) {
                    return;
                }

                StudentFavorite::create([
                    'position' => $newStudentUser->possition,
                    'favorable_type' => '',
                    'favorable_id' => '',
                    'user_id' => $newStudentUser->id,
                    'school_id' => $newSchool->id,
                    'order' => $newSchool->order,
                ]);
            });
        });

        $this->info("\n" . 'Student favorites imported successfully.');
    }

    protected function importPasses(): void
    {
        $oldEpassDetails = DB::connection('oldDB')->table('epass_detail');

        $this->info("\n" . 'Importing passes');
        $bar = $this->output->createProgressBar($oldEpassDetails->count());
        $bar->start();

        $oldEpassDetails->orderBy('pass_id')->chunk(200, function ($epassDetailsChunk) use ($bar) {
            $epassDetailsChunk->each(function ($oldEpassDetail) use ($bar) {

                $oldStudentUser = DB::connection('oldDB')->table('users')->where('user_id', $oldEpassDetail->student_id)->first();
                if (!$oldStudentUser) {
                    return true;
                }
                $newStudentUser = User::where('email', $oldStudentUser->email)->first();

                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldEpassDetail->school_id)->first();
                if (!$oldSchool) {
                    return true;
                }
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();

                if (!$newSchool || !$newStudentUser) {
                    return;
                }

                if ($oldEpassDetail->fromteacher_id) {
                    $oldFrom = DB::connection('oldDB')->table('users')->where('user_id', $oldEpassDetail->fromteacher_id)->first();
                    if (!$oldFrom) {
                        return true;
                    }
                    $newFrom = User::where('email', $oldFrom->email)->first();
                    if (!$newFrom) {
                        return true;
                    }
                    $fromType = User::class;
                } else if ($oldEpassDetail->from_room_id) {
                    $oldFrom = DB::connection('oldDB')->table('rooms')->where('room_id', $oldEpassDetail->from_room_id)->first();
                    if (!$oldFrom) {
                        return true;
                    }
                    $newFrom = Room::where('name', $oldFrom->room_name)->where('school_id', $newSchool->id)->first();
                    if (!$newFrom) {
                        return true;
                    }
                    $fromType = Room::class;
                }

                if ($oldEpassDetail->toteacher_id) {
                    $oldTo = DB::connection('oldDB')->table('users')->where('user_id', $oldEpassDetail->toteacher_id)->first();
                    if (!$oldTo) {
                        return true;
                    }
                    $newTo = User::where('email', $oldTo->email)->first();
                    if (!$newTo) {
                        return true;
                    }
                    $toType = User::class;
                } else if ($oldEpassDetail->toroom_id) {
                    $oldTo = DB::connection('oldDB')->table('rooms')->where('room_id', $oldEpassDetail->toroom_id)->first();
                    if (!$oldTo) {
                        return true;
                    }
                    $newTo = Room::where('name', $oldTo->room_name)->where('school_id', $newSchool->id)->first();
                    if (!$newTo) {
                        return true;
                    }
                    $toType = Room::class;
                }

                $outByTeacher = DB::connection('oldDB')->table('users')->where('user_id', $oldEpassDetail->out_by_teacher)->first();
                $approvedBy = User::where('email', optional($outByTeacher)->email)->first();

                $inByTeacher = DB::connection('oldDB')->table('users')->where('user_id', $oldEpassDetail->in_by_teacher)->first();
                $completedBy = User::where('email', optional($inByTeacher)->email)->first();

                $createdByTeacher = DB::connection('oldDB')->table('users')->where('user_id', $oldEpassDetail->created_by)->first();
                $createdBy = User::where('email', optional($createdByTeacher)->email)->first();

                if ($oldEpassDetail->passtype == 3) {
                    $type = Pass::KIOSK_PASS;
                } elseif ($oldEpassDetail->passtype == 1) {
                    $type = Pass::APPOINTMENT_PASS;
                } elseif ($newStudentUser->role_id == config('roles.student')) {
                    $type = Pass::STUDENT_CREATED_PASS;
                } else {
                    $type = Pass::PROXY_PASS;
                }

                Pass::create([
                    'user_id' => $newStudentUser->id,
                    'from_id' => $newFrom->id,
                    'from_type' => $fromType,
                    'to_id' => isset($newTo) ? $newTo->id : '1',
                    'to_type' => isset($toType) ? $toType : User::class,
                    'requested_at' => $oldEpassDetail->created_at,
                    'approved_at' => $oldEpassDetail->outdate,
                    'completed_at' => $oldEpassDetail->indate,
                    'from_teacher_note' => $oldEpassDetail->fromteacher_note,
                    'parent_id' => 0,
                    'school_id' => $newSchool->id,
                    'ordering' => $oldEpassDetail->ordering,
                    'to_teacher_note' => $oldEpassDetail->toteacher_note,
                    'approved_by' => optional($approvedBy)->id,
                    //'inaction' => $oldEpassDetail->inaction,
                    'completed_by' => optional($completedBy)->id,
                    'pass_status' => $oldEpassDetail->pass_status,
                    'edited_at' => $oldEpassDetail->edit_status ? Carbon::now() : null,
                    'type' => $type,
                    'approved_with' => Pass::BY_CHECKIN,
                    'ended_with' => Pass::BY_CHECKIN,
                    'created_by' => optional($createdBy)->id,
                ]);
                $bar->advance();
            });
        });
    }

    protected function importNotifications(): void
    {
        $oldNotifications = DB::connection('oldDB')->table('student_favorite');

        $this->info("\n" . 'Importing notifications');
        $bar = $this->output->createProgressBar($oldNotifications->count());
        $bar->start();

        $oldNotifications->orderBy('id')->chunk(200, function ($notificationsChunk) use ($bar) {
            $notificationsChunk->each(function ($oldNotification) use ($bar) {

                $oldStudentUser = DB::connection('oldDB')->table('users')->where('user_id', $oldNotification->student_id)->first();
                $newStudentUser = User::where('email', $oldStudentUser->email)->first();


                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldNotification->school_id)->first();
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();

                if (!$newStudentUser || !$newSchool) {
                    return;
                }

                Notification::create([
                    'user_id' => $newStudentUser->id,
                    'pass_id' => $oldNotification->pass_id,
                    'message' => $oldNotification->message,
                    'status' => $oldNotification->status,
                    'alert' => $oldNotification->alert,
                    'to_teacher_id' => $oldNotification->toteacher_id,
                    'from_teacher_id' => $oldNotification->fromteacher_id,
                    'school_id' => $newSchool->id,
                    'to_teacher_state' => $oldNotification->toteacherstate,
                    'from_teacher_state' => $oldNotification->fromteacherstate,
                    'token' => $oldNotification->token,
                    'bulk_status' => $oldNotification->bulk_status,
                    'sms' => $oldNotification->sms,
                    'email' => $oldNotification->email,
                ]);

                $bar->advance();
            });
        });

        $this->info("\n" . 'Notifications imported successfully.');

    }

    public function importPassSummaryReports(): void
    {
        $oldPassSummaryReports = DB::connection('oldDB')->table('pass_summary_reports');

        $this->info("\n" . 'Importing pass summary reports');
        $bar = $this->output->createProgressBar($oldPassSummaryReports->count());
        $bar->start();

        $oldPassSummaryReports->orderBy('id')->chunk(200, function ($passSummaryReportsChunk) use ($bar) {
            $passSummaryReportsChunk->each(function ($oldPassSummaryReport) use ($bar) {

                $oldStudentUser = DB::connection('oldDB')->table('users')->where('user_id', $oldPassSummaryReport->student_id)->first();
                $newStudentUser = User::where('email', $oldStudentUser->email)->first();


                $oldSchool = DB::connection('oldDB')->table('school_schedule')->where('school_id', $oldPassSummaryReport->school_id)->first();
                $newSchool = School::where('old_id', $oldSchool->school_id)->first();

                if (!$newStudentUser || !$newSchool) {
                    return;
                }

                PassSummaryReport::create([
                    'school_id' => $newSchool->id,
                    'user_id' => $newStudentUser->id,
                    'parent' => $oldPassSummaryReport->parent,
                    'pass_code' => $oldPassSummaryReport->pass_code,
                    'preapproved' => $oldPassSummaryReport->preapproved,
                    'pass_id' => $oldPassSummaryReport->pass_id,
                    'outdate' => $oldPassSummaryReport->outdate,
                    'indate' => $oldPassSummaryReport->indate,
                    'from_teacher_id' => $oldPassSummaryReport->fromteacher_id,
                    'to_teacher_id' => $oldPassSummaryReport->toteacher_id,
                    'from_room_id' => $oldPassSummaryReport->fromroom_id,
                    'to_room_id' => $oldPassSummaryReport->toroom_id,
                    'outaction' => $oldPassSummaryReport->outaction,
                    'inaction' => $oldPassSummaryReport->inaction,
                    'out_by_teacher' => $oldPassSummaryReport->out_by_teacher,
                    'in_by_teacher' => $oldPassSummaryReport->in_by_teacher,
                    'out_role_id' => $oldPassSummaryReport->outroleid,
                    'in_role_id' => $oldPassSummaryReport->inroleid,
                    'pass_status' => $oldPassSummaryReport->pass_status,
                    'period' => $oldPassSummaryReport->period,
                    'created_by' => $oldPassSummaryReport->created_by,
                    'from_teacher_id2' => $oldPassSummaryReport->fromteacher_id2,
                    'to_teacher_id2' => $oldPassSummaryReport->toteacher_id2,
                    'from_room_id2' => $oldPassSummaryReport->fromroom_id2,
                    'to_room_id2' => $oldPassSummaryReport->toroom_id2,
                    'outaction2' => $oldPassSummaryReport->outaction2,
                    'inaction2' => $oldPassSummaryReport->inaction2,
                    'out_by_teacher2' => $oldPassSummaryReport->out_by_teacher2,
                    'in_by_teacher2' => $oldPassSummaryReport->in_by_teacher2,
                    'totalTime' => $oldPassSummaryReport->totalTime,
                ]);
            });
        });
    }

}
