<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveV1Cols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->dropColumn("parent");
            $table->dropColumn("stop_status");
            $table->dropColumn("time_format");
            $table->dropColumn("webservice");
            $table->dropColumn("avatar_api");
            $table->dropColumn("api_key");
            $table->dropColumn("total_user_role");
            $table->dropColumn("total_users");
            $table->dropColumn("upload_folder");
            $table->dropColumn("archive_prefix");
            $table->dropColumn("csv_headers");
            $table->dropColumn("csv_roles");
            $table->dropColumn("if_username");
            $table->dropColumn("is_user_csv");
            $table->dropColumn("is_sftp_image");
            $table->dropColumn("is_manual");
        });


        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("metatag");
            $table->dropColumn("clever_id");
            $table->dropColumn("clever_building_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->integer('parent');
            $table->boolean('stop_status');
            $table->tinyInteger('time_format');
            $table->string('webservice');
            $table->string('avatar_api');
            $table->string('api_key', 50);
            $table->text('total_user_role');
            $table->integer('total_users');
            $table->string('upload_folder', 25);
            $table->string('archive_prefix', 10);
            $table->text('csv_headers');
            $table->text('csv_roles');
            $table->tinyInteger('if_username')->default(0);
            $table->tinyInteger('is_user_csv')->default(0);
            $table->integer('is_sftp_image')->default(0);
            $table->smallInteger('is_manual')->default(0);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->text('metatag')->nullable();
            $table->string('clever_id', 100)->nullable()->index('clever_id');
            $table->string('clever_building_id', 100)->nullable()->comment('Clever School ID');
        });
    }
}
