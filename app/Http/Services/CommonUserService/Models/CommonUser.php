<?php

namespace App\Http\Services\CommonUserService\Models;


use Illuminate\Database\Eloquent\Model;

class CommonUser extends Model
{
    protected $connection = 'commonDb';
    protected $table = 'auth_user';

    public static function getRoleName($roleId)
    {
        $roles = [
            2 => 'admin',
            3 => 'teacher',
//            3 => 'teacher_substitute',
            4 => 'staff',
            1 => 'student',
            5 => 'admin'
        ];
        if (isset($roles[$roleId])) {
            return [$roles[$roleId]];
        }
        return [];
    }
}
