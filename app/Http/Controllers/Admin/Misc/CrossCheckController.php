<?php

namespace App\Http\Controllers\Admin\Misc;

use App\Http\Controllers\Controller;
use App\Http\Services\CommonUserService\CommonUserService;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\V1\User;
use DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class CrossCheckController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $verifiedSchoolIds = \Illuminate\Support\Facades\DB::table(
            'verified_schools'
        )
//            ->where('status', 1)
            ->get()
            ->pluck('school_id');
        $schools = School::orderBy('name', 'asc')
            ->when(request()->has('search'), function ($query) {
                $query->where(
                    'name',
                    'LIKE',
                    '%' . request()->input('search') . '%'
                );
            })
            ->whereNotIn('id', $verifiedSchoolIds)
            ->paginate(50);

        return view('misc.indexCrossCheck', ['schools' => $schools]);
    }

    /**
     * @param $schoolName
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(School $school)
    {
        $diffsV1 = [];
        $diffsV2 = [];
        $diffsCus = [];
        if ($school->old_id) {
            $v2Emails = DB::table('schools_users as m')
                ->join('users as u', 'm.user_id', '=', 'u.id')
                ->select('u.old_id')
                ->where('m.school_id', $school->id)
                ->where('u.status', 1)
                ->where(function ($query) {
                    $query
                        ->where('u.auth_type', '!=', 1001)
                        ->orWhereNull('u.auth_type');
                })
                ->whereNull('m.deleted_at')
                ->where('m.status', 1)
                ->whereNull('u.deleted_at')
                ->whereNotNull('u.email')
                ->get()
                ->pluck('old_id');

            $diffsV1 = DB::connection('oldDB')
                ->table('ehallpassDB.users as oldu')
                ->select([
                    'oldu.user_id',
                    'oldu.email',
                    'oldu.status',
                    'oldu.clever_id',
                    'oldu.role_id',
                    'oldu.student_sis_id',
                    'oldu.user_id',
                    'oldu.auth_type',
                    'oldu.created_at'
                ])
                ->where('status', 1)
                ->where('role_id', '!=', 0)
                ->where('role_id', '!=', 7)
                ->where('school_id', $school->old_id)
                ->whereNotIn('user_id', $v2Emails)
                ->get();

            //                ->whereRaw(
            //                    "
            //	 oldu.email NOT IN(
            //		SELECT
            //			u.email FROM ehp_v2_prod.schools_users as m
            //					join ehp_v2_prod.users as u on m.user_id = u.id
            //		WHERE
            //			m.school_id = $school->id
            //			AND u.status = 1
            //			AND (u.auth_type != 1001 or u.auth_type is null)
            //			AND u.deleted_at is null) AND oldu.school_id = $school->old_id AND oldu.status = 1"
            //                )

            $v2role = request()->get('role', null);

            $roles = [
                1 => 4,
                2 => 2,
                3 => 3,
                4 => 5,
            ];

            $v1Role = $roles[$v2role] ?? null;
//            dd($v2role,$v1Role);
            $v1Emails = DB::connection('oldDB')
                ->table('users')
                ->select('user_id')
                ->where('school_id', $school->old_id)
                ->where('status', 1)
                ->when($v1Role, function ($q) use ($v1Role) {
                    $q->where('role_id', $v1Role);
                })
                ->whereNotNull('email')
                ->get()
                ->pluck('email');

            $diffsV2 = DB::table('schools_users as m')
                ->join('users as u', 'm.user_id', '=', 'u.id')
                ->select([
                    'u.email',
                    'u.status',
                    'u.student_sis_id',
                    'u.cus_uuid',
                    'u.deleted_at',
                    'm.role_id',
                    'u.old_id',
                    'u.id',
                    'u.auth_type',
                    'u.created_at'
                ])
                ->where('m.school_id', $school->id)
                ->where('u.status', 1)
                ->where('m.status', 1)
                ->where('u.is_substitute', 0)
                ->when($v2role, function ($q) use ($v2role) {
                    $q->where('m.role_id', $v2role);
                })
                ->where(function ($query) {
                    $query
                        ->where('u.auth_type', '!=', 1001)
                        ->orWhereNull('u.auth_type');
                })
                ->whereNull('u.deleted_at')
                ->whereNull('m.deleted_at')
                ->where(function ($query) use ($v1Emails) {
                    $query->whereNull('m.old_id')
                        ->orWhereNotIn('m.old_id', $v1Emails);
                })
                ->get();

            $v2CommonIds = DB::table('schools_users as m')
                ->join('users as u', 'm.user_id', '=', 'u.id')
                ->select('u.common_user_id')
                ->where('m.school_id', $school->id)
                ->where('u.status', 1)
                ->where(function ($query) {
                    $query
                        ->where('u.auth_type', '!=', 1001)
                        ->orWhereNull('u.auth_type');
                })
                ->whereNull('m.deleted_at')
                ->where('m.status', 1)
                ->whereNull('u.deleted_at')
                ->whereNotNull('u.email')
                ->get()
                ->pluck('common_user_id');

            $diffsCus = DB::connection('commonDb')
                ->table('common_bm_membership as m')
                ->join('auth_user as u', 'm.auth_user_id', '=', 'u.id')
                ->select([
                    'u.email',
                    'm.not_archived as status',
                    'm.sis_id as student_sis_id',
                    'm.uuid as cus_uuid',
                    'm.archived_at as deleted_at',
                    'm.common_bm_role_membership_id as role_id',
                    'u.created as created_at',
                    'u.id',
                ])
                ->where('m.common_bm_school_id', $school->common_id)
                ->where('m.not_archived', 1)
                ->where(function ($query) use ($v2CommonIds) {
                    $query->whereNotIn('u.id', $v2CommonIds);
                })
                ->get();
        }


        return view('misc.crossCheck', [
            'schoolName' => $school->name,
            'domains' => $school->domains,
            'schoolId' => $school->id,
            'schoolOldId' => $school->old_id,
            'state' => $school->state,
            'zip' => $school->zip,
            'logo' => $school->logo,
            'schoolCus' => $school->cus_uuid,
            'schoolStatus' => $school->status,
            'v2Counters' => $this->v2Counters($school),
            'v1Counters' => $this->v1Counters($school),
            'cusCounters' => $this->cusCounters($school),
            'adminDiffs' => $diffsV1,
            'adminDiffsV2' => $diffsV2,
            'cusDiffs' => $diffsCus
        ]);
    }

    protected function v1Counters($school)
    {
        $counter = DB::connection('oldDB')->select(
            'SELECT
	count(
		CASE WHEN role_id = 2 THEN
			1
		END) AS admin,
	count(
		CASE WHEN role_id = 3 THEN
			1
		END) AS teacher,
	count(
		CASE WHEN role_id = 4 THEN
			1
		END) AS student,
	count(
		CASE WHEN role_id = 5 THEN
			1
		END) AS staff
		FROM
	ehallpassDB.users
WHERE
	school_id = ?
	AND status = 1
LIMIT 1;',
            [$school->old_id]
        );

        return collect($counter)->first();
    }

    protected function v2Counters($school)
    {
        $counter = DB::select(
            "select
	  count(CASE m.role_id
      WHEN 1 THEN 'student' END ) as student,
      count(CASE m.role_id WHEN 2 THEN 'admin' END) as admin,
      count(case m.role_id WHEN 3 THEN 'teacher' END) as teacher,
      count(case m.role_id WHEN 4 THEN 'staff' END) as staff
     from schools_users m  join schools s on m.school_id = s.id join users u on m.user_id = u.id
 where s.id = ?
 and u.status = 1
 and m.status = 1
   and u.is_substitute = 0
and u.archived_at is null
and m.archived_at is null
 and (u.auth_type != 1001 or u.auth_type is null)

and m.deleted_at is null
 and u.deleted_at is null and not (u.username <=> 'system') and (u.first_name != 'System Ended') limit 1;
",
            [$school->id]
        );
        return collect($counter)->first();
    }

    protected function cusCounters($school)
    {
        $counter = DB::connection('commonDb')->select(
            "select
	  count(CASE m.common_bm_role_membership_id
      WHEN 5 THEN 'student' END ) as student,
      count(CASE m.common_bm_role_membership_id WHEN 1 THEN 'admin' END) as admin,
      count(case m.common_bm_role_membership_id WHEN 2 THEN 'teacher' END) as teacher,
      count(case m.common_bm_role_membership_id WHEN 4 THEN 'staff' END) as staff

from (common_bm_membership m join common_bm_school s on m.common_bm_school_id = s.id) join auth_user u on m.auth_user_id = u.id
where m.auth_client_id = 2
and s.uuid = ?
and m.not_archived = 1
and u.not_archived =1
and u.first_name != 'System Ended'
and not (u.username <=> 'system')
and not (u.username <=> 'simulate')  limit 1;
",
            [$school->cus_uuid]
        );
        return collect($counter)->first();
    }

    public function syncwithv1(School $school, \App\Models\User $user)
    {
        $membership = \Illuminate\Support\Facades\DB::table('schools_users')
            ->where('school_id', $school->id)
            ->where('user_id', $user->id)
            ->orderBy('id', 'desc')
            ->first();

        if (!$membership->old_id) {

            $oldUser = User::where('email', $user->email)->where('school_id', $school->old_id)->first();

            if ($oldUser) {
                \Illuminate\Support\Facades\DB::table('schools_users')
                    ->where('school_id', $school->id)
                    ->where('user_id', $user->id)
                    ->update([
                        'old_id' => $oldUser->user_id
                    ]);
            }


        }

        return redirect()
            ->back()
            ->with('message', ['old_id' => $oldUser->user_id]);

    }

    public function migrate(School $school, $oldId)
    {
        $oldUser = User::withoutGlobalScopes()
            ->where('user_id', $oldId)
            ->first();

        return redirect()
            ->back()
            ->with('message', $oldUser->sync($school));
    }

    public function migrateAll(School $school)
    {
        $v2Emails = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select('u.email')
            ->where('m.school_id', $school->id)
            ->where('u.status', 1)
            ->where('m.status', 1)
            ->where(function ($query) {
                $query
                    ->where('u.auth_type', '!=', 1001)
                    ->orWhereNull('u.auth_type');
            })
            ->whereNull('m.deleted_at')
            ->whereNull('u.deleted_at')
            ->whereNotNull('u.email')
            ->get()
            ->pluck('email');


        $oldUsers = User::where('status', 1)
            ->where('role_id', '!=', 0)
            ->where('role_id', '!=', 7)
            ->where('school_id', $school->old_id)
            ->whereNotIn('email', $v2Emails)
            ->get();


        foreach ($oldUsers as $oldUser) {
            $oldUser->sync();
        }

        return redirect()
            ->back()
            ->with('message', ["migrated" => $oldUsers->count()]);
    }

    public function verify(School $school, $status)
    {
        \Illuminate\Support\Facades\DB::table(
            'verified_schools'
        )->updateOrInsert(
            [
                'school_id' => $school->id
            ],
            [
                'status' => $status
            ]
        );
        return redirect()->route('api.v5.crosscheck.show', $school->id + 1);
    }

    public function deleteAll(School $school, $ids)
    {

        foreach (json_decode($ids) as $id) {

            $user = \App\Models\User::withoutGlobalScopes()
                ->where('id', $id)->first();
            if ($user) {
                $this->deleteSingle($school, $user);
            }
        }
        return redirect()
            ->back()
            ->with('message', ["migrated" => $ids]);
    }

    public function delete(School $school, \App\Models\User $user)
    {
        $this->deleteSingle($school, $user);
        return redirect()
            ->back()
            ->with('message', ["deleted" => $user->email]);
    }

    protected function deleteSingle(School $school, \App\Models\User $user)
    {
        $membership = \Illuminate\Support\Facades\DB::table('schools_users')
            ->where('user_id', $user->id)
            ->where('school_id', $school->id)
            ->whereNull('deleted_at')
            ->first();

        if ($membership) {
            $adminUser = \App\Models\User::where('email', 'viktor.lalev@donatix.net')
                ->latest()
                ->first();
            $client = new CommonUserService($adminUser);


            $response = $client->deleteUser($membership->membership_uuid);

            \Illuminate\Support\Facades\DB::table('schools_users')
                ->where('user_id', $user->id)
                ->where('school_id', $school->id)
                ->update([
                    'deleted_at' => now(),
                    'status' => 0
                ]);

            $user->searchable();
        }

        return true;
    }

    public function deleteAllCus(School $school, $memberships)
    {
        foreach (json_decode($memberships) as $membership) {

            $this->deleteSingleMembership($membership);

        }
        return redirect()
            ->back()
            ->with('message', ["deleted" => $memberships]);
    }

    protected function deleteSingleMembership($membership)
    {
        $adminUser = \App\Models\User::where('email', 'viktor.lalev@donatix.net')
            ->latest()
            ->first();
        $client = new CommonUserService($adminUser);


        return $client->deleteUser($membership);
    }

    public function deleteMembership(School $school, $membership)
    {

        $this->deleteSingleMembership($membership);
        return redirect()
            ->back()
            ->with('message', ["deleted" => $membership]);
    }

    public function sync(School $school, $memberships)
    {
        $uuids = json_decode($memberships);
        if (!$uuids) {
            $uuids = [$memberships];
        }

        foreach ($uuids as $uuid) {
            Artisan::call("cus:sync --entity=users --force --membershipUuid={$uuid}");
        }
        return redirect()
            ->back()
            ->with('message', ["synced" => $memberships]);
    }

    public function schools()
    {
        $verifiedSchoolIds = \Illuminate\Support\Facades\DB::table(
            'verified_schools_info'
        )
//            ->where('status', 1)
            ->get()
            ->pluck('school_id');
        $schools = \App\Models\V1\School::orderBy('school_name', 'asc')
            ->when(request()->has('search'), function ($query) {
                $query->where(
                    'school_name',
                    'LIKE',
                    '%' . request()->input('search') . '%'
                );
            })
            ->where('status', 1)
//            ->whereNotNull('old_id')
            ->whereNotIn('school_id', $verifiedSchoolIds)
            ->paginate(50);

        return view('misc.schools.index', ['schools' => $schools]);
    }

    public function schoolShow(\App\Models\V1\School $school)
    {
        $v2School = School::with('domains', 'modules')
            ->where('old_id', $school->school_id)
            ->first();
        $v2Modules = [];
        $v2Counters = [];
        $v1Counters = [];
        if ($v2School) {
            $v2Modules = SchoolModule::with('module')->where('school_id', $v2School->id)->get();
            $v2Counters = $this->v2Counters($v2School);
            $v1Counters = $this->v1Counters($v2School);
        }


        return view('misc.schools.show', [
            'school' => $v2School,
            'v2Modules' => $v2Modules,
            'oldSchool' => $school,
            'v2Counters' => $v2Counters,
            'v1Counters' => $v1Counters,
        ]);
    }

    public function verifySchool(\App\Models\V1\School $school, $status)
    {
        \Illuminate\Support\Facades\DB::table(
            'verified_schools_info'
        )->updateOrInsert(
            [
                'school_id' => $school->school_id
            ],
            [
                'status' => $status
            ]
        );
        return redirect()->route('api.v5.crosscheck.schools.index');
    }

    public function vs()
    {

        $verifiedSchoolIds = \Illuminate\Support\Facades\DB::table(
            'verified_schools_info'
        )
            ->get()
            ->pluck('school_id');
        $oldSchools = \App\Models\V1\School::where('status', 1)
            ->whereNotIn('school_id', $verifiedSchoolIds)
            ->get();

        foreach ($oldSchools as $oldSchool) {

            $school = School::where('old_id', $oldSchool->school_id)->first();

            if ($school) {

//                $this->migrateAll($school);

                $v2Counters = $this->v2Counters($school);
                $v1Counters = $this->v1Counters($school);

                if (
                    $v1Counters->admin == $v2Counters->admin &&
                    $v1Counters->staff == $v2Counters->staff &&
                    $v1Counters->teacher == $v2Counters->teacher &&
                    $v1Counters->student == $v2Counters->student
                ) {
                    \Illuminate\Support\Facades\DB::table(
                        'verified_schools_info'
                    )->updateOrInsert(
                        [
                            'school_id' => $oldSchool->school_id
                        ],
                        [
                            'status' => 1
                        ]
                    );
                }
            }


        }
    }

    public function migrateImage(\App\Models\V1\School $school)
    {
        $result = $school->migrateLogo();
        return redirect()
            ->back()
            ->with('message', ["migrated" => $result]);
    }

    public function migrateModule(\App\Models\V1\School $school)
    {
        $result = $school->migrateModules();

        return redirect()
            ->back()
            ->with('message', ["migrated" => $result]);
    }
}
