<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExcelDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excel_data', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('school_id');
            $table->integer('timestamp');
            $table->date('scan_date')->comment('IST time');
            $table->longText('excel_data');
            $table->string('header_data', 245);
            $table->integer('role');
            $table->boolean('is_conflict')->default(0);
            $table->boolean('status')->default(1);
            $table->integer('staffs');
            $table->integer('teachers');
            $table->integer('students');
            $table->integer('admins');
            $table->integer('newRecord');
            $table->integer('successRecord');
            $table->integer('updateRecord');
            $table->integer('number_of_error');
            $table->longText('newRecords');
            $table->longText('existRecords');
            $table->longText('existOld');
            $table->integer('correctRecords');
            $table->longText('archiveRecords');
            $table->longText('errorRecords');
            $table->longText('notUpdated');
            $table->mediumText('filterData');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('excel_data');
    }
}
