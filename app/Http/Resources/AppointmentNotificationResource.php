<?php

namespace App\Http\Resources;

use App\Models\School;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\AppointmentNotification */
class AppointmentNotificationResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $school = School::findCacheFirst(optional($this)->school_id);

        return [
            'id' => $this->id,
            'notification_type' => $this->notification_type,
            'seen_at' => School::convertTimeToCorrectTimezone($this->seen_at, $school->timezone),
            'created_at' => School::convertTimeToCorrectTimezone($this->created_at, $school->timezone),
            'updated_at' => School::convertTimeToCorrectTimezone($this->updated_at, $school->timezone),
            'user' => $this->user,
            'appointment_pass' => $this->appointmentPass,
            'updated_by_id' => $this->updated_by_id,
            'updated_by' => [
                'id' => optional($this->updatedBy)->id,
                'first_name' => optional($this->updatedBy)->first_name,
                'last_name' => optional($this->updatedBy)->last_name,
                'email' => optional($this->updatedBy)->email,
            ],
            'new_time' => School::convertTimeToCorrectTimezone($this->new_time, $school->timezone),
            'old_time' => School::convertTimeToCorrectTimezone($this->old_time, $school->timezone),
            'user_id' => $this->user_id,
            'appointment_pass_id' => $this->appointment_pass_id,
        ];
    }
}
