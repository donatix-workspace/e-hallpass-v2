#EHP V2

## Setting Up
1. Run standard flow
~~~
composer install
php artisan migrate
~~~
2. Generate API doc - `http://<domain>/docs/index.html`
~~~
php artisan route:clear
php artisan scribe:generate
~~~
3. Configure and index Elasticsearch
Add scount configurations
~~~
SCOUT_DRIVER=elastic
SCOUT_ELASTIC_HOST=localhost:9200
~~~
Run for all index configurators
~~~
php artisan elastic:create-index App\\Elastic\\PassesIndexConfigurator
php artisan scout:import App\\Models\\Pass
~~~
Or run `php artisan es:reindex`

#### API JSON STRUCTURE

```
{
    data: {},
    status: '',
    links: {},
    meta: {},
    errors: {},
    message: ''
}
```

`data: array(object) with records | always`

`status: string | always`

`links: object(array) with records | not always (only on pagination)`

`meta: array(object) | not always (only in paginations,chunks etc.)`

`errors: array(object) | not always (validation errors, system errors)`

`message: string | not always (Validation errors, info messages)`

#### Example json request
```
{

    "data": [
        {
            "id": 1,
            "first_user": {},
            "second_user": {},
            "status": 1,
            "message": "Repellat saepe quod rem et est quod quia. Sed nesciunt sit et sint non. Ut nihil omnis corrupti voluptatibus. Asperiores velit commodi earum perspiciatis.",
            "created_at": "2021-02-02T10:48:40.000000Z",
            "updated_at": "2021-02-02T10:48:40.000000Z"
        },
        {
            "id": 2,
            "first_user": {},
            "second_user": {},
            "status": 1,
            "message": "Hello",
            "created_at": "2021-02-02T10:48:41.000000Z",
            "updated_at": "2021-02-02T12:45:48.000000Z"
        }
    ],
    "status": "success",
    "links": {
        "first": "http://ehpv2.test/api/v5/polarities?page=1",
        "last": "http://ehpv2.test/api/v5/polarities?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://ehpv2.test/api/v5/polarities?page=1",
                "label": 1,
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "path": "http://ehpv2.test/api/v5/polarities",
        "per_page": 5,
        "to": 2,
        "total": 2
    }

}
```
