<?php

namespace App\Policies;

use App\Models\Module;
use App\Models\School;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ModulePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin() || $user->isSuperAdmin();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewActive(User $user)
    {
        return $user->isAdmin() || $user->isSuperAdmin();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user, Module $module, School $school)
    {
        return $user->isAdmin() && $user->school_id === $school->id;
    }

}
