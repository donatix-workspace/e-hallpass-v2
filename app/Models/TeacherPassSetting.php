<?php

namespace App\Models;

use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherPassSetting extends Model
{
    use HasFactory;

    protected $table = 'teacher_pass_settings';
    protected $hidden = ['id'];

    protected $guarded = [];

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * Convert minutes in format HH:mm:ss to seconds
     * Formula: hour*60*60 (One hour = 60 minutes, 60 minutes * 60) + remain seconds
     *
     * @param $time
     * @return float|int|null
     */
    public static function convertInSeconds($time)
    {
        if ($time === null) {
            return null;
        }

        $time = str_replace('-', '', $time);

        [$hours, $minutes, $seconds] = explode(':', $time);

        return abs($hours * 60 * 60 + $minutes * 60 + $seconds);
    }

    /**
     * @param $time
     * @return float|int
     */
    public static function convertInMinutes($time)
    {
        if ($time === null) {
            return null;
        }

        try {
            $parseTimeToCarbon = Carbon::parse($time);
        } catch (InvalidFormatException $e) {
            logger('Invalid time format given for nurse room: ' . $time);
            return 0;
        }

        return $parseTimeToCarbon->hour * 60 + $parseTimeToCarbon->minute;
    }

    public static function isForExpire($date, $minute)
    {
        // If the given date is null, then the pass itself is invalid for some reason
        // So we return true just to expire the pass and prevent future problems.
        if ($date === null) {
            return true;
        }

        $endDate = Carbon::parse($date->format('Y-m-d H:i'));
        $now = Carbon::parse(now()->format('Y-m-d H:i'));

        return $endDate->diffInMinutes($now) > $minute;
    }

    public static function createDefaultSettings($school)
    {
        //TODO Get default values
        return self::create([
            'school_id' => $school->id,
            'min_time' => '00:10:00',
            'awaiting_apt' => '00:05:00',
            'white_passes' => '00:05:00',
            'max_time' => '00:15:00',
            'auto_expire_time' => '00:30:00',
            'nurse_expire_time' => '01:30:00',
            'mail_time' => '00:15:00'
        ]);
    }
}
