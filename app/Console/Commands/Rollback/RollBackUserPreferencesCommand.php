<?php

namespace App\Console\Commands\Rollback;

use App\Models\AutoPass;
use App\Models\AutoPassPreference;
use DB;

use Illuminate\Console\Command;
use Throwable;

class RollBackUserPreferencesCommand extends Command
{
    protected $signature = 'ehpv1_rollback:user_preferences';

    protected $description = 'Rollback User Preferences from EhpV2 to EhpV1';

    /**
     * @return int
     */
    public function handle(): int
    {
        $newDb = DB::connection();
        $oldDb = DB::connection('oldDB');

        $autoPassPreferences = $newDb->table('auto_pass_preferences')
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();

        $this->output->progressStart(count($autoPassPreferences));

        $autoPassPreferencesChunks = array_chunk($autoPassPreferences, 1500);

        foreach ($autoPassPreferencesChunks as $autoPassPreferencesChunk) {
            $insertablePreferencesArray = [];

            foreach ($autoPassPreferencesChunk as $autoPass) {
                $newSchool = $newDb->table('schools')->where('id', $autoPass->school_id)
                    ->select('id', 'old_id')->first();


                if ($newSchool) {
                    $newUser = $newDb->table('users')->where('id', $autoPass->user_id)->select('old_id')->first();
                    if ($newUser) {
                        $autoPassRoomId = $newDb->table('auto_passes')->where('id', $autoPass->auto_pass_id)
                            ->select('room_id')->first();
                        if ($autoPassRoomId) {
                            $insertablePreferencesArray[] = [
                                'school_id' => $newSchool->old_id,
                                'teacher_id' => $newUser->old_id,
                                'room_id' => $autoPassRoomId->room_id,
                                'status' => $autoPass->status,
                                'mode' => $autoPass->mode === AutoPassPreference::MODE_START_AND_STOP ? 1 : 0,
                                'created_at' => now(),
                                'updated_at' => now()
                            ];
                        }
                    }

                }
            }

            try {
                DB::beginTransaction();
                $oldDb->table('auto_pass_preference')
                    ->insert($insertablePreferencesArray);
                $this->output->progressAdvance(count($insertablePreferencesArray));
                DB::commit();
            } catch (Throwable $e) {
                $this->info($e->getMessage());
            }
        }

        $this->output->progressFinish();

        return 0;
    }
}
