SET FOREIGN_KEY_CHECKS  = 0;
START TRANSACTION ;
truncate ehp_v2_prod.auto_passes_limits;
INSERT INTO ehp_v2_prod.auto_passes_limits (user_id, school_id, `limit`, created_at, updated_at)  SELECT
                        (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldAp.teacher_id limit 1),
                        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldAp.school_id limit 1),
                        (SELECT oldAp.`limit`),
                                                                                                         (SELECT NOW()),
                                                                                                         (SELECT NOW())
FROM ehallpassDB.auto_pass_limits as oldAp;
SET FOREIGN_KEY_CHECKS  = 1;
UPDATE
    ehp_v2_prod.auto_passes_limits
SET
    user_id = (
        SELECT
            to_id
        FROM
            duplicate_users
        WHERE
                from_id = user_id
        LIMIT 1)
WHERE
        user_id IN(
        SELECT
            from_id FROM duplicate_users);
