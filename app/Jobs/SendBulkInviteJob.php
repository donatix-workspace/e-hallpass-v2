<?php

namespace App\Jobs;

use App\Http\Services\CommonUserService\AuthenticationService;
use App\Mail\SendInvitationToUserMail;
use App\Models\InviteUser;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendBulkInviteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $users;
    public $user;

    /**
     * SendBulkInviteJob constructor.
     * @param $users
     * @param $user
     */
    public function __construct($users, $user)
    {
        $this->users = $users;
        $this->user = $user;
    }

    /**
     *
     */
    public function handle()
    {
        $client = new AuthenticationService();

        foreach ($this->users as $user) {
            
            $client->passwordResetInitiate($user->email);

            $user->update([
                'invited_at' => now(),
                'send_invite' => false
            ]);
        }
    }
}
