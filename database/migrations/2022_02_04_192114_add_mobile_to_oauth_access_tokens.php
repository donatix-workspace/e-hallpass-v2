<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMobileToOauthAccessTokens extends Migration
{
    public function up()
    {
        Schema::table('oauth_access_tokens', function (Blueprint $table) {
            $table
                ->boolean('mobile')
                ->nullable()
                ->default(false);
        });
    }

    public function down()
    {
        Schema::table('oauth_access_tokens', function (Blueprint $table) {
            $table->dropColumn('mobile');
        });
    }
}
