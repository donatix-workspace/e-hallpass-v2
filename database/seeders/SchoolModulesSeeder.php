<?php

namespace Database\Seeders;

use App\Models\Module;
use App\Models\School;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schools = School::all();
        $schools->each(function ($item) {
            DB::table('school_modules')->insert([
                [
                    'school_id' => $item->id,
                    'module_id' => Module::APPOINTMENTPASS_ID,
                    'option_json' => '{"admin_edit":0,"teacher":1,"staff":1,"location":1,"admin":1}',
                    'permission' => 1,
                    'status' => 1,
                ],
                [
                    'school_id' => $item->id,
                    'module_id' => Module::AUTO_CHECKIN_ID,
                    'option_json' => '[]',
                    'permission' => 1,
                    'status' => 1,
                ],
                [
                    'school_id' => $item->id,
                    'module_id' => Module::KIOSK_ID,
                    'option_json' => '{"kurl":"kurl"}',
                    'permission' => 1,
                    'status' => 1,
                ]
            ]);
        });

    }
}
