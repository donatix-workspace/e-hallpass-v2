<?php

namespace App\Events;

use App\Models\PassBlock;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PassBlockRunnedEvent implements ShouldBroadcastNow
{
    use Dispatchable, SerializesModels, InteractsWithSockets;

    public $passBlock;

    /**
     * @param PassBlock $passBlock
     */
    public function __construct(PassBlock $passBlock)
    {
        $this->passBlock = $passBlock;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('pass-blocks.' . $this->passBlock->school_id);
    }
}
