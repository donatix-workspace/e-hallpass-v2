<?php

namespace App\Console\Commands;

use App\Models\Pass;
use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class EndPassesAtMidnightCommand extends Command
{
    protected $signature = 'passes:midnight';

    protected $description = 'End all passes at midnight.';
    protected const END_HOUR = "00";

    // Task: https://eduspiresolutions.atlassian.net/browse/EHP2-1404
    public function handle()
    {
        // Get the schools with local time (00:00 AM)
        $schoolIds = School::selectRaw("id,DATE_FORMAT(CONVERT_TZ(utc_timestamp(),'+00:00', schools.timezone_offset),'%H') as logout_hour")
            ->having('logout_hour', self::END_HOUR)
            ->get()
            ->pluck('id');

        try {
            \DB::beginTransaction();
            Pass::where('pass_status', Pass::ACTIVE)
                ->where('canceled_at', null)
                ->where('expired_at', null)
                ->whereIntegerInRaw('school_id', $schoolIds)
                ->update([
                    'pass_status' => Pass::INACTIVE,
                    'approved_at' => now(),
                    'completed_at' => now(),
                    'flagged_at' => null,
                    'extended_at' => null,
                    'system_completed' => Pass::ACTIVE,
                    'expired_at' => Carbon::now()
                ]);
            \DB::commit();
        } catch (\Throwable $e) {
            logger($e->getMessage());
            $this->error($e->getMessage());
        } catch (\Exception $e) {
            logger($e->getMessage());
            $this->error($e->getMessage());
        }
    }
}
