<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('api/v5/users/login', 'Users\UserController@login')->name(
    'api.v5.users.login'
);
Route::post(
    'api/v5/users/password/reset',
    'Users\UserController@resetPassword'
)->name('api.v5.users.resetPassword');
Route::any('authcallback', 'Users\UserController@authcallback')->name(
    'authcallback'
);
Route::any('api/v5/authcallback', 'Users\UserController@authcallback');
