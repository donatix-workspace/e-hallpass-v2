<?php

namespace Database\Seeders;

use App\Models\Pass;
use App\Models\Room;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class PassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $studentId = User::where('school_id', 1)
            ->where('role_id', config('roles.student'))
            ->inRandomOrder()
            ->first()
            ->id;

        $fromId = Room::where('school_id', 1)->latest()->first()->id;
        $toId = User::where('school_id', 1)->latest()->first()->id;

        // Normal one way passes
        Pass::factory()->count(30)->create([
            'parent_id' => null,
            'from_teacher_note' => '',
            'from_type' => 'App\\Models\\Room',
            'from_id' => $fromId,
            'to_type' => 'App\\Models\\User',
            'to_id' => $toId,
            'ordering' => 0,
            'to_teacher_note' => 'Test Note 2',
            'pass_status' => Pass::INACTIVE,
            'approved_with' => null,
            'ended_with' => null,
            'approved_at' => Carbon::now(),
            'completed_at' => Carbon::now(),
            'requested_at' => Carbon::now(),
            'arrived_at' => null,
            'type' => Pass::STUDENT_CREATED_PASS,
            'expired_at' => null,
            'canceled_at' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'user_id' => function () use ($studentId) {
                return $studentId;
            },
            'school_id' => function () {
                return 1;
            },
            'requested_by' => function () {
                return User::where('school_id', 1)
                    ->where('role_id', config('roles.student'))
                    ->inRandomOrder()
                    ->first()
                    ->id;
            },
            'approved_by' => function () {
                return User::where('school_id', 1)->latest()->first()->id;
            },
            'completed_by' => function () {
                return User::where('school_id', 1)->latest()->first()->id;
            },
            'created_by' => function () use ($studentId) {
                return User::where('school_id', 1)
                    ->where('role_id', config('roles.student'))
                    ->inRandomOrder()
                    ->first()
                    ->id;
            },
        ]);

        // Passes with child
        Pass::factory()->count(30)->create([
            'parent_id' => function () use ($fromId, $toId, $studentId) {
                Pass::factory()->count(1)->create([
                    'from_teacher_note' => '',
                    'from_type' => 'App\\Models\\User',
                    'parent_id' => null,
                    'from_id' => $toId,
                    'to_type' => 'App\\Models\\Room',
                    'to_id' => $fromId,
                    'ordering' => 0,
                    'to_teacher_note' => 'Test Note 2',
                    'pass_status' => Pass::INACTIVE,
                    'approved_with' => null,
                    'ended_with' => null,
                    'approved_at' => Carbon::now(),
                    'completed_at' => Carbon::now(),
                    'requested_at' => Carbon::now(),
                    'arrived_at' => null,
                    'type' => Pass::STUDENT_CREATED_PASS,
                    'expired_at' => null,
                    'canceled_at' => null,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),

                    'user_id' => function () use ($studentId) {
                        return $studentId;
                    },
                    'school_id' => function () {
                        return 1;
                    },
                    'requested_by' => function () use ($studentId) {
                        return $studentId;
                    },
                    'approved_by' => function () use ($toId) {
                        return $toId;
                    },
                    'completed_by' => function () use ($toId) {
                        return $toId;
                    },
                    'created_by' => function () use ($studentId) {
                        return $studentId;
                    },
                ])->first()->id;
            },
            'from_teacher_note' => '',
            'from_type' => 'App\\Models\\Room',
            'from_id' => $fromId,
            'to_type' => 'App\\Models\\User',
            'to_id' => $toId,
            'ordering' => 0,
            'to_teacher_note' => 'Test Note 2',
            'pass_status' => Pass::INACTIVE,
            'approved_with' => null,
            'ended_with' => null,
            'approved_at' => Carbon::now(),
            'completed_at' => Carbon::now(),
            'requested_at' => Carbon::now(),
            'arrived_at' => null,
            'type' => Pass::STUDENT_CREATED_PASS,
            'expired_at' => null,
            'canceled_at' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'user_id' => function () use ($studentId) {
                return $studentId;
            },
            'school_id' => function () {
                return 1;
            },
            'requested_by' => function () use ($studentId) {
                return $studentId;
            },
            'approved_by' => function () use ($toId) {
                return $toId;
            },
            'completed_by' => function () use ($toId) {
                return $toId;
            },
            'created_by' => function () use ($studentId) {
                return $studentId;
            },
        ]);

        //Passes with apt type
        Pass::factory()->count(30)->create([
            'parent_id' => null,
            'from_teacher_note' => '',
            'from_type' => 'App\\Models\\Room',
            'from_id' => $fromId,
            'to_type' => 'App\\Models\\User',
            'to_id' => $toId,
            'ordering' => 0,
            'to_teacher_note' => 'Test Note 2',
            'pass_status' => Pass::INACTIVE,
            'approved_with' => null,
            'ended_with' => null,
            'approved_at' => Carbon::now(),
            'completed_at' => Carbon::now(),
            'requested_at' => Carbon::now(),
            'arrived_at' => null,
            'type' => Pass::APPOINTMENT_PASS,
            'expired_at' => null,
            'canceled_at' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'user_id' => function () use ($studentId) {
                return $studentId;
            },
            'school_id' => function () {
                return 1;
            },
            'requested_by' => function () {
                return User::where('school_id', 1)->latest()->first()->id;
            },
            'approved_by' => function () {
                return User::where('school_id', 1)->latest()->first()->id;
            },
            'completed_by' => function () {
                return User::where('school_id', 1)->latest()->first()->id;
            },
            'created_by' => function () use ($studentId) {
                return $studentId;
            },
        ]);

        // Passes Expired
        Pass::factory()->count(30)->create([
            'parent_id' => null,
            'from_teacher_note' => '',
            'from_type' => 'App\\Models\\Room',
            'from_id' => $fromId,
            'to_type' => 'App\\Models\\User',
            'to_id' => $toId,
            'ordering' => 0,
            'to_teacher_note' => 'Test Note 2',
            'pass_status' => Pass::INACTIVE,
            'approved_with' => null,
            'ended_with' => null,
            'approved_at' => null,
            'completed_at' => null,
            'requested_at' => null,
            'arrived_at' => null,
            'type' => Pass::STUDENT_CREATED_PASS,
            'expired_at' => Carbon::now(),
            'canceled_at' => null,
            'created_at' => Carbon::now()->subDay(),
            'updated_at' => Carbon::now()->subDay(),

            'user_id' => function () use ($studentId) {
                return $studentId;
            },
            'school_id' => function () {
                return 1;
            },
            'requested_by' => function () {
                return User::where('school_id', 1)->latest()->first()->id;
            },
            'approved_by' => function () {
                return User::where('school_id', 1)->latest()->first()->id;
            },
            'completed_by' => function () {
                return User::where('school_id', 1)->latest()->first()->id;
            },
            'created_by' => function () use ($studentId) {
                return $studentId;
            },
        ]);
    }
}
