<?php

namespace App\Traits;

use App\Models\Pass;
use Illuminate\Support\Collection as BaseCollection;
use Laravel\Scout\Jobs\MakeSearchable;
use ScoutElastic\Searchable;

trait ExtendedSearchable
{
    use Searchable {
        Searchable::syncWithSearchUsing as parentSyncWithSearchUsing;
        Searchable::syncWithSearchUsingQueue as parentSyncWithSearchUsingQueue;
        Searchable::queueMakeSearchable as parentQueueMakeSearchable;
    }

    protected static $massIndex = false;
    protected static $queue = 'default_long';
    public $async = false;

    /**
     * @return $this
     */
    public function aSync(): self
    {
        $this->async = true;

        return $this;
    }

    /**
     * Dispatch the job to make the given models searchable.
     *
     * @param  \Illuminate\Database\Eloquent\Collection  $models
     * @return void
     */
    public function queueMakeSearchable($models)
    {
        $async = optional($models->first())->async;

        if ($models->isEmpty()) {
            return;
        }

        if ($async) {
            return $models
                ->first()
                ->searchableUsing()
                ->update($models);
        }

        if (!config('scout.queue')) {
            return $models
                ->first()
                ->searchableUsing()
                ->update($models);
        }

        dispatch(
            (new MakeSearchable($models))
                ->onQueue($models->first()->syncWithSearchUsingQueue())
                ->onConnection($models->first()->syncWithSearchUsing())
        );
    }

    public static function enableMassIndex()
    {
        self::$massIndex = true;
    }

    public static function disableMassIndex()
    {
        self::$massIndex = false;
    }

    public static function getMassIndex()
    {
        return self::$massIndex;
    }

    /**
     * @param string $queue
     */
    public static function setSearchableQueue(string $queue = 'default_long')
    {
        self::$queue = $queue;
    }

    /**
     * Get the queue connection that should be used when syncing.
     *
     * @return string
     */
    public function syncWithSearchUsing()
    {
        return self::$massIndex
            ? 'redis-long-running'
            : config('scout.queue.connection');
    }

    /**
     * Get the queue that should be used with syncing.
     *
     * @return string
     */
    public function syncWithSearchUsingQueue()
    {
        if ($this instanceof Pass) {
            self::setSearchableQueue('passes_sync');
        }

        return self::$massIndex ? self::$queue : config('scout.queue.queue');
    }

    public static function bulkImport(
        $cli,
        $chunk,
        $offset = 0,
        $condition = null
    ) {
        $className = self::class;
        if (!$offset && !$condition) {
            $cli->call('scout:import', [
                'model' => $className,
                '--chunk' => $chunk
            ]);
        } else {
            self::where('id', '>', $offset)
                ->when($condition !== null, function ($query) use ($condition) {
                    if (count($condition) === 3) {
                        [$column, $firstRange, $secondRange] = $condition;

                        $query->whereBetween($column, [
                            $firstRange,
                            $secondRange
                        ]);
                    } else {
                        [$column, $value] = $condition;
                        $query->where($column, '=', $value);
                    }
                })
                ->chunkById($chunk, function ($models) use (
                    $className,
                    $cli,
                    $chunk
                ) {
                    $models->searchable($chunk);
                    $cli->info(
                        "Imported [$className] models up to ID:  {$models->last()->id}"
                    );
                });
        }
    }
}
