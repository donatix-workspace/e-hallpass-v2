<?php

namespace App\Http\Controllers\Admin\Periods;

use App\Http\Controllers\Controller;
use App\Http\Requests\PeriodRequest;
use App\Http\Resources\PeriodCollection;
use App\Http\Resources\PeriodResource;
use App\Models\Period;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

/**
 * @group Periods
 */
class PeriodController extends Controller
{
    /**
     * Get all period records for authenticated user school.
     *
     *
     * @authenticated
     * @return PeriodCollection
     * @apiResourceModel App\Models\Period
     * @apiResourceCollection App\Http\Resources\PeriodCollection
     * @throws AuthorizationException
     */
    public function index()
    {
        $user = auth()->user();

        $this->authorize('viewAny', Period::class);

        $periods = Period::fromSchoolId($user->school_id)->orderBy('order', 'asc')
            ->paginate(config('pagination.default_pagination'));

        return new PeriodCollection($periods);
    }

    /**
     * Create a period
     *
     * @bodyParam name string required The name of the period
     * @bodyParam status boolean required Status of the period
     *
     * @authenticated
     * @param PeriodRequest $request
     * @return PeriodResource
     * @apiResourceModel App\Models\Period
     * @apiResource App\Http\Resources\PeriodResource
     * @throws AuthorizationException
     */
    public function store(PeriodRequest $request)
    {
        $user = auth()->user();

        $this->authorize('create', Period::class);

        $period = Period::create([
            'school_id' => $user->school_id,
            'name' => $request->input('name'),
            'status' => $request->input('status')
        ]);

        return new PeriodResource($period);
    }

    /**
     * Update the period
     *
     * @urlParam period integer required The id of the period
     * @bodyParam name string required The name of the period
     * @bodyParam order integer required Ordering number
     * @bodyParam status boolean required The status of the period
     *
     * @authenticated
     * @param PeriodRequest $request
     * @param Period $period
     * @return PeriodResource
     * @apiResourceModel App\Models\Period
     * @apiResource App\Http\Resources\PeriodResource
     * @throws AuthorizationException
     */
    public function update(PeriodRequest $request, Period $period)
    {
        $this->authorize('update', $period);

        $period->update([
            'name' => $request->input('name'),
            'status' => $request->input('status'),
            'order' => $request->input('order')
        ]);

        return new PeriodResource($period->refresh());
    }

    /**
     * Update periods order
     *
     * @bodyParam periods array required The array of id and position key:value type
     *
     * @authenticated
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws AuthorizationException
     */
    public function order(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'periods' => 'array|required',
            'periods.*.id' => 'required|exists:periods,id',
            'periods.*.position' => 'required|integer|min:0'
        ]);

        $this->authorize('order', Period::class);

        $periods = $request->input('periods');

        foreach ($periods as $period) {
            $period = Period::where('school_id', auth()->user()->school_id)
                ->where('id', $period['id'])
                ->update(['order' => $period['position']]);
        }

        return response()->json([
            'data' => $periods,
            'status' => __('success'),
            'message' => __('success')
        ], 201);
    }

    /**
     * Show a period record
     *
     * @urlParam period integer required The id of the period
     *
     * @param Period $period
     * @return PeriodResource
     * @apiResourceModel App\Models\Period
     * @apiResource App\Http\Resources\PeriodResource
     * @throws AuthorizationException
     */
    public function show(Period $period)
    {
        $this->authorize('view', $period);

        return new PeriodResource($period);
    }

    /**
     * Delete a period
     *
     * @urlParam period integer required The id of the period
     *
     * @authenticated
     * @param Period $period
     * @return PeriodResource
     * @apiResourceModel App\Models\Period
     * @apiResource App\Http\Resources\PeriodResource
     * @throws AuthorizationException
     */
    public function delete(Period $period)
    {
        $this->authorize('delete', $period);

        $period->update(['deleted' => 1]);

        return new PeriodResource($period);
    }

    /**
     * Update period status
     *
     * @urlParam period integer required The id of the period
     * @bodyParam status boolean required The status of the period
     *
     * @authenticated
     * @param Period $period
     * @param Request $request
     * @return PeriodResource
     * @apiResourceModel App\Models\Period
     * @apiResource App\Http\Resources\PeriodResource
     * @throws AuthorizationException
     */
    public function status(Period $period, Request $request)
    {
        $this->authorize('status', $period);
        $request->validate(['status' => 'boolean|required']);

        $period->update([
            'status' => $request->input('status')
        ]);

        return new PeriodResource($period->refresh());
    }
}
