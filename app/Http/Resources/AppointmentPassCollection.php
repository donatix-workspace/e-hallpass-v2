<?php

namespace App\Http\Resources;

use App\Models\AppointmentPass;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AppointmentPassCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $appointmentPassesCount = AppointmentPass::ofUserId(auth()->user()->id)
            ->orderBy('confirmed_at', 'asc')
            ->where('confirmed_by_teacher_at', '!=', null)
            ->where('expired_at', null)
            ->where('ran_at', null)
            ->where('canceled_at', null)
            ->where(
                'for_date',
                '>=',
                Carbon::now()
                    ->setTimezone(
                        auth()
                            ->user()
                            ->school->getTimezone()
                    )
                    ->toDateTimeString()
            )
            ->whereDoesntHave('pass')
            ->where(
                'for_date',
                '<=',
                Carbon::now()
                    ->setTimezone(
                        auth()
                            ->user()
                            ->school->getTimezone()
                    )
                    ->endOfDay()
                    ->toDateTimeString()
            )
            ->fromSchoolId(auth()->user()->school_id)
            ->count();

        if (
            !auth()
                ->user()
                ->isStudent()
        ) {
            $appointmentPassesCount = AppointmentPass::where(
                'school_id',
                auth()->user()->school_id
            )
                ->orderBy('confirmed_by_teacher_at', 'asc')
                ->where('canceled_at', null)
                ->where('confirmed_by_teacher_at', '!=', null)
                ->where('expired_at', null)
                ->where('ran_at', null)
                ->where(function ($query) {
                    $query
                        ->where('from_type', User::class)
                        ->where('from_id', auth()->user()->id)
                        ->orWhere(function ($query) {
                            $query
                                ->where('to_type', User::class)
                                ->where('to_id', auth()->user()->id);
                        });
                })
                ->where('for_date', '>=', Carbon::now()->toDateTimeString())
                ->where(
                    'for_date',
                    '<=',
                    Carbon::now()
                        ->endOfDay()
                        ->toDateTimeString()
                )
                ->count();
        }

        return [
            'data' => $this->collection,
            'status' => 'success',
            'meta' => [
                'today_appointments_count' => $appointmentPassesCount
            ]
        ];
    }
}
