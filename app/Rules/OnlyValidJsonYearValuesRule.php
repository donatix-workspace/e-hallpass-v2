<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class OnlyValidJsonYearValuesRule implements Rule
{
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $gradeYears = collect(json_decode($value, true))->toArray();
        foreach ($gradeYears as $gradeYear) {
            if (!is_numeric($gradeYear) || $gradeYear < config('app.start_year') || $gradeYear < 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('invalid.grade.years.specified');
    }
}
