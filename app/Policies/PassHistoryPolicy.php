<?php

namespace App\Policies;

use App\Models\Pass;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PassHistoryPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return !$user->isStudent();
    }

    /**
     * @param User $user
     * @param Pass $pass
     * @return bool
     */
    public function update(User $user, Pass $pass): bool
    {
        return (
                $pass->approved_by === $user->id
                || $pass->completed_by === $user->id
                || $pass->from_type === User::class && $pass->from_id === $user->id
                || $pass->to_type === User::class && $pass->to_id === $user->id
            ) &&
            $pass->school_id === $user->school_id;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function runFilters(User $user)
    {
        return !$user->isStudent();
    }
}
