<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecurrenceAppointmentPassesTable extends Migration
{
    public function up()
    {
        Schema::create('recurrence_appointment_passes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_by');
            $table->morphs('from');
            $table->morphs('to');
            $table->unsignedBigInteger('user_id');
            $table->timestamp('for_date');
            $table->unsignedBigInteger('period_id');
            $table->string('recurrence_type')->nullable();
            $table->json('recurrence_days')->nullable();
            $table->json('recurrence_week')->nullable();
            $table->timestamp('recurrence_end_at')->nullable();
            $table->unsignedBigInteger('school_id');
            $table->string('reason')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamps();
        });

        Schema::table('recurrence_appointment_passes', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('period_id')->references('id')->on('periods');
        });
    }

    public function down()
    {
        Schema::table('appointment_passes', function (Blueprint $table) {
            $table->dropForeign('appointment_passes_created_by_foreign')->references('id')->on('users');
            $table->dropForeign('appointment_passes_user_id_foreign')->references('id')->on('users');
            $table->dropForeign('appointment_passes_school_id_foreign')->references('id')->on('schools');
            $table->dropForeign('appointment_passes_period_id_foreign')->references('id')->on('periods');
        });
        Schema::dropIfExists('recurrence_appointment_passes');
    }
}
