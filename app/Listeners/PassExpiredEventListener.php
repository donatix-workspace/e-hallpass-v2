<?php

namespace App\Listeners;

use App\Events\PassExpired;

class PassExpiredEventListener
{
    public function __construct()
    {
        //
    }

    public function handle(PassExpired $event)
    {
        $event->pass->refreshDashboard();
    }
}
