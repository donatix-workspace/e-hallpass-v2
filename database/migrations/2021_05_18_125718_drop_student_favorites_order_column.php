<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropStudentFavoritesOrderColumn extends Migration
{
    public function up()
    {
        Schema::table('student_favorites', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }

    public function down()
    {
        // Nothing
    }
}
