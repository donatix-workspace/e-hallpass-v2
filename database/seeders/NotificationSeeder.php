<?php

namespace Database\Seeders;

use App\Models\Notification;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Seeder;

class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notifications = Notification::factory()
            ->count(20)
            ->create();

        $randomSchool = School::inRandomOrder()->first();
        $randomUser = User::inRandomOrder()->first();

        $notifications->each(function ($notification) use ($randomSchool, $randomUser) {
           $notification->update(['user_id' => $randomUser->id, 'school_id' => $randomSchool->id]);
        });
    }
}
