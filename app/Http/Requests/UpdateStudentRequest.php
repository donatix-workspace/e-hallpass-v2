<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'kiosk_password' => 'sometimes|min:4|max:30'
        ];
    }

    public function authorize()
    {
        return auth()->check() && auth()->user()->isStudent();
    }
}
