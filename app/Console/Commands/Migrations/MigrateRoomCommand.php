<?php

namespace App\Console\Commands\Migrations;

use App\Models\Room;
use App\Models\RoomType;
use App\Models\School;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateRoomCommand extends Command
{
    protected $signature = 'migrate_sys:rooms {--offset=} {--notMigrated=}';

    protected $description = 'Migrate rooms from EHPv1 to EHPv2';

    public function handle()
    {
        $this->info('--- Migrating Room Types & Rooms --- ');

        if ($this->option('notMigrated')) {
            $roomsNotInV2 = DB::table('rooms')
                ->select('old_id')
                ->whereNotNull('old_id')
                ->get()
                ->pluck('old_id')
                ->toArray();

            $rooms = DB::connection('oldDB')
                ->table('rooms')
                ->orderBy('room_id', 'desc')
                ->whereNotIn('room_id', $roomsNotInV2)
                ->get()
                ->toArray();
        } else {
            $rooms = DB::connection('oldDB')
                ->table('rooms')
                ->orderBy('room_id', 'desc')
                ->where(
                    'room_id',
                    '>',
                    $this->option('offset') ? $this->option('offset') : 0
                )
                ->get()
                ->toArray();
        }

        $roomsChunk = array_chunk($rooms, 1000);
        $commentTypes = [
            0 => Room::COMMENT_HIDDEN,
            1 => Room::COMMENT_MANDATORY,
            2 => Room::COMMENT_OPTIONAL
        ];
        sleep(3);

        $this->output->newLine();

        $this->info('Room  Importing:');
        $this->output->progressStart(
            DB::connection('oldDB')
                ->table('rooms')
                ->where(
                    'room_id',
                    '>',
                    $this->option('offset') ? $this->option('offset') : 0
                )
                ->count()
        );

        foreach ($roomsChunk as $roomChunk) {
            $insertableRoomArray = [];
            foreach ($roomChunk as $oldRoom) {
                $oldSchool = DB::connection('oldDB')
                    ->table('school_schedule')
                    ->where('school_id', $oldRoom->school_id)
                    ->first();
                $newSchool = School::where(
                    'old_id',
                    optional($oldSchool)->school_id
                )->first();

                $oldRoomType = DB::connection('oldDB')
                    ->table('room_type')
                    ->where('type_id', $oldRoom->type_id)
                    ->first();

                if ($newSchool) {
                    $newRoomType = RoomType::where([
                        ['name', '=', optional($oldRoomType)->type_name],
                        ['school_id', '=', $newSchool->id]
                    ])->first();

                    $tripType = Room::TRIP_ONE_WAY;

                    if ($oldRoom->two_way === 2) {
                        $tripType = Room::TRIP_ROUNDTRIP;
                    }

                    if ($oldRoom->two_way === 1) {
                        $tripType = Room::TRIP_LAYOVER;
                    }

                    $insertableRoomArray[] = [
                        'name' => $oldRoom->room_name,
                        'type_id' => optional($newRoomType)->id,
                        'school_id' => $newSchool->id,
                        'trip_type' => $tripType,
                        'status' => $oldRoom->status,
                        'enable_appointment_passes' => $oldRoom->sapp_status,
                        'comment_type' => optional($commentTypes)[
                            $oldRoom->comment_type
                        ],
                        'old_id' => $oldRoom->room_id
                    ];
                }
            }
            DB::table('rooms')->insert($insertableRoomArray);
            $this->output->progressAdvance(count($insertableRoomArray));
        }

        sleep(2);
        $this->output->progressFinish();
    }
}
