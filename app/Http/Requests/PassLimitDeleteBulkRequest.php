<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PassLimitDeleteBulkRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'pass_limit_ids' => 'required|array',
            'pass_limit_ids.*' => ['required', 'integer', 'min:1', Rule::exists('pass_limits', 'id')->where('school_id', optional(auth()->user())->school_id)]
        ];
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check() && !auth()->user()->isStudent();
    }
}
