<?php

namespace App\Http\Controllers\Admin\Passes;

use App\Http\Controllers\Controller;
use App\Http\Filters\PassHistoryFilter;
use App\Imports\PassesImport;
use Excel;
use Log;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ImportController extends Controller
{
    /**
     * Import pass history data as csv
     *
     * @authenticated
     * @param PassHistoryFilter $filters
     * @return \Illuminate\Http\JsonResponse|BinaryFileResponse
     */
    public function export(PassHistoryFilter $filters)
    {
        try {
            return Excel::download(
                new PassesImport($filters),
                'pass-history.csv'
            );
        } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
            Log::error($e);
            return response()->json([
                'data' => [],
                'status' => __('fail'),
                'message' => __('something.went.wrong')
            ]);
        }
    }
}
