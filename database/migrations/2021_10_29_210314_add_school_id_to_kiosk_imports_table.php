<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSchoolIdToKioskImportsTable extends Migration
{
    public function up()
    {
        Schema::table('kiosk_imports', function (Blueprint $table) {
            $table->unsignedBigInteger('school_id');
        });
    }

    public function down()
    {
        Schema::table('kiosk_imports', function (Blueprint $table) {
            $table->dropColumn('school_id');
        });
    }
}
