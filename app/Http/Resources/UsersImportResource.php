<?php

namespace App\Http\Resources;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UsersImportResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "first_name" => $this->first_name,
            "email" => $this->email,
            "last_name" => $this->last_name,
            "status" => $this->status == User::ACTIVE ? User::ACTIVE_STATUS_NAME : User::INACTIVE_STATUS_NAME,
            "role" => Role::getRoleNameById($this->role_id),
            "role_id" => $this->role_id,
            "school_id" => $this->school_id,
            "grade_year" => $this->grade_year,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "inserted" => filter_var($this->inserted, FILTER_VALIDATE_BOOLEAN),
            "exist" => filter_var($this->exist, FILTER_VALIDATE_BOOLEAN),
            "file_name" => $this->file_name,
            "is_substitute" => $this->is_substitute,
            "is_failed" => $this->is_failed,
            "errors"=>json_decode($this->errors)
        ];
    }
}
