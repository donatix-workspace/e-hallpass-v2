<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullableGradeYearToUsersImports extends Migration
{
    public function up()
    {
        Schema::table('users_imports', function (Blueprint $table) {
            $table->dropColumn('grade_year');
        });
    }

    public function down()
    {
        Schema::table('users_imports', function (Blueprint $table) {

        });
    }
}
