<?php

namespace Database\Factories;

use App\Models\Period;
use App\Models\RecurrenceAppointmentPass;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class RecurrenceAppointmentPassFactory extends Factory
{
    protected $model = RecurrenceAppointmentPass::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'created_by' => $this->faker->randomNumber(),
            'from_type' => $this->faker->word,
            'from_id' => $this->faker->randomNumber(),
            'to_type' => $this->faker->word,
            'to_id' => $this->faker->randomNumber(),
            'for_date' => $this->faker->word,
            'recurrence_type' => $this->faker->word,
            'recurrence_days' => $this->faker->word,
            'recurrence_week' => $this->faker->word,
            'recurrence_end_at' => $this->faker->word,
            'reason' => $this->faker->word,
            'expired_at' => $this->faker->word,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'user_id' => function () {
                return User::factory()->create()->id;
            },
            'period_id' => function () {
                return Period::factory()->create()->id;
            },
            'school_id' => function () {
                return School::factory()->create()->id;
            },
        ];
    }
}
