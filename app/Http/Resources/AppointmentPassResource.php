<?php

namespace App\Http\Resources;

use App\Models\AppointmentPass;
use App\Models\School;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentPassResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $school = School::findCacheFirst($this->school_id);

        return [
            'id' => $this->id,
            'completed_by' => $this->completed_by,
            'canceled_by' => $this->canceled_by,
            'canceled_at' => School::convertTimeToCorrectTimezone(
                $this->canceled_at,
                $school->getTimezone()
            ),
            'acknowledged_by' => $this->acknowledged_by,
            'for_date' => School::convertTimeToCorrectTimezone(
                $this->for_date,
                $school->getTimezone()
            ),
            'confirmed_by_user' => optional($this->confirmedByUser)->setAppends(
                []
            ),
            'acknowledged_by_user' => optional(
                $this->acknowledgedByUser
            )->setAppends([]),
            'canceled_by_user' => optional($this->canceledByUser)->setAppends(
                []
            ),
            'confirmed_by' => $this->confirm_by,
            'from_type' => $this->from_type,
            'to_type' => $this->to_type,
            'string_status' => $this->string_status,
            'cancel_reason' => $this->cancel_reason,
            'latest_comment' => $this->latestComment,
            'from' => [
                'first_name' => $this->from_id
                    ? optional($this->from)->first_name
                    : AppointmentPass::SYSTEM_PREFIX,
                'last_name' => $this->from_id
                    ? optional($this->from)->last_name
                    : '',
                'id' => $this->from_id ? optional($this->from)->id : 0,
                'role_id' => optional($this->from)->role_id,
                'role' => optional($this->from)->role,
                'name' => $this->from_id ? optional($this->from)->name : 0,
                'icon' => optional($this->from)->icon
            ],
            'is_for_confirm' => $this->is_for_confirm,
            'can_be_edited' => $this->can_be_edited,
            'is_active' => $this->is_active,
            'waiting_activation' => $this->waiting_activation,
            'can_be_canceled' => $this->can_be_canceled,
            'is_for_acknowledge' => $this->is_for_acknowledge,
            'to' => [
                'first_name' => optional($this->to)->first_name,
                'last_name' => optional($this->to)->last_name,
                'id' => optional($this->to)->id,
                'role_id' => optional($this->to)->role_id,
                'role' => optional($this->to)->role,
                'name' => optional($this->to)->name,
                'icon' => optional($this->to)->icon
            ],
            'expired_at' => School::convertTimeToCorrectTimezone(
                $this->expired_at,
                $school->getTimezone()
            ),
            'to_id' => $this->to_id,
            'is_missed' => $this->is_missed,
            'is_ended' => $this->is_ended,
            'can_be_confirmed' => $this->is_for_confirm,
            'is_for_today' => $this->is_for_today,
            'is_canceled' => $this->is_canceled,
            'is_for_future' => $this->is_for_future,
            'is_missed_request' => $this->is_missed_request,
            'confirm_cancel' => $this->confirm_cancel,
            'comments' => $this->comments,
            'from_id' => $this->from_id,
            'user_id' => $this->user_id,
            'user' => optional($this->user)->setAppends([]),
            'created_by' => $this->created_by,
            'created_by_user' => optional($this->createdByUser)->setAppends([]),
            'is_in_another_pass' => $this->is_in_another_pass,
            'period_id' => $this->period_id,
            'period' => $this->period,
            'pass_id' => $this->pass_id,
            'recurrence_appointment_pass' => new RecurrenceAppointmentPassResource(
                $this->recurrenceAppointmentPass
            ),
            'recurrence_appointment_pass_id' =>
                $this->recurrence_appointment_pass_id,
            'reason' => $this->reason,
            'ran_at' => School::convertTimeToCorrectTimezone(
                $this->ran_at,
                $school->getTimezone()
            ),
            'confirmed_at' => School::convertTimeToCorrectTimezone(
                $this->confirmed_at,
                $school->getTimezone()
            ),
            'acknowledge_at' => School::convertTimeToCorrectTimezone(
                $this->acknowledge_at,
                $school->getTimezone()
            ),
            'created_at' => School::convertTimeToCorrectTimezone(
                $this->created_at,
                $school->getTimezone()
            ),
            'acknowledged_by_teacher_at' => School::convertTimeToCorrectTimezone(
                $this->acknowledged_by_teacher_at,
                $school->getTimezone()
            ),
            'confirmed_by_teacher_at' => School::convertTimeToCorrectTimezone(
                $this->confirmed_by_teacher_at,
                $school->getTimezone()
            ),
            'edited_at' => $this->edited_at,
            'acknowledged_by_mail_at' => School::convertTimeToCorrectTimezone(
                $this->acknowledged_by_mail_at,
                $school->getTimezone()
            ),
            'mail_send_at' => School::convertTimeToCorrectTimezone(
                $this->mail_send_at,
                $school->getTimezone()
            ),
            'updated_at' => School::convertTimeToCorrectTimezone(
                $this->updated_at,
                $school->getTimezone()
            )
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array|string[]
     */
    public function with($request)
    {
        return [
            'status' => 'success'
        ];
    }
}
