<?php

namespace App\Console\Commands\Rollback;

use Illuminate\Console\Command;
use DB;

class RollbackTransparencyUserCommand extends Command
{
    protected $signature = 'ehpv1_rollback:transparency_user';

    protected $description = 'Rollback Transparency Users from EHPV2 to EhpV1';

    public function handle()
    {
        $this->info('--- Rollback Transparency Users from EHPV2 to EHPV1 ---');

        $newDb = DB::connection();
        $oldDb = DB::connection('oldDB');

        $newDb->disableQueryLog();
        $oldDb->disableQueryLog();

        $transparenciesUsers = $newDb->table('transparency_users')
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();

        $this->output->progressStart(count($transparenciesUsers));

        $transparenciesChunk = array_chunk($transparenciesUsers, 1500);

        foreach ($transparenciesChunk as $transparencyChunk) {
            $insertableTransparencyArray = [];
            foreach ($transparencyChunk as $transparency) {
                $newSchool = $newDb->table('schools')->select('old_id')->where('id', $transparency->school_id)
                    ->first();

                if ($newSchool) {
                    $newUser = $newDb->table('users')->select('old_id')->where('id', $transparency->user_id)->first();

                    if ($newUser) {
                        $insertableTransparencyArray[] = [
                            'school_id' => $newSchool->old_id,
                            'teacher_id' => $newUser->old_id,
                            'created_at' => now(),
                            'updated_at' => now()
                        ];
                    }
                }
            }

            try {
                DB::beginTransaction();

                $oldDb->table('privacy_setting_teacher')
                    ->insert($insertableTransparencyArray);

                $this->output->progressAdvance(count($insertableTransparencyArray));
                DB::commit();
            } catch (\Throwable $e) {
                $this->info($e->getMessage());
            }

        }

        $this->output->progressFinish();

        return 0;
    }
}
