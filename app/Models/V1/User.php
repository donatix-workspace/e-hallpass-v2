<?php

namespace App\Models\V1;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Models\SchoolUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    protected $connection = 'oldDB';
    protected $table = 'users';
    protected $primaryKey = 'user_id';
    protected $adminUser;

    public function sync($school = null)
    {
        $this->adminUser = \App\Models\User::where(
            'email',
            'viktor.lalev@donatix.net'
        )
            ->latest()
            ->first();

        if (!$school) {
            $school = \App\Models\School::where(
                'old_id',
                $this->school_id
            )->first();
        }
        if (!$school) {
            return false;
        }
        $roles = [
            0 => 6, //subs
            -2 => null, //none => null
            1 => 5, //superadmin => superadmin
            2 => 2, //admin => admin
            3 => 3, //teacher => teacher
            4 => 1, //student => student
            5 => 4, //staff => staff
            6 => 2, //schooladmin => admin
            7 => null, //system => null
            8 => null, //norole => null
            10 => null
        ];
        $roleId = $roles[$this->role_id];
        $status = $this->status > 0 ? 1 : 0;
        $isSubs = 0;

        if ($roleId == 6) {
            $roleId = 3;
            $isSubs = 1;
        }

        if (!$roleId) {
            $status = 0;
            $roleId = 1;
        }
        if ($this->status == -2) {
            $status = 1;
        }

        $membership = DB::table('schools_users')
            ->where('school_id', $school->id)
            ->where('old_id', $this->user_id)
            ->orderBy('id', 'desc')
            ->first();


        if (!$membership) {
            $user = \App\Models\User::withoutGlobalScopes()
                ->where('email', trim($this->email))
                ->first();
            if ($user) {
                $user = $this->swapIfDuplicates($user);
            }

            if (!$user) {
                $user = \App\Models\User::create([
                    'student_sis_id' => $this->student_sis_id,
                    'email' => trim($this->email),
                    'username' => $this->username,
                    'password' => $this->password,
                    'first_name' => $this->firstname,
                    'last_name' => $this->lastname,
                    'metatag' => $this->metatag,
                    'role_id' => $roleId,
                    'avatar' => $this->avatar,
                    'grade_year' => $this->gradyear,
                    'status' => $status,
                    'school_id' => $school->id,
                    'is_searchable' => $this->is_searchable,
                    'user_initials' => $this->user_initials,
                    'is_loggin' => $this->is_loggin,
                    'clever_id' => $this->clever_id,
                    'updated_role' => $this->updated_role,
                    'last_logged_in_at' => $this->last_logged_in_at,
                    'auth_type' => $this->auth_type,
                    'created_at' => $this->created_at,
                    'updated_at' => now(),
                    'old_id' => $this->user_id,
                    'is_substitute' => $isSubs,
                ]);

            }
            $membership = DB::table('schools_users')
                ->insert([
                    'user_id' => $user->id,
                    'school_id' => $school->id,
                    'role_id' => $roleId,
                    'old_id' => $this->user_id,
                ]);
            $membership = DB::table('schools_users')
                ->where('user_id', $user->id)
                ->where('school_id', $school->id)
                ->first();
           
        } else {
            $user = \App\Models\User::withoutGlobalScopes()
                ->where('id', $membership->user_id)
                ->first();
        }

        $updateData = [
            'student_sis_id' => $this->student_sis_id,
            'email' => trim($this->email),
            'username' => $this->username,
            'password' => $this->password,
            'first_name' => $this->firstname,
            'last_name' => $this->lastname,
            'metatag' => $this->metatag,
            'role_id' => $roleId,
            'avatar' => $this->avatar,
            'grade_year' => $this->gradyear,
            'status' => $status,
            'school_id' => $school->id,
            'is_searchable' => $this->is_searchable,
            'user_initials' => $this->user_initials,
            'is_loggin' => $this->is_loggin,
            'clever_id' => $this->clever_id,
            'updated_role' => $this->updated_role,
            'last_logged_in_at' => $this->last_logged_in_at,
            'auth_type' => $this->auth_type,
            'created_at' => $this->created_at,
            'updated_at' => now(),
            'old_id' => $this->user_id,
            'is_substitute' => $isSubs,
        ];


        $user->update($updateData);
        $user->refresh();

        DB::table('schools_users')->where('id', $membership->id)
            ->update([
                'status' => $this->status == -2 ? 0 : $status,
                'archived_by' => $this->status == -2 ? $this->adminUser->id : null,
                'archived_at' => $this->status == -2 ? $this->updated_at : null,
                'student_sis_id' => $this->student_sis_id,
                'deleted_at' => null
            ]);

        $membership = DB::table('schools_users')->where('id', $membership->id)->first();

        if (!$isSubs && $this->status != -2) {

            if ($user->common_user_id) {
                $commonMembership = DB::connection('commonDb')
                    ->table('common_bm_membership')
                    ->where('auth_user_id', $user->common_user_id)
                    ->where('auth_client_id', 2)
                    ->where('common_bm_school_id', $school->common_id)
                    ->where('not_archived', $status)
                    ->orderBy('id', 'desc')
                    ->first();

                if ($commonMembership) {
                    return $this->updateCus(
                        $membership,
                        $user,
                        $school,
                        $roleId,
                        $status,
                        $commonMembership
                    );

                } else {
                    return $this->migrateToCus(
                        $user,
                        $school,
                        $roleId,
                        $status,
                        $membership
                    );
                }
            } else {
                return $this->migrateToCus(
                    $user,
                    $school,
                    $roleId,
                    $status,
                    $membership
                );
            }
        }
        $user->searchable();
        return $user;
    }

    protected function swapIfDuplicates($user)
    {

        $swapUser = $user;

        $moreUsers = \App\Models\User::withoutGlobalScopes()
            ->where('email', trim($swapUser->email))
            ->where('id', '!=', $swapUser->id)
            ->exists();

        if ($moreUsers) {
            $duplicatedUser = DB::table('duplicate_users_clone')
                ->where('from_id', $swapUser->id)
                ->first();

            if ($duplicatedUser) {
                $dUser = \App\Models\User::withoutGlobalScopes()
                    ->where('id', $duplicatedUser->to_id)
                    ->first();

                if (trim($dUser->email) == trim($swapUser->email)) {
                    $swapUser = $dUser;
                } else {
                    DB::table('duplicate_users_clone')
                        ->where('from_id', $swapUser->id)
                        ->delete();
                }

            } else {
                DB::table('duplicate_users_clone')->updateOrInsert(
                    [
                        'from_id' => $swapUser->id,
                        'to_id' => $swapUser->id
                    ],
                    [
                        'old_from_id' => $swapUser->old_id
                    ]
                );
            }
        }

        return $swapUser;
    }

    public function migrateToV2andCus($forceCreate = false)
    {
        $this->adminUser = \App\Models\User::where(
            'email',
            'viktor.lalev@donatix.net'
        )
            ->latest()
            ->first();

        $school = \App\Models\School::where(
            'old_id',
            $this->school_id
        )->first();
        if (!$school) {
            return false;
        }
        $roles = [
            0 => 6, //subs
            -2 => null, //none => null
            1 => 5, //superadmin => superadmin
            2 => 2, //admin => admin
            3 => 3, //teacher => teacher
            4 => 1, //student => student
            5 => 4, //staff => staff
            6 => 2, //schooladmin => admin
            7 => null, //system => null
            8 => null, //norole => null
            10 => null
        ];
        $roleId = $roles[$this->role_id];
        $status = $this->status > 0 ? 1 : 0;
        $archBy = null;
        $archAt = null;
        $archSchool = null;
        $isSubs = 0;

        if ($roleId == 6) {
            $roleId = 3;
            $isSubs = 1;
        }

        if (!$roleId) {
            $status = 0;
            $roleId = 1;
        }
        if ($this->status == -2) {
            $status = 1;
            $archBy = $this->adminUser->id;
            $archAt = $this->updated_at;
            $archSchool = $school->id;
        }

        $user = \App\Models\User::withoutGlobalScopes()
            ->where('email', trim($this->email))
            ->first();

        if (!$user) {
            $user = \App\Models\User::create([
                'student_sis_id' => $this->student_sis_id,
                'email' => trim($this->email),
                'username' => $this->username,
                'password' => $this->password,
                'first_name' => $this->firstname,
                'last_name' => $this->lastname,
                'metatag' => $this->metatag,
                'role_id' => $roleId,
                'avatar' => $this->avatar,
                'grade_year' => $this->gradyear,
                'status' => $status,
                'school_id' => $school->id,
                'is_searchable' => $this->is_searchable,
                'user_initials' => $this->user_initials,
                'is_loggin' => $this->is_loggin,
                'clever_id' => $this->clever_id,
                'updated_role' => $this->updated_role,
                'last_logged_in_at' => $this->last_logged_in_at,
                'auth_type' => $this->auth_type,
                'created_at' => $this->created_at,
                'updated_at' => now(),
                'old_id' => $this->user_id,
                'is_substitute' => $isSubs,
//                'archived_by' => $archBy,
//                'archived_at' => $archAt,
//                'archived_school' => $archSchool
            ]);
        } else {
            $moreUsers = \App\Models\User::withoutGlobalScopes()
                ->where('email', trim($user->email))
                ->where('id', '!=', $user->id)
                ->exists();

            if ($moreUsers) {
                $duplicatedUser = DB::table('duplicate_users_clone')
                    ->where('from_id', $user->id)
                    ->first();

                if ($duplicatedUser) {
                    $dUser = \App\Models\User::withoutGlobalScopes()
                        ->where('id', $duplicatedUser->to_id)
                        ->first();

                    if (trim($dUser->email) == trim($user->email)) {
                        $user = $dUser;
                    } else {
                        DB::table('duplicate_users_clone')
                            ->where('from_id', $user->id)
                            ->delete();
                    }

                    if ($forceCreate) {
                        $newDuplicatedUser = \App\Models\User::create([
                            'student_sis_id' => $this->student_sis_id,
                            'email' => trim($this->email),
                            'username' => $this->username,
                            'password' => $this->password,
                            'first_name' => $this->firstname,
                            'last_name' => $this->lastname,
                            'metatag' => $this->metatag,
                            'role_id' => $roleId,
                            'avatar' => $this->avatar,
                            'grade_year' => $this->gradyear,
                            'status' => $status,
                            'school_id' => $school->id,
                            'is_searchable' => $this->is_searchable,
                            'user_initials' => $this->user_initials,
                            'is_loggin' => $this->is_loggin,
                            'clever_id' => $this->clever_id,
                            'updated_role' => $this->updated_role,
                            'last_logged_in_at' => $this->last_logged_in_at,
                            'auth_type' => $this->auth_type,
                            'created_at' => $this->created_at,
                            'updated_at' => now(),
                            'old_id' => $this->user_id,
                            'is_substitute' => $isSubs,
//                            'archived_by' => $archBy,
//                            'archived_at' => $archAt,
//                            'archived_school' => $archSchool
                        ]);

                        DB::table('duplicate_users_clone')->updateOrInsert(
                            [
                                'from_id' => $newDuplicatedUser->id,
                                'to_id' => $user->id
                            ],
                            [
                                'old_from_id' => $newDuplicatedUser->old_id
                            ]
                        );
                    }
                } else {
                    DB::table('duplicate_users_clone')->updateOrInsert(
                        [
                            'from_id' => $user->id,
                            'to_id' => $user->id
                        ],
                        [
                            'old_from_id' => $user->old_id
                        ]
                    );
                }


            }

            $updateData = [
                'student_sis_id' => $this->student_sis_id,
                'email' => trim($this->email),
                'username' => $this->username,
                'password' => $this->password,
                'first_name' => $this->firstname,
                'last_name' => $this->lastname,
                'metatag' => $this->metatag,
                'role_id' => $roleId,
                'avatar' => $this->avatar,
                'grade_year' => $this->gradyear,
                'status' => $status,
                'school_id' => $school->id,
                'is_searchable' => $this->is_searchable,
                'user_initials' => $this->user_initials,
                'is_loggin' => $this->is_loggin,
                'clever_id' => $this->clever_id,
                'updated_role' => $this->updated_role,
                'last_logged_in_at' => $this->last_logged_in_at,
                'auth_type' => $this->auth_type,
                'created_at' => $this->created_at,
                'updated_at' => now(),
                'old_id' => $this->user_id,
                'is_substitute' => $isSubs,
            ];
            if ($status) {
                $updateData['archived_by'] = null;
                $updateData['archived_at'] = null;
                $updateData['deleted_at'] = null;
            }

            $commonUser = CommonUser::where('email', trim($user->email))->first();

            if ($commonUser) {
                $updateData['cus_uuid'] = $commonUser->uuid;
                $updateData['common_user_id'] = $commonUser->id;
            }
            $user->update($updateData);

        }
        $user = $user->refresh();

        if (!$isSubs && $this->status != -2) {
            $membership = DB::table('schools_users')
                ->where('school_id', $school->id)
                ->where('user_id', $user->id)
                ->whereNotNull('membership_uuid')
//                ->whereNull('deleted_at')
                ->orderBy('id', 'desc')
                ->first();

            $commonMembership = null;

            if ($user->common_user_id) {
                $commonMembership = DB::connection('commonDb')
                    ->table('common_bm_membership')
                    ->where('auth_user_id', $user->common_user_id)
                    ->where('auth_client_id', 2)
                    ->where('common_bm_school_id', $school->common_id)
                    ->orderBy('id', 'desc')
                    ->first();

            }

            if ($membership) {
                if ($commonMembership) {
                    DB::table('schools_users')
                        ->where('school_id', $school->id)
                        ->where('user_id', $user->id)
                        ->update([
                            'membership_uuid' => $commonMembership->uuid,
                            'status' => $commonMembership->not_archived,
                            'deleted_at' => $commonMembership->archived_at
                        ]);


                    if ($this->status > 0 && $commonMembership->archived_at) {
                        return $this->migrateToCus(
                            $user,
                            $school,
                            $roleId,
                            $status
                        );
                    } else {
                        return $this->updateCus(
                            $membership,
                            $user,
                            $school,
                            $roleId,
                            $status
                        );
                    }
                    $user->searchable();
                } else {
                    if ($membership->deleted_at) {
                        $updateResponse = $this->updateCus(
                            $membership,
                            $user,
                            $school,
                            $roleId,
                            $status
                        );
                        if (!$updateResponse) {
                            return $this->migrateToCus(
                                $user,
                                $school,
                                $roleId,
                                $status
                            );
                        }
                        return $membership;
                    }
                    return $this->migrateToCus(
                        $user,
                        $school,
                        $roleId,
                        $status
                    );
                }
            } else {
                if ($commonMembership) {
                    DB::table('schools_users')->updateOrInsert(
                        [
                            'school_id' => $school->id,
                            'user_id' => $user->id
                        ],
                        [
                            'role_id' => $roleId,
                            'membership_uuid' =>
                                $commonMembership->uuid ?? null,
                            'status' => $commonMembership->not_archived,
                            'deleted_at' => $commonMembership->archived_at
                        ]
                    );
                } else {

                    return $this->migrateToCus(
                        $user,
                        $school,
                        $roleId,
                        $status
                    );
                }
            }
        }

        if ($this->status == -2) {
            DB::table('schools_users')->updateOrInsert(
                [
                    'school_id' => $school->id,
                    'user_id' => $user->id
                ],
                [
                    'role_id' => $roleId,
                    'status' => 0,
                    'archived_by' => $this->adminUser->id,
                    'archived_at' => $this->updated_at,
                ]
            );
        }

        $user->searchable();
        return [
            'is_substitute' => $isSubs,
            'is_archived' => $this->status == -2
        ];
    }

    protected function migrateToCus($user, $school, $roleId, $status, $membership = null)
    {
        $client = new CommonUserService($this->adminUser);

        $roleCollection = CommonUser::getRoleName($roleId);

        $userData = [
            'email' => trim($user->email),
            'firstName' => $user->first_name != '' ? $user->first_name : '.',
            'lastName' => $user->last_name != '' ? $user->last_name : '.',
            'status' => $status,
            'password' => null
        ];

        //        if ($user->user_initials) {
        //            $userData['title'] = $user->user_initials;
        //        }

        $gradeYear = $user->grade_year;

        if (!$gradeYear && $roleId == 1) {
            $gradeYear = 0;
        }

        $sisId = $user->student_sis_id ?? 0;

        if ($membership) {
            $sisId = $membership->student_sis_id ?? $sisId;
        }

        $membershipData = [
            'status' => $status,
            'school' => $school->cus_uuid,
            'authClientId' => 2,
            'user' => $userData,
            'role' => $roleCollection[0],
            'graduationYear' => $gradeYear,
            'sisId' => $sisId,
            'sendConfirmationEmail' => false,
            'preventFromArchiving' => true
        ];

        $membership = $client->assignUserToSchool($membershipData);

        logger('MEMBERSHIP', [$membership]);
        if (isset($membership['id'])) {
            $userCommonId = CommonMembership::where(
                'id',
                $membership['id']
            )->first()->auth_user_id;

            DB::table('schools_users')->updateOrInsert(
                [
                    'school_id' => $school->id,
                    'user_id' => $user->id
                ],
                [
                    'membership_uuid' => $membership['uuid'],
                    'role_id' => $roleId,
                    'deleted_at' => null,
                    'status' => $status
                ]
            );

            $user->update([
                'cus_uuid' => CommonUser::find($userCommonId)->uuid,
                'initial_password' => null
            ]);
        }
        return [$membershipData, $membership];
    }

    protected function updateCus($membership, $user, $school, $roleId, $status, $commonMembership = null)
    {

        $client = new CommonUserService($this->adminUser);

        $roleCollection = CommonUser::getRoleName($roleId);

        $userData = [
            'uuid' => $user->cus_uuid,
            'email' => trim($user->email),
            'firstName' => $user->first_name != '' ? $user->first_name : '.',
            'lastName' => $user->last_name != '' ? $user->last_name : '.'
        ];
        $gradeYear = $user->grade_year;
        if (!$gradeYear && $roleId == 1) {
            $gradeYear = 0;
        }

        $membershipData = [
            'status' => $status,
            'school' => $school->cus_uuid,
            'authClientId' => 2,
            'user' => $userData,
            'role' => $roleCollection[0],
            'sisId' => $roleId == 1 ? $user->student_sis_id : null,
            'graduationYear' => $gradeYear
        ];

        $membershipUuid = $membership->membership_uuid;
        if($commonMembership){
            $membershipUuid = $membership->membership_uuid == $commonMembership->uuid ? $membership->membership_uuid : $commonMembership->uuid;
        }

        $response = $client->modifyMembershipUser(
            $membershipUuid,
            $membershipData
        );

        DB::table('schools_users')->updateOrInsert(
            [
                'school_id' => $school->id,
                'user_id' => $user->id
            ],
            [
                'membership_uuid' => $membershipUuid,
                'role_id' => $roleId,
                'deleted_at' => null,
                'status' => $status
            ]
        );

        $user->searchable();

        return !isset($response['status']);
    }

    public function getV2RoleId()
    {
        $roles = [
            0 => 6, //subs
            -2 => null, //none => null
            1 => 5, //superadmin => superadmin
            2 => 2, //admin => admin
            3 => 3, //teacher => teacher
            4 => 1, //student => student
            5 => 4, //staff => staff
            6 => 2, //schooladmin => admin
            7 => null, //system => null
            8 => null, //norole => null
            10 => null
        ];
        return $roles[$this->role_id];
    }

    public function isStudent()
    {
        return $this->role_id == 4;
    }
}
