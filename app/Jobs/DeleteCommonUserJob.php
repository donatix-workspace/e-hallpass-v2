<?php

namespace App\Jobs;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class DeleteCommonUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $users;

    public function __construct($users)
    {
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->users as $user) {
            $authUser = User::find($user->archived_by);
            $membership = DB::table('schools_users')
                ->where('user_id', $user->id)
                ->where('school_id', $user->archived_school)
                ->first();

            $client = new CommonUserService($authUser);

//            logger('DELETE USER', [
//                $user->id,
//                optional($membership)->membership_uuid
//            ]);

            $response = $client->deleteUser($membership->membership_uuid);

            if ($response) {
                $membership->update([
                    'deleted_at' => now(),
                    'status' => 0
                ]);

                $user->searchable();
            }
        }
    }
}
