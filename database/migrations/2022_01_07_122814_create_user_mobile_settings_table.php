<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMobileSettingsTable extends Migration
{
    public function up()
    {
        Schema::create('user_mobile_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('notification_settings');
            $table->unsignedBigInteger('user_id');
            $table->boolean('mobile_sound')->default(0);
            $table->timestamps();

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_mobile_settings');
    }
}
