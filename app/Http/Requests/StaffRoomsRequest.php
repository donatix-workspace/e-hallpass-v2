<?php

namespace App\Http\Requests;

use App\Rules\StaffRoomsPinRule;
use Illuminate\Foundation\Http\FormRequest;

class StaffRoomsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin() || auth()->user()->isStaff();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rooms' => 'required|string',
            'pins' => ['required', 'string', new StaffRoomsPinRule()]
        ];
    }
}
