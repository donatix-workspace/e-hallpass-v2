<?php

namespace App\Http\Controllers\Admin\Passes;

use App\Events\PassCanceled;
use App\Http\Controllers\Controller;
use App\Http\Resources\PassCollection;
use App\Http\Resources\PassResource;
use App\Models\Pass;
use App\Models\School;
use App\Models\User;
use App\Notifications\Push\StudentNotification;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

/**
 * @group Passes Admin functionality.
 */
class PassController extends Controller
{
    /**
     * Update pass actions for an admin.
     *
     * @bodyParam action integer required Pass approve action Example: 1 (APPROVE), 2 (END), 3 (Arrived), 4 (Returning/InOut)
     * @bodyParam return_type string Determine whether the user is returning or it's directly out Example: 'inout' OR 'return'
     * @urlParam pass integer required The id of the pass
     *
     * Approval Actions:
     * 1. Approve $approvalAction = 1;
     * 2. End/Keep $approvalAction = 2;
     * 3. Arrived $approvalAction = 3;
     * 4. In/Out & Returning $approvalAction = 4;
     *
     * @param Pass $pass
     * @param Request $request
     * @return \App\Http\Resources\PassResource|\Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Pass $pass, Request $request)
    {
        $request->validate([
            'action' => 'min:1|max:4|numeric|required',
            'return_type' => 'required_if:action,4|string|in:return,inout',
            'child' => 'sometimes|boolean'
        ]);

        $this->authorize('updateAdmin', $pass);

        $requireChild =
            $request->input('child') !== null
                ? $request->input('child')
                : false;

        $approveAction = $request->input('action');

        switch ($approveAction) {
            case Pass::ACTION_APPROVE:
                return $pass->approve();
            case Pass::ACTION_END:
                return $pass->end(null, null, null, $requireChild);
            case Pass::ACTION_ARRIVE:
                return $pass->arrive(null, null, false, true);
            case Pass::ACTION_RETURN_IN_OUT:
                return $pass->returningOrInOut(
                    null,
                    null,
                    $request->input('return_type'),
                    true
                );
        }
        return response()->json(
            [
                'data' => [],
                'message' => __('passes.update.invalid.action'),
                'status' => __('fail')
            ],
            422
        );
    }

    /**
     * Cancel a pass.
     *
     * @urlParam pass integer required The id of the pass.
     *
     * @authenticated
     * @param Pass $pass
     * @return PassResource|JsonResponse
     * @apiResourceModel App\Models\Pass
     * @apiResource App\Http\Resources\PassResource
     * @throws AuthorizationException
     */
    public function cancel(Pass $pass)
    {
        $this->authorize('cancelAdmin', $pass);

        if (
            $pass->isApproved() ||
            $pass->isCompleted() ||
            $pass->isCanceled() ||
            $pass->isExpired() ||
            $pass->isParent()
        ) {
            return response()->json(
                [
                    'data' => ['id' => $pass->id],
                    'message' => __('passes.cancel.fail'),
                    'status' => __('fail')
                ],
                422
            );
        }

        $pass->update([
            'pass_status' => Pass::INACTIVE,
            'canceled_at' => Carbon::now()
        ]);

        $pass->refreshDashboard();

        event(new PassCanceled($pass));
        //        Notification::send($pass, new StudentNotification('PassCanceled'));

        return new PassResource($pass->refresh());
    }

    /**
     * Get pass history of spec user.
     *
     * @urlParam user integer required The id of the user.
     *
     * @authenticated
     * @param User $user
     * @return PassCollection
     * @throws AuthorizationException
     */
    public function history(User $user): PassCollection
    {
        $this->authorize('historyAdmin', Pass::class);

        $schoolTimezone = optional(
            School::findCacheFirst($user->school_id)
        )->getTimezone();

        $startDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->startOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));
        $endDate = Carbon::parse(
            now()
                ->setTimezone($schoolTimezone)
                ->endOfDay(),
            $schoolTimezone
        )->setTimezone(config('app.timezone'));

        $passes = Pass::ofUserId($user->id)
            ->fromSchoolId(auth()->user()->school_id)
            ->whereParentId(null)
            ->without([
                'createdByUser',
                'completedByUser',
                'comments',
                'latestComment',
                'requestedByUser',
                'approvedByUser'
            ])
            ->whereBetween('created_at', [$startDate, $endDate])
            ->orderBy('created_at', 'desc')
            ->where(function ($query) {
                $query
                    ->where('pass_status', Pass::ACTIVE)
                    ->where('approved_at', '!=', null)
                    ->orWhere(function ($query) {
                        $query
                            ->where('completed_by', '!=', null)
                            ->orWhere('completed_at', '!=', null)
                            ->where('pass_status', Pass::INACTIVE);
                    });
            })
            ->get()
            ->append(['ended_at', 'signatures']);

        return new PassCollection($passes);
    }
}
