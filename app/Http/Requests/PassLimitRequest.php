<?php

namespace App\Http\Requests;

use App\Rules\DaysKeyExistRule;
use App\Rules\OnlyValidJsonYearValuesRule;
use App\Rules\PassLimitValidRule;
use App\Rules\RecurrenceDaysRule;
use App\Rules\RecurrenceMonthlyCorrectStructureRule;
use App\Rules\RecurrenceTypeRule;
use App\Rules\OnlyIntegersJsonValuesRule;
use Illuminate\Foundation\Http\FormRequest;

class PassLimitRequest extends FormRequest
{
    public function rules()
    {
        return [
            'limit_for' => ['required', 'string', new PassLimitValidRule()],
            'csv_file' =>
                'file|mimes:csv,txt,xlsx,doc,docx|required_if:limit_for,csv',
            'grade_years' => [
                'json',
                'required_if:limit_for,gradeyear',
                new OnlyValidJsonYearValuesRule()
            ],
            'selected_student_ids' => [
                'required_if:limit_for,student',
                'json',
                new OnlyIntegersJsonValuesRule()
            ],
            'from_date' => 'date|required|date_format:m/d/Y',
            'to_date' =>
                'date|required|after_or_equal:from_date|date_format:m/d/Y',
            'limit' => 'required|integer|min:0',
            'reason' => 'max:255|sometimes|nullable',
            'recurrence_type' => ['nullable', new RecurrenceTypeRule()]
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() &&
            auth()
                ->user()
                ->isAdmin();
    }
}
