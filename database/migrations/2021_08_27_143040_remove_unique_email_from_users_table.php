<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class RemoveUniqueEmailFromUsersTable extends Migration
{
    public function up()
    {
        Schema::table("users", function (Blueprint $table) {
            if (Schema::hasColumn("users", "users_email_unique")) {
                $foreignExists = DB::select(
                    DB::raw(
                        'SHOW KEYS FROM users WHERE Key_name=\'users_email_unique\''
                    )
                );

                if (count($foreignExists)) {
                    $table->dropUnique("users_email_unique");
                }
            }
        });
    }

    public function down()
    {
        Schema::table("users", function (Blueprint $table) {
            $table
                ->string("email")
                ->unique()
                ->change();
        });
    }
}
