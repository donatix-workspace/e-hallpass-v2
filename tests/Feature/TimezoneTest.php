<?php

namespace Tests\Feature;

use App\Models\Pass;
use App\Models\Pin;
use App\Models\School;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TimezoneTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $secondSchool;
    protected $adminUser;
    protected $firstUser;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        $this->school = School::factory()->createOne();
        $this->secondSchool = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->secondSchool->id
        ]);
    }

    /**
     * @test
     * A users are in correct timezone
     * @return void
     */
    public function a_users_are_in_correct_timezone()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.users.timezone'))
            ->assertJson(['data' => ['timezone' => $this->secondSchool->timezone]]);
        $this->actingAs($this->firstUser)
            ->get(route('api.v5.users.timezone'))
            ->assertJson(['data' => ['timezone' => $this->school->school]]);

        $this->assertNotEquals($this->school->timezone, $this->secondSchool->timezone);
    }

    /**
     * @test
     * Assert record are in correct timezone
     * @return void
     */
    public function assert_record_are_in_correct_timezone()
    {
        $pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->firstUser->id,
            'user_id' => $this->firstUser->id,
            'completed_at' => Carbon::now(optional($this->school)->getTimezone())
        ]);

        $this->assertEquals(
            Carbon::now(optional($this->school)->getTimezone())->tzName,
            $pass->completed_at->tzName
        );

        $this->assertEquals(
            Carbon::now(optional($this->school)->getTimezone())->tzName,
            $pass->created_at->tzName
        );
    }
}
