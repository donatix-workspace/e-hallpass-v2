<?php

namespace App\Http\Middleware;

use App\Models\Module;
use App\Models\School;
use Closure;
use Illuminate\Http\Request;

class ModuleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param $moduleName
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $moduleName)
    {
        $user = auth()->user();
        $school = School::findOrFail($user->school_id);
        if ($school->hasModule($moduleName)) {
            return $next($request);
        }

        return response()->json([
            'data' => ['school_id' => optional($school)->id],
            'message' => __('school.dont.have.permission'),
            'status' => __('fail')
        ], 403);
    }
}
