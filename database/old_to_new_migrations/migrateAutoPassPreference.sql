SET FOREIGN_KEY_CHECKS  = 0;
START TRANSACTION ;
TRUNCATE ehp_v2_prod.auto_passes_preferences;
INSERT INTO ehp_v2_prod.auto_passes_preferences (auto_pass_id, user_id, school_id, status, mode, created_at, updated_at)  SELECT
    (SELECT id FROM ehp_v2_prod.auto_passes WHERE auto_passes.room_id = (SELECT id FROM ehp_v2_prod.rooms WHERE old_id = oldAp.room_id limit 1) LIMIT 1),
    (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldAp.teacher_id limit 1),
    (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldAp.school_id limit 1),
    (SELECT oldAp.status),
    (SELECT IF(oldAp.mode >= 1,'start_and_stop', 'start_only')),
    (SELECT CONVERT_TZ(oldAp.created_at,'America/New_York', 'UTC')),
    (SELECT CONVERT_TZ(oldAp.updated_at,'America/New_York', 'UTC'))
FROM ehallpassDB.auto_pass_preference as oldAp;


UPDATE
   ehp_v2_prod.auto_passes_preferences
SET
    user_id = (
        SELECT
            to_id
        FROM
            duplicate_users
        WHERE
            from_id = user_id
        LIMIT 1)
WHERE
 user_id IN(
        SELECT
            from_id FROM duplicate_users);
