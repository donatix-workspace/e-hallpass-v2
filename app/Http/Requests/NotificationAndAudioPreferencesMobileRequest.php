<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationAndAudioPreferencesMobileRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'notification_settings' => 'json|nullable',
            'mobile_sound' => 'required|boolean'
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
