<?php

namespace App\Traits;

trait TransparencyMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'id' => [
                'type' => 'long'
            ],
            'school_id' => [
                'type' => 'long'
            ],
            'user_id' => [
                'type' => 'long'
            ],
            'user' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ]
        ],
    ];
}
