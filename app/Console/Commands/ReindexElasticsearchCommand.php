<?php

namespace App\Console\Commands;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use ScoutElastic\Facades\ElasticClient;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\Output;

class ReindexElasticsearchCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'es:reindex {--force} {--indexes=} {--c|chunk=} {--offset=} {--condition=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create all indexes and import data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $indexes = [
            'appointment_passes_index',
            'location_capacities',
            'pass_limits',
            'passes_index',
            'polarities',
            'room_restrictions',
            'rooms_index',
            'transparencies_index',
            'unavailables',
            'users_index',
            'pass_block',
            'polarity_reports',
            'recurrence_appointment_passes',
            'summary_report',
            'contact_tracing'
        ];
        if ($this->option('indexes')) {
            $indexes = explode(',', $this->option('indexes'));
        }
        $chunk = $this->option('chunk');
        $chunk = $chunk ?? config('scout.chunk.unsearchable');

        $offset = intval($this->option('offset')) ?: 0;
        $condition = $this->option('condition');

        foreach ($indexes as $index) {
            if ($this->option('force')) {
                $confirmationForFullReindex = $this->confirm(
                    'You are going to run full reindex and all of the data will be loose. Do you want to continue'
                );

                if ($confirmationForFullReindex) {
                    try {
                        $responses[] = ElasticClient::indices()->delete([
                            'index' => [$index]
                        ]);
                        $this->info('Force re-index: ' . $index);
                    } catch (Missing404Exception $exception) {
                        logger()->debug('Missing Index ', [
                            $exception->getMessage()
                        ]);
                    }
                }
            }

            $indexDefinition = config("scout_elastic.es_indexes.{$index}");

            if ($this->option('force')) {
                try {
                    $this->call('elastic:create-index', [
                        'index-configurator' => $indexDefinition['configurator']
                    ]);
                } catch (BadRequest400Exception $e) {
                    logger($e->getMessage());
                }
            }

            try {
                $indexDefinition['model']::enableMassIndex();

                $this->call('elastic:update-mapping', [
                    'model' => $indexDefinition['model']
                ]);
                $conditions = explode(',', $condition);

                if (!$condition) {
                    $conditions = null;
                }
                $indexDefinition['model']::bulkImport(
                    $this,
                    $chunk,
                    $offset,
                    $conditions
                );
            } catch (BadRequest400Exception $e) {
                logger($e->getMessage());
            }
            $indexDefinition['model']::disableMassIndex();
        }

        return 0;
    }
}
