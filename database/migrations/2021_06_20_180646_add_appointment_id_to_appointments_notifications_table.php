<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAppointmentIdToAppointmentsNotificationsTable extends Migration
{
    public function up()
    {
        Schema::table('appointments_notifications', function (Blueprint $table) {
            $table->dropColumn('payload');
            $table->unsignedBigInteger('appointment_pass_id')->nullable();
            $table->foreign('appointment_pass_id')->references('id')->on('appointment_passes')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('appointments_notifications', function (Blueprint $table) {
            //
        });
    }
}
