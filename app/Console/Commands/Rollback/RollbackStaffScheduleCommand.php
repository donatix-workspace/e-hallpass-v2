<?php

namespace App\Console\Commands\Rollback;

use App\Models\Room;
use Illuminate\Console\Command;

class RollbackStaffScheduleCommand extends Command
{
    protected $signature = 'ehpv1_rollback:staff_schedule';

    protected $description = 'Rollback room pins from EhpV2 to EhpV1';

    public function handle(): int
    {
        $this->info('--- RollBack Room pins From EhpV1 to EhpV2 ---');

        $newDb = DB::connection();
        $oldDb = DB::connection('oldDB');

        $newDb->disableQueryLog();
        $oldDb->disableQueryLog();

        $pins = $newDb->table('pins')
            ->where('pinnable_tyle', Room::class)
            ->orderBy('id', 'desc')
            ->get()
            ->toArray();

        $this->output->progressStart(count($pins));

        $pinsChunk = array_chunk($pins, 1500);

        foreach ($pinsChunk as $pinChunk) {
            $insertablePins = [];
            foreach ($pinChunk as $pin) {
                $newRoom = $newDb->table('rooms')->where('id', $pin->pinnable_id)->select('old_id')->first();
                $newSchool = $newDb->table('schools')->where('id', $pin->school_id)->select('id', 'timezone', 'old_id')->first();

                if ($newSchool) {
                    $insertablePins[] = [
                        'teacher_id' => $newRoom->old_id,
                        'pin' => $pin->pin,
                        'school_id' => $newSchool->old_id,
                        'created_at' => now(),
                        'updated_at' => now()
                    ];
                }
            }

            try {
                DB::beginTransaction();
                $oldDb->table('room_pins')
                    ->insert($insertablePins);
                DB::commit();
                $this->output->progressAdvance(count($insertablePins));
            } catch (\Throwable $e) {
                $this->info($e->getMessage());
            }
        }

        $this->output->progressFinish();

        return 0;
    }
}
