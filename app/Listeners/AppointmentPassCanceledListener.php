<?php

namespace App\Listeners;

use App\Events\AppointmentPassCancelled;
use App\Models\RecurrenceAppointmentPass;
use App\Models\AppointmentNotification;
use DB;

class AppointmentPassCanceledListener
{
    public function __construct()
    {
    }

    public function handle(AppointmentPassCancelled $event)
    {
        if(!$event->appointment instanceof RecurrenceAppointmentPass) {
          $appointmentPassNotification = AppointmentNotification::create([
                'notification_type' =>  AppointmentNotification::CANCELED,
                'appointment_pass_id' => $event->appointment->id,
                'user_id' => $event->appointment->user_id,
                'school_id' => $event->appointment->school_id,
                'updated_by_id' =>$event->newUserId
            ]);
        }
    }
}
