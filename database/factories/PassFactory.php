<?php

namespace Database\Factories;

use App\Models\Pass;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PassFactory extends Factory
{
    protected $model = Pass::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'parent_id' => $this->faker->randomNumber(),
            'from_teacher_note' => $this->faker->word,
            'from_type' => $this->faker->word,
            'from_id' => $this->faker->randomNumber(),
            'to_type' => $this->faker->word,
            'to_id' => $this->faker->randomNumber(),
            'ordering' => $this->faker->randomNumber(),
            'to_teacher_note' => $this->faker->word,
            'pass_status' => $this->faker->randomNumber(),
            'approved_with' => $this->faker->word,
            'ended_with' => $this->faker->word,
            'approved_at' => Carbon::now(),
            'completed_at' => Carbon::now(),
            'requested_at' => Carbon::now(),
            'arrived_at' => Carbon::now(),
            'type' => $this->faker->word,
            'expired_at' => Carbon::now(),
            'canceled_at' => $this->faker->word,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'user_id' => function () {
                return User::factory()->create()->id;
            },
            'school_id' => function () {
                return School::factory()->create()->id;
            },
            'requested_by' => function () {
                return User::factory()->create()->id;
            },
            'approved_by' => function () {
                return User::factory()->create()->id;
            },
            'completed_by' => function () {
                return User::factory()->create()->id;
            },
            'created_by' => function () {
                return User::factory()->create()->id;
            },
        ];
    }
}
