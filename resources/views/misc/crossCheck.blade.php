<!DOCTYPE html>
<html lang="en">
<head>
    <title>Cross check | E-Hallpass V2</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="inset-0">
<div class="flex flex-nowrap justify-center justify-around  m-8">
    <div class="container m-0">
        <div class="grid grid-cols-3 gap-3">

            <div class="box-border shadow-lg rounded border-solid w-128 h=128 bg-cyan-500">
                <div class="content pl-6 pt-2 pr-6">
                    <h1 class="text-stone-100">Actions</h1>

                    <br>
                    <p class="mt-3 mb-6">
                        <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                           href="{{route('api.v5.crosscheck.verify',[$schoolId,1])}}">Mark Ready</a>

                        <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                           href="{{route('api.v5.crosscheck.verify',[$schoolId,0])}}">Mark Not Ready</a>

                    </p>
                    <p class="mt-5 mb-6">
                        <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                           href="{{route('api.v5.crosscheck.migrateAll',[$schoolId])}}">Migrate All</a>
                    </p>
                    @if($adminDiffsV2)
                        <p class="mt-5 mb-6">
                            <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                               href="{{route('api.v5.crosscheck.deleteAll',[$schoolId,$adminDiffsV2->pluck('id')])}}">Delete
                                Extra in V2</a>
                        </p>
                    @endif
                    @if($cusDiffs)
                    <p class="mt-5 mb-6">
                        <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                           href="{{route('api.v5.crosscheck.sync',[$schoolId,$cusDiffs->pluck('cus_uuid')])}}">Sync
                            Extra</a>
                    </p>
                    <p class="mt-5 mb-6">
                        <a class="bg-transparent py-2 px-4 border border-blue-500 rounded"
                           href="{{route('api.v5.crosscheck.deleteAllCus',[$schoolId,$cusDiffs->pluck('cus_uuid')])}}">Delete
                            From CUS</a>
                    </p>
                    @endif

                </div>
            </div>
            <div class="box-border shadow-lg rounded border-solid w-128 h=128 bg-cyan-500">
                <div class="content pl-6 pt-2 pr-6">
                    <table class="text-stone-100">
                        <tr class="border-b">
                            <th class="text-sm px-3 py-1 whitespace-nowrap border-r">Role</th>
                            <th class="text-sm px-3 py-1 whitespace-nowrap border-r">V1</th>
                            <th class="text-sm px-3 py-1 whitespace-nowrap border-r">V2</th>
                            <th class="text-sm px-3 py-1 whitespace-nowrap border-r">CUS</th>
                        </tr>
                        <tr class="border-b">
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                Admins
                            </td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$v1Counters->admin}}</td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$v2Counters->admin}}</td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$cusCounters->admin}}</td>
                        </tr>
                        <tr class="border-b">
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                Teachers
                            </td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$v1Counters->teacher}}</td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$v2Counters->teacher}}</td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$cusCounters->teacher}}</td>
                        </tr>
                        <tr class="border-b">
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                Staff
                            </td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$v1Counters->staff}}</td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$v2Counters->staff}}</td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$cusCounters->staff}}</td>
                        </tr>
                        <tr class="border-b">
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                Students
                            </td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$v1Counters->student}}</td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$v2Counters->student}}</td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$cusCounters->student}}</td>
                        </tr>
                        <tr class="border-b">
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                Totals
                            </td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$v1Counters->student + $v1Counters->staff +  $v1Counters->teacher +  $v1Counters->admin}}</td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$v2Counters->student + $v2Counters->staff +  $v2Counters->teacher +  $v2Counters->admin}}</td>
                            <td class="text-sm px-3 py-1 whitespace-nowrap border-r">
                                {{$cusCounters->student + $cusCounters->staff +  $cusCounters->teacher +  $cusCounters->admin}}</td>
                        </tr>
                    </table>

                    {{--                <p class="text-stone-200">Admins: <b class="text-stone-300">{{ $v1Counters->admin }}</b></p>--}}
                    {{--                <p class="text-stone-200">Teachers: <b class="text-stone-300">{{$v1Counters->teacher}}</b></p>--}}
                    {{--                <p class="text-stone-200">Staff: <b class="text-stone-300">{{$v1Counters->staff}}</b> </p>--}}
                    {{--                <p class="text-stone-200">Students: <b class="text-stone-300">{{$v1Counters->student}}</b></p>--}}

                </div>
            </div>
            <div class="box-border shadow-lg rounded border-solid w-128 h=128 bg-cyan-500">
                <div class="content pl-6 pt-2 pr-6">
                    <h1 class="text-gray">{{$schoolName}}</h1>
                    <hr class="mt-2"/>
                    <p class="text-stone-200">{{$schoolCus}}</p>
                    <hr>
                    <p class="text-stone-200">{{$state}}  {{$zip}}</p>
                    <hr>
                    <p class="text-stone-200">
                        @foreach($domains as $domain)
                            {{$domain->name}} |
                        @endforeach
                    </p>
                    <hr>
                    <p class="text-stone-200">V1 ID: <strong>{{$schoolOldId}}</strong> V2 Id
                        <strong>{{ $schoolId }}</strong></p>
                    <p class="text-stone-200">STATUS: {{$schoolStatus ? 'Active' : 'Inactive'}}  </p>
                    <p>
                        <img width="100" src="{{$logo}}" alt="">
                    </p>
                </div>
            </div>

            {{--        <div class="box-border shadow-lg rounded border-solid w-128 h=128 bg-cyan-500 text-center">--}}
            {{--            <div class="content pl-6 pt-2 pr-6">--}}
            {{--                <h1 class="text-stone-100">Counters <a href="#" class="underline decoration-zinc-500 text-stone-300">CUS</a> </h1>--}}

            {{--                <hr class="mt-2"/>--}}



            {{--            </div>--}}
            {{--        </div>--}}
        </div>
    </div>
</div>
<hr/>


<!-- V1 -->
<div class="flex flex-nowrap justify-center m-2">
    <div class="container m-0">
        <div class="grid grid-cols-1 gap-2">
            <div class="box-border shadow-lg rounded border-solid w-128 h-128 bg-cyan-500 ">
                <div class="content pl-1 pr-1">
                    <h1 class="text-stone-300">Existing in (V1) but not in (V2)</h1>

                    <hr class="mt-2"/>
                    <table class="table-auto rounded bg-slate-600 min-w-full text-white text-left">
                        <thead class="bg-slate-500">
                        <tr>
                            <th>Email</th>
                            <th>Status number</th>
                            <th>Clever ID</th>
                            <th>Role</th>
                            <th>Student Sis</th>
                            <th>Id In V1</th>
                            <th>Auth Type Number:</th>
                            <th>Created:</th>
                            <th>Actions:</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($adminDiffs as $diff)
                            <tr>
                                <td>{{$diff->email}}</td>
                                <td>{{$diff->status}}</td>
                                <td>{{$diff->clever_id}}</td>
                                <td>{{getRoleNameByIdFromV1($diff->role_id)}}</td>
                                <td>{{ $diff->student_sis_id }}</td>
                                <td>{{ $diff->user_id}}</td>
                                <td>{{ $diff->auth_type}}</td>
                                <td>{{ $diff->created_at}}</td>
                                <td>
                                    <a href="{{route('api.v5.crosscheck.migrate',[$schoolId,$diff->user_id])}}">Migrate</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="results m-10">
    @if (session()->has('message'))
        <div class="flex flex-nowrap justify-center m-2">
            {!! json_encode(session()->get('message')) !!}
        </div>
    @endif
</div>

<hr/>

<!-- V2 -->
<div class="flex flex-nowrap justify-center content-center m-2">
    <div class="container m-0">
        <div class="grid grid-cols-1 gap-2">
            <div class="box-border shadow-lg rounded border-solid w-128 h-128 bg-cyan-500 text-center">
                <div class="content pl-1 pr-1">
                    <h1 class="text-stone-300">Existing in (V2) but not in (V1)</h1>

                    <hr class="mt-2"/>
                    <table class="table-auto rounded bg-slate-600 min-w-full text-white text-left">
                        <thead class="bg-slate-500">
                        <tr>
                            <th>Email</th>
                            <th>Status number</th>
                            <th>Student Sis</th>
                            <th>Role</th>
                            <th>Id In V1</th>
                            <th>Id In V2</th>
                            <th>Auth Type Number:</th>
                            <th>CUS UUID:</th>
                            <th>Created:</th>
                            <th>Action:</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($adminDiffsV2 as $diff)
                            <tr>
                                <td>{{$diff->email}}</td>
                                <td>{{$diff->status}}</td>
                                <td>{{$diff->student_sis_id}}</td>
                                <td>{{\App\Models\Role::getRoleNameById($diff->role_id)}}</td>
                                <td>{{ $diff->old_id }}</td>
                                <td>{{ $diff->id}}</td>
                                <td>{{ $diff->auth_type}}</td>
                                <td>{{ $diff->cus_uuid === null ? 'Not existing' : $diff->cus_uuid}}</td>
                                <td>{{ $diff->created_at}}</td>
                                <td>
                                    <a href="{{route('api.v5.crosscheck.delete',[$schoolId,$diff->id])}}">Delete</a>
                                    <a href="{{route('api.v5.crosscheck.syncwithv1',[$schoolId,$diff->id])}}">Sync</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CUS -->
<div class="flex flex-nowrap justify-center content-center m-2">
    <div class="container m-0">
        <div class="grid grid-cols-1 gap-2">
            <div class="box-border shadow-lg rounded border-solid w-128 h-128 bg-cyan-500 text-center">
                <div class="content pl-1 pr-1">
                    <h1 class="text-stone-300">Existing in CUS but not in V2</h1>

                    <hr class="mt-2"/>
                    <table class="table-auto rounded bg-slate-600 min-w-full text-white text-left">
                        <thead class="bg-slate-500">
                        <tr>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Student Sis</th>
                            <th>Role</th>
                            <th>ID</th>
                            <th>Membership UUID</th>
                            <th>Created</th>
                            <th>Deleted</th>
                            <th>Action:</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cusDiffs as $diff)
                            <tr>
                                <td>{{$diff->email}}</td>
                                <td>{{$diff->status}}</td>
                                <td>{{$diff->student_sis_id}}</td>
                                <td>{{$diff->role_id}}</td>
                                <td>{{ $diff->id}}</td>
                                <td>{{ $diff->cus_uuid}}</td>
                                <td>{{ $diff->created_at}}</td>
                                <td>{{ $diff->deleted_at}}</td>
                                <td>
                                    <a href="{{route('api.v5.crosscheck.deleteMembership',[$schoolId,$diff->cus_uuid])}}">Delete</a>
                                    <br>
                                    <a href="{{route('api.v5.crosscheck.sync',[$schoolId,$diff->cus_uuid])}}">Sync</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

