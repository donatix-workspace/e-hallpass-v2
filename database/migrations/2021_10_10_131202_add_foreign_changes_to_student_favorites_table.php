<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignChangesToStudentFavoritesTable extends Migration
{
    public function up()
    {
        Schema::table('student_favorites', function (Blueprint $table) {
            $table->dropForeign('student_favorites_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('student_favorites', function (Blueprint $table) {
            //
        });
    }
}
