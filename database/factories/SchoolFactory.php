<?php

namespace Database\Factories;

use App\Models\School;
use Illuminate\Database\Eloquent\Factories\Factory;

class SchoolFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = School::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cus_uuid' => $this->faker->uuid(),
            'district_uuid' => $this->faker->uuid(),
            'name' => 'Example School ' . $this->faker->numberBetween(1, 100),
            'description' => $this->faker->realText(100),
            'logo' => $this->faker->filePath(),
            'student_domain' => $this->faker->domainName,
            'school_domain' => $this->faker->domainName,
            'status' => 1,
            'timezone' => 'America/New_York',
            'message' => $this->faker->realText,
            'created_at' => now(),
            'updated_at' => now(),
            'timezone_offset' => config('timezones.offsets.America/New_York'),
            'old_id' => null,
            'show_student_photos' => 1,
            'auth_providers' => null,
        ];
    }
}
