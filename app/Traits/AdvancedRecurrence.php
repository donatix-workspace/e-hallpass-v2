<?php

namespace App\Traits;

use Carbon\Carbon;

trait AdvancedRecurrence
{
    /**
     * @return bool
     */
    public function isDaily()
    {
        $endRecurrenceDate = Carbon::parse($this->recurrence_end_at);
        $today = Carbon::today()->dayOfWeek;

        return $this->recurrence_type === config('recurrences.daily') &&
            !in_array($today, [Carbon::SATURDAY, Carbon::SUNDAY]) &&
            $this->from_date->isPast() &&
            $this->to_date->isPast() &&
            !$endRecurrenceDate->isPast();
    }

    /**
     * @return bool
     */
    public function isWeekly()
    {
        $endRecurrenceDate = Carbon::parse($this->recurrence_end_at);

        $recurrenceDays = collect(
            json_decode($this->recurrence_days, true)
        )->get('days');

        return $this->recurrence_type === config('recurrences.weekly') &&
            in_array(Carbon::now()->englishDayOfWeek, $recurrenceDays) &&
            !$endRecurrenceDate->isPast();
    }

    /**
     * @return bool
     */
    public function isMonthly()
    {
        $endRecurrenceDate = Carbon::parse($this->recurrence_end_at);
        $recurrenceWeek = json_decode($this->recurrence_week, true);
        $currentWeekNumber = Carbon::now()->weekNumberInMonth;
        $currentDay = Carbon::now()->englishDayOfWeek;

        if ($recurrenceWeek === null) {
            return false;
        }

        return $this->recurrence_type === config('recurrences.monthly') &&
            array_key_exists($currentWeekNumber, $recurrenceWeek) &&
            $recurrenceWeek[$currentWeekNumber] === $currentDay &&
            $this->from_date->isPast() &&
            $this->to_date->isPast() &&
            !$endRecurrenceDate->isPast();
    }

    /**
     * @return void
     */
    public function updateByRecurrence()
    {
        $recurrenceTypeExpired = false;
        $todayDate = Carbon::now()->format('Y-m-d');
        $toDateTime = $this->to_date->format('H:i:s');
        $fromDateTime = $this->from_date->format('H:i:s');

        if (Carbon::parse($this->recurrence_end_at)->isToday()) {
            $recurrenceTypeExpired = true;
        }

        $updatedToDate = Carbon::parse("$todayDate $toDateTime")->format(
            'Y-m-d H:i:s'
        );

        $this->update([
            'from_date' => "$todayDate $fromDateTime",
            'recurrence_type' => $recurrenceTypeExpired
                ? null
                : $this->recurrence_type,
            'to_date' => $updatedToDate,
            'canceled_for_today_at' => null
        ]);
    }
}
