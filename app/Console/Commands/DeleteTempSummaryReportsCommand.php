<?php

namespace App\Console\Commands;

use App\Models\PassSummaryReport;
use Illuminate\Console\Command;

class DeleteTempSummaryReportsCommand extends Command
{
    protected $signature = 'summary:clean';

    protected $description = 'Clean temp summary reports that are created today.';

    public function handle()
    {
        PassSummaryReport::whereDate('created_at', today())
            ->limit(100000)
            ->where('search_session_id', '!=', null)
            ->delete();

        return 1;
    }
}
