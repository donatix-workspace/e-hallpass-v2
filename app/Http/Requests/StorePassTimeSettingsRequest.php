<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePassTimeSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->isAdmin();;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'min_time' => 'required|date_format:"H:i:s"|before:auto_expire|before:extended_pass',
            'white_pass' => 'required',
            'auto_expire_time' => 'required|date_format:"H:i:s"|before:extended_pass',
            'nurse_auto_expire_time' => 'required|date_format:"H:i:s"',
            'awaiting_apt' => 'required|date_format:"H:i:s"',
        ];
    }
}
