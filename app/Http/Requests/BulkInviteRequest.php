<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BulkInviteRequest extends FormRequest
{
    public function rules()
    {
        return [
            'users' => 'required|array',
            'users.*' => 'required|integer|exists:users,id'
        ];
    }
}
