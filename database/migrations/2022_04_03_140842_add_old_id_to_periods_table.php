<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOldIdToPeriodsTable extends Migration
{
    public function up()
    {
        Schema::table('periods', function (Blueprint $table) {
            $table->integer('old_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('periods', function (Blueprint $table) {
            $table->dropColumn('old_id');
        });
    }
}
