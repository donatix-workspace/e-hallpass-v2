<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\EntityService;
use App\Http\Services\CommonUserService\Models\CommonDistrict;
use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonSchoolDomain;
use App\Http\Services\CommonUserService\Models\CommonTimezone;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Models\SchoolsEhp;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CancelAndUpdateAppointmentsAfterArchiveJob;
use App\Jobs\CreateCommonUserJob;
use App\Jobs\UpdateCommonUserJob;
use App\Models\AuthType;
use App\Models\Domain;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\SchoolUser;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use function PHPUnit\Framework\isEmpty;

class FixCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'cus:fix {--allSchoolModules} {--modules=} {--stickyRoles=} {--createMissingMemberships=} {--fixMissingMemberships} {--fixAllMissingSchoolLogos} {--fixArchivedMemberships} {--deactivateSchools} {--recreateUserCUS}  {--deleteMissingFromV1=} {--recreateMissingInV2} {--checkMissingFnLnUsersInV2} {--rolesFix=} {--titlesFix} {--fixSchoolsSftp} {--fixSchoolsSftpCompare} {--clearStatuses=} {--duplicatedCUS} {--dupMem=} {--schoolDiffDomains=} {--fixWrongEmails} {--rolesCusFix=} {--duplicateMemberships=} {--onMembership=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix School and User Details from V1';

    protected $client;
    protected $oldDb;
    protected $adminUser;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->oldDb = DB::connection('oldDB');

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->adminUser = User::where('email', 'viktor.lalev@donatix.net')
            ->latest()
            ->first();
        $this->client = new CommonUserService($this->adminUser);

        if ($this->option('modules')) {
            $this->info('Fix School Modules');
            $this->modules($this->option('modules'));
        }
        if ($this->option('allSchoolModules')) {
            $this->info('Fix School Modules For All Schools');
            $schools = \App\Models\V1\School::where('status', 1)->get();
            $this->output->progressStart($schools->count());
            foreach ($schools as $school) {
                $school->migrateModules();
                $this->output->progressAdvance();
            }
        }

        if ($this->option('fixAllMissingSchoolLogos')) {
            $this->info('Fix School Logos');
            $this->fixAllMissingSchoolLogos();
        }
        if ($this->option('stickyRoles')) {
            $this->info('Fix User Sticky Roles per school');
            if ($this->option('stickyRoles') == 'all') {
                foreach (School::where('status', 1)->get() as $school) {
                    $this->stickyRoles($school->id);
                }
            }

            $this->stickyRoles($this->option('stickyRoles'));
        }
        if ($this->option('createMissingMemberships')) {
            $this->info('Fix Missing Memberships in CUS');
            $this->createMissingMemberships(
                $this->option('createMissingMemberships')
            );
        }

        if ($this->option('fixMissingMemberships')) {
            $this->info('Fix Missing Memberships in CUS');
            $ids = [];
            //            foreach (School::whereNotNull('cus_uuid')->get()->pluck('id') as $schoolId) {
            foreach ($ids as $schoolId) {
                $this->createMissingMemberships($schoolId);
            }
        }
        if ($this->option('fixArchivedMemberships')) {
            $this->info('Fix Duplicated Memberships from CUS');
            $this->fixArchivedMemberships();
        }
        if ($this->option('deactivateSchools')) {
            $this->info('Sync schools from v1');
            $this->deactivateSchools();
        }
        if ($this->option('recreateUserCUS')) {
            $this->info('Recreate duplicate cus uuids');
            $this->recreateUserCUS();
        }

        if ($this->option('deleteMissingFromV1')) {
            $this->info('Removing users from V2.');
            $this->deleteMissingFromV1($this->option('deleteMissingFromV1'));
        }
        if ($this->option('recreateMissingInV2')) {
            $this->info('Recreate missing membershups in v2');
            $this->recreateMissingInV2();
        }

        if ($this->option('checkMissingFnLnUsersInV2')) {
            $this->info('Cleaning empty users');
            $this->checkMissingFirstNameLastNameUsersInV2();
        }
        if ($this->option('rolesFix')) {
            $this->info('Fix Roles');

            $schools = School::where('id', $this->option('rolesFix'))->where(
                'status',
                1
            );

            $count = $schools->count();

            $this->info("Fix Roles for {$count} schools");

            foreach ($schools->get() as $school) {
                $this->info("Fix Roles for {$school->name} {$school->id}");

                $this->rolesFix($school);
            }
        }
        if ($this->option('rolesCusFix')) {
            $this->info('Fix CUS Roles');

            $schools = School::where('id', $this->option('rolesCusFix'))->where(
                'status',
                1
            );

            $count = $schools->count();

            $this->info("Fix Roles for {$count} schools");

            foreach ($schools->get() as $school) {
                $this->info("Fix Roles for {$school->name} {$school->id}");

                $this->rolesCusFix($school);
            }
        }

        if ($this->option('titlesFix')) {
            $this->info('Fix Titles');

            $schoolIds = DB::table('verified_schools')
                ->orderBy('school_id')
                ->get()
                ->pluck('school_id');

            $schools = School::where('status', 1)->whereNotIn('id', $schoolIds);

            $count = $schools->count();

            $this->info("Fix Titles for {$count} schools");

            foreach ($schools->get() as $school) {
                $this->info("Fix Titles for {$school->name} {$school->id}");

                $this->titlesFix($school);
            }
        }
        if ($this->option('clearStatuses')) {
            $this->info('Clear v2 > v1 satuses');

            $school = School::where(
                'id',
                $this->option('clearStatuses')
            )->first();
            if ($school) {
                $this->clearStatuses($school);
            }
        }

        if ($this->option('fixSchoolsSftpCompare')) {
            $this->info('Rollback sftp folreds');
            $this->fixSchoolsSftpCompare();
        }
        if ($this->option('fixSchoolsSftp')) {
            $this->info('Rollback sftp folreds');
            $this->fixSchoolsSftp();
        }
        if ($this->option('duplicatedCUS')) {
            $this->info('Fix duplicated CUS');
            $this->duplicatedCUS();
        }
        if ($this->option('dupMem')) {
            $this->info('Fix duplicated memberships');
            $onMembership = $this->option('onMembership') ?? false;

            $this->dupMem($this->option('dupMem'), !!$onMembership);
        }

        if ($this->option('schoolDiffDomains')) {
            $this->info('Adding ids with wrong emails on the schools');
            $this->getUsersWithDifferentDomains();
        }

        if ($this->option('fixWrongEmails')) {
            $this->info(
                'Removing wrong membership school domains from the users'
            );
            $this->deleteTheUsersWithDifferentEmails();
        }

        return 0;
    }

    /**
     * @return int
     */
    public function deleteTheUsersWithDifferentEmails(): int
    {
        $invalidUserIdsAndSchoolIds = DB::table(
            'invalid_emails_on_schools_users'
        )
            ->select(['user_id', 'school_id'])
            ->get();

        $this->info('Start fixing the wrong memberships from user emails...');

        $this->output->progressStart($invalidUserIdsAndSchoolIds->count());

        foreach ($invalidUserIdsAndSchoolIds as $invalidUserAndSchoolId) {
            $memberShip = SchoolUser::whereUserId(
                $invalidUserAndSchoolId->user_id
            )
                ->whereSchoolId($invalidUserAndSchoolId->school_id)
                ->first();

            if ($memberShip->membership_uuid !== null) {
                $this->client->deleteUser($memberShip->membership_uuid);
            }

            $memberShip->update([
                'deleted_at' => now(),
                'status' => 0
            ]);

            $this->output->progressAdvance();
        }

        $this->output->progressFinish();

        return 0;
    }

    /**
     * @return false|int
     */
    public function getUsersWithDifferentDomains()
    {
        $fromAndToId = explode(',', $this->option('schoolDiffDomains'));

        $schools = School::where('id', '>', $fromAndToId[0])
            ->with('domains:name')
            ->where('id', '<=', $fromAndToId[1])
            ->cursor();

        foreach ($schools as $school) {
            if (!$school) {
                return false;
            }

            $users = DB::table('schools_users as m')
                ->join('users as u', 'm.user_id', '=', 'u.id')
                ->select([
                    'u.email',
                    //    'u.status',
                    // 'u.student_sis_id',
                    // 'u.cus_uuid',
                    //'u.deleted_at',
                    //'m.role_id',
                    'u.old_id',
                    'u.id'
                    // 'u.auth_type',
                    //'u.created_at'
                ])
                ->where('m.school_id', $school->id)
                ->where('u.status', 1)
                ->where(function ($query) {
                    $query
                        ->where('u.auth_type', '!=', 1001)
                        ->orWhereNull('u.auth_type');
                })
                ->whereNull('u.deleted_at')
                ->whereNull('m.deleted_at')
                ->whereNotNull('u.old_id')
                ->get();

            $this->info(
                "Checking school: {$school->name} for invalid user domains:"
            );
            $this->output->progressStart($users->count());

            foreach ($users as $user) {
                try {
                    $domain = optional(explode('@', $user->email))[1];

                    if (
                        $domain !== null &&
                        !in_array(
                            $domain,
                            $school->domains->pluck('name')->toArray()
                        )
                    ) {
                        DB::table('invalid_emails_on_schools_users')->insert([
                            'user_id' => $user->id,
                            'user_domain' => $domain,
                            'school_id' => $school->id,
                            'school_domain' => implode(
                                ',',
                                $school->domains->pluck('name')->toArray()
                            ),
                            'created_at' => now(),
                            'updated_at' => now()
                        ]);
                    }
                } catch (\ErrorException $e) {
                }

                $this->output->progressAdvance();
            }

            $this->newLine();
        }

        return 0;
    }

    public function dupMem($schoolId = null, $onMembership = false)
    {
        if ($onMembership) {
            $duplicatedMembershipIds = DB::table('schools_users')
                ->select('old_id')
//            ->whereNull('deleted_at')
                ->when($schoolId && $schoolId != 'all', function ($q) use ($schoolId) {
                    $q->where('school_id', $schoolId);
                })
                ->groupBy(['old_id'])
                ->havingRaw('count(old_id)>1')
                ->get();
        } else {
            $duplicatedMembershipIds = DB::table('schools_users')
                ->select('user_id', 'school_id')
//            ->whereNull('deleted_at')
                ->when($schoolId && $schoolId != 'all', function ($q) use ($schoolId) {
                    $q->where('school_id', $schoolId);
                })
                ->groupBy(['user_id', 'school_id'])
                ->havingRaw('count(user_id)>1')
                ->get();
        }

      
        $this->output->progressStart($duplicatedMembershipIds->count());

        foreach ($duplicatedMembershipIds as $duplicatedMembershipId) {
            $membership = DB::table('schools_users')
                ->when($onMembership, function ($q) use ($duplicatedMembershipId) {
                    $q->where('old_id', $duplicatedMembershipId->old_id);
                })
                ->when(!$onMembership, function ($q) use ($duplicatedMembershipId) {
                    $q->where('school_id', $duplicatedMembershipId->school_id)
                        ->where('user_id', $duplicatedMembershipId->user_id);
                })
                ->first();

            if ($membership) {
                $duplicatedMembership = DB::table('schools_users')
                    ->when($onMembership, function ($q) use ($duplicatedMembershipId) {
                        $q->where('old_id', $duplicatedMembershipId->old_id);
                    })
                    ->when(!$onMembership, function ($q) use ($duplicatedMembershipId) {
                        $q->where('school_id', $duplicatedMembershipId->school_id)
                            ->where('user_id', $duplicatedMembershipId->user_id);
                    })
                    ->where('id', "!=", $membership->id)
                    ->first();
                $data = [
                    'updated_at' => now(),
                    'membership_uuid' => $membership->membership_uuid ? $membership->membership_uuid : $duplicatedMembership->membership_uuid,
                    'old_id' => $membership->old_id ? $membership->old_id : $duplicatedMembership->old_id,
                    'student_sis_id' => $membership->student_sis_id ? $membership->student_sis_id : $duplicatedMembership->student_sis_id,
                    'student_sis_id' => $membership->student_sis_id ? $membership->student_sis_id : $duplicatedMembership->student_sis_id,
                ];

                DB::table('schools_users')
                    ->where('id', $membership->id)
                    ->update($data);

                $data['deleted_at'] = now();
                DB::table('schools_users')
                    ->where('id', $duplicatedMembership->id)
                    ->update($data);


            }
            $this->output->progressAdvance();
        }
    }

    public function duplicatedCUS()
    {
        $cusUuids = [
            '8489a40b-8805-4f04-a0bd-0d6b1ebb1001',
            '8e0d6ad0-5702-426d-a615-86c758e9f5e3'
        ];
        $this->output->progressStart(count($cusUuids));
        foreach ($cusUuids as $cusUuid) {
            $users = User::withoutGlobalScopes()
                ->where('cus_uuid', $cusUuid)
                ->get();
            $commonUser = CommonUser::where('uuid', $cusUuid)->first();
            $this->info($cusUuid);
            foreach ($users as $user) {
                $otherUsers = User::withoutGlobalScopes()
                    ->where('email', $user->email)
                    ->whereNotNull('email')
                    ->where('id', '!=', $user->id)
                    ->exists();

                if ($otherUsers) {
                    $toUser = User::where('email', $user->email)
                        ->whereNotNull('email')
                        ->withoutGlobalScopes()
                        ->first();

                    $swapToUser = DB::table('duplicate_users_clone')
                        ->where('from_id', $toUser->id)
                        ->where('to_id', '!=', $toUser->id)
                        ->first();

                    if ($swapToUser) {
                        $toUser = User::where('id', $swapToUser->to_id)
                            ->withoutGlobalScopes()
                            ->first();
                    }

                    DB::table('duplicate_users_clone')->updateOrInsert(
                        [
                            'from_id' => $toUser->id,
                            'to_id' => $toUser->id
                        ],
                        [
                            'old_from_id' => $toUser->old_id
                        ]
                    );
                    $duplicateUsers = DB::table('users')
                        ->select('id', 'old_id')
                        ->where('email', $toUser->email)
                        ->whereNotNull('email')
                        ->where('id', '!=', $toUser->id)
                        ->get();

                    foreach ($duplicateUsers as $duplicate) {
                        DB::table('duplicate_users_clone')->updateOrInsert(
                            [
                                'from_id' => $duplicate->id,
                                'to_id' => $toUser->id
                            ],
                            [
                                'old_from_id' => $duplicate->old_id
                            ]
                        );
                    }

                    $duplicates = DB::table('duplicate_users_clone')
                        ->select('from_id')
                        ->where('to_id', $toUser->id)
                        ->where('from_id', '!=', $toUser->id)
                        ->get()
                        ->pluck('from_id');
                    $this->info(count($duplicates));
                    if ($toUser->cus_uuid == null) {
                        $duplicateUserWithCus = User::whereIn('id', $duplicates)
                            ->whereNotNull('cus_uuid')
                            ->withoutGlobalScopes()
                            ->first();
                        if ($duplicateUserWithCus) {
                            $toUser->update([
                                'cus_uuid' => $duplicateUserWithCus->cus_uuid
                            ]);
                        }
                    }
                    if ($toUser->student_sis_id == null) {
                        $duplicateUserWithSis = User::whereIn('id', $duplicates)
                            ->whereNotNull('student_sis_id')
                            ->withoutGlobalScopes()
                            ->first();
                        if ($duplicateUserWithSis) {
                            $toUser->update([
                                'student_sis_id' =>
                                    $duplicateUserWithSis->student_sis_id
                            ]);
                        }
                    }
                    if ($toUser->username == null) {
                        $duplicateUserWithUsername = User::whereIn(
                            'id',
                            $duplicates
                        )
                            ->whereNotNull('username')
                            ->withoutGlobalScopes()
                            ->first();
                        if ($duplicateUserWithUsername) {
                            $toUser->update([
                                'username' =>
                                    $duplicateUserWithUsername->username
                            ]);
                        }
                    }
                    if ($toUser->clever_id == null) {
                        $duplicateUserWithCleverId = User::whereIn(
                            'id',
                            $duplicates
                        )
                            ->whereNotNull('clever_id')
                            ->withoutGlobalScopes()
                            ->first();
                        if ($duplicateUserWithCleverId) {
                            $toUser->update([
                                'clever_id' =>
                                    $duplicateUserWithCleverId->clever_id
                            ]);
                        }
                    }
                    if ($toUser->clever_building_id == null) {
                        $duplicateUserWithCleverBuildingId = User::whereIn(
                            'id',
                            $duplicates
                        )
                            ->whereNotNull('clever_building_id')
                            ->withoutGlobalScopes()
                            ->first();
                        if ($duplicateUserWithCleverBuildingId) {
                            $toUser->update([
                                'clever_building_id' =>
                                    $duplicateUserWithCleverBuildingId->clever_building_id
                            ]);
                        }
                    }
                    if ($toUser->deleted_at != null) {
                        $duplicateNotDeleted = User::whereIn('id', $duplicates)
                            ->whereNull('deleted_at')
                            ->withoutGlobalScopes()
                            ->first();
                        if ($duplicateNotDeleted) {
                            $toUser->update([
                                'deleted_at' => null
                            ]);
                        }
                    }

                    User::withoutGlobalScopes()
                        ->whereIn('id', $duplicates)
                        ->update([
                            'cus_uuid' => null,
                            'common_user_id' => null,
                            'status' => 0,
                            'deleted_at' => now()
                        ]);
                } else {
                    $oldUser = \App\Models\V1\User::withoutGlobalScopes()
                        ->where('user_id', $user->old_id)
                        ->first();

                    if ($oldUser) {
                        if ($commonUser->email == $oldUser->email) {
                            $user->update([
                                'email' => $oldUser->email,
                                'student_sis_id' => $oldUser->student_sis_id
                            ]);
                        } else {
                            $user->update([
                                'email' => $oldUser->email,
                                'student_sis_id' => $oldUser->student_sis_id,
                                'cus_uuid' => null,
                                'common_user_id' => null
                            ]);
                            $oldUser->migrateToV2andCus();
                        }
                    } else {
                        if ($commonUser->email != $user->email) {
                            $user->update([
                                'email' => $commonUser->email
                            ]);
                            //                            $oldUser->migrateToV2andCus();
                        }
                    }
                }
            }

            $this->output->progressAdvance();
        }
    }

    public function clearStatuses($school)
    {
        $v1Emails = DB::connection('oldDB')
            ->table('users')
            ->select('email')
            ->where('school_id', $school->old_id)
            ->where('status', 1)
            ->whereNotNull('email')
            ->get()
            ->pluck('email');

        $dbQuery = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select([
                'u.email',
                'u.status',
                'u.student_sis_id',
                'u.cus_uuid',
                'u.deleted_at',
                'm.role_id',
                'u.old_id',
                'u.id',
                'u.auth_type',
                'u.created_at'
            ])
            ->where('m.school_id', $school->id)
            ->where('u.status', 1)
            ->where(function ($query) {
                $query
                    ->where('u.auth_type', '!=', 1001)
                    ->orWhereNull('u.auth_type');
            })
            ->whereNull('u.deleted_at')
            ->whereNull('m.deleted_at')
            ->where(function ($query) use ($v1Emails) {
                $query
                    ->whereNull('u.email')
                    ->orWhereNotIn('u.email', $v1Emails);
            })
            ->whereNotNull('u.old_id')
            ->orderBy('u.id');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(10, function ($arrayUsers) use ($school) {
            foreach ($arrayUsers as $arrayUser) {
                $oldUser = \App\Models\V1\User::where(
                    'user_id',
                    $arrayUser->old_id
                )->first();

                if ($oldUser && $oldUser->status) {
                    $user = User::where('old_id', $oldUser->user_id)->first();
                    dd($user->toArray());
                    $membership = DB::table('schools_users')
                        ->where('user_id', $user->id)
                        ->where('school_id', $school->id)
                        ->first();

                    dd($membership);

                    //                    $oldUser->migrateToV2andCus();
                    //                    $user = User::where('old_id', $oldUser->user_id)->first();
                    //
                    //                    dd($oldUser);
                }
                dd($oldUser);
                $this->output->progressAdvance();
            }
        });
    }

    public function fixSchoolsSftpCompare()
    {
        $dbQuery = DB::table('sftp_directory_diff_report')->orderBy('id');
        $this->output->progressStart($dbQuery->count());
        $dbQuery->chunk(10, function ($reports) {
            foreach ($reports as $report) {
                $commonSchool = CommonSchool::where('id', $report->id)
                    ->select('id', 'full_name', 'sftp_directory')
                    ->first();
                if ($commonSchool) {
                    DB::table('sftp_directory_diff_report')
                        ->where('id', $report->id)
                        ->update([
                            'name' => $commonSchool->full_name,
                            'sftp_directory_prod' =>
                                $commonSchool->sftp_directory
                        ]);
                }

                $this->output->progressAdvance();
            }
        });
    }

    public function fixSchoolsSftp()
    {
        $dbQuery = DB::table('sftp_directory_diff_report')
            ->whereRaw('sftp_directory_prod <> sftp_directory_stage')
            ->whereRaw('sftp_directory_stage not like "% %"')
            ->orderBy('id');
        $this->output->progressStart($dbQuery->count());
        $dbQuery->chunk(10, function ($reports) {
            foreach ($reports as $report) {
                $school = School::where('common_id', $report->id)->first();
                $body = [
                    'sftpDirectory' => $report->sftp_directory_stage
                ];

                $this->updateSchool($school, $body);
                $this->output->progressAdvance();
            }
        });
    }

    protected function checkMissingFirstNameLastNameUsersInV2(): int
    {
        $v1UserWithoutFirstNameOrLastName = DB::connection('oldDB')
            ->table('users')
            ->select('*')
            ->where(function ($query) {
                $query->where('firstname', ' ')->orWhere('lastname', ' ');
            })
            ->where('username', 'not like', '%system%')
            ->orderBy('user_id', 'asc');

        $v1UserWithoutFirstNameOrLastName->chunk(300, function ($users) {
            foreach ($users as $user) {
                $schoolExistsInV2 = School::where(
                    'old_id',
                    $user->school_id
                )->exists();

                if ($schoolExistsInV2) {
                    $newUser = User::where('old_id', $user->user_id)->first();
                    if (
                        isEmpty($newUser->first_name) &&
                        isEmpty($newUser->last_name) &&
                        isEmpty($user->first_name) &&
                        isEmpty($user->last_name)
                    ) {
                        User::where('old_id', $user->user_id)->update([
                            'first_name' => 'Blank-User',
                            'last_name' => 'Blank-User'
                        ]);
                    }

                    if (
                        isEmpty($newUser->first_name) &&
                        !isEmpty($user->first_name)
                    ) {
                        User::where('old_id', $user->user_id)->update([
                            'first_name' => 'Blank-User'
                        ]);
                    }

                    if (
                        isEmpty($newUser->last_name) &&
                        !isEmpty($user->last_name)
                    ) {
                        User::where('old_id', $user->user_id)->update([
                            'last_name' => 'Blank-User'
                        ]);
                    }
                }
            }
        });

        return 0;
    }

    protected function recreateMissingInV2()
    {
        $schools = School::where('status', 1)->whereNotNull('old_id');

        foreach ($schools->get() as $school) {
            $this->info("Recreate for {$school->name} {$school->id}");

            $v2Emails = DB::table('schools_users as m')
                ->join('users as u', 'm.user_id', '=', 'u.id')
                ->select('u.email')
                ->where('m.school_id', $school->id)
                ->where('u.status', 1)
                ->where(function ($query) {
                    $query
                        ->where('u.auth_type', '!=', 1001)
                        ->orWhereNull('u.auth_type');
                })
                ->whereNull('u.deleted_at')
                ->get()
                ->pluck('email');

            $diffsV1 = DB::connection('oldDB')
                ->table('ehallpassDB.users as oldu')
                ->select(['oldu.user_id'])
                ->where('status', 1)
                ->where('email', 'not like', '%system%')
                ->where('school_id', $school->old_id)
                ->whereNotIn('email', $v2Emails)
                ->orderBy('user_id');

            $this->output->progressStart($diffsV1->count());

            $diffsV1->chunk(10, function ($oldUsers) {
                foreach ($oldUsers as $oldUser) {
                    $oldUserModel = \App\Models\V1\User::where(
                        'user_id',
                        $oldUser->user_id
                    )->first();
                    if ($oldUserModel) {
                        $oldUserModel->migrateToV2andCus();
                    }

                    $this->output->progressAdvance();
                }
            });
        }
    }

    protected function recreateUserCUS()
    {
        $ids = [];
        $this->output->progressStart(count($ids));
        foreach ($ids as $id) {
            $user = User::withoutGlobalScopes()
                ->where('id', $id)
                ->first();

            $commonUser = CommonUser::select('id', 'uuid')
                ->where('email', $user->email)
                ->first();

            if ($commonUser) {
                $user->update([
                    'cus_uuid' => $commonUser->uuid,
                    'common_user_id' => $commonUser->id
                ]);
            }

            $memberships = DB::table('schools_users')
                ->where('user_id', $user->id)
                ->get();
            foreach ($memberships as $membership) {
                $domains = Domain::where('school_id', $membership->school_id)
                    ->get()
                    ->pluck('name')
                    ->toArray();

                $userDomain = explode('@', $user->email)[1];

                if (!in_array($userDomain, $domains)) {
                    $response = $this->client->deleteUser(
                        $membership->membership_uuid
                    );
                    DB::table('schools_users')
                        ->where('user_id', $user->id)
                        ->where('school_id', $membership->school_id)
                        ->delete();

                    $this->output->progressAdvance();
                }
            }
        }
    }

    /**
     * @param null $schoolId
     * @return int
     */
    public function deleteMissingFromV1($schoolId = null): int
    {
        $school = null;

        if ($schoolId) {
            $school = School::findOrFail($schoolId);
        }

        $v1Emails = DB::connection('oldDB')
            ->table('users')
            ->select('email')
            ->when($schoolId, function ($query) use ($school) {
                $query->where('school_id', $school->old_id);
            })
            ->whereNotNull('email')
            ->where('status', 1)
            ->get()
            ->pluck('email');

        DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select(['u.email', 'u.id'])
            ->where('m.school_id', $school->id)
            ->where('u.status', 1)
            ->whereNotNull('u.old_id')
            ->where(function ($query) {
                $query
                    ->where('u.auth_type', '!=', 1001)
                    ->orWhereNull('u.auth_type');
            })
            ->whereNull('u.deleted_at')
            ->where(function ($query) use ($v1Emails) {
                $query
                    ->whereNull('u.email')
                    ->orWhereNotIn('u.email', $v1Emails);
            })
            ->update(['deleted_at' => now(), 'status' => User::INACTIVE]);

        return 0;
    }

    protected function modules($schoolId)
    {
        $school = School::where('id', $schoolId)->first();
        if (!$school) {
            return false;
        }
        $oldSchool = \App\Models\V1\School::where(
            'school_id',
            $school->old_id
        )->first();
        if (!$oldSchool) {
            return false;
        }

        return $oldSchool->migrateModules();
    }

    protected function stickyRoles($schoolId)
    {
        $roles = [
            0 => 3, //substitute
            -2 => null, //none => null
            1 => 5, //superadmin => superadmin
            2 => 2, //admin => admin
            3 => 3, //teacher => teacher
            4 => 1, //student => student
            5 => 4, //staff => staff
            6 => 2, //schooladmin => admin
            7 => null, //system => null
            8 => null, //norole => null
            10 => null
        ];

        $dbQuery = DB::table('schools_users')
            ->join('schools', 'school_id', 'id')
            ->where('school_id', $schoolId)
            ->where('role_id', '!=', 1)
            ->orderBy('user_id');
        $this->info("Sticky Role for school {$schoolId}");
        $this->output->progressStart($dbQuery->count());
        $dbQuery->chunk(100, function ($memberships) use ($roles) {
            foreach ($memberships as $membership) {
                $user = User::without('roleUser', 'role')
                    ->where('id', $membership->user_id)
                    ->whereNotNull('cus_uuid')
                    ->whereNotNull('old_id')
                    ->first();
                if ($user) {
                    $oldUser = \App\Models\V1\User::select('user_id', 'role_id')
                        ->where('email', $user->email)
                        ->where('school_id', $membership->old_id)
                        ->first();

                    if ($oldUser) {
                        $roleId = $roles[$oldUser->role_id];
                        if ($roleId != $membership->role_id) {
                            DB::table('schools_users')
                                ->where('school_id', $membership->school_id)
                                ->where('user_id', $membership->user_id)
                                ->update([
                                    'role_id' => $roleId,
                                    'role_changed_at' => now()
                                ]);

                            $this->updateUserRole($membership, $user, $roleId);
                        }
                    }
                }
                $this->output->progressAdvance();
            }
        });
    }

    protected function getSftpDirectory($name, $oldSchool)
    {
        $dirName =
            $oldSchool->upload_folder != '' ? $oldSchool->upload_folder : $name;

        return substr(
            strtolower(
                preg_replace(
                    ['/[^A-Za-z0-9]/', '/highschool/i', '/middleschool/i'],
                    ['', 'hs', 'ms'],
                    $dirName
                )
            ),
            0,
            32
        );
    }

    protected function updateSchool($school, $body = null, $oldSchool = null)
    {
        $projectManagerUuid = '7990a773-1064-4ed3-9571-cb3a6d1056ca';
        $ehpClient = 'ba63e471-0b6a-4437-afab-ff38bc36c092';

        if (!$body) {
            $body = [];

            $domains = [$school->school_domain];
            if ($school->school_domain != $school->student_domain) {
                $domains[] = $school->student_domain;
            }

            $body = [
                'clientOptionCollection' => [
                    [
                        'client' => $ehpClient,
                        'projectManager' => $projectManagerUuid,
                        'relationshipManager' => $projectManagerUuid
                    ]
                ],
                'authProviderIdCollection' => $this->getAtuhProviders($school),
                'appointmentPass' => $school->hasModule(
                    Module::APPOINTMENTPASS
                ),
                'autoCheckIn' => $school->hasModule(Module::AUTO_CHECKIN),
                'kiosk' => $school->hasModule(Module::KIOSK),
                'buildingCollection' => null,
                'description' => $school->description,
                //            "district" => $excellSchool->district,
                'emailDomainCollection' => $domains,
                'fullName' => $school->name,
                'name' => $school->name,
                //            "timeZone" => $excellSchool->timezone_uuid,
                'l10nUsTerritorialSubdivisionCodeAnsi' => $school->state,
                'zipCode' => $school->zip,
                //            "sftpCredentialsEmail" => "",
                //            "sftpSendCredentials" => "",
                'userProviderCollection' => $this->getUserProviderCollection(
                    $school
                ),
                'status' => $school->status,
                //            "providingStudentPhotos" => 0,
                'sftpDirectory' => $this->getSftpDirectory(
                    $school->name,
                    $school
                )
            ];

            if ($oldSchool) {
                $extArray = explode('.', $oldSchool->school_logo);

                $logo = null;
                if (count($extArray) > 1) {
                    $logo =
                        "data:image/{$extArray[1]};base64," .
                        base64_encode(
                            Storage::disk('s3v1')->get(
                                'public/schoolimage/' . $oldSchool->school_logo
                            )
                        );
                }
                $body['logo'] = $logo;
                $body['tier'] = $oldSchool->total_users;
                $body['sftpFilePrefix'] = $oldSchool->archive_prefix;
            }
        }
        unset($body['logo']);
        dd($school->cus_uuid, $body);
        $result = $this->client->updateSchool($school->cus_uuid, $body);

        if ($result) {
            $this->info(
                "Updated: {$school->id} {$school->name} {$school->cus_uuid}"
            );
        }
    }

    protected function updateUserRole($membership, $user, $roleId)
    {
        $roleCollection = CommonUser::getRoleName($roleId);

        //        $userData = [
        //            'uuid' => $user->cus_uuid,
        //            'email' => $user->email,
        //            'firstName' => $user->first_name,
        //            'lastName' => $user->last_name
        //        ];
        //
        $gradeYear = $user->grade_year;
        if (!$gradeYear && $roleId == 1) {
            $gradeYear = 0;
        }

        // check if membership status e 0
        $membershipData = [
            //            'status' => $user->status,
            'school' => $school->cus_uuid ?? $membership->cus_uuid,
            'authClientId' => 2,
            //            'user' => $userData,
            'role' => $roleCollection[0],
            //            'sisId' => $user->student_sis_id,
            'graduationYear' => $gradeYear,
            //            'sisPreventFromChangingRole' => true
        ];

        if ($membership->role_changed_at) {
            $membershipData['sisPreventFromChangingRole'] = true;
        }
        //        $membership = DB::table('schools_users')
        //            ->where('user_id', $user->id)
        //            ->where('school_id', $membership->id ?? $school->id)
        //            ->first();
        //        if (!$membership) {
        //            return false;
        //        }
//        dd( $membership->membership_uuid,$membershipData);
        return $this->client->modifyMembershipUser(
            $membership->membership_uuid,
            $membershipData
        );
    }

    protected function createUsers()
    {
        $excellSchools = SchoolsEhp::whereNotNull('school_uuid')
            ->whereIn('school_id', [
                1,
                2178,
                2177,
                2176,
                2175,
                2174,
                2173,
                2172,
                2171,
                2170,
                2169,
                2168,
                2167,
                2166,
                2165,
                2164,
                2163,
                2162,
                2161,
                2160,
                2159,
                2158,
                2157,
                2156,
                2155,
                2154,
                2153
            ])
            ->get()
            ->pluck('school_id');
        //        $commonSchools = CommonSchool::select('uuid','l10n_us_territorial_subdivision_code_ansi')
        //            ->where('created_by', 38);

        $dbQuery = Db::table('users as u')
            ->join('schools as s', 'u.school_id', '=', 's.id')
            ->selectRaw('u.id,s.id as school_id,s.name')
            ->whereNull('u.cus_uuid')
            ->whereNotNull('u.email')
            ->whereIntegerInRaw('s.id', $excellSchools)
            ->orderBy('u.id', 'asc');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(100, function ($existingUsers) {
            foreach ($existingUsers as $existingUser) {
                $user = User::where('id', $existingUser->id)->first();

                $school = School::where(
                    'id',
                    $existingUser->school_id
                )->first();
                if ($user) {
                    $schoolDomains = [
                        $school->school_domain,
                        $school->student_domain
                    ];
                    $email = $user->email;
                    $domain = explode('@', $email)[1];
                    if (in_array($domain, $schoolDomains)) {
                        //                        $commonSchool = CommonSchool::select('id', 'uuid', 'l10n_us_territorial_subdivision_code_ansi')
                        //                            ->where('created_by', 38)
                        //                            ->where('uuid', $school->cus_uuid)
                        //                            ->where('l10n_us_territorial_subdivision_code_ansi', $school->state)
                        //                            ->first();

                        //                        if ($commonSchool) {
                        DB::table('schools_users')->updateOrInsert(
                            [
                                'school_id' => $school->id,
                                'user_id' => $user->id
                            ],
                            [
                                'role_id' => $user->role_id
                            ]
                        );

                        if (
                            !CommonUser::where('email', $user->email)->exists()
                        ) {
                            CreateCommonUserJob::dispatchSync([
                                'user' => $user,
                                'authenticatedUser' => $this->adminUser,
                                'school' => $school
                            ]);
                        }
                        //                        }
                    }
                }
                $this->output->progressAdvance();
            }
        });
    }

    protected function getAtuhProviders($school)
    {
        $result = [0];

        $clever = $this->getCleverCredentials($school);
        if ($clever) {
            $result[] = AuthType::CLEVER;
        }

        $classLink = $this->getClassLinkCredentials($school);

        if ($classLink) {
            $result[] = AuthType::CLASSLINK;
        }
        return $result;
    }

    protected function getUserProviderCollection($school)
    {
        $result = [];

        $clever = $this->getCleverCredentials($school);
        if ($clever) {
            $result['clever'] = $clever;
        }

        $classLink = $this->getClassLinkCredentials($school);

        if ($classLink) {
            $result['classLink'] = $classLink;
        }
        return $result;
    }

    protected function getCleverCredentials($school)
    {
        $cleverInfo = $this->oldDb
            ->table('schools_clever')
            ->where('ehp_school_id', $school->old_id)
            ->get();

        $result = [];
        foreach ($cleverInfo as $singleClever) {
            $sisField = json_decode($singleClever->sis_field);
            $result[] = [
                'districtId' => $singleClever->district_id,
                'primaryId' => $sisField->clever_stu_sis_field ?? 'sis_id',
                'cleverId' => $singleClever->clever_id
            ];
        }
        return $result;
    }

    protected function getClassLinkCredentials($school)
    {
        return $this->oldDb
            ->table('schools_classlink')
            ->selectRaw('tenant_id as tenant, source_id as sourcedId')
            ->where('ehp_school_id', $school->old_id)
            ->get()
            ->toArray();
    }

    protected function fixSchools()
    {
        foreach (
            [
                418,
                535,
                560,
                561,
                783,
                879,
                940,
                1021,
                1065,
                1100,
                1328,
                1527,
                1551,
                1569,
                1570,
                1571,
                1573,
                1576,
                1579,
                1589,
                1604,
                1609,
                1626,
                1643,
                1644,
                1659
            ]
            as $id
        ) {
            $schoolForFix = DB::table('common_bm_school_diffs')
                ->where('id', $id)
                ->where('modified_by', 38)
                ->first();

            $correctSchool = DB::table('common_bm_school_diffs')
                ->where('id', $id)
                ->where(function ($q) {
                    $q->where('modified_by', '<>', 38)->orWhereNull(
                        'modified_by'
                    );
                })
                ->whereNotNull('appointment_pass')
                ->first();
            if ($correctSchool) {
                $correctTimezone = CommonTimezone::where(
                    'id',
                    $correctSchool->l10n_datetime_time_zone_id
                )->first()->uuid;
                $correctBody = [
                    //            "authProviderIdCollection" => $this->getAtuhProviders($school),
                    'appointmentPass' => boolval(
                        $correctSchool->appointment_pass
                    ),
                    'autoCheckIn' => boolval($correctSchool->auto_check_in),
                    'kiosk' => boolval($correctSchool->kiosk),
                    //                    "buildingCollection" => null,
                    'description' => $correctSchool->description,
                    //            "district" => $excellSchool->district,
                    //            "emailDomainCollection" => $domains,
                    //            "fullName" => $school->name,
                    'logo' => $correctSchool->logo,
                    'tier' => $correctSchool->tier,
                    'timeZone' => $correctTimezone,
                    'l10nUsTerritorialSubdivisionCodeAnsi' =>
                        $correctSchool->l10n_us_territorial_subdivision_code_ansi,
                    'zipCode' => $correctSchool->zip_code,
                    'sftpCredentialsEmail' =>
                        $correctSchool->sftp_credentials_email,
                    'sftpFilePrefix' => $correctSchool->sftp_file_prefix,
                    //            "userProviderCollection" => $this->getUserProviderCollection($school),
                    'status' => $correctSchool->r_status,
                    'sftpDirectory' => $correctSchool->sftp_directory
                ];
                //        unset($wrongBody['logo']);
                //                unset($correctBody['logo']);
                //                dd($correctBody);

                $result = $this->client->updateSchool(
                    $correctSchool->uuid,
                    $correctBody
                );
            }
            //            if ($result) {
            //                dd($result);
            //            } else {
            //                dd('issue');
            //            }
        }
    }

    protected function createMissingMemberships($schoolId)
    {
        $dbQuery = DB::table('schools_users as m')
            ->join('schools as s', 'm.school_id', '=', 's.id')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->select('m.user_id', 'm.school_id', 'm.role_id')
            ->where('m.school_id', $schoolId)
            ->whereNotNull('s.cus_uuid')
            //            ->whereNull('u.cus_uuid')
            ->where('u.status', '>=', 0)
            //            ->whereNull('m.membership_uuid')
            ->orderBy('m.user_id');

        $school = School::where('id', $schoolId)->first();

        $this->info("Fixing Schoo {$school->name} with id {$schoolId}");

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk('5', function ($memberships) use ($school) {
            foreach ($memberships as $membership) {
                $user = User::where('id', $membership->user_id)
                    //                    ->withoutGlobalScopes()
                    ->first();

                $retryExist = DB::table('memberships_retries')
                    ->where('user_id', $membership->user_id)
                    ->where('school_id', $membership->school_id)
                    ->exists();

                if ($user && !$retryExist) {
                    $commonUser = CommonUser::where(
                        'email',
                        $user->email
                    )->first();

                    if ($commonUser) {
                        $commonSchool = CommonSchool::select('id')
                            ->where('uuid', $school->cus_uuid)
                            ->first();

                        if ($commonSchool) {
                            $commonMembership = CommonMembership::where(
                                'auth_user_id',
                                $commonUser->id
                            )
                                ->where(
                                    'common_bm_school_id',
                                    $commonSchool->id
                                )
                                ->where('auth_client_id', 2)
                                ->first();
                            if ($commonMembership) {
                                $user->update([
                                    'cus_uuid' => $commonUser->uuid,
                                    'initial_password' => null
                                ]);
                                DB::table('schools_users')->updateOrInsert(
                                    [
                                        'school_id' => $school->id,
                                        'user_id' => $user->id
                                    ],
                                    [
                                        'membership_uuid' =>
                                            $commonMembership['uuid']
                                    ]
                                );
                            } else {
                                $membership = $this->createMembership(
                                    $membership,
                                    $user,
                                    $school
                                );
                            }
                        }
                    } else {
                        $membership = $this->createMembership(
                            $membership,
                            $user,
                            $school
                        );
                    }
                }

                $this->output->progressAdvance();
            }
        });
    }

    protected function createMembership($membership, $user, $school)
    {
        $client = new CommonUserService($this->adminUser);

        $roleCollection = CommonUser::getRoleName($membership->role_id);

        $userData = [
            'email' => $user->email,
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
            'status' => $user->status,
            'password' => null,
            'username' => $user->username
        ];
        if ($user->user_initials) {
            $userData['title'] = $user->user_initials;
        }

        $gradeYear = $user->grade_year;
        if (!$gradeYear && $user->isStudent()) {
            $gradeYear = 0;
        }

        $membershipData = [
            'status' => $user->status,
            'school' => $school->cus_uuid,
            'authClientId' => 2,
            'user' => $userData,
            'role' => $roleCollection[0],
            'graduationYear' => $gradeYear,
            'sisId' => $user->student_sis_id,
            'sendConfirmationEmail' => false,
            'preventFromArchiving' => true
        ];

        $commonMembership = $client->assignUserToSchool($membershipData);

        //logger('MEMBERSHIP', [$commonMembership]);
        if (!isset($commonMembership['status'])) {
            $userCommonId = CommonMembership::where(
                'id',
                $commonMembership['id']
            )->first()->auth_user_id;

            $user->update([
                'cus_uuid' => CommonUser::find($userCommonId)->uuid,
                'initial_password' => null
            ]);
            DB::table('schools_users')->updateOrInsert(
                [
                    'school_id' => $school->id,
                    'user_id' => $user->id
                ],
                [
                    'membership_uuid' => $commonMembership['uuid']
                ]
            );
        } else {
            $decoded = json_decode($commonMembership['error'], 1);
            $code = $decoded['status'];
            $message = '';
            if (isset($decoded['debug']['previous'])) {
                $message = $decoded['debug']['previous']['message'];
            }
            if (isset($decoded['code'])) {
                $message = $decoded['code'];
            }
            DB::table('memberships_retries')->updateOrInsert(
                [
                    'user_id' => $user->id,
                    'school_id' => $school->id
                ],
                [
                    'body' => json_encode($membershipData),
                    'code' => $code,
                    'message' => $message
                ]
            );
        }
        return $commonMembership;
    }

    protected function fixAllMissingSchoolLogos()
    {
        $commonSchools = CommonSchool::select('id', 'uuid')
            ->where(function ($query) {
                $query->where('created_by', 38)->orWhere('modified_by', 38);
            })
            //            ->where('created_by', 38)
            //            ->where('uuid', "a2f962e9-3232-4b12-b034-5aec76fba679")
            ->whereNull('logo');

        $this->output->progressStart($commonSchools->count());

        foreach ($commonSchools->get() as $commonSchool) {
            $school = School::where('cus_uuid', $commonSchool->uuid)->first();
            if ($school) {
                $oldSchool = \App\Models\V1\School::where(
                    'school_id',
                    $school->old_id
                )->first();
                if ($oldSchool && $oldSchool->school_logo != '') {
                    $extArray = explode('.', $oldSchool->school_logo);
                    $logo = null;
                    if (count($extArray) > 1) {
                        $path = 'public/schoolimage/' . $oldSchool->school_logo;
                        if (Storage::disk('s3v1')->exists($path)) {
                            $logo =
                                "data:image/{$extArray[1]};base64," .
                                base64_encode(
                                    Storage::disk('s3v1')->get($path)
                                );
                        }
                    }
                    if ($logo) {
                        $body['logo'] = $logo;
                        $this->updateSchool($school, $body, $oldSchool);
                    }
                }
            }
            $this->output->progressAdvance();
        }
    }

    protected function fixArchivedMemberships()
    {
        $ids = [];
        $memberships = Db::connection('commonDb')
            ->table('common_bm_membership as m')
            ->selectRaw('m.auth_user_id as user_id')
            ->where('m.auth_client_id', 2)
            ->where('m.not_archived', 1)
            ->whereIntegerInRaw('auth_user_id', $ids);

        $this->output->progressStart($memberships->count());

        $memberships->orderBy('user_id')->chunk(10, function ($memberships) {
            foreach ($memberships as $membership) {
                $fullRecord = Db::connection('commonDb')
                    ->table('common_bm_membership as m')
                    ->join('auth_user as u', 'm.auth_user_id', '=', 'u.id')
                    ->join(
                        'common_bm_school as s',
                        'm.common_bm_school_id',
                        '=',
                        's.id'
                    )
                    ->leftJoin(
                        'auth_title as t',
                        'u.auth_title_id',
                        '=',
                        't.id'
                    )
                    ->selectRaw(
                        'u.uuid as user_uuid,
                        u.email as email,
                        m.sis_id as student_sis_id,
                        u.username as username,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        m.common_bm_role_membership_id as role_id,
                        u.image as avatar,
                        m.graduation_year as grade_year,
                        u.r_status as user_status,
                        m.r_status as membership_status,
                        u.last_login_time as last_login_time,
                        u.created as created_at,
                        u.modified as updated_at,
                        s.uuid as school_uuid,
                        m.uuid as membership_uuid,
                        t.r_name as title,
                        u.id as common_user_id,
                        m.archived_at as deleted_at
                        '
                    )
                    ->where('m.not_archived', 1)
                    ->where('m.auth_user_id', $membership->user_id)
                    ->first();

                $school = School::where(
                    'cus_uuid',
                    $fullRecord->school_uuid
                )->first();

                if ($school) {
                    $data = UserTranslator::translateJoinedRecord(
                        $fullRecord,
                        $school
                    );

                    $user = User::withoutGlobalScopes()
                        ->where('cus_uuid', $data['cus_uuid'])
                        ->first();
                    User::enableMassIndex();
                    if ($user) {
                        $user->update($data);
                    } else {
                        $user = User::create($data);
                    }

                    DB::table('schools_users')->updateOrInsert(
                        [
                            'school_id' => $school->id,
                            'user_id' => $user->id
                        ],
                        [
                            'membership_uuid' => $fullRecord->membership_uuid,
                            'role_id' => CommonMembership::getRoleId(
                                $fullRecord->role_id
                            )
                        ]
                    );

                    //                    if ($user->status === User::INACTIVE) {
                    //                        CancelAndUpdateAppointmentsAfterArchiveJob::dispatch($user)->onQueue('appointments');
                    //                    }

                    //                    $user->searchable();
                    User::disableMassIndex();
                    $this->output->progressAdvance();
                }
            }
        });
        $this->output->progressFinish();

        return true;
    }

    public function deactivateSchools()
    {
        $oldSchools = \App\Models\V1\School::where('status', '<=', 0)->get();
        $this->output->progressStart($oldSchools->count());

        foreach ($oldSchools as $oldSchool) {
            School::where('old_id', $oldSchool->school_id)->update([
                'status' => 0
            ]);

            $this->output->progressAdvance();
        }
    }

    public function rolesFix($school)
    {
        $status = 0;
        $roles = [1, 2, 3, 5, 6];
        $oldUsers = \App\Models\V1\User::where('school_id', $school->old_id)
            ->where('school_id', $school->old_id)
            ->where('status', '>', 0)
            ->whereIntegerInRaw('role_id', $roles)
            ->orderBy('user_id');
        $this->output->progressStart($oldUsers->count());

        foreach ($oldUsers->get() as $oldUser) {
            $membership = DB::table('schools_users as m')
                ->join('users as u', 'm.user_id', '=', 'u.id')
                ->join('schools as s', 'm.school_id', '=', 's.id')
                ->select('m.*', 's.cus_uuid as cus_uuid')
                ->where('m.school_id', $school->id)
                ->where('u.email', $oldUser->email)
                ->whereNull('m.deleted_at')
                ->first();

            if ($membership) {
                $roleId = $oldUser->getV2RoleId();

                if ($roleId != $membership->role_id) {
                    DB::table('schools_users')
                        ->where('id', $membership->id)
                        ->update([
                            'role_id' => $roleId,
                            'role_changed_at' => $oldUser->isStudent()
                                ? null
                                : now()
                        ]);

                    $user = User::where('id', $membership->user_id)->first();

                    $result = $this->updateUserRole(
                        $membership,
                        $user,
                        $roleId
                    );

                    if (isset($result['id'])) {
                        $status = 2;
                    }
                }
            }

            $this->output->progressAdvance();
        }
    }

    public function rolesCusFix($school)
    {
        $status = 0;
//        $roles = [1, 2, 3, 5, 6];

        $cusRoles = [
            1 => 2, //admin
            2 => 3, //teacher
            3 => 3, //teacher subs
            4 => 4, //staf
            5 => 1, //student
        ];

        $memberships = DB::table('schools_users as m')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->join('schools as s', 'm.school_id', '=', 's.id')
            ->select('m.*', 's.cus_uuid as cus_uuid')
            ->where('m.school_id', $school->id)
            ->whereNull('m.deleted_at');

        $this->output->progressStart($memberships->count());

        foreach ($memberships->get() as $membership) {


            $cusMembership = CommonMembership::where('uuid', $membership->membership_uuid)->first();

            if ($cusMembership) {
//                $roleId = CommonUser::getRoleName($membership->role_id);

                if ($membership->role_id != $cusRoles[$cusMembership->common_bm_role_membership_id]) {


                    $user = User::where('id', $membership->user_id)->first();

                    $result = $this->updateUserRole(
                        $membership,
                        $user,
                        $membership->role_id
                    );
//                    dd($result);
                }
            }

            $this->output->progressAdvance();
        }
    }

    protected function updateCus($membership, $user, $school, $roleId, $status)
    {
        $client = new CommonUserService($this->adminUser);

        $roleCollection = CommonUser::getRoleName($roleId);

        $userData = [
            'uuid' => $user->cus_uuid,
            'email' => trim($user->email),
            'firstName' => $user->first_name != '' ? $user->first_name : '.',
            'lastName' => $user->last_name != '' ? $user->last_name : '.'
        ];
        $gradeYear = $user->grade_year;
        if (!$gradeYear && $roleId == 1) {
            $gradeYear = 0;
        }

        $membershipData = [
            'status' => $status,
            'school' => $school->cus_uuid,
            'authClientId' => 2,
            'user' => $userData,
            'role' => $roleCollection[0],
            'sisId' => $roleId == 1 ? $user->student_sis_id : null,
            'graduationYear' => $gradeYear
        ];

        $response = $client->modifyMembershipUser(
            $membership->membership_uuid,
            $membershipData
        );
        DB::table('schools_users')->updateOrInsert(
            [
                'school_id' => $school->id,
                'user_id' => $user->id
            ],
            [
                'role_id' => $roleId,
                'deleted_at' => null,
                'status' => $status
            ]
        );

        $user->searchable();

        return !isset($response['status']);
    }

    public function titlesFix($school)
    {
        $roles = [1, 2, 3, 4, 5, 6];
        $oldUsers = \App\Models\V1\User::where('school_id', $school->old_id)
            ->where('school_id', $school->old_id)
            ->where('status', '>', 0)
            ->whereNotNull('user_initials')
            ->whereIntegerInRaw('role_id', $roles)
            ->orderBy('user_id');
        $this->output->progressStart($oldUsers->count());

        foreach ($oldUsers->get() as $oldUser) {
            $membership = DB::table('schools_users as m')
                ->join('users as u', 'm.user_id', '=', 'u.id')
                ->join('schools as s', 'm.school_id', '=', 's.id')
                ->select('m.*', 's.cus_uuid as cus_uuid')
                ->where('m.school_id', $school->id)
                ->where('u.email', $oldUser->email)
                ->whereNull('m.deleted_at')
                ->whereNull('u.cus_uuid')
                ->first();

            if ($membership) {
                dd($membership);
                $oldUser->migrateToV2andCus();
            }

            $this->output->progressAdvance();
        }
        //        DB::table('verified_schools')
        //            ->updateOrInsert(
        //                [
        //                    'school_id' => $school->id
        //                ],
        //                [
        //                    'status' => 2
        //                ]
        //            );
    }
}
