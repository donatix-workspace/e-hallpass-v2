<?php

namespace App\Traits;


trait UnavailableMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'id' => [
                'type' => 'long'
            ],
            'unavailable_type' => [
                'type' => 'keyword'
            ],
            'unavailable_id' => [
                'type' => 'long'
            ],
            'school_id' => [
                'type' => 'long'
            ],
            'start_date_string' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'start_hour' => [
                'type' => 'date',
                "format" => "h:m a",
                'null_value' => 'NULL',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'end_hour' => [
                'type' => 'date',
                "format" => "h:m a",
                'null_value' => 'NULL',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'from_date' => [
                'type' => 'date',
                "format" => "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
            ],
            'to_date' => [
                'type' => 'date',
                "format" => "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
            ],
            'recurrence_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase',
                'null_value' => 'does not repeat'
            ],
            'unavailable' => [
                'properties' => [
                    'first_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'last_name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'email' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ],
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'comment' => [
                'type' => 'keyword',
                'null_value' => 0,
            ]
        ]
    ];
}
