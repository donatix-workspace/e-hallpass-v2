<?php

namespace App\Http\Requests;

use App\Models\User;
use App\Rules\DomainIsValid;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $schoolId = auth()->user()->school_id;
        $existingUserId = null;
        $existingUser = User::where(
            'email',
            $this->request->get('email')
        )->first();
        if ($existingUser) {
            $existingUserId = $existingUser->id;
        }
        return [
            'role_id' => 'required|exists:roles,id',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')
                    ->whereNull('deleted_at')
                    ->ignore($existingUserId),
                new DomainIsValid($schoolId)
            ],
            'grade_year' => 'nullable|sometimes',
            'status' => 'boolean',
            'prevent_archive' => 'nullable|boolean|sometimes',
            'student_sis_id' => [
                'sometimes',
                Rule::unique('schools_users', 'student_sis_id')->where(
                    'school_id',
                    auth()->user()->school_id
                )
            ],
            'auth_type' => 'required|integer|in:1,10,20,101,102,103',
            'kiosk_password' => 'sometimes|min:4|string|nullable',
            'password' => 'sometimes|min:5|string|nullable',
            'send_invite' => 'required_if:auth_type,1|boolean',
            'initials' => 'sometimes'
        ];
    }

    public function messages(): array
    {
        return [
            'student_sis_id.unique' => 'Please provide unique student number.'
        ];
    }
}
