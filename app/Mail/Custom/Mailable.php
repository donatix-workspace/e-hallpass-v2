<?php

namespace App\Mail\Custom;

use App\Models\User;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\PendingMail;
use Log;

class Mailable extends Mailer
{
    private $user;
    private $schoolId = null;

    /**
     * @param array|\Illuminate\Contracts\Mail\Mailable|string $view
     * @param array $data
     * @param null $callback
     * @return bool|void
     */
    public function send($view, array $data = [], $callback = null)
    {
        // Do the check in the database

        $user = User::find(optional($this->user)->id);

        if (optional($user)->isWithWrongSchoolDomain($this->schoolId)) {
            Log::channel('emailsProblem')->alert(
                'The email is with invalid school domain ' .
                    now()->toDateTimeString() .
                    ' UTC. | Email address --- ' .
                    optional($user)->email
            );

            return false;
        }

        if (optional($user)->isBlacklisted()) {
            Log::channel('emailsProblem')->alert(
                'The email from the blacklist was prevented from sending at ' .
                    now()->toDateTimeString() .
                    ' UTC. | Email address --- ' .
                    optional($user)->email
            );

            return false;
        }

        return parent::send($view, $data, $callback);
    }

    /**
     * Begin the process of mailing a mailable class instance.
     *
     * @param mixed $users
     * @param null $schoolId
     * @return PendingMail
     */
    public function to($users, $schoolId = null): PendingMail
    {
        $this->user = $users;
        $this->schoolId = $schoolId;

        return (new PendingMail($this))->to($users);
    }
}
