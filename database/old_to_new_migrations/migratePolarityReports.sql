DROP FUNCTION IF EXISTS getPolarityId;
CREATE FUNCTION `getPolarityId`( id1 BIGINT, id2 BIGINT ) RETURNS bigint(20)
BEGIN
    DECLARE idPol BIGINT DEFAULT 0;

    SELECT id FROM ehp_v2_prod.polarities
    WHERE (first_user_id = (SELECT id FROM ehp_v2_prod.users WHERE old_id=id1 LIMIT 1)
        AND second_user_id = (SELECT id FROM ehp_v2_prod.users WHERE old_id=id2 LIMIT 1))
       OR (first_user_id =  (SELECT id FROM ehp_v2_prod.users WHERE old_id=id2 LIMIT 1)
        AND second_user_id = (SELECT id FROM ehp_v2_prod.users WHERE old_id=id1 LIMIT 1)) LIMIT 1 INTO idPol;

    RETURN idPol;

END;
SET FOREIGN_KEY_CHECKS  = 0;
START TRANSACTION ;
    INSERT INTO ehp_v2_prod.polarities_reports (user_id, destination_type, destination_id, out_user_id, prevented_from, prevented_at, created_at, updated_at, polarity_id, school_id)
    SELECT
        (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldPr.student_prevented limit 1),
        (SELECT IF(oldPr.is_teacher = 1, 'App\\Models\\User', 'App\\Models\\Room')),
        (SELECT IF(oldPr.is_teacher = 1, (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldPr.prevented_destination limit 1), (SELECT id FROM ehp_v2_prod.rooms WHERE old_id = oldPr.prevented_destination limit 1))),
        (SELECT id FROM ehp_v2_prod.users WHERE old_id = oldPr.active_student_id limit 1),
        (SELECT oldPr.origin),
        (SELECT CONVERT_TZ(oldPr.created_at, 'America/New_York', 'UTC')),
        (SELECT oldPr.created_at),
        (SELECT oldPr.updated_at),
        (SELECT getPolarityId(oldPr.student_prevented, oldPr.active_student_id)),
        (SELECT id FROM ehp_v2_prod.schools WHERE old_id = oldPr.school_id limit 1)
    FROM ehallpassDB.ab_polarity_report as oldPr;
COMMIT ;

SET FOREIGN_KEY_CHECKS = 1;
