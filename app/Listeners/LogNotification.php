<?php

namespace App\Listeners;

use App\Events\AppointmentPassRemind;
use App\Events\AppointmentPassSendEmail;
use App\Mail\AppointmentPassRemindMail;
use App\Models\AppointmentPass;
use App\Models\Module;
use App\Models\SchoolModule;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Events\NotificationSent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class LogNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(NotificationSent $event)
    {
        Log::channel('notifications')
            ->info('NOTIFICATION',
                [
                    'channel' => $event->channel,
                    'notifiable' => $event->notifiable,
                    'notification' => $event->notification,
                    'response' => $event->response,
                ]);
    }
}
