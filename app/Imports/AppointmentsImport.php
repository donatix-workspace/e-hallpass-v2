<?php

namespace App\Imports;

use App\Models\AppointmentPass;
use App\Models\Transparency;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AppointmentsImport implements WithHeadings, WithMapping, FromQuery
{
    public $filters;

    /**
     * AppointmentsImport constructor.
     * @param $filters
     */
    public function __construct($filters)
    {
        $this->filters = $filters;
    }

    /**
     * @return mixed
     */
    public function query()
    {
        $user = auth()->user();

        // If there's transparency record show all of the appointments
        if (Transparency::isEnabled()) {
            $appointments = AppointmentPass::transparencyQueries($this->filters, AppointmentPass::recordsPerPage(), false, false, true);
        } else {
            $appointments = AppointmentPass::nonSearchableQueries($this->filters, AppointmentPass::recordsPerPage(), false, false, true);
        }

        if (request()->has('search_query') && request()->get('search_query') !== null) {
            $appointments = AppointmentPass::searchableQueries($this->filters, AppointmentPass::recordsPerPage(), false, Transparency::isEnabled(), true);
        }

        return $appointments;
    }

    /**
     * @return string[]
     */
    public function headings(): array
    {
        return [
            'Student Name',
            'Date/Recurrence',
            'Period & Time',
            'From',
            'To',
            'Pass Creator',
            'Notes  clickable',
            'Pass Status'
        ];
    }

    /**
     * @param mixed $row
     * @return array
     */
    public function map($row): array
    {
        return [
            $row->user->first_name . ' ' . $row->user->last_name,
            $row->for_date->toDateString(),
            $row->period->name . ' ' . $row->for_date->format('g:i A'),
            optional($row->from)->first_name . ' ' . optional($row->from)->last_name,
            optional($row->to)->first_name . ' ' . optional($row->to)->last_name,
            optional($row->createdByUser)->first_name . ' ' . optional($row->createdByUser)->last_name . ' ' . $row->created_at->format('m/d/Y g:i A'),
            $row->reason,
            'Today\'s Appointments'
        ];
    }
}
