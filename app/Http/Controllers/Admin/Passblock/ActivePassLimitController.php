<?php

namespace App\Http\Controllers\Admin\Passblock;

use App\Http\Controllers\Controller;
use App\Http\Requests\GlobalPassLimitRequest;
use App\Http\Resources\ActivePassLimitResource;
use App\Models\ActivePassLimit;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;

/**
 * @group Active Pass Limits manage.
 */
class ActivePassLimitController extends Controller
{
    /**
     * Return the active pass limit information.
     *
     * @authenticated
     * @return ActivePassLimitResource
     * @throws AuthorizationException
     */
    public function show()
    {
        $user = auth()->user();

        $this->authorize('viewAny', ActivePassLimit::class);

        $globalPassLimit = ActivePassLimit::fromSchoolId($user->school_id)
            ->first();

        return new ActivePassLimitResource($globalPassLimit);
    }

    /**
     * Create or Update a active pass limit
     *
     * @bodyParam limit required integer The limit of the active pass
     * @bodyParam proxy_pass required boolean Does this limit is gonna be applied to proxy pass creation
     * @bodyParam student_pass required boolean Does this limit is gonna be applied to student pass creation
     * @bodyParam kiosk_pass required boolean Does this limit is gonna be applied to kiosk pass creation.
     * @bodyParam teacher_override required boolean Can teacher override the existing active pass limit
     *
     * @authenticated
     * @param GlobalPassLimitRequest $request
     * @return ActivePassLimitResource|JsonResponse
     * @throws AuthorizationException
     */
    public function storeOrUpdate(GlobalPassLimitRequest $request)
    {
        $user = auth()->user();

        $this->authorize('create', ActivePassLimit::class);

        // Delete the limit when it's null
        if ($request->input('limit') === null) {
            $activePassLimitDelete = ActivePassLimit::whereSchoolId($user->school_id)
                ->delete();

            return response()->json([
                'data' => [],
                'message' => __('passes.active.pass.limit.deleted'),
                'status' => __('success')
            ]);
        }

        if (!$request->input('proxy_pass') && !$request->input('kiosk_pass') && !$request->input('student_pass')) {
            return response()->json([
                'data' => [],
                'message' => __('At least one pass type should be selected'),
                'status' => __('fail')
            ], 422);
        }

        $globalPassLimit = ActivePassLimit::updateOrCreate([
            'school_id' => $user->school_id,
        ], [
            'proxy_pass' => $request->input('proxy_pass'),
            'student_pass' => $request->input('student_pass'),
            'kiosk_pass' => $request->input('kiosk_pass'),
            'teacher_override' => $request->input('teacher_override'),
            'limit' => $request->input('limit') === "-" ? -1 : $request->input('limit')
        ]);

        return new ActivePassLimitResource($globalPassLimit);
    }

}
