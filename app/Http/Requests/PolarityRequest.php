<?php

namespace App\Http\Requests;

use App\Models\Polarity;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PolarityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = auth()->user();

        return optional($user)->isAdmin() || optional($user)->isTeacher() || optional($user)->isStaff();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_user_id' => ['integer', 'required', Rule::exists('users', 'id')
                ->where(function ($query) {
                    $query->where('school_id', auth()->user()->school_id);
                    $query->where('role_id', config('roles.student'));
                })
            ],
            'second_user_id' => ['integer', 'required', 'different:first_user_id', Rule::exists('users', 'id')
                ->where(function ($query) {
                    $query->where('school_id', auth()->user()->school_id);
                    $query->where('role_id', config('roles.student'));
                })],
            'status' => 'boolean|required',
            'message' => 'max:255'
        ];
    }
}
