<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return 'OK';
});

Route::get('/app-check', function () {
    $fails = [];

    try {
        Illuminate\Support\Facades\Redis::connection('default');
    } catch (RedisException $e) {
        $fails[] = 'REDIS';

        Log::channel('healthcheck')->error($e->getMessage());
    }
    try {
        DB::connection()
            ->table('migrations')
            ->where('id', '>', 1)
            ->where('id', '<', 10)
            ->first();
    } catch (\Exception $e) {
        $fails[] = 'DB';
        Log::channel('healthcheck')->error($e->getMessage());
    }
    try {
        $body = Http::get('https://ws.e-hallpass.com/')->body();
        if ($body !== 'OK') {
            $fails[] = 'SOCKET';
        }
    } catch (\Exception $e) {
        $fails[] = 'SOCKET';

        Log::channel('healthcheck')->error($e->getMessage());
    }

    return !count($fails) ? 'OK' : response()->json(['fails' => $fails], 500);
});

Route::get('/socketTest', function () {
    event(new \App\Events\TestEvent('hello'));
});

Route::group(
    ['middleware' => 'horizonBasicAuth', 'prefix' => 'api/v5/cross-check'],
    function () {
        Route::get('/schools/{school}', 'Admin\Misc\CrossCheckController@schoolShow')->name(
            'api.v5.crosscheck.schools.show'
        );
        Route::get('/schools', 'Admin\Misc\CrossCheckController@schools')->name(
            'api.v5.crosscheck.schools.index'
        );
        Route::get('/schools/{school}/verify/{status}', 'Admin\Misc\CrossCheckController@verifySchool')->name(
            'api.v5.crosscheck.schools.verify'
        );
        Route::get('/schools/{school}/image', 'Admin\Misc\CrossCheckController@migrateImage')->name(
            'api.v5.crosscheck.schools.image'
        );
        Route::get('/schools/{school}/modules', 'Admin\Misc\CrossCheckController@migrateModule')->name(
            'api.v5.crosscheck.schools.modules'
        );

        Route::get('/', 'Admin\Misc\CrossCheckController@index')->name('api.v5.crosscheck.index');
        Route::get('/{school}', 'Admin\Misc\CrossCheckController@show')->name(
            'api.v5.crosscheck.show'
        );
        Route::get('/{school}/migrate/{oldId}', 'Admin\Misc\CrossCheckController@migrate')->name(
            'api.v5.crosscheck.migrate'
        );
        Route::get('/{school}/migrateAll', 'Admin\Misc\CrossCheckController@migrateAll')->name(
            'api.v5.crosscheck.migrateAll'
        );
        Route::get('/{school}/verify/{status}', 'Admin\Misc\CrossCheckController@verify')->name(
            'api.v5.crosscheck.verify'
        );
        Route::get('/{school}/delete/{user}', 'Admin\Misc\CrossCheckController@delete')->name(
            'api.v5.crosscheck.delete'
        );
        Route::get('/{school}/deleteCus/{membership}', 'Admin\Misc\CrossCheckController@deleteMembership')->name(
            'api.v5.crosscheck.deleteMembership'
        );
        Route::get('/{school}/deleteAll/{ids}', 'Admin\Misc\CrossCheckController@deleteAll')->name(
            'api.v5.crosscheck.deleteAll'
        );
        Route::get('/{school}/sync/{memberships}', 'Admin\Misc\CrossCheckController@sync')->name(
            'api.v5.crosscheck.sync'
        );
        Route::get('/{school}/deleteAllCus/{memberships}', 'Admin\Misc\CrossCheckController@deleteAllCus')->name(
            'api.v5.crosscheck.deleteAllCus'
        );
        Route::get('/{school}/syncwithv1/{user}', 'Admin\Misc\CrossCheckController@syncwithv1')->name(
            'api.v5.crosscheck.syncwithv1'
        );


    }
);
