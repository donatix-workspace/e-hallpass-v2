<?php

namespace Database\Factories;

use App\Models\School;
use App\Models\Transparency;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class TransparencyFactory extends Factory
{
    protected $model = Transparency::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'school_id' => function () {
                return School::factory()->create()->id;
            },
        ];
    }
}
