<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminTokenToAppointmentsEmailTokensTable extends Migration
{
    public function up()
    {
        Schema::table('appointments_email_tokens', function (Blueprint $table) {
            $table->text('admin_token')->nullable();
        });
    }

    public function down()
    {
        Schema::table('appointments_email_tokens', function (Blueprint $table) {
            $table->dropColumn('admin_token');
        });
    }
}
