<?php

namespace Database\Factories;

use App\Models\UserImport;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserImportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserImport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'school_id' => 1,
            'status' => 1,
            'inserted' => 1,
            'role_id' => config('roles.student'),
            'file_name' => $this->faker->fileExtension
        ];
    }
}
