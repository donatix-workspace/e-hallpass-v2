<?php

namespace App\Policies;

use App\Models\PassBlock;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PassBlockPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * @param User $user
     * @param PassBlock $passBlock
     * @return bool
     */
    public function update(User $user, PassBlock $passBlock)
    {
        return $user->isAdmin() && $user->school_id === $passBlock->school_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param PassBlock $passBlock
     * @return mixed
     */
    public function delete(User $user, PassBlock $passBlock)
    {
        return $user->isAdmin() && $passBlock->school_id === $user->school_id;
    }

}
