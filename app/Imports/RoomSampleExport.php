<?php

namespace App\Imports;

use App\Models\Room;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RoomSampleExport implements FromArray, WithHeadings
{
    /**
     * @return \string[][]
     */
    public function array(): array
    {
        return [
            ['Room name 1', Room::TRIP_ROUNDTRIP, Room::COMMENT_MANDATORY, 'yes'],
            ['Room name 2', Room::TRIP_ONE_WAY, Room::COMMENT_OPTIONAL, 'no'],
            ['Room name 3', Room::TRIP_LAYOVER, Room::COMMENT_HIDDEN, 'yes'],
        ];
    }

    /**
     * @return string[]
     */
    public function headings(): array
    {
        return [
            'Room Name',
            'Trip Type',
            'Comment',
            'Allow Student APT Request'
        ];
    }
}
