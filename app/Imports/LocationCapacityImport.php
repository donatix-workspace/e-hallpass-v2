<?php

namespace App\Imports;

use App\Models\LocationCapacity;
use App\Models\PassLimit;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class LocationCapacityImport implements FromQuery, WithHeadings, WithMapping
{
    public $locationCapacityQuery;

    /**
     *
     */
    public function __construct($locationCapacityQuery)
    {
        $this->locationCapacityQuery = $locationCapacityQuery;
    }

    /**
     * @return PassLimit|\Illuminate\Database\Eloquent\Builder|Builder
     */
    public function query()
    {
        return $this->locationCapacityQuery;
    }

    /**
     * @return string[]
     */
    public function headings(): array
    {
        return ['Location', 'Limit'];
    }

    /**
     * @param mixed $row
     * @return array
     */
    public function map($row): array
    {
        return [$row->room->name, $row->limit];
    }
}
