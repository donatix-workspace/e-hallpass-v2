<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPolarityIdToPolaritiesReportsTable extends Migration
{
    public function up()
    {
        Schema::table('polarities_reports', function (Blueprint $table) {
            $table->unsignedBigInteger('polarity_id');

            $table->foreign('polarity_id')->references('id')->on('polarities')->onDelete('cascade');

            $table->unsignedBigInteger('school_id');

            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
        });
    }

    public function down()
    {

    }
}
