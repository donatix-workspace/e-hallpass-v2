<?php

namespace App\Http\Requests;

use App\Models\Room;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LocationCapacityRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'room_id' => ['required', 'integer', Rule::exists('rooms', 'id')->where(function ($query) {
                $query->where('school_id', auth()->user()->school_id)
                    ->where('status', Room::ACTIVE);
            })
            ],
            'limit' => 'integer|min:-1|nullable|sometimes',
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && (auth()->user()->isAdmin() || auth()->user()->isStaff());
    }

    public function messages(): array
    {
        return [
            'limit.integer' => 'The limit must a number or a “-” (where a “-” means unlimited).',
        ];
    }
}
