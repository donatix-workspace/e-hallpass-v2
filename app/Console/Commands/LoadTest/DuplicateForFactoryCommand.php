<?php

namespace App\Console\Commands\LoadTest;

use App\Jobs\CreateCommonUserJob;
use App\Models\AuthType;
use App\Models\User;
use DB;
use Illuminate\Console\Command;

class DuplicateForFactoryCommand extends Command
{
    protected $signature = 'load_test:factory {--authenticatedUserId=}';

    private const SCHOOL_FACTORY_NUMBER = 6;

    protected $description = 'Create test factory accounts for load testing';

    /*
     * // UUid's and schools
    '0ac5f8d9-a208-4e06-ab21-6e8a23c2220d', //Olathe North High School, 'eb5584b5-4ab2-4b62-973b-1d2ce1d2e101'
    '6047b7c6-0019-45b9-8d73-2a11c58ef608', //Lee's Summit North High School, '72a31920-2847-40d6-a640-fd6d70779435'
    'aa692c75-3419-480f-a442-79f2801c0909', //Governor Mifflin High School, '9a8e009a-0469-4c4b-9bc6-baff3efe47fa'
    'fd634553-696e-49aa-81bc-2223c4a7913f', //Lorain County JVS ,'5b19dea0-be14-47ce-8cad-59d443541e30'
    'ea3d4263-ae9f-4c3c-83e7-15767e733c64', //Uplift Luna Perparatory Middle School (kiosk), 'bd71f49c-b037-4497-b7df-f020a24dcc0b'
    'db04373d-7efa-47ad-a7e2-308ab56d9fcf', //GNB Voc-Tech High School (Proxy), ', '63a829d6-35ce-4269-9e0f-828585f890ff'
     */

    public function handle(): int
    {
        $oldCUSUuids = [
            '0ac5f8d9-a208-4e06-ab21-6e8a23c2220d' => [
                'newUuid' => 'eb5584b5-4ab2-4b62-973b-1d2ce1d2e101'
            ],
            '6047b7c6-0019-45b9-8d73-2a11c58ef608' => [
                'newUuid' => '72a31920-2847-40d6-a640-fd6d70779435'
            ],
            'aa692c75-3419-480f-a442-79f2801c0909' => [
                'newUuid' => '9a8e009a-0469-4c4b-9bc6-baff3efe47fa'
            ],
            'fd634553-696e-49aa-81bc-2223c4a7913f' => [
                'newUuid' => '5b19dea0-be14-47ce-8cad-59d443541e30'
            ],
            'ea3d4263-ae9f-4c3c-83e7-15767e733c64' => [
                'newUuid' => 'bd71f49c-b037-4497-b7df-f020a24dcc0b'
            ],
            'db04373d-7efa-47ad-a7e2-308ab56d9fcf' => [
                'newUuid' => '63a829d6-35ce-4269-9e0f-828585f890ff'
            ]
        ];

        $db = DB::connection();
        $db->disableQueryLog();

        $this->info(
            'Creating factory for schools with more data for load testing....'
        );

        $this->output->progressStart(self::SCHOOL_FACTORY_NUMBER);

        foreach ($oldCUSUuids as $oldUuid => $data) {
            $school = $db
                ->table('schools')
                ->where('cus_uuid', $oldUuid)
                ->first();

            if ($school !== null) {
                $db->table('schools')
                    ->where('cus_uuid', $oldUuid)
                    ->update([
                        'cus_uuid' => $data['newUuid'],
                        'name' => 'Test ' . $school->name
                    ]);

                $defaultPassword = 'secret2020';

                $users = User::withoutGlobalScopes()
                    ->where('school_id', $school->id)
                    ->get();

                $this->newLine();
                $this->info('Registering the new users in the test school...');

                foreach ($users as $user) {
                    $user->update(['email' => 'test-' . $user->email]);
                    CreateCommonUserJob::dispatchSync([
                        'user' => $user,
                        'authenticatedUser' => User::find(
                            $this->option('authenticatedUserId')
                        ),
                        'password' => trim($defaultPassword),
                        'sendInvite' => null
                    ]);
                }

                $this->output->progressAdvance();
            }
        }

        $this->output->progressFinish();

        return 0;
    }
}
