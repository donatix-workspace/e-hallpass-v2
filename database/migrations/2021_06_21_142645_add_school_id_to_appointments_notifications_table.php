<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSchoolIdToAppointmentsNotificationsTable extends Migration
{
    public function up()
    {
        Schema::table('appointments_notifications', function (Blueprint $table) {
            $table->unsignedBigInteger('school_id')
                ->nullable();
            $table->foreign('school_id')->references('id')->on('schools')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('appointments_notifications', function (Blueprint $table) {
            //
        });
    }
}
