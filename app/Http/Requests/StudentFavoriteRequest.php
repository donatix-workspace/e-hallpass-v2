<?php

namespace App\Http\Requests;

use App\Rules\FavoriteRoomOrUserExistsRule;
use App\Rules\RoomOrUserExistsRule;
use App\Rules\RoomOrUserStringRule;
use Illuminate\Foundation\Http\FormRequest;

class StudentFavoriteRequest extends FormRequest
{
    public function rules()
    {
        return [
            'favorable_type' => ['required','string', new RoomOrUserStringRule()],
            'favorable_id' => ['required', 'integer', new FavoriteRoomOrUserExistsRule(request()->all())],
        ];
    }

    public function authorize()
    {
        return auth()->check() && auth()->user()->isStudent();
    }
}
