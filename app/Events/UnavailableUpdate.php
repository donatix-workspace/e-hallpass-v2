<?php

namespace App\Events;

use App\Models\Unavailable;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UnavailableUpdate implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $unavailable;
    public $unavailableStatus;
    public $userId;

    /**
     * UnavailableUpdate constructor.
     * @param Unavailable|null $unavailable
     * @param bool $unavailableStatus
     * @param int|null $userId
     */
    public function __construct(
        ?Unavailable $unavailable,
        bool $unavailableStatus,
        ?int $userId = null
    ) {
        $this->unavailable = $unavailable;
        $this->unavailableStatus = $unavailableStatus;
        $this->userId = $userId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array
     */
    public function broadcastOn(): array
    {
        $user = User::find($this->userId);
        $schoolId =
            optional($this)->userId !== null
                ? optional($user)->school_id
                : optional($this->unavailable)->school_id;
        $userId =
            optional($this)->userId !== null
                ? optional($this)->userId
                : optional($this->unavailable)->unavailable_id;
        return [
            new PrivateChannel('unavailables.' . $schoolId),
            new PrivateChannel('user.unavailables.' . $userId)
        ];
    }
}
