<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransparenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transparencies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id');
            $table->timestamps();
        });

        Schema::table('transparencies', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('transparencies', function (Blueprint $table) {
            $table->foreign('transparencies_school_id_foreign');
        });
        Schema::dropIfExists('transparencies');
    }
}
