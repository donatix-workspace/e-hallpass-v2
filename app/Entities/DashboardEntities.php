<?php

namespace App\Entities;

use App\Models\AppointmentPass;
use App\Models\AutoPass;
use App\Models\AutoPassLimit;
use App\Models\AutoPassPreference;
use App\Models\Kiosk;
use App\Models\Pass;
use App\Models\PassLimit;
use App\Models\Room;
use App\Models\School;
use App\Models\StaffSchedule;
use App\Models\Transparency;
use App\Models\Unavailable;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class DashboardEntities
{
    /**
     * Return all of the counters and needed data for admin dashboard
     *
     * @return array
     */
    public static function get()
    {
        $user = auth()->user();

        $staffSchedules = StaffSchedule::fromSchoolId($user->school_id)
            ->where('status', AutoPass::ACTIVE)
            ->ofUserId($user->id)
            ->get()
            ->append(['limit', 'room_advance']);

        $autoPassesRooms = AutoPass::fromSchoolId($user->school_id)
            ->where('status', AutoPass::ACTIVE)
            ->get()
            ->append(['limit', 'room_advance']);

        $autoPassPreferences = AutoPassPreference::with('autoPass')
            ->where('user_id', $user->id)
            ->fromSchoolId($user->school_id)
            ->whereHas('autoPass', function ($query) {
                $query->where('status', AutoPass::ACTIVE);
            })
            ->get();

        $unavailable = Cache::remember(
            'dashboard_unavailable_adult_' . $user->id,
            now()->addSeconds(30),
            function () use ($user) {
                return Unavailable::active()
                    ->fromSchoolId($user->school_id)
                    ->where('unavailable_type', User::class)
                    ->where('unavailable_id', $user->id)
                    ->where(
                        'from_date',
                        '<=',
                        Carbon::now()->toDateTimeString()
                    )
                    ->where('to_date', '>=', Carbon::now()->toDateTimeString())
                    ->where('canceled_for_today_at', null)
                    ->first();
            }
        );

        return [
            'autoPassesRooms' => $autoPassesRooms,
            'staffSchedules' => $staffSchedules,
            'autoPassesPreference' => $autoPassPreferences,
            'unavailable' => $unavailable,
            'stats' => CountersEntities::get()
        ];
    }
}
