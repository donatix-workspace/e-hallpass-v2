<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned()->nullable()->default(0)->index('user_id');
            $table->bigInteger('pass_id')->unsigned()->nullable()->index('pass_id');
            $table->text('message')->nullable();
            $table->tinyInteger('status')->nullable()->default(1)->index('status');
            $table->smallInteger('alert')->nullable()->index('alert');
            $table->bigInteger('to_teacher_id')->unsigned()->nullable();
            $table->bigInteger('from_teacher_id')->unsigned()->nullable();
            $table->bigInteger('school_id')->unsigned()->nullable()->index('school_id');
            $table->boolean('to_teacher_state')->nullable()->default(1);
            $table->boolean('from_teacher_state')->nullable()->default(1);
            $table->string('token')->index('token');
            $table->boolean('bulk_status')->default(0);
            $table->boolean('sms')->default(0);
            $table->boolean('email')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
