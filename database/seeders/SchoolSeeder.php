<?php

namespace Database\Seeders;

use App\Models\School;
use Illuminate\Database\Seeder;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $school = School::factory()->create();
        \DB::table('domains')->insert([
            [
                'school_id' => $school->id,
                'name' => $school->student_domain,
            ],
            [
                'school_id' => $school->id,
                'name' => $school->school_domain
            ]
        ]);
    }
}
