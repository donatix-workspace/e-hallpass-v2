<?php

namespace Database\Seeders;

use App\Models\Kiosk;
use App\Models\Role;
use App\Models\Room;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Seeder;

class KioskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $randomRoom = Room::inRandomOrder()->first();
        $randomTeacher = User::where('role_id', '3')->first();
        $randomSchool = School::inRandomOrder()->first();
        $kiosks = Kiosk::factory()
            ->count(20)
            ->create([
                'room_id' => $randomRoom->id,
                'user_id' => $randomTeacher->id,
                'school_id' => $randomSchool->id,
            ]);
        $kiosks->each(function ($kiosk) {
            $randomRoom = Room::inRandomOrder()->first();
            $randomTeacher = User::where('role_id', '3')->first() ?: '9999';
            $randomSchool = School::inRandomOrder()->first();

            $kiosk->update([
                'room_id' => $randomRoom->id,
                'user_id' => $randomTeacher->id,
                'school_id' => $randomSchool->id,
            ]);
        });
    }
}
