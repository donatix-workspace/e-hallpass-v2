<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeReasonToPassLimitsTable extends Migration
{
    public function up()
    {
        Schema::table('pass_limits', function (Blueprint $table) {
            $table->string('reason')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('pass_limits', function (Blueprint $table) {
            $table->string('reason');
        });
    }
}
