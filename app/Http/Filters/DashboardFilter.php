<?php

namespace App\Http\Filters;

use App\Models\Pass;
use App\Models\Room;
use App\Models\StaffSchedule;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardFilter extends QueryFilters
{
    protected $request;
    protected $passTimeSettings;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->passTimeSettings = TeacherPassSetting::fromSchoolId(
            $request->user()->school_id
        )->first();
        parent::__construct($request);
    }

    //    /**
    //     * @param $sort
    //     * @return mixed
    //     */
    //    public function sort($sort)
    //    {
    //        $sortingKeywords = ['asc', 'desc'];
    //
    //        if (in_array($sort, $sortingKeywords)) {
    //            return $this->builder->orderBy('created_at', $sort);
    //        }
    //
    //        return $this->builder;
    //    }

    /**
     * @param $status
     * @return mixed
     */
    public function expired($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query->where('passes.expired_at', '!=', null);
            })
            : $this->builder->where(function ($query) {
                return $query->where('passes.expired_at', null);
            });
    }

    /**
     * @param $filters
     * @return \Illuminate\Database\Query\Builder
     */
    public function filters_list($filters)
    {
        $filters = json_decode($filters);

        return $this->builder->where(function ($query) use ($filters) {
            $query
                ->when(in_array('system_ended', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->orWhere('passes.system_completed', Pass::ACTIVE)
                            ->where('passes.extended_at', null)
                            ->orWhere(function ($query) {
                                $query
                                    ->where(function ($query) {
                                        $query
                                            ->where(
                                                'child.system_completed',
                                                Pass::ACTIVE
                                            )
                                            ->where('child.extended_at', null);
                                    })
                                    ->where(
                                        'passes.system_completed',
                                        Pass::INACTIVE
                                    )
                                    ->where('passes.extended_at', null);
                            });
                    });
                })
                ->when(in_array('yellow_time', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('passes.flagged_at', '!=', null)
                            ->where('passes.system_completed', Pass::INACTIVE)
                            ->whereDoesntHave('child')
                            ->orWhere(function ($query) {
                                $query
                                    ->where('child.flagged_at', '!=', null)
                                    ->where(
                                        'child.school_id',
                                        auth()->user()->school_id
                                    )
                                    ->where(
                                        'child.system_completed',
                                        Pass::INACTIVE
                                    );
                            })
                            ->orWhere(function ($query) {
                                $query
                                    ->where(function ($query) {
                                        $query
                                            ->where(
                                                'child.school_id',
                                                auth()->user()->school_id
                                            )
                                            ->where('child.flagged_at', null)
                                            ->where('child.extended_at', null)
                                            ->where(
                                                'child.system_completed',
                                                Pass::INACTIVE
                                            );
                                    })
                                    ->where('passes.flagged_at', '!=', null)
                                    ->where(
                                        'passes.system_completed',
                                        Pass::INACTIVE
                                    );
                            });
                    });
                })
                ->when(in_array('extended_time', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('passes.extended_at', '!=', null)
                            ->where(
                                'passes.system_completed',
                                '=',
                                Pass::ACTIVE
                            )
                            ->orWhere(function ($query) {
                                $query
                                    ->where('child.extended_at', '!=', null)
                                    ->where(
                                        'child.system_completed',
                                        '=',
                                        Pass::ACTIVE
                                    );
                            });
                    });
                })
                ->when(in_array('edited', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query->where('passes.edited_at', '!=', null);
                    });
                })
                ->when(in_array('only_missed_passes', $filters), function (
                    $query
                ) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('passes.expired_at', '!=', null)
                            ->where('passes.approved_at', null);
                    });
                })
                ->when(in_array('only_canceled_passes', $filters), function (
                    $query
                ) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('passes.canceled_at', '!=', null)
                            ->where('passes.approved_at', null)
                            ->where('passes.expired_at', null);
                    });
                })
                ->when(in_array('missed_request', $filters), function ($query) {
                    $query->orWhere(function ($query) {
                        $query
                            ->where('passes.pass_status', Pass::INACTIVE)
                            ->where('passes.approved_at', '!=', null)
                            ->where('passes.extended_at', null)
                            ->where('passes.flagged_at', null)
                            ->where('passes.expired_at', '!=', null);
                    });
                })
                ->when(in_array('waiting_approval', $filters), function (
                    $query
                ) {
                    $query->orWhere(function ($query) {
                        return $query
                            ->where('passes.approved_at', null)
                            ->where('passes.pass_status', Pass::ACTIVE);
                    });
                })
                ->when(
                    !in_array('waiting_approval', $filters) &&
                        request()->has('ended_passes'),
                    function ($query) {
                        $query->where('passes.pass_status', Pass::INACTIVE);
                    }
                );
        });
    }

    /**
     * @param $status
     * @return mixed
     */
    public function only_active($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query
                    ->where('passes.pass_status', Pass::ACTIVE)
                    ->orWhere(function ($query) {
                        $query->where(function ($child) {
                            $child->where('child.pass_status', Pass::ACTIVE);
                        });
                    });
            })
            : $this->builder->where(function ($query) {
                return $query->where('passes.pass_status', Pass::INACTIVE);
            });
    }

    /**
     * @param $status
     * @return mixed
     */
    public function both($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query
                    ->where('passes.pass_status', Pass::ACTIVE)
                    ->orWhere(function ($query) {
                        $query->where(function ($query) {
                            $query->where('child.pass_status', Pass::ACTIVE);
                        });
                    })
                    ->orWhere(function ($query) {
                        $query->where(function ($query) {
                            $query
                                ->where('passes.completed_at', '!=', null)
                                ->where(function ($query) {
                                    $query
                                        ->where(function ($query) {
                                            $query
                                                ->where(
                                                    'child.completed_at',
                                                    '!=',
                                                    null
                                                )
                                                ->orWhere(
                                                    'child.pass_status',
                                                    Pass::INACTIVE
                                                )
                                                ->where(
                                                    'child.approved_at',
                                                    '!=',
                                                    null
                                                );
                                        })
                                        ->orWhereDoesntHave('child');
                                });
                        });
                    });
            })
            : $this->builder->where(function ($query) {
                return $query->where('passes.pass_status', Pass::INACTIVE);
            });
    }

    /**
     * @param $status
     * @return mixed
     */
    public function only_my_active($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query
                    ->where('passes.pass_status', Pass::ACTIVE)
                    ->where(function ($query) {
                        return $query
                            ->where('passes.approved_by', auth()->user()->id)
                            ->orWhere(
                                'passes.completed_by',
                                auth()->user()->id
                            );
                    });
            })
            : $this->builder->where(function ($query) {
                return $query->where('passes.pass_status', Pass::ACTIVE);
            });
    }

    /**
     * @param $status
     * @return mixed
     */
    public function only_my_passes($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        $staffSchedulesFromRoomIds = StaffSchedule::where(
            'user_id',
            auth()->user()->id
        )
            ->get()
            ->pluck('room_id');

        $staffSchedulesToRoomIds = StaffSchedule::where(
            'user_id',
            auth()->user()->id
        )
            ->get()
            ->pluck('room_id');

        return $status
            ? $this->builder->where(function ($query) use (
                $staffSchedulesToRoomIds,
                $staffSchedulesFromRoomIds
            ) {
                return $query
                    ->where(function ($query) {
                        $query
                            ->where('passes.approved_by', auth()->user()->id)
                            ->orWhere(
                                'passes.completed_by',
                                auth()->user()->id
                            );
                    })
                    ->orWhere(function ($query) {
                        $query
                            ->where('passes.from_id', auth()->user()->id)
                            ->where('passes.from_type', User::class);
                    })
                    ->orWhere(function ($query) {
                        $query
                            ->where('passes.to_id', auth()->user()->id)
                            ->where('passes.to_type', User::class);
                    })
                    ->orWhere(function ($query) use (
                        $staffSchedulesFromRoomIds
                    ) {
                        $query
                            ->where('passes.from_type', Room::class)
                            ->whereIntegerInRaw(
                                'passes.from_id',
                                $staffSchedulesFromRoomIds
                            );
                    })
                    ->orWhere(function ($query) use ($staffSchedulesToRoomIds) {
                        $query
                            ->where('passes.to_type', Room::class)
                            ->whereIntegerInRaw(
                                'passes.to_id',
                                $staffSchedulesToRoomIds
                            );
                    })
                    ->orWhere(function ($query) use (
                        $staffSchedulesFromRoomIds,
                        $staffSchedulesToRoomIds
                    ) {
                        $query
                            ->where(function ($query) {
                                $query
                                    ->where(
                                        'child.approved_by',
                                        auth()->user()->id
                                    )
                                    ->orWhere(
                                        'child.completed_by',
                                        auth()->user()->id
                                    );
                            })
                            ->orWhere(function ($query) {
                                $query
                                    ->where('child.from_id', auth()->user()->id)
                                    ->where('child.from_type', User::class);
                            })
                            ->orWhere(function ($query) {
                                $query
                                    ->where('child.to_id', auth()->user()->id)
                                    ->where('child.to_type', User::class);
                            })
                            ->orWhere(function ($query) use (
                                $staffSchedulesFromRoomIds
                            ) {
                                $query
                                    ->where('child.from_type', Room::class)
                                    ->whereIntegerInRaw(
                                        'child.from_id',
                                        $staffSchedulesFromRoomIds
                                    );
                            })
                            ->orWhere(function ($query) use (
                                $staffSchedulesToRoomIds
                            ) {
                                $query
                                    ->where('child.to_type', Room::class)
                                    ->whereIntegerInRaw(
                                        'child.to_id',
                                        $staffSchedulesToRoomIds
                                    );
                            });
                    });
            })
            : $this->builder->where(function ($query) {
                return $this->builder
                    ->where('passes.approved_at', '!=', null)
                    ->orWhere('passes.completed_at', '!=', null);
            });
    }

    /**
     * @param $status
     * @return mixed
     */
    public function ended_passes($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status
            ? $this->builder->where(function ($query) {
                return $query->where(function ($query) {
                    $query
                        ->where('passes.completed_at', '!=', null)
                        ->where(function ($query) {
                            $query
                                ->where(function ($query) {
                                    $query
                                        ->where(
                                            'child.completed_at',
                                            '!=',
                                            null
                                        )
                                        ->orWhere(
                                            'child.pass_status',
                                            Pass::INACTIVE
                                        )
                                        ->where(
                                            'child.approved_at',
                                            '!=',
                                            null
                                        );
                                })
                                ->orWhereDoesntHave('child');
                        });
                });
            })
            : $this->builder->where(function ($query) {
                return $query->where('passes.completed_at', '=', null);
            });
    }

    /**
     * @param $students
     * @return mixed
     */
    public function by_students($students)
    {
        $students = explode(',', $students);
        return $this->builder->where(function ($query) use ($students) {
            return $query->whereIntegerInRaw('passes.user_id', $students);
        });
    }

    /**
     * @param $teachers
     * @return mixed
     */
    public function by_teachers($teachers)
    {
        $teachers = explode(',', $teachers);
        return $this->builder->where(function ($query) use ($teachers) {
            return $query
                ->whereIntegerInRaw('passes.approved_by', $teachers)
                ->orWhereIntegerInRaw('passes.completed_by', $teachers)
                ->orWhere(function ($query) use ($teachers) {
                    $query
                        ->where('passes.to_type', User::class)
                        ->whereIntegerInRaw('passes.to_id', $teachers)
                        ->orWhere(function ($query) use ($teachers) {
                            $query
                                ->where('passes.from_type', User::class)
                                ->whereIntegerInRaw(
                                    'passes.from_id',
                                    $teachers
                                );
                        });
                });
        });
    }

    /**
     * @param $rooms
     * @return mixed
     */
    public function by_rooms($rooms)
    {
        $rooms = explode(',', $rooms);

        return $this->builder->where(function ($query) use ($rooms) {
            return $query
                ->where('passes.to_type', Room::class)
                ->whereIntegerInRaw('passes.to_id', $rooms);
        });
    }
}
