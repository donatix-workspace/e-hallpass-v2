<?php

namespace App\Http\Filters;

use App\Models\User;
use Illuminate\Http\Request;

class PassLimitFilter extends QueryFilters
{

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;

        parent::__construct($request);
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function inactive($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status ? $this->builder->where(function ($query) {
            return $query->where('archived', 1);
        }) : $this->builder;
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function active($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status ? $this->builder->where(function ($query) {
            return $query->where('archived', 0);
        }) : $this->builder;
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function individual_students($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);

        return $status ? $this->builder->where(function ($query) {
            return $query->where('limitable_type', User::class)
                ->when(request()->get('group_limits'), function ($query) {
                    return $query->orWhere('limitable_type', '!=', User::class);
                });
        }) : $this->builder;
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Query\Builder
     */
    public function group_limits($status)
    {
        $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);


        return $status ? $this->builder->where(function ($query) {
            return $query->where('limitable_type', '!=', User::class)
                ->when(request()->get('individual_students'), function ($query) {
                    return $query->orWhere('limitable_type', User::class);
                });
        }) : $this->builder;
    }

}
