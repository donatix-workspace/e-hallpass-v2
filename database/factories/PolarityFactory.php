<?php

namespace Database\Factories;

use App\Models\Polarity;
use Illuminate\Database\Eloquent\Factories\Factory;

class PolarityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Polarity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'message' => $this->faker->text,
            'status' => 1,
        ];
    }
}
