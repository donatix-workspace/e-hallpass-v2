<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimestampToPassSummaryReports extends Migration
{
    public function up()
    {
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->timestamp('last_synced_at');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::table('pass_summary_reports', function (Blueprint $table) {
            $table->dropColumn('last_synced_at');
        });
    }
}
