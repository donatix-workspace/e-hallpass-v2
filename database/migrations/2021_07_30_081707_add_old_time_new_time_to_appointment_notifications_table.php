<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOldTimeNewTimeToAppointmentNotificationsTable extends Migration
{
    /**
     *
     */
    public function up()
    {
        Schema::table('appointments_notifications', function (Blueprint $table) {
            $table->timestamp('old_time')->nullable();
            $table->timestamp('new_time')->nullable();
        });
    }

    /**
     *
     */
    public function down()
    {
        Schema::table('appointment_notifications', function (Blueprint $table) {
            //
        });
    }
}
