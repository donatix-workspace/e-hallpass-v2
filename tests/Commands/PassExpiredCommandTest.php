<?php

namespace Tests\Commands;

use App\Models\Pass;
use App\Models\School;
use App\Models\TeacherPassSetting;
use App\Models\User;
use Carbon\Carbon;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PassExpiredCommandTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;
    protected $teacherUser;
    protected $firstUser;
    protected $secondUser;
    protected $settings;
    protected $pass;

    protected function setUp(): void
    {
        parent::setUp();


        // Run roles migration
        (new RolesDatabaseSeeder())->run();

        $this->school = School::factory()->createOne();

        $this->firstUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->secondUser = User::factory()->createOne([
            'role_id' => 1,
            'school_id' => $this->school->id
        ]);

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);


        $this->teacherUser = User::factory()->createOne([
            'role_id' => 3,
            'school_id' => $this->school->id
        ]);

        $this->settings = TeacherPassSetting::create(['school_id' => $this->school->id]);
    }

    /**
     * @test
     * The command will expire the pass when created_at time is more than normal white pass time
     * @return void
     */
    public function the_command_will_expire_the_pass_when_time_is_more_than_normal_white_pass_time()
    {
        $this->pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->secondUser->id,
            'user_id' => $this->firstUser->id,
            'created_at' => Carbon::today()->addMinutes(20),
            'type' => Pass::STUDENT_CREATED_PASS,
        ]);


        $this->artisan('passes:expired')->assertExitCode(0)->run();

        $this->assertDatabaseHas('passes', ['id' => $this->pass->id, 'pass_status' => 0]);

        $this->assertNotNull($this->pass->refresh()->expired_at);
    }

    /**
     * @test
     * The command won't expire the pass when the created_at time is in the white pass time
     * @return void
     */
    public function the_command_won_t_expire_the_pass_when_the_created_at_time_is_in_the_white_pass_time()
    {
        $this->pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->secondUser->id,
            'user_id' => $this->firstUser->id,
            'created_at' => Carbon::now(),
            'type' => Pass::STUDENT_CREATED_PASS,
        ]);


        $this->artisan('passes:expired')->assertExitCode(0)->run();

        $this->assertDatabaseHas('passes', ['id' => $this->pass->id, 'pass_status' => 1]);

        $this->assertNull($this->pass->refresh()->expired_at);

    }

    /**
     * @test
     * The command will expire the appointment pass when the awaiting_apt time is in the past
     * @return void
     */
    public function the_command_will_expire_the_appointment_pass_when_the_awaiting_apt_time_is_in_the_past()
    {
        $this->pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->secondUser->id,
            'user_id' => $this->firstUser->id,
            'created_at' => Carbon::today()->addMinutes(20),
            'type' => Pass::APPOINTMENT_PASS,
        ]);


        $this->artisan('passes:expired')->assertExitCode(0)->run();

        $this->assertDatabaseHas('passes', ['id' => $this->pass->id, 'pass_status' => 0]);

        $this->assertNotNull($this->pass->refresh()->expired_at);
    }

    /**
     * @test
     * The command won't expire the appointment pass when the awaiting apt time isn't in the past
     * @return void
     */
    public function the_command_won_t_expire_the_appointment_pass_when_the_awaiting_apt_time_isn_t_in_the_past()
    {
        $this->pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->secondUser->id,
            'user_id' => $this->firstUser->id,
            'created_at' => Carbon::now(),
            'type' => Pass::APPOINTMENT_PASS,
        ]);


        $this->artisan('passes:expired')->assertExitCode(0)->run();

        $this->assertDatabaseHas('passes', ['id' => $this->pass->id, 'pass_status' => 1]);

        $this->assertNull($this->pass->refresh()->expired_at);
    }

    /**
     * @test
     * The command will auto expire the pass after being approved
     * @return void
     */
    public function the_command_will_auto_expire_the_pass_after_being_approved()
    {
        $this->pass = Pass::create([
            'from_type' => 'App\Models\User',
            'to_type' => 'App\Models\User',
            'from_id' => $this->adminUser->id,
            'school_id' => $this->firstUser->school_id,
            'to_id' => $this->secondUser->id,
            'user_id' => $this->firstUser->id,
            'created_at' => Carbon::today(),
            'type' => Pass::STUDENT_CREATED_PASS,
            'approved_at' => Carbon::today()->addMinutes(1),
            'approved_by' => $this->adminUser->id
        ]);


        $this->artisan('passes:expired')->assertExitCode(0)->run();

        $this->assertDatabaseHas('passes', ['id' => $this->pass->id, 'pass_status' => 0]);

        $this->assertNotNull($this->pass->refresh()->expired_at);
    }
}
