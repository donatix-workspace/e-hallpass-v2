<?php

namespace App\Policies;

use App\Models\Room;
use App\Models\Unavailable;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UnavailablePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isTeacher() || $user->isAdmin() || $user->isStaff();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Unavailable $unavailable
     * @return mixed
     */
    public function view(User $user, Unavailable $unavailable)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isTeacher() || $user->isAdmin() || $user->isStaff();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Unavailable $unavailable
     * @return mixed
     */
    public function update(User $user, Unavailable $unavailable)
    {
        if ($unavailable->unavailable_type === Room::class) {
            return $user->isAdmin() || $user->isStaff();
        }
        if ($user->isAdmin() || $user->isStaff()) {
            return true;
        }
        return $user->isTeacher() && $unavailable->unavailable_id === $user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Unavailable $unavailable
     * @return mixed
     */
    public function delete(User $user, Unavailable $unavailable)
    {
        if ($unavailable->unavailable_type === Room::class) {
            return $user->isAdmin() || $user->isStaff();
        }
        if ($user->isAdmin() || $user->isStaff()) {
            return true;
        }
        return $user->isTeacher() && $unavailable->unavailable_id === $user->id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Unavailable $unavailable
     * @return mixed
     */
    public function restore(User $user, Unavailable $unavailable)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Unavailable $unavailable
     * @return mixed
     */
    public function forceDelete(User $user, Unavailable $unavailable)
    {
        //
    }
}
