<?php

namespace App\Models;

use App\Console\Commands\AppointmentPassScheduleCommand;
use App\Elastic\AppointmentPassesIndexConfigurator;
use App\Events\PassCreated;
use App\Http\Filters\Filterable;
use App\Http\Requests\StudentAppointmentRequest;
use App\Policies\AppointmentPassPolicy;
use App\Scopes\OnlyNotDeletedPeriod;
use App\Traits\AppointmentPassMapping;
use App\Traits\DashboardRefreshable;
use App\Traits\ExtendedSearchable;
use App\Traits\PerPage;
use App\Traits\WhereInRawChunked;
use Cache;
use Carbon\Carbon;
use Exception;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppointmentPass extends Model implements PushNotifications
{
    use HasFactory,
        Filterable,
        ExtendedSearchable,
        AppointmentPassMapping,
        PerPage,
        DashboardRefreshable,
        WhereInRawChunked;

    protected $guarded = [];

    protected $with = [
        'createdByUser:first_name,last_name,id,school_id,role_id,avatar,email',
        'user:first_name,last_name,id,school_id,role_id,email,avatar',
        'confirmedByUser:first_name,last_name,id,school_id,role_id,email,avatar',
        'to',
        'from',
        'comments',
        'period:name,id',
        'recurrenceAppointmentPass',
        'latestComment:id,comment,commentable_type,commentable_id',
        'canceledByUser:first_name,last_name,id,school_id,role_id,email,avatar',
        'acknowledgedByUser:first_name,last_name,id,school_id,role_id,email,avatar'
    ];

    protected $indexConfigurator = AppointmentPassesIndexConfigurator::class;

    public const GENERATE_DAILY_PER_JOB = 14;
    public const GENERATE_MONTHLY_PER_JOB = 3;
    public const MIN_NOW_MINUTE = 0;
    public const MIN_REMIND_MINUTE = 4;
    public const SYSTEM_PREFIX = 'System';

    protected $dates = [
        'ran_at',
        'confirmed_at',
        'confirmed_by_teacher_at',
        'for_date',
        'acknowledge_at',
        'canceled_at',
        'acknowledge_by_mail_at',
        'expired_at'
    ];

    protected $appends = [
        'is_in_another_pass',
        'is_ended',
        'is_missed',
        'is_missed_request',
        'is_for_today',
        'is_for_future',
        'is_active',
        'is_for_acknowledge',
        'can_be_edited',
        'can_be_canceled',
        'is_for_confirm',
        'is_canceled',
        'waiting_activation',
        'confirm_cancel',
        'string_status'
    ];

    public const EMAIL_REMIND_HOUR = '07:00';
    public const FIRST_REMIND_MINUTE = 20;
    public const SECOND_REMIND_MINUTE = 5;
    public const MAX_STUDENT_APPOINTMENTS = 5;

    /**
     * @return array
     */
    public function toSearchableArray(): array
    {
        $appointmentPass = $this->setAppends([
            'string_status',
            'is_for_confirm',
            'is_for_acknowledge',
            'confirm_cancel'
        ])->toArray();

        $appointmentPass['search_date_string'] = Carbon::parse(
            $this->for_date
        )->format('m/d/Y');
        $appointmentPass['is_recurrence'] = 0;

        // Add this flag just to indicate that the appointment is for the beyond (Depend on between_dates filter)
        $appointmentPass['show_in_beyond'] =
            $appointmentPass['recurrence_appointment_pass_id'] === null;
        $appointmentPass['recurrence_end_at'] =
            $appointmentPass['recurrence_appointment_pass'] !== null
                ? $appointmentPass['recurrence_appointment_pass'][
                    'recurrence_end_at'
                ]
                : $appointmentPass['for_date'];

        if ($appointmentPass['from_id'] === config('roles.system')) {
            $appointmentPass['from']['name'] = AppointmentPass::SYSTEM_PREFIX;
        }

        return $appointmentPass;
    }

    /**
     * @param DateTimeInterface $date
     * @return string
     */
    public function serializeDate(DateTimeInterface $date)
    {
        $school = School::findCacheFirst($this->school_id);

        return Carbon::parse($date, config('app.timezone'))
            ->setTimezone(optional($school)->getTimezone())
            ->format('Y-m-d H:i:s');
    }

    /**
     * @param $query
     * @param $user
     * @return mixed
     */
    public function scopeOfUserId($query, $user)
    {
        return $query->where('user_id', $user);
    }

    /**
     * @param $query
     * @param $school
     * @return mixed
     */
    public function scopeFromSchoolId($query, $school)
    {
        return $query->where('school_id', $school);
    }

    /**
     * @return bool
     * done (from doc)
     */
    public function getIsForAcknowledgeAttribute(): bool
    {
        $user = optional(auth()->user());

        $staffSchedulesFrom = StaffSchedule::where(
            'user_id',
            optional($user)->id
        )
            ->where('room_id', $this->from_id)
            ->exists();

        if (
            $this->from_id === config('roles.system') &&
            !$this->isAcknowledgedByTeacher() &&
            $this->pass_id === null &&
            !$this->isCanceled() &&
            !$this->isMissed()
        ) {
            return true;
        }

        if (
            $staffSchedulesFrom &&
            !$this->isAcknowledgedByTeacher() &&
            $this->pass_id === null &&
            !$this->isCanceled() &&
            !$this->isMissed()
        ) {
            return true;
        }

        if (
            !$this->from_id &&
            !$this->isAcknowledgedByTeacher() &&
            $this->pass_id === null &&
            !$this->isCanceled() &&
            !$this->isMissed()
        ) {
            return true;
        }

        return $this->from_type === User::class &&
            $this->from_id === $user->id &&
            !$this->isAcknowledgedByTeacher() &&
            $this->pass_id === null &&
            !$this->isCanceled() &&
            !$this->isMissed() &&
            !$this->is_missed_request;
    }

    /**
     * @param int $appointmentPassId
     * @return bool|\Illuminate\Support\HigherOrderCollectionProxy|mixed
     */
    public static function canGivenAppointmentBeAcknowledged(
        int $appointmentPassId
    ) {
        $appointmentPass = self::find($appointmentPassId);

        return $appointmentPass->is_for_acknowledge;
    }

    /**
     * @param int $appointmentPassId
     * @return bool|\Illuminate\Support\HigherOrderCollectionProxy|mixed
     */
    public static function canGivenAppointmentBeConfirmed(
        int $appointmentPassId
    ) {
        $appointmentPass = self::find($appointmentPassId);

        return $appointmentPass->is_for_confirm;
    }

    /**
     * @return bool
     * done (from doc)
     */
    public function getCanBeCanceledAttribute(): bool
    {
        $user = optional(auth()->user());

        $staffSchedulesFrom = StaffSchedule::where(
            'user_id',
            optional($user)->id
        )
            ->where('room_id', $this->from_id)
            ->exists();

        $staffSchedulesTo = StaffSchedule::where('user_id', optional($user)->id)
            ->where('room_id', $this->to_id)
            ->exists();

        $moduleOption = optional(
            School::findCacheFirst($user->school_id)
        )->getModuleOptions(Module::APPOINTMENTPASS);
        $moduleOption = json_decode($moduleOption);

        if (
            !$this->isCanceled() &&
            $this->pass_id === null &&
            !$this->is_missed_request
        ) {
            if ($user->isAdmin() && !optional($moduleOption)->admin_edit) {
                return true;
            }

            if ($this->from_id === config('roles.system')) {
                return true;
            }

            if (
                $this->from_type === User::class &&
                $this->from_id === optional($user)->id
            ) {
                return true;
            }

            if ($this->created_by === optional($user)->id) {
                return true;
            }

            if (
                $this->to_type === User::class &&
                $this->to_id === optional($user)->id
            ) {
                return true;
            }

            if ($this->to_type === Room::class && $staffSchedulesTo) {
                return true;
            }

            if ($this->from_type === Room::class && $staffSchedulesFrom) {
                return true;
            }

            if (
                ($user->isStaff() || $user->isAdmin()) &&
                !$moduleOption->admin_edit &&
                $this->to_type === Room::class &&
                !$staffSchedulesTo
            ) {
                return true;
            }

            if (
                !$staffSchedulesTo &&
                !$staffSchedulesFrom &&
                $this->from_type === Room::class &&
                $this->to_type === Room::class
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return int
     */
    public function getConfirmCancelAttribute(): int
    {
        if ($this->canceled_at && !$this->confirmed_by_teacher_at) {
            return (int) config('appointmentStatusWeight.canceled');
        }

        if ($this->confirmed_by_teacher_at && !$this->canceled_at) {
            return (int) config('appointmentStatusWeight.confirmed');
        }
        return (int) config('appointmentStatusWeight.both');
    }

    /**
     * @return bool
     */
    public function getIsCanceledAttribute()
    {
        return $this->isCanceled();
    }

    /**
     * @return bool
     */
    public function getCanBeEditedAttribute()
    {
        $staffSchedulesFrom = false;
        $staffSchedulesTo = false;

        $user = optional(auth()->user());
        $moduleOption = optional(
            School::findCacheFirst($user->school_id)
        )->getModuleOptions(Module::APPOINTMENTPASS);
        $moduleOption = json_decode($moduleOption);

        if (
            !$this->isCanceled() &&
            $this->pass_id === null &&
            !$this->is_missed_request
        ) {
            if (
                optional($user)->isAdmin() &&
                !optional($moduleOption)->admin_edit
            ) {
                return true;
            }

            if (
                $this->to_type === Room::class &&
                !$user->isTeacher() &&
                !$user->isStudent()
            ) {
                $staffSchedulesFrom = StaffSchedule::where(
                    'user_id',
                    optional($user)->id
                )
                    ->where('room_id', $this->from_id)
                    ->exists();

                $staffSchedulesTo = StaffSchedule::where(
                    'user_id',
                    optional($user)->id
                )
                    ->where('room_id', $this->to_id)
                    ->exists();

                if ($staffSchedulesFrom || $staffSchedulesTo) {
                    return true;
                }
            }

            if ($this->from_id === config('roles.system')) {
                return true;
            }

            if ($this->created_by === optional($user)->id) {
                return true;
            }

            if (
                $this->to_id === optional($user)->id &&
                $this->to_type === User::class
            ) {
                return true;
            }

            if (
                $this->from_id === optional($user)->id &&
                $this->from_type === User::class
            ) {
                return true;
            }

            if (
                !$staffSchedulesTo &&
                !$staffSchedulesFrom &&
                $this->from_type === Room::class &&
                $this->to_type === Room::class
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     * done (from doc)
     */
    public function getIsForConfirmAttribute(): bool
    {
        $user = auth()->user();
        $staffSchedulesTo = false;
        if (
            !$this->isConfirmedByTeacher() &&
            !$this->isCanceled() &&
            !$this->isMissed() &&
            !$this->for_date->isPast()
        ) {
            //            if ($this->to_type === User::class && $this->to_id === optional($user)->id && !$this->isConfirmedByTeacher()) {
            //                return true;
            //            }

            if (
                $this->createdByUser->isStudent() &&
                !$this->isConfirmedByTeacher() &&
                $this->to_type === User::class &&
                $this->to_id === optional($user)->id
            ) {
                return true;
            }

            if (
                $this->createdByUser->isStudent() &&
                !$this->isConfirmedByTeacher() &&
                $this->user_id === optional($user)->id
            ) {
                return true;
            }

            if (
                $this->createdByUser->isStudent() &&
                !$this->isConfirmedByTeacher() &&
                $this->to_type === Room::class
            ) {
                $staffSchedulesTo = StaffSchedule::where(
                    'user_id',
                    optional($user)->id
                )
                    ->where('room_id', $this->to_id)
                    ->exists();
            }

            if ($staffSchedulesTo && !$this->isConfirmedByTeacher()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isAcknowledged()
    {
        return $this->acknowledge_at !== null;
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return $this->expired_at !== null;
    }

    /**
     * @return bool
     */
    public function isWithValidAdminConfirmPolicyRights(?User $user)
    {
        if ($user !== null) {
            return $user->isAdmin() ||
                $user->isTeacher() ||
                ($user->isStaff() &&
                    $user->school_id === $this->school_id &&
                    !$this->isConfirmedByTeacher());
        }

        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isToHimConfirmPolicyRights(?User $user)
    {
        return $this->to_type === User::class &&
            $this->to_id !== optional($user)->id &&
            $this->school_id === optional($user)->school_id;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isWithValidAdminAcknowledgePolicyRights(?User $user)
    {
        return !$user->isStudent() &&
            $this->school_id === optional($user)->school_id &&
            !$this->isAcknowledgedByTeacher();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isFromSystem(?User $user)
    {
        return $this->school_id === optional($user)->school_id &&
            $this->from_id === 0;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isWithValidAcknowledgeRights(?User $user)
    {
        return $this->to_type === User::class &&
            $this->to_id != optional($user)->id &&
            $this->school_id === optional($user)->school_id;
    }

    /**
     * @return bool
     */
    public function isForFuture()
    {
        return !$this->for_date->isPast() && !$this->for_date->isToday();
    }

    /**
     * @return bool
     */
    public function isForFutureButNotRecurrence()
    {
        return $this->for_date->isToday() ||
            ($this->isForFuture() &&
                $this->recurrence_appointment_pass_id !== null);
    }

    /**
     * @return bool
     */
    public function getIsActiveAttribute()
    {
        return ($this->hasActivePass() && !$this->is_in_another_pass) ||
            ($this->hasActiveChild() && !$this->is_in_another_pass);
    }

    public function hasActivePass()
    {
        return $this->pass !== null &&
            $this->pass->pass_status === Pass::ACTIVE &&
            $this->pass->isApproved();
    }

    /**
     * @return bool
     */
    public function hasActiveChild()
    {
        return $this->pass !== null &&
            $this->pass->child !== null &&
            $this->pass->child->pass_status === Pass::ACTIVE;
    }

    /**
     * @return bool
     */
    public function getIsWaitingActivationAttribute()
    {
        return $this->pass !== null &&
            $this->pass->pass_status === Pass::ACTIVE &&
            !$this->pass->isApproved();
    }

    /**
     * @return bool
     */
    public function isCanceled()
    {
        return $this->canceled_at !== null ||
            ($this->pass !== null &&
                $this->pass->canceled_at !== null &&
                $this->pass->expired_at === null);
    }

    /**
     * @return bool
     */
    public function isConfirmedByTeacher()
    {
        return $this->confirmed_by_teacher_at !== null;
    }

    /**
     * @return bool
     */
    public function isAcknowledgedByTeacher()
    {
        return $this->acknowledged_by_teacher_at !== null;
    }

    /**
     * @return mixed
     */
    public function latestComment()
    {
        return $this->morphOne(Comment::class, 'commentable')->latest();
    }

    /**
     * @return bool
     */
    public function isMailSended()
    {
        return $this->mail_send_at !== null;
    }

    /**
     * @return bool
     */
    public function isForMailReminder()
    {
        $forDate = $this->for_date
            ->subMinutes(self::FIRST_REMIND_MINUTE)
            ->format('Y-m-d H:i');
        $nowDate = Carbon::now()->format('Y-m-d H:i');

        return $nowDate >= $forDate && !$this->isMailSended();
    }

    /**
     * @return bool
     */
    public function isNotReminded()
    {
        return $this->reminded_at === null;
    }

    /**
     * @return bool
     */
    public function firstReminder()
    {
        $forDate = $this->for_date
            ->subMinutes(self::FIRST_REMIND_MINUTE)
            ->format('Y-m-d H:i');
        $nowDate = Carbon::now()->format('Y-m-d H:i');

        return $nowDate >= $forDate && $this->isNotReminded();
    }

    /**
     * @return bool
     */
    public function secondReminder()
    {
        $forDate = $this->for_date
            ->subMinutes(AppointmentPass::SECOND_REMIND_MINUTE)
            ->format('Y-m-d H:i');

        return Carbon::now()->format('Y-m-d H:i') >= $forDate &&
            $this->ran_at === null &&
            now()->diffInMinutes($this->for_date, false) >=
                self::MIN_REMIND_MINUTE;
    }

    /**
     * @return bool
     */
    public function createPassFromAppointment(): bool
    {
        $pass = Pass::create([
            'from_id' => $this->from_id,
            'from_type' => $this->from_type,
            'to_id' => $this->to_id,
            'to_type' => $this->to_type,
            'user_id' => $this->user_id,
            'school_id' => $this->school_id,
            'requested_by' => $this->created_by,
            'requested_at' => $this->for_date,
            'type' => Pass::APPOINTMENT_PASS,
            'pass_status' => Pass::ACTIVE,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        $pass->comments()->create([
            'school_id' => $this->school_id,
            'user_id' => $this->created_by,
            'comment' => $this->reason
        ]);

        $this->update([
            'pass_id' => $pass->id,
            'ran_at' => now(),
            'updated_at' => now()
        ]);

        event(new PassCreated($pass));

        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * @return bool
     */
    public function isConfirmedByUser()
    {
        return $this->confirmed_at !== null;
    }

    /**
     * @return bool
     */
    public function getWaitingActivationAttribute()
    {
        return $this->pass !== null &&
            !$this->pass->isApproved() &&
            $this->pass->pass_status === Pass::ACTIVE &&
            (!$this->pass->isCompleted() || !$this->pass->isExpired()) &&
            !$this->is_in_another_pass;
    }

    /**
     * @return bool
     */
    public function isForEmailRemindHour()
    {
        return $this->for_date->isToday() &&
            Carbon::now()->hour === self::EMAIL_REMIND_HOUR;
    }

    /**
     * @return bool
     */
    public function forNow()
    {
        $nowPassTime = $this->for_date;

        return $nowPassTime->diffInMinutes(now()) <= self::MIN_NOW_MINUTE;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school()
    {
        return $this->belongsTo(School::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdByUser()
    {
        return $this->belongsTo(
            User::class,
            'created_by'
        )->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function confirmedByUser()
    {
        return $this->belongsTo(
            User::class,
            'confirmed_by'
        )->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function canceledByUser()
    {
        return $this->belongsTo(
            User::class,
            'canceled_by'
        )->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function acknowledgedByUser()
    {
        return $this->belongsTo(
            User::class,
            'acknowledged_by'
        )->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function from()
    {
        return $this->morphTo()->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function to()
    {
        return $this->morphTo()->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function period()
    {
        return $this->belongsTo(Period::class)->withoutGlobalScope(
            new OnlyNotDeletedPeriod()
        );
    }

    /**
     * @param $query
     * @param $user
     * @param $staffSchedulesFromRoomIds
     * @param $staffSchedulesToRoomIds
     * @return mixed
     */
    public function scopeTransparency(
        $query,
        $user,
        $staffSchedulesFromRoomIds,
        $staffSchedulesToRoomIds
    ) {
        return $query->where(function ($query) use (
            $user,
            $staffSchedulesFromRoomIds,
            $staffSchedulesToRoomIds
        ) {
            $query
                ->where(function ($query) use ($user) {
                    $query
                        ->where('appointment_passes.from_type', User::class)
                        ->where('appointment_passes.from_id', $user->id);
                })
                ->orWhere(function ($query) use ($user) {
                    $query
                        ->where('appointment_passes.to_type', User::class)
                        ->where('appointment_passes.to_id', $user->id);
                })
                ->orWhere(function ($query) use ($user) {
                    $query->orWhere('appointment_passes.created_by', $user->id);
                })
                ->orWhere(function ($query) use ($user) {
                    $query->orWhere(
                        'appointment_passes.confirmed_by',
                        $user->id
                    );
                })
                ->orWhere(function ($query) use ($user) {
                    $query->orWhere(
                        'appointment_passes.acknowledged_by',
                        $user->id
                    );
                })
                ->orWhere(function ($query) use ($staffSchedulesFromRoomIds) {
                    $query
                        ->where('appointment_passes.from_type', Room::class)
                        ->whereIntegerInRaw(
                            'appointment_passes.from_id',
                            $staffSchedulesFromRoomIds
                        );
                })
                ->orWhere(function ($query) use ($staffSchedulesToRoomIds) {
                    $query
                        ->where('appointment_passes.to_type', Room::class)
                        ->whereIntegerInRaw(
                            'appointment_passes.to_id',
                            $staffSchedulesToRoomIds
                        );
                });
        });
    }

    /**
     * @param $query
     * @param $user
     * @param $staffSchedules
     * @return mixed
     */
    public function scopeAwaiting($query, $user, $staffSchedules)
    {
        return $query->where(function ($query) use ($user, $staffSchedules) {
            $query
                ->where(function ($query) use ($user) {
                    $query
                        ->where('appointment_passes.to_type', User::class)
                        ->where('appointment_passes.to_id', $user->id);
                })
                ->orWhere(function ($query) use ($staffSchedules) {
                    $query
                        ->where('appointment_passes.to_type', Room::class)
                        ->whereIntegerInRaw(
                            'appointment_passes.to_id',
                            $staffSchedules
                        );
                });
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withoutGlobalScopes();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pass()
    {
        return $this->belongsTo(Pass::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recurrenceAppointmentPass()
    {
        return $this->belongsTo(
            RecurrenceAppointmentPass::class,
            'recurrence_appointment_pass_id'
        );
    }

    /**
     * @return bool|null
     * @throws Exception
     */
    public function delete()
    {
        $this->comments()->delete();

        return parent::delete(); // TODO: Change the autogenerated stub
    }

    public function setRanAtAttribute($value)
    {
        $this->attributes['ran_at'] = Carbon::parse($value)->setTimezone(
            optional($this->school)->getTimezone()
        );
    }

    public function getConfirmedAtAttribute($value)
    {
        if ($value === null) {
            return null;
        }
        return Carbon::parse($value, config('app.timezone'))->setTimezone(
            optional($this->school)->getTimezone()
        );
    }

    public function setConfirmedAtAttribute($value)
    {
        $this->attributes['confirmed_at'] = Carbon::parse($value)->setTimezone(
            config('app.timezone')
        );
    }

    public function setAcknowledgedAtAttribute($value)
    {
        $this->attributes['acknowledged_at'] = Carbon::parse(
            $value
        )->setTimezone(config('app.timezone'));
    }

    public function setCanceledAtAttribute($value)
    {
        $this->attributes['canceled_at'] = Carbon::parse($value)->setTimezone(
            config('app.timezone')
        );
    }

    public function setExpiredAtAttribute($value)
    {
        $this->attributes['expired_at'] = Carbon::parse($value)->setTimezone(
            config('app.timezone')
        );
    }

    public function setForDateAttribute($value)
    {
        $this->attributes['for_date'] = Carbon::parse($value)->setTimezone(
            config('app.timezone')
        );
    }

    public function setConfirmedByTeacherAtAttribute($value)
    {
        $this->attributes['confirmed_by_teacher_at'] = Carbon::parse(
            $value
        )->setTimezone(config('app.timezone'));
    }

    public function setAcknowledgedByMailAtAttribute($value)
    {
        $this->attributes['acknowledged_by_mail_at'] = Carbon::parse(
            $value
        )->setTimezone(config('app.timezone'));
    }

    /**
     * @param array $appointments
     * @return array
     */
    public static function validateAppointmentDates(array $appointments)
    {
        foreach ($appointments as $appointment) {
            $appointmentForThatTimeExists = AppointmentPass::where(
                'user_id',
                $appointment['user_id']
            )
                ->where(
                    'for_date',
                    '=',
                    Carbon::parse($appointment['for_date'])
                        ->setTimezone(config('app.timezone'))
                        ->toDateTimeString()
                )
                ->where('canceled_at', null)
                ->where('expired_at', null)
                ->where('ran_at', null)
                ->where('school_id', auth()->user()->school_id)
                ->exists();

            $appointmentFor15MinExists = AppointmentPass::where(
                'user_id',
                $appointment['user_id']
            )
                ->where(
                    'for_date',
                    '=',
                    Carbon::parse($appointment['for_date'])
                        ->addMinutes(15)
                        ->setTimezone(config('app.timezone'))
                        ->toDateTimeString()
                )
                ->where('canceled_at', null)
                ->where('expired_at', null)
                ->where('ran_at', null)
                ->where('school_id', auth()->user()->school_id)
                ->exists();

            if ($appointmentForThatTimeExists || $appointmentFor15MinExists) {
                return [
                    'status' => true,
                    'data' => [
                        'user_id' => $appointment['user_id'],
                        'for_date' => $appointment['for_date']
                    ],
                    'message' => $appointmentForThatTimeExists
                        ? __('appointment.for.that.time.exists')
                        : __('appointment.15.minutes.exists')
                ];
            }
        }

        return [
            'status' => false,
            'data' => null
        ];
    }

    /**
     * @param User $user
     * @param StudentAppointmentRequest $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    public static function validateStudentAppointmentRequest(
        User $user,
        StudentAppointmentRequest $request
    ) {
        $appointmentPassOfStudent = self::whereCreatedBy($user->id)
            ->whereDate('created_at', today())
            ->where('confirmed_at', null)
            ->where('expired_at', null)
            ->where('canceled_at', null)
            ->count();

        $locationCheckRequestTo = AppointmentPassPolicy::canStudentRequestToRoleOrLocation(
            auth()->user(),
            $request->input('to_id'),
            $request->input('to_type')
        );

        $appointmentForSameDate = self::whereCreatedBy($user->id)
            ->where(
                'for_date',
                Carbon::parse($request->input('for_date'))
                    ->setTimezone(config('app.timezone'))
                    ->toDateTimeString()
            )
            ->where('expired_at', null)
            ->where('canceled_at', null)
            ->exists();

        if ($appointmentForSameDate) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('appointment.for.that.time.exists'),
                    'status' => __('fail')
                ],
                422
            );
        }

        if (!$locationCheckRequestTo) {
            return response()->json(
                [
                    'data' => [
                        'to' => false
                    ],
                    'message' => __(
                        'passes.appointment.can.create.to.that.location'
                    ),
                    'status' => __('fail')
                ],
                422
            );
        }

        if (
            $appointmentPassOfStudent >=
            AppointmentPass::MAX_STUDENT_APPOINTMENTS
        ) {
            return response()->json(
                [
                    'data' => [],
                    'message' => __('passes.appointments.limit.reached'),
                    'status' => __('fail')
                ],
                422
            );
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getIsInAnotherPassAttribute()
    {
        return Cache::remember(
            'is_in_another_pass_school_id' .
                $this->school_id .
                '_user_' .
                $this->user_id .
                '_apt_' .
                $this->id,
            now()->addMinute(),
            function () {
                return Pass::wherePassStatus(Pass::ACTIVE)
                    ->where('user_id', $this->user_id)
                    ->where('id', '!=', $this->pass_id)
                    ->where('school_id', $this->school_id)
                    ->whereBetween('created_at', [
                        $this->for_date->subMinutes(5),
                        $this->for_date
                    ])
                    ->exists();
            }
        );
    }

    /**
     * @param $filters
     * @param int $perPage
     * @param bool $withoutRecurrence
     * @param bool $pagination
     * @param bool $onlyQuery
     * @return mixed
     */
    public static function nonSearchableQueries(
        $filters,
        int $perPage = 25,
        bool $withoutRecurrence = false,
        bool $pagination = true,
        bool $onlyQuery = false
    ) {
        $user = auth()->user();

        if (request()->has('sort')) {
            return self::searchableQueries(
                $filters,
                self::recordsPerPage(),
                true
            );
        }

        $query = self::onWriteConnection()
            ->where('appointment_passes.school_id', $user->school_id)
            ->orderBy('appointment_passes.for_date', 'asc')
            ->orderBy('appointment_passes.confirmed_by_teacher_at', 'asc')
            ->orderBy('appointment_passes.canceled_at', 'asc')
            ->when($withoutRecurrence, function ($query) {
                $query->where('recurrence_appointment_pass_id', null);
            })
            ->filter($filters);

        if ($onlyQuery) {
            return $query;
        }

        return $pagination ? $query->paginate($perPage) : $query->get();
    }

    /**
     * @return bool
     */
    public function getIsEndedAttribute()
    {
        return $this->pass !== null &&
            $this->pass->completed_at !== null &&
            $this->pass->pass_status === Pass::INACTIVE;
    }

    /**
     * @return bool
     */
    public function getIsMissedAttribute()
    {
        return $this->isMissed() && !$this->is_in_another_pass;
    }

    public function isMissed()
    {
        return ($this->expired_at !== null ||
            ($this->pass !== null &&
                $this->pass->expired_at !== null &&
                $this->for_date->isPast()) ||
            ($this->pass !== null &&
                $this->pass->child !== null &&
                $this->pass->child->expired_at !== null)) &&
            !$this->is_missed_request;
    }

    /**
     * @return bool
     */
    public function isCoverTheTransparency()
    {
        $user = optional(auth()->user());

        return ($this->from_type === User::class &&
            $this->from_id === $user->id) ||
            ($this->to_type === User::class && $this->to_id === $user->id) ||
            $this->created_by === $user->id;
    }

    /**
     * @return bool
     */
    public function getIsForTodayAttribute()
    {
        $schoolTimezone = optional(
            School::findCacheFirst($this->school_id)
        )->getTimezone();

        return Carbon::parse($this->for_date, config('app.timezone'))
            ->setTimezone($schoolTimezone)
            ->isToday() &&
            $this->pass_id === null &&
            $this->isConfirmedByTeacher() &&
            !$this->is_in_another_pass;
    }

    /**
     * @return bool
     */
    public function getIsForFutureAttribute()
    {
        $schoolTimezone = optional(
            School::findCacheFirst($this->school_id)
        )->getTimezone();

        return !Carbon::parse($this->for_date, config('app.timezone'))
            ->setTimezone($schoolTimezone)
            ->isPast() &&
            !Carbon::parse($this->for_date, config('app.timezone'))
                ->setTimezone($schoolTimezone)
                ->isToday();
    }

    /**
     * @return bool
     */
    public function getIsMissedRequestAttribute()
    {
        return $this->for_date->isPast() &&
            optional($this->createdByUser)->role_id ===
                config('roles.student') &&
            $this->confirmed_by_teacher_at === null;
    }

    /**
     * @param $filters
     * @param int $perPage
     * @param bool $all
     * @param bool $transparency
     * @param bool $onlyQuery
     * @return mixed
     */
    public static function searchableQueries(
        $filters,
        int $perPage = 25,
        bool $all = false,
        bool $transparency = false,
        bool $onlyQuery = false
    ) {
        $user = auth()->user();

        $schoolTimezone = School::findCacheFirst(
            $user->school_id
        )->getTimezone();

        $elasticAllRecords = config('scout_elastic.all_records');

        $staffSchedules = StaffSchedule::where('user_id', $user->id)
            ->get()
            ->pluck('room_id')
            ->toArray();

        $staffSchedulesFromRoomIds = count($staffSchedules)
            ? elasticWhereInRawString('from_id', $staffSchedules)
            : 0;
        $staffSchedulesToRoomIds = count($staffSchedules)
            ? elasticWhereInRawString('to_id', $staffSchedules)
            : 0;
        $elasticRoomsAssociated = '';

        if ($staffSchedulesToRoomIds && $staffSchedulesFromRoomIds) {
            $elasticRoomsAssociated = "OR (from_type:*Room* AND ($staffSchedulesFromRoomIds))
         OR (to_type:*Room* AND ($staffSchedulesToRoomIds))";
        }
        $elasticTransparencyString = " AND ((from_type:User AND from_id:$user->id) OR (to_type:User AND to_id:$user->id) OR (created_by:$user->id) OR (acknowledged_by:$user->id) OR (confirmed_by:$user->id) $elasticRoomsAssociated)";

        if ($transparency) {
            $elasticAllRecords .= $elasticTransparencyString;
        }

        $elasticQuery = self::search(
            !$all ? convertElasticQuery(request()->input('search_query')) : '*'
        )
            ->select('id')
            ->when(request()->has('sort'), function ($query) {
                $sort = explode(':', request()->get('sort'));
                if (count($sort) <= 1) {
                    return $query->orderBy('id', 'asc');
                }

                $sortingKeywords = ['asc', 'desc'];
                if (in_array($sort[1], $sortingKeywords)) {
                    return $query->orderBy($sort[0], $sort[1]);
                }
                return $query->orderBy('id', 'asc');
            })
            ->when(request()->has('between_dates'), function ($query) use (
                $schoolTimezone
            ) {
                $dates = explode(',', request()->input('between_dates'));

                if ($dates === null || count($dates) <= 1) {
                    return $query->whereBetween('for_date', [
                        now()
                            ->startOfDay()
                            ->toDateTimeString(),
                        now()
                            ->endOfDay()
                            ->toDateTimeString()
                    ]);
                }
                $firstDate = Carbon::parse(
                    $dates[0] . ' 00:00:00',
                    $schoolTimezone
                )
                    ->setTimezone(config('app.timezone'))
                    ->toDateTimeString();
                $secondDate = Carbon::parse(
                    $dates[1] . ' 23:59:59',
                    $schoolTimezone
                )
                    ->setTimezone(config('app.timezone'))
                    ->toDateTimeString();

                return $query->whereBetween('for_date', [
                    $firstDate,
                    $secondDate
                ]);
            })
            ->where('school_id', $user->school_id)
            ->take(
                AppointmentPass::where('school_id', $user->school_id)->count()
            )
            ->get();

        $elasticIds = collect($elasticQuery)
            ->pluck('id')
            ->toArray();

        $elasticIdsImploded = implode(',', $elasticIds);

        $query = self::whereIntegerInRawChunk(
            'appointment_passes.id',
            $elasticIds
        )
            ->when(request()->filled('sort'), function ($query) use (
                $elasticIdsImploded
            ) {
                $query->orderByRaw("field(id,$elasticIdsImploded)");
            })
            ->where('appointment_passes.school_id', $user->school_id)
            ->when(Transparency::isEnabled(), function ($query) use (
                $user,
                $staffSchedules
            ) {
                $query->transparency($user, $staffSchedules, $staffSchedules);
            })
            ->when(request()->isNotFilled('sort'), function ($query) {
                $query
                    ->orderBy('appointment_passes.for_date', 'asc')
                    ->orderBy(
                        'appointment_passes.confirmed_by_teacher_at',
                        'asc'
                    )
                    ->orderBy('appointment_passes.canceled_at', 'asc');
            })
            ->filter($filters);

        if ($onlyQuery) {
            return $query;
        }

        return $query->paginate(self::recordsPerPage());
    }

    /**
     * @param $filters
     * @param int $perPage
     * @param bool $withoutRecurrence
     * @param bool $pagination
     * @param bool $onlyQuery
     * @return mixed
     */
    public static function transparencyQueries(
        $filters,
        int $perPage = 25,
        bool $withoutRecurrence = false,
        bool $pagination = true,
        bool $onlyQuery = false
    ) {
        $user = auth()->user();

        if (request()->has('sort')) {
            return self::searchableQueries(
                $filters,
                self::recordsPerPage(),
                true,
                true
            );
        }

        $staffSchedulesRoomIds = StaffSchedule::where('user_id', $user->id)
            ->get()
            ->pluck('room_id');

        $query = self::where('appointment_passes.school_id', $user->school_id)
            ->orderBy('appointment_passes.created_at', 'desc')
            ->orderBy('appointment_passes.confirmed_by_teacher_at', 'asc')
            ->when($withoutRecurrence, function ($query) {
                $query->where('recurrence_appointment_pass_id', null);
            })
            ->orderBy('appointment_passes.canceled_at', 'asc')
            ->filter($filters)
            ->transparency(
                $user,
                $staffSchedulesRoomIds,
                $staffSchedulesRoomIds
            );

        if ($onlyQuery) {
            return $query;
        }

        return $pagination ? $query->paginate($perPage) : $query->get();
    }

    /**
     * @return string
     */
    public function getStringStatusAttribute(): string
    {
        if ($this->isCanceled()) {
            return 'Canceled';
        }

        if ($this->isExpired()) {
            return 'Missed';
        }

        if ($this->is_for_today) {
            return 'Today\'s APT';
        }

        if ($this->waiting_activation) {
            return 'Waiting for confirmation';
        }

        if ($this->is_for_future) {
            return 'Future APT';
        }

        return 'Today\'s APT';
    }

    public function getTopics()
    {
        $env = config('services.fcm.env');
        //dev.
        //local.
        //staging.
        //production.
        return "{$env}.user.passes.{$this->user_id}";
    }

    /**
     * @param $event
     * @param null $options
     * @return array
     */
    public function getNotificationTextAndMessages(
        $event,
        $options = null
    ): array {
        switch ($event) {
            case 'AppointmentPassTimeChanged':
                return [
                    'title' => __('Your apt pass time has changed.'),
                    'body' =>
                        'The new time is now ' . $options !== null
                            ? Carbon::parse(
                                optional($options)['givenDate']
                            )->format('g:i A')
                            : ' '
                ];
            case 'AppointmentPassCancelled':
                return [
                    'title' => __('An apt pass has been cancelled.'),
                    'body' => __(
                        'Check your apt pass section for further details'
                    )
                ];
            case 'AppointmentPassAcknowledged':
                return [
                    'title' => __('An apt pass has been acknowledged.'),
                    'body' =>
                        'Apt pass on ' .
                        Carbon::parse(
                            optional($options)['acknowledgedAt']
                        )->format('m/d/Y') .
                        ' at ' .
                        Carbon::parse(
                            optional($options)['acknowledgedAt']
                        )->format('g:i A') .
                        ' has been acknowledged.'
                ];
            case 'AppointmentPassConfirmed':
                return [
                    'title' => __('An apt pass request has been confirmed.'),
                    'body' =>
                        'Apt pass on ' .
                        Carbon::parse(
                            optional($options)['confirmedAt']
                        )->format('m/d/Y') .
                        ' at ' .
                        Carbon::parse(
                            optional($options)['confirmedAt']
                        )->format('g:i A') .
                        ' has been confirmed.'
                ];
            case 'AppointmentPassRemind':
                return [
                    'title' =>
                        'You have an apt pass in ' .
                        optional($options)['reminder_period'] .
                        ' mins.',
                    'body' => ' '
                ];
            case 'AppointmentPassCreated':
                return [
                    'title' => __('An apt pass has been created for you.'),
                    'body' => 'Check your apt pass section for further details.'
                ];
        }

        return [
            'title' => '',
            'body' => ''
        ];
    }

    public function getNotificationTitle($event, $options = null)
    {
        $formattedMessages = $this->getNotificationTextAndMessages(
            $event,
            $options
        );

        return optional($formattedMessages)['title'];
    }

    public function getNotificationBody($event, $options = null)
    {
        $formattedMessages = $this->getNotificationTextAndMessages(
            $event,
            $options
        );

        return optional($formattedMessages)['body'];
    }

    public function getData($event, $options)
    {
        return [
            'event' => $event,
            'payload' => [
                'id' => $this->id
            ],
            'options' => $options
        ];
    }

    public function getNotificationSettings($event, $options): array
    {
        return [
            'topics' => $this->getTopics(),
            'title' => $this->getNotificationTitle($event, $options),
            'body' => $this->getNotificationBody($event, $options),
            'data' => $this->getData($event, $options)
        ];
    }

    /**
     * @return bool
     */
    public function executeIfItsForNow(): bool
    {
        $appointmentPassScheduleManager = new AppointmentPassScheduleCommand();
        $diffInMinutes = now()->diffInMinutes($this->for_date);

        if ($diffInMinutes < AppointmentPass::SECOND_REMIND_MINUTE) {
            $appointmentPassScheduleManager->appointmentExecute($this);

            return true;
        }

        return false;
    }

    /**
     * @throws Exception
     */
    public function changeRecurrenceIfNeeded(
        ?string $recurrenceEndAtUserInput,
        ?string $recurrenceTypeUserInput,
        ?string $recurrenceDaysUserInput,
        ?string $recurrenceWeekUserInput
    ) {
        if ($this->recurrenceAppointmentPass && $recurrenceEndAtUserInput) {
            $recurrenceEndAtUserInput = Carbon::parse($recurrenceEndAtUserInput)
                ->setTimezone(config('app.timezone'))
                ->toDateTimeString();

            $this->recurrenceAppointmentPass->update([
                'recurrence_end_at' => $recurrenceEndAtUserInput,
                'recurrence_type' => $recurrenceTypeUserInput,
                'recurrence_days' => $recurrenceDaysUserInput,
                'recurrence_week' => $recurrenceWeekUserInput
            ]);

            $this->recurrenceAppointmentPass->searchable();
        }
    }
}
