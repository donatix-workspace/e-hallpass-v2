<?php

namespace App\Console\Commands;

use App\Models\Room;
use Illuminate\Console\Command;

class RoomsFixEmptyCommand extends Command
{
    protected $signature = 'rooms:comment_type';

    protected $description = 'Fix the empty rooms comment types';

    // Issue: https://eduspiresolutions.atlassian.net/browse/EHP2-1310
    public function handle(): int
    {
        Room::whereCommentType('null')
            ->update([
                'comment_type' => Room::COMMENT_OPTIONAL
            ]);

        $this->output->info('Rooms comment types fixed');

        return 0;
    }
}
