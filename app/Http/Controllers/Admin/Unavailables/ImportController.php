<?php

namespace App\Http\Controllers\Admin\Unavailables;

use App\Http\Controllers\Controller;
use App\Imports\UnavailableImport;
use Excel;

class ImportController extends Controller
{
    /**
     * Download Out Of Office CSV.
     *
     * @authenticated
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function csv()
    {
        return Excel::download(new UnavailableImport(), 'unavailable.csv');
    }
}
