<?php
return [
    'student_pass_limits' => '<h2>Limit Student Pass</h2>
    <div style="font-family: Arial; font-size: 10pt">
    <p>
        This can be done for all users, specific students, by grade year or by uploading
        a CSV list of students.
        The date or date range is specified, along with the number of passes
        allowed during the designated
        <br/>
        time period. The admin can also set the limit to reset daily.
    </p>
<p>
    • The most restrictive limit will apply.
</p>
<p><span style="font-family: Arial">Limit Student Pass provides an Administrator with the ability to set limits on the number of passes for students.&nbsp;</span></p><p><span>Once a limit has been set, it will show on the list below. &nbsp;Admins can make changes to an active limit by using the &ldquo;Edit&rdquo; button in the &ldquo;Action&rdquo; column. &nbsp;There is also the option to &ldquo;Delete&rdquo; the pass limit. &nbsp;You can tab between active and inactive pass limits.</span></p><p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"></p>
<p dir="ltr"><span>Other information about Pass Limits:</span></p>
        <p dir="ltr"><span>More than one pass limit can be set for the same time period. &nbsp;The most &lsquo;limiting&rsquo; parameters will apply. &nbsp;For example: &nbsp;A pass limit is set for all users at 5 passes per day. &nbsp;An additional pass limit is set for Student A at 2 passes per day. Student A will only be allowed to use 2 passes in the system per day.&nbsp;</span></p>
</div>
',

    'contact_tracing_meet_up_report' => '<h2>Contact Tracing / Meet-up Report</h2>
<p dir="ltr" style="font-size: medium">Contact Tracing/Meet-up Report</p><p>
    This report allows you to search for how many times a specific student
(Student A) has been on a pass simultaneously with any other student (for a <strong><em> date range up to 15 days).</em></strong>&nbsp;
    Any students who were out on a pass at the same time as Student A, will be listed according to how many times their passes overlapped with Student A.
     If they shared the same starting or ending location as Student A, then that
    will be indicated by a yes in the last column.
</p>',

    'limit_location_map_cap' => '<h2>Limit Location Max Cap</h2>
</p><p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Limit Location Max Cap allows anyone with an Admin or Staff role to limit how many students a specified location can accept. &nbsp;Both Admins and Staff members can initiate setting this limit, from their EHP dashboards, for rooms to which they are associated with in their profile. &nbsp;</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">In addition, an Admin can click on the &ldquo;Limit Location Max Cap&rdquo; menu to view or set limits for rooms that they may not be associated with or to which no one would be associated. &nbsp;Simply choose the location from the drop down, add the limit, and click submit. As locations are given a limit they will appear in gray to indicate they already have a limit. You can still choose this location and edit the limit if you wish.&nbsp;</span></p>
<p>
    <li> Appointment Passes are not affected by this limit.</li>
</p>
<p>
    <li>Anyone linked to this location has the ability to change the max
    capacity.</li>
',

    'active_pass_limit' => '<h2>Limit Active Passes</h2>
<p><span style="font-size:11pt;font-family:Arial;">The purpose of this limit is to allow an Admin to set how many passes can be active at one time in the building.
The Admin specifies which pass types count towards the active pass limit, such as Proxy (PRX), Student (STU), and Kiosk (KSK).
Appointment Passes are not included as an option with this feature. The active pass limit shows on all Teacher, Staff and Admin EHP dashboards but only an Admin can adjust the limit.
When a pass is initiated and the limit has been reached, the following message appears to the user: &nbsp;&ldquo;Hall traffic limit reached. Please try a little later.&rdquo;&nbsp;</span></p>',

    'limit_location_availability' => '<h2>Limit Location Availability</h2>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">This feature allows administrators to set up &ldquo;Access Denied&rdquo; or &ldquo;Membership Locations&rdquo;.
On the Limit Location Availability screen you can create a new limit by completing the fields on the top half of the screen.
 All location availability limits will be listed on the bottom half of the screen, where you can sort the columns, end/delete limits, as well as use the tabs to see active or inactive limits.
&nbsp;</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">If a student misbehaves in a certain location or otherwise needs a restriction, you can set up an access denied and a time limit for the limitation. &nbsp;Students will have a notification on their student device that they have a Location Restriction but it will not specify what location. The Access Denied location will not appear in that student&rsquo;s drop down locations. &nbsp;
A student with a location restriction could still be given a Teacher Pass to that location, but there will be an alert on the Teacher Pass for the adult to consider.
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Conversely, you can set up a room as a Membership Only and give students access to the location for a set date range. &nbsp;
This might work well for Off Campus Lunch, Stretch Break (for IEP or other exceptional students), certain bathrooms, and so on.
Please note, if a room has been used for Membership Only but currently has no Members, then it will appear to all students.</span></p>',

    'ab_polarity' => '<h2>Contact Control</h2>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Contact Control allows an Admin to keep pairs of students from being on an active hall pass at the same time.</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">You can create a new polarity pair by clicking on the blue down arrows next to &ldquo;Choose Students&rdquo;.
 &nbsp;You can edit the polarity message that students will receive by clicking on the blue down arrows next to &ldquo;Edit Message&rdquo;. &nbsp;</span></p>
<br />
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Once the pair of students has been added, an Admin can see how many times this restriction is triggered. &nbsp;The Admin can also make the restriction &ldquo;Inactive&rdquo; by clicking on the green &ldquo;Active&rdquo; button or completely delete the entry by clicking on &ldquo;Delete&rdquo;.</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Note: &nbsp;If more than 2 students are not allowed in the hallway at the same time, multiple &lsquo;pairings&rsquo; for these students will need to be entered. &nbsp;(For example: &nbsp;Student 1 and Student 2, Student 1 and Student 3, Student 2 and Student 3, etc.)</span></p>',

    'pass_blocking' => '<h2>Pass Blocking</h2>
<p><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Pass Blocking provides an Administrator with the ability to block passes for all users for a specified period of time. This can be done for any or all of the following types of passes: Student Pass, Proxy Pass, Kiosk and/or Appointment Pass. When a Pass Blocking Period is created, users will not be able to initiate a new pass. The Admin can create a message for students already out on a pass, with specific instructions on what to do during the pass blocking period. (For example: Please go to the nearest teacher.)</span></p>',

    'pass_transparency' => '<h2>Pass Transparency</h2>
<p dir="ltr">
    By default e-hallpass allows Teachers and Staff to see all active and ended
    passes, including appointment passes and pass history for the whole school
    building. If a school would like to limit their teachers/staff to view only
    ended passes they are associated with, then "Pass Transparency" can be
    turned off. (Teachers/Staff can still see all active passes, but only while
    they are active on the dashboard.)
</p>
 <p>
            Once Pass Transparency is turned off, a school can specify certain
            teachers/staff to be allowed to keep pass transparency, if desired.
            Anyone with admin role access will always have pass transparency
            on.
 </p>
',

    'rooms_import' => '<div>
<h2 class="mb-3">Import Rooms</h2>
<p>
 A list of rooms can be imported into the system using a CSV file.
 The file requires the following columns, headers, and data for each room being added:  Room Name, Trip Type, Comment, Allow Student APT Request.
 The additional information here will help you determine what to include in the file.
</p>
<p>Each room you import will need 2 pieces of required information. The third column is optional. <br>
A room name (eg. Room 101). The "Trip Type" you want to use for passes to that location.
"Trip Type" refers to how many adults are expected to interact on that pass. Its only function is to provide the pass (and teacher\'s dashboard)
with the logically correct button options and to correctly color code an unexpected adult interaction on that pass .
<br ></p ><h5 > 1. A Pass can only be one of the following:</h5 ><ul >
<li > Roundtrip The student is going to an unattended location and returning to original destination .
The system expects the same signature on both ends, (ex: Teacher1 approves the student to go to their locker and then back to Teacher1). Examples: Bathroom, Locker, water fountain pass . </li ><li > One Way The student leaves point A and goes to point B and is pinned out / in at each location by different people and does not return to the original destination . Example: A pass to a resource / help room(where the student won\'t return).
</li>
<li>Layover The student leaves point A and goes to point B and then returns to point A and is pinned out/in at each location by different people.
 Example: Student needs to go to the main office to get some help and then will return.
  </li></ul><h5>2. Comments</h5><ul><li>The Comment column indicates whether you will require students to make a comment on the pass (Mandatory),
   whether it is optional they make comments (Optional), or if you will not allow comments at all (Hidden).
    Feel free to edit any of those three pre-populated fields (the text that is there is just sample text).
     </li><li>If you leave this field blank (with the column header in tact), the comment will be considered optional.
     </li><li>Also if you completely remove this column (with header) all comments for each uploaded room will be considered optional as well.
      </li></ul><h5>3. Click on <a download="" rel="noopener" href="/csv-samples/rooms-import-sample.csv" target="_blank" class="rooms-import-link lh-normal text-accent-blue-light">
      <i class="fi flaticon-download mr-1"></i> <span class="d-none d-md-inline-block">[Download Sample CSV]</span></a>.
        <a download="" rel="noopener" href="/csv-samples/app-module/rooms-import-sample.csv" target="_blank" class="rooms-import-link with-app-pass lh-normal text-accent-blue-light">
      <i class="fi flaticon-download mr-1"></i> <span class="d-none d-md-inline-block">[Download Sample CSV]</span></a>.
      </h5><ul><li>Open this file and you\'ll notice that you have four column headings(please don\'t change those). </li>
      <li>Starting in row 2 (your first room) complete all the information as it pertains to your building (Room Name, Trip Type, and Comment as explained above).
      </li>Add as many rooms as you need--making sure you provide the three required fields per room.</li>
      <li>When you are finished, save your file.</li>
      <li>
      Go to the Import Rooms section and click on the CHOOSE FILE button.
      </li>
      <li>
      Select the CSV file you just created and then click Import
      </li>
      </ul>
    </div>',

    'rooms-info' => '<div>
<h2 class="mb-3">Importing rooms information</h2>
<p>Each room you import will need 2 pieces of required information. The third column is optional. <br>
A room name (eg. Room 101). The "Trip Type" you want to use for passes to that location.
"Trip Type" refers to how many adults are expected to interact on that pass. Its only function is to provide the pass (and teacher\'s dashboard)
with the logically correct button options and to correctly color code an unexpected adult interaction on that pass .
<br ></p ><h5 > 1. A Pass can only be one of the following:</h5 ><ul >
<li > Roundtrip The student is going to an unattended location and returning to original destination .
The system expects the same signature on both ends, (ex: Teacher1 approves the student to go to their locker and then back to Teacher1). Examples: Bathroom, Locker, water fountain pass . </li ><li > One Way The student leaves point A and goes to point B and is pinned out / in at each location by different people and does not return to the original destination . Example: A pass to a resource / help room(where the student won\'t return).
</li>
<li>Layover The student leaves point A and goes to point B and then returns to point A and is pinned out/in at each location by different people.
 Example: Student needs to go to the main office to get some help and then will return.
  </li></ul><h5>2. Comments</h5><ul><li>The Comment column indicates whether you will require students to make a comment on the pass (Mandatory),
   whether it is optional they make comments (Optional), or if you will not allow comments at all (Hidden).
    Feel free to edit any of those three pre-populated fields (the text that is there is just sample text).
     </li><li>If you leave this field blank (with the column header in tact), the comment will be considered optional.
     </li><li>Also if you completely remove this column (with header) all comments for each uploaded room will be considered optional as well.
      </li></ul><h5>3. Click on <a download="" rel="noopener" href="/csv-samples/rooms-import-sample.csv" target="_blank" class="rooms-import-link lh-normal text-accent-blue-light">
      <i class="fi flaticon-download mr-1"></i> <span class="d-none d-md-inline-block">[Download Sample CSV]</span></a>.
        <a download="" rel="noopener" href="/csv-samples/app-module/rooms-import-sample.csv" target="_blank" class="rooms-import-link with-app-pass lh-normal text-accent-blue-light">
      <i class="fi flaticon-download mr-1"></i> <span class="d-none d-md-inline-block">[Download Sample CSV]</span></a>.
      </h5><ul><li>Open this file and you\'ll notice that you have four column headings(please don\'t change those). </li>
      <li>Starting in row 2 (your first room) complete all the information as it pertains to your building (Room Name, Trip Type, and Comment as explained above).
      </li><li>Add as many rooms as you need--making sure you provide the three required fields per room.</li></ul><h5>4. When you are finished, save your file.
      </h5><h5>5. Go to the Import Rooms section and click on the CHOOSE FILE button.</h5><h5>6. Select the CSV file you just created and then click Import.</h5></div>',

    'rooms_title' => '<h2>Add Room</h2>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Room Name: Each location requires a room name (eg. Room 101).</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Trip Type: This refers to how many adults are expected to interact on that pass. Its only function is to provide the pass (and teacher&apos;s dashboard) with the correct button options and to correctly color code an unexpected adult interaction on that pass. A pass can only be one of the following:</span></p>
<ul style="margin-top:0;margin-bottom:0;padding-inline-start:26px;">
    <li aria-level="1" dir="ltr" style="list-style-type:disc;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:0pt;"><span ><strong>Roundtrip</strong></span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;"><br></span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;The student is going to an unattended location and returning to original destination. The system expects the same signature on both ends, (ex: Teacher1 approves the student to go to their locker and then back to Teacher1).</span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;"><br></span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;Examples: Bathroom, Locker, water fountain pass.</span></p>
    </li>
    <br/>
    <li aria-level="1" dir="ltr" style="list-style-type:disc;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span><strong>One Way</strong></span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;"><br></span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;The student leaves point A and goes to point B and is pinned out/in at each location by different people and does not return to the original destination.</span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;"><br></span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Example: A pass to a resource/help room (where the student won&apos;t return).</span></p>
    </li>
    <br/>
    <li aria-level="1" dir="ltr" style="list-style-type:disc;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:12pt;"><span><strong>Layover</strong></span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;"><br></span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">&nbsp;The student leaves point A and goes to point B and then returns to point A and is pinned out/in at each location by different people.</span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;"><br></span><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Example: Student needs to go to the main office to get some help and then will return.</span></p>
    </li>
</ul>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;"><strong>Comment Type:</strong></span></p>
<ul style="margin-top:0;margin-bottom:0;padding-inline-start:48px;">
    <li aria-level="1" dir="ltr" style="list-style-type:disc;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">The Comment column indicates whether you will require students to make a comment on the pass (Mandatory), whether it is optional they make comments (Optional), or if you will not allow comments at all (Hidden). Feel free to edit any of those three pre-populated fields (the text that is there is just sample text).</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:disc;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">If you leave this field blank (with the column header in tact), the comment will be considered optional.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:disc;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Also if you completely remove this column (with header) all comments for each uploaded room will be considered optional as well.</span></p>
    </li>
</ul>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Active/Inactive: when a location is &ldquo;Active&rdquo; then it can be found by students and adults (unless the student has a restriction in place for that location). &nbsp;When a location is "InActive" as &ldquo;Inactive&rdquo; no student or adult can choose that location to create any type of pass</span></p>',

    'rooms_favourites' => '<h2>Favourite Rooms</h2>
<ul style="margin-top:0;margin-bottom:0;padding-inline-start:48px;">
    <li aria-level="1" dir="ltr" style="list-style-type:disc;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre-wrap;">Keep in mind that individual teacher classrooms do NOT need to be added here. &nbsp;Students will use the teacher&rsquo;s names to create passes to/from teacher classrooms.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:disc;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre-wrap;">In addition, Admin and Staff role users will &ldquo;associate themselves&rdquo; to the rooms/locations in which they work, so that they can monitor passes to/from those locations. &nbsp;(For example, the Main Office)</span></p>
    </li>
</ul><p>
    *we will need to come up with the text for this section once we know how
    the “Favorites” column will work
</p>',

    'periods_create' => '<h2>Periods</h2>
<p dir="ltr">
    Periods in EHP are only used for information purposes for Appointment
    Passes. We are not importing student schedules or tying defined Periods to
    a bell schedule at this time.
</p>
<p dir="ltr">
    Name: add the name of the period. For example: After School
</p>
<p dir="ltr">
    Status: Active/Inactive An Active period will be seen in the Apt pass
    section and an inactive period will not be seen.
</p>
<p dir="ltr">
    Once you have filled out these two sections click Submit. You will see your
    period added to the list to the right. You can then click on it and drag
    and drop it into the order you choose.
</p>',

    'pass_times' => '<h2>Pass Times</h2>
<p>
    Adjust the default pass times as desired and click “Update”.
</p>
<p>
Please note that all active passes are system-ended at exactly midnight (in your school’s respective timezone).
</p>
',

    'auto_pass' => '<h2>Auto Pass</h2>
<div style="font-family: Arial; font-size: 11pt; color: black">
<p>
    Auto Pass is an optional feature that allows schools to make certain
    locations available for students to activate and/or end without requiring a
    teacher to touch their dashboard or the student device to give pass
    approval. In order to utilize this feature, first an Admin must add the
    rooms they wish to make available for Auto Pass.
</p>
<p>
 Click in the white box and
    choose the rooms you would like to make available. Once a room has been
    chosen you will see it with an “X” to the left of the name. If you wish to
    remove a room from this list, simply click on the “X”. Teachers then have
    the option to activate these rooms on their EHP dashboards.
</p>
</div>
',

    'to_create_now_pass_txt' => '<h2>Create Now Pass</h2>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">To create a &ldquo;Now&rdquo; pass follow these easy steps.</span></p>
<ol style="margin-top:0;margin-bottom:0;padding-inline-start:48px;">
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Choose &ldquo;Create Pass&rdquo; from your left hand menu</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Start typing the name of the adult you are with in the &ldquo;Departing From&rdquo; dropdown area. Once you see their name, click on it.&nbsp;</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Start typing the name of the “Destination” you would like to go to and click on the name or location once it appears.
</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">If the text box appears and says &ldquo;required&rdquo; you must give a reason for this pass request. &nbsp;If it says &ldquo;optional&rdquo; you can add a comment but you do not have to. &nbsp;If there is no text box move on to number 5.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Click the Submit button.&nbsp;</span></p>
    </li>
</ol>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">You have now created your pass request to leave the classroom, it will appear in red. &nbsp;If your teacher approves it, your request will turn green.</span></p>
<h2>Create Appointment Pass</h2>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">To create an &ldquo;Appointment Pass&rdquo; request follow these steps.</span></p>
<ol style="margin-top:0;margin-bottom:0;padding-inline-start:48px;">
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Click on &ldquo;Create Pass&rdquo; from the left hand menu.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Click on &ldquo;Appointment Pass&rdquo; (you will see the number of requests available to you listed below).</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Departing From: Choose the teacher/location you will be leaving at the time that you wish to request an appointment.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Destination: Choose the teacher/location you wish to create an appointment with.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Date: Choose the day you wish to have your appointment (it can be no more than 30 days from today).</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Period: Choose the period for the time you are requesting your appointment for.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Time: Choose the time of the appointment request. (It cannot be less than 30 minutes from now and it must fall between 6AM and 5PM.)</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Reason for Pass: Enter your reason for the appointment request.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Click the blue Submit button.&nbsp;</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">If it was successful you will receive the message &ldquo;Successfully created&rdquo;.&nbsp;</span></p>
    </li>
</ol>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Choose &ldquo;Appointment Pass&rdquo; from your left hand menu. &nbsp;You will see your appointment request. &nbsp;It will first have the status of &ldquo;Waiting for Confirmation&rdquo;. &nbsp;You can cancel the appointment by clicking on that “Action” button.  (You will be required to give a reason.) &nbsp;Cancel will be available to you up to 30 minutes prior to the appointment. &nbsp;You can send a comment to your teacher by clicking on the quote bubble and adding your comment.&nbsp;</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">If the appointment request is confirmed your appointment will change to have the status of either &ldquo;Todays Apt&rdquo; or &ldquo;Future Apt&rdquo;. &nbsp;If the appointment is canceled by an adult the status will change to &ldquo;Canceled&rdquo; and you will receive a notification. &nbsp;The bell icon will show you if you have received a notification of a canceled or edited appointment.&nbsp;</span></p>
',

    'to_add_favorite_teacher_txt' => '<h2>Favorites</h2>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">To add a Favorite teacher/room follow these steps.</span></p>
<ol style="margin-top:0;margin-bottom:0;padding-inline-start:48px;">
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Click on the blue plus symbol.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Start typing the name in the &ldquo;Search&rdquo; box or scroll down.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Once you see the name, click on it, you will now see the name in a blue bubble. You can continue to add up to 10 favorites.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Click the blue &ldquo;Submit&rdquo; button.</span></p>
    </li>
</ol>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">You will now see the person/location appear as a favorite or speed dial button. &nbsp;You can click on this button instead of typing the person/location name when creating a pass to or from this location.&nbsp;</span></p>',

    'to_create_an_appointment_text' => '
    <h2>Create Appointment Pass</h2>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">To create an &ldquo;Appointment Pass&rdquo; request follow these steps.</span></p>
<ol style="margin-top:0;margin-bottom:0;padding-inline-start:48px;">
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Click on &ldquo;Create Pass&rdquo; from the left hand menu.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Click on &ldquo;Appointment Pass&rdquo; (you will see the number of requests available to you listed below).</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Departing From: Choose the teacher/location you will be leaving at the time that you wish to request an appointment.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Destination: Choose the teacher/location you wish to create an appointment with.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Date: Choose the day you wish to have your appointment (it can be no more than 30 days from today).</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Period: Choose the period for the time you are requesting your appointment for.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Time: Choose the time of the appointment request. (It cannot be less than 30 minutes from now and it must fall between 6AM and 5PM.)</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Reason for Pass: Enter your reason for the appointment request.</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Click the blue Submit button.&nbsp;</span></p>
    </li>
    <li aria-level="1" dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;">
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">If it was successful you will receive the message &ldquo;Successfully created&rdquo;.&nbsp;</span></p>
    </li>
</ol>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Choose &ldquo;Appointment Pass&rdquo; from your left hand menu. &nbsp;You will see your appointment request. &nbsp;It will first have the status of &ldquo;Waiting for Confirmation&rdquo;. &nbsp;You can cancel the appointment by clicking on that “Action” button.  (You will be required to give a reason.) &nbsp;Cancel will be available to you up to 30 minutes prior to the appointment. &nbsp;You can send a comment to your teacher by clicking on the quote bubble and adding your comment.&nbsp;</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:12pt;margin-bottom:12pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">If the appointment request is confirmed your appointment will change to have the status of either &ldquo;Todays Apt&rdquo; or &ldquo;Future Apt&rdquo;. &nbsp;If the appointment is canceled by an adult the status will change to &ldquo;Canceled&rdquo; and you will receive a notification. &nbsp;The bell icon will show you if you have received a notification of a canceled or edited appointment.&nbsp;</span></p>
    ',

    'student_appointments' => '<h2>Student Appointments</h2>
<p><br></p><p><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">This screen will let you know if you have any appointments for today and in the future. &nbsp;You have two tabs, the &ldquo;Next 7 Days&rdquo; and &ldquo;Beyond 7 Days&rdquo;. &nbsp;You can sort each column or use the search box to find a specific appointment.&nbsp;</span></p>',

    'student_header' => '<h2>Student Top Bar Menu</h2>
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="39px" height="46px" viewBox="0 0 39 46" enable-background="new 0 0 39 46" xml:space="preserve">  <image id="image0" width="39" height="46" x="0" y="0"
    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAuCAMAAACccvDPAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAXVBMVEX////v5trt9Prx7Ojf
5uz//PO5sKqqus3x+v/v3s+pop358N+xp6DeyryqqqqvtLT78+nHvLLv5+GstsSlpqjp1MXz+PPm
287dyLm2wM2tu9CjoZ++ta3Tx7vH1eXJE02hAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAAOwwAADsMB
x2+oZAAAAAd0SU1FB+UKEgo0ILcu8c0AAAC0SURBVDjL7ZHLEoIwDEXDqy9ohFKpWPX/P9MWZVg4
JmzcONxVZnLmJGkBjhz5cYpyq6v6K9YIqdZam63+SNutTWuQ4KAVp9ztB0TKl8HUdsIgoiRPsWZU
2njOd04mOYXFR3H2ZQroad8lm8ysLJor50OMswq0z0Z8g3YPl0ZPFcXpDOXZfqaw9P2uLm5xGV0C
k+aeHzpKxYEQ0oYj8kbQIkrX7QDdIKHFB8tBk7breezIf+QJY70Jz7E/3NMAAAAldEVYdGRhdGU6
Y3JlYXRlADIwMjEtMTAtMThUMDc6NTI6MzIrMDM6MDCJEq57AAAAJXRFWHRkYXRlOm1vZGlmeQAy
MDIxLTEwLTE4VDA3OjUyOjMyKzAzOjAw+E8WxwAAAABJRU5ErkJggg==" />
</svg>
<p>
    You can choose to turn on sound notifications with this megaphone icon.
    When the line is showing the sound notifications are turned off.
    To turn on sound notifications, click on the megaphone icon, which makes the line disappear.
</p>
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="33px" height="42px" viewBox="0 0 33 42" enable-background="new 0 0 33 42" xml:space="preserve">  <image id="image0" width="33" height="42" x="0" y="0"
    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAqCAAAAAAYSG1wAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAJcEhZ
cwAADsMAAA7DAcdvqGQAAAAHdElNRQflChIKOjrUzyU5AAAAq0lEQVQ4y+3UwQ2DMAyF4Z+qAxh1
gogVOn5GaMUCIFggsjdoL4giFzeHcoRckPMpAvspzYvKc6mBU/wt9KEVMfajqzR+ctq6wtXtF6Nr
f5yR1QTSPfyOPCFAnyMxqAAg0xCI2ZYXmQOxHAGo7grfqqBjhwkLxfovgSibnbIntN+2RndEBlsW
ps+VfGabjIQJmFgyWQfs86HgAvKVsewz5hJEJzdXac7743DxBpWQN3pVoqvpAAAAJXRFWHRkYXRl
OmNyZWF0ZQAyMDIxLTEwLTE4VDA3OjU4OjU4KzAzOjAw/C9oewAAACV0RVh0ZGF0ZTptb2RpZnkA
MjAyMS0xMC0xOFQwNzo1ODo1OCswMzowMI1y0McAAAAASUVORK5CYII=" />
</svg>
<p>
    The bell will notify you of changes to Appointment passes. If you have a
    notification it will show as a number in a bubble. Click on the bell to see
    the notifications. Once the notifications are read, they will turn gray, but you can
    re-read them throughout the same day if needed.
</p>

<h6><b>Profile settings</b></h6>
<p dir="ltr">
Click on Profile Settings or hover over your picture to see the gear icon appear.
Click this to access your profile settings.
You will be able to see your name and your email address, but you cannot edit this.  </p>
<p dir="ltr">
    You will see your school logo and school name in the upper right corner. If
    you attend multiple schools you will see a dropdown carrot and you will be
    able to switch between the schools by choosing them from the dropdown.
</p>',

    'dashboard_overview' => '
	<h2 class="c2" id="h.u8ww5uelk887"><span class="c3 c8">Dashboard</span></h2>
	<p class="c5"><span class="c1" style="font-size: 12pt;">The upper menu bar</span>
		<span class="c3 c0">&nbsp;allows you to use the &ldquo;Create&rdquo; button to make quick actions or you can use the options listed on your left-hand menu. The megaphone icon allows you to turn sound notifications on and off. Your Profile Settings are available by clicking on the gear icon (under your profile picture). &nbsp;If you are in multiple school systems, you can use the carrot below the school name to choose a different school system.</span></p><p class="c5"><span class="c0">The </span><span class="c1" style="font-size: 12pt">boxes at the top</span><span class="c0">&nbsp;of your Dashboard give you a snapshot of what is happening in your school building currently. The first box </span><span class="c1" style="font-size: 12pt">Building Passes</span><span class="c0">&nbsp;allows you to see at a glance the number of active passes over the total number of passes. Below is the &ldquo;Active Pass Limit&rdquo;, if one has been set by your school. The next box is </span><span class="c1" style="font-size: 12pt">My Passes</span><span class="c0">&nbsp;which allows you to see at a glance the number of active passes you have over the total number of passes you have had so far today. &nbsp;The next box </span><span class="c1" style="font-size: 12pt">Flagged Passes</span><span class="c3 c0">&nbsp;shows you if any of the passes you are involved with have hit the &ldquo;Long running pass&rdquo; time or passes that are system ended.</span></p><p class="c9"><span class="c0">There may also be an </span><span class="c1" style="font-size: 12pt">Auto Passes</span><span class="c0">&nbsp;box which allows you to set up Auto Pass locations and settings, if your school has enabled this feature. &nbsp;If your school has </span><span class="c1" style="font-size: 12pt">Kiosk </span><span class="c0">enabled</span><span class="c1">&nbsp;</span><span class="c0">you will be able to see how many active kiosks you have set up in the Kiosk display box. &nbsp;(This will be located to the right of Flagged Passes and Auto Passes if enabled.) &nbsp;For Staff and Admin users, there may also be a </span><span class="c1" style="font-size: 12pt">Room Capacities</span><span class="c3 c0">&nbsp;section that displays the locations you&rsquo;re associated with in your profile so room limits can be managed. &nbsp;This section can be minimized and maximized as desired. &nbsp;</span></p><p class="c5 c7"><span class="c3 c0"></span></p><p class="c5"><span class="c0">There are two main tabs on your Dashboard labeled </span><span class="c1">My Passes and All Passes</span><span class="c3 c0">. &nbsp;By using these tabs you can toggle between passes on the dashboard that only involve you or passes that are happening throughout your entire school building. &nbsp;Below these two tabs are the options &ldquo;Active&rdquo;, &ldquo;Both&rdquo;, and &ldquo;Ended&rdquo;. &nbsp;These allow you to filter what you see on the dashboard. &nbsp;To the right of this toggle are several checkboxes that provide additional filters. You can change the view of your dashboard by choosing one or more of these filters. &nbsp;In order to see all passes simply remove all checkmarks.</span></p><p class="c9"><span class="c0">There will be </span><span class="c1" style="font-size: 12pt">Appointment Pass tabs</span><span class="c3 c0">&nbsp;available on your Dashboard if your school has these modules enabled. &nbsp;The &ldquo;Today&rsquo;s Appointments&rdquo; tab displays any student that is being requested from you or that you are requesting for an appointment that day. &nbsp;If your school has the student appointment request option enabled, you will also see the following two tabs: &ldquo;Today&rsquo;s Awaiting Confirmation&rdquo; and &ldquo;Future Awaiting Confirmation&rdquo;. &nbsp;Student appointment requests that are waiting to be &ldquo;confirmed&rdquo; &nbsp;will appear here for today or in the future. &nbsp;</span></p><p class="c5 c7"><span class="c3 c0"></span></p><p class="c5"><span class="c0">You can pause the dashboard by clicking the </span><span class="c1" style="font-size: 12pt">Pause Update</span><span class="c3 c0">&nbsp;gray button. &nbsp;Once clicked that button will change to a blue &ldquo;Resume Update&rdquo; button. &nbsp;Click it again to have your dashboard automatically refresh.</span></p><p class="c5"><span class="c0">Click the </span><span class="c1" style="font-size: 12pt">Download CSV</span><span class="c3 c0">&nbsp;button to download the data on the Dashboard screen into a CSV file.</span></p><p class="c5"><span class="c0">To search the dashboard you have a general </span><span class="c1">Search</span><span class="c0">&nbsp;box or you can utilize the </span><span class="c1" style="font-size: 12pt">Advanced Search</span><span class="c3 c0">&nbsp;by clicking on that box. &nbsp;It will then open up and allow you to search by &ldquo;Students&rdquo;, &ldquo;Teachers&rdquo;, or &ldquo;Rooms&rdquo;.</span></p><p class="c5"><span class="c0">The</span><span class="c1" style="font-size: 12pt">&nbsp;Basic View</span><span class="c3 c0">&nbsp;will allow you to see more passes at one time by minimizing the view of the passes on the dashboard . If you wish to act on a pass, click on it, it will expand, when done click it again.</span></p><p class="c5 c7"><span class="c3 c0"></span></p><p class="c4"><span class="c0 c3"></span></p><p class="c4"><span class="c3 c6"></span></p><p class="c4"><span class="c3 c6"></span></p><p class="c4"><span class="c3 c6"></span></p><p class="c4"><span class="c3 c6"></span></p>

',

    'dashboard_datatable' => '
    		<h2 class="c2" id="h.5mecsvps0nlp">
			<span class="c3 c8">Dashboard Table</span></h2><p class="c5"><span class="c3 c0">When a student creates a pass request it will appear with a white background. The pass will need to be activated to turn green. &nbsp;You do this by clicking the green &ldquo;Approve&rdquo; button. You also have the option to cancel the pass by clicking the red &ldquo;Cancel&rdquo; button. &nbsp;If you are the second destination, you will see a variety of buttons to choose from, which include &ldquo;Arrived&rdquo;, &ldquo;In/Out&rdquo; or &ldquo;End/Keep&rdquo;. &nbsp;When you are the final destination you will have the option to &ldquo;End&rdquo; the pass. &nbsp;It&rsquo;s important that all activated passes are ended so that accurate data is available in the system. &nbsp;</span></p><p class="c5"><span class="c0">If a pass runs beyond the time set for a </span><span class="c1" style="font-size: 12pt">Long running Pass</span><span class="c0">&nbsp;it will turn </span><span class="c1" style="font-size: 12pt">Yellow </span><span class="c0">and be counted as a Flagged Pass. &nbsp;If a pass runs beyond the time set for </span><span class="c1">System Ended</span><span class="c0">&nbsp;it will be ended by the system, counted as a Flagged Pass, and have a </span><span class="c1" style="font-size: 12pt">Red Flag</span><span class="c0">&nbsp;indicator. &nbsp;If a pass is created to a location that has an </span><span class="c1" style="font-size: 12pt">Extended Pass Time</span><span class="c0">&nbsp;it will be allowed to run longer than the System end time and when it has ended it will display a </span><span class="c1" style="font-size: 12pt">Purple Flag</span><span class="c0">. &nbsp;All </span><span class="c1">Ended</span><span class="c0">&nbsp;passes are displayed in </span><span class="c1" style="font-size: 12pt">Gray</span><span class="c3 c0">.</span></p><p class="c5"><span class="c0">The number in the </span><span class="c1" style="font-size: 12pt">blue bubble</span><span class="c3 c0">&nbsp;next to the student&rsquo;s name on a pass shows how many passes that student has already done for the current day. &nbsp;You can see the details of their pass history for today by clicking on the blue bubble. &nbsp;</span></p><p class="c5"><span class="c3 c0">You can sort the columns on the Active, Both, and Ended dashboard by using the up and down arrows to the right of the column headers.</span></p><p class="c5"><span class="c0">The </span><span class="c1">types of passes</span><span class="c3 c0">&nbsp;are: &nbsp;STU-Student, &nbsp;TCH-Teacher (old PRX-Proxy), &nbsp;APT-Appointment, and KSK-Kiosk</span></p><p class="c5"><span class="c0">You can add comments to a pass by clicking on the speech bubble in the </span><span class="c1" style="font-size: 12pt">Status/Comments </span><span class="c3 c0">section. &nbsp;Comments can be seen by adults and the student whose pass you are making a comment on. &nbsp;</span></p><p class="c4"><span class="c3 c6"></span></p><p class="c4"><span class="c3 c6"></span></p><p class="c4"><span class="c3 c6"></span></p><p class="c4"><span class="c3 c6"></span></p>
',

    'pass_history' => '<h2>
    Pass History
</h2>
<p>
    Pass History allows you to search for pass data from today and prior. Be
    sure to select the correct time frame you want to review. The report shows
    each individual pass associated with the criteria.
</p>
<div>
    <p >
        If you choose Pass view it will look like the Dashboard and if you
        select Tabular view it will be in a spreadsheet format. There is a
        vertical scroll bar to see all of the columns in the Tabular view.
    </p>
    <p >
        If you only wish to see “My Passes” make sure you are toggled there. If
        you want to see all passes make sure you are toggled to “All Passes”.
        This toggle is located below the tab “Pass History”
    </p>
    <p >
    You can search for a specific student or multiple students by entering their names in the “Search Student(s)” box.
    You can then filter those results by adding a teacher(s) and/or location(s).
    </p>
</div>',

    'teacher_pass_create' => '<h2>Teacher Pass</h2>
<p>
    Teacher pass is what we used to call Proxy Pass. This allows an adult to
    create a pass for a student to leave them and go to another adult or
    location.
</p>
<ol start="1" type="1">
    <li>
    Choose the student by typing their name in the “Search students” box. Once the student is selected, you will see a few things appear on the screen:
    the number of passes the student has had today will appear in a blue bubble,
    if the student has a pass limit and how many passes remain available to them, and if the student is part of an polarity pair, and the other student is on a pass, you will receive a warning.
    </li>
    <br/>
    <li>
    Choose the destination for the student by selecting a favorite (shortcut icon) or searching for the name of the adult in the “To” field.
    You will receive a warning message if either of the following are true:  the destination has a location max that has been reached or the student has a restriction for the location.
        <strong>
    NOTE: If you cannot find the name of the desired adult, it is because that adult has made themselves unavailable in their profile.
     Please reach out to the individual user to update their profile.
        </strong>
    </li>
    <br/>
    <li>
        “Reason…” if you would like to add a comment about this pass that other
        adults can see on the dashboard and the student can see on their
        device, you can add it here.
    </li>
    <br/>
    <li>
        “Pre-approve pass” will automatically be checkmarked. If you would
        prefer to activate the pass later, you can uncheck this box. The pass
        will appear on your dashboard as a white waiting for approval pass.
    </li>
    <br/>
    <li>
        Click the blue “Submit” button to create the pass. You will
        automatically be taken to your dashboard.
    </li>
</ol>',

    'appointment_pass_form' => '<h2>Appointment Pass</h2>
<p>
    The Appointment Pass is a teacher/staff created pass
    requesting a student come see them from another teacher or location at a
set date and time.
You can also change the “Quick filters” on this screen to see All Passes, etc.
<em>Some schools may not have this feature activated in e-hallpass.</em>
</p>
<p>
 Complete the fields on the screen in order to set up an appointment with a student.
 The period allows the student to associate the time of the pass with the period.
 By selecting the <b>From</b> location/person, the pass will appear on the teacher’s dashboard as well as yours
 Designate whether the pass will “repeat” and enter a reason for the pass.
 You can also click the blue +Add button to add another student to the same pass.
</p
<p>
    Once an Appointment Pass is made, you can scroll down to see your
    appointments. If a teacher/staff is requesting a student come from your
    class, you can Acknowledge or Deny the pass (maybe you’re giving a test and
    the appointment needs to be rescheduled). The student can acknowledge the
    pass from their device at 20 minutes and 5 minutes prior to the pass.
    Appointments for today will appear on the right hand side of the teacher
    dashboard.
</p>
<p>
    5 minutes prior to the Appointment the pass will move to the Dashboard to
    await approval.
</p>',

    'form_location_kiosk' => '<h2>Kiosk</h2>
<ol>
<li type="1" >
    <p >
        Select the FROM location for the Kiosk you are setting up.
    </p>
</li>
<ol>
<li type="I" >
        <p >
            All passes from this Kiosk will be listed as coming from that
            location.
        </p>
    </li>
<li type="I" >
        <p >
            The "ME" option shows all passes as coming from you.
        </p>
    </li>
</ol>
<li type="1" >
    <p >
        Click &#8220;SUBMIT&#8221; and your Kiosk will show up in "My
        Kiosk(s)" below and needs to be activated
    </p>
</li>
<ol>
<ol type="I" >
<li>
        <span >
            Start Kiosk via <b>URL Method.</b>&#160;<span style="color: #d43f3a"> This method is highly recommended for any
            Chromebook devices</span> because it does not require the faculty user
            to log into e-hallpass using their Google credentials.
        </span>
        </li>
<li>
            <p >
             &nbsp; Jot down the CODE (or email it to yourself). The Code is
                valid for 30 minutes. You can regenerate the code if it
                expires from "My Kiosk(s)".
            </p>
            </li>

            <li>
            <p >
                &nbsp;Go to the device you wish to put into KIOSK mode and open
                this URL: www.e-hallpass.com/klogin and enter the CODE.
            </p>

            </li>

            <li>
            <p >
               To launch multiple kiosks, complete step III above, then return to
                your device and click "Regenerate". Write down the new code,
                and use this new code to set up the next kiosk. Repeat
                this for as many devices as you would like to set up.
            </p>
           </li>
    </li>
    </ol>
            3. or <b>DIRECT Method</b> (This is not available to all Kiosk users.) This is a fast and
            secure way to log into Kiosk.<span style="color: #d43f3a;"> Chromebook devices should use the
            URL Method</span>.
            <p >
                I.&nbsp; Click the LAUNCH button in the blue box. This will
                simultaneously log you out of e-hallpass and log the device
                into the KIOSK mode.
            </p>
            <p >
                II.&nbsp;Launch additional Kiosks by repeating steps 1-3.
            </p>
    </ol>
',

    'office_out_form' => '<h2>Out of office</h2>
<p>
    Out of Office allows you to virtually lock your door when you are not there.
    You can pick a &#8220;quick select&#8221; time frame (the next &#189; hour,
    hour, &#8230;) or set a future date and time as custom. If you return
    early, you can end the Out of Office, If a student creates a pass while you
    are out, they get an alert and cannot make the pass.
    Admin and Staff can set a location as Out of Office, if they are associated with the location.
    Admins can also create an Out of Office for teachers and staff.
</p>
<p>
    Do not set Out of Office if there is a substitute or other staff member
    covering your classroom.
</p>',

    'main_users_menu' => '<h2>Main Users</h2>
<p>
 Main Users is where you can look to see who has access to your system.
 The boxes at the top of the screen will tell you how many users you have with each role, and out of those users how many have been recently added via a file upload, how many have been recently invited (via email), and how many have logged in.
 While most EHP customers automatically import their users from a third party source, like Clever or ClassLink, it is possible for an Admin to add an individual user and grant them full access to the system by selecting the “Add User (full access” option.
</p>
<p>
 It is also possible to add a Substitute user from this screen, using the “Add Substitute (PIN only)” option.
 An Admin can also import a file of users from this screen and/or import Kiosk Passwords if your school has enabled Kiosk.  (Each of these options have separate “Help” text details.)
 In the user table on the bottom half of the screen, you can sort the various columns and search for an individual user by utilizing the search boxes at the top
</p>',

    'form_add_user' => '
<h2>Add User</h2>
<p>
When adding a user here, a "Login Type" will need to be selected.  The options are Manual, Google, or Microsoft (O365).  Manual will provide an option for a password to be added and managed within e-hallpass.  (An invite can also be sent for the user to set up the password.)

<p>The Google and Microsoft login type options will not have a place for a password.   The Google or Microsoft user will use their normal email and the password that is already associated with that email.
Please be aware that when adding a user here with one of these three login types, the user will NOT be subject to the system "auto-archiving" process.  This means that if the user leaves and/or no longer needs access to e-hallpass, an Admin will need to archive (inactivate) this user in the system.  Archiving will not happen automatically.</p>
<p>Many schools provision users into e-hallpass through a sync with Clever or ClassLink, or by sending/uploading a user CSV file.  When users are INITIALLY added to the system through one of these methods and then "removed" from the Clever/ClassLink sync or the user CSV file - the user will automatically be archived in the e-hallpass system, through the auto-archiving process.</p>
<p>Please note that if a user is initially added directly into the system through the "Add User" screen but then is added to a user CSV file or a Clever/ClassLink sync, the user will never be managed by the auto-archiving process in the system.  Because the user was added directly, an Admin will need to archive the user individually if necessary.</p>
Please contact our Help Desk with any questions regarding User Management.
To add a user here, complete the fields using a school email domain in the "Email" field.  Select the desired "Login Type" from the options described above.  The "Prevent Archive" button will default and cannot be changed.  If "Manual" is selected, an email invite can also be sent to the user, if the email address is a valid email, to set up or reset their password.  Then click the "Submit" button.  The user will be added to the master user list on the bottom half of the screen.
</p>',

    'form_add_substitute_pin_only' => '<h2>Substitute User PIN</h2>
<p>
If you would like to add a user who will only receive a PIN to approve
passes in and out of the classroom on a student&#8217;s device, this is
where you add them. Choose the Title or leave as none, add the First Name,
Last Name, and either enter a PIN or use the &#8220;generate PIN&#8221;,
then click Submit. You will be able to see this new user when you click on
the &#8220;Substitute Users&#8221; option on your left hand menu. This user
will NOT be able to log in to your school system. They will only be able to
interact with passes on a student&#8217;s device.
If you wish for the user to log in you must add them using the “Add User (full access)” button.
</p>',

    'import_users_user_menu' => '
<h2>
Steps for adding users by CSV upload
</h2>
<div style="font-size: 11pt; font-family: Arial">
<p>
This import option allows schools to provision users by loading a
Comma-Separated Values (CSV) file.
</p>
<p>
Here is a sample    <a href="/csv-samples/users-import-sample.csv">CSV File </a>
with the correct headers and column order.
</p>
<p>
</div>
',

    'import_kiosk_password_user_menu' => '<h2>Import Kiosk Password</h2>
<p >
If you do not want students to change their kiosk password make sure in
Modules - Kiosk - you have checked the box for &#8220;Hide Student Kiosk
Password Field.&#8221;
</p>',

    'user_menu_create_button' => '<p>
    This is a quick way to create several items that are also listed on your
    left hand menu.
</p>
<p >
    Click on any one of the buttons to see the pop-up to that feature. For many
    of the buttons if you wish to see the full feature you can utilize the
    &#8220;Go to Full Menu&#8221; at the bottom.
</p>
',

    'teacher_pass_pop_up' => '<h2>Teacher Pass (Proxy Pass)</h2>
<p>
This pop up allows you to create a pass from you to another adult or
location for a student who many not have their device, or who has used up
all of their student generated passes for the day.
</p>
<ol>
<li >
    <p >
        Choose the student from the drop down menu. Start typing their
        name, and when you find them click on their name.
    </p>
</li>
<li >
    <p >
        In the Where do you want to go? Choose the destination for this
        this student by using the drop down menu. Start to type the name of
        the person or location, when you see it, click on it. NOTE: If you
        can not find another adult in the drop down menu it is because they
        have set in their profile to not allow passes to them. Please
        contact the adult and ask them to change their profile settings.
    </p>
</li>
<li >
    <p >
        The Reason text box is optional, if you would like to add
        information about this pass you can do so by typing it in this
        area.
    </p>
</li>
<li >
    <p >
        The check box will automatically make the pass active on the
        dashboard. If you want to create the pass now and activate it
        later, simply uncheck this box.
    </p>
</li>
<li >
    <p >
        When you are done click Submit. You will be automatically taken to
        the dashboard.
    </p>
</li>
</ol>',

    'appointment_pass_pop_up' => '<h2>Appointments Pass</h2>
<p>
This is a shortcut to create one Appointment Pass. Fill in the information
and then click the blue submit button.
</p>
<p >
If you would like to make multiple appointments or would like to see the
full Appointment pass section simply click on &#8220;Go to full
menu&#8221;.
</p>',

    'out_of_office_pop_up' => '<h2>Out Of Office</h2>
<p>
    This is a shortcut to create your out of office. Fill in the information
    and then click the blue submit button.
</p>
<p >
    If you would like to visit the complete Out of Office section simply click
    on the &#8220;Go to full menu&#8221; at the bottom of the pop-up.
</p>',

    'a_b_polarity_pop_up' => '<h2>Contact Control</h2>
<p>
 This is a shortcut to create one Polarity pair. Fill in the information
 and then click the blue submit button.
</p>
<p >
 If you would like to go to the full Contact Control section simply click on
 the &#8220;Go to full menu&#8221; option at the bottom of the popup.
</p>',

    'limit_location_max_cap_pop_up' => '<h2>Limit Location Max Cap</h2>
<p>
This is a shortcut to create location max cap limits. Fill in the
information and then click the blue submit button.
</p>
<p >
If you would like to go to the full Limit Location Max Cap section simply
click on the &#8220;Go to full menu&#8221; option at the bottom of the
popup.
</p>',

    'limit_local_availability_pop_up' => '<h2>Limit Location Availability</h2>
<p>
This is a shortcut to create location availability limits. Fill in the
information and then click the blue submit button.
</p>
<p >
If you would like to go to the full Limit Location Availability section
simply click on the &#8220;Go to full menu&#8221; option at the bottom of
the popup.
</p>',

    'limit_students_passes_pop_up' => '<h2>Limit Student(s)</h2>
<p>
    This is a shortcut to create a student limit. Fill in the information and
    then click the blue submit button.
</p>
<p >
    If you would like to go to the full Limit Student Passes section simply
    click on the &#8220;Go to full menu&#8221; option at the bottom of the
    popup.
</p>',

    'limit_acive_passes_pop_up' => '<h2>Limit Active Passes</h2>
<p>
This is a shortcut for an Admin to create an active pass limit for the entire building.
An Admin can fill in the information and then click the blue submit button.
An Admin can go to the full Limit Active Passes section by simply clicking on the “Go to full menu” option at the bottom of the popup.
Teachers and Staff can view the Active Pass Limit settings to see what is included.
</p>',

    'pass_blocking_pop_up' => '<h2>Pass blocking</h2>
<p>
This is a shortcut to create a Pass Block. Fill in the information and then
click the blue submit button.
</p>
<p >
If you would like to go to the full Pass Blocking section simply click on
the &#8220;Go to full menu&#8221; option at the bottom of the popup.
</p>',

    'profile_settings_up_right' => '<h2>Profile Settings</h2>
<p>
<strong>Photo</strong>
</p>
<p>
If you would like to add a photo to your profile simply click on the camera
icon. You can choose an image that you will see when you log in.
Students will also see this image if they add you as a favorite to their device.
After you select your image it will auto save.  If you would like to remove this image you can click on the red words “Delete Image". If you would like to choose a different photo simply click on the camera icon and choose again. You can add either a jpg, jpeg, or png file and up to a max file size of 2MB.
</p>
<p>
<strong>Profile Settings</strong>
</p>
<p>
You may choose a title to display if you would like or you can keep this
blank by leaving it set to &#8220;none&#8221;.
</p>
<p>
<strong>My Locations</strong>
</p>
<p>
Admin and Staff roles allow for users to associate with a location.
You can choose your location from the dropdown and then save it by clicking the “Location Update” button.
Once selected you will see the location on your dashboard. You can adjust the location limit from the
bubble on your dashboard. You can add additional locations by using the
&#8220;Add location&#8221; button. You can remove locations you are
associated with by using the red trash can icon.
</p>
<p>
<strong>Student Check-in PIN</strong>
</p>
<p>
 If your school has enabled Student Check-in PIN this is where an Admin or Staff user can manage it.
 Once you add the PIN it will auto save.
 Click on the blue pencil to add a PIN or change a PIN. Students
 with an approved pass to this location can then check themselves in as
 arrived. They cannot use this PIN for any other purpose. You will need to
 put this PIN on display so students know which PIN to use. You can change
 this frequently throughout your day if you wish, just use the blue pencil
 to edit the PIN associated with your location.
</p>
<p>
<strong>Allow student appointment requests</strong>
</p>
<p>
If your school has enabled the ability for students to create appointment
requests to users with your role, this is where you can manage it for you
personally. By answering &#8220;Allow student appointment requests to
you&#8221; with a Yes, students can create an appointment request to come
see you. You will still be able to decide if that appointment is confirmed
or not. You will do this from your Dashboard. If you choose No as your
answer, students will not be able to make any appointment requests to you.
You can still create Appointments for students to come see you.
</p>
<p>
<strong>Allow passes to/from you</strong>
</p>
<p>
If you answer &#8220;Do you want adults/students to be able to create
passes to/from you?&#8221; as Yes - then you will be found on the student pass creation screen,
in the dropdown list of adults under both Departing From and Destination.
You will also be found by
other adults who are trying to make a Teacher (proxy) pass for a student.
If you answer No, students will not be able to find you under Departing
From or Destination. Adults will not be able to find you to write a
Teacher (proxy) pass for a student.
</p>
<p>
<strong>PIN</strong>
</p>
<p>
Your PIN is set by you and can be used to approve passes directly on the student’s device or at a Kiosk.  Your PIN can be edited at any point in time.
The system will ensure that your PIN is unique and will not let you create a PIN that someone else is already using.
Enter/edit your PIN and click update.
Upon a successful update the system will provide a green “Successfully updated” message.
</p>',

    'modules_appointment_pass_and_request' => '<h2>Appointment Pass Module</h2>
<p >
In order to activate this feature, and make the &#8220;Appointment
Pass&#8221; menu option available to your users, toggle the button to the
right of the title from Off to On. You should see the toggle change from
red to green, indicating you have turned on Appointment Pass.
</p>
<p >
Email alert notification:
If you would like your students to receive email alert notifications about their appointment passes, check the box to the left of this statement.
Notifications will be sent to the student approximately 20 minutes and 5 minutes prior to the appointment time.

</p>
<p >
Turn off Admins ability to edit other user’s Appointment Passes:
Check this box if you would like to remove the ability of Admins to edit other users’ appointment passes.
If you leave it unchecked then Admins will be able to edit other user\'s appointment passes.
</p>
<p >
Student Appointment Requests
</p>
<p >
If you would like to allow students to create appointment requests this is
where you will manage this feature.
 First choose which roles you would like to enable and if you would like to also allow locations.
Check the box to the left of the roles and/or locations.
</p>
<p>
If you allow a specific role, the individual users will need to set their
personal permission in their profile settings. If you allow locations an
Admin will need to go to the Pass Settings - Rooms - and change the
&#8220;no&#8221; to &#8220;yes&#8221; in the new column: Allow students
APT Requests. The rooms will default to &#8220;no&#8221; and have a red
background. When you change this to &#8220;yes&#8221; you will see the
toggle change to green.
</p>',

    'module_autocheck_in' => '<h2>Auto Check-In Module</h2>
<p>
    If you would like to allow your Admin and Staff, who are associated with a location in their profile,
    to set up a PIN that students can use to check themselves into the location, you must toggle this setting from Off to On.
    You will see the toggle change from red to green, indicating that you have enabled this
    feature. Admin and Staff users in their profile will then be able to add a
    check-in PIN to those locations they are associated with.
</p>',

    'module_kiosk' => '<h2>Kiosk Module</h2>
<p>
If you would like to activate Kiosk slide the toggle form Off to On. You
will see the toggle turn form red to green indicating that you have turned
this feature on.
</p>
<p >
You then can decide if you would like to restrict the Kiosk method for
users in your school to only the URL Klogin. This is highly recommended
for Chromebooks.
</p>
<p >
 If your school utilizes student IDs containing a barcode or magnetic strip and is interested in using these in conjunction with the Kiosk Module, you can set the appropriate option here as the default login method.
 (Please keep in mind that the school is responsible for providing all necessary hardware.
 It is also recommended that you work closely with one of our Project Managers to set this up and test prior to making this available to your school.)
</p>
<p >
 The last Kiosk Mode setting option allows you to hide the Kiosk password field from students, which means Admins will control the creation and management of the Kiosk passwords for students.
 If you leave this unchecked, students can create their Kiosk password on their profile screen.
</p>',
    'todays_history' => '<p >
Today&#8217;s History lets you see at a glance what passes this student has
already had today. You can see where they went and how long they were out
on each pass and total. This is provided to you to help you decide if you
want to approve a pass request from a student who is requesting to leave
your classroom. Once you see the information on this display, you can use
the &#8220;X&#8221; to close it and then make an informed decision about
the current pass request.
</p>',
    'header asist' => '
    <style>
    .pass-status {
	 position: relative;
	 top: 0px;
	 left: 0px;
	 padding: 2px 20px 4px 20px;
	 background: linear-gradient(0deg, #7c7d7d, #6f7071);
	 color: #fff;
	 font-family: "VisbyCF-Bold";
	 text-transform: uppercase;
	 border-radius: 0 0 10px 0;
	 box-shadow: 0 0 10px -3px #000;
}
 .pass-status.waiting-confirmation {
	 background: #ff0006;
	 color: #fff;
}
 .pass-status.future-apt {
	 background: #e7c3a5;
	 color: #fff;
}
 .pass-status.wating-activation {
	 background: #fff;
	 color: #000;
	 border: 1px solid #000;
}
 .pass-status.activated, .pass-status.active {
	 background: #00b973;
	 color: #fff;
}
 .pass-status.waiting-activation, .pass-status.waiting-approval {
	 background: #fdfefd;
	 border-color: #000;
	 color: #000;
	 border: 1px solid #000;
	 border-top: none;
	 border-left: none;
}
 .pass-status.today-pass {
	 background: #d0cce3;
}
 .pass-status.ended {
	 background: #7e7e7e;
}
 .pass-status.missed-apt {
	 background: #67a0cc;
}
 .pass-status.canceled {
	 background: #7278e0;
}
 .pass-status.on-other-pass {
	 background: #54c4e3;
}
 .pass-status.min-time {
	 background: #fff200;
	 color: #000;
}
 .pass-status.missed-request, .pass-status.missed {
	 background: #007c71;
}

</style>
<h4>Dashboard color legend:</h4>
<hr />

    <div class="pass-status waiting-approval">Waiting approval</div>
    <p>White - Waiting Approval</pst>

    <br />

    <div class="pass-status activated" style="margin-top 30px;">Active</div>
    <p>Green - Active</pst>

    <br />

    <div class="pass-status min-time" style="margin-top 30px;">10+ Min</div>
    <p>Yellow - Long runnning</pst>

    <br />

    <div class="pass-status ended" style="margin-top 30px;">Ended</div>
    <p>Gray - Ended</pst>

    <br />
    <br />
<span class="status-flag" style="color: #DF3C3B; font-size: 24px;">
<i class="flaticon-flag-2"></i>  <b style="display: inline; color: gray; font-size: 12px; bottom: 50%;"> Red Flag - System ended. </b>
</span>
<br />
<span class="status-flag" style="color: #F3EA65; font-size: 24px;">
<i class="flaticon-flag-2"></i>  <b style="display: inline; color: gray; font-size: 12px; bottom: 50%;"> Yellow Flag - Long running. </b>
</span>
<br />
<span class="status-flag" style="color: #682B91; font-size: 24px;">
<i class="flaticon-flag-2"></i>  <b style="display: inline; color: gray; font-size: 12px; bottom: 50%;"> Purple Flag - System ended extended. </b>
</span>
<br />
<div style="background-color: red; border-radius: 10px; padding: 2px 5px 2px 5px; width: 40%; font-size: 9px; color: white; text-align: center; font-weight: bold;">
<b style="text-align:center;" >System Ended. </b>
</div>
<p>System ended badge</p>
<br />
<h4>Appointment Pass color legend:</h4>
<hr />

    <div class="pass-status waiting-approval">Waiting activation</div>
    <br />

    <div class="pass-status today-pass" style="margin-top 30px;">Today APT</div>
    <br />

    <div class="pass-status future-apt" style="margin-top 30px;">Future Apt</div>
    <br />

    <div class="pass-status missed-apt" style="margin-top 30px;">Missed Apt</div>

    <br />
    <div class="pass-status canceled" style="margin-top 30px;">Canceled</div>

    <br />
    <div class="pass-status on-other-pass" style="margin-top 30px;">On Other Pass</div>

    <br />
    <div class="pass-status waiting-confirmation" style="margin-top 30px;">Waiting Confirmation</div>

    <br />
    <div class="pass-status missed-request" style="margin-top 30px;">Missed Request</div>


    <br />
    ',
    'auto_pass_limit' => '
<h3>Auto Pass Limit</h3>
<p>Auto Pass allows students to activate their own pass.</p>
<li>Select the location(s) to enable “Auto Pass”.</li>
<li>Select how many students can be out on an Auto Pass at the same time (or no limit).</li>
',
    'fav_rooms_help' => '
<h4>Favorites</h4>
<p><b>To add </b> a favorite first go to #1 and click in the box.  You can either scroll through your adults and locations or start to type what you are looking for.</p>
      <p>Once you find it, click on it and then click the blue “Submit” button.</p>
       <p>You will then need to click again on “Edit” from the main screen, and then “Add/Remove Favorite”. </p>
       <p>You will now see the new location in #2. You can click on it to choose your icon and then click the blue “Submit” button.</p>
<p><b>To Remove</b> a favorite go to #1 and click the “X” to the left of the location, then click the blue “Submit” button.
 This favorite has been removed from both Teacher Pass (Proxy Pass) and Student devices as Admin destinations.</p>
    '
];
