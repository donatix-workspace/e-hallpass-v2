<?php

namespace App\Elastic\Rules;

use App\Models\AuthType;
use App\Models\School;
use App\Models\V1\User;
use ScoutElastic\Builders\SearchBuilder;
use ScoutElastic\SearchRule;

class BelongsToSchoolSearchRule extends SearchRule
{
    private $authenticatedUser;
    private $serviceSearchType;

    public function __construct(SearchBuilder $builder)
    {
        $this->serviceSearchType = request()->get('type');
        $this->authenticatedUser =
            $this->serviceSearchType !== 'kiosk'
                ? auth()->user()
                : School::where('cus_uuid', request()->get('cus_uuid'))
                    ->select('id')
                    ->firstOrFail();

        parent::__construct($builder);
    }

    /**
     * @inheritdoc
     */
    public function buildHighlightPayload()
    {
        //
    }

    /**
     * @inheritdoc
     */
    public function buildQueryPayload(): array
    {
        //        $nestedSchoolQuery = [
        //            'nested' => [
        //                'path' => 'school_user',
        //                'query' => [
        //                    'bool' => [
        //                        'must' => [
        //                            'match' => [
        //                                'school_user.id' =>
        //                                    $this->serviceSearchType === 'kiosk'
        //                                        ? $this->authenticatedUser->id
        //                                        : $this->authenticatedUser->school_id
        //                            ]
        //                        ]
        //                    ]
        //                ]
        //            ]
        //        ];

        $skipSystemElasticQuery = [
            'bool' => [
                'must_not' => [
                    [
                        'wildcard' => [
                            'email' => 'system@*'
                        ]
                    ]
                ]
            ]
        ];

        $this->builder->wheres['filter'][] = $skipSystemElasticQuery;

        $skipHiddenElasticQuery = [
            'bool' => [
                'should' => [
                    [
                        'bool' => [
                            'must_not' => [
                                [
                                    'exists' => [
                                        'field' => 'auth_type'
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        'bool' => [
                            'must_not' => [
                                [
                                    'match' => [
                                        'auth_type' => AuthType::HIDDEN
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if (request()->has('archived')) {
            $archivedElasticQuery = [
                'nested' => [
                    'path' => 'role_user',
                    'query' => [
                        'bool' => [
                            'must' => [
                                [
                                    'match' => [
                                        'role_user.pivot.school_id' =>
                                            $this->serviceSearchType === 'kiosk'
                                                ? $this->authenticatedUser->id
                                                : $this->authenticatedUser
                                                    ->school_id
                                    ]
                                ],
                                [
                                    'match' => [
                                        'role_user.pivot.status' => 1
                                    ]
                                ],
                                [
                                    'match' => [
                                        'role_user.pivot.deleted_at' => 0
                                    ]
                                ]
                            ],
                            'must_not' => []
                        ]
                    ]
                ]
            ];

            $archivedStatus = filter_var(
                request()->get('archived'),
                FILTER_VALIDATE_BOOLEAN
            );

            if ($archivedStatus) {
                $archivedElasticQuery['nested']['query']['bool'][
                    'must_not'
                ][] = [
                    'match' => [
                        'role_user.pivot.archived_at' => 0
                    ]
                ];
            } else {
                $archivedElasticQuery['nested']['query']['bool']['must'][] = [
                    'match' => [
                        'role_user.pivot.archived_at' => 0
                    ]
                ];
            }

            $this->builder->wheres['filter'][] = $archivedElasticQuery;
        }

        $this->builder->wheres['filter'][] = $skipHiddenElasticQuery;

        // Check invalid memberships
        $this->builder->wheres['filter'][] = [
            'nested' => [
                'path' => 'role_user',
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'match' => [
                                    'role_user.pivot.school_id' =>
                                        $this->serviceSearchType === 'kiosk'
                                            ? $this->authenticatedUser->id
                                            : $this->authenticatedUser
                                                ->school_id
                                ]
                            ],
                            [
                                'match' => [
                                    'role_user.pivot.status' => 1
                                ]
                            ],
                            [
                                'match' => [
                                    'role_user.pivot.deleted_at' => 0
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $memberShipRoleSearch = [];
        $memberShipSisId = '';

        if (
            $this->hasColumn('role_user.name') &&
            $this->hasColumn('role_user.pivot.school_id')
        ) {
            $memberShipRoleSearch = $this->extractSearchRole();
        }

        if ($this->hasColumn('student_sis_id')) {
            $memberShipSisId = $this->extractStudentSisId();
        }

        //        if (!empty($memberShipSisId)) {
        //            $this->builder->wheres['filter'][] = [
        //                'nested' => [
        //                    'path' => 'role_user',
        //                    'query' => [
        //                        'bool' => [
        //                            'must' => [
        //                                [
        //                                    'wildcard' => [
        //                                        'role_user.name' => $memberShipSisId
        //                                    ]
        //                                ],
        //                                [
        //                                    'match' => [
        //                                        'role_user.pivot.school_id' =>
        //                                            $this->serviceSearchType === 'kiosk'
        //                                                ? $this->authenticatedUser->id
        //                                                : $this->authenticatedUser
        //                                                    ->school_id
        //                                    ]
        //                                ],
        //                                [
        //                                    'match' => [
        //                                        'role_user.pivot.status' => 1
        //                                    ]
        //                                ],
        //                                [
        //                                    'match' => [
        //                                        'role_user.pivot.deleted_at' => 0
        //                                    ]
        //                                ]
        //                            ]
        //                        ]
        //                    ]
        //                ]
        //            ];
        //        }
        //
        // We have membership roles search, starting implement nested query
        if (count($memberShipRoleSearch)) {
            $this->builder->wheres['filter'][] = [
                'nested' => [
                    'path' => 'role_user',
                    'query' => [
                        'bool' => [
                            'must' => [
                                [
                                    'wildcard' => [
                                        'role_user.name' =>
                                            $memberShipRoleSearch['role']
                                    ]
                                ],
                                [
                                    'match' => [
                                        'role_user.pivot.school_id' =>
                                            $memberShipRoleSearch['school_id']
                                    ]
                                ],
                                [
                                    'match' => [
                                        'role_user.pivot.status' => 1
                                    ]
                                ],
                                [
                                    'match' => [
                                        'role_user.pivot.deleted_at' => 0
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        if ($this->builder->query === '__all') {
            return [
                'must' => ['match_all' => (object) []]
            ];
        }

        return [
            'must' => [
                'query_string' => [
                    'query' => $this->builder->query
                ]
            ]
        ];
    }

    /**
     * @param string $column
     * @return false|int
     */
    public function hasColumn(string $column): bool
    {
        return preg_match('/' . $column . '/', $this->builder->query);
    }

    /**
     * @return mixed|string
     */
    public function extractStudentSisId()
    {
        $sisId = $this->hasColumn('student_sis_id');

        if ($sisId) {
            $buildQueryExtractValues = explode('AND', $this->builder->query);

            foreach ($buildQueryExtractValues as $columnAndValue) {
                [$column, $value] = explode(
                    ':',
                    preg_replace('/\s+/', '', $columnAndValue)
                );

                if ($column === 'student_sis_id') {
                    return $value;
                }
            }
        }

        return '';
    }

    /**
     * @return array
     */
    public function extractSearchRole(): array
    {
        $roleName = $this->hasColumn('role_user.name');
        $roleSchoolId = $this->hasColumn('role_user.pivot.school_id');

        $queryArrayBuilder = [];

        $query = $this->builder->query;

        if ($roleName && $roleSchoolId) {
            $buildQueryExtractValues = explode('AND', $this->builder->query);
            foreach ($buildQueryExtractValues as $key => $columnAndValue) {
                $columnAndValueWithoutSpaces = explode(
                    ':',
                    preg_replace('/\s+/', '', $columnAndValue)
                );

                if (
                    $columnAndValueWithoutSpaces[0] ===
                    'role_user.pivot.school_id'
                ) {
                    $queryArrayBuilder['school_id'] = intval(
                        $columnAndValueWithoutSpaces[1]
                    );
                    unset($buildQueryExtractValues[$key]);
                }

                if ($columnAndValueWithoutSpaces[0] === 'role_user.name') {
                    $queryArrayBuilder['role'] =
                        $columnAndValueWithoutSpaces[1];

                    unset($buildQueryExtractValues[$key]);
                }
            }

            $query = implode(' AND ', $buildQueryExtractValues);
        }

        if (empty($query)) {
            $query = '__all';
        }

        $this->builder->query = $query;

        return $queryArrayBuilder;
    }
}
