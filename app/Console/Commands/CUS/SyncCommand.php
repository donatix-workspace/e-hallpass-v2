<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonSchoolDomain;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Translators\SchoolTranslator;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CancelAndUpdateAppointmentsAfterArchiveJob;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolModule;
use App\Models\SchoolUser;
use App\Models\TeacherPassSetting;
use App\Models\UnArchive;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class SyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:sync {--entity=} {--skip=} {--mins=} {--uuid=} {--force} {--userId=} {--userEmail=} {--schoolId=} {--from=} {--to=} {--membershipUuid=} {--roleId=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync data with Command User Service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $minutes = $this->option('mins') ?? 2;
        $date = now('UTC')->subMinutes($minutes);
        $toDate = now('UTC')->subMinutes($minutes - 1);
        $schoolUuid = $this->option('uuid') ?? null;
        $schoolId = $this->option('schoolId') ?? null;
        $force = $this->option('force') ?? false;
        $userId = $this->option('userId') ?? null;
        $userEmail = $this->option('userEmail') ?? null;
        $membershipUuid = $this->option('membershipUuid') ?? null;

        if ($schoolId && $school = School::select('cus_uuid')->where('id', $schoolId)->first()) {
            $schoolUuid = $school->cus_uuid;
        }

        if ($schoolId == 'all') {
            foreach (School::select('cus_uuid', 'name', 'id')->get() as $school) {
                $this->info("Sync School {$school->id} {$school->name}");
                $this->syncUsers($date, $toDate, $school->cus_uuid, $force, $userId, $userEmail);
            }
        }

        switch ($this->option('entity')) {
            case 'schools':
                $this->syncSchools($date, $schoolUuid, $force);
                break;
            case 'users':
                $this->syncUsers(
                    $date,
                    $toDate,
                    $schoolUuid,
                    $force,
                    $userId,
                    $userEmail,
                    $membershipUuid
                );
                break;
            default:
                $this->syncSchools($date, $schoolUuid, $force);
                $this->syncUsers(
                    $date,
                    $toDate,
                    $schoolUuid,
                    $force,
                    null,
                    null,
                    $membershipUuid
                );
        }

        return 0;
    }

    public function syncSchools($date = null, $schoolUuid, $force = false)
    {
        $this->info('Sync Schools From CUS DB');

        if ($force) {
            $date = null;
            $schoolsWithModifiedDomains = [];
        } else {
            $schoolsWithModifiedDomains = CommonSchoolDomain::select(
                'common_bm_school_id',
                'created',
                'modified'
            )
                ->when($date, function ($query) use ($date) {
                    $query
                        ->where('created', '>=', $date)
                        ->orWhere('modified', '>=', $date)
                        ->orWhere('archived_at', '>=', $date);
                })
                ->get()
                ->pluck('common_bm_school_id')
                ->toArray();
        }

        $dbQuery = Db::connection('commonDb')
            ->table('common_bm_school')
            ->when($schoolUuid, function ($subQuery) use ($schoolUuid) {
                $subQuery->where('uuid', $schoolUuid);
            })
            ->when($date, function ($subQuery) use ($date) {
                $subQuery->where(function ($subSubQuery) use ($date) {
                    $subSubQuery
                        ->where('created', '>=', $date)
                        ->orWhere('modified', '>=', $date);
                });
            })
            ->when(!empty($schoolsWithModifiedDomains), function (
                $subQuery
            ) use ($schoolsWithModifiedDomains) {
                $subQuery->orWhereIntegerInRaw(
                    'id',
                    $schoolsWithModifiedDomains
                );
            })
            ->orderBy('id', 'asc');

        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk(5, function ($commonSchools) {
            foreach ($commonSchools as $commonSchool) {

                $domains = CommonSchoolDomain::where('common_bm_school_id', $commonSchool->id)
                    ->where('r_status', 1)
                    ->where('not_archived', 1)
                    ->get()
                    ->pluck('email_domain');

                $state = $commonSchool->l10n_us_territorial_subdivision_code_ansi;

                $schoolModel = School::where('cus_uuid', $commonSchool->uuid)->first();
                $skip = false;

                if (!$schoolModel) {

                    $schoolModels = School::query()
                        ->whereNull('cus_uuid')
                        ->where(function ($query) use ($commonSchool) {
                            $query->where('name', $commonSchool->full_name)
                                ->orWhere('name', $commonSchool->r_name);
                        });

                    if ($schoolModels->count() > 1) {

//                        $schoolModels->where(function ($query) use ($state) {
//                            $query->where('description', $state)->orWhere('state', $state); //Remove descr
//                        });

                        $schoolModels->where('state', $state);//->where('zip',$commonSchool->zip_code); //Remove descr

                        if ($schoolModels->count() > 1) {
                            if (!$domains->isEmpty()) {
                                $schoolModels->where(function ($query) use ($domains) {
                                    $query->whereIn('student_domain', $domains)->whereIn('school_domain', $domains);
                                });
                            } else {
                                $schoolModels->where('status', $commonSchool->r_status);
                            }
                        }
                    }

                    if ($schoolModels->count() == 1) {
                        $schoolModel = $schoolModels->first();
                    }
                    if ($schoolModels->count() == 0) {
                        $schoolModel = null;
                    }
                    if ($schoolModels->count() > 1) {
                        $skip = true;
                    }

                }

                if (!$skip) {
                    $transformedData = SchoolTranslator::translateDb($commonSchool);

                    $schoolProviders = DB::connection('commonDb')
                        ->table('common_bm_school_auth_provider')
                        ->where('common_bm_school_id', $commonSchool->id)
                        ->get()
                        ->pluck('auth_provider_id');

                    $transformedData['auth_providers'] = json_encode($schoolProviders);

                    if ($schoolModel) {
                        $schoolModel->update($transformedData);
                    } else {
                        if ($schoolModel = School::create($transformedData)) {
                            TeacherPassSetting::createDefaultSettings($schoolModel);
                        }
                    }


                    $schoolModel->domains()->delete();

                    foreach ($domains as $domain) {
                        $schoolModel->domains()->create([
                            'name' => $domain
                        ]);
                    }

                    SchoolModule::dropOrCreateSchoolModule(
                        $schoolModel,
                        Module::APPOINTMENTPASS_ID,
                        $commonSchool->appointment_pass
                    );
                    SchoolModule::dropOrCreateSchoolModule(
                        $schoolModel,
                        Module::AUTO_CHECKIN_ID,
                        $commonSchool->auto_check_in
                    );
                    SchoolModule::dropOrCreateSchoolModule(
                        $schoolModel,
                        Module::KIOSK_ID,
                        $commonSchool->kiosk
                    );
                }


                $this->output->progressAdvance();
            }
        });
    }

    protected function syncUsers(
        $date,
        $toDate,
        $schoolUuid,
        $force = false,
        $userId,
        $userEmail,
        $membershipUuid = null
    )
    {
        $this->info('Users Sync after ' . $date);
        $skip = (int)$this->option('skip') ?? 0;
        $from = $this->option('from') ?? 0;
        $to = $this->option('to') ?? 3000;

        $ehpClientId = $this->getEhpClientId();
        if ($force) {
            return $this->forceUsersSync($schoolUuid, $from, $to, $membershipUuid);
        }
        return $this->normalUsersSync($ehpClientId, $date, $schoolUuid);

    }

    protected function getEhpClientId()
    {
        return 2;
    }

    protected function forceUsersSync($schoolUuid, $from, $to, $membershipUuid = null)
    {

        if ($schoolUuid) {
            $schools = School::where('cus_uuid', $schoolUuid)->get();
        } elseif ($membershipUuid) {
            $this->normalUsersSync(2, null, null, $membershipUuid);
            return true;
        } else {
            $schools = School::where('id', '>', $from)->where('id', '<=', $to)->get();

        }

        foreach ($schools as $school) {
            $this->info("Sync {$school->name} with id {$school->id}");

            if ($school->cus_uuid) {
                $this->normalUsersSync(2, null, $school->cus_uuid);
            }


        }

    }

    protected function normalUsersSync($ehpClientId, $date, $schoolUuid = null, $membershipUuid = null)
    {

        if ($schoolUuid) {

            $roleId = $this->option('roleId') ?? null;


            $union = Db::connection('commonDb')
                ->table('common_bm_membership as m')
                ->join('common_bm_school as s', 'm.common_bm_school_id', '=', 's.id')
                ->selectRaw('m.auth_user_id as user_id')
                ->where('m.auth_client_id', 2)
                ->when($roleId, function ($query) use ($roleId) {
                    $query->where('m.common_bm_role_membership_id', $roleId);
                })
                ->where('s.uuid', $schoolUuid);
        } elseif ($membershipUuid) {
            $union = Db::connection('commonDb')
                ->table('common_bm_membership as m')
                ->join('common_bm_school as s', 'm.common_bm_school_id', '=', 's.id')
                ->selectRaw('m.auth_user_id as user_id')
                ->where('m.auth_client_id', 2)
                ->where('m.uuid', $membershipUuid);
        } else {
            $memberships = Db::connection('commonDb')
                ->table('common_bm_membership as m')
                ->selectRaw('m.auth_user_id as user_id')
                ->where('m.auth_client_id', $ehpClientId)
                ->where('m.modified', '>=', $date);

            $archivedMemberships = Db::connection('commonDb')
                ->table('common_bm_membership as m')
                ->selectRaw('m.auth_user_id as user_id')
                ->where('m.auth_client_id', $ehpClientId)
                ->where('m.archived_at', '>=', $date);

            $createdUsers = Db::connection('commonDb')
                ->table('common_bm_membership as m')
                ->join('auth_user as u', 'm.auth_user_id', '=', 'u.id')
                ->selectRaw('u.id as user_id')
                ->where('m.auth_client_id', $ehpClientId)
                ->where('u.created', '>=', $date);

            $updatedUsers = Db::connection('commonDb')
                ->table('common_bm_membership as m')
                ->join('auth_user as u', 'm.auth_user_id', '=', 'u.id')
                ->selectRaw('u.id as user_id')
                ->where('m.auth_client_id', $ehpClientId)
                ->where('u.modified', '>=', $date);

            $union = $memberships->union($archivedMemberships)->union($createdUsers)->union($updatedUsers);
        }


//        $this->info($union->count());
        $this->output->progressStart($union->count());
        $union->orderBy('user_id')->chunk(10, function ($commonUserIds) {

            foreach ($commonUserIds as $commonUserId) {

                $userId = $commonUserId->user_id;

                $commonSchoolsIds = Db::connection('commonDb')
                    ->table('common_bm_membership as m')
                    ->selectRaw('m.common_bm_school_id as school_id',
                    )
                    ->where('m.auth_user_id', $userId)
                    ->where('m.auth_client_id', 2)
                    ->groupBy('m.common_bm_school_id')
                    ->get()
                    ->pluck('school_id');

                foreach ($commonSchoolsIds as $commonSchoolsId) {

                    $membership = Db::connection('commonDb')
                        ->table('common_bm_membership as m')
                        ->selectRaw('m.id')
                        ->where('m.auth_user_id', $userId)
                        ->where('m.common_bm_school_id', $commonSchoolsId)
                        ->where('m.auth_client_id', 2)
                        ->orderBy('id', 'desc')
                        ->first();

                    $fullRecord = Db::connection('commonDb')
                        ->table('common_bm_membership as m')
                        ->join('auth_user as u', 'm.auth_user_id', '=', 'u.id')
                        ->join('common_bm_school as s', 'm.common_bm_school_id', '=', 's.id')
                        ->leftJoin('auth_title as t', 'u.auth_title_id', '=', 't.id')
                        ->selectRaw('u.uuid as user_uuid,
                        u.email as email,
                        m.sis_id as student_sis_id,
                        u.username as username,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        m.common_bm_role_membership_id as role_id,
                        u.image as avatar,
                        m.graduation_year as grade_year,
                        u.r_status as user_status,
                        m.r_status as membership_status,
                        u.last_login_time as last_login_time,
                        u.created as created_at,
                        u.modified as updated_at,
                        s.uuid as school_uuid,
                        m.uuid as membership_uuid,
                        t.r_name as title,
                        u.id as common_user_id,
                        m.archived_at as deleted_at
                        ',
                        )
                        ->where('m.id', $membership->id)
                        ->first();

                    if ($fullRecord) {
                        $school = School::where('cus_uuid', $fullRecord->school_uuid)->first();

                        if ($school) {
                            $data = UserTranslator::translateJoinedRecord($fullRecord, $school);

                            $user = User::withoutGlobalScopes()->where('cus_uuid', $data['cus_uuid'])->first();

                            if (!$user) {
                                $user = User::withoutGlobalScopes()->where('email', $data['email'])
                                    ->whereNotNull('email')->first();
                                unset($data['email']);
                            }

                            if (!$user) {

                                $userMembership = DB::table('schools_users as m')
                                    ->join('users as u', 'm.user_id', '=', 'u.id')
                                    ->select('u.id')
                                    ->where('m.school_id', $school->id)
                                    ->where(function ($query) use ($data) {
                                        $query->where(function ($q) use ($data) {
                                            $q->where('m.student_sis_id', $data['student_sis_id'])
                                                ->whereNotNull('m.student_sis_id')
                                                ->whereNotIn('m.student_sis_id',
                                                    ["", "#N/A", 0, 0000, 00000, 000000, 0000000, 00000000]);
                                        })
                                            ->orWhere(function ($q) use ($data) {
                                                $q->where('m.student_number', $data['student_sis_id'])
                                                    ->whereNotNull('m.student_number')
                                                    ->whereNotIn('m.student_number',
                                                        ["", "#N/A", 0, 0000, 00000, 000000, 0000000, 00000000]);
                                            });
                                    })
                                    ->orderBy('m.id', 'desc')
                                    ->first();

                                if ($userMembership) {
                                    $user = User::withoutGlobalScopes()->where('id', $userMembership->id)->first();
                                }

                            }

                            User::enableMassIndex();

                            $data['email'] = $fullRecord->email;

                            if ($user) {
//                            if ($user->isArchivePrevented($school)) {
//                                unset($data['deleted_at']);
//                            }
                                $user->update($data);
                            } else {
                                $user = User::create($data);
                            }

                            $membershipData = [
                                'membership_uuid' => $fullRecord->membership_uuid,
                                'role_id' => CommonMembership::getRoleId($fullRecord->role_id),
                                'deleted_at' => $fullRecord->deleted_at,
                                'updated_at' => now(),
                                'status' => !$fullRecord->deleted_at,
                                'student_sis_id' => $data['student_sis_id']
                            ];

                            DB::table('schools_users')->updateOrInsert(
                                [
                                    'school_id' => $school->id,
                                    'user_id' => $user->id,
                                ], $membershipData);
                            if (!$fullRecord->deleted_at === User::INACTIVE) {
                                CancelAndUpdateAppointmentsAfterArchiveJob::dispatch($user)->onQueue('appointments');
                            }

                            $user->searchable();
                            User::disableMassIndex();
                        }
                    }
                }

                $this->output->progressAdvance();
            }
//
        });

        return true;
    }

}
