<?php

namespace Tests\Feature;

use App\Models\School;
use App\Models\SchoolModule;
use App\Models\User;
use Database\Seeders\ModuleSeeder;
use Database\Seeders\RolesDatabaseSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ModuleTest extends TestCase
{
    use RefreshDatabase;

    protected $school;
    protected $adminUser;

    protected function setUp(): void
    {
        parent::setUp();
        // Run roles migration
        (new RolesDatabaseSeeder())->run();
        (new ModuleSeeder())->run();

        $this->school = School::factory()->createOne();

        $this->adminUser = User::factory()->createOne([
            'role_id' => 2,
            'school_id' => $this->school->id
        ]);
    }

    /**
     * @test
     * An admin user can view active modules page
     * @return void
     */
    public function an_admin_user_can_view_active_modules_page()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.admin.modules.active'))
            ->assertJsonStructure(['data', 'status']);
    }

    /**
     * @test
     * An admin user can update active modules options
     * @return void
     */
    public function an_admin_user_can_update_active_modules_options()
    {
        SchoolModule::create([
            'module_id' => 1,
            'school_id' => $this->school->id,
            'permission' => 0,
            'status' => 1,
        ]);

        $this->actingAs($this->adminUser)
            ->put(route('api.v5.admin.modules.update', ['module' => 1]), [
                'option_json' => '{"peer":"Peer Email Notification ","sms":"SMS Notification","email":"Email Alert Notification"}'
            ])
            ->assertOk()
            ->assertJsonStructure(['data', 'status']);
    }

    /**
     * @test
     * An admin user can view all available modules
     * @return void
     */
    public function an_admin_user_can_view_all_available_modules()
    {
        $this->actingAs($this->adminUser)
            ->get(route('api.v5.admin.modules'))
            ->assertJsonStructure(['data', 'status'])
            ->assertOk();
    }
}
