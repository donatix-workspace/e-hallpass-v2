<?php

namespace App\Events;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;

class FavoriteUnavailableEvent implements ShouldBroadcastNow
{
    use Dispatchable;

    private $schoolId;
    public $forFavorite;

    /**
     * @param int $schoolId
     * @param array $forFavorite
     */
    public function __construct(int $schoolId, array $forFavorite)
    {
        /*
         *    {
         *             type: "App\Models\User",
         *             id: 1,
         *             unavailability: {
         *                  ....
         *             }
         *    }
         */
        $this->forFavorite = [
            'id' => $forFavorite['id'],
            'type' => $forFavorite['type'],
            'unavailability' =>
                $forFavorite['unavailability'] !== null
                    ? [
                        'id' => $forFavorite['unavailability']['id'],
                        'from_date' =>
                            $forFavorite['unavailability']['from_date'],
                        'to_date' => $forFavorite['unavailability']['to_date'],
                        'comment' => $forFavorite['unavailability']['comment']
                    ]
                    : null
        ];

        /**
         * @var int School id
         */
        $this->schoolId = $schoolId;
    }

    /**
     * @return PrivateChannel
     */
    public function broadcastOn(): PrivateChannel
    {
        return new PrivateChannel('student.unavailables.' . $this->schoolId);
    }
}
