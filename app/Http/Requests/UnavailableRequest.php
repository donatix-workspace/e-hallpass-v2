<?php

namespace App\Http\Requests;

use App\Rules\DaysKeyExistRule;
use App\Rules\RecurrenceMonthlyCorrectStructureRule;
use App\Rules\RecurrenceDaysRule;
use App\Rules\RecurrenceTypeRule;
use App\Rules\RoomOrUserExistsRule;
use App\Rules\RoomOrUserStringRule;
use App\Rules\TimeAfterNowRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class UnavailableRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && (auth()->user()->isAdmin() || auth()->user()->isTeacher() || auth()->user()->isStaff());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unavailable_id' => ['required', 'integer', new RoomOrUserExistsRule(Request::all())],
            'unavailable_type' => ['required', 'string', new RoomOrUserStringRule()],
            'from_date' => "date|required|after_or_equal:today",
            'from_time' => ['required', 'date_format:H:i'],
            'to_time' => ['required', 'date_format:H:i'],
            'to_date' => 'date|required|after_or_equal:from_date',
            'comment' => 'nullable|string|max:255',
            'recurrence_type' => ['nullable', new RecurrenceTypeRule()],
            'recurrence_days' => ['json', 'nullable', new DaysKeyExistRule(), new RecurrenceDaysRule()],
            'recurrence_week' => ['json', 'nullable', 'required_if:recurrence_type,monthly', new RecurrenceMonthlyCorrectStructureRule()],
            'recurrence_end_at' => 'date|after:yesterday|nullable'
        ];
    }
}
