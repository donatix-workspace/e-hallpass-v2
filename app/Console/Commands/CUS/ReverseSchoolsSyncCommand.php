<?php

namespace App\Console\Commands\CUS;

use App\Http\Services\CommonUserService\CommonUserService;
use App\Http\Services\CommonUserService\EntityService;
use App\Http\Services\CommonUserService\Models\CommonDistrict;
use App\Http\Services\CommonUserService\Models\CommonMembership;
use App\Http\Services\CommonUserService\Models\CommonSchool;
use App\Http\Services\CommonUserService\Models\CommonSchoolDomain;
use App\Http\Services\CommonUserService\Models\CommonTimezone;
use App\Http\Services\CommonUserService\Models\CommonUser;
use App\Http\Services\CommonUserService\Models\SchoolsEhp;
use App\Http\Services\CommonUserService\Translators\UserTranslator;
use App\Jobs\CancelAndUpdateAppointmentsAfterArchiveJob;
use App\Jobs\CreateCommonUserJob;
use App\Models\Domain;
use App\Models\Module;
use App\Models\School;
use App\Models\SchoolUser;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ReverseSchoolsSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cus:rschools {--migrate} {--mapEHP} {--mapUuid} {--mapCUS} {--mapDistricts} {--mapTimezoneNames} {--mapTimezoneCUS} {--createDistricts} {--createSftp} {--createSchools} {--createSchool=} {--createUsers} {--mapUsers} {--updateUsers} {--updateSftpSchools} {--fixSchools} {--usersAnalize=?} {--createMissingMemberships} {--updateDomains} {--retryMemberships=} {--createNewMemberships}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create missing schools in CUS';

    protected $client;
    protected $oldDb;
    protected $adminUser;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->client = new EntityService();


//        $this->client = new CommonUserService($this->adminUser);
        $this->oldDb = DB::connection('oldDB');

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->adminUser = User::where('email', 'viktor.lalev@donatix.net')->latest()->first();
        $this->client = new CommonUserService($this->adminUser);
//        if ($this->option('migrate')) {
//            $this->info('Change DB Structure');
//            $this->migrate();
//        }
        if ($this->option('mapEHP')) {
            $this->info('Map Excel with EHP');
            $this->mapEHP();
        }
        if ($this->option('mapCUS')) {
            $this->info('Map Excel with CUS');
            $this->mapCUS();
        }
        if ($this->option('mapUuid')) {
            $this->info('Map Excel with uuid');
            $this->mapUuid();
        }

        if ($this->option('mapDistricts')) {
            $this->info('Map Excel with Current Schools District');
            $this->mapDistricts();
        }
        if ($this->option('mapTimezoneNames')) {
            $this->info('Map Timezone Names With EHP1');
            $this->mapTimezoneNames();
        }
        if ($this->option('mapTimezoneCUS')) {
            $this->info('Find Timezones in CUS');
            $this->mapTimezoneCUS();
        }
        if ($this->option('createDistricts')) {
            $this->info('Create Missing Districts');
            $this->createDistricts();
        }
        if ($this->option('createSftp')) {
            $this->info('Create Missing SFTP');
            $this->createSftp();
        }

        if ($this->option('createSchools')) {
            $this->warn('MAKE SURE THE ADMIN TOKEN IS FRESH');

            $this->info('Create Missing Schools in CUS');
            $this->createSchools();
        }

        if ($this->option('createSchool')) {
            $this->warn('MAKE SURE THE ADMIN TOKEN IS FRESH');

            $excellSchool = SchoolsEhp::where('id', $this->option('createSchool'))->first();
            if ($excellSchool) {
                $this->info('Create Single School in CUS');
                $this->createSchool($excellSchool);
            }

        }
        if ($this->option('mapUsers')) {
            $this->info('Create Memberships Only from CUS');
            $this->mapUsers();
        }
        if ($this->option('updateUsers')) {
            $this->info('Update wrong memberships');
            $this->updateUsers();
        }


//        if ($this->option('updateSchool')) {
//            $this->warn('MAKE SURE THE ADMIN TOKEN IS FRESH');
//
//            $excellSchool = SchoolsEhp::where('id', $this->option('updateSchool'))->first();
//            if ($excellSchool) {
//                $this->info('Update Single School in CUS');
//
//                $this->updateSchool($excellSchool);
//            }
//
//        }

        if ($this->option('createUsers')) {
            $this->warn('MAKE SURE THE ADMIN TOKEN IS FRESH');

            $this->info('Create Users in CUS');
            $this->createUsers();
        }
        if ($this->option('updateSftpSchools')) {
            $this->warn('MAKE SURE THE ADMIN TOKEN IS FRESH');

            $this->info('Update sftp');
            $this->updateSftpSchools();
        }
        if ($this->option('fixSchools')) {
            $this->warn('MAKE SURE THE ADMIN TOKEN IS FRESH');

            $this->info('Fix duplicated school');
            $this->fixSchools();
        }
        if ($this->option('usersAnalize')) {
            $this->warn('MAKE SURE THE ADMIN TOKEN IS FRESH');

            $this->info('Check random users creation');
            $this->usersAnalize($this->option('usersAnalize'));
        }
        if ($this->option('createMissingMemberships')) {
            $this->warn('MAKE SURE THE ADMIN TOKEN IS FRESH');

            $this->info('Create missing memberships');
            $this->createMissingMemberships();
        }
        if ($this->option('updateDomains')) {
            $this->warn('MAKE SURE THE ADMIN TOKEN IS FRESH');

            $this->info('Update school domains');
            $this->updateDomains();
        }
        if ($this->option('retryMemberships')) {
            $this->warn('MAKE SURE THE ADMIN TOKEN IS FRESH');

            $this->info('Retry user school creation');
            $this->retryMemberships($this->option('retryMemberships'));
        }
        if ($this->option('createNewMemberships')) {
            $this->info('Create memberships in v1');
            $this->createNewMemberships();
        }


    }

    protected function migrate()
    {
        //ALTER TABLE employees RENAME COLUMN id TO employ_id;
        DB::statement("ALTER TABLE school_ehp ADD id bigint unsigned PRIMARY KEY AUTO_INCREMENT");
        DB::statement("ALTER TABLE school_ehp RENAME COLUMN `School Name in Superadmin` TO `name`");
        DB::statement("ALTER TABLE school_ehp RENAME COLUMN `State` TO `state`");
        DB::statement("ALTER TABLE school_ehp RENAME COLUMN `Zip Code` TO `zip`");
        DB::statement("ALTER TABLE school_ehp RENAME COLUMN `School District Name` TO `district`");
        DB::statement("ALTER TABLE school_ehp RENAME COLUMN `Status` TO `status`");
        DB::statement("ALTER TABLE school_ehp RENAME COLUMN `Creation Date` TO `created_at`");
        DB::statement("ALTER TABLE school_ehp ADD school_id bigint unsigned");
        DB::statement("ALTER TABLE school_ehp ADD updated_at timestamp");
        DB::statement("ALTER TABLE school_ehp ADD school_uuid varchar(50)");
        DB::statement("ALTER TABLE school_ehp ADD district_uuid varchar(50)");
    }

    protected function mapEHP()
    {

        $excellSchools = SchoolsEhp::whereNull('school_id')->whereNull('school_uuid');

        $this->output->progressStart($excellSchools->count());
        foreach ($excellSchools->get() as $excellSchool) {

            $school = School::where('name', $excellSchool->name)->first();
            if ($school) {
                $excellSchool->update([
                    'school_id' => $school->id,
                    'school_uuid' => $school->cus_uuid,
                    'district_uuid' => $school->district_uuid,
                    'status' => 1,
                    'updated_at' => now()
                ]);
            }


            $this->output->progressAdvance();
        }

//        $schools = School::get();
//
//
//        $this->output->progressStart($schools->count());
//        foreach ($schools as $school) {
//            $excellSchools = SchoolsEhp::where('name', $school->name)
//                ->whereNull('school_uuid')
//                ->where(function ($q) use ($school) {
//                    $q->where('state', $school->state);
////                        ->orWhere('state', $school->description);
//                });
//
//
//            if ($excellSchools->count() > 1) {
//                logger("Unmapped School: {$school->id}", [$excellSchools->get()->pluck('id')]);
//            }
//            if ($excellSchools->count() == 1) {
//                $excellSchools->update([
//                    'school_id' => $school->id,
//                    'school_uuid' => $school->cus_uuid,
//                    'district_uuid' => $school->district_uuid,
//                    'status' => 1,
//                ]);
//            }
//            $this->output->progressAdvance();
//        }

    }

    protected function mapCUS()
    {


        $schools = School::whereNull('cus_uuid')->whereNotNull('state')->whereIn('id', [2027, 2112, 2123, 2124, 2129, 2130]);

        $this->output->progressStart($schools->count());

        foreach ($schools->get() as $school) {

            $commonSchools = CommonSchool::where(function ($q) use ($school) {
                $q->where('full_name', $school->name)->orWhere('r_name', $school->name);
            })->where('l10n_us_territorial_subdivision_code_ansi', $school->state)
                ->where('r_status', $school->status);

            if ($commonSchools->count() > 1) {
                foreach ($commonSchools->get() as $commonSchool) {
                    $domains = CommonSchoolDomain::where('common_bm_school_id', $commonSchool->id)
                        ->where('r_status', 1)->get()->pluck('email_domain')->toArray();

                    $this->info(json_encode([$school->id, $school->name, $commonSchool->uuid, $domains]));

                }
                //dd(2);
                //logger("Multiple cus: {$school->id}", [$commonSchools->get()->pluck('id')]);
            }
            if ($commonSchools->count() == 1) {

                $commonSchool = $commonSchools->first();

                $domains = CommonSchoolDomain::where('common_bm_school_id', $commonSchool->id)
                    ->where('r_status', 1)->get()->pluck('email_domain')->toArray();

                $school->update([
                    'cus_uuid' => $commonSchool->uuid,
                ]);

//                    if (in_array($school->student_domain, $domains) && in_array($school->school_domain, $domains)) {
//                        $school->update([
//                            'cus_uuid' => $commonSchool->uuid,
//                        ]);
//                    } else {
//                        logger("Wrong Domains cus: {$school->id}", [$commonSchools->get()->pluck('id')]);
//                    }

            }
            if ($commonSchools->count() == 0) {
                $school->update([
                    'cus_uuid' => -1,
                ]);
            }
            $this->output->progressAdvance();


        }
//        $schoolsForMap = SchoolsEhp::whereNull('school_uuid')->get();
//        $this->output->progressStart($schoolsForMap->count());
//        foreach ($schoolsForMap as $schoolForMap) {
//            $existingSchool = CommonSchool::where('r_name', $schoolForMap->name)
//                ->where('l10n_us_territorial_subdivision_code_ansi', $schoolForMap->state)->first();
//            if ($existingSchool) {
//                $schoolForMap->update([
//                    'school_uuid' => $existingSchool->uuid,
//                    'status' => 2 //Mapped with Existing CUS
//                ]);
//            } else {
//                $schoolForMap->update([
//                    'status' => -2, //Not mapped
//                ]);
//            }
//            $this->output->progressAdvance();
//        }

    }

    protected function mapUuid()
    {
        $schoolsForMap = SchoolsEhp::whereNull('school_id')->get();
        $this->output->progressStart($schoolsForMap->count());
        foreach ($schoolsForMap as $schoolForMap) {
            $existingSchool = School::where('cus_uuid', $schoolForMap->school_uuid)->first();
            if ($existingSchool) {
                $schoolForMap->update([
                    'school_id' => $existingSchool->id,
                    'district_uuid' => $existingSchool->district_uuid,
                    'status' => 1 //Map with existing EHP school
                ]);
            } else {
                $schoolForMap->update([
                    'status' => -1, //Not mapped
                ]);
            }
            $this->output->progressAdvance();
        }

    }

    //Update From Excell
    protected function mapDistricts()
    {
        $excellSchools = SchoolsEhp::whereNull('district_uuid')->get();

        $this->output->progressStart($excellSchools->count());
        foreach ($excellSchools as $excellSchool) {
            $existingDistrict = CommonDistrict::where('r_name', $excellSchool->district)->first();
            if ($existingDistrict) {
                $excellSchool->update([
                    'district_uuid' => $existingDistrict->uuid,
                    'status' => 3 //mapped district with CUS
                ]);

            } else {
                $excellSchool->update([
                    'status' => -3 //not mapped district
                ]);
            }
            $this->output->progressAdvance();
        }

    }

    protected function mapTimezoneNames()
    {
        $excellSchools = SchoolsEhp::whereNull('timezone')->whereNotNull('school_id')->get();
        $deprecatedTimezones = array_flip(config('timezones.deprecated'));

        $this->output->progressStart($excellSchools->count());
        foreach ($excellSchools as $excellSchool) {
            $existingSchool = School::where('id', $excellSchool->school_id)->first();
            $oldSchool = $this->oldDb->table('school_schedule')
                ->where('school_id', $existingSchool->old_id)
                ->first();

            if ($oldSchool) {
                $tz = array_key_exists($oldSchool->school_timezone, $deprecatedTimezones) ? $deprecatedTimezones[$oldSchool->school_timezone] : $oldSchool->school_timezone;

                $excellSchool->update([
                    'timezone' => $tz,
                    'status' => 4 //Set Timezone from EHPv1
                ]);

            } else {
                $excellSchool->update([
                    'status' => -4 //timezone not set
                ]);
            }


            $this->output->progressAdvance();
        }

    }

    protected function mapTimezoneCUS()
    {
        $excellSchools = SchoolsEhp::whereNull('timezone_uuid')->get();

        $this->output->progressStart($excellSchools->count());
        foreach ($excellSchools as $excellSchool) {

            $timezone = CommonTimezone::where('r_name', $excellSchool->timezone)->first();
            if ($timezone) {
                $excellSchool->update([
                    'timezone_uuid' => $timezone->uuid,
                    'status' => 5 //Map Timezone
                ]);
            } else {
                $excellSchool->update([
                    'status' => -5 //timezone not set
                ]);
            }
            $this->output->progressAdvance();
        }

    }

    protected function createDistricts()
    {
        $excellSchools = SchoolsEhp::whereNull('district_uuid')->get();

        $this->output->progressStart($excellSchools->count());


        foreach ($excellSchools as $excellSchool) {

            $district = $this->client->createDistrict($excellSchool->district, $excellSchool->state);
            if ($district) {
                $excellSchool->update([
                    'district_uuid' => $district['uuid'],
                    'status' => 6 //Created District
                ]);
            } else {
                $existingDistrict = $this->client->listDistricts($excellSchool->district);

                if (count($existingDistrict) > 0) {
                    $excellSchool->update([
                        'district_uuid' => $existingDistrict[0]['uuid'],
                        'status' => 6 //Created District
                    ]);
                } else {
                    $excellSchool->update([
                        'status' => -6 //Not created District
                    ]);
                }

            }
            $this->output->progressAdvance();
        }
    }

    protected function createSftp()
    {
        $excellSchools = SchoolsEhp::whereNull('school_uuid')->whereNull('sftp');

        $this->output->progressStart($excellSchools->count());


        foreach ($excellSchools->get() as $excellSchool) {

            $school = School::where('id', $excellSchool->school_id)->first();

            if ($school) {

                $sftp = $this->getSftpDirectory($school->name, null);

                $existingSftp = CommonSchool::where('sftp_directory', $sftp)->exists();

                if ($existingSftp) {
                    dd();
                }

                $excellSchool->update([
                    'sftp' => $sftp
                ]);

            }

            $this->output->progressAdvance();
        }
    }

    protected function createSchools()
    {
        $excellSchools = SchoolsEhp::whereNull('school_uuid')->whereNotNull('school_id');
        $this->output->progressStart($excellSchools->count());


        foreach ($excellSchools->get() as $excellSchool) {

            $this->createSchool($excellSchool);


            $this->output->progressAdvance();
        }

    }

    protected function createSchool($excellSchool)
    {
        $projectManagerUuid = "7990a773-1064-4ed3-9571-cb3a6d1056ca";
        $ehpClient = "ba63e471-0b6a-4437-afab-ff38bc36c092";

        $school = School::where('id', $excellSchool->school_id)->first();

        if ($school) {
            $body = [];
            $oldSchool = $this->oldDb->table('school_schedule')
                ->where('school_id', $school->old_id)
                ->first();

            if ($oldSchool) {

                $domains = [$school->school_domain];
                if ($school->school_domain != $school->student_domain) {
                    $domains[] = $school->student_domain;
                }

                $extArray = explode('.', $oldSchool->school_logo);

                $logo = null;
                if (count($extArray) > 1 && Storage::disk('s3v1')->exists('public/schoolimage/' . $oldSchool->school_logo)) {
                    $logo = "data:image/{$extArray[1]};base64," . base64_encode(Storage::disk('s3v1')->get('public/schoolimage/' . $oldSchool->school_logo));
                }

                $body = [
                    "clientOptionCollection" => [
                        [
                            "client" => $ehpClient,
                            "projectManager" => $projectManagerUuid,
                            "relationshipManager" => $projectManagerUuid,
                        ]
                    ],
                    "authProviderIdCollection" => [0],
                    "appointmentPass" => $school->hasModule(Module::APPOINTMENTPASS),
                    "autoCheckIn" => $school->hasModule(Module::AUTO_CHECKIN),
                    "kiosk" => $school->hasModule(Module::KIOSK),
                    "buildingCollection" => null,
                    "description" => $school->description,
                    "district" => $excellSchool->district,
                    "emailDomainCollection" => $domains,
                    "fullName" => $school->name,
                    "logo" => $logo,
                    "name" => $school->name,
                    "tier" => $oldSchool->total_users,
                    "timeZone" => $excellSchool->timezone_uuid,
                    "l10nUsTerritorialSubdivisionCodeAnsi" => trim($excellSchool->state),
                    "zipCode" => $excellSchool->zip,
                    "sftpSendCredentials" => "",
                    "sftpFilePrefix" => $oldSchool->archive_prefix,
                    "userProviderCollection" => $this->getUserProviderCollection($school),
                    "status" => $school->status,
                    "providingStudentPhotos" => 0,
                    "sftpDirectory" => $excellSchool->sftp,
                    "sftpCredentialsEmail" => "jenniferskudlarek@eduspiresolutions.org",
                    "sftpSendCredentials" => 1

                ];
//                unset($body['logo']);
//                dd($body);
                $result = $this->client->createSchool($excellSchool->district_uuid, $body);

                if ($result) {
                    $school->update([
                        'cus_uuid' => $result['uuid'],
                        'district_uuid' => $excellSchool->district_uuid
                    ]);
                    $excellSchool->update([
                        'status' => 7, //Created
                        'school_uuid' => $result['uuid'],
                    ]);
                } else {
                    $excellSchool->update([
                        'status' => -7, //Service Error
                    ]);
                }
                \App\Models\V1\School::where('school_id', $oldSchool->school_id)
                    ->first()->migrateModules();

            }

        }


//        $oldUsers = \App\Models\V1\User::where('school_id', $oldSchool->school_id)
//            ->where('status', 1);
//        $this->output->progressStart($oldUsers->count());
//        foreach ($oldUsers->get() as $oldUser) {
//            $oldUser->migrateToV2andCus(true);
//            $this->output->progressAdvance();
//        }
//        $this->info($oldSchool->school_id);
    }

    protected function getSftpDirectory($name, $oldSchool)
    {
        $dirName = substr(strtolower(preg_replace(
            ['/[^A-Za-z0-9]/', '/highschool/i', '/middleschool/i'],
            ['', 'hs', 'ms'],
            $name)),
            0, 32
        );

        if (CommonSchool::where('sftp_directory', $dirName)->exists()) {
            $dirName = $dirName . 'sftp';
        }

        return $dirName;

    }

    protected function updateSchools()
    {
        $excellSchools = SchoolsEhp::whereNotNull('school_uuid');
        $this->output->progressStart($excellSchools->count());


        foreach ($excellSchools->get() as $excellSchool) {

            $this->updateSchool($excellSchool);


            $this->output->progressAdvance();
        }

    }

    protected function updateSchool($school)
    {

        $body = [];

        $domains = [$school->school_domain];
        if ($school->school_domain != $school->student_domain) {
            $domains[] = $school->student_domain;
        }

//        $extArray = explode('.', $oldSchool->school_logo);
//
//        $logo = null;
//        if (count($extArray) > 1) {
//            $logo = "data:image/{$extArray[1]};base64," . base64_encode(Storage::disk('s3v1')->get('public/schoolimage/' . $oldSchool->school_logo));
//        }


        $body = [
//                    "clientOptionCollection" => [
//                        [
//                            "client" => $ehpClient,
//                            "projectManager" => $projectManagerUuid,
//                            "relationshipManager" => $projectManagerUuid,
//                        ]
//                    ],
//                    "authProviderIdCollection" => $this->getAtuhProviders($school),
//                    "appointmentPass" => $school->hasModule(Module::APPOINTMENTPASS),
//                    "autoCheckIn" => $school->hasModule(Module::AUTO_CHECKIN),
//                    "kiosk" => $school->hasModule(Module::KIOSK),
////                    "buildingCollection" => null,
//                    "description" => $school->description,
//                    "district" => $excellSchool->district,
            "emailDomainCollection" => $domains,
//                    "fullName" => $school->name,
//                    "logo" => $logo,
//                    "name" => $school->name,
//                    "tier" => $oldSchool->total_users,
//                    "timeZone" => $excellSchool->timezone_uuid,
//                    "l10nUsTerritorialSubdivisionCodeAnsi" => $excellSchool->state,
//                    "zipCode" => $excellSchool->zip,
//                    "sftpCredentialsEmail" => "",
//                    "sftpSendCredentials" => "",
//                    "sftpFilePrefix" => $oldSchool->archive_prefix,
//                    "userProviderCollection" => $this->getUserProviderCollection($school),
//                    "status" => $school->status,
//                    "providingStudentPhotos" => 0,
//            "sftpDirectory" => $this->getSftpDirectory($school->name, $school),

        ];
//                unset($body['logo']);
//                dd($school->cus_uuid,$body);

        $result = $this->client->updateSchool($school->cus_uuid, $body);

//        if ($result) {
//            $school->update([
//                'cus_uuid' => $result['uuid'],
//                'district_uuid' => $excellSchool->district_uuid
//            ]);
//            $excellSchool->update([
//                'status' => 8, //Updated
//                'school_uuid' => $result['uuid'],
//            ]);
//        } else {
//            $excellSchool->update([
//                'status' => -8, //Service Error
//            ]);
//
//        }
    }

    protected function updateSftpSchools()
    {
        $commonSchools = CommonSchool::select('uuid')->where('created_by', 38)
            ->where('sftp_directory', '');


        $this->output->progressStart($commonSchools->count());
        foreach ($commonSchools->get() as $commonSchool) {
            $excellSchool = SchoolsEhp::where('school_uuid', $commonSchool->uuid)->first();

            if ($excellSchool) {
                $this->updateSchool($excellSchool);
            }
            $this->output->progressAdvance();
//            dd(2);
        }

    }

    protected function createUsers()
    {
        $excellSchools = SchoolsEhp::whereNotNull('school_uuid')
            ->where('id', '>',709)
            ->get()->pluck('school_id');
//        $commonSchools = CommonSchool::select('uuid','l10n_us_territorial_subdivision_code_ansi')
//            ->where('created_by', 38);

        $dbQuery = Db::table('users as u')
            ->join('schools as s', 'u.school_id', '=', 's.id')
            ->selectRaw('u.id,s.id as school_id,s.name',
            )
            ->whereNull('u.cus_uuid')
            ->whereNotNull('u.email')
            ->whereIntegerInRaw('s.id', $excellSchools)
            ->orderBy('u.id', 'asc');;

        $this->output->progressStart($dbQuery->count());


        $dbQuery->chunk(100, function ($existingUsers) {
            foreach ($existingUsers as $existingUser) {

                $user = User::where('id', $existingUser->id)->first();

                $school = School::where('id', $existingUser->school_id)->first();
                if ($user) {
                    $schoolDomains = [$school->school_domain, $school->student_domain];
                    $email = $user->email;
                    $domain = explode('@', $email)[1];
                    if (in_array($domain, $schoolDomains)) {

//                        $commonSchool = CommonSchool::select('id', 'uuid', 'l10n_us_territorial_subdivision_code_ansi')
//                            ->where('created_by', 38)
//                            ->where('uuid', $school->cus_uuid)
//                            ->where('l10n_us_territorial_subdivision_code_ansi', $school->state)
//                            ->first();

//                        if ($commonSchool) {
                        DB::table('schools_users')->updateOrInsert(
                            [
                                'school_id' => $school->id,
                                'user_id' => $user->id
                            ],
                            [
                                'role_id' => $user->role_id
                            ]
                        );

                        if (!CommonUser::where('email', $user->email)->exists()) {
                            CreateCommonUserJob::dispatchSync([
                                'user' => $user,
                                'authenticatedUser' => $this->adminUser,
                                'school' => $school
                            ]);
                        }
//                        }
                    }
                }
                $this->output->progressAdvance();
            }

        });

//        foreach ($excellSchools->get() as $excellSchool) {
//
//            $this->info($excellSchool->name);
//
//            $school = School::where('id', $excellSchool->school_id)->first();
//            if ($school) {
//
//                $dbQuery = Db::table('users as u')
//                    ->select('id')
//                    ->where('u.school_id', $excellSchool->school_id)
//                    ->whereNull('u.cus_uuid')
//                    ->orderBy('u.id', 'asc');
//                $this->output->progressStart($dbQuery->count());
//
//                $dbQuery->chunk(10, function ($userIds) use ($school) {
//                    foreach ($userIds as $userId) {
//
//                        $user = User::where('id', $userId->id)->first();
//
//                        if ($user) {
//                            DB::table('schools_users')->updateOrInsert(
//                                [
//                                    'school_id' => $school->id,
//                                    'user_id' => $user->id
//                                ],
//                                [
//                                    'role_id' => $user->role_id
//                                ]
//                            );
//                            CreateCommonUserJob::dispatch([
//                                'user' => $user,
//                                'authenticatedUser' => $this->adminUser,
//                                'school' => $school
//                            ]);
//                        }
//
//
//                        $this->output->progressAdvance();
//
//                    }
//                });
//            }
//            $excellSchool->update([
//                'status' => 6
//            ]);
//            $this->output->progressFinish();
//        }
    }

    protected function getAtuhProviders($school)
    {

        $result = [0];

        $clever = $this->getCleverCredentials($school);
        if ($clever) {
            $result[] = AuthType::CLEVER;
        }

        $classLink = $this->getClassLinkCredentials($school);

        if ($classLink) {
            $result[] = AuthType::CLASSLINK;
        }
        return $result;
    }

    protected function getUserProviderCollection($school)
    {

        $result = [];

        $clever = $this->getCleverCredentials($school);
        if ($clever) {
            $result['clever'] = $clever;
        }

        $classLink = $this->getClassLinkCredentials($school);

        if ($classLink) {
            $result['classLink'] = $classLink;
        }
        return $result;
    }


    protected function getCleverCredentials($school)
    {
        $cleverInfo = $this->oldDb->table('schools_clever')
            ->where('ehp_school_id', $school->old_id)
            ->get();

        $result = [];
        foreach ($cleverInfo as $singleClever) {
            $sisField = json_decode($singleClever->sis_field);
            $result[] = [
                'districtId' => $singleClever->district_id,
                'primaryId' => $sisField->clever_stu_sis_field ?? 'sis_id',
                'cleverId' => $singleClever->clever_id,
            ];
        }
        return $result;
    }

    protected function getClassLinkCredentials($school)
    {
        return $this->oldDb->table('schools_classlink')
            ->selectRaw("tenant_id as tenant, source_id as sourcedId")
            ->where('ehp_school_id', $school->old_id)
            ->get()
            ->toArray();
    }

    protected function mapUsers($skip = 0)
    {

        $dbQuery = Db::connection('commonDb')
            ->table('common_bm_membership as m')
            ->join('auth_user as u', 'm.auth_user_id', '=', 'u.id')
            ->join('common_bm_school as s', 'm.common_bm_school_id', '=', 's.id')
            ->leftJoin('auth_title as t', 'u.auth_title_id', '=', 't.id')
            ->selectRaw('u.uuid as user_uuid,
            u.email as email,
            m.sis_id as student_sis_id,
            u.username as username,
            u.first_name as first_name,
            u.last_name as last_name,
            m.common_bm_role_membership_id as role_id,
            u.image as avatar,
            m.graduation_year as grade_year,
            u.r_status as user_status,
            m.r_status as membership_status,
            u.last_login_time as last_login_time,
            u.created as created_at,
            u.modified as updated_at,
            s.uuid as school_uuid,
            m.uuid as membership_uuid,
            t.r_name as title,
            u.id as common_user_id,
            m.archived_at as deleted_at
            ',
            )
            ->where('m.created_by', 38)
            ->where('m.auth_client_id', 2)
//            ->whereIn('s.uuid', $uuids)
            ->orderBy('m.id', 'asc');

        $this->output->progressStart($dbQuery->count() - $skip ?? 0);
        $dbQuery
            ->when($skip, function ($query) use ($skip) {
                $query->skip($skip);
            })->chunk(100, function ($fullRecords) {
                foreach ($fullRecords as $fullRecord) {

                    $school = School::where('cus_uuid', $fullRecord->school_uuid)->first();

                    if ($school) {
                        $data = UserTranslator::translateJoinedRecord($fullRecord, $school);
//                        $user = User::withoutGlobalScopes()->where('cus_uuid', $data['cus_uuid'])->first();
//                        if (!$user) {
                        $user = User::withoutGlobalScopes()->where('email', $data['email'])->first();
//                            unset($data['email']);
//                        }
//                        User::enableMassIndex();
//                        if ($user) {
////                            if ($user->isArchivePrevented($school)) {
////                                unset($data['deleted_at']);
////                            }
//                            $user->update($data);
//                        } else {
//                            $data['email'] = $fullRecord->email;
//                            $user = User::create($data);
//                        }
                        if ($user) {
                            DB::table('schools_users')->updateOrInsert(
                                [
                                    'school_id' => $school->id,
                                    'user_id' => $user->id,
                                ], [
                                'membership_uuid' => $fullRecord->membership_uuid,
                                'role_id' => CommonMembership::getRoleId($fullRecord->role_id),
                            ]);
                        }


//                        if ($user->status === User::INACTIVE) {
//                            CancelAndUpdateAppointmentsAfterArchiveJob::dispatch($user)->onQueue('appointments');
//                        }

//                        $user->searchable();
//                        User::disableMassIndex();

                    }
                    $this->output->progressAdvance();
                }

            });
        return true;


    }

    protected function updateUsers()
    {
        $uuids = ["fcae41a1-5a21-4674-9deb-2c4aef9dc90f", "b30bd955-33ac-418b-9296-11b77549dccc", "0304b049-fc54-44c0-bdda-5099963629c4", "0ece6ef3-41e7-44c7-864e-379d2473b493", "0ee594c3-497d-4017-89a2-4d8d408b604d", "f24482cd-4646-4c59-9dbf-54ddf941b4a9", "d27dc6f5-de57-4a9e-8a90-7f2d69cc7823", "973bd7b3-48c7-4866-b6d6-fd41a5591cf2", "5228733b-80e7-4999-a60a-b394ca8e97f4", "c0ea49dc-f00f-4c19-8837-25756f30af0d", "a26a1f55-4fc6-4212-a86f-1d662ad8dc05", "71ad2dbc-b42a-46e0-a591-baf5aa214e7d", "ea91fedf-08ea-41d5-9275-6649213246ec", "719178e9-87a5-4da6-9506-3e813cfc9fa3", "1e39eb93-a536-488b-9e3e-cbf64876206c", "55c8f89e-ba5b-4209-af06-e853aa1126ee", "10cd246b-b8de-4c2f-a123-c1d4da08de79", "bf0f1014-ac37-4967-82a8-c1743188134f", "e540ba9d-398d-46f1-969a-8c376e3983ad", "283e5853-2578-495e-a0b0-7b0b8b60da59", "e6308d28-546d-461a-b04b-b92418e61fac", "3ccab0a2-d06c-429d-a768-16e6e528e685"];
        $correctSchools = School::whereIn('cus_uuid', $uuids)->get();

        foreach ($correctSchools as $correctSchool) {
            $this->info($correctSchool->id);
            $correctUsers = User::where('school_id', $correctSchool->id)->get();
            $this->output->progressStart($correctUsers->count());
            foreach ($correctUsers as $correctUser) {

                $wrongMembership = SchoolUser::where('user_id', $correctUser)
                    ->where('school_id', '<>', $correctSchool->id)
                    ->first();

                if ($wrongMembership) {
                    dd($wrongMembership);
                }

                $this->output->progressAdvance();
            }

        }
//        dd($correctSchools);

//        dd($correctSchools->count());
    }

    protected function fixSchools()
    {
        foreach ([418, 535, 560, 561, 783, 879, 940, 1021, 1065, 1100, 1328, 1527, 1551, 1569, 1570, 1571, 1573, 1576, 1579, 1589, 1604, 1609, 1626, 1643, 1644, 1659] as $id) {
            $schoolForFix = DB::table('common_bm_school_diffs')
                ->where('id', $id)
                ->where('modified_by', 38)
                ->first();

            $correctSchool = DB::table('common_bm_school_diffs')
                ->where('id', $id)
                ->where(function ($q) {
                    $q->where('modified_by', '<>', 38)->orWhereNull('modified_by');
                })
                ->whereNotNull('appointment_pass')
                ->first();
            if ($correctSchool) {


//            $wrongTimezone = CommonTimezone::where('id',$schoolForFix->l10n_datetime_time_zone_id)->first()->uuid;
//
//            $wrongBody = [
//
////            "authProviderIdCollection" => $this->getAtuhProviders($school),
//                "appointmentPass" => $schoolForFix->appointment_pass,
//                "autoCheckIn" => $schoolForFix->auto_check_in,
//                "kiosk" => $schoolForFix->kiosk,
////                    "buildingCollection" => null,
//                "description" => $schoolForFix->description,
////            "district" => $excellSchool->district,
////            "emailDomainCollection" => $domains,
////            "fullName" => $school->name,
//                "logo" => $schoolForFix->logo,
//                "tier" => $schoolForFix->tier,
//                "timeZone" => $wrongTimezone,
//                "l10nUsTerritorialSubdivisionCodeAnsi" => $schoolForFix->l10n_us_territorial_subdivision_code_ansi,
//                "zipCode" => $schoolForFix->zip_code,
//                "sftpCredentialsEmail" => $schoolForFix->sftp_credentials_email,
//                "sftpFilePrefix" => $schoolForFix->sftp_file_prefix,
////            "userProviderCollection" => $this->getUserProviderCollection($school),
//                "status" => $schoolForFix->r_status,
//                "sftpDirectory" => $schoolForFix->sftp_directory,
//
//            ];

                $correctTimezone = CommonTimezone::where('id', $correctSchool->l10n_datetime_time_zone_id)->first()->uuid;
                $correctBody = [

//            "authProviderIdCollection" => $this->getAtuhProviders($school),
                    "appointmentPass" => boolval($correctSchool->appointment_pass),
                    "autoCheckIn" => boolval($correctSchool->auto_check_in),
                    "kiosk" => boolval($correctSchool->kiosk),
//                    "buildingCollection" => null,
                    "description" => $correctSchool->description,
//            "district" => $excellSchool->district,
//            "emailDomainCollection" => $domains,
//            "fullName" => $school->name,
                    "logo" => $correctSchool->logo,
                    "tier" => $correctSchool->tier,
                    "timeZone" => $correctTimezone,
                    "l10nUsTerritorialSubdivisionCodeAnsi" => $correctSchool->l10n_us_territorial_subdivision_code_ansi,
                    "zipCode" => $correctSchool->zip_code,
                    "sftpCredentialsEmail" => $correctSchool->sftp_credentials_email,
                    "sftpFilePrefix" => $correctSchool->sftp_file_prefix,
//            "userProviderCollection" => $this->getUserProviderCollection($school),
                    "status" => $correctSchool->r_status,
                    "sftpDirectory" => $correctSchool->sftp_directory,

                ];
//        unset($wrongBody['logo']);
//                unset($correctBody['logo']);
//                dd($correctBody);

                $result = $this->client->updateSchool($correctSchool->uuid, $correctBody);
            }
//            if ($result) {
//                dd($result);
//            } else {
//                dd('issue');
//            }
        }


    }

    protected function usersAnalize($schoolId = "all")
    {
        $schools = DB::table('v2_schools_with_notcreated_correct_users')->get();

        if ($schoolId != 'all') {
            $schools = DB::table('schools')->where('id', $schoolId)->get();
        }

        $client = new CommonUserService($this->adminUser);

        $this->info('Total Schools: ' . $schools->count());

        foreach ($schools as $school) {
            $randomUsers = User::where('school_id', $school->id)->whereNull('cus_uuid')
//                ->inRandomOrder()
//                ->limit(5)
                ->get();
            $issue = [];
            $schoolModel = School::where('id', $school->id)->first();
            $this->info($schoolModel->name);
            $this->output->progressStart($randomUsers->count());
            foreach ($randomUsers as $randomUser) {

                $commonUser = CommonUser::where('email', $randomUser->email)->first();

                if ($commonUser) {
                    $commonSchool = CommonSchool::select('id')->where('uuid', $school->cus_uuid)->first();

                    if ($commonSchool) {
                        $membership = CommonMembership::where('auth_user_id', $commonUser->id)
                            ->where('common_bm_school_id', $commonSchool->id)
                            ->where('auth_client_id', 2)
                            ->first();
                        if ($membership) {
                            $randomUser->update([
                                'cus_uuid' => $commonUser->uuid,
                                'initial_password' => null
                            ]);
                            DB::table('schools_users')->updateOrInsert(
                                [
                                    'school_id' => $schoolModel->id,
                                    'user_id' => $randomUser->id,
                                ],
                                [
                                    'membership_uuid' => $membership['uuid'],
                                    'role_id' => $randomUser->role_id
                                ]);

                            $issue[] = [
                                "userID" => $randomUser->id,
                                "code" => 201,
                                "message" => $membership['uuid'],
                            ];
                        }
                    }
                } else {
                    $roleCollection = CommonUser::getRoleName($randomUser->role_id);

                    $userData = [
                        'email' => $randomUser->email,
                        'firstName' => $randomUser->first_name,
                        'lastName' => $randomUser->last_name,
                        'status' => $randomUser->status,
                        'password' => null,
                    ];
                    if ($randomUser->user_initials) {
                        $userData['title'] = $randomUser->user_initials;
                    }

                    $gradeYear = $randomUser->grade_year;
                    if (!$gradeYear && $randomUser->isStudent()) {
                        $gradeYear = 0;
                    }


                    $membershipData = [
                        "status" => $randomUser->status,
                        "school" => $schoolModel->cus_uuid,
                        'authClientId' => 2,
                        "user" => $userData,
                        "role" => $roleCollection[0],
                        "graduationYear" => $gradeYear,
                        'sisId' => $randomUser->student_sis_id,
                        "sendConfirmationEmail" => false,
                        "preventFromArchiving" => true
                    ];
                    $membership = $client->assignUserToSchool($membershipData);

                    //logger('MEMBERSHIP', [$membership]);
                    if (!isset($membership['status'])) {

                        $userCommonId = CommonMembership::where('id', $membership['id'])->first()->auth_user_id;

                        $randomUser->update([
                            'cus_uuid' => CommonUser::find($userCommonId)->uuid,
                            'initial_password' => null
                        ]);
                        DB::table('schools_users')->updateOrInsert(
                            [
                                'school_id' => $schoolModel->id,
                                'user_id' => $randomUser->id,
                            ],
                            [
                                'membership_uuid' => $membership['uuid'],
                                'role_id' => $randomUser->role_id
                            ]);

                        $issue[] = [
                            "userID" => $randomUser->id,
                            "code" => 201,
                            "message" => $membership['uuid'],
                        ];
                    } else {
                        $decoded = json_decode($membership['error'], 1);
                        $code = $decoded['status'];
                        $message = $decoded['debug'];
                        if (isset($decoded['debug']['previous'])) {
                            $message = $decoded['debug']['previous']['message'];
                        }
                        $issue[] = [
                            "userID" => $randomUser->id,
                            "code" => $code,
                            "message" => $message,
                        ];
                    }
                }


                $this->output->progressAdvance();

            }
            if (!$schoolId) {
                DB::table('v2_schools_with_notcreated_correct_users')
                    ->where('id', $school->id)
                    ->update(['note' => json_encode($issue)]);
                $issue = [];
            }

        }

    }

    protected function createMissingMemberships()
    {

        $dbQuery = DB::table('schools_users as m')
            ->join('schools as s', 'm.school_id', '=', 's.id')
            ->whereNotNull('s.cus_uuid')
            ->where('s.status', '>', 0)
            ->whereNull('m.membership_uuid')
            ->orderBy('m.user_id');

        $this->output->progressStart($dbQuery->count());
        $dbQuery->chunk('100', function ($memberships) {

            foreach ($memberships as $membership) {

                $user = User::where('id', $membership->user_id)->whereNotNull('email')->first();
                $school = School::where('id', $membership->school_id)->first();
                $retryExist = DB::table('memberships_retries')
                    ->where('user_id', $membership->user_id)
                    ->where('school_id', $membership->school_id)
                    ->exists();

                if ($user && $school && !$retryExist) {
                    $commonUser = CommonUser::where('email', $user->email)->first();

                    if ($commonUser) {
                        $commonSchool = CommonSchool::select('id')->where('uuid', $school->cus_uuid)->first();

                        if ($commonSchool) {
                            $commonMembership = CommonMembership::where('auth_user_id', $commonUser->id)
                                ->where('common_bm_school_id', $commonSchool->id)
                                ->where('auth_client_id', 2)
                                ->first();
                            if ($commonMembership) {

                                $user->update([
                                    'cus_uuid' => $commonUser->uuid,
                                    'initial_password' => null
                                ]);
                                DB::table('schools_users')->updateOrInsert(
                                    [
                                        'school_id' => $school->id,
                                        'user_id' => $user->id,
                                    ],
                                    [
                                        'membership_uuid' => $commonMembership['uuid'],
                                    ]);

                            } else {
                                $membership = $this->createMembership($membership, $user, $school);
                            }
                        }
                    } else {
                        $membership = $this->createMembership($membership, $user, $school);
                    }

                }


                $this->output->progressAdvance();

            }
        });


    }

    /**
     * @param $membership
     * @param $user
     * @param $school
     * @param CommonUserService $client
     * @return array|mixed
     */
    protected function createMembership($membership, $user, $school)
    {

        $client = new CommonUserService($this->adminUser);

        $roleCollection = CommonUser::getRoleName($membership->role_id);

        $userData = [
            'email' => $user->email,
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
            'status' => $user->status,
            'password' => null,
            'username' => $user->username
        ];
        if ($user->user_initials) {
            $userData['title'] = $user->user_initials;
        }

        $gradeYear = $user->grade_year;
        if (!$gradeYear && $user->isStudent()) {
            $gradeYear = 0;
        }


        $membershipData = [
            "status" => $user->status,
            "school" => $school->cus_uuid,
            'authClientId' => 2,
            "user" => $userData,
            "role" => $roleCollection[0],
            "graduationYear" => $gradeYear,
            'sisId' => $user->student_sis_id,
            "sendConfirmationEmail" => false,
            "preventFromArchiving" => true
        ];

        $commonMembership = $client->assignUserToSchool($membershipData);

        //logger('MEMBERSHIP', [$commonMembership]);
        if (!isset($commonMembership['status'])) {

            $userCommonId = CommonMembership::where('id', $commonMembership['id'])->first()->auth_user_id;

            $user->update([
                'cus_uuid' => CommonUser::find($userCommonId)->uuid,
                'initial_password' => null
            ]);
            DB::table('schools_users')->updateOrInsert(
                [
                    'school_id' => $school->id,
                    'user_id' => $user->id,
                ],
                [
                    'membership_uuid' => $commonMembership['uuid'],
                ]);


        } else {
            $decoded = json_decode($commonMembership['error'], 1);
            $code = $decoded['status'];
            $message = "";
            if (isset($decoded['debug']['previous'])) {
                $message = $decoded['debug']['previous']['message'];
            }
            if (isset($decoded['code'])) {
                $message = $decoded['code'];
            }
            DB::table('memberships_retries')->updateOrInsert(
                [
                    'user_id' => $user->id,
                    'school_id' => $school->id,
                ],
                [

                    'body' => json_encode($membershipData),
                    'code' => $code,
                    'message' => $message
                ]);
        }
        return $commonMembership;
    }

    protected function updateDomains()
    {
        $uuids = ["72c87d88-8e7d-4594-8adc-755ab5f11ec0", "124e8f58-6c57-441c-823b-5e28661d317d", "21c2fb35-aa86-4ba3-8c35-0015197bfda4", "ab6580f9-4c65-49a6-9a23-8f1505ea153d", "2977a4b4-b723-4a20-b5a8-05406e75d970", "9836e707-d659-4144-aefa-a078edcf3bbe", "c743f9e3-68ca-4d84-803e-cf91018d5ea8", "50513d68-c518-4884-870b-2b666fefe41e", "49877727-86bc-468a-94cb-03f78b32e634", "0437d891-707b-4b52-b0d8-e54cf4347dcf", "4817e67d-80ef-47fe-a116-737dc61577f9", "d73e3353-b172-4bf2-a56a-ad66e597de13", "70d20b07-2150-4e33-b1bd-3d3c3691827e", "610f49e3-59b4-409e-bcc8-168914fe5a16", "7aa97648-b8e2-4402-b0e3-cf24e7f2c8ea", "837e9347-b038-47ae-a192-84948bf4c8cd", "c7bc9c78-63eb-4dea-86e8-e3f79456b77d", "6fe7cf08-9b68-4353-9214-54809d89da59", "feb5b43e-8767-48be-a13b-80b65c49d34f", "78028d73-4482-44b0-b606-5a78bea7bc20", "9b0d8d9b-876d-446a-8beb-5a50fd71932c", "1ab21788-8a84-417f-9ae4-c1c030dd1546", "0dc68fa9-f216-430b-8d62-a0c4eea27f4c", "78f35ec2-59e4-4b6e-adfd-6fb3c377b919", "0f6cdd3f-2eee-4b2f-aa9d-9f05d6981d4f", "eedadccd-1994-4b03-993f-9fad4cca8dba", "63da70bf-1eb5-40ef-9c91-98518645c2e2", "1460fede-b79a-4b7b-863f-1e507960d1ea", "93e20c42-9559-44c3-a173-1db3b9073361", "ab34f4e9-485f-42ea-8148-4b8f5269ae34", "c203b154-e181-41a7-8db6-52a2c5fd2192", "ede29bc2-0bab-4af4-949e-da9b0eb7393f", "f590e8f3-98d8-4d34-9949-1d6063fa24e3", "1ff1b3e3-246d-453d-b934-aec421374298", "c4f2cf59-6a39-41aa-b99f-4fb7a73ddcdf", "521c40d8-aceb-46e2-8bc1-9bf853d64477", "bb8e7ef1-ab9e-4984-bdec-2ecb48b6cd6f", "c9fb35da-70f0-46c9-a086-0e11c621f991", "23b5c149-bfec-4cb4-a338-e4306d49cf1e", "bf0f1014-ac37-4967-82a8-c1743188134f", "bea92f3c-284c-4d47-99af-d03fecce9b84", "3d64e22f-b40b-497b-9b56-386f0ebd92a5", "d6fc459d-30cb-4db2-8692-c8cc4334f116", "407b0c62-b151-4a76-ab86-45575d92bdca", "fd25b62f-615f-4ed6-b4c3-a3d3a5109fe3", "e899a2b5-d839-482b-8cf1-9e6f4efef78f", "f47b0b9b-6cb0-483e-81ec-41ca0f9a7793", "01cfc02c-1318-4c19-aa62-491473d8ff60", "3ffa2235-1357-47ba-b655-ee3156fc28ba", "9b819235-a33d-4f22-b229-37e2ebea64a2", "3ae31d50-d4ec-4f9b-8d20-7d5f918d224f", "a2f962e9-3232-4b12-b034-5aec76fba679", "bf5cb24d-2c52-478e-a52c-1976e4cfa0c6", "4960fe8f-d51d-4fd4-9481-15663f959d5f", "4b31f256-cb4d-4550-9efb-81a5d5f7ec4c", "2e5da657-86e7-483b-bda5-c7e3bca3e8e0", "9f234c5a-fc44-49ba-8df9-d6c0d6455751", "e6308d28-546d-461a-b04b-b92418e61fac", "df70c16c-efb6-49c5-ad66-c14f12e3a8f8", "5e2fcd9d-1d96-4fb8-a073-79f9fd97952d", "749e4962-8d01-4cc7-9cd6-100d2f5b2732", "d53c0df8-bfd7-43d8-890b-95b1f33e55b7", "aa6fe3b8-e237-4545-90ee-05e64e925190", "e93aad46-999f-49d1-ab1b-78035bbb86ea", "c83dfcd9-6890-45e6-9fb6-0fc400574723", "df3479f4-b1b9-4c51-90f3-f2425b900a27", "ef747ebb-2b46-46fa-a2f8-2ddeacde48bb", "153faa9a-ba7b-4134-99b9-8082c0d33f83", "55b51c5c-e144-473d-83f2-7461a7b7c476", "baf4deb3-d37b-41ef-bee9-bfdadb2d86ea", "41fc5b1e-6451-4d3c-b8fa-c7c05e798d8f", "3bf2bccd-2745-473d-b3e7-834f03e46eb9", "5c814862-3777-4d07-ae29-7290b7b9ec57", "691edfed-28b9-4e4d-b99f-473af8e8fdb8", "d3ee78aa-6703-4197-bfb2-5a0732dda530", "9aba0f2a-785b-42f5-8d8b-a5cd2338e622", "03b48bad-f2fd-4549-bab5-423050b68fdb", "89e26984-35a7-4d59-89e1-20b7c93b5140", "a4a5ff02-2643-4e5d-8f7a-3058f9754411", "93f42a4c-dceb-48a9-be27-50a618fc0a51", "db2fd9de-de66-4471-b008-d2ecefc13d5f", "f1bf89af-7601-4506-9386-04297587a030", "21404bdf-1ba6-4c84-a3c7-80266520ac04", "fbe3b3c4-723c-4fdb-8ecc-535004f5bf23", "26d0dd5f-4bef-43b3-a679-c9c4f1d86bca", "07405f96-bbb3-45a1-b39d-08c96665ebe4"];
        $schools = School::whereIn('cus_uuid', $uuids);
        $this->output->progressStart($schools->count());

        foreach ($schools->get() as $school) {

            $this->updateSchool($school);
            $this->output->progressAdvance();
        }

    }

    protected function retryMemberships($schoolId)
    {
        $otherAdmin = User::where('email', 'venelin.petrov@donatix.net')->first();
        $client = new CommonUserService($otherAdmin);
//foreach($ids as $schoolId){
        $memberships = DB::table('memberships_retries')->where('school_id', $schoolId);

        $this->output->progressStart($memberships->count());

        foreach ($memberships->get() as $membership) {
            $body = json_decode($membership->body, 1);
//		unset($body['user']['title']);
            $commonMembership = $client->assignUserToSchool($body);
            //logger('MEMBERSHIP', [$commonMembership]);
            if (!isset($commonMembership['status'])) {

                $userCommonId = CommonMembership::where('id', $commonMembership['id'])->first()->auth_user_id;

                DB::table('users')->where('id', $membership->user_id)->update([
                    'cus_uuid' => CommonUser::find($userCommonId)->uuid,
                    'initial_password' => null
                ]);

                DB::table('schools_users')->updateOrInsert(
                    [
                        'school_id' => $membership->school_id,
                        'user_id' => $membership->user_id,
                    ],
                    [
                        'membership_uuid' => $commonMembership['uuid'],
                    ]);

                DB::table('memberships_retries')
                    ->where('school_id', $membership->school_id)
                    ->where('user_id', $membership->user_id)
                    ->delete();
            } else {
                $decoded = json_decode($commonMembership['error'], 1);
                $code = $decoded['status'];
                $message = "";
                if (isset($decoded['debug']['previous'])) {
                    $message = $decoded['debug']['previous']['message'];
                }
                if (isset($decoded['code'])) {
                    $message = $decoded['code'];
                }
                DB::table('memberships_retries')->updateOrInsert(
                    [
                        'school_id' => $membership->school_id,
                        'user_id' => $membership->user_id,
                    ],
                    [

                        'body' => $membership->body,
                        'code' => $code,
                        'message' => $message
                    ]);
            }
            $this->output->progressAdvance();
        }
        // }
    }

    protected function createNewMemberships()
    {
        $ids = [1, 2178, 2177, 2176, 2175, 2174, 2173, 2172, 2171, 2170, 2169, 2168, 2167, 2166, 2165, 2164, 2163, 2162, 2161, 2160, 2159, 2158, 2157, 2156, 2155, 2154, 2153];
        $dbQuery = DB::table('users')
            ->select('id', 'school_id', 'role_id')
            ->whereNull('cus_uuid')
            ->whereIn('school_id', $ids)
            ->orderBy('id');
        $this->output->progressStart($dbQuery->count());

        $dbQuery->chunk('1000', function ($users) {
            foreach ($users as $user) {

                DB::table('schools_users')->updateOrInsert(
                    [
                        'school_id' => $user->school_id,
                        'user_id' => $user->id
                    ],
                    [
                        'role_id' => $user->role_id
                    ]
                );


                $this->output->progressAdvance();

            }
        });

    }
}
