<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\School */
class SchoolResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return parent::toArray($request);
        //        return [
        //            'id' => $this->id,
        //            'cus_uuid' => $this->cus_uuid,
        //            'district_uuid' => $this->district_uuid,
        //            'name' => $this->name,
        //            'description' => $this->description,
        //            'logo' => $this->logo,
        //            'student_domain' => $this->student_domain,
        //            'school_domain' => $this->school_domain,
        //            'status' => $this->status,
        //            'timezone' => $this->timezone,
        //            'message' => $this->message,
        //            'created_at' => $this->created_at,
        //            'updated_at' => $this->updated_at,
        //            'timezone_offset' => $this->timezone_offset,
        //            'old_id' => $this->old_id,
        //            'show_student_photos' => $this->show_student_photos,
        //            'auth_providers' => $this->auth_providers,
        //            'appointment_passes_count' => $this->appointment_passes_count,
        //            'auto_passes_count' => $this->auto_passes_count,
        //            'auto_passes_limits_count' => $this->auto_passes_limits_count,
        //            'block_passes_count' => $this->block_passes_count,
        //            'domains_count' => $this->domains_count,
        //            'hide_student_kiosk_password_field' =>
        //                $this->hide_student_kiosk_password_field,
        //            'show_photos' => $this->show_photos,
        //            'global_pass_limits_count' => $this->global_pass_limits_count,
        //            'location_capacities_count' => $this->location_capacities_count,
        //            'modules_count' => $this->modules_count,
        //            'pass_blocks_count' => $this->pass_blocks_count,
        //            'pass_limits_count' => $this->pass_limits_count,
        //            'passes_count' => $this->passes_count,
        //            'pin_attempts_count' => $this->pin_attempts_count,
        //            'pins_count' => $this->pins_count,
        //            'restrictions_count' => $this->restrictions_count,
        //            'room_types_count' => $this->room_types_count,
        //            'rooms_count' => $this->rooms_count,
        //            'school_user_count' => $this->school_user_count,
        //            'staff_schedules_count' => $this->staff_schedules_count,
        //            'students_count' => $this->students_count,
        //            'transparencies_count' => $this->transparencies_count,
        //            'transparency_users_count' => $this->transparency_users_count,
        //            'users_count' => $this->users_count
        //        ];
    }
}
