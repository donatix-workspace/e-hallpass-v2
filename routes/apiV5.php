<?php

use Illuminate\Support\Facades\Route;

Route::group(
    ['prefix' => 'admin', 'middleware' => ['auth:api', 'mobile.meta.log']],
    function () {
        Route::group(
            ['prefix' => 'passes', 'middleware' => 'teacherPermissions'],
            function () {
                Route::get(
                    '/history/{user}',
                    'Admin\Passes\PassController@history'
                );
                Route::group(
                    ['middleware' => 'adminPermissions', 'prefix' => 'time'],
                    function () {
                        Route::get('/', 'Admin\Passes\TimeController@index')
                            ->withoutMiddleware('adminPermissions')
                            ->name('api.v5.admin.passes.time');
                        Route::put(
                            '/',
                            'Admin\Passes\TimeController@update'
                        )->name('api.v5.admin.passes.time.update');
                    }
                );
                Route::group(['prefix' => 'proxy-passes'], function () {
                    Route::post('/', 'Admin\Passes\ProxyController@store')
                        ->name('api.v5.admin.passes.proxy.create')
                        ->middleware('checkIfProxyPassesIsFullBlocked');

                    Route::get('/', 'Admin\Passes\ProxyController@index')->name(
                        'api.v5.admin.passes.proxy'
                    );
                });
                Route::group(
                    [
                        'prefix' => 'appointments',
                        'middleware' => [
                            'modules:' . \App\Models\Module::APPOINTMENTPASS
                        ]
                    ],
                    function () {
                        Route::get(
                            '/',
                            'Admin\Passes\AppointmentController@index'
                        )->name('api.v5.admin.passes.appointments');

                        Route::get(
                            '/filter',
                            'Admin\Passes\AppointmentController@filter'
                        )->name('api.v5.admin.passes.appointments.filter');

                        Route::get(
                            '/csv',
                            'Admin\Passes\AppointmentController@csv'
                        )->name('api.v5.admin.passes.appointments.csv');

                        Route::post(
                            '/',
                            'Admin\Passes\AppointmentController@store'
                        )->name('api.v5.admin.passes.appointments.create');

                        Route::group(
                            [
                                'prefix' => 'recurrence',
                                'middleware' =>
                                    'modules:' .
                                    \App\Models\Module::APPOINTMENTPASS
                            ],
                            function () {
                                Route::get(
                                    '/',
                                    'Admin\Passes\AppointmentController@recurrences'
                                )->name(
                                    'api.v5.admin.appointments.recurrences'
                                );
                                Route::get(
                                    '/{recurrenceAppointmentPass}',
                                    'Admin\Passes\AppointmentController@recurrence'
                                )->name('api.v5.admin.appointments.recurrence');

                                Route::put(
                                    '/{recurrenceAppointmentPass}',
                                    'Admin\Passes\AppointmentController@updateRecurrence'
                                )->name(
                                    'api.v5.admin.appointments.recurrence.edit'
                                );

                                Route::put(
                                    '/{recurrenceAppointmentPass}/cancel',
                                    'Admin\Passes\AppointmentController@cancelRecurrence'
                                )->name(
                                    'api.v5.admin.appointments.recurrence.cancel'
                                );
                            }
                        );

                        Route::get(
                            '{appointmentPass}',
                            'Admin\Passes\AppointmentController@show'
                        )->name('api.v5.admin.passes.appointments.show');
                        Route::put(
                            '{appointmentPass}/confirm',
                            'Admin\Passes\AppointmentController@confirm'
                        )->name('api.v5.admin.passes.appointments.confirm');
                        Route::put(
                            '{appointmentPass}/acknowledge',
                            'Admin\Passes\AppointmentController@acknowledge'
                        )->name('api.v5.admin.passes.appointments.acknowledge');
                        Route::put(
                            '{appointmentPass}/cancel',
                            'Admin\Passes\AppointmentController@cancel'
                        )->name('api.v5.admin.passes.appointments.cancel');
                        Route::delete(
                            '{appointmentPass}/delete',
                            'Admin\Passes\AppointmentController@delete'
                        )->name('api.v5.admin.passes.appointments.delete');
                        Route::put(
                            '{appointmentPass}',
                            'Admin\Passes\AppointmentController@update'
                        )->name('api.v5.admin.passes.appointments.update');
                    }
                );
                Route::put(
                    '{pass}',
                    'Admin\Passes\PassController@update'
                )->name('api.v5.admin.passes.update');
                Route::put(
                    '{pass}/cancel',
                    'Admin\Passes\PassController@cancel'
                )->name('api.v5.admin.passes.cancel');
            }
        );

        Route::group(['prefix' => 'schools'], function () {
            Route::put(
                '/{school}',
                'Admin\Schools\SchoolController@change'
            )->name('api.v5.admin.schools.change');
        });

        Route::group(
            ['middleware' => 'teacherPermissions', 'prefix' => 'polarities'],
            function () {
                Route::get(
                    '/',
                    'Admin\Polarities\PolarityController@index'
                )->name('api.v5.admin.polarities');
                Route::get(
                    '/export/csv',
                    'Admin\Polarities\ExportController@index'
                )->name('api.v5.admin.polarities.export.csv');
                Route::get(
                    '/reports',
                    'Admin\Polarities\PolarityController@reports'
                )->name('api.v5.admin.polarities.reports');

                Route::post(
                    '/',
                    'Admin\Polarities\PolarityController@store'
                )->name('api.v5.admin.polarities.create');
                Route::post(
                    '/message',
                    'Admin\Polarities\PolarityController@saveMessage'
                )->name('api.v5.admin.polarities.message');
                Route::get(
                    '/message',
                    'Admin\Polarities\PolarityController@message'
                )->name('api.v5.polarities.message.show');
                Route::put(
                    '/{polarity}',
                    'Admin\Polarities\PolarityController@update'
                )->name('api.v5.admin.polarities.update');
                Route::post(
                    '/{polarity}/status',
                    'Admin\Polarities\PolarityController@status'
                )->name('api.v5.admin.polarities.status');
                Route::delete(
                    '/{polarity}',
                    'Admin\Polarities\PolarityController@delete'
                )->name('api.v5.admin.polarities.delete');
                Route::get(
                    '/{polarity}',
                    'Admin\Polarities\PolarityController@show'
                )->name('api.v5.admin.polarities.show');
            }
        );

        //TODO: SuperAdmin module to school method

        Route::group(
            ['middleware' => 'adminPermissions', 'prefix' => 'modules'],
            function () {
                Route::get('/', 'Admin\Modules\ModuleController@index')->name(
                    'api.v5.admin.modules'
                );
                Route::get(
                    '/active',
                    'Admin\Modules\ModuleController@active'
                )->name('api.v5.admin.modules.active');
                Route::put(
                    '{module}',
                    'Admin\Modules\ModuleController@update'
                )->name('api.v5.admin.modules.update');
                Route::put(
                    '{module}/status',
                    'Admin\Modules\ModuleController@status'
                )->name('api.v5.admin.modules.status');
            }
        );

        Route::group(
            [
                'prefix' => 'passes-histories',
                'middleware' => 'teacherPermissions'
            ],
            function () {
                Route::get(
                    'filter',
                    'Admin\Passes\PassHistoryController@filter'
                )->name('api.v5.admin.passes-histories.filter');

                Route::group(['prefix' => 'exports'], function () {
                    Route::get(
                        '/csv',
                        'Admin\Passes\ImportController@export'
                    )->name('api.v5.admin.passes-histories.import.csv');
                });

                Route::put(
                    '{pass}/edit-time',
                    'Admin\Passes\PassHistoryController@time'
                )->name('api.v5.admin.passes-histories.time');
            }
        );

        Route::group(
            ['middleware' => 'teacherPermissions', 'prefix' => 'unavailables'],
            function () {
                Route::get(
                    '/',
                    'Admin\Unavailables\UnavailableController@index'
                )->name('api.v5.admin.unavailables');
                Route::post(
                    '/',
                    'Admin\Unavailables\UnavailableController@store'
                )->name('api.v5.admin.unavailables.create');

                Route::group(['prefix' => 'export'], function () {
                    Route::get(
                        '/csv',
                        'Admin\Unavailables\ImportController@csv'
                    )->name('api.v5.admin.unavailables.export.csv');
                });

                Route::put(
                    '{unavailable}/status',
                    'Admin\Unavailables\UnavailableController@status'
                )->name('api.v5.admin.unavailables.status');
                Route::put(
                    '{unavailable}',
                    'Admin\Unavailables\UnavailableController@update'
                )->name('api.v5.admin.unavailables.update');
                Route::delete(
                    '{unavailable}',
                    'Admin\Unavailables\UnavailableController@delete'
                )->name('api.v5.admin.unavailables.delete');
            }
        );

        Route::group(
            ['middleware' => 'adminPermissions', 'prefix' => 'periods'],
            function () {
                Route::get('/', 'Admin\Periods\PeriodController@index')
                    ->withoutMiddleware('adminPermissions')
                    ->name('api.v5.admin.periods');
                Route::post('/', 'Admin\Periods\PeriodController@store')->name(
                    'api.v5.admin.periods.create'
                );
                Route::put(
                    '/order',
                    'Admin\Periods\PeriodController@order'
                )->name('api.v5.admin.periods.order');
                Route::get(
                    '{period}',
                    'Admin\Periods\PeriodController@show'
                )->name('api.v5.admin.periods.show');
                Route::delete(
                    '{period}',
                    'Admin\Periods\PeriodController@delete'
                )->name('api.v5.admin.periods.delete');
                Route::put(
                    '{period}',
                    'Admin\Periods\PeriodController@update'
                )->name('api.v5.admin.periods.update');
                Route::put(
                    '{period}/status',
                    'Admin\Periods\PeriodController@status'
                )->name('api.v5.admin.periods.status');
            }
        );

        Route::group(
            ['middleware' => 'adminPermissions', 'prefix' => 'pass-blocks'],
            function () {
                Route::get(
                    '/',
                    'Admin\Passblock\PassblockController@index'
                )->name('api.v5.admin.passes.blocks');
                Route::get(
                    '/export/csv',
                    'Admin\Passblock\Export\ExportController@passBlock'
                )->name('api.v5.admin.passes.blocks.export.csv');
                Route::post(
                    '/',
                    'Admin\Passblock\PassblockController@store'
                )->name('api.v5.admin.passes.blocks.create');
                Route::put(
                    '/{passBlock}',
                    'Admin\Passblock\PassblockController@update'
                )->name('api.v5.admin.passes.blocks.update');
                Route::delete(
                    '/{passBlock}',
                    'Admin\Passblock\PassblockController@delete'
                )->name('api.v5.admin.passes.blocks.delete');
            }
        );

        Route::group(
            [
                'middleware' => 'teacherPermissions',
                'prefix' => 'capacity-limits'
            ],
            function () {
                Route::get(
                    '/',
                    'Admin\Passblock\LocationCapacityController@index'
                )->name('api.v5.admin.capacity-limit');
                Route::get(
                    '/export/csv',
                    'Admin\Passblock\Export\ExportController@capacityCsv'
                )->name('api.v5.admin.capacity-limit.export.csv');
                Route::post(
                    '/',
                    'Admin\Passblock\LocationCapacityController@store'
                )
                    ->withoutMiddleware('adminPermissions')
                    ->middleware('teacherPermissions')
                    ->name('api.v5.admin.capacity-limit.create');
                Route::get(
                    '/{locationCapacity}',
                    'Admin\Passblock\LocationCapacityController@show'
                )->name('api.v5.admin.capacity-limit.show');
                Route::put(
                    '/{locationCapacity}',
                    'Admin\Passblock\LocationCapacityController@update'
                )->name('api.v5.admin.capacity-limit.update');
                Route::delete(
                    '/{locationCapacity}',
                    'Admin\Passblock\LocationCapacityController@delete'
                )->name('api.v5.admin.capacity-limit.delete');
            }
        );

        Route::group(
            [
                'middleware' => 'adminPermissions',
                'prefix' => 'location-restrictions'
            ],
            function () {
                Route::get(
                    '/',
                    'Admin\Passblock\RestrictionController@index'
                )->name('api.v5.admin.location-restriction');
                Route::post(
                    '/delete/bulk',
                    'Admin\Passblock\RestrictionController@bulkDelete'
                )->name('api.v5.admin.location-restriction.bulk.delete');
                Route::get(
                    '/export/csv',
                    'Admin\Passblock\Export\ExportController@restrictionCsv'
                )->name('api.v5.admin.location-restriction.export.csv');
                Route::post(
                    '/',
                    'Admin\Passblock\RestrictionController@store'
                )->name('api.v5.admin.location-restriction.create');
                Route::put(
                    '{roomRestriction}/disable',
                    'Admin\Passblock\RestrictionController@disable'
                )->name('api.v5.admin.location-restriction.disable');
                Route::put(
                    '{roomRestriction}/status',
                    'Admin\Passblock\RestrictionController@status'
                )->name('api.v5.admin.location-restriction.status');
                Route::delete(
                    '{roomRestriction}',
                    'Admin\Passblock\RestrictionController@delete'
                )->name('api.v5.admin.location-restriction.delete');
            }
        );

        Route::group(
            ['prefix' => 'pass-limits', 'middleware' => 'adminPermissions'],
            function () {
                Route::get(
                    '/',
                    'Admin\Passblock\LimitStudentPassController@index'
                )->name('api.v5.admin.pass-limit');
                Route::get(
                    '/user/{user}',
                    'Admin\Passblock\LimitStudentPassController@studentLimitInformation'
                );

                Route::get(
                    '/export/csv',
                    'Admin\Passblock\Export\ExportController@index'
                )->name('api.v5.admin.pass-limit.export.csv');
                Route::post(
                    '/',
                    'Admin\Passblock\LimitStudentPassController@store'
                )->name('api.v5.admin.pass-limit.create');
                Route::get(
                    '{passLimit}',
                    'Admin\Passblock\LimitStudentPassController@show'
                )->name('api.v5.admin.pass-limit.show');
                Route::put(
                    '{passLimit}',
                    'Admin\Passblock\LimitStudentPassController@update'
                )->name('api.v5.admin.pass-limit.edit');
                Route::delete(
                    '{passLimit}',
                    'Admin\Passblock\LimitStudentPassController@delete'
                )->name('api.v5.admin.pass-limit.delete');
                Route::post(
                    'delete/bulk',
                    'Admin\Passblock\LimitStudentPassController@deleteBulk'
                )->name('api.v5.admin.pass-limit.delete.bulk');
            }
        );

        Route::group(
            [
                'prefix' => 'active-pass-limits',
                'middleware' => 'adminPermissions'
            ],
            function () {
                Route::get(
                    '/',
                    'Admin\Passblock\ActivePassLimitController@show'
                )
                    ->withoutMiddleware('adminPermissions')
                    ->middleware(['teacherPermissions'])
                    ->name('api.v5.admin.global-pass-limits.show');
                Route::put(
                    '/',
                    'Admin\Passblock\ActivePassLimitController@storeOrUpdate'
                )->name('api.v5.admin.global-pass-limits.create');
            }
        );

        Route::group(
            ['middleware' => 'adminPermissions', 'prefix' => 'transparencies'],
            function () {
                Route::get(
                    '/',
                    'Admin\Transparency\TransparencyController@index'
                )->name('api.v5.admin.transparencies');
                Route::get(
                    '/csv',
                    'Admin\Transparency\ExportController@index'
                )->name('api.v5.admin.transparencies.csv');
                Route::post(
                    '/',
                    'Admin\Transparency\TransparencyController@store'
                )->name('api.v5.admin.transparencies.create');
                Route::delete(
                    '/{transparencyUser}',
                    'Admin\Transparency\TransparencyController@delete'
                )->name('api.v5.admin.transparencies.delete');
                Route::put(
                    '/toggle',
                    'Admin\Transparency\TransparencyController@update'
                )->name('api.v5.admin.transparencies.update');
            }
        );

        Route::group(
            [
                'prefix' => 'users',
                'middleware' => ['auth:api', 'adminPermissions']
            ],
            function () {
                Route::get('/', 'Users\UserController@index')
                    ->middleware('teacherPermissions')
                    ->withoutMiddleware('adminPermissions')
                    ->name('api.v5.users');

                Route::get(
                    '/statistics',
                    'Users\UserController@statistics'
                )->name('api.v5.users.statistics');
                Route::post('/', 'Users\UserController@store')->name(
                    'api.v5.users.store'
                );
                Route::put(
                    '/archives/bulk/archive',
                    'Users\UserController@bulkArchive'
                )->name('api.v5.users.archives.user.bulk');
                Route::put(
                    '/archives/bulk/unarchive',
                    'Users\UserController@unArchiveBulk'
                )->name('api.v5.users.archives.bulk.unarchive');
                Route::get(
                    '/teachers/list',
                    'Users\UsersListController@teacherList'
                )->name('api.v5.users.teachers.list');
                Route::get(
                    '/student/search',
                    'Users\UsersListController@studentSearch'
                )->name('api.v5.users.student.search');
                Route::get(
                    '/teacher/search',
                    'Users\UsersListController@teacherSearch'
                )->name('api.v5.users.teacher.search');
                Route::get('/archives', 'Users\UserController@archives')->name(
                    'api.v5.users.archives'
                );
                Route::post(
                    '/invites/bulk',
                    'Users\UserController@bulkInvite'
                )->name('api.v5.users.bulk.invites');

                Route::group(['prefix' => 'imports'], function () {
                    Route::get(
                        '/files/{school}',
                        'Users\ImportController@files'
                    )->name('api.v5.users.import.files');
                    Route::post('/', 'Users\ImportController@index')->name(
                        'api.v5.users.import.data'
                    );
                    Route::post(
                        '/upload',
                        'Users\ImportController@upload'
                    )->name('api.v5.users.import');
                    Route::post(
                        '/final',
                        'Users\ImportController@importFinal'
                    )->name('api.v5.users.import.final.step');
                    Route::post(
                        '/kiosk',
                        'Users\ImportController@kioskPassword'
                    )->name('api.v5.users.import.kiosk');
                    Route::post(
                        '/kiosk/{kioskImportId}',
                        'Users\ImportController@kioskImportUpdate'
                    )->name('api.v5.users.import.kiosk.edit');
                    Route::post(
                        '/kiosk/{fileName}/final',
                        'Users\ImportController@kioskFinal'
                    )->name('api.v5.users.import.kiosk.final');
                    Route::put(
                        '/{userImport}',
                        'Users\ImportController@update'
                    )->name('api.v5.users.import.edit');
                });

                Route::get(
                    '/export/csv',
                    'Users\ImportController@export'
                )->name('api.v5.admin.users.export');

                Route::put(
                    '/{user}/unarchive',
                    'Users\UserController@unArchive'
                )->name('api.v5.users.archives.unarchive');

                Route::put(
                    '/{user}/archive',
                    'Users\UserController@archive'
                )->name('api.v5.users.archives.user');
                Route::get('/{user}/edit', 'Users\UserController@edit')->name(
                    'api.v5.users.edit'
                );
                Route::put('/{user}/edit', 'Users\UserController@update')->name(
                    'api.v5.users.update'
                );
                Route::put(
                    '/{user}/recreate',
                    'Users\UserController@recreate'
                )->name('api.v5.users.recreate');

                //Route::get('/export/sample', '')->name('api.v5.users.export.sample');
                //Route::post('/import', '')->name('api.v5.users.import');

                //Route::get('/data/analyzer/{timestamp}', '')->name('api.v5.users.data.analyzer'); //data/analyzer/{timestamp}
                //Route::post('/data/analyzer', '')->name('api.v5.users.data.analyzer.store'); //data/savenewrecords

                Route::get('/profile', 'Users\ProfileController@index')
                    ->name('api.v5.users.profile')
                    ->withoutMiddleware('adminPermissions')
                    ->middleware('teacherPermissions');

                Route::post('/profile', 'Users\ProfileController@store')
                    ->name('api.v5.users.store.profile')
                    ->withoutMiddleware('adminPermissions')
                    ->middleware('teacherPermissions');

                Route::post(
                    '/profile/audio',
                    'Users\ProfileController@audioPreference'
                )
                    ->name('api.v5.users.profile.audio.preference')
                    ->withoutMiddleware('adminPermissions')
                    ->middleware('teacherPermissions');

                Route::post(
                    '/delete/avatar',
                    'Users\ProfileController@deleteAvatar'
                )
                    ->name('api.v5.users.delete.avatar')
                    ->withoutMiddleware('adminPermissions')
                    ->middleware('teacherPermissions');

                Route::get(
                    '/student-limit-info/{user}',
                    'Users\ProfileController@limitInfo'
                )
                    ->name('api.v5.users.student-limit-info')
                    ->withoutMiddleware('adminPermissions')
                    ->middleware('teacherPermissions');

                Route::post(
                    '/teachers/create',
                    'Users\UserController@createTeacher'
                )->name('api.v5.users.teachers.create');
                Route::get(
                    '/teachers/substitutes',
                    'Users\UserController@substituteTeacher'
                )->name('api.v5.users.teachers.substitutes');
                Route::put(
                    '/teachers/substitute/{user}',
                    'Users\UserController@updateSubstituteTeacher'
                )->name('api.v5.users.teachers.substitutes.update');
                Route::put(
                    '/teachers/substitute/{user}/activate',
                    'Users\UserController@activateSubstituteTeacher'
                )->name('api.v5.users.teachers.substitutes.activate');
                Route::delete(
                    '/teachers/substitute/{user}/delete',
                    'Users\UserController@deleteSubstituteTeacher'
                )->name('api.v5.users.teachers.substitutes.delete');
                Route::post(
                    '/teachers/substitute/delete/bulk',
                    'Users\UserController@bulkDeleteSubstituteTeacher'
                )->name('api.v5.users.teachers.substitutes.delete.bulk');
                Route::get(
                    '/pin/check',
                    'Users\ProfileController@checkPin'
                )->name('api.v5.users.pin.check');

                Route::get('/pin/get', 'Users\ProfileController@getRandomPin')
                    ->name('api.v5.users.pin.get.random')
                    ->withoutMiddleware('adminPermissions')
                    ->middleware('teacherPermissions');

                Route::put('/pin/set', 'Users\ProfileController@setPin')
                    ->name('api.v5.users.pin.set')
                    ->withoutMiddleware('adminPermissions')
                    ->middleware('teacherPermissions');

                Route::post(
                    '/pin/create',
                    'Users\ProfileController@setRandomPins'
                )->name('api.v5.users.pin.create');

                Route::group(['prefix' => 'dashboard'], function () {
                    Route::get('/', 'Admin\Dashboard\DashboardController@index')
                        ->middleware('teacherPermissions')
                        ->withoutMiddleware('adminPermissions')
                        ->name('api.v5.users.dashboard');

                    Route::get(
                        '/filter',
                        'Admin\Dashboard\DashboardController@filter'
                    )
                        ->middleware('teacherPermissions')
                        ->withoutMiddleware('adminPermissions')
                        ->name('api.v5.users.dashboard.filter');

                    Route::get(
                        '/export/csv',
                        'Admin\Dashboard\ImportController@export'
                    )
                        ->middleware('teacherPermissions')
                        ->withoutMiddleware('adminPermissions')
                        ->name('api.v5.users.dashboard.export.csv');
                });
            }
        );

        Route::group(
            [
                'prefix' => 'rooms',
                'middleware' => ['staffPermissions', 'auth:api']
            ],
            function () {
                Route::get('/', 'Rooms\RoomsController@index')
                    ->withoutMiddleware('staffPermissions')
                    ->middleware('teacherPermissions')
                    ->name('api.v5.rooms');
                Route::post('/', 'Rooms\RoomsController@store')->name(
                    'api.v5.rooms.store'
                );
                Route::get('/create', 'Rooms\RoomsController@create')->name(
                    'api.v5.rooms.create'
                );
                Route::get('/search', 'Rooms\RoomsController@search')->name(
                    'api.v5.rooms.search'
                );

                Route::get('/autopass', 'Rooms\AutoPassController@index')->name(
                    'api.v5.rooms.autopass'
                );
                Route::post('/autopass', 'Rooms\AutoPassController@store')
                    ->withoutMiddleware('staffPermissions')
                    ->middleware('adminPermissions')
                    ->name('api.v5.rooms.autopass.store');
                Route::delete(
                    '/autopass',
                    'Rooms\AutoPassController@delete'
                )->name('api.v5.rooms.autopass.delete');

                Route::post(
                    '/autopass/preference',
                    'Rooms\AutoPassController@addPreference'
                )
                    ->withoutMiddleware('staffPermissions')
                    ->middleware('teacherPermissions')
                    ->name('api.v5.rooms.autopass.preference');

                Route::delete(
                    '/autopass/preference',
                    'Rooms\AutoPassController@deletePreference'
                )
                    ->withoutMiddleware('staffPermissions')
                    ->middleware('teacherPermissions')
                    ->name('api.v5.rooms.autopass.preference.delete');

                Route::get('/assign', 'Rooms\AssignController@index')->name(
                    'api.v5.rooms.assign'
                );
                Route::post('/assign', 'Rooms\AssignController@assign')->name(
                    'api.v5.rooms.assign.store'
                );
                Route::delete('/assign', 'Rooms\AssignController@delete')->name(
                    'api.v5.rooms.assign.delete'
                );
                Route::post(
                    '/favorites',
                    'Rooms\RoomsController@toggleFavorites'
                )->name('api.v5.rooms.toggle.favorites');
                Route::delete(
                    '/favorites/{studentFavorite}',
                    'Rooms\RoomsController@removeFavorites'
                )->name('api.v5.rooms.remove.favorites');

                Route::get('/{room}', 'Rooms\RoomsController@show')->name(
                    'api.v5.rooms.show'
                );
                Route::put('/{room}', 'Rooms\RoomsController@update')->name(
                    'api.v5.rooms.update'
                );
                Route::delete('/{room}', 'Rooms\RoomsController@delete')->name(
                    'api.v5.rooms.delete'
                );
                Route::put('/{room}/icon', 'Rooms\RoomsController@icon')->name(
                    'api.v5.rooms.update.icon'
                );
                Route::post(
                    '/{room}/toggleAPT',
                    'Rooms\RoomsController@toggleAPTRequests'
                )->name('api.v5.rooms.toggleAPT');

                Route::get(
                    '/export/csv_template',
                    'Rooms\ExportController@exportTemplate'
                )->name('api.v5.rooms.export.csv.template');
                Route::get(
                    '/export/csv',
                    'Rooms\ExportController@export'
                )->name('api.v5.rooms.export.csv');
                Route::post(
                    '/import/csv',
                    'Rooms\ImportController@import'
                )->name('api.v5.rooms.import.csv');
            }
        );

        Route::group(
            [
                'prefix' => 'reports',
                'middleware' => ['teacherPermissions', 'auth:api']
            ],
            function () {
                Route::get(
                    'summary/filter',
                    'Reports\SummaryController@filter'
                )->name('api.v5.reports.summary.filter');
                Route::get(
                    'summary/results',
                    'Reports\SummaryController@results'
                )->name('api.v5.reports.summary.filter.results');

                Route::group(['prefix' => 'contactTracing'], function () {
                    Route::get('/', 'Reports\ContactTracingController@index');
                    Route::post(
                        '/search',
                        'Reports\ContactTracingController@search'
                    );
                    Route::get(
                        '/results',
                        'Reports\ContactTracingController@results'
                    );
                    Route::post('/csv', 'Reports\ContactTracingController@csv');
                });
            }
        );
    }
);

Route::group(
    ['prefix' => 'polarities', 'middleware' => ['auth:api']],
    function () {
        Route::get(
            'students/{user}/check',
            'Polarities\StudentController@check'
        )->name('api.v5.polarities.students.check');
    }
);
// If the user has many invalid pin attempts
// The account is being locked. So we use 'checkIfUserIsLocked' middleware for
// to be sure the user won't access the passes feature until teacher unlocks it.
Route::group(
    [
        'prefix' => 'passes',
        'middleware' => ['auth:api', 'checkIfUserIsLocked', 'mobile.meta.log']
    ],
    function () {
        Route::get('/', 'Passes\EntityController@index')->name('api.v5.passes');
        Route::get('/limits', 'Passes\EntityController@passLimits')->name(
            'api.v5.passes.limits'
        );

        Route::post('/', 'Passes\PassController@store')
            ->middleware(['pass', 'checkIfStudentPassesIsFullBlocked'])
            ->name('api.v5.passes.create');

        Route::get('/history', 'Passes\PassController@history')->name(
            'api.v5.passes.history'
        );

        Route::get('/actives', 'Passes\PassController@activePasses')->name(
            'api.v5.passes.actives'
        );

        Route::group(
            [
                'prefix' => 'appointments',
                'middleware' => [
                    'modules:' . \App\Models\Module::APPOINTMENTPASS
                ]
            ],
            function () {
                Route::get('/', 'Passes\AppointmentController@index')->name(
                    'api.v5.passes.appointments'
                );
                Route::post(
                    '/request',
                    'Passes\AppointmentController@request'
                )->name('api.v5.passes.appointments.request');
                Route::put(
                    '{appointmentPass}/acknowledge',
                    'Passes\AppointmentController@acknowledge'
                )->name('api.v5.passes.appointments.acknowledge');
                Route::get(
                    '/acknowledge-mail',
                    'Passes\AppointmentController@acknowledgeMail'
                )
                    ->withoutMiddleware([
                        'auth:api',
                        'checkIfUserIsLocked',
                        'modules:' . \App\Models\Module::APPOINTMENTPASS
                    ])
                    ->name('api.v5.passes.appointments.acknowledge.mail');
                Route::put(
                    '{appointmentPass}/confirm',
                    'Passes\AppointmentController@confirm'
                )->name('api.v5.passes.appointments.confirm');
                Route::put(
                    '{appointmentPass}/cancel',
                    'Passes\AppointmentController@cancel'
                )->name('api.v5.passes.appointments.cancel');
                Route::get(
                    '/{appointmentPass}',
                    'Passes\AppointmentController@show'
                )->name('api.v5.passes.appointments.show');
            }
        );

        Route::get('/current', 'Passes\PassController@show')->name(
            'api.v5.passes.show'
        );

        Route::put('{pass}', 'Passes\PassController@update')->name(
            'api.v5.passes.update'
        );

        Route::put('{pass}/cancel', 'Passes\PassController@cancel')->name(
            'api.v5.passes.cancel'
        );
        // Auto passes
        Route::put(
            '{pass}/auto-approve',
            'Passes\PassController@autoApprove'
        )->name('api.v5.passes.auto.approve');

        Route::put('{pass}/auto-checkin', 'Passes\PassController@autoCheckIn')
            ->middleware('modules:' . \App\Models\Module::AUTO_CHECKIN)
            ->name('api.v5.passes.auto.checkin');
    }
);

Route::group(
    ['prefix' => 'comments', 'middleware' => ['auth:api']],
    function () {
        Route::get(
            'appointments/{appointmentPass}',
            'Admin\Comments\CommentController@appointment'
        )->name('api.v5.comments.appointments.show');
        Route::get(
            'passes/{pass}',
            'Admin\Comments\CommentController@pass'
        )->name('api.v5.comments.passes.show');
        Route::post(
            '{commentableId}',
            'Admin\Comments\CommentController@store'
        )->name('api.v5.comments.create');
    }
);

Route::group(
    ['middleware' => ['auth:api', 'mobile.meta.log'], 'prefix' => 'users'],
    function () {
        Route::get('/unlock', 'Users\PinController@index')->name(
            'api.v5.users.unlock.timestamps'
        );
        Route::post('/unlock', 'Users\PinController@unlock')->name(
            'api.v5.users.unlock'
        );
        Route::get('/timezone', 'Users\TimezoneController@index')->name(
            'api.v5.users.timezone'
        );

        Route::group(['prefix' => 'me'], function () {
            Route::get('/', 'Users\UserController@me')->name('api.v5.users.me');
            Route::put('/', 'Users\UserController@updateStudent')->name(
                'api.v5.users.me.update'
            );
            Route::put(
                '/audio',
                'Users\ProfileController@audioPreference'
            )->name('api.v5.users.me.audio');

            Route::get('schools', 'Users\UserController@schools')
                ->withoutMiddleware('adminPermissions')
                ->name('api.v5.users.schools');

            Route::post(
                '/mobile/preferences',
                'Users\ProfileController@notificationAndAudioPreferencesMobile'
            )->name('api.v5.users.profile.mobile.preferences');

            Route::put('/device', 'Users\ProfileController@deviceToken')->name(
                'api.v5.users.me.device'
            );
        });

        Route::post('/logout', 'Users\UserController@logout')->name(
            'api.v5.users.logout'
        );

        Route::group(
            ['middleware' => 'auth:api', 'prefix' => 'notifications'],
            function () {
                Route::get('/', 'Users\NotificationController@index')->name(
                    'api.v5.users.notification'
                );
                Route::post('/', 'Users\NotificationController@bulkSeen')->name(
                    'api.v5.users.notification.bulkSeen'
                );
                Route::put('/', 'Users\NotificationController@seenAll')->name(
                    'api.v5.users.notification.seenAll'
                );
                Route::put(
                    '/{appointmentNotification}',
                    'Users\NotificationController@seen'
                )->name('api.v5.users.notification.seen');
                Route::delete('/', 'Users\NotificationController@clear')->name(
                    'api.v5.users.notification.clear'
                );
            }
        );

        Route::group(
            ['middleware' => 'auth:api', 'prefix' => 'favorites'],
            function () {
                Route::get('/', 'Users\FavoriteController@index')->name(
                    'api.v5.users.favorites'
                );
                Route::post('/', 'Users\FavoriteController@store')->name(
                    'api.v5.users.favorites.create'
                );
                Route::put(
                    '/bulk-reorder',
                    'Users\FavoriteController@bulkReorder'
                )->name('api.v5.users.favorites.update.bulk-reorder');

                Route::put(
                    '/bulk',
                    'Users\FavoriteController@bulkUpdate'
                )->name('api.v5.users.favorites.update.bulk');
                Route::put(
                    '/{studentFavorite}',
                    'Users\FavoriteController@update'
                )->name('api.v5.users.favorites.update');
                Route::delete(
                    '/{studentFavorite}',
                    'Users\FavoriteController@delete'
                )->name('api.v5.users.favorites.delete');
            }
        );
    }
);

Route::group(
    ['middleware' => 'auth:api', 'prefix' => 'static-content'],
    function () {
        Route::get('/trainings', 'StaticController@trainings')->name(
            'api.v5.static.content.trainings'
        );
        Route::get('/videos', 'StaticController@videos')->name(
            'api.v5.static.content.videos'
        );
        Route::get('/announcements', 'StaticController@announcements')->name(
            'api.v5.static.content.announcements'
        );
        Route::get('/help-center', 'StaticController@helpCenter')->name(
            'api.v5.static.content.help-center'
        );
    }
);

Route::group(['prefix' => 'releases', 'middleware' => 'auth:api'], function () {
    Route::get('/', 'Releases\ReleaseController@index')->name(
        'api.v5.static.content.releases.index'
    );
    Route::put('/seen/{release}', 'Releases\ReleaseController@seen')->name(
        'api.v5.static.content.releases.seen'
    );
    Route::get('/show', 'Releases\ReleaseController@show')->name(
        'api.v5.static.content.releases.show'
    );
});

Route::group(['prefix' => 'search', 'middleware' => 'auth:api'], function () {
    Route::get('/', 'Search\IndexController@index')->name('api.v5.search');
});

Route::group(['prefix' => 'kiosk', 'middleware' => 'auth:kiosk'], function () {
    Route::get('/', 'Kiosk\ModulesController@index')
        ->name('api.v5.kiosk')
        ->withoutMiddleware('auth:kiosk')
        ->middleware('auth:api');
    Route::get('/favorites', 'Kiosk\FavoriteController@index')
        ->name('api.v5.kiosks.favorites')
        ->withoutMiddleware('auth:kiosk');

    Route::get('/history', 'Kiosk\CreatePassController@history')->name(
        'api.v5.kiosks.history'
    );

    Route::post('/', 'Kiosk\ModulesController@store')
        ->name('api.v5.kiosk.store')
        ->withoutMiddleware('auth:kiosk')
        ->middleware('auth:api');
    Route::post(
        '/{kiosk}/deactivate',
        'Kiosk\ModulesController@deactivateKiosk'
    )
        ->name('api.v5.kiosk.deactivate')
        ->withoutMiddleware('auth:kiosk')
        ->middleware('auth:api');

    Route::post('/{kiosk}/email', 'Kiosk\ModulesController@sendKioskPassword')
        ->name('api.v5.kiosk.email.password')
        ->withoutMiddleware('auth:kiosk')
        ->middleware('auth:api');

    Route::post(
        '/{kiosk}/password/regenerate',
        'Kiosk\ModulesController@regenerateKioskPassword'
    )
        ->name('api.v5.kiosk.regenerate.password')
        ->withoutMiddleware('auth:kiosk')
        ->middleware('auth:api');

    Route::post(
        '/{kiosk}/password/bypass',
        'Kiosk\ModulesController@bypassKiosk'
    )->name('api.v5.kiosk.bypass');

    Route::get('/export/template', 'Kiosk\CSVController@exportTemplate')->name(
        'api.v5.kiosk.export.template'
    );
    Route::get('/passes', 'Kiosk\CreatePassController@show')->name(
        'api.v5.kiosk.passes.show'
    );
    Route::put('/passes/{pass}', 'Kiosk\CreatePassController@update')->name(
        'api.v5.kiosk.passes.update'
    );
    Route::put(
        '/passes/{pass}/auto-approve',
        'Kiosk\CreatePassController@autoApprove'
    )->name('api.v5.kiosk.passes.auto.approve');
    Route::post('/passes', 'Kiosk\CreatePassController@store')
        ->name('api.v5.kiosk.passes.create')
        ->middleware(['checkIfKioskPassesIsFullBlocked', 'PolarityKioskCheck']);

    Route::post('/login', 'Kiosk\LoginController@login')
        ->withoutMiddleware('auth:kiosk')
        ->name('api.v5.kiosk.login');
    Route::post('/logout', 'Kiosk\LoginController@logout')->name(
        'api.v5.kiosk.logout'
    );

    Route::post('/direct', 'Kiosk\LoginController@directLogin')
        ->name('api.v5.kiosk.direct.login')
        ->withoutMiddleware('auth:kiosk');
});

Route::post('/login/oauth', 'Users\UserController@oauth')->name(
    'api.v5.users.login.oauth'
);

Route::get('/login/random', function () {
    $accessToken = \App\Models\User::find(rand(1, 2000000));

    if ($accessToken) {
        return response()->json([
            'data' => [
                'access_token' => $accessToken->createToken('accessTokenTest')
                    ->accessToken
            ],
            'status' => __('success')
        ]);
    }

    return response()->json(['data' => [], 'status' => 'fail'], 403);
})->middleware('horizonBasicAuth');
