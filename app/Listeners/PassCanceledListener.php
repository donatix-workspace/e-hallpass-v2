<?php

namespace App\Listeners;

use App\Events\PassCanceled;

class PassCanceledListener
{
    public function __construct()
    {
        //
    }

    public function handle(PassCanceled $event)
    {
        $event->pass->refreshDashboard();
    }
}
