<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePassSummaryReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pass_summary_reports', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('school_id')->unsigned()->index('school_id');
            $table->bigInteger('user_id')->unsigned()->index('user_id');
            $table->integer('parent')->index('parent');
            $table->string('pass_code', 10)->index('pass_code');
            $table->integer('preapproved');
            $table->bigInteger('pass_id')->unsigned();
            $table->dateTime('outdate');
            $table->dateTime('indate');
            $table->bigInteger('from_teacher_id')->unsigned();
            $table->bigInteger('to_teacher_id')->unsigned();
            $table->bigInteger('from_room_id')->unsigned()->nullable();
            $table->bigInteger('to_room_id')->unsigned()->nullable();
            $table->integer('outaction');
            $table->integer('inaction');
            $table->integer('out_by_teacher');
            $table->integer('in_by_teacher');
            $table->bigInteger('out_role_id')->unsigned();
            $table->bigInteger('in_role_id')->unsigned();
            $table->integer('pass_status');
            $table->integer('period');
            $table->integer('created_by');
            $table->bigInteger('from_teacher_id2')->unsigned();
            $table->bigInteger('to_teacher_id2')->unsigned();
            $table->bigInteger('from_room_id2')->unsigned()->nullable();
            $table->bigInteger('to_room_id2')->unsigned()->nullable();
            $table->integer('outaction2');
            $table->integer('inaction2');
            $table->integer('out_by_teacher2');
            $table->integer('in_by_teacher2');
            $table->integer('totalTime')->comment('Time in seconds.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pass_summary_reports');
    }
}
