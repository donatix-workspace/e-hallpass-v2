<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class StaffRoomsPinRule implements Rule
{
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $pins = explode(',', $value);

        foreach ($pins as $pin) {
            if (strlen($pin) < 4 || strlen($pin) > 6) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('invalid.pin.format');
    }
}
