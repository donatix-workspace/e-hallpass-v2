<?php

namespace App\Elastic;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class TransparenciesIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'transparencies_index';
    /**
     * @var array
     */
    protected $settings = [
        'analysis' => [
            'analyzer' => [
                'lowercase' => [
                    'type' => 'custom',
                    'tokenizer' => 'keyword',
                    'filter' => [
                        'lowercase'
                    ]
                ]
            ]
        ],
        'index.mapping.total_fields.limit' => 2000,
        'index.max_result_window' => 500000
    ];
}
