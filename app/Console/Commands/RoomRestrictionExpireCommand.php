<?php

namespace App\Console\Commands;

use App\Models\RoomRestriction;
use DB;
use Illuminate\Console\Command;

class RoomRestrictionExpireCommand extends Command
{
    protected $signature = 'room_restrictions:expire';

    protected $description = 'Change the status to inactive for the restrictions with expired date.';

    /**
     * @throws \Throwable
     */
    public function handle()
    {
        $this->info('Starting expiring the restrictions');

        DB::beginTransaction();

        RoomRestriction::where('to_date', '<', now()->toDateString())
            ->where('status', RoomRestriction::ACTIVE)
            ->update([
                'status' => RoomRestriction::INACTIVE
            ]);

        DB::commit();

        $this->info('Room restriction expiration finished!');
    }
}
