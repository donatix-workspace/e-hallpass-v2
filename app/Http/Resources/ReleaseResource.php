<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Release */
class ReleaseResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => optional($this)->id,
            'school_id' => optional($this)->school_id,
            'heading' => optional($this)->heading,
            'content' => optional($this)->content,
            'version' => optional($this)->version,
            'seen' => optional($this)->seen,
            'created_at' => optional($this)->created_at,
            'updated_at' => optional($this)->updated_at,
        ];
    }
}
