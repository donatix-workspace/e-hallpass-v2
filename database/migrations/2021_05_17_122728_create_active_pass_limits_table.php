<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivePassLimitsTable extends Migration
{
    public function up()
    {
        Schema::create('active_pass_limits', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->boolean('student_pass')
                ->default(0)
                ->nullable();

            $table->boolean('kiosk_pass')
                ->default(0)
                ->nullable();

            $table->boolean('proxy_pass')
                ->default(0)
                ->nullable();

            $table->boolean('teacher_override')
                ->default(0)
                ->nullable();

            $table->integer('limit');

            $table->unsignedBigInteger('school_id');

            $table->foreign('school_id')->references('id')->on('schools');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('active_pass_limits');
    }
}
