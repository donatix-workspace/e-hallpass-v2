<?php

return [
    'deprecated' => [
        'US/Eastern' => 'America/New_York',
        'US/Mountain' => 'America/Denver',
        'US/Central' => 'America/Chicago',
        'US/Pacific' => 'America/Los_Angeles',
        'US/Alaska' => 'America/Anchorage',
        'US/Aleutian' => 'America/Adak',
        'US/Arizona' => 'America/Phoenix',
        'US/East-Indiana' => 'America/Indiana/Indianapolis',
        'US/Hawaii' => 'Pacific/Honolulu',
        'US/Indiana-Starke' => 'America/Indiana/Knox',
        'US/Michigan' => 'America/Detroit',
        'US/Samoa' => 'Pacific/Pago_Pago'
    ],
    'offsets' => [
        'America/New_York' => '-05:00',
        'America/Denver' => '-07:00',
        'America/Chicago' => '-06:00',
        'America/Los_Angeles' => '-08:00',
        'Africa/Porto-Novo' => '+01:00',
        'America/Anchorage' => '-09:00',
        'America/Argentina/Salta' => '-03:00',
        'America/Fortaleza' => '-03:00',
        'America/Argentina/San_Juan' => '-03:00',
        'America/Havana' => '-05:00',
        'America/Lima' => '-05:00',
        'America/Phoenix' => '-07:00',
        'Asia/Famagusta' => '+02:00',
        'Asia/Khandyga' => '+09:00',
        'Asia/Vladivostok' => '+10:00',
        'Atlantic/Canary' => '+00:00',
        'Europe/Uzhgorod' => '+02:00',
        'Indian/Cocos' => '+06:30',
        'Pacific/Honolulu' => '-10:00',
        'Europe/Sofia' => '+02:00'
    ]
];
