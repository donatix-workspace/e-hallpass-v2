<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PolarityMessageRequest extends FormRequest
{
    public function rules()
    {
        return [
            'message' => 'string|max:256|sometimes|nullable'
        ];
    }

    public function authorize()
    {
        return !auth()->user()->isStudent();
    }
}
