<?php

namespace Database\Seeders;

use App\Models\AutoPass;
use App\Models\Room;
use App\Models\RoomType;
use Illuminate\Database\Seeder;
use MongoDB\BSON\Type;

class AutoPassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $autoPasses = AutoPass::factory()
            ->for(Room::factory()->forSchool()->for(RoomType::factory()->forSchool()))
            ->forSchool()
            ->count(20)
            ->create();
        $autoPasses->each(function ($autoPass) {

        });
    }
}
