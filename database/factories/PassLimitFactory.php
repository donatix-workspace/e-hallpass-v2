<?php

namespace Database\Factories;

use App\Models\PassLimit;
use App\Models\School;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PassLimitFactory extends Factory
{
    protected $model = PassLimit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'limitable_type' => $this->faker->word,
            'limitable_id' => $this->faker->randomNumber(),
            'limit' => $this->faker->randomNumber(),
            'from_date' => $this->faker->word,
            'to_date' => $this->faker->word,
            'grade_year' => $this->faker->word,
            'reason' => $this->faker->word,
            'recurrence_type' => $this->faker->word,
            'archived' => $this->faker->randomNumber(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            'school_id' => function () {
                return School::factory()->create()->id;
            },
        ];
    }
}
