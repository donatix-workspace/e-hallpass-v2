<?php

namespace App\Traits;

trait PassSummaryReportMapping
{
    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'school_id' => [
                'type' => 'long'
            ],
            'destination_type' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'destination_id' => [
                'type' => 'long',
            ],
            'destination' => [
                'properties' => [
                    'name' => [
                        'type' => 'keyword',
                        'normalizer' => 'lowercase'
                    ]
                ]
            ],
            'total_passes' => [
                'type' => 'keyword',
                'fields' => [
                    'raw_int' => [
                        'type' => 'integer'
                    ]
                ]
            ],
            'total_time_all' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'total_time_ksk' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'from_date' => [
                'type' => 'date',
                "format" => "hour_minute||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||HH:mm||HH||epoch_millis"
            ],
            'to_date' => [
                'type' => 'date',
                "format" => "hour_minute||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||HH:mm||HH||epoch_millis"
            ],
            'total_time_proxy' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'total_time_student' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'total_time_apt' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'selected_filter' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ],
            'search_session_id' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase'
            ]
        ]
    ];
}
